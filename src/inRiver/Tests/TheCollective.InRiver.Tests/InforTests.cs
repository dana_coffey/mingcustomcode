﻿using Absolunet.InRiver.Core.Repositories;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting;
using NUnit.Framework;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;
using TheCollective.InRiver.InboundConnector.Infor.Repositories;
using TheCollective.InRiver.InboundConnector.Infor.Services;

namespace TheCollective.InRiver.Tests
{
    [TestFixture]
    public class InforTests
    {
        [SetUp]
        public void Setup()
        {
            const string username = "scriptuser";
            const string password = "Pj0vMOiFvgwnSmCuOEC8";
            const string pimUrl = "http://inriver.ming.local.absolunet.com:9790";

            var ticket = RemoteManager.Authenticate(pimUrl, username, password);
            RemoteManager.CreateInstance(pimUrl, ticket);

            _coreService = new CoreService(new DataRepository(), new ModelRepository(), new ChannelRepository(), new UtilityRepository(), new ReportRepository(), new UserRepository());
            _catalogRepository = new CatalogRepository();
            _cvlService = new CvlService(_coreService);

            _connectionStringSetting = new ConnectionStringSetting("Progress OpenEdge 10.2B Driver", "dc-sxe-server", "7300", "test60nxt", "sysprogress", "123");

            _importService = new ImportService(_coreService, _catalogRepository, _cvlService) { ConnectionStringSetting = _connectionStringSetting };
        }

        private ICatalogRepository _catalogRepository;
        private IImportService _importService;
        private ICoreService _coreService;
        private ICvlService _cvlService;
        private ConnectionStringSetting _connectionStringSetting;

        [Test]
        public void ImportItem()
        {
            var importParameters = new ImportParameters()
            {
                //ProductCategoryDescriptionSastaCodeIden = "c",
                //MajorDescriptionSastaCodeIden = "91",
                GhostWarehouse = "MALD",
                InforCompanyNumber = "1"
            };

            var productModel = _catalogRepository.GetProducts(_connectionStringSetting, importParameters, "--HH--57AC-078");
            //_importService.CreateOrUpdateItem(productModel.First(), importParameters, new ImportResult(), "InforConnector");
        }

        [Test]
        public void ImportProduct()
        {
            var importParameters = new ImportParameters()
            {
                ProductCategoryDescriptionSastaCodeIden = "c",
                MajorDescriptionSastaCodeIden = "91",
                InforCompanyNumber = "1",
                GhostWarehouse = "MALD"
            };

            _importService.ImportProducts("InforConnector", importParameters);
        }
    }
}
