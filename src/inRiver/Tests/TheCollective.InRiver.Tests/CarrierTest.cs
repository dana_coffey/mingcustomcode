﻿using System.Collections.Generic;
using Absolunet.InRiver.Core.Repositories;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting;
using NUnit.Framework;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.InboundConnector.Carrier.Services;
using TheCollective.InRiver.OutboundConnector.Carrier.Models;
using TheCollective.InRiver.OutboundConnector.Carrier.Repositories;
using TheCollective.InRiver.OutboundConnector.Carrier.Services;

namespace TheCollective.InRiver.Tests
{
    [TestFixture]
    public class CarrierTest
    {
        [SetUp]
        public void Setup()
        {
            const string username = "scriptuser";
            const string password = "Pj0vMOiFvgwnSmCuOEC8";
            const string pimUrl = "http://inriver.ming.local.absolunet.com:9790";

            var ticket = RemoteManager.Authenticate(pimUrl, username, password);
            RemoteManager.CreateInstance(pimUrl, ticket);

            _coreService = new CoreService(new DataRepository(), new ModelRepository(), new ChannelRepository(), new UtilityRepository(), new ReportRepository(), new UserRepository());
            _carrierClient = new CarrierClient();
            _carrierEpicClient = new CarrierEpicClient(_carrierClient);
            _carrierApiService = new CarrierApiService(_carrierClient);
            _carrierEpicApiService = new CarrierEpicApiService(_carrierEpicClient);
            _cvlService = new CvlService(_coreService);
            _inRiverRepository = new InRiverRepository(_coreService, _cvlService);
            _carrierSupersededService = new CarrierSupersededService(_coreService);

            _carrierImportService = new CarrierImportService(_coreService, _carrierApiService, _carrierEpicApiService, _inRiverRepository, _carrierSupersededService);

            _partsImportService = new PartsImportService(_coreService, _carrierEpicApiService, _carrierSupersededService);

            _connectorSettings = new CarrierConnectorSetting(Id, _coreService.UtilityRepository);

            _carrierImportService.ApiCredentials = new CarrierApiCredentials
            {
                ApiPassword = _connectorSettings.CarrierApiPassword,
                BaseApiUrl = _connectorSettings.CarrierApiBaseUrl,
                BearerTokenPassword = _connectorSettings.CarrierApiBearerTokenPassword,
                BearerTokenUsername = _connectorSettings.CarrierApiBearerTokenUsername,
                Username = _connectorSettings.CarrierApiUsername
            };

            _apiCredentials = _carrierImportService.ApiCredentials;
        }

        private const string Id = "CarrierConnector";

        private ICoreService _coreService;
        private ICarrierApiService _carrierApiService;
        private ICarrierEpicApiService _carrierEpicApiService;
        private ICarrierClient _carrierClient;
        private ICarrierEpicClient _carrierEpicClient;
        private ICarrierImportService _carrierImportService;
        private IInRiverRepository _inRiverRepository;
        private ICvlService _cvlService;
        private IPartsImportService _partsImportService;
        private ICarrierSupersededService _carrierSupersededService;
        private CarrierConnectorSetting _connectorSettings;
        private CarrierApiCredentials _apiCredentials;

        [Test]
        public void EnrichEquipmentEntity()
        {
            const int entityId = 200330;
            var settings = new CarrierConnectorSetting(Id, _coreService.UtilityRepository);

            _carrierImportService.EnrichEquipmentEntity(entityId, Id, settings.CarrierVendorNumberIds, settings.CarrierCategoryFieldSetJsonMapper);
        }

        [Test]
        public void EnrichPartsEntity()
        {
            var entityId = 262599;
            var sxePartsVendorIds = new List<string> { "1" };
            var settings = new CarrierConnectorSetting(Id, _coreService.UtilityRepository);
            _carrierImportService.EnrichPartsEntity(entityId, Id, sxePartsVendorIds);
        }
    }
}
