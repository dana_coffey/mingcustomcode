﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using FluentAssertions;
using NUnit.Framework;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;

namespace TheCollective.CarrierApi.Integration.Tests.Services
{
    [TestFixture]
    public class CarrierApiServiceTest
    {
        private CarrierApiService _carrierApiService;

        [SetUp]
        public void SetUp()
        {
            var carrierClient = new CarrierClient { ApiCredentials = GetDefaultApiCredentials() };
            _carrierApiService = new CarrierApiService(carrierClient);
        }

        [Test]
        public void GetDocuments_ProductThat_AreValid()
        {
            // act
            var result = _carrierApiService.GetDocuments("59MN7A080V21--20");

            // assert
            result.Should().NotBeNull();
        }

        [Test]
        public void GetProduct_ProductThat_IsNotValid()
        {
            // act
            var result = _carrierApiService.GetProduct("invalid_sku");

            // assert
            result.Should().BeNull();
        }

        [Test]
        public void GetProduct_ProductThat_IsValid()
        {
            // act
            var result = _carrierApiService.GetProduct("59MN7A120V24--22");

            // assert
            result.Should().NotBeNull();
        }

        [Test]
        public void GetProductAttributes_ProductThat_90()
        {
            // act
            var result = _carrierApiService.GetProduct("59SP5A026E14--10").Attributes.SelectMany(a => a.Value)
                .Where(a => a.Type != null && !a.Type.ValueType.IsNullOrEmpty() && a.Type.ValueType == "Range")
                .ToDictionary(a => a.Type.Urn, at => at.Values);

            // assert
            result.Should().NotBeNull();
        }

        [Test]
        public void GetProductAttributes_ProductThat_AirConditioners()
        {
            // act
            var result = _carrierApiService.GetProduct("24AAA618A003").Attributes.SelectMany(a => a.Value)
                .Where(a => a.Type != null && !a.Type.ValueType.IsNullOrEmpty() && a.Type.ValueType == "Range")
                .ToDictionary(a => a.Type.Urn, at => at.Values);

            // assert
            result.Should().NotBeNull();
        }

        [Test]
        public void GetProductAttributes_ProductThat_Evaporators()
        {
            // act
            var result = _carrierApiService.GetProduct("CAPMP1814ACA").Attributes.SelectMany(a => a.Value)
                .Where(a => a.Type != null && !a.Type.ValueType.IsNullOrEmpty() && a.Type.ValueType == "Range")
                .ToDictionary(a => a.Type.Urn, at => at.Values);

            // assert
            result.Should().NotBeNull();
        }

        [Test]
        public void GetProductAttributesRangeAndNotRange_ProductThat_AirConditioners()
        {
            // act
            var resultRange = _carrierApiService.GetProduct("24AAA618A003").Attributes.SelectMany(a => a.Value)
                .Where(a => a.Type != null && !a.Type.ValueType.IsNullOrEmpty() && a.Type.ValueType == "Range")
                .ToDictionary(a => a.Type.Urn, at => at.Values);

            var resultNotRange = _carrierApiService.GetProduct("24AAA618A003").Attributes.SelectMany(a => a.Value)
                .Where(a => a.Type != null && !a.Type.ValueType.IsNullOrEmpty() && a.Type.ValueType != "Range")
                .ToDictionary(a => a.Type.Urn, at => new List<string> { at.Value });

            var result = resultRange.Concat(resultNotRange);

            // assert
            result.Should().NotBeNull();
        }

        private static CarrierApiCredentials GetDefaultApiCredentials()
        {
            return new CarrierApiCredentials
            {
                ApiPassword = "zH7hmnQ#ZaqCTe3",
                BaseApiUrl = "https://services.ccs.utc.com",
                Username = "ming-10010",
                BearerTokenPassword = "f057c1ad-6e2e-4c66-849a-86347beed228"
            };
        }
    }
}
