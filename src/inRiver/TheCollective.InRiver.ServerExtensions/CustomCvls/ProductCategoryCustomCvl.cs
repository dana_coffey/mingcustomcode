﻿using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Models.Entities;
using Absolunet.InRiver.Core.Repositories;
using inRiver.Remoting.Objects;
using inRiver.Server.Extension;
using TheCollective.InRiver.Core.CVL;

namespace TheCollective.InRiver.ServerExtensions.CustomCvls
{
    public class ProductCategoryCustomCvl : ICustomValueList
    {
        private readonly IExtensionServiceRepository _extensionServiceRepository;

        public ProductCategoryCustomCvl()
        {
            _extensionServiceRepository = new ExtensionServiceRepository();
        }

        public List<CVLValue> GetAllCVLValues()
        {
            return _extensionServiceRepository.GetFieldSetsForEntityType(nameof(Item)).Select(ToCvlValue).ToList();
        }

        public CVLValue GetCVLValueByKey(string key)
        {
            return ToCvlValue(_extensionServiceRepository.GetFieldSetsForEntityType(nameof(Item)).FirstOrDefault(x => x.Id == key));
        }

        public string GetId()
        {
            return nameof(ProductCategory);
        }

        private static CVLValue ToCvlValue(FieldSet fieldSet)
        {
            return new CVLValue
            {
                Key = fieldSet?.Id,
                Value = fieldSet?.Name
            };
        }
    }
}
