﻿using System.Collections.Generic;
using Absolunet.InRiver.CompletenessRule.Core;
using Absolunet.InRiver.CompletenessRule.FieldUnique;
using Absolunet.InRiver.CompletenessRule.RegexValidation;
using Absolunet.InRiver.Core.Repositories;
using inRiver.Remoting.Dto;
using inRiver.Remoting.Objects;
using inRiver.Server.Extension;

namespace TheCollective.InRiver.ServerExtensions
{
    public class UniqueTitleCompletenessRule : BaseCompletenessCriteria, ICompletenessCriteria
    {
        private const string FieldTypeId = "FieldTypeId";
        private const string FieldTypeTitleId = "ProductName";
        private const string FieldTypeTitleOverrideId = "ProductNameOverride";
        private const string RegExp = "RegExp";

        public string Name => "Title with unique value and validate with Regex";

        public List<string> SettingsKeys => new List<string> { RegExp };

        public UniqueTitleCompletenessRule()
        {
        }

        public UniqueTitleCompletenessRule(IExtensionServiceRepository repository) : base(repository)
        {
        }

        public int GetCriteriaCompletenessPercentage(int entityId, List<CompletenessRuleSetting> settings)
        {
            var fieldUnique = new FieldUnique(Repository);
            var regexValidation = new RegexValidation(Repository);

            var field = GetField(entityId);
            if (field == null)
            {
                return 100;
            }

            settings.Add(new CompletenessRuleSetting
            {
                Key = FieldTypeId,
                Value = field.FieldTypeId
            });

            var result = regexValidation.GetCriteriaCompletenessPercentage(entityId, settings);
            result += fieldUnique.GetCriteriaCompletenessPercentage(entityId, settings);

            return result / 2;
        }

        private DtoField GetField(int entityId)
        {
            var entity = Repository.GetEntity(entityId, LoadLevel.DataOnly);

            if (entity == null)
            {
                return null;
            }

            // Get Title Override
            var field = Repository.GetField(entity.Id, FieldTypeTitleOverrideId);

            // Validate title Override value.
            if (!string.IsNullOrEmpty(field?.Data))
            {
                return field;
            }

            // Get Title to validate
            field = Repository.GetField(entity.Id, FieldTypeTitleId);

            return field;
        }
    }
}
