﻿using System;
using System.Configuration;
using System.Linq;
using Absolunet.InRiver.Business.Services;
using Absolunet.InRiver.Core.Models.Entities;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Repositories;
using Absolunet.InRiver.Core.Services;
using Absolunet.InRiver.Data.Repositories;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using inRiver.Remoting;
using inRiver.Remoting.Objects;
using inRiver.Remoting.Query;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;
using TheCollective.InRiver.Core.Helpers;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.InboundConnector.Infor.Services;
using TheCollective.InRiver.OutboundConnector.Carrier.Services;
using TCRE = TheCollective.InRiver.Core.Entities;

namespace TheCollective.Routine
{
    public class Program
    {
        private static string password;
        private static string pimUrl;
        private static string username;
        private static int? ProductsLimitToQueue
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings.Get("ProductsLimitToQueue"));
                }
                catch
                {
                    return null;
                }
            }
        }

        public static void Main(string[] args)
        {
            Setup();

            var container = new WindsorContainer(new XmlInterpreter());
            var carrierApiService = container.Resolve<ICarrierApiService>();
            var carrierImportService = container.Resolve<ICarrierImportService>();
            var cvlService = container.Resolve<ICvlService>();

            carrierApiService.ApiCredentials = new CarrierApiCredentials
            {
                ApiPassword = "zH7hmnQ#ZaqCTe3",
                BaseApiUrl = "https://services.ccs.utc.com",
                BearerTokenPassword = "f057c1ad-6e2e-4c66-849a-86347beed228",
                Username = "ming-10010"
            };

            var userRepository = new UserRepository();
            var dataRepository = new DataRepository(username, password, pimUrl);
            var modelRepository = new ModelRepository(username, password, pimUrl);
            var channelRepository = new ChannelRepository();
            var reportRepository = new ReportRepository();
            var utilityRepository = new UtilityRepository();
            var operationRepository = container.Resolve<IOperationRepository>();

            var coreService = new CoreService(dataRepository, modelRepository, channelRepository, utilityRepository, reportRepository, userRepository);
            var queueOperationService = new QueueOperationService(coreService, operationRepository);

            var executer = new Executer(coreService, carrierApiService, carrierImportService, cvlService, queueOperationService);

            Console.WriteLine("Choose a task to execute");
            Console.WriteLine("1: Clean product and item entities");
            Console.WriteLine("2: Add random products to random categories in Insite Channel");
            Console.WriteLine("3: Remove items with empty Urn");
            Console.WriteLine("4: Get similar products query string");
            Console.WriteLine("5: Update Products name and description");
            Console.WriteLine("6: Update all items from Carrier API");
            Console.WriteLine("7: ChangeCvlYesNoToBool");
            Console.WriteLine("8: ImportAllRelationsFromSxe");
            Console.WriteLine("9: Create CVL from files");
            Console.WriteLine("10: Delete CVL from files");
            Console.WriteLine("11: ImportAllCarrierDocuments");
            Console.WriteLine("12: Rebuild product titles");
            Console.WriteLine($"13: Requeue {ProductsLimitToQueue} products");

            var readedLine = Console.ReadLine();

            switch (readedLine)
            {
                case "1":
                    CleanUpEntities();
                    break;
                case "2":
                    AddProductToChannel();
                    break;
                case "3":
                    CleanupEmptyUrnItems();
                    break;
                case "4":
                    executer.CleanInRiverAndGetQueryStringFromProductCategories();
                    break;
                case "5":
                    executer.PopulateCarrierProductWithName();
                    break;
                case "6":
                    executer.UpdateAllItemsFromCarrier();
                    break;
                case "7":
                    executer.ChangeCvlYesNoToBool();
                    break;
                case "8":
                    ImportAllRelationsFromSxe();
                    break;
                case "9":
                    executer.CreerCvlFromDirectoryWithCvsFiles();
                    break;
                case "10":
                    executer.DeleteCvlFromDirectoryWithCvsFiles();
                    break;
                case "11":
                    executer.ImportAllRelatedCarrierDocuments();
                    break;
                case "12":
                    executer.RebuildAllTitles();
                    break;
                case "13":
                    executer.RequeueProducts(1, ProductsLimitToQueue);
                    break;
            }

            Console.Write("Press any key to exit");
            var exit = Console.ReadLine();
        }

        public static void Setup()
        {
            username = "scriptuser";
            password = "Pj0vMOiFvgwnSmCuOEC8";
            pimUrl = ConfigurationManager.AppSettings["ServerUrl"];
            Console.WriteLine("Connecting to " + pimUrl);

            var ticket = RemoteManager.Authenticate(pimUrl, username, password);
            RemoteManager.CreateInstance(pimUrl, ticket);
        }

        private static void AddProductToChannel()
        {
            var channelNodeEntities = RemoteManager.ChannelService.GetEntitiesForChannelAndEntityType(1, nameof(ChannelNode));

            var productEntities = RemoteManager.DataService.GetAllEntitiesForEntityType(nameof(Product), LoadLevel.Shallow).ToList();

            var rnd = new Random();
            var channelNodeProductLinkType = RemoteManager.ModelService.GetLinkType(nameof(ChannelNodeProduct));

            foreach (var productEntity in productEntities)
            {
                var channelNodeIndex = rnd.Next(channelNodeEntities.Count);
                var channelNodeEntity = channelNodeEntities[channelNodeIndex];

                if (!RemoteManager.DataService.LinkAlreadyExists(channelNodeEntity.Id, productEntity.Id, null, nameof(ChannelNodeProduct)))
                {
                    RemoteManager.DataService.AddLink(new Link { Source = channelNodeEntity, Target = productEntity, LinkType = channelNodeProductLinkType });

                    Console.WriteLine($"Link added between Category {channelNodeEntity.Id} and product {productEntity.Id}");
                }
            }
        }

        private static void CleanupEmptyUrnItems()
        {
            var userRepository = new UserRepository();
            var dataRepository = new DataRepository(username, password, pimUrl);
            var modelRepository = new ModelRepository(username, password, pimUrl);
            var channelRepository = new ChannelRepository();
            var reportRepository = new ReportRepository();
            var utilityRepository = new UtilityRepository();

            var coreService = new CoreService(dataRepository, modelRepository, channelRepository, utilityRepository, reportRepository, userRepository);

            var entities = RemoteManager.DataService.Search(new Criteria() { FieldTypeId = nameof(TCRE.Item.ItemUrn), Operator = Operator.Empty }, LoadLevel.Shallow);

            foreach (var entity in entities)
            {
                coreService.DeleteEntityAndLinks(entity);
            }

            Console.WriteLine("finished cleanup");
        }

        private static void CleanUpEntities()
        {
            var userRepository = new UserRepository();
            var dataRepository = new DataRepository(username, password, pimUrl);
            var modelRepository = new ModelRepository(username, password, pimUrl);
            var channelRepository = new ChannelRepository();
            var reportRepository = new ReportRepository();
            var utilityRepository = new UtilityRepository();

            var coreService = new CoreService(dataRepository, modelRepository, channelRepository, utilityRepository, reportRepository, userRepository);

            Console.WriteLine("Starting product cleanup");

            var productsEntities = dataRepository.GetAllEntitiesForEntityType(nameof(Product), LoadLevel.Shallow);

            productsEntities = productsEntities.Where(
                    e =>
                    {
                        var displayname = e.DisplayName.Data.ToString();
                        var product1 = "24ACC618A003".ToLocaleString().ToString();
                        var product2 = "24ACC624A003".ToLocaleString().ToString();

                        return displayname != product1 && displayname != product2;
                    })
                .ToList();

            foreach (var entity in productsEntities)
            {
                coreService.DeleteEntityAndLinks(entity);
            }

            Console.WriteLine("Finished product cleanup");
            Console.WriteLine("Starting item cleanup");

            var itemEntities = dataRepository.GetAllEntitiesForEntityType(nameof(Item), LoadLevel.Shallow);

            itemEntities = itemEntities.Where(
                    e =>
                    {
                        var displayname = e.DisplayName.Data.ToString();
                        var product1 = "24ACC618A003";
                        var product2 = "24ACC624A003";

                        return displayname != product1 && displayname != product2;
                    })
                .ToList();

            foreach (var entity in itemEntities)
            {
                coreService.DeleteEntityAndLinks(entity);
            }

            Console.WriteLine("Finished product cleanup");

            Console.WriteLine("Finished cleanup");
        }

        private static void ImportAllRelationsFromSxe()
        {
            var dataRepository = new DataRepository(username, password, pimUrl);
            var modelRepository = new ModelRepository();
            var utilityRepository = new UtilityRepository();

            var sxeRelationService = new SxeRelationsService(dataRepository, utilityRepository, modelRepository, "InforConnector");
            var sxeRelationsImportResultFilePath = ConfigurationManager.AppSettings["SxeRelationsImportResultFilePath"];

            sxeRelationService.ImportAllRelations(sxeRelationsImportResultFilePath, Console.WriteLine);
        }
    }
}
