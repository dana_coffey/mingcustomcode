﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Absolunet.InRiver.Business.Definitions;
using Absolunet.InRiver.Business.Models;
using Absolunet.InRiver.Business.Services;
using Absolunet.InRiver.Core.Services;
using Castle.Core.Internal;
using inRiver.Remoting;
using inRiver.Remoting.Objects;
using inRiver.Remoting.Query;
using Newtonsoft.Json;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;
using TheCollective.InRiver.Core.Entities;
using TheCollective.InRiver.Core.Helpers;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.InboundConnector.Infor.Helpers;
using TheCollective.InRiver.OutboundConnector.Carrier.Helpers;
using TheCollective.InRiver.OutboundConnector.Carrier.Models;
using TheCollective.InRiver.OutboundConnector.Carrier.Services;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Services;

namespace TheCollective.Routine
{
    public class Executer
    {
        private readonly ICarrierApiService _carrierApiService;
        private readonly ICarrierImportService _carrierImportService;
        private readonly ICoreService _coreService;
        private readonly ICvlService _cvlService;
        private readonly IQueueOperationService _operationService;

        public Executer(ICoreService coreService, ICarrierApiService carrierApiService, ICarrierImportService carrierImportService, ICvlService cvlService, IQueueOperationService queueOperationService)
        {
            _coreService = coreService;
            _carrierApiService = carrierApiService;
            _carrierImportService = carrierImportService;
            _cvlService = cvlService;
            _operationService = queueOperationService;
        }

        public void ChangeCvlYesNoToBool()
        {
            var yesCvlKeys = new List<string> { "93CBA07454F06A4A960172BBD6E2A435", "A6105C0A611B41B08F1209506350279E", "7469A286259799E5B37E5DB9296F00B3" };

            var fieldSets = _coreService.ModelRepository.GetAllFieldSets();
            var originalFieldTypes = _coreService.ModelRepository.GetAllFieldTypes().Where(x => x.CVLId == "YesNo").ToList();

            foreach (var originalFieldType in originalFieldTypes)
            {
                var newFieldType = CopyFieldType(originalFieldType, originalFieldType.Id.Insert(4, "Has"));
                newFieldType.DataType = DataType.Boolean;
                newFieldType.CVLId = null;
                newFieldType.DefaultValue = "false";

                _coreService.ModelRepository.AddFieldType(newFieldType);

                var fieldTypeFieldSets = fieldSets.Where(x => x.FieldTypes.Any(y => y == originalFieldType.Id)).ToList();

                foreach (var fieldTypeFieldSet in fieldTypeFieldSets)
                {
                    _coreService.ModelRepository.AddFieldTypeToFieldSet(fieldTypeFieldSet.Id, newFieldType.Id);
                }
            }

            var itemEntities = _coreService.DataRepository.GetAllEntitiesForEntityType(nameof(Item), LoadLevel.DataOnly);

            foreach (var itemEntity in itemEntities)
            {
                foreach (var originalFieldType in originalFieldTypes)
                {
                    var fieldValue = _coreService.GetFieldValue(itemEntity, originalFieldType.Id)?.ToString();
                    var boolValue = yesCvlKeys.Any(x => x == fieldValue);

                    _coreService.SetFieldValue(itemEntity, originalFieldType.Id.Insert(4, "Has"), boolValue);
                }

                _coreService.DataRepository.UpdateEntity(itemEntity);
            }

            foreach (var originalFieldType in originalFieldTypes)
            {
                _coreService.ModelRepository.DeleteFieldType(originalFieldType.Id);
            }

            _coreService.ModelRepository.DeleteCVL("YesNo");
        }

        public void CleanInRiverAndGetQueryStringFromProductCategories()
        {
            var productModels = new List<ProductModel>
            {
                _carrierApiService.GetProduct("24VNA913A003"),
                _carrierApiService.GetProduct("CNPHP2417ALA"),
                _carrierApiService.GetProduct("59SP5A040E14--10"),

                // evaporator coils
                _carrierApiService.GetProduct("CNPVP1814ACA"),
                _carrierApiService.GetProduct("CSPHP2412ACA"),
                _carrierApiService.GetProduct("CNPVU1814ACA"),
                _carrierApiService.GetProduct("CAPMP1814ACA"),
                _carrierApiService.GetProduct("CNPFU2418ACA"),

                // Air conditioners
                _carrierApiService.GetProduct("24AAA518A003"),
                _carrierApiService.GetProduct("24AAA618A003"),
                _carrierApiService.GetProduct("24ABB318A003"),
                _carrierApiService.GetProduct("24ABB318C003"),
                _carrierApiService.GetProduct("24ABC618A003"),
                _carrierApiService.GetProduct("24ACA418C003"),
                _carrierApiService.GetProduct("24ACB318A003"),
                _carrierApiService.GetProduct("24ACB724A003"),
                _carrierApiService.GetProduct("24ACC418A003"),
                _carrierApiService.GetProduct("24ACC618A003"),
                _carrierApiService.GetProduct("24AHA418A003"),
                _carrierApiService.GetProduct("24ANB124A003"),
                _carrierApiService.GetProduct("24ANB618A003"),
                _carrierApiService.GetProduct("24ANB724A003"),
                _carrierApiService.GetProduct("24ANB724C003"),
                _carrierApiService.GetProduct("24VNA913A003"),
                _carrierApiService.GetProduct("PA13NA018000"),
                _carrierApiService.GetProduct("PA14NC01800G"),
                _carrierApiService.GetProduct("PA15NC018000"),
                _carrierApiService.GetProduct("PA16NA01800G"),
                _carrierApiService.GetProduct("PA17NA02400G"),
            };

            var allProductsFromFamilies = new List<ProductModel>();
            var inRiverEntities = new List<Entity>();

            foreach (var productModel in productModels)
            {
                allProductsFromFamilies.AddRange(productModel.FamilyProductOf.FamilyProducts);
                inRiverEntities.AddRange(RemoteManager.DataService.Search(new Criteria() { FieldTypeId = nameof(InRiver.Core.Entities.Product.ProductFamilyProductsUrn), Operator = Operator.Equal, Value = productModel.FamilyProductOf.Urn }, LoadLevel.Shallow));
            }

            ////foreach (var product in allProductsFromFamilies)
            ////{
            ////    inRiverEntities.AddRange(RemoteManager.DataService.Search(new Criteria() { FieldTypeId = nameof(InRiver.Core.Entities.Item.ItemUrn), Operator = Operator.Equal, Value = product.Urn }, LoadLevel.Shallow).ToList());
            ////}

            ////foreach (var entity in inRiverEntities)
            ////{
            ////    _coreService.DeleteEntityAndLinks(entity);
            ////}

            var inQuery = QueryHelper.ToSqlInFormat(allProductsFromFamilies.Select(fp => fp.Urn).ToArray());

            Console.WriteLine(inQuery);
            Console.ReadLine();
        }

        public void CreerCvlFromDirectoryWithCvsFiles()
        {
            var di = new DirectoryInfo(@"C:\temp\files");
            var files = di.GetFiles();
            foreach (var fi in files)
            {
                if (fi.Extension != ".csv")
                {
                    continue;
                }

                var posIni = fi.Name.LastIndexOf("-") + 1;
                var posFin = fi.Name.LastIndexOf(".");
                var cvlName = fi.Name.Substring(posIni, posFin - posIni).Trim();

                try
                {
                    using (var reader = new StreamReader($@"C:\temp\files\{fi.Name}"))
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        var dataType = values[3];
                        while (!reader.EndOfStream)
                        {
                            line = reader.ReadLine();
                            values = line.Split(',');
                            var value = values[3];
                            var cvlKey = CarrierHelpers.CalculateMd5Hash(value);

                            switch (dataType)
                            {
                                case "Value":
                                    _cvlService.CreateCvlAndValueWhenMissing(cvlName, cvlKey, value, DataType.String);
                                    break;
                                case "en":
                                    _cvlService.CreateCvlAndValueWhenMissing(cvlName, cvlKey, value, DataType.LocaleString);
                                    break;
                            }
                        }
                    }

                    Console.WriteLine($@"Creation finished: {cvlName}");
                }
                catch (Exception)
                {
                    Console.WriteLine($@"Creation failed: {cvlName}");
                }
            }

            Console.ReadKey();
        }

        public void DeleteCvlFromDirectoryWithCvsFiles()
        {
            var di = new DirectoryInfo(@"C:\temp\files");
            var files = di.GetFiles();
            foreach (var fi in files)
            {
                try
                {
                    if (fi.Extension != ".csv")
                    {
                        continue;
                    }

                    var posIni = fi.Name.LastIndexOf("-") + 1;
                    var posFin = fi.Name.LastIndexOf(".");
                    var cvlName = fi.Name.Substring(posIni, posFin - posIni).Trim();

                    if (_coreService.ModelRepository.GetCVL(cvlName) == null)
                    {
                        Console.WriteLine($@"CVL {cvlName} doesnot exist");
                        continue;
                    }

                    _coreService.ModelRepository.DeleteCVL(cvlName);

                    Console.WriteLine($@"Deleted finished: {fi.Name}");
                }
                catch (Exception)
                {
                    Console.WriteLine($@"Deleted failed: {fi.Name}");
                }
            }

            Console.ReadKey();
        }

        public void ImportAllRelatedCarrierDocuments()
        {
            var inRiverEntities = new List<Entity>();

            inRiverEntities.AddRange(RemoteManager.DataService.Search(new Criteria() { FieldTypeId = nameof(InRiver.Core.Entities.Item.ItemERPVendorNo), Operator = Operator.Equal, Value = 4 }, LoadLevel.DataAndLinks));

            foreach (var itemEntity in inRiverEntities)
            {
                var sku = _coreService.GetFieldValue(itemEntity, nameof(Item.ItemSku));
                if (sku == null)
                {
                    continue;
                }

                var carrierProduct = _carrierApiService.GetProduct(sku.ToString());
                if (carrierProduct != null && !carrierProduct.Urn.IsNullOrEmpty() &&
                    carrierProduct.FamilyProductOf != null && !carrierProduct.FamilyProductOf.Urn.IsNullOrEmpty())
                {
                    _carrierImportService.ManageProductDocuments("1", itemEntity, carrierProduct.Urn,
                        carrierProduct.FamilyProductOf.Urn);
                }
            }
        }

        public void PopulateCarrierProductWithName()
        {
            ////var productModels = new List<ProductModel>();

            ////productModels.Add(_carrierApiService.GetProduct("24VNA913A003"));
            ////productModels.Add(_carrierApiService.GetProduct("CNPHP2417ALA"));
            ////productModels.Add(_carrierApiService.GetProduct("59SP5A040E14--10"));

            // evaporator coils
            ////productModels.Add(_carrierApiService.GetProduct("CNPVP1814ACA"));
            ////productModels.Add(_carrierApiService.GetProduct("CSPHP2412ACA"));
            ////productModels.Add(_carrierApiService.GetProduct("CNPVU1814ACA"));
            ////productModels.Add(_carrierApiService.GetProduct("CAPMP1814ACA"));
            ////productModels.Add(_carrierApiService.GetProduct("CNPFU2418ACA"));

            // Air conditioners
            ////productModels.Add(_carrierApiService.GetProduct("24AAA518A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24AAA618A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ABB318A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ABB318C003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ABC618A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ACA418C003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ACB318A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ACB724A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ACC418A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ACC618A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24AHA418A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ANB124A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ANB618A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ANB724A003"));
            ////productModels.Add(_carrierApiService.GetProduct("24ANB724C003"));
            ////productModels.Add(_carrierApiService.GetProduct("24VNA913A003"));
            ////productModels.Add(_carrierApiService.GetProduct("PA13NA018000"));
            ////productModels.Add(_carrierApiService.GetProduct("PA14NC01800G"));
            ////productModels.Add(_carrierApiService.GetProduct("PA15NC018000"));
            ////productModels.Add(_carrierApiService.GetProduct("PA16NA01800G"));
            ////productModels.Add(_carrierApiService.GetProduct("PA17NA02400G"));

            var productEntities = _coreService.DataRepository.GetAllEntitiesForEntityType(nameof(Product), LoadLevel.DataOnly);

            foreach (var entity in productEntities)
            {
                ////var productEntity = _coreService.DataRepository.GetEntityByUniqueValue(nameof(Product.ProductFamilyProductsUrn), productModel.FamilyProductOf.Urn, LoadLevel.DataOnly);
                var productEntity = entity;

                var productName = WebUtility.HtmlDecode(_coreService.GetFieldValue(productEntity, nameof(Product.ProductCarrierName)).ToString());

                _coreService.SetFieldValue(productEntity, nameof(Product.ProductCarrierName), productName + "test");
                _coreService.SetFieldValue(productEntity, nameof(Product.ProductName), productName.ToLocaleString());
                _coreService.SetFieldValue(productEntity, nameof(Product.ProductDescription), productName.ToLocaleString());

                productEntity = _coreService.DataRepository.UpdateEntity(productEntity);

                _coreService.SetFieldValue(productEntity, nameof(Product.ProductCarrierName), productName);
                _coreService.DataRepository.UpdateEntity(productEntity);
            }
        }

        public void RebuildAllTitles()
        {
            var connectorSettings = _coreService.UtilityRepository.GetConnectorSettings("TitleBuilderConnector");

            var titleBuilderService = new TitleBuilderService(_coreService);

            var json = connectorSettings["TitleRecipes"];

            var allRecipes = JsonConvert.DeserializeObject<TitleRecipesSetting>(json)?.TitleRecipes;

            var allItems = RemoteManager.DataService.GetAllEntitiesForEntityType(nameof(Item), LoadLevel.Shallow).ToList();

            foreach (var item in allItems)
            {
                titleBuilderService.BuildTitle("1", item, allRecipes);
            }
        }

        public void RequeueProducts(int channelId, int? limit)
        {
            Console.Clear();
            Console.WriteLine("Retrieving products...");

            var products = _coreService.ChannelRepository.GetEntitiesForChannelAndEntityType(channelId, nameof(Product));

            var nbAdded = 0;
            var totalProducts = products.Count();

            Console.WriteLine($"Found {totalProducts} products in channel, starting to queue");

            foreach (var product in products)
            {
                if (product != null)
                {
                    var productData = _coreService.DataRepository.GetEntity(product.Id, LoadLevel.DataOnly);
                    var productSku = _coreService.GetFieldValue(productData, nameof(Product.ProductSku)).ToString();

                    /*
                    var enriChment = _coreService.GetFieldValue(productData, "ProductEnrichmentStatus").ToString();

                    if (!enriChment.ToLower().Contains("final"))
                    {
                        nbSkipped++;
                        if (nbSkipped % 20 == 0)
                        {
                            Console.Write(".");
                        }

                        if (nbSkipped % 500 == 0)
                        {
                            Console.Write($"Skipped {nbSkipped} products");
                            Console.WriteLine(string.Empty);
                        }

                        continue;
                    }


                    if (nbSkipped > 0)
                    {
                        Console.Write($"Skipped {nbSkipped} products");
                        Console.WriteLine(string.Empty);
                        nbSkipped = 0;
                    }
                    */

                    var definition = new EntityDefinition
                    {
                        EntityId = product.Id,
                        ChannelId = channelId,
                        EntityTypeId = nameof(Product),
                        OperationEvent = OperationEvent.ChannelEntityUpdated,
                        ProductSku = productSku
                    };

                    _operationService.AddOrUpdateProduct(definition);

                    nbAdded++;
                    Console.WriteLine($"{nbAdded} / {limit}".PadLeft(20) + " - " + productSku);

                    if (limit.HasValue && limit.Value > 0 && nbAdded == limit.Value)
                    {
                        break;
                    }

                    if (nbAdded % 1000 == 0)
                    {
                        Console.Clear();
                    }
                }
            }

            Console.WriteLine($"Done queuing products, queued {nbAdded} products");
        }

        public void UpdateAllItemsFromCarrier()
        {
            var itemEntities = _coreService.ChannelRepository.GetEntitiesForChannelAndEntityType(1, nameof(Item));
            var connectorSettings = _coreService.UtilityRepository.GetConnectorSettings("CarrierConnector");

            var carrierVendorIds = connectorSettings[nameof(CarrierConnectorSetting.CarrierVendorNumberIds)].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var categoryFieldSetJsonMapper = JsonConvert.DeserializeObject<Dictionary<string, string>>(connectorSettings[nameof(CarrierConnectorSetting.CarrierCategoryFieldSetJsonMapper)]);

            foreach (var itemEntity in itemEntities)
            {
                _carrierImportService.EnrichEquipmentEntity(itemEntity.Id, "Script", carrierVendorIds, categoryFieldSetJsonMapper);
            }
        }

        private static FieldType CopyFieldType(FieldType originalFieldType, string newId)
        {
            var fieldType = new FieldType(newId, originalFieldType.EntityTypeId, originalFieldType.DataType, originalFieldType.CategoryId)
            {
                CVLId = originalFieldType.CVLId,
                DefaultValue = originalFieldType.DefaultValue,
                Description = originalFieldType.Description,
                ExcludeFromDefaultView = originalFieldType.ExcludeFromDefaultView,
                Hidden = originalFieldType.Hidden,
                Index = originalFieldType.Index,
                IsDisplayDescription = originalFieldType.IsDisplayDescription,
                IsDisplayName = originalFieldType.IsDisplayName,
                Mandatory = originalFieldType.Mandatory,
                Multivalue = originalFieldType.Multivalue,
                Name = originalFieldType.Name,
                ReadOnly = originalFieldType.ReadOnly,
                TrackChanges = originalFieldType.TrackChanges,
                Unique = originalFieldType.Unique,
                Units = originalFieldType.Units,
                Settings = originalFieldType.Settings
            };

            return fieldType;
        }
    }
}
