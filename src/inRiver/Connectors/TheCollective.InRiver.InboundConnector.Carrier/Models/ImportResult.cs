﻿using System.Collections.Generic;
using System.Linq;
using inRiver.Remoting.Objects;

namespace TheCollective.InRiver.InboundConnector.Carrier.Models
{
    public class ImportResult
    {
        public List<string> Errors { get; } = new List<string>();
        public bool IsSuccess => !Errors.Any();
        public int PartsCreatedCount { get; set; }
        public int PartsUpdatedCount { get; set; }
        public List<Entity> SupersededItemEntities { get; } = new List<Entity>();
    }
}
