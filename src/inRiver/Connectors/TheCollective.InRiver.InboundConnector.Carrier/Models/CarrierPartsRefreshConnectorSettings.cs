﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Connectors;

namespace TheCollective.InRiver.InboundConnector.Carrier.Models
{
    public class CarrierPartsRefreshConnectorSettings : WorkerConnectorSettings
    {
        public string CarrierApiBaseUrl => GetSetting(nameof(CarrierApiBaseUrl));
        public string CarrierApiBearerTokenPassword => GetSetting(nameof(CarrierApiBearerTokenPassword));
        public string CarrierApiBearerTokenUsername => GetSetting(nameof(CarrierApiBearerTokenUsername));
        public string CarrierApiPassword => GetSetting(nameof(CarrierApiPassword));
        public string CarrierApiUsername => GetSetting(nameof(CarrierApiUsername));
        public List<int> DaysRunImport => GetSetting(nameof(DaysRunImport)).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
        public List<string> SxePartsVendorIds => GetSetting(nameof(SxePartsVendorIds)).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();

        public CarrierPartsRefreshConnectorSettings(Func<string, string> getSetting) : base(getSetting)
        {
        }
    }
}
