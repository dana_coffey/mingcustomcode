﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;
using TheCollective.InRiver.Core.Services;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.InboundConnector.Carrier.Services
{
    public class PartsImportService : IPartsImportService
    {
        private readonly ICarrierEpicApiService _carrierEpicApiService;
        private readonly ICarrierSupersededService _carrierSupersededService;
        private readonly ICoreService _coreService;
        private int _linksRefreshError;

        private int _linksRefreshSuccess;

        public PartsImportService(ICoreService coreService, ICarrierEpicApiService carrierEpicApiService, ICarrierSupersededService carrierSupersededService)
        {
            _coreService = coreService;
            _carrierEpicApiService = carrierEpicApiService;
            _carrierSupersededService = carrierSupersededService;
        }

        public void EnrichSupersededParts(CarrierApiCredentials apiCredentials, string connectorId, List<string> sxePartsVendorIds, List<int> daysRunImport)
        {
            if (!daysRunImport.Any() || !daysRunImport.Contains(DateTime.Now.Day))
            {
                return;
            }

            _carrierEpicApiService.ApiCredentials = apiCredentials;

            var itemEntities = GetPartsEntities(connectorId, sxePartsVendorIds);
            var itemProcessed = 0;

            // Get Epic APi Parts.
            foreach (var itemEntity in itemEntities)
            {
                var partNumber = _coreService.GetFieldValue(itemEntity, nameof(TCEntities.Item.ItemSku))?.ToString();
                if (!string.IsNullOrEmpty(partNumber))
                {
                    try
                    {
                        var supersededParts = _carrierEpicApiService.GetSupersededParts(partNumber);

                        if (supersededParts != null && supersededParts.Any())
                        {
                            // Create links
                            var createSupersededLinkResult = _carrierSupersededService.CreateSupersededLinks(supersededParts, itemEntity);
                            _linksRefreshSuccess += createSupersededLinkResult.Success.Count;
                            _linksRefreshError += createSupersededLinkResult.Errors.Count;
                        }
                    }
                    catch (Exception ex)
                    {
                        _coreService.ReportRepository.WriteError(connectorId, $"An error has occurred while getting the superseded parts for the part number: '{partNumber}'. Exception: {ex}");
                    }
                }

                itemProcessed++;
                if (itemProcessed % Connectors.Constants.ConnectorSettings.BatchSizeForProcessLog == 0)
                {
                    _coreService.ReportRepository.Write(connectorId, $"'{itemProcessed}' of '{itemEntities.Count}' parts entities have been processed.");
                }
            }

            WriteLogSupersededParts(connectorId);
        }

        private List<Entity> GetPartsEntities(string connectorId, List<string> sxePartsVendorIds)
        {
            // Get PIM carrier parts
            var itemVendorFields = _coreService.DataRepository.GetAllFieldsByFieldType(nameof(TCEntities.Item.ItemERPVendorNo));
            itemVendorFields = itemVendorFields.Where(x => sxePartsVendorIds.Contains(x.Data?.ToString())).ToList();
            var itemEntityIds = itemVendorFields.Select(x => x.EntityId).ToList();

            var partEntities = new List<Entity>();

            var batchSize = 2500;
            var batchCount = Math.Ceiling((decimal)itemEntityIds.Count / (decimal)batchSize);

            for (var i = 0; i < batchCount; i++)
            {
                partEntities.AddRange(_coreService.DataRepository.GetEntities(itemEntityIds.Skip(i * batchSize).Take(batchSize).ToList(), LoadLevel.DataOnly).ToList());
                _coreService.ReportRepository.Write(connectorId, $"Fetching parts from inRiver... '{(i + 1) * batchCount}' of '{itemEntityIds.Count}' completed.");
            }

            if (partEntities.Any())
            {
                _coreService.ReportRepository.Write(connectorId, $"Updating superseded links for '{partEntities.Count}' parts entities has started.");
            }

            return partEntities;
        }

        private void WriteLogSupersededParts(string connectorId)
        {
            if (_linksRefreshSuccess > 0)
            {
                _coreService.ReportRepository.Write(connectorId, $"'{_linksRefreshSuccess}' superseded parts links have been created successfully and '{_linksRefreshError}' have had error.");
            }
            else if (_linksRefreshError > 0)
            {
                _coreService.ReportRepository.WriteError(connectorId, $"Unable to create '{_linksRefreshError}' superseded parts links because they were not existing in the PIM");
            }

            _coreService.ReportRepository.Write(connectorId, "Updating superseded links has finished.");
        }
    }
}
