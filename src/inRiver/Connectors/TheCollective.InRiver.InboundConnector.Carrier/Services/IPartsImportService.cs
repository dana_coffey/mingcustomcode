﻿using System.Collections.Generic;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.InRiver.InboundConnector.Carrier.Services
{
    public interface IPartsImportService
    {
        void EnrichSupersededParts(CarrierApiCredentials apiCredentials, string connectorId, List<string> sxePartsVendorIds, List<int> daysRunImport);
    }
}
