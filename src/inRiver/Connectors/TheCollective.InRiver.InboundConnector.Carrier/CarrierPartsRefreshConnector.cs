﻿using System;
using System.Collections.Generic;
using Absolunet.InRiver.Core.Connectors;
using Absolunet.InRiver.Core.Services;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using inRiver.Integration.Interface;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.InRiver.InboundConnector.Carrier.Models;
using TheCollective.InRiver.InboundConnector.Carrier.Services;

namespace TheCollective.InRiver.InboundConnector.Carrier
{
    public class CarrierPartsRefreshConnector : WorkerConnector, IInboundConnector
    {
        private readonly IPartsImportService _carrierImportService;
        private readonly ICoreService _coreService;

        private CarrierPartsRefreshConnectorSettings _connectorSettings;

        public CarrierPartsRefreshConnector()
        {
            var container = new WindsorContainer(new XmlInterpreter());
            _coreService = container.Resolve<ICoreService>();
            _carrierImportService = container.Resolve<IPartsImportService>();
        }

        public CarrierPartsRefreshConnector(WindsorContainer container) : this(container.Resolve<ICoreService>(), container.Resolve<IPartsImportService>())
        {
        }

        public CarrierPartsRefreshConnector(ICoreService coreService, IPartsImportService carrierImportService) : base(coreService.ReportRepository, coreService.UtilityRepository)
        {
            _coreService = coreService;
            _carrierImportService = carrierImportService;
        }

        public override void BeforeStart()
        {
            _connectorSettings = new CarrierPartsRefreshConnectorSettings(GetSetting);
        }

        public override void DequeueTask()
        {
            try
            {
                ReportRepository.Write(Id, "Parts refresh has started.");

                var apiCredentials = new CarrierApiCredentials
                {
                    ApiPassword = _connectorSettings.CarrierApiPassword,
                    BaseApiUrl = _connectorSettings.CarrierApiBaseUrl,
                    BearerTokenPassword = _connectorSettings.CarrierApiBearerTokenPassword,
                    BearerTokenUsername = _connectorSettings.CarrierApiBearerTokenUsername,
                    Username = _connectorSettings.CarrierApiUsername
                };

                _carrierImportService.EnrichSupersededParts(apiCredentials, Id, _connectorSettings.SxePartsVendorIds, _connectorSettings.DaysRunImport);
            }
            catch (Exception exception)
            {
                ReportRepository.WriteError(Id, exception.ToString());
            }
        }

        public override Dictionary<string, string> GetDefaultSettings()
        {
            var defaultSettings = base.GetDefaultSettings();

            defaultSettings.Add(nameof(_connectorSettings.CarrierApiBaseUrl), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.CarrierApiUsername), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.CarrierApiPassword), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.CarrierApiBearerTokenPassword), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.CarrierApiBearerTokenUsername), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.DaysRunImport), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.SxePartsVendorIds), string.Empty);

            return defaultSettings;
        }
    }
}
