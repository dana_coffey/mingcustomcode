﻿namespace TheCollective.InRiver.InboundConnector.Carrier
{
    public class Constants
    {
        public static class ServerSettings
        {
            public const string CarrierInboundConnectorLastExecutionDate = "CARRIER_INBOUNDCONNECTOR_LAST_EXEC_DATE";
        }
    }
}
