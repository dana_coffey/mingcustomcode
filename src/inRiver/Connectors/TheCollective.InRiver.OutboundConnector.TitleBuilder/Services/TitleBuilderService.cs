﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;
using RestSharp.Extensions;
using TheCollective.InRiver.Core.Helpers;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings;
using TCCore = TheCollective.InRiver.Core;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Services
{
    public class TitleBuilderService : ITitleBuilderService
    {
        private readonly ICoreService _coreService;

        public TitleBuilderService(ICoreService coreService)
        {
            _coreService = coreService;
        }

        public void BuildTitle(string connectorId, Entity itemEntity, List<TitleRecipe> recipes)
        {
            if (!string.IsNullOrEmpty(itemEntity.FieldSetId))
            {
                _coreService.ReportRepository.Write(connectorId, $"Searching title recipe for fieldset '{itemEntity.FieldSetId}'");
                var titleRecipe = recipes.FirstOrDefault(tr => tr.Fieldset == itemEntity.FieldSetId);
                if (titleRecipe != null)
                {
                    _coreService.ReportRepository.Write(connectorId, $"Found title recipe for fieldset '{titleRecipe.Fieldset}'");

                    itemEntity = _coreService.DataRepository.GetEntity(itemEntity.Id, LoadLevel.DataAndLinks);

                    var productItemLinkType = _coreService.ModelRepository.GetLinkType(nameof(ProductItem));
                    var productEntity = itemEntity.Links.FirstOrDefault(l => l.LinkType == productItemLinkType)?.Source;

                    if (productEntity != null)
                    {
                        productEntity = _coreService.DataRepository.GetEntity(productEntity.Id, LoadLevel.DataOnly);

                        EnrichProductTitle(connectorId, itemEntity, productEntity, titleRecipe);
                    }
                }
            }
        }

        private void EnrichProductTitle(string connectorId, Entity itemEntity, Entity productEntity, TitleRecipe recipe)
        {
            var titleBuilder = new StringBuilder();
            var displayTitle = string.Empty;

            try
            {
                foreach (var recipeStep in recipe.RecipeSteps)
                {
                    var targetEntity = recipeStep.Entity == nameof(TCEntities.Item) ? itemEntity : productEntity;
                    var titlePropertyValue = _coreService.GetFieldValue(targetEntity, recipeStep.Fieldtype)?.ToString();

                    if (!string.IsNullOrEmpty(titlePropertyValue))
                    {
                        var fieldTypeInfo = _coreService.ModelRepository.GetFieldType(recipeStep.Fieldtype);
                        if (fieldTypeInfo.CVLId.HasValue())
                        {
                            titlePropertyValue = GetCvlTitlePropertyValue(fieldTypeInfo, titlePropertyValue.Split(';').ToList());
                        }

                        if (recipeStep.Override != null)
                        {
                            if (recipeStep.Override.ContainsKey(titlePropertyValue))
                            {
                                titlePropertyValue = recipeStep.Override[titlePropertyValue];
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(titlePropertyValue))
                        {
                            if (recipeStep.Prefix.HasValue())
                            {
                                titleBuilder.Append($" {recipeStep.Prefix}");
                            }

                            titleBuilder.Append($" {titlePropertyValue}");

                            if (recipeStep.Suffix.HasValue())
                            {
                                titleBuilder.Append($" {recipeStep.Suffix}");
                            }
                        }
                    }
                }

                displayTitle = titleBuilder.ToString().Trim();
                _coreService.ReportRepository.Write(connectorId, $"Title applied to product from recipe '{recipe.Fieldset}' is '{displayTitle}'");
            }
            catch (Exception)
            {
                titleBuilder.Clear();
            }

            if (displayTitle.HasValue())
            {
                var localStringValue = displayTitle.ToLocaleString();

                _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductName), localStringValue);
                _coreService.DataRepository.UpdateEntity(productEntity);
            }
        }

        private string GetCvlTitlePropertyValue(FieldType fieldType, List<string> cvlKeys)
        {
            var valueList = new List<string>();

            foreach (var key in cvlKeys)
            {
                var cvlValue = _coreService.ModelRepository.GetCVLValueByKey(key, fieldType.CVLId);

                if (!string.IsNullOrEmpty(cvlValue?.Value?.ToString()))
                {
                    if (cvlValue.Value is LocaleString)
                    {
                        var cvlValueLocalString = cvlValue.Value as LocaleString;
                        var englishCulture = TCCore.Constants.Cultures.English;

                        if (cvlValueLocalString.ContainsCulture(englishCulture))
                        {
                            var localStringValue = cvlValueLocalString[englishCulture];
                            if (localStringValue.HasValue())
                            {
                                valueList.Add(localStringValue);
                            }
                        }
                    }
                    else
                    {
                        valueList.Add(cvlValue.Value.ToString());
                    }
                }
            }

            return string.Join("-", valueList);
        }
    }
}
