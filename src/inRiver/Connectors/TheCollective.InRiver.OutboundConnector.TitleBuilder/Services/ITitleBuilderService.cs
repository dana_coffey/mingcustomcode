﻿using System.Collections.Generic;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Services
{
    public interface ITitleBuilderService
    {
        void BuildTitle(string connectorId, Entity itemEntity, List<TitleRecipe> recipes);
    }
}
