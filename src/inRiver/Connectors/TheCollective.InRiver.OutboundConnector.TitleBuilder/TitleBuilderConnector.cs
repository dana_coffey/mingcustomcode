﻿using System;
using Absolunet.InRiver.Core.Services;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using inRiver.Integration.Export;
using inRiver.Integration.Interface;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings;
using TheCollective.InRiver.OutboundConnector.TitleBuilder.Services;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder
{
    public class TitleBuilderConnector : ServerListener, IEntityListener, IOutboundConnector
    {
        private readonly ICoreService _coreService;
        private readonly ITitleBuilderService _titleBuilderService;

        private TitleBuilderConnectorSetting _connectorSettings;

        public TitleBuilderConnector() : this(new WindsorContainer(new XmlInterpreter()))
        {
        }

        public TitleBuilderConnector(WindsorContainer container) : this(container.Resolve<ICoreService>(), container.Resolve<ITitleBuilderService>())
        {
        }

        public TitleBuilderConnector(ICoreService coreService, ITitleBuilderService titleBuilderService)
        {
            _coreService = coreService;
            _titleBuilderService = titleBuilderService;
        }

        public void EntityCommentAdded(int entityId, int commentId)
        {
        }

        public void EntityCreated(int entityId)
        {
            ManageItemEntity(entityId);
        }

        public void EntityDeleted(Entity deletedEntity)
        {
        }

        public void EntityFieldSetUpdated(int entityId, string fieldSetId)
        {
        }

        public void EntityLocked(int entityId)
        {
        }

        public void EntitySpecificationFieldAdded(int entityId, string fieldName)
        {
        }

        public void EntitySpecificationFieldUpdated(int entityId, string fieldName)
        {
        }

        public void EntityUnlocked(int entityId)
        {
        }

        public void EntityUpdated(int entityId, string[] fields)
        {
            ManageItemEntity(entityId);
        }

        public override void InitConfigurationSettings()
        {
            base.InitConfigurationSettings();

            _connectorSettings.TitleRecipes = new TitleRecipesSetting();
        }

        void IOutboundConnector.Start()
        {
            try
            {
                _coreService.ReportRepository.Write(Id, "Title Builder Connector started");
                _connectorSettings = new TitleBuilderConnectorSetting(Id, _coreService.UtilityRepository);

                try
                {
                    if (_connectorSettings.TitleRecipes == null)
                    {
                        _coreService.ReportRepository.WriteError(Id, $"The setting {nameof(TitleBuilderConnectorSetting.TitleRecipes)} is not configured");
                    }
                }
                catch (Exception ex)
                {
                    _coreService.ReportRepository.WriteException(Id, $"Unable to load {nameof(TitleBuilderConnectorSetting.TitleRecipes)}", ex);
                    throw;
                }

                Start();
            }
            catch (Exception e)
            {
                _coreService.ReportRepository.WriteError(Id, e.ToString());
            }
        }

        private void ManageItemEntity(int entityId)
        {
            var entity = _coreService.DataRepository.GetEntity(entityId, LoadLevel.Shallow);

            if (entity != null && entity.EntityType.Id == nameof(TCEntities.Item))
            {
                var recipes = _connectorSettings.TitleRecipes;

                _titleBuilderService.BuildTitle(Id, entity, recipes.TitleRecipes);
            }
        }
    }
}
