﻿using System.Collections.Generic;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings
{
    public class TitleRecipe
    {
        public string Fieldset { get; set; }
        public List<RecipeStep> RecipeSteps { get; set; }
    }
}
