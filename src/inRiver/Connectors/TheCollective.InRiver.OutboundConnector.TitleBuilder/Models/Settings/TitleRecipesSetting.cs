﻿using System.Collections.Generic;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings
{
    public class TitleRecipesSetting
    {
        public List<TitleRecipe> TitleRecipes { get; set; }
    }
}
