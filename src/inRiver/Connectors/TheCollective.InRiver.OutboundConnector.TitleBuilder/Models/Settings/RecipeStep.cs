﻿using System.Collections.Generic;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings
{
    public class RecipeStep
    {
        public string Entity { get; set; }
        public string Fieldtype { get; set; }
        public Dictionary<string, string> Override { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
    }
}
