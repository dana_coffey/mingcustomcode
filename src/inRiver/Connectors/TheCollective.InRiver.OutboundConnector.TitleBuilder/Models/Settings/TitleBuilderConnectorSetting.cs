﻿using System.Collections.Generic;
using Absolunet.InRiver.Core.Repositories;
using Newtonsoft.Json;

namespace TheCollective.InRiver.OutboundConnector.TitleBuilder.Models.Settings
{
    public class TitleBuilderConnectorSetting
    {
        protected string Id { get; set; }
        private readonly Dictionary<string, string> _settings;
        private readonly IUtilityRepository _utilityRepository;
        private TitleRecipesSetting _titleRecipesSetting;

        public TitleRecipesSetting TitleRecipes
        {
            get => _titleRecipesSetting ?? (_titleRecipesSetting = JsonConvert.DeserializeObject<TitleRecipesSetting>(GetSettings(nameof(TitleRecipes))));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(TitleRecipes), JsonConvert.SerializeObject(value));
        }

        public TitleBuilderConnectorSetting(string id, IUtilityRepository utilityRepository)
        {
            Id = id;
            _utilityRepository = utilityRepository;
            _settings = utilityRepository.GetConnectorSettings(id);
        }

        private string GetSettings(string settingName)
        {
            return _settings.ContainsKey(settingName) ? _settings[settingName].Trim() : string.Empty;
        }
    }
}
