﻿using System.Linq;
using Absolunet.InRiver.Core.Services;
using Absolunet.InRiver.Dequeue.Insite.Models.Validation;
using Absolunet.InRiver.Dequeue.Insite.Repositories;
using Absolunet.InRiver.Dequeue.Insite.Services;
using inRiver.Remoting.Objects;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.OutboundConnectors.Insite.Services
{
    public class TheCollectiveInsiteExportService : InsiteExportService
    {
        private readonly ICoreService _coreService;

        public TheCollectiveInsiteExportService(ICoreService coreService, IInsiteExportRepository insiteExportRepository, IFtpService ftpService, IMailService mailService)
            : base(coreService, insiteExportRepository, ftpService, mailService)
        {
            _coreService = coreService;
        }

        protected override void CustomProductValidation(ProductValidation validation, Entity product)
        {
            if (!ValidateTitle(product))
            {
                validation.ErrorMessages.Add($"Product {product.Id} validation failed with empty title");
            }

            var productDescription = _coreService.GetFieldValue(product, nameof(TCEntities.Product.ProductDescription)) as LocaleString;
            if (!ValidateAllLocalValuesAreFilled(productDescription))
            {
                validation.ErrorMessages.Add($"Product {product.Id} validation failed with an empty description");
            }
        }

        private static bool ValidateAllLocalValuesAreFilled(LocaleString localeString)
        {
            if (localeString == null)
            {
                return false;
            }

            return localeString.Languages.All(language => !string.IsNullOrEmpty(localeString[language]));
        }

        private bool ValidateTitle(Entity product)
        {
            var titleValues = _coreService.GetFieldValue(product, nameof(TCEntities.Product.ProductNameOverride)) as LocaleString;

            if (!ValidateAllLocalValuesAreFilled(titleValues))
            {
                titleValues = _coreService.GetFieldValue(product, nameof(TCEntities.Product.ProductName)) as LocaleString;
            }

            return ValidateAllLocalValuesAreFilled(titleValues);
        }
    }
}
