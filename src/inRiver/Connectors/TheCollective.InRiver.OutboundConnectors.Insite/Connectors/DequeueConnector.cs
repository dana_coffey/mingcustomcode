﻿using System;
using Absolunet.InRiver.Core.Helpers;
using Absolunet.InRiver.Core.Models.Errors;
using Absolunet.InRiver.Dequeue.Insite.Connectors;

namespace TheCollective.InRiver.OutboundConnectors.Insite.Connectors
{
    public class DequeueConnector : InsiteConnector
    {
        public override void AfterDoWorkException(Exception exception)
        {
            Errors.Add(new Error
            {
                Message = exception.Message
            });

            EmailHelper.FormatAndSendErrorEmail(Errors, ConnectorSettings.ErrorEmailFrom, ConnectorSettings.ErrorEmailTo, string.Empty, string.Empty);
        }
    }
}
