﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Services;
using Castle.Core.Internal;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;
using TheCollective.InRiver.Core.LinkType;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.OutboundConnector.Carrier.Repositories;
using TCCore = TheCollective.InRiver.Core;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Services
{
    public class CarrierImportService : ICarrierImportService
    {
        private const string HighestRes = "highest-res";
        private const string ImageResourceType = "IMAGE";

        private readonly ICarrierApiService _carrierApiService;
        private readonly ICarrierEpicApiService _carrierEpicApiService;
        private readonly ICarrierSupersededService _carrierSupersededService;
        private readonly ICoreService _coreService;
        private readonly IInRiverRepository _inRiverRepository;

        public CarrierApiCredentials ApiCredentials
        {
            get => _carrierApiService.ApiCredentials;
            set => _carrierApiService.ApiCredentials = value;
        }

        public CarrierImportService(ICoreService coreService, ICarrierApiService carrierApiService, ICarrierEpicApiService carrierEpicApiService, IInRiverRepository inRiverRepository, ICarrierSupersededService carrierSupersededService)
        {
            _coreService = coreService;
            _carrierApiService = carrierApiService;
            _carrierEpicApiService = carrierEpicApiService;
            _inRiverRepository = inRiverRepository;
            _carrierSupersededService = carrierSupersededService;
        }

        public void EnrichEquipmentEntity(int entityId, string connectorId, List<string> carrierVendorNumberIds, Dictionary<string, string> categoryToFieldsetMapper, bool validateFieldSetCarrier = false)
        {
            var entity = _coreService.DataRepository.GetEntity(entityId, LoadLevel.DataOnly);

            if (entity != null && entity.EntityType.Id == nameof(TCEntities.Item))
            {
                var autoEnrichementStatus = _coreService.GetFieldValue(entity, nameof(TCEntities.Item.AutoEnrichmentStatus))?.ToString();

                if (!validateFieldSetCarrier || autoEnrichementStatus != TCCore.Constants.FieldTypes.ImportItem.Done)
                {
                    EnrichEquipment(entity, connectorId, carrierVendorNumberIds, categoryToFieldsetMapper);
                }
            }
            else if (entity == null)
            {
                _coreService.ReportRepository.Write(connectorId, $"Unable to enrich the entity '{entityId}' because it doesn't exist in the PIM.");
            }
        }

        public void EnrichPartsEntity(int entityId, string connectorId, List<string> sxePartsVendorIds)
        {
            var itemEntity = _coreService.DataRepository.GetEntity(entityId, LoadLevel.Shallow);

            if (itemEntity != null && itemEntity.EntityType.Id == nameof(TCEntities.Item))
            {
                itemEntity = _coreService.SetLoadLevel(itemEntity, LoadLevel.DataAndLinks);
                if (!IsSxePartVendor(itemEntity, sxePartsVendorIds))
                {
                    return;
                }

                EnrichItemSupersededParts(itemEntity, connectorId);
                _coreService.SetFieldValue(itemEntity, nameof(TCEntities.Item.AutoEnrichmentStatus), TCCore.Constants.FieldTypes.ImportItem.Done);
            }
            else if (itemEntity == null)
            {
                _coreService.ReportRepository.Write(connectorId, $"Unable to enrich the part entity '{entityId}' because it doesn't exist in the PIM.");
            }
        }

        public void ManageProductDocuments(string connectorId, Entity itemEntity, string productUrn, string familyUrn)
        {
            var productDocuments = _carrierApiService.GetDocuments(productUrn);
            var productFamilyDocuments = _carrierApiService.GetDocuments(familyUrn);

            var allDocuments = productDocuments.Documents.Union(productFamilyDocuments.Documents).Distinct().ToList();

            foreach (var document in allDocuments)
            {
                try
                {
                    var documentResourceEntity = _inRiverRepository.GetOrCreateDocumentResource(connectorId, document);

                    // Create Create ItemRelatedDocumentLink
                    var itemRelatedDocumentLinkType = _coreService.ModelRepository.GetLinkType(nameof(ItemRelatedDocument));
                    _coreService.DataRepository.CreateLinkIfNecessary(itemEntity, documentResourceEntity, itemRelatedDocumentLinkType);

                    if (document.Current)
                    {
                        // Create ItemResourceLink
                        var itemResourceLinkType = _coreService.ModelRepository.GetLinkType(nameof(ItemResource));
                        _coreService.DataRepository.CreateLinkIfNecessary(itemEntity, documentResourceEntity, itemResourceLinkType);
                    }
                }
                catch (Exception)
                {
                    _coreService.ReportRepository.WriteError(connectorId, $"Could not attach document '{document.ContentId}' to item {itemEntity.Id}.");
                }
            }
        }

        private static List<ProductMedia> GetProductMediaImages(Dictionary<string, List<ProductMedia>> medias)
        {
            if (medias == null || !medias.Any())
            {
                return null;
            }

            var listMedias = new List<ProductMedia>();

            if (medias.ContainsKey(HighestRes))
            {
                listMedias.AddRange(medias[HighestRes]);
            }
            else
            {
                // Get the next highest resolution available
                var mediaImages = medias.SelectMany(x => x.Value).Where(media => media.Type == ImageResourceType).ToList();

                if (mediaImages.Any())
                {
                    var fallbackImage = mediaImages.OrderByDescending(media => media.Width * media.Height).First();
                    listMedias.Add(fallbackImage);
                }
            }

            return listMedias;
        }

        private void EnrichEquipment(Entity itemEntity, string connectorId, List<string> supportedVendorIds, Dictionary<string, string> categoryToFieldsetMapper)
        {
            itemEntity = _coreService.SetLoadLevel(itemEntity, LoadLevel.DataAndLinks);

            var itemVendorNumber = _coreService.GetFieldValue(itemEntity, nameof(TCEntities.Item.ItemERPVendorNo));

            if (!supportedVendorIds.Contains(itemVendorNumber))
            {
                return;
            }

            var itemVendorUrn = (string)_coreService.GetFieldValue(itemEntity, nameof(TCEntities.Item.ItemUrn));

            if (itemVendorUrn.IsNullOrEmpty())
            {
                return;
            }

            var carrierProduct = _carrierApiService.GetProduct(itemVendorUrn);

            if (string.IsNullOrEmpty(carrierProduct?.Urn) || carrierProduct.FamilyProductOf == null)
            {
                _coreService.ReportRepository.Write(connectorId, $"Unable to get info from Carrier API for product URN '{itemVendorUrn}'");
                return;
            }

            var productEntity = GetProductEntity(itemEntity);

            if (productEntity == null)
            {
                return;
            }

            productEntity = _coreService.SetLoadLevel(productEntity, LoadLevel.DataOnly);
            productEntity = _inRiverRepository.UpdateProduct(carrierProduct.FamilyProductOf, productEntity);

            var resourceImages = GetResourceImages(carrierProduct.FamilyProductOf);

            var firstImage = resourceImages.FirstOrDefault();
            if (firstImage != null)
            {
                _inRiverRepository.CreateLinkProductResource(productEntity, firstImage);
            }

            foreach (var resource in resourceImages)
            {
                _inRiverRepository.CreateLinkItemResource(itemEntity, resource);
            }

            ManageProductDocuments(connectorId, itemEntity, carrierProduct.Urn, carrierProduct.FamilyProductOf.Urn);
            _coreService.ReportRepository.Write(connectorId, $"item with urn '{itemVendorUrn}' has been enriched.");
        }

        private void EnrichItemSupersededParts(Entity itemEntity, string connectorId)
        {
            var partNumber = _coreService.GetFieldValue(itemEntity, nameof(TCEntities.Item.ItemSku))?.ToString();
            try
            {
                if (!partNumber.IsNullOrEmpty())
                {
                    _carrierEpicApiService.ApiCredentials = ApiCredentials;
                    var supersededParts = _carrierEpicApiService.GetSupersededParts(partNumber);

                    if (!supersededParts.Any())
                    {
                        return;
                    }

                    var createSupersededLinkResult = _carrierSupersededService.CreateSupersededLinks(supersededParts, itemEntity);

                    WriteLogSupersededParts(connectorId, createSupersededLinkResult.Success, partNumber, createSupersededLinkResult.Errors);
                }
            }
            catch
            {
                _coreService.ReportRepository.Write(connectorId, $"Unable to enrich the item '{itemEntity.Id}' with superseded parts for part number {partNumber} because an error has ocurred getting information from Epic API.");
            }
        }

        private Entity GetProductEntity(Entity itemEntity)
        {
            Entity productEntity;
            const int MaxRetryAttempt = 5;
            var nbRetry = 0;
            bool stopRetry;

            do
            {
                productEntity = itemEntity.InboundLinks.FirstOrDefault(x => x.LinkType.Id == nameof(ProductItem))?.Source;
                if (productEntity == null)
                {
                    if (nbRetry++ < MaxRetryAttempt)
                    {
                        itemEntity = _coreService.SetLoadLevel(itemEntity, LoadLevel.DataAndLinks);
                        stopRetry = false;
                        Thread.Sleep(TimeSpan.FromSeconds(3));
                    }
                    else
                    {
                        stopRetry = true;
                    }
                }
                else
                {
                    stopRetry = true;
                }
            }
            while (!stopRetry);

            return productEntity;
        }

        private List<Entity> GetResourceImages(ProductFamily familyModel)
        {
            var listMedias = GetProductMediaImages(familyModel.Media);

            var resourceEntities = listMedias.Select(_inRiverRepository.GetOrCreateProductResource).ToList();

            return resourceEntities;
        }

        private bool IsSxePartVendor(Entity itemEntity, List<string> sxePartsVendorIds)
        {
            var itemVendorNumber = _coreService.GetFieldValue(itemEntity, nameof(TCEntities.Item.ItemERPVendorNo));

            return sxePartsVendorIds.Contains(itemVendorNumber);
        }

        private void WriteLogSupersededParts(string connectorId, List<string> linkPartsSuccess, string partNumber, List<string> linksPartsErrors)
        {
            if (linkPartsSuccess != null && linkPartsSuccess.Any())
            {
                _coreService.ReportRepository.Write(connectorId, $"Superseded parts link between '{partNumber}' and {string.Join(", ", linkPartsSuccess)} has been created successfully");
            }

            if (linksPartsErrors != null && linksPartsErrors.Any())
            {
                _coreService.ReportRepository.Write(connectorId, $"Unable to create superseded parts link between '{partNumber}' and {string.Join(", ", linksPartsErrors)}");
            }
        }
    }
}
