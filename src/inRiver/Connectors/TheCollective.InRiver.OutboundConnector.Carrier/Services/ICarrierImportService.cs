﻿using System.Collections.Generic;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Services
{
    public interface ICarrierImportService
    {
        CarrierApiCredentials ApiCredentials { get; set; }

        void EnrichEquipmentEntity(int entityId, string connectorId, List<string> carrierVendorNumberIds, Dictionary<string, string> categoryToFieldsetMapper, bool validateFieldSetCarrier = false);

        void EnrichPartsEntity(int entityId, string connectorId, List<string> sxePartsVendorIds);

        void ManageProductDocuments(string connectorId, Entity itemEntity, string productUrn, string familyUrn);
    }
}
