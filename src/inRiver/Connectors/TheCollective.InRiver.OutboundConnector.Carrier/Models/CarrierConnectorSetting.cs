﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Repositories;
using Newtonsoft.Json;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Models
{
    public class CarrierConnectorSetting
    {
        private readonly Dictionary<string, string> _settings;
        private readonly IUtilityRepository _utilityRepository;

        public string CarrierApiBaseUrl
        {
            get => GetSettings(nameof(CarrierApiBaseUrl));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierApiBaseUrl), value);
        }

        public string CarrierApiBearerTokenPassword
        {
            get => GetSettings(nameof(CarrierApiBearerTokenPassword));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierApiBearerTokenPassword), value);
        }

        public string CarrierApiBearerTokenUsername
        {
            get => GetSettings(nameof(CarrierApiBearerTokenUsername));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierApiBearerTokenUsername), value);
        }

        public string CarrierApiPassword
        {
            get => GetSettings(nameof(CarrierApiPassword));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierApiPassword), value);
        }

        public string CarrierApiUsername
        {
            get => GetSettings(nameof(CarrierApiUsername));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierApiUsername), value);
        }

        public Dictionary<string, string> CarrierCategoryFieldSetJsonMapper
        {
            get => JsonConvert.DeserializeObject<Dictionary<string, string>>(GetSettings(nameof(CarrierCategoryFieldSetJsonMapper)));
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierCategoryFieldSetJsonMapper), JsonConvert.SerializeObject(value));
        }

        public List<string> CarrierVendorNumberIds
        {
            get => GetSettings(nameof(CarrierVendorNumberIds)).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            set => _utilityRepository.SetConnectorSetting(Id, nameof(CarrierVendorNumberIds), string.Join(",", value));
        }

        public List<string> SxePartsVendorIds
        {
            get => GetSettings(nameof(SxePartsVendorIds)).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            set => _utilityRepository.SetConnectorSetting(Id, nameof(SxePartsVendorIds), string.Join(",", value));
        }

        protected string Id { get; set; }

        public CarrierConnectorSetting(string id, IUtilityRepository utilityRepository)
        {
            Id = id;
            _utilityRepository = utilityRepository;
            _settings = utilityRepository.GetConnectorSettings(id);
        }

        private string GetSettings(string settingName)
        {
            return _settings.ContainsKey(settingName) ? _settings[settingName].Trim() : string.Empty;
        }
    }
}
