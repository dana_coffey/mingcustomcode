﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Helpers
{
    public static class CarrierHelpers
    {
        public static string CalculateMd5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            var md5 = MD5.Create();

            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            var sb = new StringBuilder();
            foreach (var t in hash)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Return the first letter of each word in UpperCase.
        /// </summary>
        public static string ToTitleCase(string value)
        {
            var ti = CultureInfo.CurrentCulture.TextInfo;
            var result = ti.ToTitleCase(value);

            return result;
        }
    }
}
