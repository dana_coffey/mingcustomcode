﻿using System.Collections.Generic;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Repositories
{
    public interface IInRiverRepository
    {
        void CreateLinkItemResource(Entity itemEntity, Entity resourceEntity);

        void CreateLinkProductResource(Entity productEntity, Entity resourceEntity);

        Entity GetOrCreateDocumentResource(string connectorId, Document doc);

        Entity GetOrCreateProductResource(ProductMedia media);

        Entity UpdateItem(Entity itemEntity, Entity productEntity, string connectorId, ProductModel productModel, Dictionary<string, string> categoryToFieldsetMapper);

        Entity UpdateProduct(ProductFamily familyModel, Entity productEntity);
    }
}
