﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using Absolunet.InRiver.Core.Models.Entities;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Services;
using Castle.Core.Internal;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.InRiver.Core.CVL;
using TheCollective.InRiver.Core.FieldSets;
using TheCollective.InRiver.Core.Helpers;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.OutboundConnector.Carrier.Helpers;
using TCCore = TheCollective.InRiver.Core;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.OutboundConnector.Carrier.Repositories
{
    public class InRiverRepository : IInRiverRepository
    {
        private const string DataTypeRange = "Range";
        private const string No = "no";
        private const string Yes = "yes";

        private readonly ICoreService _coreService;
        private readonly ICvlService _cvlService;

        public InRiverRepository(ICoreService coreService, ICvlService cvlService)
        {
            _coreService = coreService;
            _cvlService = cvlService;
        }

        public void CreateLinkItemResource(Entity itemEntity, Entity resourceEntity)
        {
            var linkType = _coreService.ModelRepository.GetLinkType(nameof(ItemResource));
            _coreService.DataRepository.CreateLinkIfNecessary(itemEntity, resourceEntity, linkType);
        }

        public void CreateLinkProductResource(Entity productEntity, Entity resourceEntity)
        {
            var linkType = _coreService.ModelRepository.GetLinkType(nameof(ProductResource));
            _coreService.DataRepository.CreateLinkIfNecessary(productEntity, resourceEntity, linkType);
        }

        public Entity GetOrCreateDocumentResource(string connectorId, Document document)
        {
            var documentLocal = document.Locales?.FirstOrDefault();

            var documentOriginalFilename = document.Url.Substring(document.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);
            var filename = $"{document.SubType} - {documentOriginalFilename}";

            // Carrier document titles are not always properly formed. We've received \r\n, @, and a bunch of characters causing trouble. We remove them.
            var regex = new Regex("[^a-zA-Z0-9-_. ]");
            var sanitizedFilename = regex.Replace(filename, string.Empty);

            _coreService.ReportRepository.Write(connectorId, $"GetOrCreateResourve {sanitizedFilename}");
            var resourceEntity = GetOrCreateResource(sanitizedFilename, document.Url, nameof(ResourceRelatedDocument));

            var titleLocalString = string.IsNullOrEmpty(documentLocal) ? document.Title.ToLocaleString() : document.Title.ToLocaleString(new CultureInfo(documentLocal));

            var localTitleRegezFilter = new Regex("[^a-zA-Z0-9-/_. ]");
            foreach (var language in titleLocalString.Languages)
            {
                titleLocalString[language] = localTitleRegezFilter.Replace(titleLocalString[language], string.Empty);
            }

            var cvlKey = CarrierHelpers.CalculateMd5Hash(document.SubType);
            _cvlService.CreateCvlValueWhenMissing(nameof(DocumentType), cvlKey, document.SubType);

            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceDocumentType), cvlKey);
            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceTitle), titleLocalString);
            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourcePublishDate), document.PublishedDate);
            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceLocale), documentLocal);

            var entityCreated = _coreService.DataRepository.AddOrUpdateEntity(resourceEntity);

            return entityCreated;
        }

        public Entity GetOrCreateProductResource(ProductMedia media)
        {
            var filename = media.Url.Substring(media.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);

            return GetOrCreateResource(filename, media.Url);
        }

        public Entity UpdateItem(Entity itemEntity, Entity productEntity, string connectorId, ProductModel productModel, Dictionary<string, string> categoryToFieldsetMapper)
        {
            SetItemFieldset(itemEntity, productEntity, connectorId, productModel, categoryToFieldsetMapper);

            if (productModel.Attributes != null && productModel.Attributes.Any())
            {
                // Get only carrier attributes with specified format: dictionnary key = type / urn, value = value
                var carrierAttributes = productModel.Attributes.SelectMany(a => a.Value)
                    .Where(a => a.Type != null && !a.Type.Urn.IsNullOrEmpty())
                    .ToDictionary(a => a.Type.Urn, at => at.Type.ValueType == DataTypeRange ? at.Values : new List<string> { at.Value });

                EnrichItemWithAttributes(itemEntity, carrierAttributes);

                _coreService.SetFieldValue(itemEntity, nameof(TCEntities.Item.AutoEnrichmentStatus), TCCore.Constants.FieldTypes.ImportItem.Done);

                _coreService.DataRepository.UpdateEntity(itemEntity);
            }

            return itemEntity;
        }

        public Entity UpdateProduct(ProductFamily familyModel, Entity productEntity)
        {
            var overViewText = familyModel.OverviewTexts != null && familyModel.OverviewTexts.Any() ? familyModel.OverviewTexts.First().Value : string.Empty;
            var featureText = familyModel.FeaturesTexts != null && familyModel.FeaturesTexts.Any() ? familyModel.FeaturesTexts.First().Value : string.Empty;
            var familyCategories = string.Join(";", familyModel.Categories);

            _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductFamilyProductsUrn), familyModel.Urn);
            _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductCarrierCategories), familyCategories);
            _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductCarrierName), WebUtility.HtmlDecode(familyModel.Name));
            _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductCarrierOverviewTexts), overViewText);
            _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductCarrierFeaturesTexts), featureText);

            productEntity = _coreService.DataRepository.UpdateEntity(productEntity);

            if (productEntity.FieldSetId != nameof(ProductCarrierEnrichment))
            {
                _coreService.DataRepository.SetEntityFieldSet(productEntity.Id, nameof(ProductCarrierEnrichment));
            }

            return productEntity;
        }

        private static byte[] GetStreamFromUrl(string url)
        {
            byte[] data;

            using (var webClient = new WebClient())
            {
                data = webClient.DownloadData(url);
            }

            return data;
        }

        private void EnrichItemWithAttributes(Entity itemEntity, Dictionary<string, List<string>> carrierAttributes)
        {
            var itemFieldTypesWithCarrierId = _coreService.ModelRepository.GetAllFieldTypes().Where(ft => ft.EntityTypeId == nameof(Item)).Where(f => f.Settings.ContainsKey(TCCore.Constants.FieldTypes.Settings.CarrierId)).ToList();

            foreach (var carrierAttribute in carrierAttributes)
            {
                var searchedFieldType = itemFieldTypesWithCarrierId.FirstOrDefault(f => f.Settings[TCCore.Constants.FieldTypes.Settings.CarrierId] == carrierAttribute.Key);

                if (searchedFieldType != null)
                {
                    string value;
                    string attributeTitleCase;
                    if (searchedFieldType.DataType == DataType.CVL)
                    {
                        var keys = new List<string>();
                        foreach (var fieldValue in carrierAttribute.Value)
                        {
                            attributeTitleCase = CarrierHelpers.ToTitleCase(fieldValue);

                            var htmlDecodedFieldValue = WebUtility.HtmlDecode(attributeTitleCase);

                            var cvlKey = CarrierHelpers.CalculateMd5Hash(htmlDecodedFieldValue);
                            keys.Add(cvlKey);

                            _cvlService.CreateCvlValueWhenMissing(searchedFieldType.CVLId, cvlKey, htmlDecodedFieldValue);
                        }

                        value = string.Join(";", keys);
                    }
                    else
                    {
                        attributeTitleCase = CarrierHelpers.ToTitleCase(carrierAttribute.Value.First());
                        value = carrierAttribute.Value.Any() ? WebUtility.HtmlDecode(attributeTitleCase) : string.Empty;
                    }

                    SetValueByDataType(itemEntity, searchedFieldType.Id, value, searchedFieldType.DataType);
                }
            }
        }

        private Entity GetOrCreateResource(string filename, string url, string fieldsetId = null)
        {
            var resourceEntity = _coreService.DataRepository.GetEntityByUniqueValue(nameof(TCEntities.Resource.ResourceFilename), filename, LoadLevel.DataOnly);

            if (resourceEntity != null)
            {
                return resourceEntity;
            }

            var stream = GetStreamFromUrl(url);
            var mimeType = MimeMapping.GetMimeMapping(filename);

            var fileId = _coreService.UtilityRepository.AddFile(filename, stream);

            resourceEntity = _coreService.DataRepository.CreateEntity(_coreService.ModelRepository.GetEntityType(nameof(Resource)));

            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceFileId), fileId);
            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceFilename), filename);
            _coreService.SetFieldValue(resourceEntity, nameof(TCEntities.Resource.ResourceMimeType), mimeType);

            var entityCreated = _coreService.DataRepository.AddOrUpdateEntity(resourceEntity);

            if (fieldsetId != null)
            {
                _coreService.DataRepository.SetEntityFieldSet(entityCreated.Id, fieldsetId);
            }

            return entityCreated;
        }

        private void SetItemFieldset(Entity itemEntity, Entity productEntity, string connectorId, ProductModel productModel, Dictionary<string, string> categoryToFieldsetMapper)
        {
            var carrierCategory = productModel.FamilyProductOf.Categories.FirstOrDefault();

            if (carrierCategory != null)
            {
                categoryToFieldsetMapper.TryGetValue(carrierCategory, out var fieldSetValue);

                if (!fieldSetValue.IsNullOrEmpty())
                {
                    _coreService.DataRepository.SetEntityFieldSet(itemEntity.Id, fieldSetValue);
                    _coreService.SetFieldValue(productEntity, nameof(ProductCategory), fieldSetValue);
                    _coreService.DataRepository.UpdateEntity(productEntity);
                }
                else
                {
                    _coreService.ReportRepository.Write(connectorId, $"Could not resolve carrier caterogy '{carrierCategory}' to a fieldset value");
                }
            }
        }

        private void SetValueBoolean(Entity itemEntity, string fieldTypeId, string newValueField)
        {
            if (!newValueField.IsNullOrEmpty() && (newValueField.Equals(Yes, StringComparison.InvariantCultureIgnoreCase) || newValueField.Equals(No, StringComparison.InvariantCultureIgnoreCase)))
            {
                newValueField = newValueField.Equals(Yes, StringComparison.InvariantCultureIgnoreCase) ? bool.TrueString : bool.FalseString;
            }

            if (bool.TryParse(newValueField?.ToLowerInvariant(), out var booleanTypeValue))
            {
                _coreService.SetFieldValue(itemEntity, fieldTypeId, booleanTypeValue);
            }
        }

        private void SetValueByDataType(Entity itemEntity, string fieldTypeId, string newValueField, string dataType)
        {
            if (newValueField != null)
            {
                var culture = TCCore.Constants.Cultures.English;

                const NumberStyles StyleNum = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;

                switch (dataType)
                {
                    case nameof(DataType.Double):
                        SetValueDouble(itemEntity, fieldTypeId, newValueField, culture, StyleNum);
                        break;
                    case nameof(DataType.Integer):
                        SetValueInteger(itemEntity, fieldTypeId, newValueField, culture, StyleNum);
                        break;
                    case nameof(DataType.Boolean):
                        SetValueBoolean(itemEntity, fieldTypeId, newValueField);
                        break;
                    case nameof(DataType.DateTime):
                        SetValueDateTime(itemEntity, fieldTypeId, newValueField, culture);
                        break;
                    case nameof(DataType.LocaleString):
                        SetValueLocalString(itemEntity, fieldTypeId, newValueField);
                        break;
                    case nameof(DataType.Xml):
                        SetValueXml(itemEntity, fieldTypeId, newValueField);
                        break;
                    default:
                        _coreService.SetFieldValue(itemEntity, fieldTypeId, newValueField);
                        break;
                }
            }
        }

        private void SetValueDateTime(Entity itemEntity, string fieldTypeId, string newValueField, IFormatProvider culture)
        {
            const DateTimeStyles dateTimeStyles = DateTimeStyles.None;

            if (DateTime.TryParse(newValueField, culture, dateTimeStyles, out var dateTimeTypeValue))
            {
                _coreService.SetFieldValue(itemEntity, fieldTypeId, dateTimeTypeValue);
            }
        }

        private void SetValueDouble(Entity itemEntity, string fieldTypeId, string newValueField, IFormatProvider culture, NumberStyles styleNum)
        {
            if (double.TryParse(newValueField, styleNum, culture, out var doubleTypeValue))
            {
                _coreService.SetFieldValue(itemEntity, fieldTypeId, doubleTypeValue);
            }
        }

        private void SetValueInteger(Entity itemEntity, string fieldTypeId, string newValueField, IFormatProvider culture, NumberStyles styleNum)
        {
            if (int.TryParse(newValueField, styleNum, culture, out var integerTypeValue))
            {
                _coreService.SetFieldValue(itemEntity, fieldTypeId, integerTypeValue);
            }
        }

        private void SetValueLocalString(Entity itemEntity, string fieldTypeId, string newValueField)
        {
            var languages = _coreService.Languages;
            var localeString = new LocaleString(languages);

            foreach (var language in languages)
            {
                localeString[language] = newValueField;
            }

            _coreService.SetFieldValue(itemEntity, fieldTypeId, localeString);
        }

        private void SetValueXml(Entity itemEntity, string fieldTypeId, string newValueField)
        {
            var xml = new XmlDocument();
            xml.LoadXml(newValueField);
            _coreService.SetFieldValue(itemEntity, fieldTypeId, xml);
        }
    }
}
