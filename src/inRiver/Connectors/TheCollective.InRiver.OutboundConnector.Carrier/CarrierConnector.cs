﻿using System;
using System.Collections.Generic;
using Absolunet.InRiver.Core.Services;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using inRiver.Integration.Export;
using inRiver.Integration.Interface;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.InRiver.OutboundConnector.Carrier.Models;
using TheCollective.InRiver.OutboundConnector.Carrier.Services;

namespace TheCollective.InRiver.OutboundConnector.Carrier
{
    public class CarrierConnector : ServerListener, IEntityListener, IOutboundConnector
    {
        private readonly ICarrierImportService _carrierImportService;
        private readonly ICoreService _coreService;

        private CarrierConnectorSetting _connectorSettings;

        protected CarrierConnectorSetting ConnectorSettings => _connectorSettings ?? (_connectorSettings = new CarrierConnectorSetting(Id, _coreService.UtilityRepository));

        public CarrierConnector() : this(new WindsorContainer(new XmlInterpreter()))
        {
        }

        public CarrierConnector(WindsorContainer container) : this(container.Resolve<ICoreService>(), container.Resolve<ICarrierImportService>())
        {
        }

        public CarrierConnector(ICoreService coreService, ICarrierImportService carrierImportService)
        {
            _coreService = coreService;
            _carrierImportService = carrierImportService;
        }

        public void EntityCommentAdded(int entityId, int commentId)
        {
        }

        public void EntityCreated(int entityId)
        {
            try
            {
                _carrierImportService.EnrichEquipmentEntity(entityId, Id, _connectorSettings.CarrierVendorNumberIds, _connectorSettings.CarrierCategoryFieldSetJsonMapper);
                _carrierImportService.EnrichPartsEntity(entityId, Id, _connectorSettings.SxePartsVendorIds);
            }
            catch (Exception e)
            {
                _coreService.ReportRepository.WriteError(Id, $"Error treating entity id {entityId} on create event. Exception: {e}");
            }
        }

        public void EntityDeleted(Entity deletedEntity)
        {
        }

        public void EntityFieldSetUpdated(int entityId, string fieldSetId)
        {
        }

        public void EntityLocked(int entityId)
        {
        }

        public void EntitySpecificationFieldAdded(int entityId, string fieldName)
        {
        }

        public void EntitySpecificationFieldUpdated(int entityId, string fieldName)
        {
        }

        public void EntityUnlocked(int entityId)
        {
        }

        public void EntityUpdated(int entityId, string[] fields)
        {
            try
            {
                _carrierImportService.EnrichEquipmentEntity(entityId, Id, _connectorSettings.CarrierVendorNumberIds, _connectorSettings.CarrierCategoryFieldSetJsonMapper, true);
                _carrierImportService.EnrichPartsEntity(entityId, Id, _connectorSettings.SxePartsVendorIds);
            }
            catch (Exception e)
            {
                _coreService.ReportRepository.WriteError(Id, $"Error treating entity id {entityId} on create event. Exception: {e}");
            }
        }

        public override void InitConfigurationSettings()
        {
            base.InitConfigurationSettings();

            _connectorSettings.CarrierApiBaseUrl = "https://services.ccs.utc.com";
            _connectorSettings.CarrierApiUsername = string.Empty;
            _connectorSettings.CarrierApiPassword = string.Empty;
            _connectorSettings.CarrierApiBearerTokenUsername = string.Empty;
            _connectorSettings.CarrierApiBearerTokenPassword = string.Empty;

            _connectorSettings.SxePartsVendorIds = new List<string>();
            _connectorSettings.CarrierVendorNumberIds = new List<string>();
        }

        void IOutboundConnector.Start()
        {
            try
            {
                _coreService.ReportRepository.Write(Id, "Carrier Connector started");
                _connectorSettings = new CarrierConnectorSetting(Id, _coreService.UtilityRepository);

                _carrierImportService.ApiCredentials = new CarrierApiCredentials
                {
                    ApiPassword = _connectorSettings.CarrierApiPassword,
                    BaseApiUrl = _connectorSettings.CarrierApiBaseUrl,
                    BearerTokenPassword = _connectorSettings.CarrierApiBearerTokenPassword,
                    BearerTokenUsername = _connectorSettings.CarrierApiBearerTokenUsername,
                    Username = _connectorSettings.CarrierApiUsername
                };

                Start();
            }
            catch (Exception e)
            {
                _coreService.ReportRepository.WriteError(Id, e.ToString());
            }
        }
    }
}
