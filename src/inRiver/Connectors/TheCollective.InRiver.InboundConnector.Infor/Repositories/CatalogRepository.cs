﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheCollective.InRiver.InboundConnector.Infor.Helpers;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;

namespace TheCollective.InRiver.InboundConnector.Infor.Repositories
{
    public class CatalogRepository : ICatalogRepository
    {
        public IEnumerable<BarcodeInformation> GetCatalogBarcodes(ConnectionStringSetting connectionStringSetting, string[] productSkus)
        {
            if (!productSkus?.Any() ?? true)
            {
                return new List<BarcodeInformation>();
            }

            var query = ResourceHelper.GetQueryFile("CatalogBarcodeInfoQuery.sql");
            var whereClause = $" AND icsc.catalog IN ({QueryHelper.ToSqlInFormat(productSkus)})";

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<BarcodeInformation>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<BarcodeInformation> GetCatalogBarcodesByDate(ConnectionStringSetting connectionStringSetting, DateTime updatedSince)
        {
            var whereClause = string.Empty;
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsc.slchgdt >= '{formattedDate}' OR icsc.transdt >= '{formattedDate}')");
            }

            var query = ResourceHelper.GetQueryFile("CatalogBarcodeInfoQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<BarcodeInformation>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetCatalogs(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, params string[] skus)
        {
            if (!skus.Any())
            {
                return new List<Product>();
            }

            var sqlSkus = QueryHelper.ToSqlInFormat(skus);

            var whereClause = $" AND icsc.catalog IN ({sqlSkus})";
            var sastaWhereClause = IncludeSastaFilter(parameters);

            var query = ResourceHelper.GetQueryFile("CatalogQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{SastaWhereClause}", sastaWhereClause }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<BarcodeInformation> GetProductBarcodes(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, string[] productsSku)
        {
            var whereClause = string.Empty;
            var query = ResourceHelper.GetQueryFile("ProductBarcodeInfoQuery.sql");

            if (productsSku?.Any() ?? false)
            {
                whereClause = $" AND icsp.prod IN ({QueryHelper.ToSqlInFormat(productsSku)})";
            }

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{GhostWarehouse}", parameters.GhostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<BarcodeInformation>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<BarcodeInformation> GetProductBarcodesByDate(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince)
        {
            var whereClause = string.Empty;
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);
            var query = ResourceHelper.GetQueryFile("ProductBarcodeInfoQuery.sql");

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsp.slchgdt >= '{formattedDate}' OR icsw.slchgdt >= '{formattedDate}' OR icsp.transdt >= '{formattedDate}' OR icsw.transdt >= '{formattedDate}')");
            }

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{GhostWarehouse}", parameters.GhostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<BarcodeInformation>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetProducts(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, params string[] skus)
        {
            if (!skus.Any())
            {
                return new List<Product>();
            }

            var sqlSkus = QueryHelper.ToSqlInFormat(skus);

            var whereClause = $" AND icsp.prod IN ({sqlSkus})";
            var sastaWhereClause = IncludeSastaFilter(parameters);

            var query = ResourceHelper.GetQueryFile("ProductsQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{SastaWhereClause}", sastaWhereClause },
                { "{GhostWarehouse}", parameters.GhostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<ErpLink> GetRelations(ConnectionStringSetting connectionStringSetting, string companyNumber)
        {
            var query = ResourceHelper.GetQueryFile("RelationsQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber }
            });

            return QueryHelper.ExecuteOdbc<ErpLink>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetUpdatedCatalog(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince)
        {
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);

            var whereClause = string.Empty;
            var sastaWhereClause = IncludeSastaFilter(parameters);

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsc.slchgdt >= '{formattedDate}' OR icsc.transdt >= '{formattedDate}')");
            }

            var query = ResourceHelper.GetQueryFile("CatalogQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{SastaWhereClause}", sastaWhereClause }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetUpdatedProducts(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince)
        {
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);

            var whereClause = string.Empty;
            var sastaWhereClause = IncludeSastaFilter(parameters);

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsp.slchgdt >= '{formattedDate}' OR icsw.slchgdt >= '{formattedDate}' OR icsp.transdt >= '{formattedDate}' OR icsw.transdt >= '{formattedDate}')");
            }

            var query = ResourceHelper.GetQueryFile("ProductsQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", parameters.InforCompanyNumber },
                { "{WhereClause}", whereClause },
                { "{SastaWhereClause}", sastaWhereClause },
                { "{GhostWarehouse}", parameters.GhostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        private static string IncludeSastaFilter(ImportParameters parameters)
        {
            var whereClause = string.Empty;

            if (string.IsNullOrEmpty(parameters.MajorDescriptionSastaCodeIden) &&
                string.IsNullOrEmpty(parameters.MajorMinorCategoryDescriptionSastaCodeIden) &&
                string.IsNullOrEmpty(parameters.ProductCategoryDescriptionSastaCodeIden))
            {
                whereClause = string.Concat(whereClause, " AND sasta.codeiden = 'c'");
            }
            else
            {
                var sqlCodeIden = QueryHelper.ToSqlInFormat(new string[]
                {
                    parameters.MajorDescriptionSastaCodeIden,
                    parameters.MajorMinorCategoryDescriptionSastaCodeIden,
                    parameters.ProductCategoryDescriptionSastaCodeIden
                }).Replace(",,", ",").Trim(',');

                whereClause = string.Concat(whereClause, $" AND sasta.codeiden IN ({sqlCodeIden})");
            }

            return whereClause;
        }
    }
}
