﻿using System;
using System.Collections.Generic;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;

namespace TheCollective.InRiver.InboundConnector.Infor.Repositories
{
    public interface ICatalogRepository
    {
        IEnumerable<BarcodeInformation> GetCatalogBarcodes(ConnectionStringSetting connectionStringSetting, string[] productSkus);

        IEnumerable<BarcodeInformation> GetCatalogBarcodesByDate(ConnectionStringSetting connectionStringSetting, DateTime updatedSince);

        IEnumerable<Product> GetCatalogs(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, params string[] skus);

        IEnumerable<BarcodeInformation> GetProductBarcodes(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, string[] productsSku);

        IEnumerable<BarcodeInformation> GetProductBarcodesByDate(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince);

        IEnumerable<Product> GetProducts(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, params string[] skus);

        IEnumerable<ErpLink> GetRelations(ConnectionStringSetting connectionStringSetting, string companyNumber);

        IEnumerable<Product> GetUpdatedCatalog(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince);

        IEnumerable<Product> GetUpdatedProducts(ConnectionStringSetting connectionStringSetting, ImportParameters parameters, DateTime updatedSince);
    }
}
