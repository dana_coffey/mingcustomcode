﻿using System;
using System.Collections.Generic;
using Absolunet.InRiver.Core.Connectors;
using Absolunet.InRiver.Core.Services;
using Castle.Core.Internal;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using inRiver.Integration.Interface;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;
using TheCollective.InRiver.InboundConnector.Infor.Services;

namespace TheCollective.InRiver.InboundConnector.Infor
{
    public class ImportConnector : WorkerConnector, IInboundConnector
    {
        private readonly IImportService _importService;
        private readonly IRelationsService _relationsService;

        private InforConnectorSetting _connectorSettings;

        public ImportConnector()
        {
            var container = new WindsorContainer(new XmlInterpreter());
            _importService = container.Resolve<IImportService>();
            _relationsService = container.Resolve<IRelationsService>();
        }

        public ImportConnector(ICoreService coreService, IImportService importService, IRelationsService relationsService) : base(coreService.ReportRepository, coreService.UtilityRepository)
        {
            _importService = importService;
            _relationsService = relationsService;
        }

        public override void BeforeStart()
        {
            _connectorSettings = new InforConnectorSetting(GetSetting);
            var connectionStringSetting = new ConnectionStringSetting(_connectorSettings.InforOdbcDriver, _connectorSettings.InforOdbcHostname, _connectorSettings.InforOdbcPort, _connectorSettings.InforOdbcDatabase, _connectorSettings.InforOdbcUsername, _connectorSettings.InforOdbcPassword);
            _importService.ConnectionStringSetting = connectionStringSetting;
            _relationsService.ConnectionStringSetting = connectionStringSetting;
        }

        public override void DequeueTask()
        {
            var productImportResult = new ImportResult();
            var relationRebuildResult = new ImportResult();
            try
            {
                ReportRepository.Write(Id, "Product import has started!");

                ValidateRequiredPameters();

                productImportResult = _importService.ImportProducts(Id, new ImportParameters()
                {
                    InforCompanyNumber = _connectorSettings.InforCompanyNumber,
                    ImportCatalogItems = string.IsNullOrEmpty(_connectorSettings.ImportCatalogItems) || (_connectorSettings.ImportCatalogItems?.Equals(bool.TrueString, StringComparison.InvariantCultureIgnoreCase) ?? true),
                    MajorDescriptionSastaCodeIden = _connectorSettings.MajorDescriptionSastaCodeIden,
                    MajorMinorCategoryDescriptionSastaCodeIden = _connectorSettings.MajorMinorCategoryDescriptionSastaCodeIden,
                    ProductCategoryDescriptionSastaCodeIden = _connectorSettings.ProductCategoryDescriptionSastaCodeIden,
                    PCatsFieldSetJsonMapper = _connectorSettings.PCatsFieldSetJsonMapper ?? new Dictionary<string, string>(),
                    GhostWarehouse = _connectorSettings.GhostWarehouse
                });

                relationRebuildResult = _relationsService.RebuildLinks(Id, _connectorSettings.InforCompanyNumber);
            }
            catch (Exception exception)
            {
                ReportRepository.WriteError(Id, exception.ToString());
            }
            finally
            {
                if (!productImportResult.IsSuccess)
                {
                    ReportRepository.WriteError(Id, $"Errors importing: {string.Join(Environment.NewLine, productImportResult.Errors)}");
                }

                if (!relationRebuildResult.IsSuccess)
                {
                    ReportRepository.WriteError(Id, $"Errors rebulding relations: {string.Join(Environment.NewLine, relationRebuildResult.Errors)}");
                }

                ReportRepository.Write(Id, $"Product import is done! {productImportResult.ProductsCreatedCount} created, {productImportResult.ProductsUpdatedCount} updated and {productImportResult.Errors.Count} errors");
            }
        }

        public override Dictionary<string, string> GetDefaultSettings()
        {
            var defaultSettings = base.GetDefaultSettings();

            defaultSettings.Add(nameof(_connectorSettings.InforOdbcDriver), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcHostname), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcPort), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcDatabase), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcUsername), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcPassword), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforCompanyNumber), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.InforOdbcDriver), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.PCatsFieldSetJsonMapper), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.GhostWarehouse), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.MajorDescriptionSastaCodeIden), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.MajorMinorCategoryDescriptionSastaCodeIden), string.Empty);
            defaultSettings.Add(nameof(_connectorSettings.ProductCategoryDescriptionSastaCodeIden), string.Empty);

            return defaultSettings;
        }

        private static void ThrowExecption(string parameterName)
        {
            throw new Exception($"The {parameterName} parameter is required.");
        }

        private void ValidateRequiredPameters()
        {
            if (string.IsNullOrEmpty(_connectorSettings.InforOdbcDriver) || string.IsNullOrEmpty(_connectorSettings.InforOdbcHostname) ||
                string.IsNullOrEmpty(_connectorSettings.InforOdbcPort) || string.IsNullOrEmpty(_connectorSettings.InforOdbcDatabase) ||
                string.IsNullOrEmpty(_connectorSettings.InforOdbcUsername) || string.IsNullOrEmpty(_connectorSettings.InforOdbcPassword))
            {
                throw new Exception("All the ODBC parameters are required.");
            }

            if (_connectorSettings.InforCompanyNumber.IsNullOrEmpty())
            {
                ThrowExecption(nameof(_connectorSettings.InforCompanyNumber));
            }

            if (_connectorSettings.GhostWarehouse.IsNullOrEmpty())
            {
                ThrowExecption(nameof(_connectorSettings.GhostWarehouse));
            }
        }
    }
}
