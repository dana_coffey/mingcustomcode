SELECT UPPER(icsec.prod) as ProductSku,
       UPPER(icsec.altprod) as AlternateProductSku,
       icsec.keyno as LinkIndex,
       CASE WHEN UPPER(icsec.rectype) = 'O' THEN 'RelatedProduct' ELSE 'SupersededProduct' END AS LinkType
FROM pub.icsec icsec
WHERE icsec.cono = {CompanyNumber}
      AND icsec.prod <> ''
      AND icsec.prod IS NOT NULL
      AND icsec.rectype IN ('P', 'O')
WITH(NOLOCK)