SELECT distinct icsec.altprod as ProductSku,
       icsec.prod as Barcode
FROM pub.icsp icsp
JOIN pub.icsec icsec ON icsec.altprod = icsp.prod AND icsec.cono = icsp.cono
JOIN pub.icsw icsw ON (icsp.prod = icsw.prod AND icsw.cono = icsp.cono)
WHERE icsp.cono = {CompanyNumber}
    AND icsec.rectype = 'B'
    AND icsec.prod <> ''
    AND icsec.prod IS NOT NULL
    AND icsw.whse = '{GhostWarehouse}'
    {WhereClause}
WITH(NOLOCK)
