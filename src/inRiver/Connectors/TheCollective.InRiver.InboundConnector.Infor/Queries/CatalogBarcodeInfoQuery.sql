SELECT icsec.altprod as ProductSku,
    icsec.prod as Barcode
FROM pub.icsc icsc 
JOIN pub.icsec icsec ON icsec.altprod = icsc.catalog
WHERE icsec.rectype = 'B'
    AND icsec.prod <> ''
    AND icsec.prod IS NOT NULL
    {WhereClause}
WITH(NOLOCK)
