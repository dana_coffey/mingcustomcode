SELECT icsp.prod as Sku,
    icsw.vendprod as VendorSku,
    icsw.arpvendno as VendorNumber,
    icsw.prodline as ProductLine,
    icsp.descrip as Description,
    icsp.weight as Weight,
    icsp.height as Height,
    icsp.width as Width,
    icsp.length as Length,
    icsp.statustype as Status,
    icsp.prodcat as ProductCategory,
    sasta.codeval as SastaCodeValue,
    sasta.codeiden as SastaCodeIdentifier,
    sasta.descrip as ProductCategoryDescription,
    icsp.user2 as TosCode,
    apsv.name as VendorName,
    icsp.sellmult as RoundBy,
    UCASE(icsp.unitsell) as StockingUnit,
    0 as IsCatalogProduct
FROM pub.icsp icsp
    JOIN pub.icsw icsw ON (icsp.prod = icsw.prod AND icsw.cono = icsp.cono)
    JOIN pub.apsv apsv ON icsw.arpvendno = apsv.vendno AND apsv.cono = icsp.cono
    JOIN (SELECT RIGHT(codeval, 4) AS prodcat,
        MIN(codeiden) AS codeiden,
        MIN(codeval) AS codeval,
        MIN(descrip) AS descrip
    FROM pub.sasta
    WHERE (sasta.exclecomm != 'y')
        AND cono = {CompanyNumber}
        {SastaWhereClause}
    GROUP BY RIGHT(codeval, 4)) AS sasta ON icsp.prodcat = sasta.prodcat
WHERE icsp.cono = {CompanyNumber}
    AND icsw.whse = '{GhostWarehouse}'
    {WhereClause}
WITH(NOLOCK)
