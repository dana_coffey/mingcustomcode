SELECT icsc.catalog as Sku,
    icsc.vendprod as VendorSku,
    icsc.vendno as VendorNumber,
    icsc.descrip as Description,
    icsc.weight as Weight,
    icsc.height as Height,
    icsc.width as Width,
    icsc.length as Length,
    icsc.prodcat as ProductCategory,
    icsc.prodline as ProductLine,
    sasta.codeval as SastaCodeValue,
    sasta.codeiden as SastaCodeIdentifier,
    sasta.descrip as ProductCategoryDescription,
    apsv.name as VendorName,
    '' as TosCode,
    0 as RoundBy,
    icsc.unitstock as StockingUnit,
    1 as IsCatalogProduct,
	Superseded.Status
FROM pub.icsc icsc
    JOIN pub.apsv apsv ON icsc.vendno = apsv.vendno AND apsv.cono = {CompanyNumber}
    JOIN (SELECT RIGHT(codeval, 4) AS prodcat,
			MIN(codeiden) AS codeiden,
			MIN(codeval) AS codeval,
			MIN(descrip) AS descrip
		FROM pub.sasta
		WHERE (sasta.exclecomm != 'y')
			AND cono = {CompanyNumber}
			{SastaWhereClause}
		GROUP BY RIGHT(codeval, 4)) AS sasta ON icsc.prodcat = sasta.prodcat
	LEFT JOIN (SELECT 'A' AS Status, 
                icsec.prod 
        FROM pub.icsec 
        WHERE icsec.rectype = 'P' AND icsec.cono = {CompanyNumber}
        GROUP BY icsec.prod 
        HAVING Count(icsec.prod) > 0) Superseded ON Superseded.prod = icsc.catalog
WHERE 1 = 1
    {WhereClause}
WITH(NOLOCK)
