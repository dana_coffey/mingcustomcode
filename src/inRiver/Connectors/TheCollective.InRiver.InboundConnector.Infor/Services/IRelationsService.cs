﻿using TheCollective.InRiver.InboundConnector.Infor.Models;

namespace TheCollective.InRiver.InboundConnector.Infor.Services
{
    public interface IRelationsService
    {
        ConnectionStringSetting ConnectionStringSetting { get; set; }

        ImportResult RebuildLinks(string connectorId, string inforCompanyNumber);
    }
}
