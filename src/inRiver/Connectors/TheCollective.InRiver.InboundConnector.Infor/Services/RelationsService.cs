﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.Core.LinkType;
using TheCollective.InRiver.InboundConnector.Infor.Enums;
using TheCollective.InRiver.InboundConnector.Infor.Helpers;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Repositories;
using InRiverModels = TheCollective.InRiver.InboundConnector.Infor.Models.Entities;

namespace TheCollective.InRiver.InboundConnector.Infor.Services
{
    public class RelationsService : IRelationsService
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly ICoreService _coreService;

        public ConnectionStringSetting ConnectionStringSetting { get; set; }

        public RelationsService(ICoreService coreService, ICatalogRepository catalogRepository)
        {
            _coreService = coreService;
            _catalogRepository = catalogRepository;
        }

        public ImportResult RebuildLinks(string connectorId, string inforCompanyNumber)
        {
            var importResult = new ImportResult();
            var linkProcessedCount = 0;
            var itemsEntityCache = new Dictionary<string, Entity>();
            var productsEntityCache = new Dictionary<string, Entity>();
            var supersededLinkType = _coreService.ModelRepository.GetLinkType(nameof(ItemItemSuperseded));
            var relatedProductLinkType = _coreService.ModelRepository.GetLinkType(nameof(ProductProductRelatedProduct));

            _coreService.ReportRepository.Write(connectorId, "Getting all required data to manage products links...");
            var existingSupersededItemsLinks = _coreService.DataRepository.GetAllLinksForLinkType(nameof(ItemItemSuperseded));

            _coreService.ReportRepository.Write(connectorId, "Getting all product and items fields skus...");
            var itemSkuFields = _coreService.DataRepository.GetAllFieldsByFieldType(nameof(InRiverModels.Item.ItemSkuKey)).Select(x => new EntitySkuModel
            {
                EntityId = x.EntityId,
                Sku = x.Data?.ToString()
            }).ToList();

            var productSku = _coreService.DataRepository.GetAllFieldsByFieldType(nameof(InRiverModels.Product.ProductSkuKey)).Select(x => new EntitySkuModel
            {
                EntityId = x.EntityId,
                Sku = x.Data?.ToString()
            }).ToList();

            var erpLinks = _catalogRepository.GetRelations(ConnectionStringSetting, inforCompanyNumber).ToList();
            _coreService.ReportRepository.Write(connectorId, $"Manage related products started ({erpLinks.Count} to process).");
            var linksToProcess = erpLinks.Where(erpLink => !erpLink.ProductSku.Equals(erpLink.AlternateProductSku) && productSku.Any(entitySku => erpLink.ProductSku?.ToUpper() == entitySku.Sku) && productSku.Any(entitySku => erpLink.AlternateProductSku?.ToUpper() == entitySku.Sku)).GroupBy(x => x.ProductSku).ToList();
            _coreService.ReportRepository.Write(connectorId, $"{linksToProcess.Count} links found in PIM.");

            foreach (var links in linksToProcess)
            {
                try
                {
                    linkProcessedCount++;
                    var productEntity = EntityHelper.GetProductEntityBySku(_coreService, links.Key, LoadLevel.DataAndLinks);
                    var existingLinks = productEntity.Links.Where(x => x.LinkType.Id == nameof(ProductProductRelatedProduct)).Select(x => new LinkReference
                    {
                        LinkType = x.LinkType,
                        Index = x.Index,
                        Id = x.Id,
                        TargetSku = productSku.FirstOrDefault(sku => x.Target.Id.Equals(sku.EntityId))?.Sku,
                        SourceSku = productSku.FirstOrDefault(sku => x.Source.Id.Equals(sku.EntityId))?.Sku,
                    }).ToList();

                    existingLinks.AddRange(GetExistingProductLinksByItem(itemSkuFields, existingSupersededItemsLinks, links.Key));
                    var newErpLinks = links.OrderBy(x => x.LinkIndex).ToList();

                    // get item not in list...
                    var linksToDelete = GetLinksToDelete(existingLinks, newErpLinks);
                    if (linksToDelete.Count > 0)
                    {
                        _coreService.DataRepository.DeleteLinks(linksToDelete);
                    }

                    // Links to create or update
                    foreach (var newErpLink in newErpLinks)
                    {
                        var zeroBaseLinkOrder = (int)newErpLink.LinkIndex - 1;
                        var link = existingLinks.FirstOrDefault(x => string.Equals(x.TargetSku, newErpLink.AlternateProductSku, StringComparison.InvariantCultureIgnoreCase));

                        if (link != null)
                        {
                            if (link.Index != zeroBaseLinkOrder)
                            {
                                _coreService.DataRepository.UpdateLinkSortOrder(link.Id, zeroBaseLinkOrder);
                            }
                        }
                        else
                        {
                            (Entity sourceEntity, Entity targetEntity) entities;

                            if (newErpLink.LinkType == ErpLinkType.RelatedProduct)
                            {
                                entities = GetEntitiesForLink(productsEntityCache, links, newErpLink, sku => EntityHelper.GetProductEntityBySku(_coreService, sku, LoadLevel.Shallow));
                            }
                            else
                            {
                                entities = GetEntitiesForLink(itemsEntityCache, links, newErpLink, sku => EntityHelper.GetItemEntityBySku(_coreService, sku, LoadLevel.Shallow));
                            }

                            if (entities.sourceEntity != null && entities.targetEntity != null)
                            {
                                _coreService.DataRepository.AddLink(new Link
                                {
                                    Source = entities.sourceEntity,
                                    Target = entities.targetEntity,
                                    Index = zeroBaseLinkOrder,
                                    LinkType = newErpLink.LinkType == ErpLinkType.RelatedProduct ? relatedProductLinkType : supersededLinkType,
                                });

                                _coreService.ReportRepository.Write(connectorId, $"Product : {links.Key} - Creating new {newErpLink.LinkType.ToString()} product link To: {newErpLink.AlternateProductSku}");
                            }
                            else
                            {
                                _coreService.ReportRepository.Write(connectorId, $"Product : {links.Key} - Imposible to create link between {newErpLink.ProductSku} and : {newErpLink.AlternateProductSku} be sure the Product Item relation exist before.");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    importResult.ErrorSkus.Add(links.Key);
                    importResult.Errors.Add($"Error updating link information for product with SKU '{links.Key}' - {e}");
                }

                if (linkProcessedCount % Connectors.Constants.ConnectorSettings.BatchSizeForProcessLog == 0)
                {
                    _coreService.ReportRepository.Write(connectorId, $"'{linkProcessedCount}' of '{linksToProcess.Count}' links products have been processed.");
                }
            }

            _coreService.ReportRepository.Write(connectorId, "Manage products links ended");
            return importResult;
        }

        private static IEnumerable<LinkReference> GetExistingProductLinksByItem(IEnumerable<EntitySkuModel> items, List<Link> existingLinksToFilter, string linkKey)
        {
            var existingLinks = new List<LinkReference>();

            foreach (var itemLink in items.Where(x => x.Sku.Equals(linkKey)))
            {
                existingLinks.AddRange(existingLinksToFilter.Where(x => x.Source.Id.Equals(itemLink.EntityId)).Select(link => new LinkReference
                {
                    SourceSku = link.Source?.DisplayName?.Data?.ToString(),
                    TargetSku = link.Target?.DisplayName?.Data?.ToString(),
                    Id = link.Id,
                    Index = link.Index,
                    LinkType = link.LinkType,
                }).ToList());
            }

            return existingLinks;
        }

        private static List<int> GetLinksToDelete(List<LinkReference> existingLinks, List<ErpLink> newErpLinks)
        {
            return existingLinks.Where(link => newErpLinks.All(newLink => !string.Equals(newLink.AlternateProductSku, link.TargetSku, StringComparison.InvariantCultureIgnoreCase))).Select(x => x.Id).ToList();
        }

        private (Entity sourceEntity, Entity targetEntity) GetEntitiesForLink(Dictionary<string, Entity> cacheEntities, IGrouping<string, ErpLink> links, SxeCrossReferenceRecord erpLink, Func<string, Entity> getEntityBySkuFunc)
        {
            (Entity sourceEntity, Entity targetEntity) result;
            var sourceSku = links.Key.ToUpper();
            var targetSku = erpLink.AlternateProductSku.ToUpper();

            if (cacheEntities.ContainsKey(sourceSku))
            {
                result.sourceEntity = cacheEntities[sourceSku];
            }
            else
            {
                result.sourceEntity = getEntityBySkuFunc(sourceSku);
                cacheEntities.Add(sourceSku, result.sourceEntity);
            }

            if (cacheEntities.ContainsKey(targetSku))
            {
                result.targetEntity = cacheEntities[targetSku];
            }
            else
            {
                result.targetEntity = getEntityBySkuFunc(targetSku);
                cacheEntities.Add(targetSku, result.targetEntity);
            }

            return result;
        }
    }
}
