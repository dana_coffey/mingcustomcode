﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Core.Repositories;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.Core.LinkType;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;
using TheCollective.InRiver.InboundConnector.Infor.Repositories;
using Product = TheCollective.InRiver.InboundConnector.Infor.Models.Entities.Product;

namespace TheCollective.InRiver.InboundConnector.Infor.Services
{
    public class SxeRelationsService
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly IDataRepository _dataRepository;
        private readonly string _inforConnectorName;
        private readonly IModelRepository _modelRepository;
        private readonly IUtilityRepository _utilityRepository;

        public SxeRelationsService(IDataRepository dataRepository, IUtilityRepository utilityRepository, IModelRepository modelRepository, string inforConnectorName)
        {
            _dataRepository = dataRepository;
            _utilityRepository = utilityRepository;
            _modelRepository = modelRepository;
            _inforConnectorName = inforConnectorName;

            _catalogRepository = new CatalogRepository();
        }

        /// <summary>
        /// Import all existing and matching relations between sku from Sx.e. Output import result on disk
        /// </summary>
        /// <param name="sxeRelationsImportResultFilePath">Path of where result will be output as txt files</param>
        /// /// <param name="writeLog">Method to write logs</param>
        /// <returns></returns>
        public void ImportAllRelations(string sxeRelationsImportResultFilePath, Action<string> writeLog)
        {
            var productProductRelatedProductLinkType = _modelRepository.GetLinkType(nameof(ProductProductRelatedProduct));

            var allRelatedProducts = GetRelatedProducts();

            writeLog($"{allRelatedProducts.Count} relations found");

            var skuFields = _dataRepository.GetAllFieldsByFieldType(nameof(Product.ProductSkuKey));
            var skus = skuFields.Select(x => x.Data?.ToString()).ToList();

            var relatedProducts = allRelatedProducts.Where(x => x.ProductSku != x.AlternateProductSku && skus.Any(y => x.ProductSku?.ToUpper() == y) && skus.Any(y => x.AlternateProductSku?.ToUpper() == y)).ToList();

            writeLog($"{relatedProducts.Count} match found in the PIM");

            var index = 0;

            var linkedProducts = new List<ErpLink>();
            var notLinkedProducts = allRelatedProducts.Except(relatedProducts).ToList();
            var errorProducts = new List<ErpLink>();

            foreach (var relatedProduct in relatedProducts)
            {
                writeLog($"{index}/{relatedProducts.Count} - Processing");

                try
                {
                    var productEntitySource = _dataRepository.GetEntityByUniqueValue(nameof(Product.ProductSkuKey), relatedProduct.ProductSku?.ToUpper(), LoadLevel.Shallow);
                    var productEntityTarget = _dataRepository.GetEntityByUniqueValue(nameof(Product.ProductSkuKey), relatedProduct.AlternateProductSku?.ToUpper(), LoadLevel.Shallow);

                    if (productEntitySource != null && productEntityTarget != null
                        && productEntitySource.Id != productEntityTarget.Id)
                    {
                        _dataRepository.CreateLinkIfNecessary(productEntitySource, productEntityTarget, productProductRelatedProductLinkType);

                        linkedProducts.Add(relatedProduct);

                        writeLog($"Link added between sku {relatedProduct.ProductSku} & {relatedProduct.AlternateProductSku}");
                    }
                    else
                    {
                        notLinkedProducts.Add(relatedProduct);
                        writeLog($"Link not added between sku {relatedProduct.ProductSku} ({(productEntitySource != null ? "found" : "not found")}) & {relatedProduct.AlternateProductSku} ({(productEntityTarget != null ? "found" : "not found")})");
                    }
                }
                catch (Exception e)
                {
                    errorProducts.Add(relatedProduct);
                    writeLog($"Link not added between sku {relatedProduct.ProductSku} & {relatedProduct.AlternateProductSku}. Error: {e}");
                }

                index++;
            }

            WriteResult(linkedProducts, sxeRelationsImportResultFilePath, "linkedProducts.txt");
            WriteResult(notLinkedProducts, sxeRelationsImportResultFilePath, "notLinkedProducts.txt");
            WriteResult(errorProducts, sxeRelationsImportResultFilePath, "errorProducts.txt");
        }

        private static void WriteResult(List<ErpLink> relatedProducts, string sxeRelationsImportResultFilePath, string fileName)
        {
            var fileNameAndPath = sxeRelationsImportResultFilePath + fileName;

            var lines = relatedProducts.Select(x => x.ProductSku + ", " + x.AlternateProductSku).ToList();

            System.IO.File.WriteAllLines(fileNameAndPath, lines);
        }

        private List<ErpLink> GetRelatedProducts()
        {
            var connecterSettings = _utilityRepository.GetConnectorSettings(_inforConnectorName);

            var connectionStringSetting = new ConnectionStringSetting(
                connecterSettings[nameof(InforConnectorSetting.InforOdbcDriver)],
                connecterSettings[nameof(InforConnectorSetting.InforOdbcHostname)],
                connecterSettings[nameof(InforConnectorSetting.InforOdbcPort)],
                connecterSettings[nameof(InforConnectorSetting.InforOdbcDatabase)],
                connecterSettings[nameof(InforConnectorSetting.InforOdbcUsername)],
                connecterSettings[nameof(InforConnectorSetting.InforOdbcPassword)]);

            var relatedProducts = _catalogRepository.GetRelations(connectionStringSetting, connecterSettings[nameof(InforConnectorSetting.InforCompanyNumber)]).ToList();
            return relatedProducts;
        }
    }
}
