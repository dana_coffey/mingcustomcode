﻿using System.Collections.Generic;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;

namespace TheCollective.InRiver.InboundConnector.Infor.Services
{
    public interface IImportService
    {
        ConnectionStringSetting ConnectionStringSetting { get; set; }

        Entity CreateOrUpdateItem(Product product, List<BarcodeInformation> productBarcodes, ImportParameters importParameters, ImportResult importResult, string connectorId);

        ImportResult ImportProducts(string connectorId, ImportParameters importParameters);
    }
}
