﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Absolunet.InRiver.Core.Models.Links;
using Absolunet.InRiver.Core.Services;
using Castle.Core.Internal;
using inRiver.Remoting.Objects;
using TheCollective.InRiver.Core.CVL;
using TheCollective.InRiver.Core.Services;
using TheCollective.InRiver.InboundConnector.Infor.Helpers;
using TheCollective.InRiver.InboundConnector.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models.EqualityComparer;
using TheCollective.InRiver.InboundConnector.Infor.Models.Settings;
using TheCollective.InRiver.InboundConnector.Infor.Repositories;
using InRiverModels = TheCollective.InRiver.InboundConnector.Infor.Models.Entities;
using StringExtensions = TheCollective.InRiver.Core.Helpers.StringExtensions;
using TCCore = TheCollective.InRiver.Core;
using TCEntities = TheCollective.InRiver.Core.Entities;

namespace TheCollective.InRiver.InboundConnector.Infor.Services
{
    public class ImportService : IImportService
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly ICoreService _coreService;
        private readonly ICvlService _cvlService;

        public ConnectionStringSetting ConnectionStringSetting { get; set; }

        public ImportService(ICoreService coreService, ICatalogRepository catalogRepository, ICvlService cvlService)
        {
            _coreService = coreService;
            _catalogRepository = catalogRepository;
            _cvlService = cvlService;
        }

        public Entity CreateOrUpdateItem(Product product, List<BarcodeInformation> productBarcodes, ImportParameters importParameters, ImportResult importResult, string connectorId)
        {
            var itemEntity = EntityHelper.GetItemEntityBySku(_coreService, product.Sku);
            if (itemEntity == null)
            {
                _coreService.ReportRepository.Write(connectorId, $"Creating item with SKU '{product.Sku}' started");
                itemEntity = _coreService.DataRepository.CreateEntity(_coreService.ModelRepository.GetEntityType(nameof(InRiverModels.Item)));

                _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemSkuKey), product.Sku.ToUpper());
                _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.AutoEnrichmentStatus), TCCore.Constants.FieldTypes.ImportItem.Ready);

                importResult.ProductsCreatedCount++;
            }
            else
            {
                _coreService.ReportRepository.Write(connectorId, $"Updating item with SKU '{product.Sku}' started");
                importResult.ProductsUpdatedCount++;
            }

            var productStatus = product.Status.ToUpper();

            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemSku), product.Sku);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemUrn), product.VendorSku);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPDescription), (product.Description ?? string.Empty).RemoveInforSpecialCharacters());
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPStatus), product.Status.ToUpper());
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemIsCatalogProduct), product.IsCatalogProduct);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPVendorNo), product.VendorNumber);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemLastModifiedTime), DateTime.UtcNow);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPCategory), product.ProductCategory);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemTosCode), product.TosCode);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPRoundBy), product.RoundBy > 0 ? product.RoundBy : 0);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPStockingUnit), _coreService.ModelRepository.GetCVLValueByKey(product.StockingUnit.ToUpper(CultureInfo.InvariantCulture), nameof(ERPStockingUnit))?.Key.ToUpper(CultureInfo.InvariantCulture) ?? ERPStockingUnit.Each);
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemERPCategoryDescription), (product.ProductCategoryDescription ?? string.Empty).RemoveInforSpecialCharacters());
            _coreService.SetFieldValue(itemEntity, nameof(InRiverModels.Item.ItemUpc), string.Join(";", productBarcodes.Select(x => x.Barcode.Trim())));

            // Update vendor number CVL
            _cvlService.CreateCvlValueWhenMissing(nameof(ERPVendorNo), product.VendorNumber, product.VendorName ?? string.Empty);

            itemEntity = _coreService.DataRepository.AddOrUpdateEntity(itemEntity);

            importParameters.PCatsFieldSetJsonMapper.TryGetValue(product.ProductCategory, out var fieldSetValue);

            if (!fieldSetValue.IsNullOrEmpty() && FieldSetExists(fieldSetValue, connectorId))
            {
                _coreService.DataRepository.SetEntityFieldSet(itemEntity.Id, fieldSetValue);
            }

            _coreService.DataRepository.AddOrUpdateEntity(itemEntity);
            _coreService.ReportRepository.Write(connectorId, $"Creating or updating item with SKU '{product.Sku}' ended");

            return itemEntity;
        }

        public ImportResult ImportProducts(string connectorId, ImportParameters importParameters)
        {
            var updateDate = DateTime.Now;
            var lastExecutionDate = GetLastExecutionDate();
            var importResult = new ImportResult();

            _coreService.ReportRepository.Write(connectorId, $"Last execution date : {lastExecutionDate}");
            _coreService.ReportRepository.Write(connectorId, $"{nameof(ImportProducts)} > Create or update entities.");
            CreateOrUpdateEntities(connectorId, importParameters, lastExecutionDate, importResult);

            SetSkuImportErrorsList(importResult.ErrorSkus.ToArray());
            SetLastExecutionDate(updateDate);

            return importResult;
        }

        private void CreateOrUpdateEntities(string connectorId, ImportParameters importParameters, DateTime lastExecutionDate, ImportResult importResult)
        {
            var importData = GetDataToImport(lastExecutionDate, importParameters, connectorId);
            _coreService.ReportRepository.Write(connectorId, $"{importData.Products.Count} products returned by SX.e");

            importResult.ProductsUpdatedCount = 0;
            importResult.ProductsCreatedCount = 0;

            var productItemLinkType = _coreService.ModelRepository.GetLinkType(nameof(ProductItem));
            var productProcessedCount = 0;

            foreach (var product in importData.Products)
            {
                try
                {
                    productProcessedCount++;
                    var productEntity = CreateOrUpdateProduct(product, importParameters, connectorId);
                    var itemEntity = CreateOrUpdateItem(product, importData.Barcodes.Where(x => string.Equals(x.ProductSku, product.Sku)).ToList(), importParameters, importResult, connectorId);

                    _coreService.DataRepository.CreateLinkIfNecessary(productEntity, itemEntity, productItemLinkType);
                }
                catch (Exception e)
                {
                    _coreService.ReportRepository.WriteException(connectorId, $"import product with SKU '{product.Sku}' in error.", e);
                    importResult.ErrorSkus.Add(product.Sku);
                    importResult.Errors.Add($"Error updating product with SKU '{product.Sku}' - {e}");
                }

                if (productProcessedCount % Connectors.Constants.ConnectorSettings.BatchSizeForProcessLog == 0)
                {
                    _coreService.ReportRepository.Write(connectorId, $"'{productProcessedCount}' of '{importData.Products.Count}' products have been processed.");
                }
            }
        }

        private Entity CreateOrUpdateProduct(Product product, ImportParameters parameters, string connectorId)
        {
            var msg = $"Updating product with SKU '{product.Sku}' started";
            var productEntity = EntityHelper.GetProductEntityBySku(_coreService, product.Sku);

            if (productEntity == null)
            {
                msg = $"Creating product with SKU '{product.Sku}' started";
                productEntity = _coreService.DataRepository.CreateEntity(_coreService.ModelRepository.GetEntityType(nameof(InRiverModels.Product)));
                _coreService.SetFieldValue(productEntity, nameof(InRiverModels.Product.ProductName), StringExtensions.ToLocaleString((product.Description ?? string.Empty).RemoveInforSpecialCharacters()));
                _coreService.SetFieldValue(productEntity, nameof(InRiverModels.Product.ProductSkuKey), product.Sku.ToUpper());
                _coreService.SetFieldValue(productEntity, nameof(InRiverModels.Product.ProductSku), product.Sku);
            }

            _coreService.ReportRepository.Write(connectorId, msg);

            ManageMajorMinorCodes(productEntity, product, parameters);

            parameters.PCatsFieldSetJsonMapper.TryGetValue(product.ProductCategory, out var fieldSetValue);

            if (!fieldSetValue.IsNullOrEmpty() && FieldSetExists(fieldSetValue, connectorId))
            {
                _coreService.SetFieldValue(productEntity, nameof(ProductCategory), fieldSetValue);
            }

            productEntity = _coreService.DataRepository.AddOrUpdateEntity(productEntity);

            _coreService.ReportRepository.Write(connectorId, $"Creating or updating product with SKU '{product.Sku}' ended");

            return productEntity;
        }

        private bool FieldSetExists(string fieldSetValue, string connectorId)
        {
            var fieldSet = _coreService.ModelRepository.GetFieldSet(fieldSetValue);

            if (fieldSet != null)
            {
                return true;
            }

            _coreService.ReportRepository.WriteError(connectorId, $"The fieldSet '{fieldSetValue}' could not be found in the model tool.");
            return false;
        }

        private ImportData GetDataToImport(DateTime lastExecutionDate, ImportParameters parameters, string connectorId)
        {
            var result = new ImportData();

            var skusInErrorFromPreviousImport = GetSkuImportErrorsList();
            _coreService.ReportRepository.Write(connectorId, $"There are {skusInErrorFromPreviousImport.Length} skus in errors from previous import.");

            var updatedErpProducts = _catalogRepository.GetUpdatedProducts(ConnectionStringSetting, parameters, lastExecutionDate).ToList();
            _coreService.ReportRepository.Write(connectorId, $"There are {updatedErpProducts.Count} products returned by the ERP.");

            var productsInError = _catalogRepository.GetProducts(ConnectionStringSetting, parameters, skusInErrorFromPreviousImport).ToList();
            var productsToImport = updatedErpProducts.Concat(productsInError);
            _coreService.ReportRepository.Write(connectorId, $"Getting {productsInError.Count} products returned by the ERP that were in error on last import.");

            _coreService.ReportRepository.Write(connectorId, "Getting barcode data for products");
            var barcodes = _catalogRepository.GetProductBarcodesByDate(ConnectionStringSetting, parameters, lastExecutionDate).ToList();
            barcodes.AddRange(_catalogRepository.GetProductBarcodes(ConnectionStringSetting, parameters, skusInErrorFromPreviousImport).ToList());

            if (parameters.ImportCatalogItems)
            {
                var updatedErpCatalog = _catalogRepository.GetUpdatedCatalog(ConnectionStringSetting, parameters, lastExecutionDate).ToList();
                productsToImport = productsToImport.Concat(updatedErpCatalog);
                _coreService.ReportRepository.Write(connectorId, $"There are {updatedErpCatalog.Count} catalog products returned by the ERP.");

                var catalogsInError = _catalogRepository.GetCatalogs(ConnectionStringSetting, parameters, skusInErrorFromPreviousImport).ToList();
                productsToImport = productsToImport.Concat(catalogsInError);
                _coreService.ReportRepository.Write(connectorId, $"Getting {catalogsInError.Count} catalog products returned by the ERP that were in error on last import.");

                _coreService.ReportRepository.Write(connectorId, "Getting barcode data for catalog items");
                barcodes.AddRange(_catalogRepository.GetCatalogBarcodesByDate(ConnectionStringSetting, lastExecutionDate).ToList());
                barcodes.AddRange(_catalogRepository.GetCatalogBarcodes(ConnectionStringSetting, skusInErrorFromPreviousImport).ToList());
            }

            result.Products = productsToImport.Distinct(new ProductEqualityComparer()).ToList();
            result.Barcodes = barcodes.Distinct(new BarcodeInformationEqualityComparer()).ToList();

            return result;
        }

        private DateTime GetLastExecutionDate()
        {
            var lastDate = _coreService.UtilityRepository.GetServerSetting(Constants.Constants.InRiver.ServerSettings.InforInboundConnectorLastExecutionDate);
            var result = lastDate.IsNullOrEmpty() ? DateTime.MinValue : Convert.ToDateTime(lastDate);

            return result;
        }

        private string[] GetSkuImportErrorsList()
        {
            var listAsString = _coreService.UtilityRepository.GetServerSetting(Constants.Constants.InRiver.ServerSettings.InforInboundConnectorSkuErrors);

            return listAsString.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
        }

        private void ManageMajorMinorCodes(Entity productEntity, Product product, ImportParameters parameters)
        {
            // Get Major/Minor codes. Format MMmmCCCC
            if (!string.IsNullOrEmpty(product.SastaCodeIdentifier) && !string.IsNullOrEmpty(product.SastaCodeValue))
            {
                var productCategory = string.IsNullOrEmpty(product.ProductCategory) ? string.Empty : product.ProductCategory.ToUpper();
                _coreService.SetFieldValue(productEntity, nameof(InRiverModels.Product.ProductERPCategoryCode), productCategory);

                if (product.SastaCodeValue.Length == 8)
                {
                    var major = product.SastaCodeValue.Substring(0, 2);
                    var minor = product.SastaCodeValue.Substring(2, 2);

                    _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductERPMajorCode), major);
                    _coreService.SetFieldValue(productEntity, nameof(InRiverModels.Product.ProductERPMinorCode), minor);
                }

                if (!string.IsNullOrEmpty(parameters.MajorDescriptionSastaCodeIden) &&
                    product.SastaCodeIdentifier.Equals(parameters.MajorDescriptionSastaCodeIden,
                        StringComparison.InvariantCultureIgnoreCase))
                {
                    _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductERPMajorDescription),
                        product.ProductCategoryDescription);
                }

                if (!string.IsNullOrEmpty(parameters.MajorMinorCategoryDescriptionSastaCodeIden) &&
                    product.SastaCodeIdentifier.Equals(parameters.MajorMinorCategoryDescriptionSastaCodeIden,
                        StringComparison.InvariantCultureIgnoreCase))
                {
                    _coreService.SetFieldValue(productEntity,
                        nameof(TCEntities.Product.ProductERPMajorMinorCategoryDescription),
                        product.ProductCategoryDescription);
                }

                if (!string.IsNullOrEmpty(parameters.ProductCategoryDescriptionSastaCodeIden) &&
                    product.SastaCodeIdentifier.Equals(parameters.ProductCategoryDescriptionSastaCodeIden, StringComparison.InvariantCultureIgnoreCase))
                {
                    _coreService.SetFieldValue(productEntity, nameof(TCEntities.Product.ProductERPCategoryDescription), product.ProductCategoryDescription);
                }
            }
        }

        private void SetLastExecutionDate(DateTime datetime)
        {
            _coreService.UtilityRepository.SetServerSetting(Constants.Constants.InRiver.ServerSettings.InforInboundConnectorLastExecutionDate, datetime.ToString(CultureInfo.InvariantCulture));
        }

        private void SetSkuImportErrorsList(params string[] skus)
        {
            var skuListAsString = string.Join(",", skus.Distinct().ToArray());

            _coreService.UtilityRepository.SetServerSetting(Constants.Constants.InRiver.ServerSettings.InforInboundConnectorSkuErrors, skuListAsString);
        }
    }
}
