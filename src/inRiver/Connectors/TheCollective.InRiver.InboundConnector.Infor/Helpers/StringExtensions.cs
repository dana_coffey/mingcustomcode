﻿using System;

namespace TheCollective.InRiver.InboundConnector.Infor.Helpers
{
    public static class StringExtensions
    {
        public static string RemoveInforSpecialCharacters(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            var endOfLineIndex = value.IndexOf("\n", StringComparison.Ordinal);
            if (endOfLineIndex > 0)
            {
                value = value.Substring(0, endOfLineIndex);
            }

            var firstIndex = value.IndexOf("~;;", StringComparison.Ordinal);
            if (firstIndex > 0 && value.EndsWith("~;"))
            {
                value = value.Substring(0, firstIndex);
            }

            value = value.Replace("~;;", " ");
            value = value.Replace(";", " ");

            value = value.Trim(' ');

            return value;
        }
    }
}
