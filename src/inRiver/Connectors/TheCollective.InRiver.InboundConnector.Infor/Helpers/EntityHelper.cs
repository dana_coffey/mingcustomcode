﻿using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;
using InRiverModels = TheCollective.InRiver.InboundConnector.Infor.Models.Entities;

namespace TheCollective.InRiver.InboundConnector.Infor.Helpers
{
    public static class EntityHelper
    {
        public static Entity GetItemEntityBySku(ICoreService coreService, string sku, LoadLevel level = LoadLevel.DataOnly)
        {
            return coreService.DataRepository.GetEntityByUniqueValue(nameof(InRiverModels.Item.ItemSkuKey), sku.ToUpper(), level);
        }

        public static Entity GetProductEntityBySku(ICoreService coreService, string sku, LoadLevel level = LoadLevel.DataOnly)
        {
            return coreService.DataRepository.GetEntityByUniqueValue(nameof(InRiverModels.Product.ProductSkuKey), sku.ToUpper(), level);
        }
    }
}
