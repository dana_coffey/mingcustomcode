﻿using TheCollective.InRiver.InboundConnector.Infor.Models;

namespace TheCollective.InRiver.InboundConnector.Infor.Helpers
{
    public static class ConnectionHelper
    {
        public static string GetInforSxeConnectionString(ConnectionStringSetting connectionStringSetting)
        {
            return $"DRIVER={connectionStringSetting.Driver};HOST={connectionStringSetting.Host};PORT={connectionStringSetting.Port};DB={connectionStringSetting.Database};UID={connectionStringSetting.Username};PWD={connectionStringSetting.Password};";
        }
    }
}
