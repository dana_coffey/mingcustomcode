﻿using System.Configuration;

namespace TheCollective.InRiver.InboundConnector.Infor.Helpers
{
    public static class ConfigHelper
    {
        public static string Get(string name)
        {
            return Get(name, string.Empty);
        }

        public static string Get(string name, string defaultTo)
        {
            return ConfigurationManager.AppSettings.Get(name) ?? defaultTo;
        }
    }
}
