﻿using System;
using System.Collections.Generic;
using Absolunet.InRiver.Core.Connectors;
using Newtonsoft.Json;

namespace TheCollective.InRiver.InboundConnector.Infor.Models.Settings
{
    public class InforConnectorSetting : WorkerConnectorSettings
    {
        public string GhostWarehouse => GetSetting(nameof(GhostWarehouse));
        public string ImportCatalogItems => GetSetting(nameof(ImportCatalogItems));
        public string InforCompanyNumber => GetSetting(nameof(InforCompanyNumber));
        public string InforOdbcDatabase => GetSetting(nameof(InforOdbcDatabase));
        public string InforOdbcDriver => GetSetting(nameof(InforOdbcDriver));
        public string InforOdbcHostname => GetSetting(nameof(InforOdbcHostname));
        public string InforOdbcPassword => GetSetting(nameof(InforOdbcPassword));
        public string InforOdbcPort => GetSetting(nameof(InforOdbcPort));
        public string InforOdbcUsername => GetSetting(nameof(InforOdbcUsername));
        public string MajorDescriptionSastaCodeIden => GetSetting(nameof(MajorDescriptionSastaCodeIden));
        public string MajorMinorCategoryDescriptionSastaCodeIden => GetSetting(nameof(MajorMinorCategoryDescriptionSastaCodeIden));
        public Dictionary<string, string> PCatsFieldSetJsonMapper => JsonConvert.DeserializeObject<Dictionary<string, string>>(GetSetting(nameof(PCatsFieldSetJsonMapper)));
        public string ProductCategoryDescriptionSastaCodeIden => GetSetting(nameof(ProductCategoryDescriptionSastaCodeIden));

        public InforConnectorSetting(Func<string, string> getSetting) : base(getSetting)
        {
        }
    }
}
