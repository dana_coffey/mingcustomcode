﻿using System.Collections.Generic;

namespace TheCollective.InRiver.InboundConnector.Infor.Models.Settings
{
    public class ImportParameters
    {
        public string GhostWarehouse { get; set; }
        public bool ImportCatalogItems { get; set; }
        public string InforCompanyNumber { get; set; }
        public string MajorDescriptionSastaCodeIden { get; set; }
        public string MajorMinorCategoryDescriptionSastaCodeIden { get; set; }
        public Dictionary<string, string> PCatsFieldSetJsonMapper { get; set; }
        public string ProductCategoryDescriptionSastaCodeIden { get; set; }
    }
}
