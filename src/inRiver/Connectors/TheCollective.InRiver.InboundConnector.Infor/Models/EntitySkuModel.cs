﻿using TheCollective.InRiver.InboundConnector.Infor.Enums;

namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class EntitySkuModel
    {
        public int EntityId { get; set; }
        public string Sku { get; set; }
    }
}
