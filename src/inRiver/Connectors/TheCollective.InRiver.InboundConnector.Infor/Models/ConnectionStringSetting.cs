﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class ConnectionStringSetting
    {
        public string Database { get; }
        public string Driver { get; }
        public string Host { get; }
        public string Password { get; }
        public string Port { get; }
        public string Username { get; }

        public ConnectionStringSetting(string driver, string host, string port, string database, string username, string password)
        {
            Driver = driver;
            Host = host;
            Port = port;
            Database = database;
            Username = username;
            Password = password;
        }
    }
}
