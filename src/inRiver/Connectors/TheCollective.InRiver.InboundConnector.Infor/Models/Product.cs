﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class Product
    {
        public string Description { get; set; }
        public string Height { get; set; }
        public bool IsCatalogProduct { get; set; }
        public string Length { get; set; }
        public string ProductCategory { get; set; }
        public string ProductCategoryDescription { get; set; }
        public string ProductLine { get; set; }
        public int RoundBy { get; set; }
        public string SastaCodeIdentifier { get; set; }
        public string SastaCodeValue { get; set; }
        public string Sku { get; set; }
        public string Status { get; set; }
        public string StockingUnit { get; set; }
        public string TosCode { get; set; }
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
        public string VendorSku { get; set; }
        public string Weight { get; set; }
        public string Width { get; set; }
    }
}
