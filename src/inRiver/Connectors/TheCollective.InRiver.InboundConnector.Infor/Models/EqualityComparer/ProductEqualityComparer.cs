﻿using System;
using System.Collections.Generic;

namespace TheCollective.InRiver.InboundConnector.Infor.Models.EqualityComparer
{
    public class ProductEqualityComparer : IEqualityComparer<Product>
    {
        public bool Equals(Product x, Product y)
        {
            return string.Equals(x?.Sku, y?.Sku, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Product obj)
        {
            return obj.Sku?.GetHashCode() ?? 0;
        }
    }
}
