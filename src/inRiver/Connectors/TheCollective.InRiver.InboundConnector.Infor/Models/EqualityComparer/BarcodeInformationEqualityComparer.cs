﻿using System;
using System.Collections.Generic;

namespace TheCollective.InRiver.InboundConnector.Infor.Models.EqualityComparer
{
    public class BarcodeInformationEqualityComparer : IEqualityComparer<BarcodeInformation>
    {
        public bool Equals(BarcodeInformation x, BarcodeInformation y)
        {
            return string.Equals(x?.ProductSku, y?.ProductSku, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x?.Barcode, y?.Barcode, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(BarcodeInformation obj)
        {
            return ((obj.ProductSku ?? string.Empty) + (obj.Barcode ?? string.Empty)).GetHashCode();
        }
    }
}
