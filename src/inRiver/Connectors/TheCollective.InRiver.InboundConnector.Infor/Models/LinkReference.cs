﻿using inRiver.Remoting.Objects;

namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class LinkReference
    {
        public int Id { get; set; }
        public long Index { get; set; }
        public LinkType LinkType { get; set; }
        public string SourceSku { get; set; }
        public string TargetSku { get; set; }
    }
}
