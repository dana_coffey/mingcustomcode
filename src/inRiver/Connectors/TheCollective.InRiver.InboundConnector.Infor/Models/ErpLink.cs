﻿using TheCollective.InRiver.InboundConnector.Infor.Enums;

namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class ErpLink : SxeCrossReferenceRecord
    {
        public long LinkIndex { get; set; }
        public ErpLinkType LinkType { get; set; }
    }
}
