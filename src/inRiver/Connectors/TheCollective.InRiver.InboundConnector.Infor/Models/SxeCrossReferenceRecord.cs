﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class SxeCrossReferenceRecord
    {
        public string AlternateProductSku { get; set; }
        public string ProductSku { get; set; }
    }
}
