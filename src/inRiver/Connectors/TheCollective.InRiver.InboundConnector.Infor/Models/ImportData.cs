﻿using System.Collections.Generic;

namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class ImportData
    {
        public List<BarcodeInformation> Barcodes { get; set; }
        public List<Product> Products { get; set; }
    }
}
