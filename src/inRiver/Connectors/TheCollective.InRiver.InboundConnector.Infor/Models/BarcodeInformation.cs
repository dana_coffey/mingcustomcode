﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class BarcodeInformation
    {
        public string Barcode { get; set; }
        public string ProductSku { get; set; }
    }
}
