﻿using System.Collections.Generic;
using System.Linq;
using inRiver.Remoting.Objects;

namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class ImportResult
    {
        public List<string> Errors { get; } = new List<string>();
        public List<string> ErrorSkus { get; } = new List<string>();
        public bool IsSuccess => !Errors.Any();
        public int ProductsCreatedCount { get; set; }
        public int ProductsUpdatedCount { get; set; }
    }
}
