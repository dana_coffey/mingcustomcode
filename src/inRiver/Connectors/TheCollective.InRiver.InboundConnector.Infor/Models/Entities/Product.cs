﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models.Entities
{
    internal class Product : Core.Entities.Product
    {
        // Internal to avoid confusion and errors because it's should only be used in SX.e connector
        public string ProductSkuKey { get; set; }
    }
}
