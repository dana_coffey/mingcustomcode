﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models.Entities
{
    internal class Item : Core.Entities.Item
    {
        public bool ItemIsCatalogProduct { get; set; }

        // Internal to avoid confusion and errors because it's should only be used in SX.e connector
        public string ItemSkuKey { get; set; }
    }
}
