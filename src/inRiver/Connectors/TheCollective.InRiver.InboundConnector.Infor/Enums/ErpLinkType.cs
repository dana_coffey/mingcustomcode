﻿namespace TheCollective.InRiver.InboundConnector.Infor.Enums
{
    public enum ErpLinkType
    {
        RelatedProduct = 0,
        SupersededProduct = 1,
    }
}
