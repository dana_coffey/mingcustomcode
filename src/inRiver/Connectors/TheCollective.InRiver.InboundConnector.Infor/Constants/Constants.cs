﻿namespace TheCollective.InRiver.InboundConnector.Infor.Constants
{
    public static class Constants
    {
        public static class InRiver
        {
            public static class ServerSettings
            {
                public const string InforInboundConnectorLastExecutionDate = "INFOR_INBOUNDCONNECTOR_LAST_EXEC_DATE";
                public const string InforInboundConnectorSkuErrors = "INFOR_INBOUNDCONNECTOR_SKU_IMPORT_ERROR_LIST";
            }
        }
    }
}
