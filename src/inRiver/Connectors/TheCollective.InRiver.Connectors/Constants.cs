﻿namespace TheCollective.InRiver.Connectors
{
    public static class Constants
    {
        public static class ConnectorSettings
        {
            public const int BatchSizeForProcessLog = 50;
        }
    }
}
