﻿Param(
	[string]$Connectors = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.Connectors\bin\Debug",
    [string]$InforBuildPath = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.InboundConnector.Infor\bin\Debug",
    [string]$InboundCarrierBuildPath = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.InboundConnector.Carrier\bin\Debug",
	[string]$CarrierBuildPath = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.OutboundConnector.Carrier\bin\Debug",
	[string]$TitleBuilderPath = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.OutboundConnector.TitleBuilder\bin\Debug",
    [string]$OutboundBuildPath = "C:\VSDEV\ming\src\inRiver\Connectors\TheCollective.InRiver.OutboundConnectors.Insite\bin\Debug",
    [string]$ConnectFolderPath = "C:\VSDEV\ming\src\inRiver\inRiver AB\MING\inRiver Connect",
    [string]$ServiceName = "inRiver Connect MING",
    [string]$ComputerName = $ENV:ComputerName
)

If (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
   $arguments = "& '" + $myinvocation.mycommand.definition + "'"
   Start-Process powershell -Verb runAs -ArgumentList $arguments
   Break
}

# http://superuser.com/questions/447814/synchronously-stop-a-remote-service-with-powershell
[System.ServiceProcess.ServiceController]$service = Get-Service -Name "$ServiceName" -ComputerName "$ComputerName"
$initialStatus = $service.Status
do
{
    switch($service.Status)
    {
        { @(
        [System.ServiceProcess.ServiceControllerStatus]::ContinuePending,
        [System.ServiceProcess.ServiceControllerStatus]::PausePending,
        [System.ServiceProcess.ServiceControllerStatus]::StartPending,
        [System.ServiceProcess.ServiceControllerStatus]::StopPending) -contains $_ }
        {
            # A status change is pending. Do nothing.
            break;
        }

        { @(
        [System.ServiceProcess.ServiceControllerStatus]::Paused,
        [System.ServiceProcess.ServiceControllerStatus]::Running) -contains $_ }
        {
            # The service is paused or running. We need to stop it.
            $service.Stop()
            break;
        }

        [System.ServiceProcess.ServiceControllerStatus]::Stopped
        {
            # This is the service state that we want, so do nothing.
            break;
        }
    }

    # Sleep, then refresh the service object.
    Sleep -Seconds 1
    $service.Refresh()

} while (($service.Status -ne [System.ServiceProcess.ServiceControllerStatus]::Stopped))

Sleep -Seconds 1

Get-ChildItem $Connectors -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\InboundConnectors\" $_.FullName.Substring($Connectors.length)}
Get-ChildItem $InforBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\InboundConnectors\" $_.FullName.Substring($InforBuildPath.length)}
Get-ChildItem $InboundCarrierBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\InboundConnectors\" $_.FullName.Substring($InboundCarrierBuildPath.length)}
Get-ChildItem $CarrierBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\OutboundConnectors\" $_.FullName.Substring($CarrierBuildPath.length)}
Get-ChildItem $TitleBuilderPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\OutboundConnectors\" $_.FullName.Substring($TitleBuilderPath.length)}
Get-ChildItem $OutboundBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination {Join-Path "$ConnectFolderPath\OutboundConnectors\" $_.FullName.Substring($OutboundBuildPath.length)}

Sleep -Seconds 1

If ($initialStatus -eq [System.ServiceProcess.ServiceControllerStatus]::Running)
{
    $service.Start()
}

Read-Host -Prompt "Press Enter to exit"