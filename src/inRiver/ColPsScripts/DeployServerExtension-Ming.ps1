Param(
	[string]$workingDirectory,
	[string]$shortPlanKey,
    [string]$ServerExtentionBuildPath = "src\inRiver\Deploy.InRiver.ServerExtensions\bin\Debug",
    [string]$ExtensionFolderPath = $workingDirectory + "\temp\Extensions"
)

Remove-Item "$ExtensionFolderPath\" -Recurse -Confirm:$false

if (!(Test-Path -path "$ExtensionFolderPath")) {
	Write-Host "Creating $ExtensionFolderPath" 
	New-Item "$ExtensionFolderPath" -Type Directory
}

Write-Host "Copying ServerExtentions..."

Get-ChildItem $ServerExtentionBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe, inRiver.Server.exe | Copy-Item -Destination "$ExtensionFolderPath"

$destination = $workingDirectory + "\builds\ServerExtensions-" + $shortPlanKey + ".zip"

Write-Host "Creating package in " + $destination

If (Test-path $destination) {
	Remove-item $destination 
}else{
    New-Item -ItemType directory -Path "$workingDirectory\builds"
}

Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($ExtensionFolderPath, $destination)