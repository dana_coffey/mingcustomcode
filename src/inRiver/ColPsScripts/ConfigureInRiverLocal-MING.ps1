$rootPath = "C:\VSDEV\ming\src\inRiver\inRiver AB\MING"
$projectName = "MING"
$amiAdminWebsite = "C:\VSDEV\ming\src\inRiver\inRiver AB\MING\AdminWebsite"
$amiAdminApi = "C:\VSDEV\ming\src\inRiver\inRiver AB\MING\DataAPI"

If (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
   $arguments = "& '" + $myinvocation.mycommand.definition + "'"
   Start-Process powershell -Verb runAs -ArgumentList $arguments
   Break
}

$dns = "inriver.$($projectName.ToLower()).local.absolunet.com"
$poolName = "inRiver $projectName"
$Website = @{Name = "inRiver $projectName"; Path = "$rootPath\inRiver Web"; Binding = $dns; Pool = $poolName}


if(-Not (Test-Path IIS:\AppPools\$poolName))
{
	Write-Host "Create AppPool : $poolName"
    New-WebAppPool "$poolName" -Force
}

$websiteInfo = Get-Website $Website.Name
if ($websiteInfo -eq $null) {
	Write-Host "Create website : " $Website.Name
	New-Website -Name $Website.Name -ApplicationPool $Website.Pool -HostHeader $Website.Binding -PhysicalPath $Website.Path -Port 80 -Force
}


If ((Get-Content "$($env:windir)\system32\Drivers\etc\hosts" ) -notcontains "127.0.0.1 $($dns)")
{
	Write-Host "Add website to hostfile : $dns"
	ac -Encoding UTF8  "$($env:windir)\system32\Drivers\etc\hosts" "`r`n127.0.0.1 $($dns)"
}


$serverServiceName = "inRiver Server $projectName"
$serverService = get-service $serverServiceName -ErrorAction Ignore
if ($serverService -eq $null) {
	Write-Host "Add service inRiver Server"
	New-Service -Name $serverServiceName -BinaryPathName "$rootPath\inRiver Server\inRiver.Server.exe" -StartupType Manual
}

$connectServiceName = "inRiver Connect $projectName"
$connectService = get-service $connectServiceName -ErrorAction Ignore
if ($serverService -eq $null) {
	Write-Host "Add service inRiver Connect"
	New-Service -Name $connectServiceName -BinaryPathName "$rootPath\inRiver Connect\inRiver.Integration.exe" -DependsOn "inRiver Server $projectName" 
}


$amiAdminName = "amiadmin"
if ((Get-WebApplication -Site $Website.Name -Name $amiAdminName ) -eq $null) {
	Write-Host "Add web applications for $amiAdminName "
	New-WebApplication -Name $amiAdminName  -Site $Website.Name -PhysicalPath $amiAdminWebsite -ApplicationPool $poolName
}

$amiApiName = "dataapi"
if ((Get-WebApplication -Site $Website.Name -Name $amiApiName) -eq $null) {
	Write-Host "Add web applications for $amiApiName"
	New-WebApplication -Name $amiApiName -Site $Website.Name -PhysicalPath $amiAdminApi -ApplicationPool $poolName
}


Read-Host -Prompt "Press Enter to exit"