Param(
	[string]$workingDirectory,
	[string]$shortPlanKey,
	[string]$InboundBuildPath = "src\inRiver\Deploy.InRiver.InboundConnectors\bin\Debug",
	[string]$OutboundBuildPath = "src\inRiver\Deploy.InRiver.OutboundConnectors\bin\Debug",
    [string]$ConnectFolderPath = $workingDirectory + "\temp\Connectors"
)

Remove-Item "$ConnectFolderPath\" -Recurse -Confirm:$false

$inboundDestination = "$ConnectFolderPath\InboundConnectors\"
if (!(Test-Path -path "$inboundDestination")) {
	Write-Host "Creating $inboundDestination" 
	New-Item "$inboundDestination" -Type Directory
}

Write-Host "Copying Inbound connectors"
Get-ChildItem $InboundBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination "$inboundDestination"

$outboundDestination = "$ConnectFolderPath\OutboundConnectors\"
if (!(Test-Path -path "$outboundDestination")) {
	Write-Host "Creating $outboundDestination" 
	New-Item "$outboundDestination" -Type Directory
}

Write-Host "Copying Outbound connector"
Get-ChildItem $OutboundBuildPath -Recurse -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Integration.exe | Copy-Item -Force -Destination "$outboundDestination"

Write-Host "Copying Castle.Config"
Copy-Item -path "$workingDirectory\source\src\inRiver\inRiver AB\MING\inRiver Connect\Castle.config" -Destination "$ConnectFolderPath"

$destination = $workingDirectory + "\builds\Connectors-" + $shortPlanKey + ".zip"


Write-Host "Creating package in " + $destination
Remove-item $destination

Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($ConnectFolderPath, $destination)