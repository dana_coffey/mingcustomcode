$rootPath = "C:\VSDEV\ming\src\inRiver\inRiver AB\MING"
$projectName = "MING"
$serverServiceName = "inRiver Server MING"
$serverService = get-service $serverServiceName -ErrorAction Ignore
if ($serverService -eq $null) {
	Write-Host "Add service inRiver Server"
	New-Service -Name $serverServiceName -BinaryPathName "$rootPath\inRiver Server\inRiver.Server.exe" -StartupType Manual
}

$connectServiceName = "inRiver Connect MING"
$connectService = get-service $connectServiceName -ErrorAction Ignore
if ($serverService -eq $null) {
	Write-Host "Add service inRiver Connect"
	New-Service -Name $connectServiceName -BinaryPathName "$rootPath\inRiver Connect\inRiver.Integration.exe" -DependsOn "inRiver Server $projectName" 
}