﻿Param(
    [string]$ServerExtensionBuildPath = "C:\VSDEV\ming\src\inRiver\Deploy.InRiver.ServerExtensions\bin\Debug",
    [string]$ServerExtensionFolderPath = "C:\VSDEV\ming\src\inRiver\inRiver AB\ming\inRiver Server\Extensions",
    [string]$ServiceName = "inRiver Server MING",
    [string]$ComputerName = $ENV:ComputerName
)

If (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
   $arguments = "& '" + $myinvocation.mycommand.definition + "'"
   Start-Process powershell -Verb runAs -ArgumentList $arguments
   Break
}

# http://superuser.com/questions/447814/synchronously-stop-a-remote-service-with-powershell
[System.ServiceProcess.ServiceController]$service = Get-Service -Name "$ServiceName" -ComputerName "$ComputerName"
$initialStatus = $service.Status
do
{
    switch($service.Status)
    {
        { @(
        [System.ServiceProcess.ServiceControllerStatus]::ContinuePending,
        [System.ServiceProcess.ServiceControllerStatus]::PausePending,
        [System.ServiceProcess.ServiceControllerStatus]::StartPending,
        [System.ServiceProcess.ServiceControllerStatus]::StopPending) -contains $_ }
        {
            # A status change is pending. Do nothing.
            break;
        }

        { @(
        [System.ServiceProcess.ServiceControllerStatus]::Paused,
        [System.ServiceProcess.ServiceControllerStatus]::Running) -contains $_ }
        {
            # The service is paused or running. We need to stop it.
		Write-host "Stopping the service"
            $service.Stop()
            break;
        }

        [System.ServiceProcess.ServiceControllerStatus]::Stopped
        {
            # This is the service state that we want, so do nothing.
            break;
        }
    }

    # Sleep, then refresh the service object.
    Sleep -Seconds 1
    $service.Refresh()

} while (($service.Status -ne [System.ServiceProcess.ServiceControllerStatus]::Stopped))

Sleep -Seconds 1

Write-host "Copy dll from $ServerExtensionBuildPath"
Get-ChildItem $ServerExtensionBuildPath -Exclude inRiver.Logging.dll, inRiver.Remoting.dll, inRiver.Server.exe | Copy-Item -Force -Destination {Join-Path "$ServerExtensionFolderPath\" $_.FullName.Substring($ServerExtensionBuildPath.length)}

Sleep -Seconds 1

If ($initialStatus -eq [System.ServiceProcess.ServiceControllerStatus]::Running)
{
Write-host "Starting the service"
    $service.Start()
}

pause