Param(
    [string]$DeploymentFolderPath = "\\NORDC-EC2-INSIG\InRiver_WIS\inriver-builds",
    [string]$InRiverPath = "D:\inRiver AB",
	[string]$ConnectServiceName = "inRiver Connect MING",
	[string]$ServerServiceName = "inRiver Server MING",
    [string]$ComputerName = $ENV:ComputerName
)

Add-Type -assembly �system.io.compression.filesystem�

function RunAsAdministrator($arguments, $path)
{
    $myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
    $adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator

    # Check to see if we are currently running "as Administrator"
    if (!$myWindowsPrincipal.IsInRole($adminRole))
    {
        # Create a new process object that starts PowerShell as Administrator
        $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
        $newProcess.Arguments = $arguments;
        $newProcess.Verb = "runas";
        $newProcess.WorkingDirectory = $path;
        [System.Diagnostics.Process]::Start($newProcess);
        exit
    }
}

function StopService([string] $ServiceName){ 
    [System.ServiceProcess.ServiceController]$service = Get-Service -Name $ServiceName # -ComputerName "$ComputerName"
    $initialStatus = $service.Status
	
	Write-Host "$ServiceName has initial status $initialStatus"
	
    do
    {
        switch($service.Status)
        {
            { @(
            [System.ServiceProcess.ServiceControllerStatus]::ContinuePending,
            [System.ServiceProcess.ServiceControllerStatus]::PausePending,
            [System.ServiceProcess.ServiceControllerStatus]::StartPending,
            [System.ServiceProcess.ServiceControllerStatus]::StopPending) -contains $_ }
            {
                # A status change is pending. Do nothing.
                break;
            }

            { @(
            [System.ServiceProcess.ServiceControllerStatus]::Paused,
            [System.ServiceProcess.ServiceControllerStatus]::Running) -contains $_ }
            {
                # The service is paused or running. We need to stop it.
                $service.Stop()
                break;
            }

            [System.ServiceProcess.ServiceControllerStatus]::Stopped
            {
                # This is the service state that we want, so do nothing.
               
                break;
            }
        }

        # Sleep, then refresh the service object.
        Sleep -Seconds 1
        $service.Refresh()

    } while (($service.Status -ne [System.ServiceProcess.ServiceControllerStatus]::Stopped))

}


$directoryInfo = Get-ChildItem $DeploymentFolderPath | Measure-Object
$directoryInfo.count

If ($directoryInfo.count -gt 0)
{
    RunAsAdministrator $MyInvocation.MyCommand.Definition $MyInvocation.MyCommand.Path
	$CurrentPath = $DeploymentFolderPath

	StopService($ConnectServiceName)
	StopService($ServerServiceName)

	echo "************************************************************************"
	echo "--------------------deploying connectors ...----------------------------"
	echo "************************************************************************"

	sleep -seconds 1
	$connectorssourcepath = $currentpath + "\connectors"
	[io.compression.zipfile]::extracttodirectory($currentpath + "\connectors-mip.zip", $connectorssourcepath)
	copy-item -path "$connectorssourcepath\*" -destination "$inriverpath\inriver connect" -force -recurse
	sleep -seconds 1
	
	echo "************************************************************************"
	echo "------------------deploying server extensions ...-----------------------"
	echo "************************************************************************"

	sleep -seconds 1
	$serversourcepath = $currentpath + "\server"
	[io.compression.zipfile]::extracttodirectory($currentpath + "\serverextensions-mip.zip", $serversourcepath)
	copy-item -path "$serversourcepath\*" -destination "$inriverpath\inriver server\extensions" -force -recurse
	sleep -seconds 1

	[System.ServiceProcess.ServiceController]$serverService = Get-Service -Name $ServerServiceName # -ComputerName "$ComputerName"
	$serverService.Start()

	[System.ServiceProcess.ServiceController]$connectService = Get-Service -Name $ConnectServiceName # -ComputerName "$ComputerName"
	$connectService.Start()
}

Remove-Item "$DeploymentFolderPath\*" -Force -Recurse