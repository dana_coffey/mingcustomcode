﻿var $ = nwayo.vendor.jQuery;

$('.btn-filter').click(function (e) {
    e.preventDefault();
    $('#BulkUpdateStatusValue').val('');
    $("#TaskQueueForm").submit();
});

$('.btn-delete').click(function (e) {
    e.preventDefault();
    var action = $(this).data('action');
    deleteTask(action);
});

$('.btn-delete-filter').click(function (e) {
    e.preventDefault();
    var confirmed = confirm(Resources.DeleteButtonConfirm);
    if (confirmed) {
        deleteFilteredTasks();
    }
});

$('.btn-status-filter').click(function (e) {
    e.preventDefault();

    var status = $('#BulkUpdateStatusValue').val();

    if (status != '' && status != 'undefined') {

        var statusNum = parseInt(status);

        var confirmed = confirm(Resources.UpdateAllStatusConfirm + statusNum + '?');
            if (confirmed) {
                updateFilteredTasks();
            }
        }
});

$('.btn-restart-queue').click(function (e) {
    e.preventDefault();
    var confirmed = confirm(Resources.RestartQueueConfirm);
    if (confirmed) {
        var action = $(this).data('action');
        restartTaskQueue(action);
    }
});

$('.btn-cvl-submit').click(function (e) {
    e.preventDefault();
    var confirmed = confirm(Resources.ResyncCVLsConfirm);
    if (confirmed) {
        $('#SelectedCVLChannel').val($(this).data("attribute"));
        $("#ToolsCVLForm").submit();
    }
});

$('.btn-product-submit').click(function (e) {
    e.preventDefault();
    var confirmed = confirm(Resources.ResyncProductsConfirm);
    if (confirmed) {
        $('#SelectedProductsChannel').val($(this).data("attribute"));
        $("#ToolsProductForm").submit();
    }
});

$('.btn-category-submit').click(function (e) {
    e.preventDefault();
    var confirmed = confirm(Resources.ResyncCategoriesConfirm);
    if (confirmed) {
        $('#SelectedCategoriesChannel').val($(this).data("attribute"));
        $("#ToolsCategoriesForm").submit();
    }
});
     
function deleteTask(taskQueueDeleteAction) {
    $.ajax({
        url: taskQueueDeleteAction,
        type: 'DELETE',
    }).done(function (data) {
        if (data == 'True') {
            location.reload(true);
        } else {
            alert(Resources.ErrorOccured);
        }
    });
}

function deleteFilteredTasks() {
    $('#ProcessBulkDelete').val('True');
    $("#TaskQueueForm").submit();
}

function updateFilteredTasks() {
    $("#TaskQueueForm").submit();
}

function restartTaskQueue(restartTaskQueueAction) {
    $.ajax({
        url: restartTaskQueueAction,
        type: 'POST',
    }).done(function (data) {
        if (data == 'True') {
            location.reload(true);
            alert(Resources.RestartConnectorManually);
        } else {
            alert(Resources.ErrorOccured);
        }
    });
}


