define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/header/headerTitleTemplate.html'
], function($, _, Backbone, headerTitleTemplate){

  var HeaderTitleView = Backbone.View.extend({
    //el: $("#view-header"),

    initialize: function(data) {
        this.render(data);
    },

    render: function (data) {
        var compiledTemplate = _.template(headerTitleTemplate, data);
        $("#view-header").html(compiledTemplate);

        //var compiledTemplate = _.template( headerTemplate, data );
        //var headerTemplate = _.template($("#view-header-template").html());
        //this.headerHtml = headerTemplate({ data: { header: "CVL", desc: "Manage controlled value lists" } });
        //this.$el.html(headerTemplate);
    }
  });

  return HeaderTitleView;
  
});
