define([
  'jquery',
  'underscore',
  'backbone',
  'models/category/CategoryModel'
], function ($, _, Backbone, CategoryModel) {
  var CategoriesCollection = Backbone.Collection.extend({
      model: CategoryModel
  });
 
  return CategoriesCollection;
});
