// Filename: app.js
define([
  'jquery', 
  'underscore', 
  'backbone',
  'router', // Request router.js
  'alertify',
  'sharedjs/views/settingsmenu/settingsMenu'
], function ($, _, Backbone, Router, alertify, settingsMenu) {
    var initialize = function () {
        // Pass in our Router module and call it's initialize function
        Router.initialize();
        initApp();
    };

    function initApp() {
        settingsMenu.initializeSettingsMenu();
        alertify.set({ buttonReverse: true });
    }

  return { 
    initialize: initialize
  };
});
