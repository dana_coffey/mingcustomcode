﻿define([
    'jquery',
    'underscore',
    'backbone',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'models/imageconfig/imageConfigurationModel',
    'collections/imageconfig/imageConfigurationCollection',
    'text!templates/home/modalTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, imageConfigurationModel, imageConfigurationCollection, modalTemplate) {

    var imageConfigurationAddView = backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.parent = data.parent;
            this.header = data.header;
            if (!data.model) {
                this.model = new imageConfigurationModel();
            } else {
                this.model = data.model;
            }

            if (data.parent.model != undefined) {
                this.modelFromCopy = data.parent.model;
            }

            this.render();
        },
        events: {
            "click button#addConfiguration": "save",
            "click button#closeModal": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        cancel: function () {
            $("#modalWindow").hide();

        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var that = this;
                var uniqueCollection = new imageConfigurationCollection();
                uniqueCollection.url = '/api/imageconfiguration/name/' + this.form.fields.Name.editor.getValue() + "/extension/" + this.form.fields.Extension.editor.getValue();
                uniqueCollection.fetch({
                    success: function(models) {
                        if (models.length > 0) {
                            // The combo already exist.
                            inRiverUtil.NotifyError("Not unique configuration.", "The name/extension combination already exist in the system.<br/><br/>Cannot proceed with the request.");
                            return;
                        }

                        var self = that;
                        that.model.urlRoot = '/api/imageconfiguration';
                        that.model.save({
                            Name: that.form.fields.Name.editor.getValue(),
                            Extension: that.form.fields.Extension.editor.getValue(),
                            OutputExtension: that.form.fields.OutputExtension.editor.getValue(),
                            Arguments: that.form.fields.Arguments.editor.getValue()
                        }, {
                            success: function() {
                                inRiverUtil.Notify("Configuration has been added successfully");

                                if (self.parent.refreshList != undefined) {
                                    self.parent.refreshList();
                                } else {
                                    self.parent.options.parent.refreshList();
                                }

                                $("#modalWindow").find("div").html("");
                                $("#modalWindow").hide();
                            },
                            error: function(model, response) {
                                inRiverUtil.OnErrors(model, response);
                            },
                            wait: true
                        });
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onContentChanged: function () {
            this.save();
        },
        render: function () {
            this.form = new Backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new modal();
            this.$el.html(modalView.template({ data: this.header }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>" + this.header + "</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addConfiguration">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            if (this.modelFromCopy != undefined) {
                this.$el.find(".modal_section .field-Name input").val(this.modelFromCopy.attributes.Name);
                this.$el.find(".modal_section .field-Extension input").val(this.modelFromCopy.attributes.Extension);
                this.$el.find(".modal_section .field-OutputExtension input").val(this.modelFromCopy.attributes.OutputExtension);
                this.$el.find(".modal_section .field-Arguments input").val(this.modelFromCopy.attributes.Arguments);
            }

            //remove disabled when add
            this.$el.find("input[name='Name']").removeAttr("disabled");

            return this;
        }
    });

    return imageConfigurationAddView;
});