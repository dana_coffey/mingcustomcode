﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var completenessStructureModel = backbone.DeepModel.extend({
        idAttribute: "defintionId",
        initialize: function () {
        },
        urlRoot: '/api/completeness/structure',

    });

    return completenessStructureModel;

});
