define([
  'underscore',
  'backbone',
  'sharedjs/extensions/localeStringEditor'
], function (_, backbone, localeStringEditor) {

    var cvlValueModel = backbone.Model.extend({
        idAttribute: "Key",
        urlRoot: function () {
            return '/api/cvlvalue/' +this.get("CVLId");
        },
        initialize: function() {
        },
        schema: {
            // Used by backbone forms extension
            //Id: { type: 'Hidden' },
            CVLId: { type: 'Hidden' },
            Keys: { type: 'Select', title: 'Keys', options: [] },
            Key: { type: 'Text', title: 'Key', editorAttrs: { disabled: true }, validators: ['required', { type: 'regexp', message: 'Only alpha-numeric content with No space are allowed', regexp: /^[a-zA-Z0-9]+$/ }] },
            ParentKey: { type: 'Select', title: 'Parent Key', options: [] },
            Languages: { type: 'Select', title: 'Language', options: [] },
            Value: { type: 'Text', title: 'String Value' },
            LSValue: { type: localeStringEditor, title: 'String Value', editorAttrs: { settingMode: true } },
            Index: { type: 'Text', title: 'Index' }
        },
        toString: function () {
            return this.get("Key");
        },
    });

    return cvlValueModel;

});
