﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'models/completeness/completenessCriteriaModel',
    'sharedjs/collections/languages/LanguagesCollection',
    'views/completeness/completenessRuleTypeSettingView',
    'text!templates/completeness/completenessRuleDetailTemplate.html'
], function($, _, backbone, inRiverUtil, completenessCriteriaModel, languagesCollection, completenessRuleTypeSettingView, completenessRuleDetailTemplate) {

    var completenessRuleDetailView = backbone.View.extend({
        initialize: function(options) {
            var self = this;

            this.model = options.model;
            this.groupId = options.parentId;
            this.action = options.action;
            this.criteriaCollection = options.criterias;

            this.selectedLanguage = window.appHelper["userLanguage"];
            this.nameFieldValue = this.model.get("Name");
            this.defaultNameFieldValue = jQuery.extend(true, {}, this.nameFieldValue);

            this.langCollection = new languagesCollection();
            this.langCollection.fetch({
                success: function() {
                    self.render();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            var settings = this.model.get("Settings");
            if (settings != null && settings.length > 0) {
                var type = settings[0].Type;
                settings.every(function(setting) {
                    if (type != setting.Type) {
                        inRiverUtil.NotifyError("Bad settings", "The rule has different types in the settings list.");
                        return;
                    }
                });

                this.typeFieldValue = type;
                this.defaultTypeFieldValue = jQuery.extend(true, {}, this.typeFieldValue);
            } else {
                this.typeFieldValue = "";
                this.defaultTypeFieldValue = "";
            }

            this.nameLanguageField = "#rule-name-language-value";
            this.nameInputField = "#rule-name-value";
            this.ruleTypeField = "#rule-type-name-value";
        },
        events: {
            "change #rule-name-language-value": "onNameLanguageSelectedChange",
            "keyup #rule-name-value": "onKeyUpNameText",
            "change #rule-type-name-value": "onRuleTypeSelectedChange"
        },
        onCriteriaValueChanged: function(e) {
            e.stopPropagation();

            var value = "";
            inRiverUtil.Notify("onCriteriaValueChanged");
        },
        onRuleTypeSelectedChange: function(e) {
            e.stopPropagation();

            var self = this;
            var value = this.$el.find(this.ruleTypeField).val();
            var criteraModel = new completenessCriteriaModel();
            criteraModel.url = criteraModel.urlRoot + "/" + value;
            criteraModel.fetch({
                success: function(model) {
                    var criteria = model.toJSON();
                    var settingTag = self.$el.find("#rule-type-settings");
                    settingTag.empty();
                    self.criteriaType = criteria.Type;
                    _.each(criteria.SettingsKeys, function(key) {
                        settingTag.append(new completenessRuleTypeSettingView({ Type: self.criteriaType, Label: key, Value: "" }).el);
                    });
                }
            });
        },
        onNameLanguageSelectedChange: function(e) {
            e.stopPropagation();

            this.selectedLanguage = this.$el.find(this.nameLanguageField).val();
            var inputValue;

            if (this.nameFieldValue.stringMap) {
                inputValue = this.nameFieldValue.stringMap[this.selectedLanguage];
            } else {
                inputValue = this.nameFieldValue[this.selectedLanguage][1];
            }

            this.$el.find(this.nameInputField).val(inputValue);
        },
        onKeyUpNameText: function(e) {
            e.stopPropagation();

            var languageValue = this.selectedLanguage;

            if (!jQuery.isEmptyObject(this.nameFieldValue)) {
                if (this.nameFieldValue.stringMap) {
                    languageValue = this.selectedLanguage;
                } else {
                    languageValue = this.selectedLanguage;
                }
            }

            this.nameFieldValue.stringMap[languageValue] = this.$el.find(this.nameInputField).val();
            if (!this.isSameLocaleString(this.nameFieldValue.stringMap, this.defaultNameFieldValue.stringMap)) {
                this.model.set("Name", this.nameFieldValue);
                window.appHelper.event_bus.trigger('completeness_content_updated', this.model);
            }
        },
        onCriteriaValueUpdated: function(type, key, value) {
            if (this.model.get("Settings").length == 0) {
                // Create new
                var first = {
                    BusinessRulesId: this.model.id,
                    Id: 0,
                    Type: type,
                    Key: key,
                    Value: value
                }
                this.model.get("Settings").push(first);
            } else {
                var found = 0;
                _.each(this.model.get("Settings"), function(innerSetting) {
                    if (innerSetting.Type == type && innerSetting.Key == key) {
                        found++;
                        innerSetting.Value = value;
                    }
                });

                if (found == 0) {
                    // Create new
                    var additional = {
                        BusinessRulesId: this.model.id,
                        Id: 0,
                        Type: type,
                        Key: key,
                        Value: value
                    }
                    this.model.get("Settings").push(additional);
                }
            }
        },
        isSameLocaleString: function(first, second) {
            var result = true;
            _.each(first, function(value, key) {
                if ((second[key] == null && value != null) || second[key] != value) {
                    result = false;
                }
            });

            return result;
        },
        render: function() {
            var self = this;
            var values = {};

            this.$el.html(_.template(completenessRuleDetailTemplate, { NameValue: this.model.get("DisplayName"), WeightValue: this.model.get("Weight") }));
            this.$el.find(this.nameLanguageField).empty();

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'completeness_criteria_value_updated', this.onCriteriaValueUpdated);

            _.each(this.langCollection.toJSON(), function (lang) {
                if (self.selectedLanguage == lang.Name) {
                    self.$el.find(self.nameLanguageField).append('<option value="' + lang.Name + '" selected>' + lang.DisplayName + '</option>');
                } else {
                    self.$el.find(self.nameLanguageField).append('<option value="' + lang.Name + '">' + lang.DisplayName + '</option>');
                }
            });

            this.$el.find(this.ruleTypeField).empty();
            var settingTag;
            var settings = self.model.get("Settings");
            var modeltype = "";
            if (settings != null && settings.length > 0) {
                modeltype = settings[0].Type;
            }
            _.each(this.criteriaCollection.models, function(model) {
                var criteria = model.toJSON();
                if (modeltype == criteria.Type) {
                    self.$el.find(self.ruleTypeField).append('<option value="' + criteria.Type + '" selected>' + criteria.Name + '</option>');
                } else {
                    self.$el.find(self.ruleTypeField).append('<option value="' + criteria.Type + '">' + criteria.Name + '</option>');
                }
            });

            if (self.action == "add") {
                var firstCriteria = this.criteriaCollection.models[0].toJSON();
                settingTag = self.$el.find("#rule-type-settings");
                settingTag.empty();
                self.criteriaType = firstCriteria.Type;
                _.each(firstCriteria.SettingsKeys, function(key) {
                    settingTag.append(new completenessRuleTypeSettingView({ Type: self.criteriaType, Label: key, Value: "" }).el);
                });
            } else {
                var rulesettings = this.model.get("Settings");
                if (rulesettings != null) {
                    settingTag = this.$el.find("#rule-type-settings");
                    settingTag.empty();
                    _.each(rulesettings, function(rulesetting) {
                        settingTag.append(new completenessRuleTypeSettingView({ Type: rulesetting.Type, Label: rulesetting.Key, Value: rulesetting.Value }).el);
                    });
                }
            }

            if (this.action != "add") {
                this.$el.find(this.ruleTypeField).prop('disabled', 'true');
            }

            return this;
        }
    });

    return completenessRuleDetailView;
});
