define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var RoleModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/role',
        toString: function () {
            return this.get("Name");
            //return this.toJSON().Name;
        },
        defaults: {
            //friends: new User.Collection(),
            //userConsoleId: 0,
            //detailsLoaded: false
        },
        schema: { // Used by backbone forms extension
            Name: { type: 'Text', validators: ['required', { type: 'regexp', message: 'Value must be only alphanumeric', regexp: /^[a-zA-Z0-9]*$/ }], editorAttrs: { disabled: true } },
            Description: { type: 'Text', validators: ['required'] },
            Permissions: { type: 'Checkboxes', title: 'Permissions', options: []/*, options: (new app.RoleList())*/ }
        },
        get: function (attr) {
            // Override of get() to support "synthetic" attributes (such as RolesList below)
            if (typeof this[attr] == 'function') {
                return this[attr]();
            }
            return Backbone.Model.prototype.get.call(this, attr);
        },
        PermissionList: function () {
            var perms = this.get("Permissions");
            var plist = _.pluck(perms, "Name").join(", ");
            return plist;
        },
    });

    return RoleModel;

});
