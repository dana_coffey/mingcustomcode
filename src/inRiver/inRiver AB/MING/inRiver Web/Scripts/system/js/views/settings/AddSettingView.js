define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-modal',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/setting/SettingModel',
    'sharedjs/collections/settings/SettingsCollection',
    'text!templates/home/modalTemplate.html'
], function ($, _, Backbone, Modal, alertify, inRiverUtil, SettingModel, SettingCollection, modalTemplate) {

    var AddSettingView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.parent = data.data.parent;
            this.model = new SettingModel();
            this.render();
            _.bindAll(this, "onContentChanged");
        },
        events: {
            "click button#addSetting": "save",
            "click": "changed",
            "change": "changed",
            "click button#closeModal": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
  
        },
        cancel: function () {
            $("#modalWindow").hide();

        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;
                this.model.save({
                    Setting: this.form.fields.Setting.editor.getValue(),
                    Value: this.form.fields.Value.editor.getValue()
                }, {
                    success: function () {
                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();
                        $("#modalWindow").hide();
                        inRiverUtil.Notify("Setting has been added successfully");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        onContentChanged: function () {
            this.save();
        },
        render: function () {

            var self = this;

            this.form = new Backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var Modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add Setting" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add Setting</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addSetting">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            // Remove disabled when add
            this.$el.find("input[name='Setting']").removeAttr("disabled");

            return this;

        }
    });

    return AddSettingView;
});
