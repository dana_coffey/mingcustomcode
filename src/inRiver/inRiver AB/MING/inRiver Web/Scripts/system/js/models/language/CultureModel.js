define([
  'underscore',
  'backbone',
  //'collections/languages/CulturesCollection'
], function(_, Backbone) {
    
    var CultureModel = Backbone.Model.extend({
        idAttribute: 'Name',
        urlRoot: '/api/culture',
        defaults: {
        },
        initialize: function () {

        },
        schema: { // Used by backbone forms extension
            Name: { type: 'Select', options: [], validators: ['required'] }
           //Name: { type: 'Select', options: new CulturesCollection() }
        },
        toString: function () {
            return this.get("DisplayName") + " (" + this.get("Name") + ")";
        }
    });

    return CultureModel;

});
