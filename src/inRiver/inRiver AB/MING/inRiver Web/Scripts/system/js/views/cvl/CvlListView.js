define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'models/cvl/CvlModel',
  'sharedjs/collections/languages/languagesCollection',
  'collections/cvl/CvlCollection',
  'views/cvl/CvlView',
  'views/cvl/AddCvlView',
  'text!templates/cvl/cvlListTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, CvlModel, languagesCollection, CvlCollection, CvlView, AddCvlView, cvlListTemplate) {

    var CvlListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();

            //Get Cvl by id
            if (data && data.id) {
                var model = new CvlModel();
                model.fetch({
                    data: { id: data.id },
                    success: (function (res) {
                        var view = new CvlView({ model: model });
                        return view;
                    }),
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            //Get CVL List
            var that = this;

            var onDataHandler = function (collection) {
                that.languages = new languagesCollection();
                that.languages.fetch({
                    success: function (e) {
                        window.appHelper.loadingLangages = false;
                        window.appHelper.languagesCollection = that.languages;
                        that.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

            };

            that.collection = new CvlCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            //console.log("CvlListView init");

            that.collection.on('rowClicked', function (model, selected) {
                event.stopPropagation();

                //var modWindow = $("#modalWindow").find("div").html();

                //if (modWindow && modWindow != "") {
                //    return false; 
                //}
                var oldView = that._editCvlView;
                if (oldView != null) {
                    // oldView.remove();
                    if (oldView.cvlValuesView != null) {
                        oldView.cvlValuesView.undelegateEvents();
                        oldView.cvlValuesView.stopListenOnBus();
                    }
                    oldView.undelegateEvents();
                }

                $("#edit-panel").slideDown(200, "swing", function () { });
                //console.log("onLoadEditCvlView");

               
                that._editCvlView = new CvlView({ model: model });
                $("#edit-panel").html(that._editCvlView.el);

            });

            this.listenTo(Backbone, 'clickedAnywhere', function () { this.onUnloadEditCvlView(); });

            var columns = [
                { name: "Id", label: "Id", cell: "string" },
                { name: "DataType", label: "Data Type", cell: "string" },
                { name: "ParentId", label: "Parent CVL", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });

            /*this.collection.bind("reset", _.bind(this.render, this));
            this.collection.fetch({ reset: true });*/
        },
        events: {
            "click #newCvl": "newCvl",
            "click #edit-panel": "onClick"

        },
        newCvl: function () {
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddCvlView({ data: { header: "Add CVL", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        onClickedAnywhere: function () {
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            this.closeEditPanel(this.unloadEditCvlView);
        },
        onLoadEditCvlView: function (model) {
            //console.log("onLoadEditCvlView2");
            //this.onUnloadEditCvlView();
            this._editCvlView = new CvlView({ model: model }).render();
            $("#edit-panel").html(this._editCvlView.el);
        },
        onUnloadEditCvlView: function (parameters) {
                //console.log("onUnloadEditCvlView");
                if (this._editCvlView) {
                    $("#edit-panel").slideUp(200, "swing");
                    Backbone.trigger('closeEditPanel');
                }
            if (this._editCvlView != null) {
                if (this._editCvlView.cvlValuesView != null) {
                    this._editCvlView.cvlValuesView.undelegateEvents();

                }
                this._editCvlView.undelegateEvents();

            }
            this._editCvlView = null;
          
        },
        onClick: function () {
            event.stopPropagation();

        },
        render: function () {
            this.$el.html(cvlListTemplate);
            //console.log("CvlListView Render");
            this.$el.find("#cvl-list").append(this.grid.render().$el);
            //return this;
        }
    });

    return CvlListView;
});