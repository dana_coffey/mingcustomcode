define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){

    var EditPanelMixin = {
        closeEditPanel: function () {
            $("#edit-panel").slideUp(200, "swing");
            Backbone.trigger('closeEditPanel');
        },
        openEditPanel: function (model) {
            $("#edit-panel").slideDown(200, "swing", function () {
            });
            Backbone.trigger('openEditPanel', model);
        },
    };

    return EditPanelMixin;
  
});
