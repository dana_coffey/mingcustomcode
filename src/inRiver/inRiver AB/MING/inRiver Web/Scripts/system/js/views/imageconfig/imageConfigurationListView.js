﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backgrid',
    'sharedjs/misc/inRiverUtil',
    'extensions/GridDeleteItem',
    'collections/imageconfig/imageConfigurationCollection',
    'views/imageconfig/imageConfigurationEditView',
    'views/imageconfig/imageConfigurationAddModifyView',
    'text!templates/imageconfig/imageConfigurationListTemplate.html'
], function ($, _, backbone, backgrid, inRiverUtil, gridDeleteItem, imageConfigurationCollection, imageConfigurationEditView, imageConfigurationAddModifyView, imageConfigurationListTemplate) {

    var imageConfigurationListView = backbone.View.extend({
        initialize: function() {

            var that = this;

            this.collection = new imageConfigurationCollection();
            this.collection.fetch({
                success: function() {
                    that.render();
                },

                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }

            });

            this.editView = null;
            this.collection.on('rowClicked', function(model) {
                event.stopPropagation();
                var modalWindowOpen = $('#modalWindow').is(':visible');
                if (modalWindowOpen) {
                    return;
                }

                $("#edit-panel").slideDown(200, "swing", function() {});
                var oldView = that.editView;
                if (oldView != null) {
                    oldView.undelegateEvents();
                }

                that.editView = new imageConfigurationEditView({ model: model, parent: that });
                $("#edit-panel").html(that.editView.el);
            });

            this.listenTo(Backbone, 'clickedAnywhere', this.onUnloadEditView);
        },
        events: {
            "click #newImageConfiguration": "newImageConfiguration",
            "click #edit-panel": "onClick"

        },
        onUnloadEditView: function () {
            if (this.editView) {
                $("#edit-panel").slideUp(200, "swing");
                Backbone.trigger('closeEditPanel');
            }
            this.editView = null;
        },
        onClick: function() {
            event.stopPropagation();
        },
        newImageConfiguration: function () {
            if (this.addView) {
                this.addView.remove();
            }
            window.scrollTo(0, 0);
            this.addView = new imageConfigurationAddModifyView({ header: "Add Configuration", parent: this });
            $("#modalWindow").html(this.addView.el);
            $("#modalWindow").show();
        },
        refreshList: function () {
            var that = this;
            this.collection.fetch({
                success: function (models) {
                    that.render();
                }
            });
        },
        render: function () {
            var columns = [
                { name: "Name", label: "Name", cell: "string" },
                { name: "Extension", label: "Extension", cell: "string" },
                { name: "OutputExtension", label: "Output Extension", cell: "string" },
                { name: "Arguments", label: "Arguments", cell: "string" },
                { name: "", cell: gridDeleteItem }
            ];

            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });

            this.$el.html(imageConfigurationListTemplate);
            this.$el.find("#image-configurations-list").append(this.grid.render().$el);

        }

    });


    return imageConfigurationListView;
});
    