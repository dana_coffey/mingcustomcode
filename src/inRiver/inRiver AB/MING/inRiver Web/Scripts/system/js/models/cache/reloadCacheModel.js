﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var reloadCacheModel = backbone.DeepModel.extend({
        initialize: function () {
        },
        urlRoot: '/api/reloadCache',
    });

    return reloadCacheModel;

});
