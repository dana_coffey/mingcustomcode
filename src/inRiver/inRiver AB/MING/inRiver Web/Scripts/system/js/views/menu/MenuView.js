﻿define([
  'jquery',
  'underscore',
  'backbone',
    'sharedjs/misc/permissionUtil',

  'text!templates/menu/menuTemplate.html'
], function ($, _, Backbone, permissionUtil, menuTemplate) {

    var MenuView = Backbone.View.extend({
        el: $("#sidebar"),

        initialize: function () {
            this.render();
        },

        render: function () {


            var navigationItems = []; 

            if (permissionUtil.CheckPermission("Administrator") || permissionUtil.CheckPermission("UpdateCVL")) {
                navigationItems.push({ route: "cvl", name: "CVL", icon: "fa fa-list" });
            }
            if (permissionUtil.CheckPermission("Administrator")) {
                navigationItems.push({ route: "users", name: "Users", icon: "fa fa-user" },
                { route: "roles", name: "Roles", icon: "fa fa-users" },
                { route: "permissions", name: "Permissions", icon: "fa fa-key" },
                { route: "restrictedfields", name: "Restricted Fields", icon: "fa fa-ban" },
                { route: "languages", name: "Languages", icon: "fa fa-flag" },
                { route: "locks", name: "Locks", icon: "fa fa-lock" },
                { route: "imageconfig", name: "Image Configuration", icon: "fa fa-picture-o" },
                { route: "model", name: "Model", icon: "fa fa-sitemap" },
                { route: "completeness", name: "Completeness", icon: "fa fa-check-square-o" },
                { route: "htmltemplates", name: "HTML Templates", icon: "fa fa-file-o" },
                { route: "jobs", name: "Jobs", icon: "fa fa-briefcase" },
                { route: "settings", name: "Settings", icon: "fa fa-wrench" });
            }

            var compiledTemplate = _.template(menuTemplate, { items: navigationItems });
            this.$el.html(compiledTemplate);
        }
    });
    return MenuView;
});
