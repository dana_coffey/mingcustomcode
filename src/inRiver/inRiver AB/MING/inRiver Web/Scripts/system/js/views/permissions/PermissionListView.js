define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'views/sidebar/SidebarView',
  'models/permission/PermissionModel',
  'collections/permissions/PermissionsCollection',
  'views/permissions/PermissionView',
  'views/permissions/AddPermissionView',
  'text!templates/permissions/permissionsTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, SidebarView, PermissionModel, PermissionsCollection, PermissionView, AddPermissionView, permissionsTemplate) {

    var PermissionListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new PermissionsCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //console.log("PermissionListView init");

            that.collection.on('rowClicked', function (model, selected) {
                event.stopPropagation();
                var modWindow = $("#modalWindow").find("div").html();
                var modalWindowOpen = $('#modalWindow').is(':visible');


                if (modalWindowOpen) {
                    return false;
                }

                $("#edit-panel").slideDown(200, "swing", function () { });
                //console.log("onLoadEditPermissionView");
                var oldView = this._editCvlView;
                if (oldView != null) {
                    oldView.undelegateEvents();
                }
                that._editCvlView = new PermissionView({ model: model });
                $("#edit-panel").html(that._editCvlView.el);
            });
            var columns = [
                { name: "Name", label: "Name", cell: "string" },
                { name: "Description", label: "Description", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });

            this.listenTo(Backbone, 'clickedAnywhere', function () { this.onUnloadPermissionView(); });


        },

        events: {
            "click #newPermission": "newPermission",
            "click #edit-panel": "onClick"

        },
        newPermission: function () {
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddPermissionView({ data: { header: "Add Permission", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        onUnloadPermissionView: function (parameters) {
            //console.log("onUnloadPermissionView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                Backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;

        }, onClick: function () {
            event.stopPropagation();

        },
        render: function () {
            this.$el.html(permissionsTemplate);
            console.log("PermissionListView Render");
            this.$el.find("#permissions-list").append(this.grid.render().$el);
           
        }
    });

    return PermissionListView;
});