define([
    'underscore',
    'backbone'
], function (_, backbone) {
    
    var completenessRuleModel = backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/completeness/group/rule',
    });

    return completenessRuleModel;
});
