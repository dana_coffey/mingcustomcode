﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'jstree',
    'models/completeness/completenessDefinitionModel',
    'models/completeness/completenessGroupModel',
    'models/completeness/completenessRuleModel',
    'models/completeness/recalculateCompletenessModel',
    'collections/completeness/completenessStructureCollection',
    'collections/completeness/completenessGroupCollection',
    'collections/completeness/completenessRuleCollection',
    'views/completeness/completenessContentView',
    'text!templates/completeness/completenessStructureTemplate.html'
], function ($, _, backbone, inRiverUtil, jstree, completenessDefinitionModel, completenessGroupModel, completenessRuleModel, recalculateCompletenessModel,
    completenessStructureCollection, completenessGroupCollection, completenessRuleCollection, completenessContentView, completenessStructureTemplate) {

    var completenessStructureView = backbone.View.extend({
        initialize: function() {
            var self = this;

            this.loaded = false;

            this.structure = new completenessStructureCollection();
            this.structure.fetch({
                success: function() {
                    self.render();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #add": "onAddNode",
            "click #adddefintion": "onAddDefinition",
            "click #remove": "onDeleteNode",
            "click #viewcontent": "onViewContent",
            "click #expand": "expandAll",
            "click #collapse": "collapseAll",
            "click #recalculate": "onRecalculateDefintions"
        },
        onAddNode: function(e) {
            e.stopPropagation();

            if ($("#structure-row-container")[0].innerText == "No nodes") {
                return;
            }

            var ref = $("#structure-row-container").jstree(true),
                sel = ref.get_selected();
            if (!sel.length) {
                return;
            }
            sel = sel[0];
            var ids = sel.split("/");
            if (((sel.match(new RegExp("/", "g")) || []).length) == 0) {
                inRiverUtil.Notify("An empty group has been opened for editing.");
                this.goTo("/completeness/def/" + ids[0] + "/grp/-1");
            }
            if (((sel.match(new RegExp("/", "g")) || []).length) == 1) {
                inRiverUtil.Notify("An empty rule has been opened for editing.");
                this.goTo("/completeness/def/" + ids[0] + "/grp/" + ids[1] + "/rule/-1");
            }
        },
        onAddDefinition: function(e) {
            e.stopPropagation();
            inRiverUtil.Notify("An empty defintion has been opened for editing.");
            this.goTo("/completeness/def/-1");
        },
        onDeleteNode: function(e) {
            if (e != null) {
                e.stopPropagation();
            }

            if ($("#structure-row-container")[0].innerText == "No nodes") {
                return;
            }

            var self = this;
            var ref = $("#structure-row-container").jstree(true),
                sel = ref.get_selected();
            if (!sel.length) {
                return;
            }
            sel = sel[0];
            var ids = sel.split("/");
            if (((sel.match(new RegExp("/", "g")) || []).length) == 0) {
                // Definition
                inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete the selected definition?", function() {
                    var definition = new completenessDefinitionModel({ Id: ids[0] });
                    definition.destroy({
                        success: function() {
                            inRiverUtil.Notify("Completeness defintion has been removed");
                            self.goTo("/completeness");
                        },
                        error: function(model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });
            }
            if (((sel.match(new RegExp("/", "g")) || []).length) == 1) {
                // Group
                inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete the selected group?", function() {
                    var group = new completenessGroupModel({ Id: ids[1] });
                    group.destroy({
                        success: function() {
                            inRiverUtil.Notify("Completeness group has been removed");
                            self.goTo("/completeness");
                        },
                        error: function(model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });
            }
            if (((sel.match(new RegExp("/", "g")) || []).length) == 2) {
                // Rule
                inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete the selected rule?", function() {
                    var rule = new completenessRuleModel();
                    rule.url = "/api/completeness/group/" + ids[1] + "/rule/" + ids[2];
                    rule.fetch({
                        success: function() {
                            rule.destroy({
                                success: function() {
                                    inRiverUtil.Notify("Completeness rule has been removed from group");
                                    self.goTo("/completeness");
                                },
                                error: function(model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        },
                        error: function(model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });
            }
        },
        onViewContent: function(e) {
            if (e != null) {
                e.stopPropagation();
            }

            if (!this.loaded) {
                return;
            }

            if ($("#structure-row-container").jstree(true).element != null) {
                var ref = $("#structure-row-container").jstree(true),
                    sel = ref.get_selected();
            }
            if (sel == null || !sel.length) {
                return;
            }
            sel = sel[0];
            var ids = sel.split("/");
            var tab = "";
            if (window.appSession.completenessDetailTabName) {
                tab = "/" + window.appSession.completenessDetailTabName;
            }

            if (((sel.match(new RegExp("/", "g")) || []).length) == 0) {
                this.goTo("/completeness/def/" + ids[0] + tab);
            }
            if (((sel.match(new RegExp("/", "g")) || []).length) == 1) {
                this.goTo("/completeness/def/" + ids[0] + "/grp/" + ids[1] + tab);
            }
            if (((sel.match(new RegExp("/", "g")) || []).length) == 2) {
                this.goTo("/completeness/def/" + ids[0] + "/grp/" + ids[1] + "/rule/" + ids[2] + tab);
            }
        },
        expandAll: function(e) {
            e.stopPropagation();

            if ($("#structure-row-container")[0].innerText == "No nodes") {
                return;
            }

            $("#structure-row-container").jstree(true).open_all();
        },
        collapseAll: function(e) {
            e.stopPropagation();

            if ($("#structure-row-container")[0].innerText == "No nodes") {
                return;
            }

            $("#structure-row-container").jstree(true).close_all();
        },
        onRecalculateDefintions: function (e) {
            var self = this;

            if ($("#structure-row-container")[0].innerText == "No nodes") {
                return;
            }

            inRiverUtil.NotifyConfirm("Confirm action", "Starting recalculation of completeness on all defintions, this can take time.<br/><br/>Do you want to continue?", function() {
                var recalculateModel = new recalculateCompletenessModel();
                var count = 0;
                _.each(self.structure.toJSON(), function (model) {
                    if (model.level == 1) {
                        recalculateModel.url = "/api/completeness/definition/" + model.id + "/recalculate";
                        recalculateModel.fetch({
                            success: function () {
                                count++;
                                if (count == self.structure.length) {
                                    inRiverUtil.Notify("All definitions has been recalculated.");
                                }
                            },
                            error: function (err, response) {
                                inRiverUtil.OnErrors(err, response);
                                return;
                            }
                        });
                    }
                });
            });
        },
        updateSortOrderOnChilds: function(parentNode, nodeType) {
            var parentId = parentNode.id.substring(parentNode.id.lastIndexOf("/") + 1);
            var collection;
            var singleUrl;
            if (nodeType.indexOf("Definition") > -1) {
                // Inform user self definition is not sortable.
                inRiverUtil.NotifyError("No action made", "Definitions are not sortable.");
                return;
            }
            if (nodeType.indexOf("Group") > -1) {
                // Groups
                collection = new completenessGroupCollection({ definitionId: parentId });
                collection.url = "/api/completeness/definition/" + parentId + "/group";
                singleUrl = "/api/completeness/group/";
            }
            if (nodeType.indexOf("Rule") > -1) {
                // Rules
                collection = new completenessRuleCollection({ groupId: parentId });
                collection.url = "/api/completeness/group/" + parentId + "/rule";
                singleUrl = "/api/completeness/group/" + parentId + "/rule/";
            }

            if (collection == null || singleUrl == null) {
                inRiverUtil.NotifyError("Bad arguments", "Could not identify the node type of the selected node.<br/><br/>Sorting aborted.");
                return;
            }

            var order = 0;
            var count = 0;
            collection.fetch({
                success: function(result) {
                    _.each(parentNode.children, function(child) {
                        var childId = child.substring(child.lastIndexOf("/") + 1);
                        _.each(result.models, function(model) {
                            if (childId == model.get("Id")) {
                                model.set("SortOrder", order);
                                order++;
                            }
                        });
                    });

                    _.each(result.models, function(child) {
                        child.url = singleUrl + child.get("Id");
                        child.save({}, {
                            type: "PUT",
                            success: function() {
                                count++;
                                if (count == order) {
                                    inRiverUtil.Notify("New sort order succesfully stored");
                                    window.location.reload();
                                }
                            },
                            error: function(model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    });
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onRefreshStructure: function() {
            var self = this;
            this.structure.fetch({
                success: function() {
                    self.render();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        buildStructure: function() {
            var self = this;
            if (this.structure.length == 0) {
                $("#structure-row-container").html("No nodes");
            } else {
                $("#structure-row-container").html("");
                $("#structure-row-container").jstree({
                    core: {
                        data: self.structure.toJSON(),
                        animation: "100",
                        themes: { dots: false },
                        'check_callback': function( /* operation, node, node_parent, node_position, more */) {
                            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                            // in case of 'rename_node' node_position is filled with the new node name
                            // return operation == 'move_node' ? false : false;
                            return true;
                        }
                    },
                    "plugins": ["types", "wholerow", "dnd", "state"],
                    "types": {
                        "#": {
                            "max_children": 100,
                            "max_depth": 4,
                            "valid_children": ["completenessDefinition-WeightOk", "completenessDefinition-WeightNotOk"]
                        },
                        "completenessDefinition-WeightOk": {
                            "icon": "fa fa-check-square-o",
                            "valid_children": ["completenessGroup-WeightOk", "completenessGroup-WeightNotOk"]
                        },
                        "completenessDefinition-WeightNotOk": {
                            "icon": "fa fa-check-square-o tree-node-alert-color",
                            "valid_children": ["completenessGroup-WeightOk", "completenessGroup-WeightNotOk"]
                        },
                        "completenessGroup-WeightOk": {
                            "icon": "fa fa-share-alt",
                            "valid_children": ["completenessRule-WeightOk", "completenessRule-WeightNotOk"]
                        },
                        "completenessGroup-WeightNotOk": {
                            "icon": "fa fa-share-alt tree-node-alert-color",
                            "valid_children": ["completenessRule-WeightOk", "completenessRule-WeightNotOk"]
                        },
                        "completenessRule-WeightOk": {
                            "icon": "fa fa-check",
                            "valid_children": []
                        },
                        "completenessRule-WeightNotOk": {
                            "icon": "fa fa-check tree-node-alert-color",
                            "valid_children": []
                        }
                    },
                });

                $("#structure-row-container").on("after_open.jstree", function() {
                    self.setTitleOnTreeNodes(self.$el.find(".tree-node-alert-color"));
                });

                $("#structure-row-container").on("ready.jstree", function() {
                    self.loaded = true;
                    self.setTitleOnTreeNodes(self.$el.find(".tree-node-alert-color"));
                });

                $("#structure-row-container").on('changed.jstree', function(e, data) {
                    e.stopPropagation();
                    if (data.node != null && data.node.id == data.selected && data.node.state.selected) {
                        self.onViewContent(e);
                    }
                });

                $("#structure-row-container").on('loaded.jstree', function() {
                    if (window.appSession.completenessDefinitionId) {
                        var sel = window.appSession.completenessDefinitionId;
                        if (window.appSession.completenessGroupId) {
                            sel = sel + "/" + window.appSession.completenessGroupId;
                            if (window.appSession.completenessRuleId) {
                                sel = sel + "/" + window.appSession.completenessRuleId;
                                $("#structure-row-container").jstree(true).select_node(sel);
                            } else {
                                $("#structure-row-container").jstree(true).select_node(sel);
                            }
                        } else {
                            $("#structure-row-container").jstree(true).select_node(sel);
                        }
                    }

                    self.onViewContent();
                });

                $("#structure-row-container").on('move_node.jstree', function(node, data) {
                    var sourceIds = data.node.id.split("/");
                    var targetIds = data.parent.split("/");
                    if (data.node.type.indexOf("completenessRule") > -1) {
                        if (targetIds[0] != sourceIds[0]) {
                            inRiverUtil.NotifyError("Not allowed", "Not allowed to move rules to other definitions.");
                            return;
                        }

                        if (targetIds[1] != sourceIds[1]) {
                            inRiverUtil.NotifyConfirm("Confirm reuse of rule.", "Do you want to also use this rule in this group?", function() {
                                var ruleModel = new completenessRuleModel();
                                ruleModel.url = "/api/completeness/group/" + sourceIds[1] + "/rule/" + sourceIds[2] + "/group/" + targetIds[1];
                                ruleModel.save({}, {
                                    success: function() {
                                        self.onRefreshStructure();
                                    },
                                    error: function(error, response) {
                                        inRiverUtil.OnErrors(error, response);
                                    }
                                });
                            });
                        }

                        var parentNode = $("#structure-row-container").jstree(true).get_node(data.parent);
                        self.updateSortOrderOnChilds(parentNode, data.node.type);
                        return;
                    }

                    if (data.parent != data.old_parent) {
                        inRiverUtil.NotifyError("Not allowed", "Not allowed to move to another definition.");
                        self.onRefreshStructure();
                        return;
                    }

                    var parentNode = $("#structure-row-container").jstree(true).get_node(data.parent);
                    self.updateSortOrderOnChilds(parentNode, data.node.type);
                });
            }
        },
        setTitleOnTreeNodes: function(elements) {
            _.each(elements, function(element) {
                if ((" " + element.className + " ").replace(/[\n\t]/g, " ").indexOf(" fa-check-square-o ") > -1) {
                    //Definition
                    element.title = "Attention! The definition must have groups with valid weights";
                    return;
                }
                if ((" " + element.className + " ").replace(/[\n\t]/g, " ").indexOf(" fa-share-alt ") > -1) {
                    //Group
                    element.title = "Attention! The total weight of the group must be 100%";
                    return;
                }
                if ((" " + element.className + " ").replace(/[\n\t]/g, " ").indexOf(" fa-check ") > -1) {
                    //Rule
                    element.title = "Attention! The weight of the rule must not be 0";
                }
            });
        },
        openContentView: function(model, type, action, parentId, definitionId) {
            this.$el.find("#content-container").empty();
            this.$el.find("#content-container").append(new completenessContentView({
                model: model,
                type: type,
                action: action,
                parentId: parentId,
                definitionId: definitionId
            }).render().el);
        },
        onRedirectToDetails: function() {
            var self = this;
            var action = "edit";

            if (window.appSession.completenessDefinitionId) {
                if (window.appSession.completenessGroupId) {
                    if (window.appSession.completenessRuleId) {
                        // Rule details
                        if (window.appSession.completenessRuleId == -1) {
                            action = "add";
                        }

                        var rule = new completenessRuleModel();
                        rule.fetch({
                            url: "/api/completeness/group/" + window.appSession.completenessGroupId + "/rule/" + window.appSession.completenessRuleId,
                            success: function(model) {
                                self.openContentView(model, "Rule", action, window.appSession.completenessGroupId, window.appSession.completenessDefinitionId);
                            },
                            error: function(model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });

                        return;
                    }

                    //Group details
                    if (window.appSession.completenessGroupId == -1) {
                        action = "add";
                    }

                    var group = new completenessGroupModel({ Id: window.appSession.completenessGroupId });
                    group.fetch({
                        success: function(model) {
                            self.openContentView(model, "Group", action, window.appSession.completenessDefinitionId, window.appSession.completenessDefinitionId);
                        },
                        error: function(model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    return;
                }

                // Definition details
                if (window.appSession.completenessDefinitionId == -1) {
                    action = "add";
                }

                var defintion = new completenessDefinitionModel({ Id: window.appSession.completenessDefinitionId });
                defintion.fetch({
                    success: function(model) {
                        self.openContentView(model, "Definition", action, 0, window.appSession.completenessDefinitionId);
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

        },
        render: function() {
            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'completeness_node_deleted', this.onDeleteNode);
            this.listenTo(window.appHelper.event_bus, 'completeness_node_saved', this.onRefreshStructure);

            this.$el.html(completenessStructureTemplate);
            this.buildStructure();

            if (window.appSession.completenessDetailTabName) {
                this.onRedirectToDetails();
            }

            return this;
        }

    });

    return completenessStructureView;
});
