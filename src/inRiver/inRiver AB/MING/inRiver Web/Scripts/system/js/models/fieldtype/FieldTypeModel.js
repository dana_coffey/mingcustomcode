define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var FieldTypeModel = Backbone.Model.extend({
        idAttribute: "id",
        initialize: function () {
        },
        schema: { // Used by backbone forms extension
            FieldType: { type: 'Select', title: "Field Type", options: [] }
        },
        toString: function () {
            return this.get("displayname");
        },
    });

    return FieldTypeModel;

});
