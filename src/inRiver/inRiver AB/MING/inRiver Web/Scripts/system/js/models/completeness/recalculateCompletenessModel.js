define([
    'underscore',
    'backbone'
], function (_, backbone) {
    
    var recalculateCompletenessModel = backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/completeness/definition/{Id}/recalculate',
    });

    return recalculateCompletenessModel;
});
