define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-modal',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'models/language/LanguageModel',
  'models/language/CultureModel',
  'collections/languages/LanguagesCollection',
  'collections/languages/CulturesCollection',
  'text!templates/home/modalTemplate.html'
], function ($, _, backbone, Modal, alertify, inRiverUtil, languageModel, cultureModel, languagesCollection, culturesCollection, modalTemplate) {

    var AddLanguageView = backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();

            this.parent = data.data.parent;

            this.language = new languageModel();
            this.culture = new cultureModel();
            this.render();
        },
        events: {
            "click button#addLanguage": "save",
            "click button#closeModal": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        cancel: function () {
            $("#modalWindow").hide();

        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;

                //var cultValue = this.form.fields.Name.editor.getValue(); 
                this.language.save({
                    Name: self.form.fields.Name.editor.getValue()
                }, {
                    success: function () {
                        self.language.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();

                        $("#modalWindow").find("div").html("");
                        $("#modalWindow").hide();
                        inRiverUtil.Notify("Language has been added successfully");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        render: function (id) {

            var self = this;

            this.form = new backbone.Form({
                model: this.culture
            });

            // Create a modal view class
            var Modal = backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add Language" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add Language</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addLanguage">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            var onDataHandler = function (langCollection) {
                _.each(self.cultCollection.models, function (culture) {
                    //if(_.contains(langCollection, model);
                    var existsLanguage = _.find(langCollection.models,
                        function(language) {
                             return culture.id == language.id;
                        });
                    if (existsLanguage != undefined) {
                        self.cultCollection.remove(culture.id);
                    }
                });

                self.form.fields.Name.editor.setOptions(self.cultCollection);
            };

            this.cultCollection = new culturesCollection();
            this.cultCollection.fetch({
                data: function (collection, response) {
                },
                success: function () {
                    var langCollection = new languagesCollection();
                    langCollection.fetch({
                        data: function (collection, response) {
                        },
                        success: onDataHandler,
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            return this;

        }
    });

    return AddLanguageView;
});