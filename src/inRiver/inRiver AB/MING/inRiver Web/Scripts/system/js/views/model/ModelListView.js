define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'models/entitytype/EntityTypeModel',
  'models/cache/reloadCacheModel',
  'collections/entitytype/EntityTypeCollection',
  'text!templates/model/modelTemplate.html'
], function ($, _, backbone, backgrid, alertify, inRiverUtil, sidebarView, entityTypeModel, reloadCacheModel, entityTypeCollection, modelTemplate) {

    var modelListView = backbone.View.extend({
        events: {
            "click button#downloadSchema": "downloadSchema",
            "click button#downloadModel": "downloadModel",
            "click button#reloadModel": "reloadModel",
            "submit form": "upload"
        },
        downloadSchema: function () {
            var entType = this.form.fields.EntityType.editor.getValue();
            window.location.href = "/app/Model?method=GetSchema&entityType=" + entType;
        },
        downloadModel: function () {
            var cvlValues = $("#cvlvalues").is(':checked');
            window.location.href = "/app/Model?method=GetModel&cvlValues=" + cvlValues;
        },
        reloadModel: function() {
            this.cacheModel = new reloadCacheModel();
            // Use POST to reload the cache
            this.cacheModel.save(null, {
                success: function() {
                    inRiverUtil.Notify('The model has been reloaded');
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        upload: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var confirmCallback = function() {
                ///create a new FormData object
                var formData = new FormData(); //var formData = new FormData($('form')[0]);

                ///get the file and append it to the FormData object
                formData.append('file', $('#file')[0].files[0]);

                ///AJAX request
                $.ajax(
                {
                    ///server script to process data
                    url: "/api/filemodel", //web service
                    //url: "../Handlers/GetModel.ashx?method=UploadModel", //web service
                    type: 'POST',
                    timeout: 0,
                    complete: function () {
                        //on complete event     
                    },
                    progress: function () {
                        //progress event    
                    },
                    ///Ajax events
                    beforeSend: function () {
                        //before event  
                    },
                    success: function () {
                        //success event
                        inRiverUtil.Notify('Model was successfully uploaded');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //errorHandler
                       inRiverUtil.NotifyError('Error', 'Model could not be uploaded.');
                    },
                    ///Form data
                    data: formData,
                    ///Options to tell JQuery not to process data or worry about content-type
                    cache: false,
                    contentType: false,
                    processData: false
                });
                ///end AJAX request
            }
            inRiverUtil.NotifyConfirm("Confirm action", "This operation will delete both model and all existing data in inRiver PIM. Are you really sure?", confirmCallback, this);
        },
        initialize: function () {
            this.model = new entityTypeModel();
            //console.log("ModelListView init");
            this.render();
        },
        render: function () {
            var self = this;

            this.$el.html(modelTemplate);

            this.form = new backbone.Form({
                model: this.model
            });

            this.$el.find("#entityTypes").append(this.form.render().el);
            
            var $buttonDownloadSchema = $('<button class="btn" type="button" id="downloadSchema"><i class="fa fa-download"></i> Download</button>');
            this.$el.find("#entityTypes").append($buttonDownloadSchema);

            var $buttonDownload = $('<button class="btn" type="button" id="downloadModel"><i class="fa fa-download"></i> Download</button>');
            this.$el.find("#downloadModelButton").append($buttonDownload);

            var $buttonUpload = $('<button class="btn" type="submit" id="upload"><i class="fa fa-upload"></i> Upload</button>');
            this.$el.find("#uploadModelButton").append($buttonUpload);

            var $resetModel = $('<button class="btn" type="button" id="reloadModel"><i class="fa fa-refresh"></i> Reload</button>');
            this.$el.find("#resetModelButton").append($resetModel);

            var onDataHandler = function () {
                self.form.fields.EntityType.editor.setOptions(self.collection);
                self.form.fields.EntityType.editor.setValue("Product");
            };

            self.collection = new entityTypeCollection();
            self.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            console.log("ModelListView Render");
            return this;
        }
    });

    return modelListView;
});
