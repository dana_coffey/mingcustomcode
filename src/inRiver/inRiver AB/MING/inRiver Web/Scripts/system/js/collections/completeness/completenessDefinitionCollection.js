define([
  'jquery',
  'underscore',
  'backbone',
  'models/completeness/CompletenessDefinitionModel'
], function ($, _, backbone, completenessDefinitionModel) {
    var completenessDefinitionCollection = backbone.Collection.extend({
      model: completenessDefinitionModel,
      url: '/api/completeness/definition'
  });
 
  return completenessDefinitionCollection;
});
