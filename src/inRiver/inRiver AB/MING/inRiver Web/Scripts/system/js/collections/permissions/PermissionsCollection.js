define([
  'jquery',
  'underscore',
  'backbone',
  'models/permission/PermissionModel'
], function($, _, Backbone, PermissionModel){
    var PermissionsCollection = Backbone.Collection.extend({
      model: PermissionModel,
      url: '/api/permission'
  });
 
    return PermissionsCollection;
});
