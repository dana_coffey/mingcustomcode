define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'models/permission/PermissionModel',
  'collections/permissions/PermissionsCollection',
  'text!templates/permissions/permissionTemplate.html'
], function($, _, Backbone, alertify, inRiverUtil, SidebarView, RoleModel, PermissionsCollection, permissionTemplate){

  var PermissionListView = Backbone.View.extend({
    //  el: $("#main-container"),
      initialize: function (data) {
            this.render();
            this.undelegateEvents();
            // _.bindAll(this, "onContentChanged");
      },
      events: {
          "click button#cancelButton": "cancel",
          "click button#saveButton": "savePermission",
          "click": "onClick",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown"
      },
      cancel: function () {
          $("#edit-panel").slideUp(200, "swing");
          Backbone.trigger('closeEditPanel');

      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.savePermission();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.savePermission();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.savePermission();
      },
      savePermission: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;
              
              this.model.save({
                  Name: this.form.fields.Name.editor.getValue(),
                  Description: this.form.fields.Description.editor.getValue()
              }, {
                  success: function () {
                      inRiverUtil.Notify("Permission has been updated successfully");

                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                      $("#edit-panel").slideUp(200, "swing");
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  }
              });
          }
      },
       onClick: function () {
          event.stopPropagation();

      },
      render: function (id) {
          var self = this;
          this.$el.html(permissionTemplate);
          this.form = new Backbone.Form({model: this.model});
          this.$el.find("#permission-content").append(this.form.render().el);
        //  return this;
      }
  });

  return PermissionListView;
});
