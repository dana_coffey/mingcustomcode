﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'models/completeness/completenessRuleModel',
    'sharedjs/collections/languages/LanguagesCollection',
    'text!templates/completeness/completenessGroupDetailTemplate.html'
], function ($, _, backbone, inRiverUtil, completenessRuleModel, languagesCollection, completenessGroupDetailTemplate) {

    var completenessGroupDetailView = backbone.View.extend({
        initialize: function (options) {
            this.model = options.model;
            this.defintionId = options.parentId;
            this.action = options.action;

            this.badWeight = false;

            this.selectedLanguage = window.appHelper["userLanguage"];
            this.nameFieldValue = this.model.get("Name");
            this.defaultNameFieldValue = jQuery.extend(true, {}, this.nameFieldValue);

            this.nameLanguageField = "#group-name-language-value";
            this.nameInputField = "#group-name-value";

            this.render();
        },
        events: function () {
            var theEvents = {};
            _.each(this.model.get("Rules"), function(rule) {
                theEvents["click #ruleid-" + rule.Id] = "onOpenRule";
                theEvents["click #delete-" + rule.Id] = "onDeleteRule";
                theEvents["change #weight-" + rule.Id] = "onWeightChanged";
            });

            theEvents["change " + this.nameLanguageField] = "onNameLanguageSelectedChange";
            theEvents["keyup " + this.nameInputField] = "onKeyUpNameText";
            return theEvents;
        },
        noAction: function(e) {
            e.stopPropagation();
        },
        onWeightChanged: function(e) {
            e.stopPropagation();
            var self = this;

            var weights = {};
            var total = 0;
            _.each(this.model.get("Rules"), function (rule) {
                var weight = parseInt(self.$el.find("#weight-" + rule.Id).val());
                if (weight != 'NaN') {
                    weights[rule.Id] = weight;
                    total += weight << 0;
                }
            });

            if (total > 100 || total < 100) {
                this.badWeight = true;
                this.$el.find("#exclamation-icon").show();
            } else {
                this.badWeight = false;
                this.$el.find("#exclamation-icon").hide();
            }

            this.$el.find("#total-weight-value").text(total);
            window.appHelper.event_bus.trigger('completeness_group_weight_updated', weights);
        },
        onOpenRule: function (e) {
            e.stopPropagation();
            var targetId = e.currentTarget.id.substring(e.currentTarget.id.lastIndexOf("-") + 1);
            this.goTo("/completeness/def/" + this.model.get("DefinitionId") + "/grp/" + this.model.get("Id") + "/rule/" + targetId);
        },
        onDeleteRule: function (e) {
            e.stopPropagation();

            var self = this;
            var targetId = e.currentTarget.id.split('-');
            inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete the group?", function () {
                var rule = new completenessRuleModel();
                rule.url = "/api/completeness/group/" + self.model.get("Id") + "/rule/" + targetId[1];
                rule.fetch({
                    success: function () {
                        rule.destroy({
                            success: function () {
                                inRiverUtil.Notify("Completeness rule has been removed from group");
                                self.goTo("/completeness/def/" + self.model.get("DefinitionId") + "/grp/" + self.model.get("Id"));
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            });
        },
        addRowToTable: function (rule) {
            var table = this.$el.find("#table-body-content");
            if (table == null || rule == null) {
                return;
            }

            var weightTitle = "";
            if (rule.Weight == 0) {
                weightTitle = "The weight should not be 0, remove the rule instead if it is not to be included in any calculations.";
            }

            table.append("<tr><td>" + rule.DisplayName + "</td>" +
                "<td><input type='text' title='" + weightTitle + "' value='" + rule.Weight + "' maxlength=''3 size='3' id='weight-" + rule.Id + "'></td>" +
                "<td>" + rule.SortOrder + "</td>" +
                "<td><i id='ruleid-" + rule.Id + "' class='icon-entypo-fix-pencil completeness-content-row-icon-button' title='Edit rule'></i>" +
                "<i id='delete-" + rule.Id + "' class='fa fa-trash-o completeness-content-row-icon-button' title='Delete rule'></i></td></tr>");
        },
        onNameLanguageSelectedChange: function (e) {
            if (e != null) {
                e.stopPropagation();
            }

            this.selectedLanguage = this.$el.find(this.nameLanguageField).val();
            var inputValue;

            if (this.nameFieldValue.stringMap) {
                inputValue = this.nameFieldValue.stringMap[this.selectedLanguage];
            } else {
                inputValue = this.nameFieldValue[this.selectedLanguage][1];
            }

            this.$el.find(this.nameInputField).val(inputValue);
        },
        onKeyUpNameText: function (e) {
            e.stopPropagation();

            var languageValue = this.selectedLanguage;

            if (!jQuery.isEmptyObject(this.nameFieldValue)) {
                if (this.nameFieldValue.stringMap) {
                    languageValue = this.selectedLanguage;
                } else {
                    languageValue = this.selectedLanguage;
                }
            }

            this.nameFieldValue.stringMap[languageValue] = this.$el.find(this.nameInputField).val();
            if (!this.isSameLocaleString(this.nameFieldValue.stringMap, this.defaultNameFieldValue.stringMap)) {
                this.model.set("Name", this.nameFieldValue);
                window.appHelper.event_bus.trigger('completeness_content_updated', this.model);
            }
        },
        isSameLocaleString: function (first, second) {
            var result = true;
            _.each(first, function (value, key) {
                if ((second[key] == null && value != null) || second[key] != value) {
                    result = false;
                }
            });

            return result;
        },
        render: function () {
            var self = this;
            var values = {};
            var totalWeight = 0;
            var weights = {};
            _.each(this.model.get("Rules"), function (rule) {
                weights[rule.Id] = rule.Weight;
                totalWeight += rule.Weight;
            });

            this.$el.html(_.template(completenessGroupDetailTemplate, { NameValue: this.model.get("DisplayName"), WeightValue: totalWeight }));
            _.each(this.model.get("Rules"), function (rule) {
                self.addRowToTable(rule);
            });

            if (totalWeight > 100 || totalWeight < 100) {
                this.badWeight = true;
                this.$el.find("#exclamation-icon").show();
            } else {
                this.badWeight = false;
                this.$el.find("#exclamation-icon").hide();
            }

            if (totalWeight > 0) {
                window.appHelper.event_bus.trigger('completeness_group_weight_updated', weights);
            }
            this.$el.find(this.nameLanguageField).empty();

            var langCollection = new languagesCollection();
            langCollection.fetch({
                success: function (languages) {
                    _.each(languages.toJSON(), function (model) {
                        values[model.Name] = model.DisplayName;
                        if (self.selectedLanguage == model.Name) {
                            self.$el.find(self.nameLanguageField).append('<option value="' + model.Name + '" selected>' + model.DisplayName + '</option>');
                        } else {
                            self.$el.find(self.nameLanguageField).append('<option value="' + model.Name + '">' + model.DisplayName + '</option>');
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            if (this.badWeight) {
                this.$el.find("#exclamation-icon").show();
            } else {
                this.$el.find("#exclamation-icon").hide();
            }

            return this;
        }
    });

    return completenessGroupDetailView;
});
