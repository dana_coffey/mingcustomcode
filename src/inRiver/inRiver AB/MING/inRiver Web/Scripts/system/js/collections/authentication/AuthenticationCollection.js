define([
  'jquery',
  'underscore',
  'backbone',
  'models/authentication/AuthenticationModel'
], function ($, _, Backbone, AuthenticationModel) {
    var AuthenticationCollection = Backbone.Collection.extend({
        model: AuthenticationModel,
        initialize: function (models, options) {
            if (options != undefined) {
                this.userName = options.userName;
            } else {
                this.userName = "";
            }
        },
        url: function() {
            return '/api/authentication/' + this.userName;
        },
    });
 
  return AuthenticationCollection;
});
