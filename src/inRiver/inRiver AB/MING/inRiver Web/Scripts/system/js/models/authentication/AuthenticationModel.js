define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var AuthenticationModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        toString: function () {
            return this.get("Id");
        },
    });

    return AuthenticationModel;

});
