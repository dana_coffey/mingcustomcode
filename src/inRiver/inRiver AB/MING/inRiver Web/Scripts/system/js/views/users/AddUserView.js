define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-modal',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'models/user/UserModel',
  'collections/users/UsersCollection',
  'collections/roles/RolesCollection',
  'collections/authentication/AuthenticationCollection',
  'text!templates/home/modalTemplate.html'
], function ($, _, Backbone, Modal, alertify, inRiverUtil, UserModel, UsersCollection, RolesCollection, AuthenticationCollection, modalTemplate) {

    var AddUserView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.parent = data.data.parent;

            console.log("AddUserView init");
            this.model = new UserModel();
            this.render();
        },
        events: {
            "click button#addUser": "addUser",
            "click button#findUser": "findUser",
            "click button#closeModal": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        findUser: function () {
            var self = this;
            var authHandler = function (m, r) {
                if (r != undefined) {
                    self.$el.find("input[name='FirstName']").val(r[0]);
                    self.$el.find("input[name='LastName']").val(r[1]);
                    self.$el.find("input[name='Email']").val(r[2]);
                } else {
                    inRiverUtil.NotifyError("User not found", "No user found in Active Directory");
                }
            };

            var userName = this.$el.find("input[name='Username']").val();
            self.collection = new AuthenticationCollection([], { userName: userName });
            self.collection.fetch({
                success: authHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.addUser();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.addUser();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.addUser();
        },
        addUser: function () {
            this.authType = this.form.fields.AuthenticationType.editor.getValue();
            // Adding Forms User 
            if (this.authType == "Forms") {
                var errors = this.form.commit(); // runs schema validation
            }

            var rolesInput = this.form.fields.Roles.editor.getValue();
            if (rolesInput.length == 0) {
                var element = $("div.field-Roles");
                element.addClass("error");
                element.find('[data-error]').html("Required");
                errors = 'set';
            }

            if (!errors) {
                var self = this;

                this.model.save({
                    AuthenticationType: this.form.fields.AuthenticationType.editor.getValue(),
                    FirstName: this.form.fields.FirstName.editor.getValue(),
                    LastName: this.form.fields.LastName.editor.getValue(),
                    Username: this.form.fields.Username.editor.getValue(),
                    Email: this.form.fields.Email.editor.getValue(),
                    Settings: { Password: this.form.fields.Password.editor.getValue() },
                    Roles: _.map(this.form.fields.Roles.editor.getValue(), function (item) {
                        // this is a bit ugly but enough for the logics to work
                        // best option would be to assign en entire Role model here instead of just the id
                        // this is because of the odd checkboxes support in Backbone Forms
                        return { Id: item };
                    })
                }, {
                    type: 'Post',
                    success: function () {
                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();
                        $("#modalWindow").hide();
                        inRiverUtil.Notify("User has been added successfully");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        cancel: function () {
            $("#modalWindow").hide();

        },
       
        render: function (data) {
            var self = this;

            this.form = new Backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var Modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Create User" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Create User</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" id="addUser">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            // set Auth type
            var authHandler = function (m, r) {
                self.authType = r;
                if (r == "Mixed") {
                    self.$el.find(".form-group field-AuthenticationType").show();
                    self.$el.find("select[name='AuthenticationType']").removeAttr("disabled");
                } else {
                    self.$el.find(".form-group field-AuthenticationType").hide();
                    self.form.fields.AuthenticationType.editor.setValue(r);
                }
            };

            self.collection = new AuthenticationCollection();
            self.collection.fetch({
                success: authHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            self.form.on('AuthenticationType:change', function (form, editor) {
                var authType = editor.getValue();
                if (authType == "Forms") {
                    self.$el.find("#findUser").remove();
                    self.$el.find("input[name='FirstName']").removeAttr("disabled");
                    self.$el.find("input[name='LastName']").removeAttr("disabled");
                    self.$el.find("input[name='Password']").removeAttr("disabled");
                    self.$el.find("input[name='Password2']").removeAttr("disabled");
                } else {
                    var $buttonFind = $('<button class="btn" type="button" id="findUser">Find user</button>');
                    self.$el.find("input[name='Username']").after($buttonFind);
                    self.$el.find("input[name='FirstName']").attr("disabled", true);
                    self.$el.find("input[name='LastName']").attr("disabled", true);
                    self.$el.find("input[name='Password']").attr("disabled", true);
                    self.$el.find("input[name='Password2']").attr("disabled", true);
                }
            });

            //remove disabled when add
            this.$el.find("input[name='Username']").removeAttr("disabled");
            //this.$el.find("ul[id*=Roles]").removeClass("form-control");

            // populate the roles checkbox list... this is a bit of hack because Backbone.Forms didnt seem to support dynamic checkbox lists so well...
            var dataHandler = function (m, r) {
                self.form.fields.Roles.editor.setOptions(m);
                //self.form.fields.Roles.editor.setValue(_.pluck(self.model.get("Roles"), "Name")); // pluck will return an array of values that we set as selected
            };

            self.collection = new RolesCollection();
            self.collection.fetch({
                success: dataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            return this;
        }
    });

    return AddUserView;
});
