define([
  'jquery',
  'underscore',
  'backbone',
  'models/role/RoleModel'
], function($, _, Backbone, RoleModel){
  var RolesCollection = Backbone.Collection.extend({
      model: RoleModel,
      url: '/api/role'
  });
 
  return RolesCollection;
});
