﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/completeness/completenessCriteriaModel'
], function ($, _, backbone, completenessCriteriaModel) {
    var completenessCriteriaCollection = backbone.Collection.extend({
        model: completenessCriteriaModel,
        url: '/api/completeness/criteria'
    });

    return completenessCriteriaCollection;
});