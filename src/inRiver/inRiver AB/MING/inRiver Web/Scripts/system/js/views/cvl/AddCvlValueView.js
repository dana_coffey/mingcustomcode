define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-modal',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/cvl/CvlValuesView',
  'models/cvl/CvlValueModel',
  'collections/cvl/CvlCollection',
  'collections/languages/LanguagesCollection',
  'collections/cvl/CvlValueCollection',
    'text!templates/home/modalTemplate.html'
], function ($, _, backbone, Modal, alertify, inRiverUtil, cvlValuesView, cvlValueModel, cvlCollection, languagesCollection, cvlValueCollection, modalTemplate) {

    var addCvlValueView = backbone.View.extend({
        // el: '#edit-panel-wrap2',
        tagName: 'div',
        initialize: function (options) {
            this.undelegateEvents();
            this.model = new cvlValueModel();
            this.cvlId = options.cvlId;
            this.parentKey = options.parentKey;
            this.render(options.isLocaleString);
        },
        events: {
            "click #addCvlValue": "save",
            "click #closeModal": "cancel",
            "click": "onClick",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",

        },
        onClick: function () {
            // Prevent closing of edit panel
            event.stopPropagation();
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        cancel: function () {
            event.stopPropagation();
            $("#modalWindow").hide();
        },
        save: function() {
            event.stopPropagation();
            var self = this;
            var errors = this.form.commit(); // runs schema validation
            if (!errors) {
                this.model.save({
                    Id: -1,
                    CVLId: this.cvlId,
                    Key: this.form.fields.Key.editor.getValue(),
                    Index: this.form.fields.Index.editor.getValue(),
                    Value: this.form.fields.Value.editor.getValue(),
                    LSValue: this.form.fields.LSValue.editor.getValue(),
                    ParentKey: this.form.fields.ParentKey.editor.getValue(),
                    

                }, {
                    success: function() {
                        self.model.fetch({
                            success: function() {
                                window.appHelper.event_bus.trigger('cvlvaluecreated', self.model);
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        }); // to make sure we get a complete description of the model from server
                        $("#modalWindow").hide();
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        render: function (isLocaleString) {
            var self = this;

            this.form = new backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var Modal = backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            this.isLocaleString = isLocaleString;

            $("#addCvlValue").on('click', function () {
                self.save();
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add CVL Value" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add CVL Value</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addCvlValue">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            var onDataHandler = function (collection) {
                self.form.fields.ParentKey.editor.setOptions(collection);
            }
            var cvlCollect = new cvlCollection();
            cvlCollect.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.$el.find(".field-Languages").remove();

            if (!isLocaleString) {
                this.$el.find(".field-LSValue").remove();
            } else {
                this.$el.find(".field-Value").remove();

                // Fill an empty local string value based on system languages.
                this.languages = new languagesCollection();
                self = this;
                this.languages.fetch({
                    success: function() {
                        var jsonString = "{";
                        var setCommasOnPrevious = false;
                        $.each(self.languages.models, function(key, model) {
                            if (setCommasOnPrevious) {
                                jsonString = jsonString + ",";
                            }
                            jsonString = jsonString + "\"" + model.get("Name") + "\":[\"" + model.get("DisplayName") + "\",\"\"]";
                            setCommasOnPrevious = true;
                        });
                        jsonString = jsonString + "}";
                        var lsValue = JSON.parse(jsonString);
                        self.form.fields.LSValue.editor.setValue(lsValue);
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            // Remove disabled when add
            this.$el.find(".field-Keys").remove();
            this.$el.find("input[name='Key']").removeAttr("disabled");
            if (this.parentKey == null || this.parentKey == "") {
                this.$el.find(".field-ParentKey").remove();
            } else {
                // Populate Parent Id
                var onDataHandler2 = function () {
                  
                    this.cvlValueList = new cvlValueCollection([], { cvlId: self.parentKey });
                    this.cvlValueList.fetch({
                        success: function (m) {
                            self.form.fields.ParentKey.editor.setOptions(m);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
                this.collection = new cvlCollection();
                this.collection.fetch({
                    success: onDataHandler2,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            return this; // enable chained calls
        },
    });

    return addCvlValueView;
});
