define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'models/user/UserModel',
  'models/serversettings/settingsModel',
  'collections/users/UsersCollection',
  'views/users/UserView',
  'views/users/AddUserView',
  'text!templates/users/usersTemplate.html'
// ReSharper disable InconsistentNaming
], function ($, _, backbone, backgrid, inRiverUtil, gridDeleteItem, UserModel, SettingsModel, UsersCollection, UserView, AddUserView, usersTemplate) {
// ReSharper restore InconsistentNaming

    var userListView = backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.lockedForEditing = false;

            //Get User by id
            if (data && data.id) {
                var model = new UserModel();
                model.fetch({
                    data: { id: data.id },
                    success: (function () {
                        var view = new UserView({ model: model });
                        return view;
                    }),
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            //Get Users
            var that = this;
            var authTypeSetting = new SettingsModel({ Id: "AUTHENTICATION_TYPE" });
            authTypeSetting.fetch({
                success: function (result) {
                    if (result.attributes["ADFS"] != null) {
                        that.lockedForEditing = true;
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            var onDataHandler = function () {
                that.render();
            };

            that.collection = new UsersCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            that.collection.on('rowClicked', function (model) {

                event.stopPropagation();
                if (that.lockedForEditing) {
                    inRiverUtil.Notify("Users are not editable in inRiver. They are handled in your Active Directory.");
                    return true;
                }

                var modWindow = $("#modalWindow").find("div").html();
                var modalWindowOpen = $('#modalWindow').is(':visible');

                if (modalWindowOpen) {
                    return false;
                }

                var oldView = that._editCvlView;
                if (oldView != null) {
                    oldView.remove();
                    oldView.undelegateEvents();
                }

                //self.onEditRow(model);
                $("#edit-panel").slideDown(200, "swing", function () {
                });
                console.log("onLoadEditUserView");
                //this.onUnloadEditCvlView();

                that._editCvlView = new UserView({ model: model });
                $("#edit-panel").html(that._editCvlView.el);
                return true;
            });

            console.log("UserListView init");

            var columns = [
                { name: "Username", label: "Username", cell: "string" },
                { name: "FirstName", label: "First Name", cell: "string" },
                { name: "LastName", label: "Last Name", cell: "string" },
                { name: "Email", label: "Email", cell: "string" },
                { name: "RolesList", label: "Roles", cell: "string" },
                { name: "", cell: gridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });

            this.listenTo(backbone, 'clickedAnywhere', function () { this.onUnloadUserView(); });

        },
        events: {
            "click #newUser": "newUser",
            "click button#userDelete": "userDelete",
            "click #edit-panel": "onClick"
      },
        onClickedAnywhere: function () {
            this.closeEditPanel(this.unloadEditCvlView);
        },
        newUser: function () {
            if (this._modalView) {
                this._modalView.remove();
            }

            if (this.lockedForEditing) {
                inRiverUtil.Notify("Users are not handled in inRiver. They are handled in your Active Directory.");
                return true;
            }

            window.scrollTo(0, 0);
            this._modalView = new AddUserView({ data: { header: "Create User", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
            return true;
        },
        userDelete: function () {
            var selectedModels = this.grid.getSelectedModels();

            _.each(selectedModels, function (sModel) {
                sModel.destroy({
                    success: function () {
                        
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            });
        },
        onClick: function () {
            event.stopPropagation();
        },
        onUnloadUserView: function () {


            console.log("onUnloadEditUserView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;
        },
        render: function () {
            this.$el.html(usersTemplate);
            console.log("UserListView Render");
            this.$el.find("#users-list").append(this.grid.render().$el);
        }
    });

    return userListView;
});