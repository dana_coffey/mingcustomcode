define([
  'jquery',
  'underscore',
  'backbone',
  'alertify'
], function($, _, Backbone){

    var MenuTabView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(Backbone.history, 'route', this.onRouteChanged);
        },

        onRouteChanged: function () {

            //get current url hash part
            var hash = Backbone.history.getHash();

            //find the link that has a matching href attribute
            var $activeTab = $("a[href='#" + hash + "']");

            //if route matched one of the tabs, update tabs.
            if ($activeTab.is("a")) {

                // First remove selected status on all tabs
                $("#sidebar li a").removeClass("sidebar-active-bg");
                //$("#left-panel li #navigation-item-wrap").removeClass("left-panel-active2");

                // Then select the clicked tab
                $activeTab.addClass("sidebar-active-bg");
                //$("#left-panel li[route=" + route + "] #navigation-item-wrap").addClass("left-panel-active2");
            }
        }
    });

  return MenuTabView;
  
});
