define([
  'underscore',
  'backbone'
], function(_, backbone) {
    
    var settingsModel = backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/setting'
    });

    return settingsModel;

});
