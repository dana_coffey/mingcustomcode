define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var LockModel = Backbone.Model.extend({
        idAttribute: 'id',
        defaults: {
            //detailsLoaded: false
        },
        schema: { // Used by backbone forms extension
            Name: { type: 'Text', validators: ['required'], editorAttrs: { disabled: true } },
            Description: { type: 'Text', title: 'Description', validators: ['required'] },
            Type: { type: 'Text', title: 'Type', validators: ['required'] },
            Lockedby: { type: 'Text', title: 'Type', validators: ['required'] }
        }
    });

    return LockModel;

});
