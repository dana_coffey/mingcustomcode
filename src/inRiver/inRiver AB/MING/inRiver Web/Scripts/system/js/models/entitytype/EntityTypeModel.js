define([
  'underscore',
  'backbone',
  'deep-model'
], function(_, Backbone) {
    
    var EntityTypeModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        schema: { // Used by backbone forms extension
            EntityType: { type: 'Select', title: "Entity Type", options: [] }
        },
        toString: function () {

            
            return this.get("Name").stringMap[appHelper.userLanguage];
        },
    });

    return EntityTypeModel;

});
