define([
  'jquery',
  'underscore',
  'backbone',
  'models/restrictedfield/RestrictedFieldModel'
], function ($, _, Backbone, RestrictedFieldModel) {
  var RestrictedFieldsCollection = Backbone.Collection.extend({
      model: RestrictedFieldModel,
      url: '/api/restrictedfield'
  });
 
  return RestrictedFieldsCollection;
});
