define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'views/sidebar/SidebarView',
  'views/restrictedfields/AddRestrictedFieldView',
  'models/restrictedfield/RestrictedFieldModel',
  'collections/restrictedfields/RestrictedFieldsCollection',
  'text!templates/restrictedfields/restrictedTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, SidebarView, AddRestrictedFieldView, RestrictedFieldModel, RestrictedFieldsCollection, restrictedTemplate) {

    var RestrictedFieldListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new RestrictedFieldsCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            console.log("RestrictedFieldListView init");

            var columns = [
                { name: "EntityTypeId", label: "Entity Type", cell: "string" },
                { name: "CategoryId", label: "Category", cell: "string" },
                { name: "FieldTypeId", label: "Field Type", cell: "string" },
                { name: "RestrictionType", label: "Restriction", cell: "string" },
                { name: "RoleName", label: "Role", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });
        },

        events: {
            "click #newRestricted": "newRestricted"
        },
        newRestricted: function (model) {
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddRestrictedFieldView({ data: { header: "Add Restricted field", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        render: function () {
            this.$el.html(restrictedTemplate);
            console.log("RestrictedFieldListView Render");
            this.$el.find("#restricted-list").append(this.grid.render().$el);
            return this;
        }
    });

    return RestrictedFieldListView;
});
