define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'views/sidebar/SidebarView',
  'models/role/RoleModel',
  'collections/roles/RolesCollection',
  'views/roles/RoleView',
  'views/roles/AddRoleView',
  'text!templates/roles/rolesTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, SidebarView, RoleModel, RolesCollection, RoleView, AddRoleView, rolesTemplate) {

    var RoleListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            //Get Roles
            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new RolesCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            console.log("RoleListView init");

            that.collection.on('rowClicked', function (model, selected) {

                event.stopPropagation();
                var modWindow = $("#modalWindow").find("div").html();
                var modalWindowOpen = $('#modalWindow').is(':visible');


                if (modalWindowOpen) {
                    return false;
                }



                var oldView = this._editCvlView;
                if (oldView != null) {
                    oldView.remove();
                    oldView.undelegateEvents();
                }

                $("#edit-panel").slideDown(200, "swing", function () { });
                console.log("onLoadEditRoleView");
                that._editCvlView = new RoleView({ model: model });

                $("#edit-panel").html(that._editCvlView.el);
            });
            var columns = [
                { name: "Name", label: "Name", cell: "string" },
                { name: "Description", label: "Description", cell: "string" },
                { name: "PermissionList", label: "Permissions", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });


            this.listenTo(Backbone, 'clickedAnywhere', function () { this.onUnloadRoleView(); });
        },

        events: {
            "click #newRole": "newRole",
            "click button#roleDelete": "roleDelete",
            "click #edit-panel": "onClick"


        },
        newRole: function () {
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddRoleView({ data: { header: "Add Role", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();

        },
        roleDelete: function () {
            var selectedModels = this.grid.getSelectedModels();
            _.each(selectedModels, function (sModel) {
                sModel.destroy({
                    success: function (m) {
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            });
        },
        onUnloadRoleView: function (parameters) {
            console.log("onUnloadRoleView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                Backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;

        },
        onClick: function () {
            event.stopPropagation();

        },
        render: function () {
            this.$el.html(rolesTemplate);
            console.log("RoleListView Render");
            this.$el.find("#roles-list").append(this.grid.render().$el);
          //  return this;
        }
    });

    return RoleListView;
});
