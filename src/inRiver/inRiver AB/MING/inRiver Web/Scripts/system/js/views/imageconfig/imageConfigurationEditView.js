﻿define([
    'jquery',
    'underscore',
    'backbone',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'models/imageconfig/imageConfigurationModel',
    'collections/imageconfig/imageConfigurationCollection',
    'views/imageconfig/imageConfigurationAddModifyView',
    'text!templates/imageconfig/imageConfigurationEditTemplate.html'
], function($, _, backbone, alertify, inRiverUtil, imageConfigurationModel, imageConfigurationCollection, imageConfigurationAddModifyView, imageConfigurationEditTemplate) {

    var imageConfigurationEditView = backbone.View.extend({
        initialize: function (data) {
            this.render();
            this.parent = data.parent;
            this.undelegateEvents();
        },
        events: {
            "click button#closeButton": "onClose",
            "click button#saveButton": "saveConfig",
            "click button#copyButton": "copyConfig",
            "click": "onClick",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown"
        },
        onClose: function () {
            $("#edit-panel").slideUp(200, "swing");
            Backbone.trigger('closeEditPanel');
        },
        onKeyUp: function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (e.keyCode == 9) {
                return;
            }

            // Enter
            if (e.keyCode == 13) {
                this.saveConfig();
                return;
            }
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.ctrlKey || e.metaKey) {
                switch (String.fromCharCode(e.which).toLowerCase()) {
                    case 's':
                        e.preventDefault();
                        this.saveConfig();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.saveConfig();
        },
        copyConfig: function(e) {
            e.stopPropagation();
            inRiverUtil.NotifyConfirm("Copy Exiting Configuration", "You are about to create a copy, with the shown parameters.<br/><br/>Are you sure?.", this.doUpdate, this);
        },
        doUpdate: function(self) {
            self.model.set("Id", null);
            
            this.addView = new imageConfigurationAddModifyView({ header: "Add Configuration", parent: self });
            $("#modalWindow").html(this.addView.el);
            $("#modalWindow").show();
        },
        saveConfig: function () {
            var errors = this.form.commit(); // runs schema validation
            if (!errors) {
                var that = this;
                var uniqueCollection = new imageConfigurationCollection();
                uniqueCollection.url = '/api/imageconfiguration/name/' + this.form.fields.Name.editor.getValue() + "/extension/" + this.form.fields.Extension.editor.getValue();
                uniqueCollection.fetch({
                    success: function (models) {
                        if (models.length > 0 && that.model.get("Id") == null) {
                            // It's not unique combo, but only when create new.
                            inRiverUtil.NotifyError("Not unique configuration.", "The name/extension combination already exist in the system.<br/><br/>Cannot proceed with the request.");
                            that.model.fetch(); // refresh the model.
                            return;
                        }

                        var self = that;
                        that.model.urlRoot = '/api/imageconfiguration';
                        that.model.save({
                            Name: that.form.fields.Name.editor.getValue(),
                            Extension: that.form.fields.Extension.editor.getValue(),
                            OutputExtension: that.form.fields.OutputExtension.editor.getValue(),
                            Arguments: that.form.fields.Arguments.editor.getValue()
                        }, {
                            success: function () {
                                inRiverUtil.Notify("Configuration has been updated successfully");
                                self.parent.refreshList();
                                $("#edit-panel").slideUp(200, "swing");
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onClick: function () {
            event.stopPropagation();
        },
        render: function () {
            this.$el.html(imageConfigurationEditTemplate);
            this.form = new Backbone.Form({ model: this.model });
            this.$el.find("#image-configuration-content").append(this.form.render().el);
            
        }

    });

    return imageConfigurationEditView;
});
