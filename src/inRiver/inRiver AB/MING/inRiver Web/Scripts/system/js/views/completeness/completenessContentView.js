﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'models/completeness/completenessRuleModel',
    'models/completeness/completenessGroupModel',
    'models/completeness/recalculateCompletenessModel',
    'collections/completeness/completenessActionCollection',
    'collections/completeness/completenessCriteriaCollection',
    'views/completeness/completenessDefinitionDetailView',
    'views/completeness/completenessGroupDetailView',
    'views/completeness/completenessRuleDetailView',
    'views/completeness/completenessActionListView',
    'text!templates/completeness/completenessContentTemplate.html'
], function ($, _, backbone, inRiverUtil, completenessRuleModel, completenessGroupModel, recalculateCompletenessModel, completenessActionCollection, completenessCriteriaCollection, completenessDefinitionDetailView, completenessGroupDetailView, completenessRuleDetailView, completenessActionListView, completenessContentTemplate) {

    var completenessContentView = backbone.View.extend({
        initialize: function (options) {
            this.model = options.model;
            this.parentId = options.parentId;
            this.definitionId = options.definitionId;
            this.nodeType = options.type;
            this.action = options.action;

            var displayName;

            if (this.action == "add") {
                displayName = "Add new " + this.nodeType;
            } else {
                displayName = "Update " + this.model.get("DisplayName");
            }
            this.inputData = { DisplayName: displayName, Type: this.nodeType, Adding: this.model.get("Id") == 0 };

            this.ruleWeights = {};
            this.groupWeights = {};
        },
        events: {
            "click #delete-button": "onDeletePressed",
            "click #save-button": "onSavePressed",
            "click #undo-button": "onUndoNodePressed",
            "click #close-button": "onClosePressed",
            "click #tab-details": "openDetails",
            "click #tab-actions": "openActions",
            "click #recalculate-button": "onRecalculateCompleteness"
        },
        onDeletePressed: function (e) {
            e.stopPropagation();
            if (this.model.get("Id") == 0) {
                inRiverUtil.Notify("Action not applicable.");
                return;
            }

            window.appHelper.event_bus.trigger('completeness_node_deleted');
        },
        onClosePressed: function (e) {
            e.stopPropagation();

            var self = this;
            inRiverUtil.NotifyConfirm("Confirm action", "You are about to cancel the creation and go back to previvous page.</br></br>Are you sure?", function () {
                switch (self.nodeType) {
                    case "Definition":
                        self.goTo("/completeness");
                        break;
                    case "Group":
                        self.goTo("/completeness/def/" + self.definitionId);
                        break;
                    case "Rule":
                        self.goTo("/completeness/def/" + self.definitionId + "/grp/" + self.parentId);
                        break;
                }
            });
        },
        onRecalculateCompleteness: function (e) {
            e.stopPropagation();

            var recalculateModel = new recalculateCompletenessModel();
            recalculateModel.url = "/api/completeness/definition/" + this.model.get("Id") + "/recalculate";
            recalculateModel.fetch({
                success: function () {
                    inRiverUtil.Notify("The completeness has been recalculcated.");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onSavePressed: function (e) {
            e.stopPropagation();
            var self = this;
            var nameOk = false;
            _.each(this.model.get("Name").stringMap, function (value, key) {
                if (value != "") {
                    nameOk = true;
                }
            });

            if (!nameOk) {
                inRiverUtil.NotifyError("Missing name", "You need to have to type in a name.");
                return;
            }

            if (this.nodeType == "Rule") {
                this.model.urlRoot = "/api/completeness/group/" + this.parentId + "/rule";
                if (this.model.get("Settings").length == 0) {
                    inRiverUtil.NotifyError("Missing settings", "You need to have a rule type.");
                    return;
                }
            }

            var totalWeight = 0;
            if (this.nodeType == "Group") {
                this.model.set("DefinitionId", this.parentId);
                for (var id in this.ruleWeights) {
                    totalWeight += this.ruleWeights[id];
                }

                if (totalWeight > 100) {
                    inRiverUtil.NotifyError("Total weight too high", "The weight of the group are not allowed to exceed 100%. <br/>Please correct your inputs and try again.");
                    return;
                }

                if (this.model.attributes.Rules.length > 0 && totalWeight < 100) {
                    inRiverUtil.NotifyError("Total weight too low", "Note that the weight is less than 100%.");
                }
            }

            if (this.nodeType == "Definition") {
                totalWeight = 0;
                for (var gId in this.groupWeights) {
                    totalWeight += this.groupWeights[gId];
                }

                if (totalWeight > 100) {
                    inRiverUtil.NotifyError("Total weight too high", "The weight of the definition are not allowed to exceed 100%. <br/>Please correct your inputs and try again.");
                    return;
                }

                if (this.model.attributes.Groups.length > 0 && totalWeight < 100) {
                    inRiverUtil.NotifyError("Total weight too low", "Note that the weight is less than 100%.");
                }
            }

            this.ruleWeightTotalCount = 0;
            this.ruleWeightDoneCount = 0;
            for (var rwId in this.ruleWeights) {
                this.ruleWeightTotalCount++;
                var ruleModel = new completenessRuleModel();
                ruleModel.url = "/api/completeness/group/" + this.model.get("Id") + "/rule/" + rwId;
                ruleModel.fetch({
                    success: function (model) {
                        model.set("Weight", self.ruleWeights[model.get("Id")]);
                        model.save({}, {
                            success: function () {
                                self.ruleWeightDoneCount++;
                                self.afterSave();
                            },
                            error: function (err, response) {
                                inRiverUtil.OnErrors(err, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            this.groupWeightTotalCount = 0;
            this.groupWeightDoneCount = 0;
            for (var groupId in this.groupWeights) {
                this.groupWeightTotalCount++;
                var groupModel = new completenessGroupModel();
                groupModel.url = "/api/completeness/group/" + groupId;
                groupModel.fetch({
                    success: function (model) {
                        model.set("Weight", self.groupWeights[model.get("Id")]);
                        model.save({}, {
                            success: function () {
                                self.groupWeightDoneCount++;
                                self.afterSave();
                            },
                            error: function (err, response) {
                                inRiverUtil.OnErrors(err, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            this.modelSaved = false;
            this.model.save({}, {
                success: function () {
                    self.modelSaved = true;
                    self.afterSave();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        afterSave: function () {
            if (this.ruleWeightTotalCount == this.ruleWeightDoneCount && this.groupWeightTotalCount == this.groupWeightDoneCount && this.modelSaved) {
                inRiverUtil.Notify("The changes have been saved");
                var tab = "";
                if (window.appSession.completenessDetailTabName) {
                    tab = "/" + window.appSession.completenessDetailTabName;
                }

                switch (this.nodeType) {
                    case "Definition":
                        this.goTo("/completeness/def/" + this.model.get("Id") + tab);
                        break;
                    case "Group":
                        this.goTo("/completeness/def/" + this.definitionId + "/grp/" + this.model.get("Id") + tab);
                        break;
                    case "Rule":
                        this.goTo("/completeness/def/" + this.definitionId + "/grp/" + this.parentId + "/rule/" + this.model.get("Id") + tab);
                        break;
                }
            }
        },
        onUndoNodePressed: function (e) {
            e.stopPropagation();

            if (this.model.get("Id") == 0) {
                return;
            }

            var self = this;

            this.model.url = "/api/completeness/group/" + this.parentId + "/rule/" + this.model.get("Id");

            inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want to undo the changes?", function () {
                self.model.fetch({
                    success: function (model) {
                        inRiverUtil.Notify("The changes have been discarded");
                        self.inputData = { DisplayName: model.get("DisplayName"), Type: self.nodeType };
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            });
        },
        activateTab: function (newTab) {
            if (newTab == null) {
                return;
            }

            window.appSession.completenessDetailTabName = newTab;

            var tabDetails = this.$el.find("#tab-details");
            var tabActions = this.$el.find("#tab-actions");
            if (tabDetails != null && tabActions != null) {
                tabDetails.removeClass("completeness-content-tab-button-active");
                tabActions.removeClass("completeness-content-tab-button-active");

                if (newTab == "tab-details") {
                    tabDetails.addClass("completeness-content-tab-button-active");
                }

                if (newTab == "tab-actions") {
                    tabActions.addClass("completeness-content-tab-button-active");
                }
            }
        },
        openDetails: function () {
            var self = this;
            this.activateTab("tab-details");
            var container = this.$el.find("#completeness-content-info-container");
            if (container == null) {
                return;
            }

            container.empty();
            switch (this.nodeType) {
                case "Definition":
                    container.append(new completenessDefinitionDetailView({ model: this.model, action: this.action }).el);
                    break;
                case "Group":
                    container.append(new completenessGroupDetailView({ model: this.model, action: this.action, parentId: this.parentId }).el);
                    break;
                case "Rule":
                    var criterias = new completenessCriteriaCollection();
                    criterias.fetch({
                        success: function () {
                            container.append(new completenessRuleDetailView({ model: self.model, action: self.action, parentId: self.parentId, criterias: criterias }).el);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
            };
        },
        openActions: function () {
            var self = this;

            this.activateTab("tab-actions");
            this.actionCollection = new completenessActionCollection();
            if (this.nodeType == "Definition") {
                this.actionCollection.url = "/api/completeness/action/definition/" + this.definitionId;
            }
            if (this.nodeType == "Group") {
                this.actionCollection.url = "/api/completeness/action/definition/" + this.definitionId + "/group/" + this.model.get("Id");
            }
            if (this.nodeType == "Rule") {
                this.actionCollection.url = "/api/completeness/action/definition/" + this.definitionId + "/rule/" + this.model.get("Id");
            }

            this.actionCollection.fetch({
                success: function () {
                    var container = self.$el.find("#completeness-content-info-container");
                    if (!container) {
                        return;
                    }

                    var groupId = 0;
                    var ruleId = 0;

                    if (self.nodeType == "Group") {
                        groupId = self.model.get("Id");
                    }
                    if (self.nodeType == "Rule") {
                        ruleId = self.model.get("Id");
                    }

                    container.empty();
                    container.append(new completenessActionListView({
                        actionCollection: self.actionCollection,
                        action: self.action,
                        type: self.nodeType,
                        definitionId: self.definitionId,
                        groupId: groupId,
                        ruleId: ruleId
                    }).render().el);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onCompletenessContentUpdatedFired: function (model) {
            this.model = model;
        },
        onGroupWeightChanged: function (ruleWeights) {
            this.ruleWeights = ruleWeights;
        },
        onDefinitionWeightChanged: function (groupWeights) {
            this.groupWeights = groupWeights;
        },
        render: function () {
            this.$el.html(_.template(completenessContentTemplate, { data: this.inputData }));

            this.stopListening(window.appHelper.event_bus);
            if (this.nodeType == "Group") {
                this.listenTo(window.appHelper.event_bus, 'completeness_group_weight_updated', this.onGroupWeightChanged);
            }
            if (this.nodeType == "Definition") {
                this.listenTo(window.appHelper.event_bus, 'completeness_definition_weight_updated', this.onDefinitionWeightChanged);
            }

            this.listenTo(window.appHelper.event_bus, 'completeness_content_updated', this.onCompletenessContentUpdatedFired);

            if (window.appSession.completenessDetailTabName == "tab-actions") {
                this.openActions();
            } else {
                this.openDetails();
            }

            return this;
        }
    });

    return completenessContentView;
});
