// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'extensions/MenuTabView',
  'jquery-ui',
  'alertify',
  'views/home/HomeView',
  'views/users/UserListView',
  'views/roles/RoleListView',
  'views/permissions/PermissionListView',
  'views/locks/LockListView',
  'views/languages/LanguageListView',
  'views/model/ModelListView',
  'views/cvl/CvlListView',
  'views/restrictedfields/RestrictedFieldListView',
  'views/settings/SettingListView',
  'views/menu/MenuView',
  'views/imageconfig/imageConfigurationListView',
  'views/completeness/completenessStructureView',
  'views/htmltemplates/HtmlTemplatesView',
  'views/jobs/JobListView',
  'sharedjs/views/topheader/topHeaderView',
  'views/footer/FooterView'
], function ($, _, backbone, menuTabView, jqueryui, alertify, homeView, userListView, roleListView, permissionListView, lockListView, languageListView, modelListView,
    cvlListView, restrictedFieldListView, settingListView, menuView, imageConfigurationListView, completenessStructureView, htmlTemplatesView, jobListView, topHeaderView, footerView) {
  
  var AppRouter = backbone.Router.extend({
    routes: {
        // Define some URL routes
      "home": "home",
      "users(/:id)": 'users',
      "users/add": "addUser",
      "users/:id": "editUser",
      "cvl(/:id)": "cvl",
      "locks(/:id)": "locks",
      "languages(/:id)": "languages",
      "model(/:id)": "model",
      "roles(/:id)": "roles",
      "permissions(/:id)": "permissions",
      "users/:id/edit": "users",
      "completeness": "completeness",
      "completeness/def/:defId(/:tab)": "completenessdefinitiondetails",
      "completeness/def/:defId/grp/:groupId(/:tab)": "completenessgroupdetails",
      "completeness/def/:defId/grp/:groupId/rule/:ruleId(/:tab)": "completenessruledetails",
      "restrictedfields(/:id)": "restrictedfields",
      "settings(/:id)": "settings",
      "usersettings(/:id)": "usersettings",
      "imageconfig(/:id)": "imageconfig",
      "htmltemplates(/:mode)": "htmltemplates",
      "jobs(/:id)": "jobs",

      // Default
      '*actions': 'home',
    },
    switchView: function (newView) {
        if (this._currentView) {
            this._currentView.close();
            this._currentView.remove();
        }
        window.scrollTo(0, 0);
        this._currentView = newView;
        $("#main-container").html(this._currentView.el);
    },
  });
    
    var initialize = function() {
        console.log("init router");
        var appRouter = new AppRouter;
        window.appHelper.event_bus = _({}).extend(backbone.Events);

        var headerView = new topHeaderView(({ app: "System", icon: "fa-cog" }));
        
        appRouter.on('route:users', function () {
            this.switchView(new userListView());
        });

        appRouter.on('route:editUser', function (id) {
            // Note the variable in the route definition being passed in here
            this.switchView(new userListView({ id: id }));
        });

        appRouter.on('route:roles', function () {
            this.switchView(new roleListView());
        });

        appRouter.on('route:permissions', function () {
            this.switchView(new permissionListView());
        });

        appRouter.on('route:locks', function () {
            this.switchView(new lockListView());
        });

        appRouter.on('route:languages', function () {
            this.switchView(new languageListView());
        });

        appRouter.on('route:model', function () {
            this.switchView(new modelListView());
        });

        appRouter.on('route:cvl', function () {
            this.switchView(new cvlListView());
        });

        appRouter.on('route:completeness', function () {
            window.appSession.completenessDefinitionId = undefined;
            window.appSession.completenessGroupId = undefined;
            window.appSession.completenessRuleId = undefined;
            window.appSession.completenessDetailTabName = undefined;

            this.switchView(new completenessStructureView());
        });
        appRouter.on('route:completenessdefinitiondetails', function (defId, tab) {
            window.appSession.completenessDefinitionId = defId;
            window.appSession.completenessGroupId = undefined;
            window.appSession.completenessRuleId = undefined;

            if (tab) {
                tab = tab.substring(tab.lastIndexOf("-") + 1);
                window.appSession.completenessDetailTabName = "tab-" + tab;
            } else {
                window.appSession.completenessDetailTabName = "tab-details";
            }

            this.switchView(new completenessStructureView());
        });
        appRouter.on('route:completenessgroupdetails', function (defId, groupId, tab) {
            window.appSession.completenessDefinitionId = defId;
            window.appSession.completenessGroupId = groupId;
            window.appSession.completenessRuleId = undefined;

            if (tab) {
                tab = tab.substring(tab.lastIndexOf("-") + 1);
                window.appSession.completenessDetailTabName = "tab-" + tab;
            } else {
                window.appSession.completenessDetailTabName = "tab-details";
            }

            this.switchView(new completenessStructureView());
        });
        appRouter.on('route:completenessruledetails', function (defId, groupId, ruleId, tab) {
            window.appSession.completenessDefinitionId = defId;
            window.appSession.completenessGroupId = groupId;
            window.appSession.completenessRuleId = ruleId;

            if (tab) {
                tab = tab.substring(tab.lastIndexOf("-") + 1);
                window.appSession.completenessDetailTabName = "tab-" + tab;
            } else {
                window.appSession.completenessDetailTabName = "tab-details";
            }

            this.switchView(new completenessStructureView());
        });
        appRouter.on('route:restrictedfields', function () {
            this.switchView(new restrictedFieldListView());
        });

        appRouter.on('route:settings', function () {
            this.switchView(new settingListView());
        });

        appRouter.on('route:imageconfig', function () {
            this.switchView(new imageConfigurationListView());
        });

        appRouter.on('route:htmltemplates', function (mode) {
            this.switchView(new htmlTemplatesView({ mode: mode }));
        });

        appRouter.on('route:jobs', function () {
            this.switchView(new jobListView());
        });

        appRouter.on('route:home', function (id) {
            // We have no matching route, lets display the home page 
            this.switchView(new cvlListView({ modelToEdit: id }));
        });

        var menu = new menuView();

        backbone.history.start();

        var tabView = new menuTabView();

        $(document).ready(function () {
            initApp();
        });

        backbone.View.prototype.goTo = function (loc, changeurl) {
            if (changeurl == undefined) {
                changeurl = true;
            }
            appRouter.navigate(loc, changeurl);
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        Backbone.View.prototype.close = function () {

            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };
  };
  return { 
    initialize: initialize
  };
    
  function initApp() {
      initializeSettings();
      alertify.set({ buttonReverse: true });

      // Detect clicks anywhere, a bit ugly, but used for closing the edit panel on outside click
      $('html').click(function () {
          backbone.trigger("clickedAnywhere");
      });
  }

  function initializeSettings() {
  }
});
