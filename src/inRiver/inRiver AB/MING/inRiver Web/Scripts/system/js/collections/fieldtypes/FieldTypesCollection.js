define([
  'jquery',
  'underscore',
  'backbone',
  'models/fieldtype/FieldTypeModel'
], function ($, _, Backbone, FieldTypeModel) {
  var FieldTypesCollection = Backbone.Collection.extend({
      model: FieldTypeModel
  });
 
  return FieldTypesCollection;
});
