define([
  'jquery',
  'underscore',
  'backbone',
  'models/language/LanguageModel'
], function($, _, backbone, languageModel){
  var languagesCollection = backbone.Collection.extend({
      model: languageModel,
      url: '/api/language'
  });
 
  return languagesCollection;
});
