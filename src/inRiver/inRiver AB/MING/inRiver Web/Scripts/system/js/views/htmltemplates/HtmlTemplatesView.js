define([
  'jquery',
  'underscore',
  'backbone',
  'simpleAjaxUploader',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'multiple-select',
  'jquery-filedownload',
  'codemirror',
  'codemirror/mode/htmlmixed/htmlmixed',
  'collections/entitytype/EntityTypeCollection',
  'sharedjs/collections/channel/ChannelCollection',
  'collections/roles/RolesCollection',
  'text!templates/htmltemplates/HtmlTemplatesTemplate.html',
  'text!templates/htmltemplates/HtmlTemplateItemTemplate.html',
  'text!templates/htmltemplates/HtmlTemplateEditViewTemplate.html',
  'text!templates/htmltemplates/HtmlTemplatesContentStoreTemplateEditViewTemplate.html',
  'text!templates/htmltemplates/HtmlTemplatesPreviewViewTemplate.html',
  'text!templates/htmltemplates/HtmlTemplatesNavigationTemplate.html',

], function ($, _, Backbone, simpleAjaxUploader, inRiverUtil,
    modalPopup, multipleSelect, jqueryFiledownload, CodeMirror, codeMirrorModeHtmlMixed,
    EntityTypeCollection, ChannelCollection, RolesCollection,
    htmlTemplatesTemplate, htmlTemplateItemTemplate, htmlTemplateEditViewTemplate,
    htmlTemplatesContentStoreTemplateEditViewTemplate, htmlTemplatesPreviewViewTemplate,
    navigationTemplate) {

    var HtmlTemplatesView = Backbone.View.extend({
        initialize: function (options) {
            this._currentSettingsView = null;
            this._currentMode = options.mode ? options.mode : "inriver";

            if (this._currentMode === "xconnect") {
                this.collection = new Backbone.Collection(null, { url: "/api/xconnect/htmltemplate", model: Backbone.DeepModel });
            } else {
                this.collection = new Backbone.Collection(null, { url: "/api/htmltemplate", model: Backbone.DeepModel.extend({ idAttribute: "Id" }) });
            }
            this.listenToOnce(this.collection, "reset", this.onInit);
            //this.listenTo(this.collection, "error", this.onInit);
            this.collection.fetch({ reset: true });

            this.entityTypeCollection = new EntityTypeCollection();
            this.listenToOnce(this.entityTypeCollection, "reset", this.onInit);
            this.entityTypeCollection.fetch({ reset: true });

            this.channelCollection = new ChannelCollection();
            this.listenToOnce(this.channelCollection, "reset", this.onInit);
            this.channelCollection.fetch({ reset: true });

            this.rolesCollection = new RolesCollection();
            this.listenToOnce(this.rolesCollection, "reset", this.onInit);
            this.rolesCollection.fetch({ reset: true });
        },
        onInit: function (collection) {
            var that = this;
            collection.isLoaded = true;
            if (this.collection.isLoaded && this.entityTypeCollection.isLoaded && this.channelCollection.isLoaded && this.rolesCollection.isLoaded) {
                //this.listenTo(this.collection, "sync destroy", this.updateUi);
                this.render();

                this.uploader = new ss.SimpleUpload({
                    button: this.$el.find("#upload-template-content"),
                    url: '/api/htmltemplate/filecontents/',
                    //name: 'uploadfile', // Parameter name of the uploaded file
                    multipart: true,
                    onSubmit: function (filename, extension) {
                        that.$el.find("#file-upload-spinner").show();
                    },
                    onComplete: function (filename, response) {
                        that.$el.find("#file-upload-spinner").hide();
                        if (!response) {
                            alert('upload failed');
                        } else {
                            //that.$el.find("#template-content").val(response);
                            that._templateContentEditor.setValue(response);
                            that.setIsUnsaved(true);
                        }
                    }
                });
                
                // Set up codemirror editor
                that._templateContentEditor = CodeMirror.fromTextArea(that.$el.find("#template-content")[0], {
                    lineNumbers: true,
                    mode: "htmlmixed"
                });
                that._templateContentEditor.setSize(500, 400);
                that._templateContentEditor.on("change", function () {
                    that.onTemplateModified();
                });

                that.updateUi();
                that.listenTo(that.collection, "sync destroy", that.updateUi);
                               
            }
        },
        events: {
            "click #add-template": "onAddTemplate",
            "change #templates-dropdown": "onChangeTemplate",
            "click #save-changes": "onSaveChanges",
            "click #delete": "onDeleteTemplate",
            "input #template-name": "onTemplateModified",
            "input #template-localized-name": "onTemplateModified",
            "change #template-type": "onTypeChanged",
            "change #template-localized-name-languages": "onLocalizedNameLanguageChanged",
            "click #expand-collapse-template-source-view": "onToggleSourceViewExpansion",
        },
        onAddTemplate: function () {
            var type = this._currentMode === "xconnect" ? "ContentStoreTemplate" : "EnrichPDFTemplate";
            this.addTemplate(type);
        },
        onDeleteTemplate: function () {
            var that = this;
            inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete this template?", function () {
                var modelToDestroy = that.model;
                that.model = null;  // To make sure we don't render the model we are going to destroy
                modelToDestroy.destroy();
                that._templateContentEditor.clearHistory();
            });
        },
        onChangeTemplate: function (e) {
            var templateId = $(e.currentTarget).val();
            this.model = this.collection.get(templateId);
            this.updateUi();
            this._templateContentEditor.clearHistory();
        },
        onTypeChanged: function (e) {
            // Force reload of template type specific UI
            this.model.set("Type", this.$el.find("#template-type").val());
            if (this._currentSettingsView) this._currentSettingsView.close();
            this._currentSettingsView = null;
            this.updateTemplateSettingsView(this.model);
            this.updateUi();
            this.onTemplateModified();
        },
        onLocalizedNameLanguageChanged: function (e) {
            this.$el.find("#template-localized-name").val(this.model.getEx("LocalizedName." + this.$el.find("#template-localized-name-languages").val() + ".1"));
        },
        onToggleSourceViewExpansion: function(e) {
            if (this.$el.find("#template-settings-wrap").is(":visible")) {
                this.$el.find("#template-settings-wrap").hide();
                this.$el.find("#expand-collapse-template-source-view").html("<<");
                this.$el.find("#html-template-properties-wrap").hide();
                this._templateContentEditor.setSize(1100, 600);
            } else {
                this.$el.find("#template-settings-wrap").show();
                this.$el.find("#expand-collapse-template-source-view").html(">>");
                this.$el.find("#html-template-properties-wrap").show();
                this._templateContentEditor.setSize(500, 400);
            }
        },
        onTemplateModified: function () {
            var selectedLocalizedNameLang = this.$el.find("#template-localized-name-languages").val();
            if (selectedLocalizedNameLang) {
                var localizedName = this.$el.find("#template-localized-name").val();
                this.model.set("LocalizedName." + selectedLocalizedNameLang + ".1", localizedName);
            }
            this.setIsUnsaved(true);
        },
        setIsUnsaved: function (isUnsaved) {
            if (isUnsaved) {
                this.$el.find("#save-changes").show();
            } else {
                this.$el.find("#save-changes").hide();
            }
        },
        addTemplate: function (type) {
            var that = this;
            inRiverUtil.Prompt("Enter a name for your new Template", function (e, name) {
                if (e) {
                    that.model = that.collection.create({
                        "type": type,
                        "Name": name,
                        "content": ""
                    });
                    that.setIsUnsaved(true);
                    that._templateContentEditor.clearHistory();
                }
            });
        },
        /*editTemplate: function (model) {
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new HtmlTemplateEditView({
                model: model
            }), {
                header: "Edit HTML Template",
                size: "large",
                onSave: this.onEditPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onAddPopupSave: function (popup, subView) {
            var errors = subView.updateModel();
            if (!errors) {
                this.collection.create(subView.model.toJSON(), {
                    method: "POST",
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {

            }
        },
        onEditPopupSave: function (popup, subView) {
            var errors = subView.updateModel();
            if (!errors) {
                subView.model.save(null, {
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                if (window.appSession.isXConnectConfigured) {
                    this.updateTemplateOnXConnectServer(subView.model);
                }
                popup.close();
            } else {

            }
        },*/
        removeTemplate: function (model) {
            inRiverUtil.NotifyConfirm("Confirm Delete HTML Template", "Do you want remove this HTML template?", function (e) {
                model.destroy({
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }, this);
        },
        onClose: function () {
            $("#context-menu-container-left").html("");
        },
        updateTemplateSettingsView: function (model) {
            if (this._currentSettingsView) {
                if (this._currentSettingsView.model.id !== model.id) {
                    this._currentSettingsView.close();
                    this._currentSettingsView = null;
                }
            }
            if (!this._currentSettingsView) {
                if (this.isContentStoreTemplate(model)) { // Content store preview 
                    this._currentSettingsView = new HtmlTemplatesContentStoreTemplatePreviewView({ model: model, parent: this });
                    this.$el.find("#template-settings-wrap").html(this._currentSettingsView.el);
                } else {
                    this._currentSettingsView = new HtmlTemplatesPreviewView({ model: model, parent: this });
                    this.$el.find("#template-settings-wrap").html(this._currentSettingsView.el);
                }
            }
        },
        onSaveChanges: function () {
            // Let the settings view do stuff first
            this._currentSettingsView.save();

            // Save template
            var content = this._templateContentEditor.getValue();
            this.model.save({
                "Name": this.$el.find("#template-name").val(),
                "Type": this.$el.find("#template-type").val(),
                "Content": content //this.$el.find("#template-content").val()
            }, {
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.setIsUnsaved(false);
        },
        isContentStoreTemplate: function (model) {
            var type = model.getEx("Type");
            if (type === "ContentStoreTemplate" || type === "ContentStoreCoverPage" || type === "ContentStoreEndPage") {
                return true;
            }
            return false;
        },
        updateUi: function () {
            var that = this;

            // Show current route in the top header navigation area
            $("#header-navigation-links-wrap a").removeClass("current");
            if (this._currentMode === "xconnect") {
                $("#header-navigation-links-wrap a[href='#htmltemplates/xconnect']").addClass("current");
            } else {
                $("#header-navigation-links-wrap a[href='#htmltemplates']").addClass("current");
            }

            if (!this.model) {
                if (this.collection.length > 0) {
                    this.model = this.collection.models[0];
                } else {
                    this.$el.find("#html-templates-edit-wrap").hide();
                    return;
                }
            }
            //this.currentTemplateId = parseInt(this.$el.find("#templates-dropdown").val());

            this.$el.find("#html-templates-edit-wrap").show();

            if (this.model.getEx("Type") === "EditTemplate") {
                this.$el.find("#display-name-row").show();
                var selectedLang = this.$el.find("#template-localized-name-languages").val();
                this.$el.find("#template-localized-name-languages").empty();
                _.each(this.model.getEx("LocalizedName"), function (x, key) {
                    var tmpEl = $("<option>").attr("value", key).text(x[0]);
                    that.$el.find("#template-localized-name-languages").append(tmpEl);
                });
                if (selectedLang) {
                    this.$el.find("#template-localized-name-languages").val(selectedLang);
                }
                this.$el.find("#template-localized-name").val(this.model.getEx("LocalizedName." + this.$el.find("#template-localized-name-languages").val() + ".1"));
            } else {
                this.$el.find("#template-localized-name").val("");
                this.$el.find("#display-name-row").hide();
            }

            var selEl = this.$el.find("#templates-dropdown");
            selEl.empty();
            this.collection.each(function (model) {
                var optionEl = $("<option></option>").attr("value", model.id).text(model.getEx("Name"));
                selEl.append(optionEl);
            });

            selEl = this.$el.find("#template-type");
            selEl.empty();
            if (this._currentMode === "xconnect") {
                selEl.append($("<option></option>").attr("value", "ContentStoreTemplate").text("Content Store Template"));
                selEl.append($("<option></option>").attr("value", "ContentStoreCoverPage").text("Content Store Cover Page"));
                selEl.append($("<option></option>").attr("value", "ContentStoreEndPage").text("Content Store End Page"));
            } else {
                selEl.append($("<option></option>").attr("value", "EnrichPDFTemplate").text("Enrich PDF Template"));
                selEl.append($("<option></option>").attr("value", "EnrichPreviewTemplate").text("Enrich Preview Template"));
                selEl.append($("<option></option>").attr("value", "PlanReleasePreviewTemplate").text("Plan & Release Preview Template"));
                selEl.append($("<option></option>").attr("value", "EditTemplate").text("Edit Template"));
            }

            this.$el.find("#templates-dropdown").val(this.model.id);

            //var model = this.collection.get(this.currentTemplateId);
            if (this.model.getEx("Content")) {
                if (this._templateContentEditor.getValue() !== this.model.getEx("Content")) {
                    this._templateContentEditor.setValue(this.model.getEx("Content"));
                }
            } else {
                this._templateContentEditor.setValue("");
            }

            this.updateTemplateSettingsView(this.model);

            this.$el.find("#template-name").val(this.model.getEx("Name"));
            this.$el.find("#template-type").val(this.model.getEx("Type"));

            
            this.setIsUnsaved(false);
            //this.$el.find("#template-rendered-iframe").attr("src", "renderedtemplate?" + $.param(data));
            //this.$el.find("#template-rendered-iframe").attr("src", appSession.xConnectContentStoreTemplatePrintingUrl + "?" + $.param(data));
        },
        render: function () {
            var that = this;
            this.$el.html(htmlTemplatesTemplate);

            if (window.appSession.isXConnectConfigured) {
                $("#context-menu-container-left").html(_.template(navigationTemplate)());
            }

            

            return this;
        }
    });

    var HtmlTemplatesPreviewView = Backbone.View.extend({
        template: _.template(htmlTemplatesPreviewViewTemplate),
        initialize: function (options) {
            var that = this;
            this.model = options.model;
            this._entityPreviewMode = "entitytype";
            this.languageCollection = new Backbone.Collection(window.appData.languages, { model: Backbone.Model.extend({ idAttribute: "Name" }) });
            this.onInit();
        },
        onInit: function (c) {
            var that = this;
            this.render();

            // Entity types dropdown
            if ($('#entity-types').length > 0) {  // might not exist depending on template type
                this.options.parent.entityTypeCollection.each(function (t) {
                    var optionEl = $("<option></option>")
                        .attr("value", t.getEx("Id"))
                        .text(t.getEx("DisplayName"));
                    $("#entity-types").append(optionEl);
                });
                $('#entity-types').multipleSelect({
                    selectAll: false,
                    placeholder: "No selection",
                    onClick: function () {
                        that.options.parent.setIsUnsaved(true);
                        that.updateUi.call(that);
                        that.updatePreviewBox.call(that);
                    }
                });
            }

            // Channel dropdown
            if ($('#assigned-channels').length > 0) { // might not exist depending on template type
                this.options.parent.channelCollection.each(function (t) {
                    var optionEl = $("<option></option>")
                        .attr("value", t.getEx("Id"))
                        .text(t.getEx("DisplayName"));
                    $("#assigned-channels").append(optionEl);
                });
                $('#assigned-channels').multipleSelect({
                    selectAll: false,
                    placeholder: "No selection",
                    onClick: function () {
                        that.options.parent.setIsUnsaved(true);
                    }
                });
            }

            // Roles dropdown
            if ($('#assigned-roles').length > 0) { // might not exist depending on template type
                this.options.parent.rolesCollection.each(function (t) {
                    var optionEl = $("<option></option>")
                        .attr("value", t.getEx("Id"))
                        .text(t.getEx("Name"));
                    $("#assigned-roles").append(optionEl);
                });
                $('#assigned-roles').multipleSelect({
                    selectAll: false,
                    placeholder: "No selection",
                    onClick: function () {
                        that.options.parent.setIsUnsaved(true);
                    }
                });
            }

            // Set up codemirror editor
            this._dataPreviewEditor = CodeMirror.fromTextArea(this.$el.find("#template-data-preview")[0], {
                lineNumbers: false,
                readOnly: true,
                mode: "javascript"
            });
            this._dataPreviewEditor.setSize(500, 400);

            this.updateExtras();
            this.updateUi();
            this.updatePreviewBox();
        },
        events: {
            "change #language": "updatePreviewBox",
            "change #preview-entity-type": "updatePreviewBox",
            "click input[name=entity-mode-radio]": "onChangeEntityMode",
            "input #entity-ids": "onChangeEntityIdsTextbox",
            "click #open-rendered-template": "onOpenRenderedTemplate",
            "click #download-pdf": "onDownloadPdf"
        },
        onChangeEntityMode: function () {
            this._entityPreviewMode = this.$el.find("input[name=entity-mode-radio]:checked").val();
            this.updateUi();
            this.updatePreviewBox();
        },
        onChangeEntityIdsTextbox: function () {
            var that = this;
            _.throttle(function () { that.updatePreviewBox.call(that); }, 1000, { leading: false })();
        },
        updateExtras: function () {
            var channels = this.model.getEx("Properties.Channels") ? this.model.getEx("Properties.Channels") : [];
            $('#assigned-channels').multipleSelect("setSelects", channels);
            var entityTypes = this.model.getEx("Properties.EntityTypes") ? this.model.getEx("Properties.EntityTypes") : [];
            $('#entity-types').multipleSelect("setSelects", entityTypes);
            var roles = this.model.getEx("Properties.Roles") ? this.model.getEx("Properties.Roles") : this.options.parent.rolesCollection.pluck("Id");
            $('#assigned-roles').multipleSelect("setSelects", roles);
        },
        updateUi: function () {
            var that = this;

            // entity type
            this.$el.find("#preview-entity-type").empty();
            var availablePreviewEntityTypes = $('#entity-types').multipleSelect("getSelects");
            _.each(availablePreviewEntityTypes, function (et) {
                var t = that.options.parent.entityTypeCollection.get(et);
                var optionEl = $("<option></option>")
                    .attr("value", t.getEx("Id"))
                    .text(t.getEx("DisplayName"));
                that.$el.find("#preview-entity-type").append(optionEl);
            });

            // Mode
            this.$el.find("input[name=entity-mode-radio][value=" + this._entityPreviewMode + "]").prop('checked', true);
            this.$el.find("#preview-entity-type").prop('disabled', this._entityPreviewMode === "entitytype" ? false : 'disabled');
            this.$el.find("#entity-ids").prop('disabled', this._entityPreviewMode === "entityid" ? false : 'disabled');
        },
        updatePreviewBox: function () {
            var that = this;
            if (this._entityPreviewMode === "entityid") {
                var entities = this.$el.find("#entity-ids").val().split(",");
                for (var i = 0; i < entities.length; i++) {
                    entities[i] = entities[i].trim();
                }
                this._currentPreviewEntities = entities;
                this.fetchTemplateDataPreview();
            } else if (this._entityPreviewMode === "entitytype") {
                var data = {
                    entityType: this.$el.find("#preview-entity-type").val(),
                    maxCount: 1
                }
                $.ajax({
                    type: "GET",
                    url: "/api/entity",
                    data: $.param(data, true),
                    success: function (response) {
                        var entities = [];
                        if (response.length > 0) {
                            entities = _.uniq(_.pluck(response, "Id"));
                        }
                        that._currentPreviewEntities = entities;
                        that.fetchTemplateDataPreview.call(that);
                    },
                    error: function (model, response) {
                        //inRiverUtil.OnErrors(model, response);
                        that._currentPreviewEntities = [];
                        that.fetchTemplateDataPreview.call(that);
                    },
                    dataType: "json"
                });
            }
        },
        fetchTemplateDataPreview: function () {
            var that = this;
            var data = {
                entities: this._currentPreviewEntities,
                language: this.$el.find("#language").val()
            };
            //data.entities = [11472];
            $.ajax({
                type: "GET",
                url: "/api/tools/templatedatapreview",
                data: $.param(data, true),
            }).fail(function () {
                that._dataPreviewEditor.setValue("Failed to preview this template...");
                //that.$el.find("#template-data-preview").text("Failed to preview this template...");
            }).done(function (response, data) {
                that._dataPreviewEditor.setValue(response.content);
                //that.$el.find("#template-data-preview").text(response.content);
            });
        },
        onOpenRenderedTemplate: function () {
            var data = {
                entities: this._currentPreviewEntities,
                templateId: this.model.id,
                language: this.$el.find("#language").val()
            };
            window.open("/app/templateprinting?" + $.param(data, true));
        },
        onDownloadPdf: function () {
            var that = this;

            var data = {
                entities: this._currentPreviewEntities,
                mainTemplate: this.model.id,
                language: this.$el.find("#language").val()
            }

            data.pdfFileName = this.model.get("Name");

            $.fileDownload("/api/tools/templatepdfprint/", {
                httpMethod: "GET",
                data: data,
                prepareCallback: function (url) {
                    that.$el.find("#download-pdf").prop('disabled', true);
                },
                successCallback: function (url) {
                    that.$el.find("#download-pdf").prop('disabled', false);
                },
                failCallback: function (responseHtml, url) {
                    that.$el.find("#download-pdf").prop('disabled', false);
                    alert("PDF Download failed...");
                }
            });
        },
        save: function () { // Called from parent
            var that = this;
            if ($('#entity-types').length > 0) {
                var selectedEntityTypes = $('#entity-types').multipleSelect("getSelects");
                this.model.set("Properties.EntityTypes", selectedEntityTypes, { silent: true });
            }
            if ($('#assigned-channels').length > 0) {
                var channels = _.map($('#assigned-channels').multipleSelect("getSelects"), function (x) { return parseInt(x); });
                this.model.set("Properties.Channels", channels, { silent: true });
            }
            if ($('#assigned-roles').length > 0) {
                var roles = _.map($('#assigned-roles').multipleSelect("getSelects"), function (x) { return parseInt(x); });
                if (_.isEqual(roles, this.options.parent.rolesCollection.pluck("Id"))) {
                    this.model.unset("Properties.Roles", { silent: true }); // If all roles are selected, then remove the model attribute (implies that all roles are allowed)
                } else {
                    this.model.set("Properties.Roles", roles, { silent: true });
                }
            }
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ /* data: this.model.toJSON() */ }));

            // Language
            var currentLanguage = appHelper.userLanguage;
            var languageOptionsHtml = "";
            this.languageCollection.each(function (model) {
                languageOptionsHtml += "<option value='" + model.get("Name") + "'" + (model.get("Name") === currentLanguage ? " selected" : "") + ">" + model.get("DisplayName") + "</option>";
            });
            this.$el.find("#language").html(languageOptionsHtml);

            // Extras
            var tmpEl = $("#template-type-specific-settings");
            tmpEl.empty();
            if (this.model.getEx("Type") === "EnrichPreviewTemplate" ||
                this.model.getEx("Type") === "EnrichPDFTemplate" ||
                this.model.getEx("Type") === "PlanReleasePreviewTemplate" ||
                this.model.getEx("Type") === "EditTemplate") {
                tmpEl.append("<div class='template-setting-row'><label class='left-column' for='entity-types'>Entity Types</label><select id='entity-types' style='width: 230px;'></select></div>");
            }
            if (this.model.getEx("Type") === "PlanReleasePreviewTemplate") {
                tmpEl.append("<div class='template-setting-row'><label class='left-column' for='assigned-channels'>Channels</label><select id='assigned-channels' style='width: 230px;'></select></div>");
            }
            if (this.model.getEx("Type") === "EditTemplate") {
                tmpEl.append("<div class='template-setting-row'><label class='left-column' for='assigned-roles'>Roles</label><select id='assigned-roles' style='width: 230px;'></select></div>");
            }
            return this;
        }
    });

    var HtmlTemplatesContentStoreTemplatePreviewView = Backbone.View.extend({
        template: _.template(htmlTemplatesContentStoreTemplateEditViewTemplate),
        initialize: function (options) {
            var that = this;
            this.model = options.model;

            this.xConnectUserCollection = new Backbone.Collection(null, { url: "/api/xconnect/user" });
            //this.allEntityTypesCollection = new Backbone.Collection(null, { url: "/api/entitytype", model: Backbone.Model.extend({ idAttribute: "Id" }) });
            this.xConnectContentStoreCollection = new Backbone.Collection(null, { url: "/api/xconnect/public/contentstore" });
            this.xConnectContentStoreLanguageCollection = new Backbone.Collection(null, { model: Backbone.Model.extend({ idAttribute: "languageCode" }) });
            //this.xConnectEntityPreviewCollection = new Backbone.Collection(null, { });
            this.xConnectManagementContentStoreCollection = new Backbone.Collection(null, { url: "/api/xconnect/contentstore" });


            this.listenToOnce(this.xConnectUserCollection, "reset", this.onInit);
            this.xConnectUserCollection.fetch({ reset: true });

            this.listenToOnce(this.xConnectManagementContentStoreCollection, "reset error", this.onInit);
            this.xConnectManagementContentStoreCollection.fetch({ reset: true });

            //this.listenToOnce(this.allEntityTypesCollection, "reset", this.onInit);
            //this.allEntityTypesCollection.fetch({ reset: true });


            //this.xconnectApiContentStoreCollection = new Backbone.Collection(null, { url: "/api/xconnect/public/contentstore" });
            //this.listenToOnce(this.xconnectApiContentStoreCollection, "reset", this.onInit);
            //this.xconnectApiContentStoreCollection.fetch({ reset: true });



            //this.updateEntityTypes();
        },
        onInit: function (c) {
            c.isLoaded = true;
            if (!this.xConnectManagementContentStoreCollection.isLoaded || !this.xConnectUserCollection.isLoaded) {
                return;
            }

            var that = this;
            this.render();

            // Set up codemirror editor
            this._dataPreviewEditor = CodeMirror.fromTextArea(this.$el.find("#template-data-preview")[0], {
                lineNumbers: false,
                readOnly: true,
                mode: "htmlmixed"
            });
            this._dataPreviewEditor.setSize(500, 400);

            this.uiStateModel = new Backbone.Model();
            this.listenTo(this.uiStateModel, "change", this.fetchAvailableEntitiesForPreview);
            this.listenTo(this.uiStateModel, "change", this.updateUiFromState);
            this.listenTo(this.uiStateModel, "change:entities", this.fetchTemplateDataPreview);
            this.listenTo(this.uiStateModel, "change:language", this.fetchTemplateDataPreview);

            this.listenTo(this.xConnectContentStoreCollection, "reset", function () {
                that.$el.find("#content-store").empty();
                that.xConnectContentStoreCollection.each(function (t) {
                    var optionEl = $("<option></option>")
                        .attr("value", t.get("id"))
                        .text(t.get("name"));
                    that.$el.find("#content-store").append(optionEl);
                });
                if (!this.xConnectContentStoreCollection.get(that.uiStateModel.get("contentStore"))) {
                    that.uiStateModel.set("contentStore", this.xConnectContentStoreCollection.length > 0 ? this.xConnectContentStoreCollection.models[0].get("id") : null);
                }
            });

            this.listenTo(this.xConnectContentStoreLanguageCollection, "reset", function () {
                that.$el.find("#language").empty();
                that.xConnectContentStoreLanguageCollection.each(function (t) {
                    var optionEl = $("<option></option>")
                        .attr("value", t.get("languageCode"))
                        .text(t.get("languageCode"));
                    that.$el.find("#language").append(optionEl);
                });
                if (!this.xConnectContentStoreLanguageCollection.get(that.uiStateModel.get("language"))) {
                    that.uiStateModel.set("language", this.xConnectContentStoreLanguageCollection.length > 0 ? this.xConnectContentStoreLanguageCollection.models[0].get("languageCode") : "en");
                }
            });

            this.listenTo(this.uiStateModel, "change:user", function (m) {
                that.uiStateModel.set({ "contentStore": null }, { silent: true });
                that._dataPreviewEditor.setValue("");
                this.xConnectContentStoreCollection.fetch({
                    reset: true,
                    beforeSend: function (xhr) {
                        var viewAsUser = that.xConnectUserCollection.get(m.get("user"));
                        xhr.setRequestHeader("Authorization", "Basic " + btoa("apikey:" + viewAsUser.get("apiKey")));
                    },
                });
            });

            this.listenTo(this.xConnectContentStoreCollection, "sync", function () {
                if (!this.xConnectContentStoreCollection.get(that.uiStateModel.get("contentStore"))) {
                    that.uiStateModel.set("contentStore", this.xConnectContentStoreCollection.length > 0 ? this.xConnectContentStoreCollection.models[0].get("languageCode") : null);
                }
                that.onUpdateStateFromUi();
            });

            this.listenTo(this.uiStateModel, "change:contentStore", function (m) {
                that.uiStateModel.set("entities", null, { silent: true });
                if (!m.get("contentStore")) {
                    that.xConnectContentStoreLanguageCollection.reset();
                    that.$el.find("entity-type").empty();
                } else {
                    // Languages
                    var channelId = that.xConnectContentStoreCollection.get(m.get("contentStore")).get("channelId");
                    this.xConnectContentStoreLanguageCollection.url = "/api/xconnect/public/channel/" + channelId + "/languages";
                    this.xConnectContentStoreLanguageCollection.fetch({
                        reset: true,
                        beforeSend: function (xhr) {
                            var viewAsUser = that.xConnectUserCollection.get(m.get("user"));
                            xhr.setRequestHeader("Authorization", "Basic " + btoa("apikey:" + viewAsUser.get("apiKey")));
                        },
                    });
                    // Entity types
                    that.$el.find("#entity-type").empty();
                    var entityTypes = _.pluck(that.xConnectContentStoreCollection.get(m.get("contentStore")).get("entityTypes"), "type");
                    _.each(entityTypes, function (t) {
                        var optionEl = $("<option></option>")
                            .attr("value", t)
                            .text(t);
                        that.$el.find("#entity-type").append(optionEl);
                    });
                    if (!_.contains(entityTypes, that.uiStateModel.get("entityType"))) {
                        that.uiStateModel.set("entityType", entityTypes.length > 0 ? entityTypes[0] : null);
                    }
                }
            });

            this.uiStateModel.set({
                user: this.xConnectUserCollection.models[0].get("id"),
                language: "en",
                entityMode: this.$el.find("input[name=entity-mode-radio]").first().val()
            });

            this.xConnectUserCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("id"))
                    .text(t.get("email"));
                that.$el.find("#user").append(optionEl);
            });

            // Published to content stores dropdown
            this.xConnectManagementContentStoreCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("id"))
                    .text(t.get("name"));
                $("#assigned-content-stores").append(optionEl);
            });
            $('#assigned-content-stores').multipleSelect({
                selectAll: false,
                placeholder: "No selection",
                onClick: function () {
                    that.options.parent.setIsUnsaved(true);
                }
            });
            var assignedContentStores = [];
            this.xConnectManagementContentStoreCollection.each(function (cs) {
                if (_.contains(cs.get("htmlTemplates"), that.model.id)) {
                    assignedContentStores.push(cs.get("id"));
                }
            });
            $('#assigned-content-stores').multipleSelect("setSelects", assignedContentStores);

            // Properties
            this.listenTo(this.model, "change:type", this.updateExtras);
            this.updateExtras();
        },
        events: {
            "change #user": "onUpdateStateFromUi",
            "change #entity-type": "onUpdateStateFromUi",
            "change #language": "onUpdateStateFromUi",
            "change #content-store": "onUpdateStateFromUi",
            "click input[name=entity-mode-radio]": "onUpdateStateFromUi",
            "input #entity-ids": "onChangeEntityIdsTextbox",
            "click #open-rendered-template": "onOpenRenderedTemplate",
            "click #download-pdf": "onDownloadPdf"
        },
        updateExtras: function () {
            if (this.model.get("type") === "ContentStoreTemplate") {
                var nEntitiesPerPage = 1;
                if (this.model.get("properties.entitiesPerPage")) {
                    nEntitiesPerPage = this.model.get("properties.entitiesPerPage");
                }
                $("#number-of-entities-per-page").val(nEntitiesPerPage);
                $("#number-of-entities-per-page").closest("div").show();
            } else {
                $("#number-of-entities-per-page").closest("div").hide();
            }
        },
        onChangeEntityIdsTextbox: function () {
            var that = this;
            _.throttle(function () { that.onUpdateStateFromUi.call(that); }, 1000, { leading: false })();
        },
        onUpdateStateFromUi: function () {
            var attrs = {
                user: this.$el.find("#user").val(),
                entityType: this.$el.find("#entity-type").val(),
                contentStore: this.$el.find("#content-store").val(),
                language: this.$el.find("#language").val(),
                entityMode: this.$el.find("input[name=entity-mode-radio]:checked").val(),
                entityIds: this.$el.find("#entity-ids").val(),
            }
            this.uiStateModel.set(attrs);
        },
        updateUiFromState: function () {
            this.$el.find("#user").val(this.uiStateModel.get("user"));
            this.$el.find("#entity-type").val(this.uiStateModel.get("entityType"));
            this.$el.find("#content-store").val(this.uiStateModel.get("contentStore"));
            this.$el.find("#language").val(this.uiStateModel.get("language"));

            this.$el.find("input[name=entity-mode-radio][value=" + this.uiStateModel.get("entityMode") + "]").prop('checked', true);
            this.$el.find("#entity-type").prop('disabled', this.uiStateModel.get("entityMode") === "entitytype" ? false : 'disabled');
            this.$el.find("#entity-ids").prop('disabled', this.uiStateModel.get("entityMode") === "entityid" ? false : 'disabled');
        },
        fetchAvailableEntitiesForPreview: function () {
            var that = this;

            if (this.uiStateModel.get("user") && this.uiStateModel.get("contentStore")) {
                if (this.uiStateModel.get("entityMode") === "entityid") {
                    var entities = this.uiStateModel.get("entityIds").split(",");
                    for (var i = 0; i < entities.length; i++) {
                        entities[i] = entities[i].trim();
                    }
                    that.uiStateModel.set("entities", entities);
                } else if (this.uiStateModel.get("entityType")) {

                    var channelId = that.xConnectContentStoreCollection.get(this.uiStateModel.get("contentStore")).get("channelId");

                    var pageSize = 20; // for cover page and end page
                    if (this.model.get("type") === "ContentStoreTemplate") {
                        pageSize = this.determineNumberOfEntitiesPerPage();
                    }

                    var data = {
                        entityTypeFilter: [this.uiStateModel.get("entityType")],
                        page: 0,
                        pageSize: pageSize
                    };

                    //that.$el.find("#entities").empty();
                    //that.$el.find("#entities").multipleSelect("refresh");

                    $.ajax({
                        type: "GET",
                        url: "/api/xconnect/public/channel/" + channelId + "/entity",
                        beforeSend: function (xhr) {
                            var viewAsUser = that.xConnectUserCollection.get(that.uiStateModel.get("user"));
                            xhr.setRequestHeader("Authorization", "Basic " + btoa("apikey:" + viewAsUser.get("apiKey")));
                        },
                        data: $.param(data, true),
                        success: function (response) {
                            var arr = response[1];
                            that.uiStateModel.set("entities", _.uniq(_.pluck(arr, "id")));
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                            that.uiStateModel.set("entities", null);
                        },
                        dataType: "json"
                    });

                    //this.listenToOnce(this.xConnectEntityPreviewCollection, "reset", this.updateAvailableEntities);
                    //this.xConnectEntityPreviewCollection.fetch({
                    //    data: $.param(data, true),
                    //    reset: true,
                    //    beforeSend: function (xhr) {
                    //        var viewAsUser = that.xConnectUserCollection.get(that.uiStateModel.get("user"));
                    //        xhr.setRequestHeader("Authorization", "Basic " + btoa("apikey:" + viewAsUser.get("apiKey")));
                    //    },
                    //});
                }
            }
        },
        determineNumberOfEntitiesPerPage: function () {
            var entitiesPerPage = this.model.get("properties.entitiesPerPage");
            if (inRiverUtil.isInt(entitiesPerPage)) {
                return entitiesPerPage;
            }
            return 1;
        },
        fetchTemplateDataPreview: function () {
            var that = this;
            if (!this.uiStateModel.get("entities") || !this.uiStateModel.get("contentStore") || !this.uiStateModel.get("language")) {
                this._dataPreviewEditor.setValue("");
            } else {
                var channelId = this.xConnectContentStoreCollection.get(this.uiStateModel.get("contentStore")).get("channelId");

                var data = { entities: this.uiStateModel.get("entities"), channel: channelId, templateId: this.model.id, language: this.uiStateModel.get("language") };
                //data.entities = [11472];
                $.ajax({
                    type: "GET",
                    url: "/api/xconnect/templatedatapreview",
                    data: $.param(data, true),
                }).fail(function () {
                    that._dataPreviewEditor.setValue("Failed to preview this template...");
                    //that.$el.find("#template-data-preview").text("Failed to preview this template...");
                }).done(function (response, data) {
                    that._dataPreviewEditor.setValue(response.content);
                    //that.$el.find("#template-data-preview").text(response.content);
                });
            }
        },
        onOpenRenderedTemplate: function () {
            var channelId = this.xConnectContentStoreCollection.get(this.uiStateModel.get("contentStore")).get("channelId");
            var viewAsUser = this.xConnectUserCollection.get(this.uiStateModel.get("user"));
            var data = {
                entities: this.uiStateModel.get("entities"),
                channel: channelId,
                templateId: this.model.id,
                language: this.uiStateModel.get("language"),
                apiKey: viewAsUser.get("apiKey")
            };

            window.open(appSession.xConnectContentStoreTemplatePrintingUrl + "?" + $.param(data, true));
        },
        onDownloadPdf: function () {
            var that = this;
            var channelId = this.xConnectContentStoreCollection.get(this.uiStateModel.get("contentStore")).get("channelId");
            var viewAsUser = this.xConnectUserCollection.get(this.uiStateModel.get("user"));

            var data = {
                entities: this.uiStateModel.get("entities"),
                channel: channelId,
                language: this.uiStateModel.get("language"),
                apiKey: viewAsUser.get("apiKey")
            };

            if (this.model.get("type") === "ContentStoreCoverPage") {
                data.coverPageTemplate = this.model.id;
            } else if (this.model.get("type") === "ContentStoreTemplate") {
                data.mainTemplate = this.model.id;
            } else if (this.model.get("type") === "ContentStoreEndPage") {
                data.endPageTemplate = this.model.id;
            }

            $.fileDownload(appSession.xConnectContentStoreTemplatePdfUrl, {
                httpMethod: "GET",
                data: data,
                prepareCallback: function (url) {
                    that.$el.find("#download-pdf").prop('disabled', true);
                },
                successCallback: function (url) {
                    that.$el.find("#download-pdf").prop('disabled', false);
                },
                failCallback: function (responseHtml, url) {
                    that.$el.find("#download-pdf").prop('disabled', false);
                    alert("PDF Download failed...");
                }
            });
        },
        save: function () { // Called from parent
            var that = this;
            // Save "published in content stores"
            var assignedContentStores = _.map($('#assigned-content-stores').multipleSelect("getSelects"), function (x) { return parseInt(x); });
            this.xConnectManagementContentStoreCollection.each(function (cs) {
                var isAssigned = _.contains(assignedContentStores, cs.get("id"));
                if (!isAssigned && _.contains(cs.get("htmlTemplates"), that.model.id)) {
                    // Remove this template for the content store
                    var newArr = _.without(cs.get("htmlTemplates"), that.model.id);
                    cs.save("htmlTemplates", newArr);
                } else if (isAssigned && !_.contains(cs.get("htmlTemplates"), that.model.id)) {
                    cs.get("htmlTemplates").push(that.model.id);
                    cs.save();
                }
            });

            var nEntitiesPerPage = parseInt($('#number-of-entities-per-page').val());
            if (inRiverUtil.isInt(nEntitiesPerPage)) {
                this.model.set("properties.entitiesPerPage", nEntitiesPerPage, { silent: true });
            } else {
                this.model.set("properties.entitiesPerPage", null, { silent: true });
            }

            this.fetchAvailableEntitiesForPreview();
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ /* data: this.model.toJSON() */ }));

            var tmpEl = $("#template-type-specific-settings");
            tmpEl.empty();
            tmpEl.append("<div class='template-setting-row'><label class='left-column' for='assigned-content-stores'>Published to Content Stores</label><select id='assigned-content-stores' style='width: 230px;'></select></div>");

            //if (this.model.get("type") === "ContentStoreTemplate") {
            tmpEl.append("<div class='template-setting-row'><label class='left-column' for='number-of-entities-per-page'>Number of Entities per Page</label><input type='text' id='number-of-entities-per-page'></input></div>");
            tmpEl.find("#number-of-entities-per-page").on("change keypress", function (e) {
                that.options.parent.setIsUnsaved(true);
            });
            // }

            return this;
        }
    });

    return HtmlTemplatesView;
});
