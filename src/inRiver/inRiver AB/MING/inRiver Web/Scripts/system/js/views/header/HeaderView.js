define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/header/headerTemplate.html'
], function($, _, Backbone, headerTemplate){

  var HeaderView = Backbone.View.extend({
    el: $("#top-header"),

    initialize: function() {
        ithis.render();
        /*var that = this;
      var options = { query: 'thomasdavis' };

        var onDataHandler = function(collection) {
            that.render();
        };

      this.model = new OwnerModel(options);
      this.model.fetch({ success : onDataHandler, dataType: "jsonp"});*/

    },

    render: function(){

      /*var data = {
        owner: this.model.toJSON(),
        _: _ 
      };*/

      //var compiledTemplate = _.template( headerTemplate, data );
      this.$el.html(headerTemplate);
    }

  });

  return HeaderView;
  
});
