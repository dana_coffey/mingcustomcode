define([
    'jquery',
    'underscore',
    'backbone',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'views/sidebar/SidebarView',
    'models/permission/PermissionModel',
    'text!templates/home/modalTemplate.html'
], function($, _, Backbone, alertify, inRiverUtil, SidebarView, PermissionModel, modalTemplate) {

    var AddPermissionView = Backbone.View.extend({
        initialize: function(data) {
            this.undelegateEvents();
            this.parent = data.data.parent;
            this.model = new PermissionModel();
            this.render();
        },
        events: {
            "click button#addPermission": "save",
            "click button#closeModal": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        cancel: function() {
            $("#modalWindow").hide();

        },
        onKeyUp: function(event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function(event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    this.save();
                    break;
                }
            }
        },
        submit: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        save: function() {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;

                this.model.save({
                    Name: this.form.fields.Name.editor.getValue(),
                    Description: this.form.fields.Description.editor.getValue()
                }, {
                    success: function() {
                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();

                        $("#modalWindow").find("div").html("");
                        $("#modalWindow").hide();
                        inRiverUtil.Notify("Permission has been added successfully");
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        onContentChanged: function() {
            this.save();
        },
        render: function(id) {
            var self = this;

            this.form = new Backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var Modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add Permission" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add Permission</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addPermission">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            //remove disabled when add
            this.$el.find("input[name='Name']").removeAttr("disabled");

            return this;
        }
    });

    return AddPermissionView;
});
