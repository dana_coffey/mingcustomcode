define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var UserModel = Backbone.Model.extend({
        idAttribute: 'Username',
        urlRoot: '/api/user',
        defaults: {
        },
        schema: { // Used by backbone forms extension
            AuthenticationType: { type: 'Select', editorAttrs: { disabled: true }, options: ['Forms', 'ActiveDirectory']},
            Username: { type: 'Text', validators: ['required', { type: 'regexp', message: 'No spaces or invalid characters allowed', regexp: /^[a-zA-Z0-9-_.]+$/ }], editorAttrs: { disabled: true } },
            FirstName: { type: 'Text', title: 'First Name'},
            LastName: { type: 'Text', title: 'Last Name' },
            Email: { validators: ['email'] },
            Roles: { type: 'Checkboxes', validators: ['required'], title: 'Roles', options: []/*, options: (new app.RoleList())*/ },
            Password: { type: 'Password', validators: ['required', { type: 'match', field: 'Password2', message: 'Passwords must match!' }] },
            Password2: { type: 'Password', title: 'Repeat password', validators: ['required', { type: 'match', field: 'Password', message: 'Passwords must match!' }] },
            RestAPIkey: { type: 'TextArea', editorAttrs: { disabled: true }, title: 'Rest Api-key' }
        },
        get: function (attr) {
            // Override of get() to support "synthetic" attributes (such as RolesList below)
            if (typeof this[attr] == 'function') {
                return this[attr]();
            }
            return Backbone.Model.prototype.get.call(this, attr);
        },
        RolesList: function () {
            var roles = _.pluck(this.get("Roles"), "Name").join(", ");
            return roles;
        },
        toString: function () {
            return this.get("Username");
        },
    });

    return UserModel;

});
