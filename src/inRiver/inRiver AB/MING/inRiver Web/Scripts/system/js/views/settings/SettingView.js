define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'sharedjs/models/setting/SettingModel',
  'sharedjs/collections/settings/SettingsCollection',
  'text!templates/settings/settingTemplate.html'
], function($, _, Backbone, alertify, inRiverUtil, SidebarView, SettingModel, SettingCollection, settingTemplate){

  var SettingView = Backbone.View.extend({
 
      initialize: function (data) {
          this.undelegateEvents();
          this.render();
      },
      events: {
          "click button#saveButton": "saveSetting",
          "click button#cancelButton": "cancel",
          "click": "onClick",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown",
      },
      cancel: function () {
          $("#edit-panel").slideUp(200, "swing");
          Backbone.trigger('closeEditPanel');

      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.saveSetting();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.saveSetting();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.saveSetting();
      },
      saveSetting: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;

              
              this.model.save({
                  Setting: this.form.fields.Setting.editor.getValue(),
                  Value: this.form.fields.Value.editor.getValue()
              }, {
                  success: function () {
                      inRiverUtil.Notify("Setting has been updated successfully");

                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                      $("#edit-panel").slideUp(200, "swing");
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  },
                  wait: true
              });
          }
      },
      deleteSetting: function() {

          //Delete model
          this.model.collection.remove(this.model);
          this.model.destroy();
          $("#edit-panel").slideUp(200, "swing");

          //Delete view
          this.remove();
      },
      onContentChanged: function () {
          this.saveSetting();
      },
      onClick: function () {
          event.stopPropagation();

      },
      render: function () {

          /*$('.menu li').removeClass('active');
          $('.menu li a[href="' + window.location.hash + '"]').parent().addClass('active');*/
          //this.$el.html(usersTemplate);
          this.$el.html(settingTemplate);

          this.form = new Backbone.Form({
              model: this.model
          });

          this.$el.find("#setting-content").append(this.form.render().el);

          //var $buttonSave = $('<button class="btn" type="button" id="deleteButton">Delete</button>');
          //$("#setting-content").append($buttonSave);



//          return this;
      }
  });

  return SettingView;
});
