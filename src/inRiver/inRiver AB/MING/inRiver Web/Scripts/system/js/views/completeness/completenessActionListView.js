﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'modalPopup',
    'models/completeness/completenessActionModel',
    'collections/completeness/completenessActionCollection',
    'views/completeness/completenessActionView',
    'text!templates/completeness/completenessActionListTemplate.html'
], function ($, _, backbone, inRiverUtil, modalPopup, completenessActionModel, completenessActionCollection, completenessActionView, completenessActionListTemplate) {

    var completenessActionListView = backbone.View.extend({
        initialize: function(options) {
            this.actionCollection = options.actionCollection;
            this.definitionId = options.definitionId;
            this.groupId = options.groupId;
            this.ruleId = options.ruleId;
            this.nodeType = options.type;
            this.action = options.action;
        },
        events: function (){
            var theEvents = {};
            _.each(this.actionCollection.models, function (action) {
                theEvents["click #viewedit-" + action.get("Id")] = "onViewAction";
                theEvents["click #delete-" + action.get("Id")] = "onDeleteAction";
            });

            theEvents["click #addNewAction"] = "onAddNewAction";

            return theEvents;
        },
        onDeleteAction: function(e) {
            e.stopPropagation();

            var self = this;
            inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want to delete the selected action?", function() {
                var targetIdValue = e.currentTarget.id;
                targetIdValue = targetIdValue.substring(targetIdValue.lastIndexOf("-") + 1);
                var model = new completenessActionModel({ Id: targetIdValue });
                model.destroy({
                    success: function () {
                        inRiverUtil.Notify("Action deleted");
                        self.actionCollection.fetch({
                            success: function() {
                                self.render();
                            },
                            error: function(error, response) {
                                inRiverUtil.OnErrors(error, response);
                            }
                        });
                    },
                    error: function (error, response) {
                        inRiverUtil.OnErrors(error, response);
                    }
                });
            });
        },
        onAddNewAction: function(e) {
            e.stopPropagation();

            var self = this;

            this.modal = new modalPopup({ callbackContext: this });
            var actionCollection = new completenessActionCollection();
            switch (this.nodeType) {
                case "Definition":
                    actionCollection.url = "/api/completeness/action/definition/-1";
                    break;
                case "Group":
                    actionCollection.url = "/api/completeness/action/definition/" + this.definitionId + "/group/-1";
                    break;
                case "Rule":
                    actionCollection.url = "/api/completeness/action/definition/" + this.definitionId + "/rule/-1";
                    break;
            }

            actionCollection.fetch({
                success: function (result) {
                    var model = result.models[0];
                    self.modal.popOut(new completenessActionView({
                        model: model,
                        definitionId: self.definitionId,
                        groupId: self.groupId,
                        ruleId: self.ruleId,
                        type: self.nodeType,
                        action: "add"
                    }), {
                        header: "Create Action",
                        size: "small",
                        usesavebutton: true,
                        onSave: self.onActionPopupSaveNew,
                        onCancel: function (popup) {
                            popup.close();
                        }
                    });
                    $("#save-form-button").removeAttr("disabled");
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onViewAction: function(e) {
            e.stopPropagation();

            var self = this;

            this.modal = new modalPopup({ callbackContext: this });

            var targetIdValue = e.currentTarget.id;
            targetIdValue = targetIdValue.substring(targetIdValue.lastIndexOf("-") + 1);
            var actionModel = new completenessActionModel({ Id: targetIdValue });
            actionModel.fetch({
                success: function(model) {
                    self.modal.popOut(new completenessActionView({
                        model: model,
                        definitionId: self.definitionId,
                        groupId: self.groupId,
                        ruleId: self.ruleId,
                        type: self.nodeType,
                        action: "edit"
                    }), {
                        header: "Action",
                        size: "small",
                        usesavebutton: true,
                        onSave: self.onActionPopupSave,
                        onCancel: function (popup) {
                            popup.close();
                        }
                    });
                    $("#save-form-button").removeAttr("disabled");
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onActionPopupSave: function (popup, childView) {
            if (childView.model.get("TaskName") == "" || childView.model.get("TaskDescription") == "") {
                inRiverUtil.NotifyError("Missing input", "The task name and task description need value to proceed with the save.");
                return;
            }

            var self = this;

            var model = new completenessActionModel({ Id: childView.model.get("Id") });
            model.urlRoot = "/api/completeness/action";
            model.fetch({
                success: function (result) {
                    result.set("ActionTrigger", childView.model.get("ActionTrigger"));
                    result.set("TaskName", childView.model.get("TaskName"));
                    result.set("TaskDescription", childView.model.get("TaskDescription"));
                    result.set("TaskAssignedTo", childView.model.get("TaskAssignedTo"));
                    result.set("TaskCreatedBy", childView.model.get("TaskCreatedBy"));
                    result.set("ActionType", childView.model.get("ActionType"));

                    result.save(null, {
                        success: function (newModel) {
                            self.addRowToTable(newModel);
                            inRiverUtil.Notify("Action saved");
                            popup.close();
                        },
                        error: function (error, response) {
                            inRiverUtil.OnErrors(error, response);
                        }
                    });

                },
                error: function (error, response) {
                    inRiverUtil.OnErrors(error, response);
                }
            });
        },
        onActionPopupSaveNew: function(popup, childView) {
            if (childView.model.get("TaskName") == "" || childView.model.get("TaskDescription") == "") {
                inRiverUtil.NotifyError("Missing input", "The task name and task description need value to proceed with the save.");
                return;
            }

            var self = this;

            var model = new completenessActionModel({ Id: -1 });
            model.urlRoot = "/api/completeness/action";
            model.fetch({
                success: function(result) {
                    result.set("CompletenessDefinitionId", childView.model.get("CompletenessDefinitionId"));
                    result.set("CompletenessGroupId", childView.model.get("CompletenessGroupId"));
                    result.set("CompletenessRuleId", childView.model.get("CompletenessRuleId"));

                    result.set("ActionTrigger", childView.model.get("ActionTrigger"));
                    result.set("TaskName", childView.model.get("TaskName"));
                    result.set("TaskDescription", childView.model.get("TaskDescription"));
                    result.set("TaskAssignedTo", childView.model.get("TaskAssignedTo"));
                    result.set("TaskCreatedBy", childView.model.get("TaskCreatedBy"));
                    result.set("ActionType", childView.model.get("ActionType"));


                    result.save(null, {
                        success: function(newModel) {
                            self.addRowToTable(newModel);
                            inRiverUtil.Notify("New action saved");
                            popup.close();

                            self.actionCollection.push(newModel);
                            self.delegateEvents();

                        },
                        error: function(error, response) {
                            inRiverUtil.OnErrors(error, response);
                        }
                    });

                },
                error: function(error, response) {
                    inRiverUtil.OnErrors(error, response);
                }
            });
        },
        addRowToTable: function (actionModel) {
            var table = this.$el.find("#table-body-content");
            if (table == null || actionModel == null) {
                return;
            }

            table.append("<tr id='actionid-" + actionModel.get("Id") + "'><td>" + actionModel.get("ActionTrigger") + "</td>" +
                "<td>" + actionModel.get("TaskName") + "</td>" +
                "<td>" + actionModel.get("TaskAssignedTo") + "</td>" +
                "<td><i id='viewedit-" + actionModel.get("Id") + "' class='icon-entypo-fix-pencil completeness-content-row-icon-button' title='View/edit action'></i>" +
                "<i id='delete-" + actionModel.get("Id") + "' class='fa fa-trash-o completeness-content-row-icon-button' title='Delete action'></i></td></tr>");
        },
        render: function () {
            var self = this;

            this.$el.html(_.template(completenessActionListTemplate, {} ));

            _.each(this.actionCollection.models, function (model) {
                self.addRowToTable(model);
            });

            return this;
        }
    });

    return completenessActionListView;
});
