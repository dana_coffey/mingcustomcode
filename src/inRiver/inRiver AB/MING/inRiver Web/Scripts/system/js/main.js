// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
    packages: [{
        name: "codemirror",
        location: "../../libs/codemirror",
        main: "lib/codemirror"
    }],
  paths: {
      jquery: '../../libs/jquery/jquery-2.0.3.min',
      underscore: '../../libs/underscore/underscore',
      backbone: '../../libs/backbone/backbone-min',
      templates: '../templates',
      sharedtemplates: '../../shared/templates',
      sharedjs: '../../shared/js',
      'backbone-forms': '../../libs/backbone-forms/backbone-forms.amd',
      'backbone-modal': '../../libs/backbone-modal/backbone.modal',
      'bootstrap3': '../../libs/backbone-forms/bootstrap3',
      'bootstrap': '../../libs/bootstrap/bootstrap.min',
      'jquery-ui': '../../libs/jquery-ui/jquery-ui.min',
      'jquery-ui-touch-punch': '../../libs/jquery.ui.touch-punch/jquery.ui.touch-punch',
      'alertify': '../../libs/alertify/alertify',
      'simpleAjaxUploader': '../../libs/simpleajaxuploader/SimpleAjaxUploader',
      'backgrid': '../../libs/backgrid/backgrid-mod',
      'text': '../../libs/text/text',
      'deep-model': '../../libs/deep-model/deep-model',
      'moment': '../../libs/moment/moment.min',
      'datetimepicker': '../../libs/datetimepicker/jquery.datetimepicker',
      'dropzone': '../../libs/dropzone/dropzone-amd-module',
      'localeStringEditor': '../../shared/js/extensions/localeStringEditor',
      'dateTimeEditor': '../../shared/js/extensions/dateTimeEditor',
      'jstree': '../../libs/jstree/jstree.min',
      'modalPopup': '../../shared/js/views/modalpopup/modalPopup',
      'jquery-filedownload': '../../libs/filedownload/jquery.fileDownload',
      'jquery-steps': '../../libs/jquery.steps/jquery.steps.min',
      'multiple-select': '../../libs/multiple-select/multiple-select',
      'jquery-mousewheel': '../../libs/jquery.mousewheel/jquery.mousewheel',
  },
  shim: {
      underscore: { exports: '_' },
      backbone: { deps: ['underscore', 'jquery'], exports: 'Backbone' },
      'backbone-modal': { deps: ['jquery', 'backbone'], exports: 'Modal' },
      'backbone-forms': { deps: ['jquery', 'backbone'], exports: 'backbone-forms' },
      'backgrid': { deps: ['jquery', 'backbone'], exports: 'Backgrid' },
      'deep-model': { deps: ['backbone'], exports: 'deep-model' },
      'datetimepicker': { deps: ['underscore', 'jquery'], exports: 'datetimepicker' },
      'jquery-ui-touch-punch': { deps: ['jquery-ui'], exports: 'jquery-ui-touch-punch' },
      'jquery-filedownload': { deps: ['jquery'] },
      'jstree': { deps: ['jquery'], exports: 'jstree' },
  }
});

require([
  // Load our app module and pass it to our definition function
  'app', 'backbone-forms'

], function(App){
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    App.initialize();
});
