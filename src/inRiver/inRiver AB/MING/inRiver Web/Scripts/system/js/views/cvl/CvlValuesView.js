define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'bootstrap3',
  'sharedjs/misc/inRiverUtil',
  'views/cvl/AddCvlValueView',
  'models/cvl/CvlModel',
  'models/cvl/CvlValueModel',
  'collections/cvl/CvlCollection',
  'collections/cvl/CvlValueCollection',
  'text!templates/home/editPanelTemplate.html'
], function ($, _, backbone, alertify, bootstrap3, inRiverUtil, addCvlValueView, cvlModel, cvlValueModel, cvlCollection, cvlValueCollection, editPanelTemplate) {

    var cvlValuesView = backbone.View.extend({
          initialize: function (options) {

            this.model = options.model;
            this.collection = options.collection;

            this.hasParent = options.hasParent;
            // this.undelegateEvents();
            //_.bindAll(this, 'onContentChanged');
            //this.model.on('change', this.render, this);
        },
        events: {
            "click button#deleteCvlValue": "deleteCvlValue",
            "click button#saveCvlValue": "saveCvlValue",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        onClose: function () {
            this.save();
        },
        deleteCvlValue: function () {
            event.stopPropagation();
            var self = this;

            //Delete model
            this.model.url = '/api/cvlvalue/' + this.model.get("CVLId") + "/" + this.model.get("Id");
            this.model.destroy({
                success: function() {
                    self.refreshCvlList();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        refreshCvlList: function () {
            var self = this;

            this.collection.fetch({
                success: function () {
                    //$("#cvl-content").html("");

                    if (self.collection.length > 0) {
                        self.model = self.collection.models[0];
                    } else {
                        self.model = null;
                    }
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        stopListenOnBus: function() {
            this.stopListening(window.appHelper.event_bus);
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.saveCvlValue();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.saveCvlValue();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.saveCvlValue();
        },
        saveCvlValue: function () {
            event.stopPropagation();

            var modelToEdit = this.form.fields.Keys.editor.getValue();
            this.model = this.collection.get(modelToEdit);

            var self = this;

            // Save currently edited string value
            var lang = this.form.fields.Languages.editor.getValue();
            var stringValue = this.form.fields.Value.editor.getValue();
            if (lang != null) {
                this.model.get("Value").stringMap[lang] = stringValue;
            } else {
                this.model.Value = stringValue;
            }

            this.model.save({
                Index: this.form.fields.Index.editor.getValue(),
                LSValue: this.form.fields.LSValue.editor.getValue(),
                Value: this.form.fields.Value.editor.getValue(),
                ParentKey: this.form.fields.ParentKey.editor.getValue(),
            }, {
                success: function () {
                    inRiverUtil.Notify("CVL value has been updated successfully");

                    self.model.fetch({
                        success: function() {
                            self.form.fields.Index.editor.setValue(self.model.get("Index"));
                            self.form.fields.Key.editor.setValue(self.model.get("Key"));
                            var value = self.model.get("Value");
                            var localeStringValue = self.model.get("LSValue");
                            if (self.isLocaleString) {
                                self.form.fields.LSValue.editor.setValue(localeStringValue);
                            } else {
                                self.form.fields.Value.editor.setValue(value);
                            }

                            self.collection.sort();
                            self.form.fields.Keys.editor.setOptions(self.collection);
                            self.form.fields.Keys.editor.setValue(self.model.get("Key"));
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    }); // to make sure we get a complete description of the model from server
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                wait: true
            });
        },
        updateParents: function (m) {
            if (!m.get("ParentId")) // If no parents, skip update.
                return;

            var self = this;
            this.cvlValueList = new cvlValueCollection([], { cvlId: m.get("ParentId") });
            this.cvlValueList.fetch({
                success: function (models) {
                    self.form.fields.ParentKey.editor.setOptions(models);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var self = this;

            //   $("#cvl-content").undelegateEvents();


            this.$el.html(_.template(editPanelTemplate));

            //$("#cvl-content").append(editPanelTemplate);

            if (this.model != null) {
                // check if model exists

                self.form = new backbone.Form({
                    model: this.model
                }).render();
           
                self.form.fields.Keys.editor.setOptions(this.collection);

                if (!this.hasParent) {

                    self.form.fields.ParentKey.remove();
                } else {
                    var onDataHandler = function (collection) {
                        var newCvModel = collection.get(self.model.get("CVLId"));

                        this.cvlValueList = new cvlValueCollection([], { cvlId: newCvModel.get("ParentId") });
                        this.cvlValueList.fetch({
                            success: function (models) {
                                self.form.fields.ParentKey.editor.setOptions(models);
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    }
                    this.cvlcollection = new cvlCollection();
                    this.cvlcollection.fetch({
                        success: onDataHandler,
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }

                self.form.on('Keys:change', function (form, editor) {

                    event.stopPropagation();

                    var modelToEdit = editor.getValue();
                    this.model = self.collection.get(modelToEdit);
                    self.form.fields.Index.editor.setValue(this.model.get("Index"));
                    self.form.fields.Key.editor.setValue(this.model.get("Key"));
                    var value = this.model.get("Value");
                    var localeStringValue = this.model.get("LSValue");
                    if (self.isLocaleString) {
                        self.form.fields.LSValue.editor.setValue(localeStringValue);
                    } else {
                        self.form.fields.Value.editor.setValue(value);
                    }

                    if (self.hasParent) {
                        self.form.fields.ParentKey.editor.setValue(this.model.get("ParentKey"));
                    }

                    self.model = this.model;
                });


                if (this.model.get("Id")) {
                    this.$el.find("#cvl-panel-properties").append(this.form.$el);

                    // only do this for proper models
                    var modelValue = this.model.get("Value");
                    this.isLocaleString = false;
                    if (_.isString(modelValue)) {
                        this.$el.find(".field-LSValue").remove();

                    } else {
                        //LocaleString

                        this.$el.find(".field-Value").remove();

                        this.isLocaleString = true;
                    }

                    this.$el.find(".field-Languages").remove();

                    if (!this.hasParent) {
                        $(".field-ParentKey").remove();
                    }

                    this.$el.find(".field-Key").hide();
                }
            }

            var $buttonNew = $('<button class="btn" type="button" id="saveCvlValue">Save</button>');
            var $buttonDelete = $('<button class="btn" type="button" id="deleteCvlValue">Delete CVL Value</button>');

            if (this.model != null) {
                // check if model exists
                this.$el.find("#edit-panel-wrap").append($buttonNew);
                this.$el.find("#edit-panel-wrap").append($buttonDelete);

                this.$el.find("#deleteCvlValue").on('click', function () {
                    self.deleteCvlValue();
                });

                this.$el.find("#saveCvlValue").on('click', function () {
                    self.saveCvlValue();
                });
            }

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'cvlvaluecreated', this.refreshCvlList);

            return this; // enable chained calls
        },
    });

    return cvlValuesView;
});
