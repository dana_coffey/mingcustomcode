define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'views/sidebar/SidebarView',
  'views/languages/AddLanguageView',
  'models/language/LanguageModel',
  'collections/languages/LanguagesCollection',
  'text!templates/languages/languagesTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, SidebarView, AddLanguageView, LanguageModel, LanguagesCollection, languagesTemplate) {

    var LanguageListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            //Get Languages
            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new LanguagesCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //console.log("LanguageListView init");

            var columns = [
                { name: "DisplayName", label: "Language", cell: "string" },
                { name: "Name", label: "Code", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });
        },

        events: {
            "click #newLanguage": "newLanguage"
        },
        newLanguage: function (model) {

            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddLanguageView({ data: { header: "Add Language", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        render: function () {
            this.$el.html(languagesTemplate);
            //console.log("LanguageListView Render");
            this.$el.find("#languages-list").append(this.grid.render().$el);
            return this;
        }
    });

    return LanguageListView;
});
