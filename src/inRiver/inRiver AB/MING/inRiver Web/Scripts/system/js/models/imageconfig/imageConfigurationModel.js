﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var imageConfigurationModel = backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/imageconfiguration',
        defaults: {
        },
        toString: function () {
            return this.get("Name");
        },
        schema: { 
            Name: { type: 'Text', validators: ['required', { type: 'regexp', message: 'No spaces or invalid characters allowed', regexp: /^[a-zA-Z0-9-_.]+$/ }], editorAttrs: { disabled: true } },
            Extension: { type: 'Text', validators: ['required'] },
            OutputExtension: { type: 'Text', validators: ['required'] },
            Arguments: { type: 'Text' }
        }
    });

    return imageConfigurationModel;
});