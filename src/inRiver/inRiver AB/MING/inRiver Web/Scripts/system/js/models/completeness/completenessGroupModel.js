define([
  'underscore',
  'backbone',
], function (_, backbone) {
    
    var completenessDefinitionModel = backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/completeness/group',
    });

    return completenessDefinitionModel;

});
