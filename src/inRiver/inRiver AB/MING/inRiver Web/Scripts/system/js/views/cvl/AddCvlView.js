define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-modal',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'models/cvl/CvlModel',
    'collections/cvl/CvlCollection',
    'text!templates/home/modalTemplate.html'
], function ($, _, Backbone, Modal, alertify, inRiverUtil, CvlModel, CvlCollection, modalTemplate) {

    var AddCvlView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.parent = data.data.parent;

            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new CvlCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            console.log("AddCvlView init");

            this.model = new CvlModel();
            this.render();
        },
        events: {
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
            "click button#addCvl": "save",
            "click button#closeModal": "cancel",
            'click input[name="CustomValueList"]': "onClickCustomCVL",
            "submit" :"submit"
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.save(); 
        },
        onClickCustomCVL: function (e) {
            var r = confirm("A custom CVL must be bound to an extension dll. Are you sure you want to do this?");
            if (r == false) {
                $(e.currentTarget).removeAttr('checked');
            }
        },
        cancel: function () {
            $("#modalWindow").hide();

        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;

                this.model.save({
                    Id: this.form.fields.Id.editor.getValue(),
                    DataType: this.form.fields.DataType.editor.getValue(),
                    ParentId: this.form.fields.ParentId.editor.getValue(),
                    CustomValueList: this.form.fields.CustomValueList.editor.getValue()
                }, {
                    success: function () {
                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();

                        $("#modalWindow").find("div").html("");
                        $("#modalWindow").hide();
                        inRiverUtil.Notify("CVL has been added successfully");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        render: function (id) {
            var self = this;

            this.form = new Backbone.Form({
                model: this.model
            });

            // Create a modal view class
            var Modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add CVL" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add CVL</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addCvl">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);
            this.$el.find(".modal_section").append(this.form.render().el);

            // Remove disabled when add
            this.$el.find("input[name='Id']").removeAttr("disabled");
            this.$el.find("select[name='DataType']").removeAttr("disabled");
            this.$el.find("input[name='CustomValueList']").removeAttr("disabled");
            this.$el.find(".form-group field-Keys").hide();

            // Populate Parent Id
            var options = [""].concat(_.pluck(self.collection.models, "id"));
            self.form.fields.ParentId.editor.setOptions(options);

            return this;
        }
    });

    return AddCvlView;
});
