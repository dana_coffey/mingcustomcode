define([
  'jquery',
  'underscore',
  'backbone',
  'models/entitytype/EntityTypeModel'
], function ($, _, Backbone, EntityTypeModel) {
  var EntityTypeCollection = Backbone.Collection.extend({
      model: EntityTypeModel,
      url: '/api/entitytype'
  });
 
  return EntityTypeCollection;
});
