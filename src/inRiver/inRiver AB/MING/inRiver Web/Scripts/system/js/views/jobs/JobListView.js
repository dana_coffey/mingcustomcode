define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'sharedjs/models/job/JobModel',
  'sharedjs/collections/jobs/JobsCollection',
  'views/jobs/JobView',
  'text!templates/jobs/jobsTemplate.html',
  'views/jobs/GridRunJob'
], function ($, _, Backbone, Backgrid, inRiverUtil, SidebarView, JobModel, JobsCollection, JobView, jobsTemplate, GridRunJob) {

    var JobListView = Backbone.View.extend({
        initialize: function () {
            //Get Jobs
            var that = this;
            this.undelegateEvents();
            var onDataHandler = function () {
                that.render();
            };

            that.collection = new JobsCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            that.collection.on('rowClicked', function (model, selected) {
                event.stopPropagation();

                var modalWindowOpen = $('#modalWindow').is(':visible');
                if (modalWindowOpen) {
                    return false;
                }
                
                var oldView = this._editCvlView;
                if (oldView != null) {
                    oldView.remove();
                    oldView.undelegateEvents();
                }

                $("#edit-panel").slideDown(200, "swing", function () { });
                //console.log("onLoadEditCvlView");

                that._editCvlView = new JobView({ model: model });
                $("#edit-panel").html(that._editCvlView.el);
            });

            console.log("JobListView init");
            var columns = [
               // { name: "Job", label: "Id", cell: "string" },
                { name: "Name", label: "Name", cell: "string" },
                { name: "Interval", label: "Interval (min)", cell: "string" },
                { name: "Enabled", label: "Enabled", cell: "string" },
                { name: "Run", cell: GridRunJob}
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });
            
            this.listenTo(Backbone, 'clickedAnywhere', function () { this.onUnloadJobView(); });
         
        },

        events: {
            "click #edit-panel" : "onClick"
        },
        onUnloadJobView: function (parameters) {
            //console.log("onUnloadJobView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                Backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;
        },
        onClick: function () {
            event.stopPropagation();

        },
        render: function () {
            this.$el.html(jobsTemplate);
            //console.log("JobListView Render");
            this.$el.find("#jobs-list").append(this.grid.render().$el);
        //    return this;
        }
    });

    return JobListView;
});
