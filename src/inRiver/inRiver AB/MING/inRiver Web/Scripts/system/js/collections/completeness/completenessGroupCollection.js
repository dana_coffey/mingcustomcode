define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, backbone) {
    var completenessGroupCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.definitionId = options.definitionId;
        },
        url: function () {
            return '/api/completeness/definition/' + this.definitionId + "/group";
        }
    });

    return completenessGroupCollection;
});
