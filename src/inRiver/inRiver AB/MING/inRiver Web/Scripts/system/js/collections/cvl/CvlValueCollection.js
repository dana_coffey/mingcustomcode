define([
  'jquery',
  'underscore',
  'backbone',
  'models/cvl/CvlValueModel'
], function ($, _, backbone, cvlValueModel) {
    var cvlValueCollection = backbone.Collection.extend({
        model: cvlValueModel,
        Id: "Key", 
        initialize: function (models, options) {
            this.cvlId = options.cvlId;
        },
        url: function () {
            return '/api/cvlvalue/' + this.cvlId;
        },
        comparator: function(itemA, itemB) {
            var result = 0;
            if (itemA.get("Index") > itemB.get("Index")) {
                result = 1;
            }
            if (itemA.get("Index") < itemB.get("Index")) {
                result = -1;
            }

            return result;
        }
    });

    return cvlValueCollection;
});