define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, backbone) {
    var completenessRuleCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.groupId = options.groupId;
        },
        url: function() {
            return '/api/completeness/group/' + this.groupId + "rule";
        }
});
 
  return completenessRuleCollection;
});
