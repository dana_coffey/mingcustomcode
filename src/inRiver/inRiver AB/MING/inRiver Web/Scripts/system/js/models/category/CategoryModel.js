define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var CategoryModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        schema: { // Used by backbone forms extension
            Category: { type: 'Select', title: "Category", options: [] }
        },
        toString: function () {
            return this.get("Name").stringMap[appHelper.userLanguage];
        },
    });

    return CategoryModel;

});
