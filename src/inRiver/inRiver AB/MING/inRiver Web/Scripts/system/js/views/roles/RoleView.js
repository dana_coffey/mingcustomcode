define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'models/role/RoleModel',
  'collections/roles/RolesCollection',
  'collections/permissions/PermissionsCollection',
  'text!templates/roles/roleTemplate.html'
], function ($, _, Backbone, alertify, inRiverUtil, SidebarView, RoleModel, RolesCollection, PermissionsCollection, roleTemplate) {

  var RoleView = Backbone.View.extend({
   //   el: $("#main-container"),
      initialize: function (data) {
           this.render();
           this.undelegateEvents();

          // _.bindAll(this, "onContentChanged");

      },
      events: {
          "click button#saveButton": "saveRole",
          "click button#cancelButton": "cancel",
          //"change input": "onContentChanged",
          "click": "onClick",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown",

      },
      onClick: function () {
          event.stopPropagation();
      },
      cancel:function() {
              $("#edit-panel").slideUp(200, "swing");
              Backbone.trigger('closeEditPanel');
      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.saveRole();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.saveRole();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.saveRole();
      },
      saveRole: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;
              
              var permission = _.map(this.form.fields.Permissions.editor.getValue(), function (item) {
                  // this is a bit ugly but enough for the logics to work
                  // best option would be to assign en entire Role model here instead of just the id
                  // this is because of the odd checkboxes support in Backbone Forms
                  return { Id: item };
              });

             
              this.model.save({
                  Name: this.form.fields.Name.editor.getValue(),
                  Description: this.form.fields.Description.editor.getValue(),
                  Permissions: _.map(this.form.fields.Permissions.editor.getValue(), function (item) {
                      // this is a bit ugly but enough for the logics to work
                      // best option would be to assign en entire Role model here instead of just the id
                      // this is because of the odd checkboxes support in Backbone Forms
                      return { Id: item };
                  })
              }, {
                  success: function () {

                      var onDataHandler = function (m, r) {

                          inRiverUtil.Notify("Role has been updated successfully");

                          var test = m.get("PermissionList");
                          var i = self.model.get("PermissionList");

                          var roleslist = _.pluck(permission, "Id").join(", ");
                          self.model.set("PermissionList", roleslist);
                          $("#edit-panel").slideUp(200, "swing");
                        };

                      self.model.fetch({
                          success: onDataHandler,
                          error: function (model, response) {
                              inRiverUtil.OnErrors(model, response);
                          }
                      }); // to make sure we get a complete description of the model from server (with role descriptions etc)
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  }
              });
          }
      },
      onContentChanged: function () {
          this.saveRole();
      },
      render: function (id) {
          var self = this;

          this.$el.html(roleTemplate);
          this.form = new Backbone.Form({
              model: this.model
          });

          this.$el.find("#role-content").append(this.form.render().el);
          this.$el.find("ul[id*=Permission]").removeClass("form-control");


          // populate the roles checkbox list... this is a bit of hack because Backbone.Forms didnt seem to support dynamic checkbox lists so well...
          var onDataHandler = function (m, r) {
              //var names = self.collection.pluck('Name');
              self.form.fields.Permissions.editor.setOptions(m);
              self.form.fields.Permissions.editor.setValue(_.pluck(self.model.get("Permissions"), "Id")); // pluck will return an array of values that we set as selected
          };

          self.collection = new PermissionsCollection();
          self.collection.fetch({
              success: onDataHandler,
              error: function (model, response) {
                  inRiverUtil.OnErrors(model, response);
              }
          });
          
         // return this;
      }
  });

  return RoleView;
});
