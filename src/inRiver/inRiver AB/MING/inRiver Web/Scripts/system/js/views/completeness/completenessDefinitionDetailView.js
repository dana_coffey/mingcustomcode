﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'models/completeness/completenessGroupModel',
    'sharedjs/collections/languages/LanguagesCollection',
    'sharedjs/collections/entityTypes/entityTypeCollection',
    'text!templates/completeness/completenessDefinitionDetailTemplate.html'
], function ($, _, backbone, inRiverUtil, completenessGroupModel, languagesCollection, entityTypeCollection, completenessDefinitionDetailTemplate) {

    var completenessDefinitionDetailView = backbone.View.extend({
        initialize: function (options) {
            this.model = options.model;
            this.action = options.action;

            this.badWeight = false;

            this.selectedLanguage = window.appHelper["userLanguage"];
            this.nameFieldValue = this.model.get("Name");
            this.defaultNameFieldValue = jQuery.extend(true, {}, this.nameFieldValue);

            this.entityTypeFieldValue = this.model.get("EntityTypeId");
            this.defaultEntityTypeFieldValue = jQuery.extend(true, {}, this.entityTypeFieldValue);

            this.nameLanguageField = "#defintion-name-language-value";
            this.nameInputField = "#defintion-name-value";
            this.entityTypeField = "#entity-type-id-value";

            this.render();
        },
        events: function() {
            var theEvents = {};
            _.each(this.model.get("Groups"), function(group) {
                theEvents["click #groupid-" + group.Id] = "onOpenGroup";
                theEvents["click #delete-" + group.Id] = "onDeleteGroup";
                theEvents["change #weight-" + group.Id] = "onWeightChanged";
            });

            theEvents["change " + this.nameLanguageField] = "onNameLanguageSelectedChange";
            theEvents["keyup " + this.nameInputField] = "onKeyUpNameText";

            theEvents["change " + this.entityTypeField] = "onEntityTypeSelectedChange";

            return theEvents;
        },
        onWeightChanged: function (e) {
            e.stopPropagation();
            var self = this;

            var weights = {};
            var total = 0;
            _.each(this.model.get("Groups"), function (group) {
                var weight = parseInt(self.$el.find("#weight-" + group.Id).val());
                if (weight != 'NaN') {
                    weights[group.Id] = weight;
                    total += weight << 0;
                }
            });

            if (total > 100 || total < 100) {
                this.badWeight = true;
                this.$el.find("#exclamation-icon").show();
            } else {
                this.badWeight = false;
                this.$el.find("#exclamation-icon").hide();
            }

            this.$el.find("#total-weight-value").text(total);
            window.appHelper.event_bus.trigger('completeness_definition_weight_updated', weights);
        },
        onOpenGroup: function(e) {
            e.stopPropagation();
            var targetId = e.currentTarget.id.substring(e.currentTarget.id.lastIndexOf("-") + 1);
            this.goTo("/completeness/def/" + this.model.get("Id") + "/grp/" + targetId);
        },
        onDeleteGroup: function(e) {
            e.stopPropagation();

            var self = this;
            var targetId = e.currentTarget.id.split('-');
            inRiverUtil.NotifyConfirm("Confirmation", "Are you sure you want delete the group?", function () {
                var group = new completenessGroupModel({ Id: targetId[1] });
                group.destroy({
                    success: function () {
                        inRiverUtil.Notify("Completeness group has been removed");
                        self.goTo("/completeness/def/" + self.model.get("Id"));
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            });
        },
        addRowToTable: function(group) {
            var table = this.$el.find("#table-body-content");
            if (table == null || group == null) {
                return;
            }

            var weightTitle = "";
            if (group.Weight == 0) {
                weightTitle = "The weight should not be 0, remove the group instead if it is not to be included in any calculations.";
            }

            table.append("<tr><td>" + group.DisplayName + "</td>" +
                "<td><input type='text' title='" + weightTitle + "' value='" + group.Weight + "' maxlength=''3 size='3' id='weight-" + group.Id + "'></td>" +
                "<td>" + group.Rules.length + "</td>" +
                "<td>" + group.SortOrder + "</td>" +
                "<td><i id='groupid-" + group.Id + "' class='icon-entypo-fix-pencil completeness-content-row-icon-button' title='Edit group'></i>" +
                "<i id='delete-" + group.Id + "' class='fa fa-trash-o completeness-content-row-icon-button' title='Delete group'></i></td></tr>");
        },
        onEntityTypeSelectedChange: function(e) {
            if (e != null) {
                e.stopPropagation();
            }

            this.entityTypeFieldValue = this.$el.find(this.entityTypeField).val();
            if (this.entityTypeFieldValue != this.defaultEntityTypeFieldValue) {
                this.model.set("EntityTypeId", this.entityTypeFieldValue);
                window.appHelper.event_bus.trigger('completeness_content_updated', this.model);
            }
        },
        onNameLanguageSelectedChange: function (e) {
            if (e != null) {
                e.stopPropagation();
            }

            this.selectedLanguage = this.$el.find(this.nameLanguageField).val();
            var inputValue;

            if (this.nameFieldValue.stringMap) {
                inputValue = this.nameFieldValue.stringMap[this.selectedLanguage];
            } else {
                inputValue = this.nameFieldValue[this.selectedLanguage][1];
            }

            this.$el.find(this.nameInputField).val(inputValue);
        },
        onKeyUpNameText: function (e) {
            e.stopPropagation();

            var languageValue = this.selectedLanguage;

            if (!jQuery.isEmptyObject(this.nameFieldValue)) {
                if (this.nameFieldValue.stringMap) {
                    languageValue = this.selectedLanguage;
                } else {
                    languageValue = this.selectedLanguage;
                }
            }

            this.nameFieldValue.stringMap[languageValue] = this.$el.find(this.nameInputField).val();
            if (!this.isSameLocaleString(this.nameFieldValue.stringMap, this.defaultNameFieldValue.stringMap)) {
                this.model.set("Name", this.nameFieldValue);
                window.appHelper.event_bus.trigger('completeness_content_updated', this.model);
            }
        },
        isSameLocaleString: function (first, second) {
            var result = true;
            _.each(first, function (value, key) {
                if ((second[key] == null && value != null) || second[key] != value) {
                    result = false;
                }
            });

            return result;
        },
        render: function () {
            var self = this;
            var values = {};
            var totalWeight = 0;
            var weights = {};
            _.each(this.model.get("Groups"), function (group) {
                weights[group.Id] = group.Weight;
                totalWeight += group.Weight;
            });

            this.$el.html(_.template(completenessDefinitionDetailTemplate, { NameValue: this.model.get("DisplayName"), WeightValue: totalWeight }));
            _.each(this.model.get("Groups"), function(group) {
                self.addRowToTable(group);
            });

            if (totalWeight > 100 || totalWeight < 100) {
                this.badWeight = true;
                this.$el.find("#exclamation-icon").show();
            } else {
                this.badWeight = false;
                this.$el.find("#exclamation-icon").hide();
            }

            if (totalWeight > 0) {
                window.appHelper.event_bus.trigger('completeness_definition_weight_updated', weights);
            }

            this.$el.find(this.nameLanguageField).empty();
            var langCollection = new languagesCollection();
            langCollection.fetch({
                success: function (languages) {
                    _.each(languages.toJSON(), function (model) {
                        values[model.Name] = model.DisplayName;
                        if (self.selectedLanguage == model.Name) {
                            self.$el.find(self.nameLanguageField).append('<option value="' + model.Name + '" selected>' + model.DisplayName + '</option>');
                        } else {
                            self.$el.find(self.nameLanguageField).append('<option value="' + model.Name + '">' + model.DisplayName + '</option>');
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            var collection = new entityTypeCollection();
            collection.fetch({
                success: function (entityTypes) {
                    _.each(entityTypes.toJSON(), function (entityType) {
                        if (self.model.get("EntityTypeId") == entityType.Id) {
                            self.$el.find(self.entityTypeField).append('<option value="' + entityType.Id + '" selected>' + entityType.DisplayName + '</option>');
                        } else {
                            self.$el.find(self.entityTypeField).append('<option value="' + entityType.Id + '">' + entityType.DisplayName + '</option>');
                        }
                    });

                    if (self.entityTypeFieldValue == null) {

                        var defaultOptionValue = self.$el.find((self.entityTypeField + " option:first")).val();
                        self.model.set("EntityTypeId", defaultOptionValue);
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            if (this.action != "add") {
                this.$el.find(this.entityTypeField).prop('disabled', 'true');
            }

            if (this.badWeight) {
                this.$el.find("#exclamation-icon").show();
            } else {
                this.$el.find("#exclamation-icon").hide();
            }

            return this;
        }
    });

    return completenessDefinitionDetailView;
});
