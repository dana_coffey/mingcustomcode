define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'alertify',
   'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, backgrid, alertify, inRiverUtil) {

    var gridDeleteItem = backgrid.Cell.extend({
        template: _.template("<i class='fa fa-trash-o' style='font-size: 16px;'></i>"),
        events: {
            "click": "deleteItem"
        },
        deleteItem: function (e) {

            e.stopPropagation();
            e.preventDefault();
            
            var confirmCallback = function(that) {
                that.model.destroy({
                    success: function () {
                        inRiverUtil.Notify("Item has been deleted");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
            inRiverUtil.NotifyConfirm("Confirm action", "Do you want to delete this item?", confirmCallback, this);
        },
        render: function () {
            this.$el.html(this.template());
            this.delegateEvents();
            return this;
        }
    });

  return gridDeleteItem;
  
});
