﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'collections/users/UsersCollection',
    'text!templates/completeness/completenessActionTemplate.html'
], function ($, _, backbone, inRiverUtil, usersCollection, completenessActionTemplate) {

    var completenessActionView = backbone.View.extend({
        initialize: function(options) {
            this.definitionId = options.definitionId;
            this.groupId = options.groupId;
            this.ruleId = options.ruleId;
            this.nodeType = options.type;
            this.model = options.model;
            this.action = options.action;

            // Only when adding a new action.
            if (this.model.get("CompletenessDefinitionId") == 0) {
                this.model.set("CompletenessDefinitionId", this.definitionId);
            }
            if (this.nodeType == "Group") {
                this.model.set("CompletenessGroupId", this.groupId);
            }
            if (this.nodeType == "Rule") {
                this.model.set("CompletenessRuleId", this.ruleId);
            }

            this.createActionTriggers();

            this.render();
        },
        events: {
            "change #action-trigger-value": "onValueChanged",
            "change #action-task-name-value": "onValueChanged",
            "change #action-task-description-value": "onValueChanged",
            "change #action-task-assign-to-value": "onValueChanged",
            "change #action-task-created-by-value": "onValueChanged",
            "change #actiontype": "onValueChanged",
        },
        setDefaultValues: function() {
            this.model.set("ActionTrigger", this.$el.find("#action-trigger-value").val());
            this.model.set("TaskName", "");
            this.model.set("TaskDescription", "");
            this.model.set("ActionTask", "task,");


            var assignedTo = this.$el.find("#action-task-assign-to-value")[0];
            this.model.set("TaskAssignedTo", assignedTo[assignedTo.selectedIndex].text);

            var createdBy = this.$el.find("#action-task-created-by-value")[0];
            this.model.set("TaskCreatedBy", createdBy[createdBy.selectedIndex].text);
        },
        onValueChanged: function(e) {
            e.stopPropagation();

            var targetId = e.currentTarget.id;
            switch(targetId) {
                case "action-trigger-value":
                    this.model.set("ActionTrigger", e.currentTarget[e.currentTarget.selectedIndex].text);
                    break;
                case "action-task-name-value":
                    this.model.set("TaskName", e.currentTarget.value);
                    break;
                case "action-task-description-value":
                    this.model.set("TaskDescription", e.currentTarget.value);
                    break;
                case "action-task-assign-to-value":
                    this.model.set("TaskAssignedTo", e.currentTarget[e.currentTarget.selectedIndex].text);
                    break;
                case "action-task-created-by-value":
                    this.model.set("TaskCreatedBy", e.currentTarget[e.currentTarget.selectedIndex].text);
                    break;
                case "actiontype":
                    var checked = this.$el.find("input[name=actiontype]:checked");
                    var checkedlist = "";
                    $.each(checked, function () {

                        checkedlist += this.value + ",";

                    });
                    this.model.set("ActionType", checkedlist);
                    break;
            }
        },
        createActionTriggers: function () {
            this.actionTriggers = [];
            if (this.nodeType == "Rule") {
                this.actionTriggers = ["onRuleComplete"];
                return;
            }

            if (this.nodeType == "Group") {
                this.actionTriggers = ["onGroupComplete"];
                return;
            }

            this.actionTriggers = ["onEntityCreated", "onDefinitionComplete"];
        },
        addUsers: function () {
            var self = this;
            _.each(this.users.models, function(model) {
                var userId = model.get("Id");
                var userName = model.get("Username");

                if (self.model.get("TaskAssignedTo") == userName) {
                    self.$el.find("#action-task-assign-to-value").append('<option value="' + userId + '" selected>' + userName + '</option>');
                } else {
                    self.$el.find("#action-task-assign-to-value").append('<option value="' + userId + '">' + userName + '</option>');
                }

                if (self.model.get("TaskCreatedBy") == userName) {
                    self.$el.find("#action-task-created-by-value").append('<option value="' + userId + '" selected>' + userName + '</option>');
                } else {
                    self.$el.find("#action-task-created-by-value").append('<option value="' + userId + '">' + userName + '</option>');
                }
            });
        },
        addActionTriggers: function () {
            var self = this;

            _.each(this.actionTriggers, function(trigger) {
                if (self.model.get("ActionTrigger") == trigger) {
                    self.$el.find("#action-trigger-value").append('<option value="' + trigger + '" selected>' + trigger + '</option>');
                } else {
                    self.$el.find("#action-trigger-value").append('<option value="' + trigger + '">' + trigger + '</option>');
                }
            });
        },
        render: function () {
            var self = this;

            this.$el.html(_.template(completenessActionTemplate, { TaskName: this.model.get("TaskName"), TaskDescription: this.model.get("TaskDescription"), actiontask: this.model.get("ActionType") }));

            this.addActionTriggers();

            this.users = new usersCollection();
            this.users.fetch({
                success: function() {
                    self.addUsers();

                    if (self.action == "add") {
                        self.setDefaultValues();
                    }
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            return this;
        }
    });

    return completenessActionView;
});
