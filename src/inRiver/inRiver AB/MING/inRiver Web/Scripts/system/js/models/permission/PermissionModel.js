define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var PermissionModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/permission',
        defaults: {
        },
        toString: function () {
            return this.get("Name");
            //return this.toJSON().Name;
        },
        schema: { // Used by backbone forms extension
            Name: { type: 'Text', validators: ['required', { type: 'regexp', message: 'Value must be only alphanumeric', regexp: /^[a-zA-Z0-9]*$/ }], editorAttrs: { disabled: true } },
            Description: { type: 'Text', validators: ['required'] }
        }
    });

    return PermissionModel;
});