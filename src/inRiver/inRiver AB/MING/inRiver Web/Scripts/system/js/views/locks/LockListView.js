define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'models/lock/LockModel',
  'collections/locks/LocksCollection',
  'text!templates/locks/locksTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, LockModel, LocksCollection, locksTemplate) {

    var LockListView = Backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            var that = this;

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new LocksCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //console.log("LockListView init");

            var columns = [
               {// name is a required parameter, but you don't really want one on a select all column
                // Backgrid.Extension.SelectRowCell lets you select individual rows
                // Backgrid.Extension.SelectAllHeaderCell lets you select all the row on a page
                name: "", cell: "select-row", headerCell: "select-all" },
                { name: "name", label: "Name", cell: "string" },
                { name: "description", label: "Description", cell: "string" },
                { name: "type", label: "Type", cell: "string" },
                { name: "lockedBy", label: "Locked by", cell: "string" }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });
        },
        events: { "click #unLock": "unLock" },
        unLock: function () {
            var selectedModels = this.grid.getSelectedModels();

            _.each(selectedModels, function (sModel) {
                sModel.destroy({
                        success: function(m) {
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        },
                        wait: true
                });
            });
        },
        render: function () {
            this.$el.html(locksTemplate);
            //console.log("LockListView Render");
            this.$el.find("#locks-list").append(this.grid.render().$el);
            return this;
        }
    });

    return LockListView;
});
