define([
  'jquery',
  'underscore',
  'backbone',
  'models/lock/LockModel'
], function($, _, Backbone, LockModel){
  var LockCollection = Backbone.Collection.extend({
      model: LockModel,
      url: '/api/lock'
  });
 
  return LockCollection;
});
