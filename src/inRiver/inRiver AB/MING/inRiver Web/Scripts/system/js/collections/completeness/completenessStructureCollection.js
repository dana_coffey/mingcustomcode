﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/completeness/completenessStructureModel'
], function ($, _, backbone, completenessStructureModel) {
    var completenessStructureCollection = backbone.Collection.extend({
        model: completenessStructureModel,
        url: '/api/completeness/structure'
    });

    return completenessStructureCollection;
});
