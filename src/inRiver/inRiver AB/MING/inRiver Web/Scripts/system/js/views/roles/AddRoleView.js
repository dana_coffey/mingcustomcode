define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-modal',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'models/role/RoleModel',
  'collections/permissions/PermissionsCollection',
    'text!templates/home/modalTemplate.html'
], function ($, _, Backbone, Modal, alertify, inRiverUtil, RoleModel, PermissionsCollection, modalTemplate) {

  var RoleView = Backbone.View.extend({
      initialize: function (data) {
          this.undelegateEvents();
          this.parent = data.data.parent;
          this.model = new RoleModel();
         
          this.render();
      },
      events: {
          "click button#addRole": "save",
          "click button#closeModal": "cancel",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown"

      },
      cancel: function () {
          $("#modalWindow").hide();

      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.save();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.save();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.save();
      },
      save: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;
              
              this.model.save({
                  Name: this.form.fields.Name.editor.getValue(),
                  Description: this.form.fields.Description.editor.getValue(),
                  Permissions: _.map(this.form.fields.Permissions.editor.getValue(), function (item) {
                      // this is a bit ugly but enough for the logics to work
                      // best option would be to assign en entire Role model here instead of just the id
                      // this is because of the odd checkboxes support in Backbone Forms
                      return { Id: item };
                  })
              }, {
                  success: function () {
                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                      self.parent.collection.fetch();

                      $("#modalWindow").find("div").html("");
                      $("#modalWindow").hide();
                      inRiverUtil.Notify("Role has been added successfully");
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  },
                  wait: true
              });
          }
      },
      onContentChanged: function () {
          this.save();
      },
      render: function (id) {
          var self = this;

          this.form = new Backbone.Form({
              model: this.model
          });

          // Create a modal view class
          var Modal = Backbone.Modal.extend({
              template: _.template(modalTemplate),
              cancelEl: '#closeModal',
          });


          // Render an instance of your modal
          var modalView = new Modal();

          this.$el.html(modalView.template({ data: "Add Role" }));
          this.$el.html(modalView.render().el);
            
          this.$el.find(".modal_title").html("<h2>Add Role</h2>");
          var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addRole">Add</button>');
          this.$el.find(".modal_bottombar").prepend($buttonAdd);
          this.$el.find(".modal_section").append(this.form.render().el);

          //// Populate Parent Id
          //var options = [""].concat(_.pluck(self.collection.models, "id"));
          //self.form.fields.ParentId.editor.setOptions(options);
          ////this.$el.html(this.form.render().el);

          //remove disabled when add
          this.$el.find("input[name='Name']").removeAttr("disabled");
          this.$el.find("ul[id*=Permissions]").removeClass("form-control");

          // populate the roles checkbox list... this is a bit of hack because Backbone.Forms didnt seem to support dynamic checkbox lists so well...
          var onDataHandler = function (m, r) {
              //var names = self.collection.pluck('Name');
              self.form.fields.Permissions.editor.setOptions(m);
             // self.form.fields.Permissions.editor.setValue(_.pluck(self.model.get("Permissions"), "Id")); // pluck will return an array of values that we set as selected
          };

          self.collection = new PermissionsCollection();
          self.collection.fetch({
              success: onDataHandler,
              error: function (model, response) {
                  inRiverUtil.OnErrors(model, response);
              }
          });

          return this; 

      }
  });

  return RoleView;
});
