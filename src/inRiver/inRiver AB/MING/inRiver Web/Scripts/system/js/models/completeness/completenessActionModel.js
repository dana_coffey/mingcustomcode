define([
  'underscore',
  'backbone'
], function (_, backbone) {
    
    var completenessActionModel = backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/completeness/action',
    });

    return completenessActionModel;

});
