define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/cvl/CvlValuesView',
  'views/cvl/AddCvlValueView',
  'models/cvl/CvlModel',
  'collections/cvl/CvlCollection',
  'collections/cvl/CvlValueCollection',
  'text!templates/cvl/cvlTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, cvlValuesView, addCvlValueView, cvlModel, cvlCollection, cvlValueCollection, cvlTemplate) {

    var cvlView = backbone.View.extend({
       // el: $("#main-container"),
       // tagName: 'div',
        initialize: function () {
            this.undelegateEvents();
            this.render();
        },
        events: {
            "click button#saveButton": "save",
            "click button#cancelButton": "cancel",
            "submit": "submit",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
        },
        cancel: function () {
            $("#edit-panel").slideUp(200, "swing");
            backbone.trigger('closeEditPanel');
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.save();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.save();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.save();
        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;
                
                this.model.save({
                    DataType: this.form.fields.DataType.editor.getValue(),
                    ParentId: this.form.fields.ParentId.editor.getValue(),
                }, {
                    success: function (model) {
                        self.cvlValuesView.updateParents(model);
                        inRiverUtil.Notify("CVL has been updated successfully");

                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                    },
                    error: function (model, xhr) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        newCvlValue: function (cvlId, isLocaleString, parentKey) {
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new addCvlValueView({
                isLocaleString: isLocaleString, cvlId: cvlId, parentKey : parentKey
            });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
       
        onContentChanged: function () {
            event.stopPropagation();
            this.save();
        },
        render: function (id) {
            var self = this;
            var customCvl = "";

            if (this.model.get('CustomValueList')) {
                customCvl = "<b>Custom CVL:</b> " + this.model.get('CustomValueList');
            }

            this.$el.html(_.template(cvlTemplate, { cvlId: this.model.get('Id'), dataType: this.model.get('DataType'), customCVL: customCvl }));

            this.form = new backbone.Form({
                model: this.model
            });

            this.$el.find("#cvl-details").append(this.form.render().el);
            this.$el.find(".field-Id").hide();
            this.$el.find(".field-DataType").hide();
            this.$el.find(".field-CustomValueList").hide();

            //Populate Parent Id
            var options = [""].concat(_.pluck(self.model.collection.models, "id"));
            self.form.fields.ParentId.editor.setOptions(options);

            var hasParent = self.model.attributes.ParentId != null && self.model.attributes.ParentId != ""; 

            var isLocaleString = false;
            var dataType = this.model.get("DataType");
            if (dataType == "LocaleString") {
                isLocaleString = true;
            }

            this.cvlValueList = new cvlValueCollection([], { cvlId: this.model.get("Id") });
            this.cvlValueList.fetch({
                success: function (m) {
                    self.$el.find("#edit-panel-properties").append(self.form.el);

                    var firstModel = m.at(0);
                    self.cvlValuesView = new cvlValuesView({ model: firstModel, collection: m, hasParent: hasParent }).render();

                    self.$el.find("#newCvlValue").on('click', function () {
                        self.newCvlValue(self.model.get("Id"), isLocaleString, self.model.get("ParentId"));
                    });

                    self.$el.find("#cvl-content").html(self.cvlValuesView.$el);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        
            return this;
        }
    });

    return cvlView;
});
