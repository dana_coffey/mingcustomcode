﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/completeness/completenessRuleTypeSettingTemplate.html'
], function($, _, backbone, inRiverUtil, completenessRuleTypeSettingTemplate) {

    var completenessRuleTypeSettingView = backbone.View.extend({
        initialize: function(options) {
            this.Type = options.Type;
            this.Label = options.Label;
            this.Value = options.Value;

            this.Key = this.Type.substring(this.Type.lastIndexOf(".") + 1) + "-" + this.Label;
            this.render();
        },
        events: function () {
            var theEvents = {};
            theEvents["change #criteria-value-" + this.Key] = "onValueChanged";
            return theEvents;
        },
        onValueChanged: function(e) {
            e.stopPropagation();

            var newValue = e.currentTarget.value;
            window.appHelper.event_bus.trigger('completeness_criteria_value_updated', this.Type, this.Label, newValue);
        },
        render: function () {
            this.$el.html(_.template(completenessRuleTypeSettingTemplate, { Label: this.Label, Key: this.Key, Value: this.Value }));

            return this;
        }
    });

    return completenessRuleTypeSettingView;
});
