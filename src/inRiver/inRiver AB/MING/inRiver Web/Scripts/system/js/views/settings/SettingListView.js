define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'sharedjs/misc/inRiverUtil',
  'extensions/GridDeleteItem',
  'views/sidebar/SidebarView',
  'sharedjs/models/setting/SettingModel',
  'sharedjs/collections/settings/SettingsCollection',
  'views/settings/SettingView',
  'views/settings/AddSettingView',
  'text!templates/settings/settingsTemplate.html'
], function ($, _, Backbone, Backgrid, inRiverUtil, GridDeleteItem, SidebarView, SettingModel, SettingsCollection, SettingView, AddSettingView, settingsTemplate) {

    var SettingListView = Backbone.View.extend({
        initialize: function (data) {
            //Get Settings
            var that = this;
            this.undelegateEvents();

            var onDataHandler = function (collection) {
                that.render();
            };

            that.collection = new SettingsCollection();
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            that.collection.on('rowClicked', function (model, selected) {
                event.stopPropagation();

                var modWindow = $("#modalWindow").find("div").html();
                var modalWindowOpen = $('#modalWindow').is(':visible');


                if (modalWindowOpen) {
                    return false;
                }


                var oldView = this._editCvlView;
                if (oldView != null) {
                    oldView.remove();
                    oldView.undelegateEvents();
                }


                $("#edit-panel").slideDown(200, "swing", function () { });
                console.log("onLoadEditCvlView");

                that._editCvlView = new SettingView({ model: model });
                $("#edit-panel").html(that._editCvlView.el);
            });

            console.log("SettingListView init");

            var columns = [
                { name: "Setting", label: "Setting", cell: "string" },
                { name: "Value", label: "Value", cell: "string" },
                { name: "", cell: GridDeleteItem }
            ];

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });


            this.listenTo(Backbone, 'clickedAnywhere', function () { this.onUnloadSettingView(); });
         
        },

        events: {
            "click #newSetting": "newSetting",
            "click #edit-panel" : "onClick"
        },
        newSetting: function () {
            //Backbone.history.navigate("setting/create", true);
            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);
            this._modalView = new AddSettingView({ data: { header: "Create User", parent: this } });
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        onUnloadSettingView: function (parameters) {
            console.log("onUnloadSettingView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                Backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;

        },
        onClick: function () {
            event.stopPropagation();

        },
        render: function () {
            this.$el.html(settingsTemplate);
            console.log("SettingListView Render");
            this.$el.find("#settings-list").append(this.grid.render().$el);
        //    return this;
        }
    });

    return SettingListView;
});
