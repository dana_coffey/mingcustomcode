define([
  'underscore',
  'backbone'
], function (_, backbone) {
    
    var completenessCriteriaModel = backbone.Model.extend({
        initialize: function () {
        },
        urlRoot: '/api/completeness/criteria',
    });

    return completenessCriteriaModel;

});
