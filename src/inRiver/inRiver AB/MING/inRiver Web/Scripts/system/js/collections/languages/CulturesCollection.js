define([
  'jquery',
  'underscore',
  'backbone',
  'models/language/CultureModel'
], function($, _, Backbone, CultureModel){
  var CulturesCollection = Backbone.Collection.extend({
      model: CultureModel,
      url: '/api/culture'
  });
 
  return CulturesCollection;
});
