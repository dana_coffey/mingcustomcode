define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'sharedjs/models/job/JobModel',
  'sharedjs/collections/jobs/JobsCollection',
  'text!templates/jobs/jobTemplate.html'
], function($, _, Backbone, alertify, inRiverUtil, SidebarView, JobModel, JobCollection, jobTemplate){

  var JobView = Backbone.View.extend({
 
      initialize: function () {
          this.undelegateEvents();
          this.render();
      },
      events: {
          "click button#saveButton": "saveJob",
          "click button#cancelButton": "cancel",
          "click": "onClick",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown",
      },
      cancel: function () {
          $("#edit-panel").slideUp(200, "swing");
          Backbone.trigger('closeEditPanel');

      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.saveJob();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.saveJob();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.saveJob();
      },
      saveJob: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;

              
              this.model.save({
                  Job: this.form.fields.Job.editor.getValue(),
                  Interval: this.form.fields.Interval.editor.getValue(),
                  Enabled: this.form.fields.Enabled.editor.getValue()
              }, {
                  success: function () {
                      inRiverUtil.Notify("Job has been updated successfully");

                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                      $("#edit-panel").slideUp(200, "swing");
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  },
                  wait: true
              });
          }
      },
      onContentChanged: function () {
          this.saveJob();
      },
      onClick: function () {
          event.stopPropagation();

      },
      render: function () {
          this.$el.html(jobTemplate);
          this.form = new Backbone.Form({
              model: this.model
          });

          this.$el.find("#job-content").append(this.form.render().el);
      }
  });

  return JobView;
});
