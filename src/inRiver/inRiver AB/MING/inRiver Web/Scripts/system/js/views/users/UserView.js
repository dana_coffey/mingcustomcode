define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/sidebar/SidebarView',
  'models/user/UserModel',
  'collections/users/UsersCollection',
  'collections/roles/RolesCollection',
  'collections/authentication/AuthenticationCollection',
  'text!templates/users/userTemplate.html'
], function ($, _, Backbone, alertify, inRiverUtil, SidebarView, UserModel, UsersCollection, RolesCollection, AuthenticationCollection, userTemplate) {

    var UserView = Backbone.View.extend({
        initialize: function (data) {
            this.render();
            this.undelegateEvents();
            //_.bindAll(this, "onContentChanged");
        },
        events: {
           "click": "onClick",
           "click button#saveButton": "saveUser",
           "click button#cancelButton": "cancel",
           "submit": "submit",
           "keyup": "onKeyUp",
           "keydown": "onKeyDown",
           "click button#generateKeyButton": "generateKey",
           "click button#removeKeyButton": "removeKey"
        },
        deleteUser: function () {
            //Delete model
            this.model.destroy();
            this.collection.remove(this.model);
            $("#edit-panel").slideUp(200, "swing");
            //Delete view
            //this.remove();
        },
        cancel: function () {
            $("#edit-panel").slideUp(200, "swing");
            Backbone.trigger('closeEditPanel');

        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                this.saveUser();
                return;
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.saveUser();
                        break;
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.saveUser();
        },
        saveUser: function () {
            if (this.form.fields.Password.editor.getValue() == null || this.form.fields.Password.editor.getValue() == "") {
                this.form.fields.Password.editor.validators = null; 
                this.form.fields.Password2.editor.validators = null;
            }

            if (this.model.get("AuthenticationType") == "ActiveDirectory") {
                this.form.fields.Username.editor.validators = null;
            }

            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;

                var role = _.map(this.form.fields.Roles.editor.getValue(), function (item) {
                    // this is a bit ugly but enough for the logics to work
                    // best option would be to assign en entire Role model here instead of just the id
                    // this is because of the odd checkboxes support in Backbone Forms
                    return { Id: item };
                });

            this.model.save({
                    AuthenticationType: this.form.fields.AuthenticationType.editor.getValue(),
                    FirstName: this.form.fields.FirstName.editor.getValue(),
                    LastName: this.form.fields.LastName.editor.getValue(),
                    Username: this.form.fields.Username.editor.getValue(),
                    Email: this.form.fields.Email.editor.getValue(),
                    Settings: { Password: this.form.fields.Password.editor.getValue() },
                    Roles: role,
                    RestAPIkey: this.form.fields.RestAPIkey.editor.getValue()
                }, {
                    success: function () {
                        var onDataHandler = function (m, r) {
                            var roleslist = _.pluck(role, "Id").join(", ");
                            self.model.set("RolesList", roleslist);
                            inRiverUtil.Notify("User has been updated successfully");
                            $("#edit-panel").slideUp(200, "swing");
                        };
                        self.model.fetch({
                            success: onDataHandler,
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        }); // to make sure we get a complete description of the model from server (with role descriptions etc)
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                });
            }
        },
        onContentChanged: function () {
            this.saveUser();
        },
        onClick: function() {
            event.stopPropagation(); 

        },
        setGeneratedApiKey: function (key) {
            this.model.set("RestAPIkey", key);
            this.$el.find("#user-content fieldset .field-RestAPIkey TextArea").first().text(key);
        },
        generateKey: function () {

            var username = this.model.id;
            var that = this;

            $.ajax({
                type: "POST",
                url: "../../../Admin/Handlers/UserHandler.ashx",
                data: { method: "GenerateRestApiKey", id: username },
                success: function (result) {
                    inRiverUtil.Notify("Rest Api key for user '" + username + "' has been generated and saved successfully");
                    
                    that.setGeneratedApiKey(result);
                },
                error: function () {
                    // TODO?
                }
            });
        },
        removeKey: function () {
            var username = this.model.id;
            var that = this;

            $.ajax({
                type: "POST",
                url: "../../../Admin/Handlers/UserHandler.ashx",
                data: { method: "RemoveRestApiKey", id: username },
                success: function () {
                    inRiverUtil.Notify("Rest Api key for user '" + username + "' has been removed and saved successfully");

                    that.setGeneratedApiKey("");
                },
                error: function () {
                    // TODO?
                }
            });
        },
        render: function (id) {
            var self = this;

            this.$el.html(userTemplate);

            this.form = new Backbone.Form({
                model: this.model
            });

            this.$el.find("#user-content").append(this.form.render().el);
            this.$el.find("ul[id*=Roles]").removeClass("form-control");

            // �nject a button to be able to generate a new API-key
            var RestAPIkeyCtrl = this.$el.find("#user-content fieldset .field-RestAPIkey .controls");
            RestAPIkeyCtrl.append('<button class="btn" type="button" id="generateKeyButton" style="width: 120px">Generate</button>');
            RestAPIkeyCtrl.append('<br><br><button class="btn" type="button" id="removeKeyButton" style="width: 120px">Remove</button>');

            // populate the roles checkbox list... this is a bit of hack because Backbone.Forms didnt seem to support dynamic checkbox lists so well...
            var onDataHandler = function (m, r) {
                self.form.fields.Roles.editor.setOptions(m);
                self.form.fields.Roles.editor.setValue(_.pluck(self.model.get("Roles"), "Id")); // pluck will return an array of values that we set as selected
            };

            self.collection = new RolesCollection();
            self.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            // set Auth type
            var authType = self.form.fields.AuthenticationType.editor.getValue();

            if (authType == "ActiveDirectory") {
                self.$el.find("input[name='FirstName']").attr("disabled", true);
                self.$el.find("input[name='LastName']").attr("disabled", true);
                self.$el.find("input[name='Password']").attr("disabled", true);
                self.$el.find("input[name='Password2']").attr("disabled", true);
            }

            var authHandler = function (m, r) {
                if (r == "Mixed") {
                    self.$el.find(".form-group field-AuthenticationType").show();
                } else {
                    self.$el.find(".form-group field-AuthenticationType").hide();
                }
            };

            self.collection = new AuthenticationCollection();
            self.collection.fetch({
                success: authHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            return this;
        }
    });

    return UserView;
});
