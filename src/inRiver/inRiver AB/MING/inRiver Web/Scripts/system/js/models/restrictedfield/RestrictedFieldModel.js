define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var RestrictedFieldModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/restrictedfield',
        schema: { // Used by backbone forms extension
            EntityTypeId: { type: 'Select', title: 'Entity Type', options: [], validators: ['required'] },
            CategoryId: { type: 'Select', title: 'Category', options: [] },
            FieldTypeId: { type: 'Select',  title: 'Field Type', options: [] },
            RestrictionType: { type: 'Select', title: 'Restriction', options: ['Readonly', 'Hidden'], validators: ['required'] },
            RoleId: { type: 'Select', title: 'Role', options: [], validators: ['required'] }
        }
    });

    return RestrictedFieldModel;
});
