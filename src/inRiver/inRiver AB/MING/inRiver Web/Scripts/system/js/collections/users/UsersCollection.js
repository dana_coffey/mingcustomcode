define([
  'jquery',
  'underscore',
  'backbone',
  'models/user/UserModel'
], function($, _, Backbone, UserModel){
  var UsersCollection = Backbone.Collection.extend({
      model: UserModel,
      url: '/api/user'
  });
 
  return UsersCollection;
});
