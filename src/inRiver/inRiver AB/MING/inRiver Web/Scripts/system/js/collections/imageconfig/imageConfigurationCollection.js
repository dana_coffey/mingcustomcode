﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/imageconfig/imageConfigurationModel'
], function ($, _, backbone, imageConfigurationModel) {
    var imageConfigurationCollection = backbone.Collection.extend({
        model: imageConfigurationModel,
        url: '/api/imageconfiguration'
    });

    return imageConfigurationCollection;
});