define([
  'jquery',
  'underscore',
  'backbone',
  'models/completeness/completenessActionModel'
], function ($, _, backbone, completenessActionModel) {
    var completenessActionCollection = backbone.Collection.extend({
        model: completenessActionModel,
      url: '/api/completeness/action'
  });
 
    return completenessActionCollection;
});
