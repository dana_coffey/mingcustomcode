define([
  'jquery',
  'underscore',
  'backbone',
   'backbone-modal',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'models/restrictedfield/RestrictedFieldModel',
  'collections/restrictedfields/RestrictedFieldsCollection',
  'collections/entitytype/EntityTypeCollection',
  'collections/roles/RolesCollection',
  'collections/categories/CategoriesCollection',
  'collections/fieldtypes/FieldTypesCollection',
  'text!templates/home/modalTemplate.html'
], function ($, _, Backbone, Modal, alertify, inRiverUtil, RestrictedFieldModel, RestrictedFieldsCollection, EntityTypeCollection, RolesCollection, CategoriesCollection, FieldTypesCollection, modalTemplate) {

  var AddRestrictedFieldView = Backbone.View.extend({
      initialize: function (data) {
          this.undelegateEvents();
          this.parent = data.data.parent;
          this.model = new RestrictedFieldModel();
          this.render();
      },
      events: {
          "click button#addRestricted": "save",
          "click button#closeModal": "cancel",
          "submit": "submit",
          "keyup": "onKeyUp",
          "keydown": "onKeyDown"

      },
      cancel: function () {
          $("#modalWindow").hide();

      },
      onKeyUp: function (event) {
          event.preventDefault();
          event.stopPropagation();
          if (event.keyCode == 9) {
              return;
          }

          // Enter
          if (event.keyCode == 13) {
              this.save();
              return;
          }
      },
      onKeyDown: function (event) {
          event.stopPropagation();

          if (event.ctrlKey || event.metaKey) {
              switch (String.fromCharCode(event.which).toLowerCase()) {
                  case 's':
                      event.preventDefault();
                      this.save();
                      break;
              }
          }
      },
      submit: function (e) {
          e.stopPropagation();
          e.preventDefault();
          this.save();
      },
      save: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;

              //var cultValue = this.form.fields.Name.editor.getValue(); 
              this.model.save({
                 // DisplayName: this.form.fields.DisplayName.editor.getValue(),
                  EntityTypeId: self.form.fields.EntityTypeId.editor.getValue(),
                  CategoryId: self.form.fields.CategoryId.editor.getValue(),
                  FieldTypeId:self.form.fields.FieldTypeId.editor.getValue(), 
                  RestrictionType: self.form.fields.RestrictionType.editor.getValue(), 
                  RoleId: self.form.fields.RoleId.editor.getValue()
              }, {
                  success: function () {
                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                      self.parent.collection.fetch();
                      $("#modalWindow").hide();
                      inRiverUtil.Notify("Restricted field has been added successfully");
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  },
                  wait: true
              });
          }
      },
      render: function (id) {
          var self = this;

          this.form = new Backbone.Form({
              model: this.model
          });

          // Create a modal view class
          var Modal = Backbone.Modal.extend({
              template: _.template(modalTemplate),
              cancelEl: '#closeModal',
          });

          // Render an instance of your modal
          var modalView = new Modal();
          this.$el.html(modalView.render().el);

          this.$el.find(".modal_title").html("<h2>Add Restricted Field</h2>");
          var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addRestricted">Add</button>');
          this.$el.find(".modal_bottombar").prepend($buttonAdd);

          this.$el.find(".modal_section").append(this.form.render().el);

          // Populate Entity Types
          var onDataHandler = function (collection) {
              self.form.fields.EntityTypeId.editor.setOptions(collection);
          };

          var entityTypeCollection = new EntityTypeCollection();
          entityTypeCollection.fetch({
              success: onDataHandler,
              error: function (model, response) {
                  inRiverUtil.OnErrors(model, response);
              }
          });

          // Populate Role
          var onDataHandler2 = function (collection) {
              self.form.fields.RoleId.editor.setOptions(collection);
          };

          var rolesCollection = new RolesCollection();
          rolesCollection.fetch({
              success: onDataHandler2,
              error: function (model, response) {
                  inRiverUtil.OnErrors(model, response);
              }
          });

          self.form.on('EntityTypeId:change', function (form, editor) {
              var entityTypeId = editor.getValue();

              self.EntityTypeId = entityTypeId; 
              var newOptions;

              var catColl = new CategoriesCollection(); // id 0 means new object 
              catColl.url = "/api/category/" + entityTypeId;

              var that = this;
              catColl.fetch({
                  success: onDataHandler3,
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  }
              });
              
              var fieldTypeColl = new FieldTypesCollection(); // id 0 means new object 
              fieldTypeColl.url = "/api/fieldtype/" + entityTypeId;

              fieldTypeColl.fetch({
                  success: onDataHandler4,
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  }
              });
          });

          // Populate Categories
          var onDataHandler3 = function (collection) {
              self.form.fields.CategoryId.editor.setOptions(collection);
          };
          // Populate Categories
          var onDataHandler4 = function (collection) {
              self.form.fields.FieldTypeId.editor.setOptions(collection);
          };


          self.form.on('CategoryId:change', function (form, editor) {
              self.form.fields.FieldTypeId.editor.setValue(""); 

          });
          self.form.on('FieldTypeId:change', function (form, editor) {
              self.form.fields.CategoryId.editor.setValue("");

          });


          return this;
      }
  });

  return AddRestrictedFieldView;
});