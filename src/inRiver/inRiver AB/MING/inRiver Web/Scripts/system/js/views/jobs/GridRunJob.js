define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'alertify',
  'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, backgrid, alertify, inRiverUtil) {

    var gridRunJob = backgrid.Cell.extend({
        template: _.template("<i class='fa fa-play' style='font-size: 16px;'></i>"),
        events: {
            "click": "runJob"
        },
        runJob: function (e) {

            e.stopPropagation();
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "/api/job/run/" + this.model.get("Job"),
                dataType: "application/json; charset=utf-8",
                success: function () {
                    inRiverUtil.Notify("Job has been run.");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            this.$el.html(this.template());
            this.delegateEvents();
            return this;
        }
    });

    return gridRunJob;

});
