define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',
  'text!templates/start/startPageLatestEventListTemplate.html',
  'views/connectorevents/connectorEventView',
  'collections/connectorevents/latestConnectorEventCollection'
], function ($, _, backbone, jqueryui, inRiverUtil, startPageLatestEventListTemplate, connectorEventView, latestConnectorEventCollection) {

    var startPageLatestEventList = backbone.View.extend({
        initialize: function () {
            var self = this;
            this.$el.html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            this.undelegateEvents();

            this.list = new latestConnectorEventCollection();
            this.list.fetch({
                success: function() {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ maxNumberOfEvents: "5", onlyForChannel: "true" })
            });
        },
        render: function () {
            var channellist = this.$el.html(startPageLatestEventListTemplate);
            if (this.list.models.length == 0) {
                this.$el.find("#latest-event-list").html("<div class='no-items-text'>No events</div>");
            } else {
                _.each(this.list.models, function (model) {
                    var eventView = new connectorEventView({ model: model });
                    channellist.find('#latest-event-list').append(eventView.render().el);
                });
            }

            return this;
        }
    });

    return startPageLatestEventList;
});

