﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/completeness/completenessModel',
  'collections/relations/relationCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/collections/entities/entityTableCollection',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'sharedjs/collections/linkrule/linkRuleCollection',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/usersetting/userSettingModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/entityview/entityTableView',
  'views/nodeview/nodeContentCardView',
  'text!templates/nodeview/nodeContentTemplate.html',
  'text!templates/nodeview/nodeContentHeaderTemplate.html',
  'text!templates/nodeview/nodeContentTypeGroupTemplate.html',
  'text!templates/nodeview/nodeContentTypeLinkEntityGroupTemplate.html'

], function ($, _, backbone, completenessModel, relationCollection, relationTypeCollection, entityTableCollection, modalPopup, inRiverUtil, permissionUtil, linkRuleCollection, relationModel, entityModel, userSettingModel, entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, entityTableView, nodeContentCardView, nodeContentTemplate, nodeContentHeaderTemplate, nodeContentTypeGroupTemplate, nodeContentTypeLinkEntityGroupTemplate) {

    var nodeContentView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.id = options.model.id;
            this.direction = "outbound";
            this.entityTypeId = options.model.attributes.EntityType;
            this.channelId = options.channelId;

            this.relationTypes = new relationTypeCollection([], { direction: this.direction, entityTypeId: this.entityTypeId });
            this.relations = new relationCollection([], { channelId: this.channelId, entityId: this.id, direction: this.direction });

            this.listMode = "box";
            this.loadContent = false;
            this.includeFields = false;

            this.droppable = permissionUtil.CheckPermission("AddLink");
            this.sortable = permissionUtil.CheckPermission("UpdateLink");

            if (window.appSession.listMode != null) {
                this.listMode = window.appSession.listMode;
            }
            if (window.appSession.loadContent != null) {
                this.loadContent = window.appSession.loadContent;
                that.collection = new linkRuleCollection({ entityid: that.id });
                that.collection.fetch({
                    success: function () {
                        if (that.collection.models.length > 0) {
                            that.droppable = false;
                        }
                        that.preRender();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    url: '/api/linkrule/' + that.id
                });
            } else {
                var key = "PlanAndReleaseShowContent";
                var settingModel = new userSettingModel(key);
                settingModel.url = "/api/usersetting/" + key;
                settingModel.fetch({
                    success: function (res, val) {

                        if (val != null && val.toLowerCase() == "true") {
                            that.loadContent = true;
                        }
                        window.appSession.loadContent = that.loadContent;

                        key = "PlanAndReleaseListMode";
                        settingModel.url = "/api/usersetting/" + key;
                        settingModel.fetch({
                            success: function (resu, vallistMode) {
                                if (vallistMode != "") {
                                    that.listMode = vallistMode;
                                    window.appSession.listMode = that.listMode;
                                } 
                                that.collection = new linkRuleCollection({ entityid: that.id });
                                that.collection.fetch({
                                    success: function () {
                                        if (that.collection.models.length > 0) {
                                            that.droppable = false;
                                        }
                                        that.preRender();
                                    },
                                    error: function (model, response) {
                                        inRiverUtil.OnErrors(model, response);
                                    },
                                    url: '/api/linkrule/' + that.id
                                });

                                //that.preRender();
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }


            //     this.relations.on('change', this.render, this);
        },
        events: {
            "click .relation-wrap": "onCardClick",
            "click #edit-link-button": "onEditLink",
            "click .add-entity-button": "onAddAndLinkEntity",
            "click .add-entity-withentity-button": "onAddAndLinkEntityWithLinkEntity",
            "click #relation-completeness-text": "onShowCompletnessDetails",
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "sortorderupdate": "onChangeSortOrderDone",
            "click #load-content-button": "onLoadContentChange",
            "click #card-view": "onChangeListMode",
            "click #sortable-view": "onChangeListMode",
            "click #table-view": "onChangeListMode",
            "click #include-fields": "onIncludeFields"
        },
        preRender: function () {
            var that = this;

            if (this.loadContent) {
                this.relationTypes.fetch(
                {
                    success: function () {
                        that.relations.fetch({
                            success: function () {
                                that.render();
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        }
                        );
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.render();
            }
        },
        onIncludeFields: function (e) {
            e.stopPropagation();
            if (this.includeFields) {
                this.includeFields = false;
            } else {
                this.includeFields = true;
            }

            this.render();
        },
        onLoadContentChange: function (e) {
            this.loadContent = !this.loadContent;
            window.appSession.loadContent = this.loadContent;
            this.preRender();

            var settingModel = new userSettingModel();
            settingModel.save({
                PlanAndReleaseShowContent: this.loadContent
            });
        },
        onContentFiltering: function () {
            var filterString = this.$el.find("#contentFilterInput").val();
            var that = this;


            if (filterString && filterString.length > 2) {
                that.$el.find(".card-wrap-large").hide();
                that.$el.find(".card-wrap-large").each(function (i, e) {
                    if (e.innerText != "") {
                        if ($(e).find(".card-text1")[0].innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(e).show();
                        if ($(e).find(".card-text2-large")[0].innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(e).show();
                    }
                });

                that.$el.find(".card-wrap").hide();
                that.$el.find(".card-wrap").each(function (i, e) {
                    if (e.innerText != "") {
                        if ($(e).find(".card-text1")[0].innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(e).show();
                        if ($(e).find(".card-text2-large")[0].innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(e).show();
                    }
                });
            } else {
                that.$el.find(".card-wrap-large").show();
                that.$el.find(".card-wrap").show();
            }

        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = e.currentTarget.find("#card-tools-container");
            $box.toggle(200);
        },
        onAddAndLinkEntity: function (e) {
            e.stopPropagation();


            var entitytypeid = e.currentTarget.id.replace("EntityType-", "");

            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;

            var linkentitytypeid = "";
            var linkentitytypeidObj = $(event.target).closest(".relation-section-wrap")[0].attributes["linkentitytypeid"];
            if (linkentitytypeidObj) {

                linkentitytypeid = linkentitytypeidObj.value;
            }

            var direction = this.direction;
            var sourceId;
            var targetId;
            if (direction == "outbound") {
                sourceId = this.id;
                targetId = -1;
            } else {
                sourceId = -1;
                targetId = this.id;
            }

            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = sourceId;
            this.targetIdToCreate = targetId;

            this.entityTypeIdToCreate = entitytypeid;
            this.linkentitytypeidToCreate = linkentitytypeid;

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeAddAndLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: entitytypeid,
                showInWorkareaOnSave: false
            });

            //this._modalView = new entityDetailSettingView({ id: 0, entityTypeId: entitytypeid });

            //window.scrollTo(0, 0);
            //$("#modalWindow").html(this._modalView.el);
            //$("#modalWindow").show();
        },
        completeAddAndLinkEntity: function (model) {
            var that = this;

            this.targetIdToCreate = model.attributes.Id;

            if (this.linkentitytypeidToCreate) {

                this.stopListening(window.appHelper.event_bus);
                this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeLinkCreatation);

                if (this._modalView) {
                    this._modalView.remove();
                }

                this._modalView = new entityDetailSettingView({ id: 0, entityTypeId: this.linkentitytypeidToCreate });

                window.scrollTo(0, 0);
                $("#modalWindow").html(this._modalView.el);
                $("#modalWindow").show();
            } else {

                var newModel = new relationModel({ LinkType: this.relationTypeToCreate, SourceId: this.sourceIdToCreate, TargetId: this.targetIdToCreate, LinkEntityId: this.linkentityidToCreate });
                newModel.save([], {
                    success: function (updatedmodel) {
                        that.relations.add(updatedmodel);

                        var relationCard = $(_.template(nodeContentTemplate, { model: updatedmodel.toJSON(), direction: that.direction, active: updatedmodel.attributes["Active"] }));

                        $("#" + updatedmodel.attributes["LinkType"] + " #relation-section-container").append(relationCard.html());

                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onAddAndLinkEntityWithLinkEntity: function (e) {
            e.stopPropagation();

            if (this._modalView) {
                this._modalView.remove();
            }
            window.scrollTo(0, 0);

            var entitytypeid = e.currentTarget.id.replace("EntityType-", "");

            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;

            var linkentityid = $(event.target).closest(".relation-sub-section-wrap")[0].id;

            var direction = this.direction;
            var sourceId;
            var targetId;
            if (direction == "outbound") {
                sourceId = this.id;
                targetId = -1;
            } else {
                sourceId = -1;
                targetId = this.id;
            }

            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = sourceId;
            this.targetIdToCreate = targetId;
            this.linkentityidToCreate = linkentityid;

            this.entityTypeIdToCreate = entitytypeid;
            this.linkentityidToCreate = linkentityid;

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeAddAndLinkEntity);

            this._modalView = new entityDetailSettingView({ id: 0, entityTypeId: entitytypeid });

            window.scrollTo(0, 0);
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        onEditLink: function (e) {
            e.stopPropagation();

            var linkEntityId = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].id;

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: linkEntityId
            });
        },
        onCardClick: function () {
            //var id = e.currentTarget.id;
            //this.goTo("entity/" + id);
        },

        onClose: function () {
        },
        completeLinkCreatation: function (model) {
            var that = this;

            if (this.LinksCollection.length > 0) {

                var subsectionEl;

                _.each(this.LinksCollection.models, function (link) {

                    var linkentityid = model.id;
                    var linktype = this.relationTypeToCreate;

                    var newModel = new relationModel({ LinkType: link.relationTypeToCreate, SourceId: link.sourceIdToCreate, TargetId: link.targetIdToCreate, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (xmodel) {

                            if (typeof subsectionEl === "undefined") {
                                that.relations.add(xmodel);

                                subsectionEl = $(_.template(nodeContentTypeLinkEntityGroupTemplate, { model: xmodel.toJSON() }));
                                $("#" + linktype + " #relation-section-container").append(subsectionEl.html());

                                var id = "#" + linkentityid.toString().replace(" ", "-");
                                var $heading = $(id + " .relation-sub-section-title");
                                var $box = $(id).find("#relation-sub-section-container");

                                var $showTask = $(id + '-show i');

                                $heading.click(function () {
                                    $box.toggle(200);
                                    $showTask.toggle();
                                });
                            }

                            var relationCard = $(_.template(nodeContentTemplate, { model: xmodel.toJSON(), direction: that.direction, active: xmodel.attributes["Active"] }));

                            $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.html());

                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });
            }

        },
        completeMultipleLinkCreatation: function () {
            var that = this;

            if (this.LinksCollection.length > 0) {
                _.each(this.LinksCollection.models, function (link) {

                    var linkentityid = model.id;
                    var linktype = link.relationTypeToCreate;

                    var newModel = new relationModel({ LinkType: link.relationTypeToCreate, SourceId: link.sourceIdToCreate, TargetId: link.targetIdToCreate, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (xmodel) {

                            if (that.$el.find("#" + linkentityid + " #relation-sub-section-container").length == 0) {

                                var subsectionEl = $(_.template(nodeContentTypeLinkEntityGroupTemplate, { model: xmodel.toJSON() }));
                                $("#" + linktype + " #relation-section-container").append(subsectionEl.html());


                                var id = "#" + linkentityid.toString().replace(" ", "-");
                                var $heading = $(id + " .relation-sub-section-title");
                                var $box = $(id).find("#relation-sub-section-container");

                                var $showTask = $(id + '-show i');

                                $heading.click(function () {
                                    $box.toggle(200);
                                    $showTask.toggle();
                                });
                            }

                            var relationCard = $(_.template(nodeContentTemplate, { model: xmodel.toJSON(), direction: that.direction, active: xmodel.attributes["Active"] }));
                            $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.html());

                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });
            }
        },
        createLineEntityAndAddRelation: function (relationType, sourceId, targetId, entitytypeid, that) {
            if (that._modalView) {
                that._modalView.remove();
            }

            that._modalView = new entityDetailSettingView({ id: 0, entityTypeId: entitytypeid });
            window.scrollTo(0, 0);
            $("#modalWindow").html(that._modalView.el);
            $("#modalWindow").show();

            that.relationTypeToCreate = relationType;
            that.sourceIdToCreate = sourceId;
            that.targetIdToCreate = targetId;

            that.entityTypeIdToCreate = entitytypeid;
            that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeLinkCreatation);
        },

        addRelation: function (event, ui, self, entitytype) {
            event.stopPropagation();


            var linkentitytypeid = "";
            if (event.target.attributes["linkentitytypeid"]) {
                linkentitytypeid = event.target.attributes["linkentitytypeid"];
            };

            var that = this;
            var addedToEntity = self.id;
            var relationType = event.target.id;
            var tableView = entitytype == null;

            var entitys = [];

            //1. Vi tar reda på idn på de som ska länkas.
            //Även typ?
            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {

                var workareaView = window.appHelper.workAreaHandler.getActiveWorkarea();

                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                //var cards = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");
                if (!workareaView) {
                    return;
                }


                //var cards2 = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");
                var cards = workareaView.getSelectedEntityIds();

                if (cards.length > 0) {
                    _.each(cards, function (card) {
                        var cardView = _.find(workareaView.cards, function (c) {
                            return c.card.model.attributes.Id == card;
                        });
                        //entitys.push({ id: card.id.replace("card-", ""), entitytype: card.attributes["entity-type-id"].value });
                        entitys.push({ id: card, entitytype: cardView.card.model.attributes.EntityType });

                    });
                } else {
                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var entities = grid.getSelectedRowKeys();
                        _.each(entities, function (entity) {
                            entitys.push({ id: entity.Id, entitytype: entity.EntityType });
                        });
                    }
                }
            } else {
                if (ui.draggable[0].id != null && ui.draggable[0].id != "") {
                    var addedEntity = { id: ui.draggable[0].id.replace("card-", ""), entitytype: ui.draggable[0].attributes["entity-type-id"].value };
                } else {

                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var selection = grid.getSelectedRowKeys();
                        addedEntity = { id: selection[0].Id, entitytype: selection[0].EntityType }
                    }
                }
                entitys.push(addedEntity);
            }


            this.LinksCollection = new Backbone.Collection();

            if (this.LinksCollection.length > 0) {
                this.LinksCollection.reset();
            }

            _.each(entitys, function (entity) {

                var addedEntity = entity.id;

                if (entitytype != null && entity.entitytype != entitytype) {
                    return;
                }

                if (tableView) {
                    var relationTypeObj = _.find(self.relationTypes.models, function (relationType) {
                        return relationType.attributes.TargetEntityTypeId == entity.entitytype;
                    });
                    if (relationTypeObj) {
                        relationType = relationTypeObj.attributes.Id;
                    }
                }

                var relationSectionElement = self.$el.find("#" + relationType + " #relation-section-container #" + entity.id);
                if (relationSectionElement.length > 0) {
                    return;
                }


                var direction = self.direction;
                var sourceId;
                var targetId;
                if (direction == "outbound") {
                    sourceId = addedToEntity;
                    targetId = addedEntity;
                } else {
                    sourceId = addedEntity;
                    targetId = addedToEntity;
                }

                if (linkentitytypeid && linkentitytypeid.value != "") {
                    var linkItemRelation = new Backbone.Model();

                    linkItemRelation.relationTypeToCreate = relationType;
                    linkItemRelation.sourceIdToCreate = sourceId;
                    linkItemRelation.targetIdToCreate = targetId;
                    linkItemRelation.entityTypeIdToCreate = linkentitytypeid.value;

                    that.LinksCollection.add(linkItemRelation);

                } else {
                    var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId });
                    newModel.save([], {
                        success: function (model) {
                            that.relations.add(model);
                            //var relationCard = $(_.template(nodeContentTemplate, { model: model.toJSON(), direction: that.direction, active: model.attributes["Active"], size: that.size }));

                            if (that.size == "small") {
                                if ($("#nodelist #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodelist #" + model.attributes["LinkType"]).removeClass("droppable-empty");
                                    // finns det någon mer som har entiteter men inte visar rubrik..
                                    $.each($("#nodelist .card-wrap").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }
                            else if (that.size == "table") {
                                that.renderTableList();
                            }
                            else {
                                if ($("#nodebox #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodebox #" + model.attributes["LinkType"]).removeClass("droppable-empty");

                                    // finns det någon mer som har entiteter men inte visar rubrik..
                                    $.each($("#nodebox .card-wrap-large").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }

                            var checkIfCardExists = self.$el.find("#" + relationType + " #relation-section-container #card-" + model.get("TargetId"));

                            if (checkIfCardExists.length === 0) {
                                var relationCardSub = new nodeContentCardView({ relation: model, direction: that.direction, size: that.size, channelId: that.channelId });

                                $("#" + model.attributes["LinkType"] + " #relation-section-container").append(relationCardSub.$el);
                            }

                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
            });

            if (linkentitytypeid && linkentitytypeid.value != "" && that.LinksCollection.length > 0) {

                that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeMultipleLinkCreatation);

                entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                    id: 0,
                    entityTypeId: linkentitytypeid.value,
                    showInWorkareaOnSave: false
                });
            }
        },

        onChangeSortOrder: function (event, ui) {

            // var that = this; 

            var id = $(ui.item[0]).find(".card-wrap")[0].id;
            var position = 0;
            var temp = $(ui.item[0]).find("#contextmenu > div");
            var relationId = temp[0].id;

            var parent = $(this).closest("div#relation-section-container");

            parent.find(".card-wrap").each(function (i) {
                if ($(this).attr('id') == id) {
                    position = i - 1;
                    if (position < 0) {
                        position = 0;
                    }

                }
            });

            // API and save order
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    ui.item.trigger('sortorderupdate', ui.item.index());
                }
            };

            xmlhttp.open("GET", "/api/tools/updaterelationsortorder/" + relationId + "/" + position, true);
            xmlhttp.send();
        },
        onChangeSortOrderDone: function () {
            this.boxListRendered = false;
            if (this.loadContent) {
                this.relations.fetch();
            }

            return;
        },
        onChangeListMode: function (e) {
            e.stopPropagation();

            if (e.currentTarget.id == "card-view") {
                this.listMode = "box";
            }
            if (e.currentTarget.id == "sortable-view") {
                this.listMode = "list";
            }
            if (e.currentTarget.id == "table-view") {
                this.listMode = "table";
            }

            window.appSession.listMode = this.listMode;

            this.setContentList();

            var settingModel = new userSettingModel();
            settingModel.save({
                PlanAndReleaseListMode: this.listMode
            });


        },
        createDroppable: function (section, id) {
            var self = this;
            section.droppable({

                accept: function (draggable) {

                    var drager = draggable[0].id;

                    if (drager == "") {
                        //kolla om det är från en tabell.

                        var gridDiv = $(draggable[0]).closest("#entity-table-container");
                        if (gridDiv.length > 0) {
                            var grid = gridDiv.dxDataGrid('instance');
                            var entities = grid.getSelectedRowKeys();
                            if (entities && entities.length > 0) {
                                var ok = false;
                                var i = 0;
                                while (!ok && i < entities.length) {
                                    var x = $(this).find("#" + entities[i].Id);
                                    ok = !x || x.length == 0;

                                    ok = ok && (entities[i].EntityType == id);
                                    i++;
                                }

                                return ok;
                            }

                        }
                    }

                    var typeObj = $(draggable).closest(".card-section-wrap");
                    var linktype = "";
                    if (typeObj[0]) {
                        linktype = typeObj[0].id;
                    }
                    if (linktype != id) {
                        return false;
                    }

                    //kolla om dragger finns.
                    if (drager == null || drager == "") {
                        return false;
                    }

                    var x = $(this).find("#" + drager);
                    if (x.length > 0) {
                        return false;
                    }
                    return true;
                },
                activeClass: "relation-section-container-active",
                hoverClass: "relation-dropable-hover",
                drop:
                    function (event, ui) {
                        self.addRelation(event, ui, self, id);
                    }
            });
        },
        createMessageOnNotDroppableEntities: function (section) {
            var self = this;
            section.droppable({
                drop: function () {
                    if (!self.isSorting) {
                        inRiverUtil.NotifyError("Entity drop is not allowed", "ChannelNode contains configure node rules. </br> Entity is not allowed to be dropped.");
                    }
                }
            });

        },
        renderBoxList: function () {

            var self = this;
            self.$el.find("#nodebox").html("");
            this.relations.models = _.sortBy(this.relations.models, function (model) {
                return model.attributes.Index;
            });

            _.each(self.relationTypes.models, function (model) {

                var entitytype = model.attributes["TargetEntityTypeId"];
                var sectionEl = "";

                if (entitytype != self.entityTypeId) {

                    sectionEl = $(_.template(nodeContentTypeGroupTemplate, { model: model.toJSON(), entitytypeid: entitytype, type: "box" }));

                    var type = model.attributes["Id"];
                    var count = 0;
                    var lastLinkEntityId = -1;
                    _.each(self.relations.models, function (relation) {

                        if (relation.attributes["LinkType"] == type) {

                            count++;
                            if (relation.attributes["LinkEntity"]) {

                                var linkentity = relation.attributes["LinkEntity"];
                                if (lastLinkEntityId != linkentity.Id) {
                                    lastLinkEntityId = linkentity.Id;

                                    var subsectionEl = $(_.template(nodeContentTypeLinkEntityGroupTemplate, { model: relation.toJSON(), entitytypeid: entitytype, type: "box" }));
                                    sectionEl.find("#relation-section-container").append(subsectionEl.html());
                                }

                                var relationCardSub = new nodeContentCardView({ relation: relation, direction: self.direction, size: "large", channelId: self.channelId });

                                sectionEl.find("#" + lastLinkEntityId + " #relation-sub-section-container").append(relationCardSub.$el);


                            } else {
                                var relationCard = new nodeContentCardView({ relation: relation, direction: self.direction, size: "large", channelId: self.channelId });


                                sectionEl.find("#relation-section-container").append(relationCard.$el);
                            }
                        }
                    });
                }

                var id = model.attributes["TargetEntityTypeId"];

                if (count == 0) {
                    sectionEl.find("#" + model.attributes["Id"]).addClass("droppable-empty");
                }


                self.$el.find("#nodebox").append(sectionEl);
                if (self.droppable) {
                    self.createDroppable(self.$el.find("#nodebox #" + model.attributes["Id"]), id);
                }
                else {
                    self.createMessageOnNotDroppableEntities(self.$el.find("#nodelist #" + model.attributes["Id"]));
                }
            });

            if (this.$el.find("#nodebox .relation-section-wrap:not(.droppable-empty)").length == 1) {
                this.$el.find("#nodebox .relation-section-wrap:not(.droppable-empty) .relation-section-title").addClass("droppable-empty");
            }

            this.onContentFiltering();
            this.boxListRendered = true;
        },
        renderPlainList: function () {

            var self = this;
            self.$el.find("#nodelist").html("");


            _.each(self.relationTypes.models, function (model) {

                var entitytype = model.attributes["TargetEntityTypeId"];
                var sectionEl = "";

                if (entitytype != self.entityTypeId) {

                    sectionEl = $(_.template(nodeContentTypeGroupTemplate, { model: model.toJSON(), entitytypeid: entitytype, type: "list" }));

                    var type = model.attributes["Id"];

                    var lastLinkEntityId = -1;
                    var count = 0;
                    _.each(self.relations.models, function (relation) {

                        if (relation.attributes["LinkType"] == type) {
                            count++;
                            if (relation.attributes["LinkEntity"]) {

                                var linkentity = relation.attributes["LinkEntity"];
                                if (lastLinkEntityId != linkentity.Id) {
                                    lastLinkEntityId = linkentity.Id;

                                    var subsectionEl = $(_.template(nodeContentTypeLinkEntityGroupTemplate, { model: relation.toJSON(), entitytypeid: entitytype, type: "list" }));
                                    sectionEl.find("#relation-section-container").append(subsectionEl.html());
                                }

                                var relationCardSub = new nodeContentCardView({ relation: relation, direction: self.direction, size: "small", channelId: self.channelId });

                                sectionEl.find("#" + lastLinkEntityId + " #relation-sub-section-container").append(relationCardSub.$el);

                            } else {
                                var relationCard = new nodeContentCardView({ relation: relation, direction: self.direction, size: "small", channelId: self.channelId });
                                sectionEl.find("#relation-section-container").append(relationCard.$el);
                            }
                        }

                    });
                }


                var id = model.attributes["TargetEntityTypeId"];

                if (count == 0) {
                    sectionEl.find("#" + model.attributes["Id"]).addClass("droppable-empty");
                }

                self.$el.find("#nodelist").append(sectionEl);

                if (self.droppable) {
                    self.createDroppable(self.$el.find("#nodelist #" + model.attributes["Id"]), id);
                } else {
                    self.createMessageOnNotDroppableEntities(self.$el.find("#nodelist #" + model.attributes["Id"]));
                }
            });

            this.makeSortable("#nodelist #relation-section-container", ".card-picture-area, .card-wrap");

            if (this.$el.find("#nodelist .relation-section-wrap:not(.droppable-empty)").length == 1) {
                this.$el.find("#nodelist .relation-section-wrap:not(.droppable-empty) .relation-section-title").addClass("droppable-empty");
            }

            this.onContentFiltering();
            this.plainListRendered = true;
        },
        makeSortable: function (selector, handle) {
            if (this.sortable) {
                var self = this;
                this.$el.find(selector).sortable({
                    connectWith: "#relation-section-container",
                    handle: handle,
                    placeholder: "card-placeholder",
                    update: self.onChangeSortOrder,
                    start: function (event, ui) {
                        self.isSorting = true;
                    },
                    stop: function (event, ui) {
                        self.isSorting = false;
                    }
                });
            }
        },
        renderTableList: function () {
            var self = this;
            var idList = [];
            this.cleanedRelations = this.relations;

            this.cleanedRelations.on('change:Index', this.renderTableList, this);

            this.cleanedRelations.models = _.filter(this.relations.models, function (relation) {
                return relation.attributes.TargetEntityTypeId != self.entityTypeId;
            });

            this.$el.find("#nodetable").empty();
            if (this.includeFields) {
                _.each(this.cleanedRelations.toJSON(), function (model) {
                    idList.push(model.TargetId);
                });

                var entityTable = new entityTableCollection();
                entityTable.fetch({
                    data: { ids: idList },
                    type: "POST",
                    success: function (models) {
                        var tableView = new entityTableView({ collection: models, includeFields: self.includeFields, placement: "relations", direction: "outbound", clickable: false });
                        self.$el.find("#nodetable").append(tableView.el);
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                        return;
                    }
                });
            } else {
                var entitytable = new entityTableView({ collection: this.cleanedRelations, includeFields: this.includeFields, placement: "relations", direction: "outbound", clickable: false });
                this.$el.find("#nodetable").append(entitytable.el);
            }

            this.$el.find("#include-fields").show();
            var icon = this.$el.find("#include-fields i");
            if (this.includeFields) {
                icon.removeClass("fa-square-o");
                icon.addClass("fa-check-square-o");
            } else {
                icon.removeClass("fa-check-square-o");
                icon.addClass("fa-square-o");
            }

            if (this.droppable) {
                this.$el.find("#nodetable").droppable({
                    greedy: true,
                    tolerance: "pointer",
                    accept: function (draggable) {
                        var drager = draggable[0].id;

                        if (drager == "") {
                            //kolla om det är från en tabell.

                            var gridDiv = $(draggable[0]).closest("#entity-table-container");
                            if (gridDiv.length > 0) {
                                var grid = gridDiv.dxDataGrid('instance');
                                var entities = grid.getSelectedRowKeys();
                                if (entities && entities.length > 0) {
                                    var ok = false;
                                    var i = 0;
                                    while (!ok && i < entities.length) {
                                        var x = $(this).find("#" + entities[i].id);
                                        ok = !x || x.length == 0;
                                        i++;
                                    }

                                    return ok;
                                }

                            }
                        }


                        var workarea = $(draggable[0]).closest("div#cards-container");
                        var cards = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");

                        if (cards && cards.length > 1) {
                            var ok = false;
                            var i = 0;
                            while (!ok && i < cards.length) {
                                var x = $(this).find("#" + cards[i].id.replace("card-", ""));
                                ok = !x || x.length == 0;
                                i++;
                            }

                            return ok;
                        }

                        if (drager == null || drager == "") {
                            return false;
                        }

                        //Check if drabbable object exists in target. If its not possible to addit again
                        var x = $(this).find("#" + drager);
                        if (x.length > 0) {
                            return false;
                        }
                        return true;
                    },
                    activeClass: "workarea-container-active",
                    hoverClass: "workarea-dropable-hover",
                    drop:
                        function (event, ui) {
                            self.addRelation(event, ui, self, null);
                        }
                });
            }


        },
        setContentList: function () {
            this.$el.find("#nodebox").hide();
            this.$el.find("#nodelist").hide();
            this.$el.find("#nodetable").hide();

            this.$el.find("#card-view").removeClass("content-active-view");
            this.$el.find("#sortable-view").removeClass("content-active-view");
            this.$el.find("#table-view").removeClass("content-active-view");
            this.$el.find("#include-fields").hide();

            if (this.listMode == "box") {
                this.renderBoxList();
                this.$el.find("#card-view").addClass("content-active-view");
                this.$el.find("#nodebox").show();
                this.size = "large";
            }

            if (this.listMode == "list") {
                this.renderPlainList();
                this.$el.find("#sortable-view").addClass("content-active-view");
                this.$el.find("#nodelist").show();
                this.size = "small";
            }

            if (this.listMode == "table") {
                this.renderTableList();
                this.$el.find("#table-view").addClass("content-active-view");
                this.$el.find("#nodetable").show();
                this.size = "table";
            }
        },
        render: function () {
            var that = this;

            if (this.relations == null) {
                return this;
            }

            this.$el.html(_.template(nodeContentHeaderTemplate, { loadContent: this.loadContent }));

            var icon = this.$el.find("#load-content-button i");
            if (this.loadContent) {
                icon.removeClass("fa-square-o");
                icon.addClass("fa-check-square-o");
            } else {
                icon.removeClass("fa-check-square-o");
                icon.addClass("fa-square-o");
            }

            if (this.loadContent == false) {
                return this;
            }

            // Find out which list mode to render
            this.setContentList();

            var ids = this.$el.find(".relation-section-wrap");

            for (var index = 0; index < ids.length; ++index) {
                showLinkTypes('#' + ids[index].id);
            }

            function showLinkTypes(id) {
                id = id.replace(" ", "-");
                var $heading = $(id + " .relation-section-title");
                var $box = $(id).find("#relation-section-container");

                var $showTask = $(id + '-show i');

                $heading.click(function (e) {
                    if ($(e.target).hasClass("add-entity-button")) {
                        return;
                    }
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            ids = $(".relation-sub-section-wrap");

            for (index = 0; index < ids.length; ++index) {
                showSubLinkTypes('#' + ids[index].id);
            }

            function showSubLinkTypes(id) {
                id = id.replace(" ", "-");
                var $heading = $(id + " .relation-sub-section-title");
                var $box = $(id).find("#relation-sub-section-container");
                var $showTask = $(id + '-show i');

                $heading.click(function (e) {
                    if ($(e.target).hasClass("add-entity-button")) {
                        return;
                    }
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            this.makeSortable(".relation-section-container", ".relation-picture-area, .relation-wrap");

            $("#nodelist").height($("#node-area").height() - 135);
            $("#nodebox").height($("#node-area").height() - 135);
            $("#nodetable").height($("#node-area").height() - 135);
            return this;
        }
    });


    return nodeContentView;
});
