define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/workarea/workareaModel'
], function ($, _, Backbone, entityModel) {
    var workareaCollection = Backbone.Collection.extend({
        model: entityModel,
        url: '/api/workarea'
    });

    return workareaCollection;
});
