﻿define([
  'jquery',
  'underscore',
  'backbone',
  'collections/channel/channelEntityCollection',
  'collections/relations/relationCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'modalPopup',
  'sharedjs/views/panelsplitter/panelBorders',
  'sharedjs/views/entityview/entityDetailView',
  'views/nodeview/nodeContentView',
  'sharedjs/views/configureLinkRule/configureLinkRuleView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/entityview/entitySystemInformationView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'text!templates/nodeview/nodeTemplate.html'

], function ($, _, backbone, channelEntityCollection, relationCollection, relationTypeCollection, entityModel, completenessModel, relationModel, modalPopup, panelBorders, entityDetailView, nodeContentView, configureLinkRuleView, entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, contextMenuView, entitySystemInformationView, inRiverUtil, permissionUtil, entityTemplate) {

    var nodeView = backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            var that = this;
            this.channelId = options.channelId;
            //Ladda om entitetet, för vi vet ju för lite!
            var onDataHandler = function () {
                that.relations = new relationCollection([], { entityId: that.model.id, direction: "outbound", entitytypeid: that.model.attributes.EntityType });

                that.relations.fetch(
                {
                    success: function () {
                        that.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            };

            if (this.model) {
                this.model.fetch({ success: onDataHandler });
            } else {
                var id = options.id;
                this.model = new entityModel(id);
                this.model.url = "/api/entity/" + id;
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        events: {
            "click #card-completeness-text": "onShowCompletnessDetails",
            "click #progress-rect-percentage": "onShowCompletnessDetails",
            "click #tab-node-content": "onChangeTopTab",
            "click #tab-node-details": "onChangeTopTab",
            "click #tab-node-configure": "onChangeTopTab",
            "click #menu-node-show-info-button": "onShowSystemInfo",
            "click #edit-node-button": "onEditDetail",
            "click #menu-node-lock-button": "onEntityLock",
            "click #menu-node-unlock-button": "onEntityUnlock",
            "click #star-node-button": "onEntityStar",
            "click #menu-node-delete-button": "onEntityDelete",
            "click #menu-node-add-task-button": "onAddTask",
            "click #menu-node-open-in-enrich-button": "onEnrichOpenClick",
            "click #showsubnodes": "onShowSubnodes"
        },
        onShowSubnodes: function() {
            this.$el.find(".subnodes").toggle();
            this.$el.find(".subnodes").position({
                of: $("#showsubnodes"),
                my: "left center",
                at: "left center+20",
                collision: "flip"
            });


        },

        onPublishClick: function(e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").hide();
                    $("#unpublish-button").show();
                    
                }
            }


            xmlhttp.open("GET", "/api/channelaction/publish/" + this.model.id, true);
            xmlhttp.send();
            

        },
        onUnpublishClick: function (e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").show();
                    $("#unpublish-button").hide();

                }
            }

            xmlhttp.open("GET", "/api/channelaction/unpublish/" + this.model.id, true);
            xmlhttp.send();
        },
        onAddTask: function (e) {
            e.stopPropagation();

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });

        },
        taskAddedNowLinkEntity: function (model) {
            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function () {
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityClose: function () {
            appHelper.event_bus.trigger('entityareaclose');
            this.remove();
            this.unbind();
        },
        onEntityDelete: function () {
            inRiverUtil.deleteEntity(this);
        },
        onEditDetail: function () {
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onEntityLock: function () {
            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    that.entityUpdated(that.model.id); 
                }
            }
            xmlhttp.open("GET", "/api/tools/lockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityUnlock: function () {
            var that = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    that.entityUpdated(that.model.id);
                }
            };

            xmlhttp.open("GET", "/api/tools/unlockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();

            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");

                e.currentTarget.title = "Star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star");
                $(e.currentTarget).addClass("icon-entypo-fix-star-empty");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");

                e.currentTarget.title = "Un-star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star-empty");
                $(e.currentTarget).addClass("icon-entypo-fix-star");
            }
        },
        onShowSystemInfo: function () {
            inRiverUtil.ShowEntityInfo(this.model.id);
        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },
        createDetailsTabView: function () {
            var contentSubView = new entityDetailSettingWrapperView({
                model: this.model
            });
            return contentSubView;
        },
        createContentTabView: function () {
            var contentSubView = new nodeContentView
            ({
                model: this.model,
                channelId: this.channelId
            });
            return contentSubView;
        },
        createConfigureTabView: function () {
            var configureSubView = new configureLinkRuleView
                ({
                    model: this.model
                });
            return configureSubView;
        },
        onChangeTopTab: function (e) {
            if (appSession.nodeViewTabId == e.currentTarget.id) {
                return;
            }

            appSession.nodeViewTabId = e.currentTarget.id; // remember default tab
            this.createTopTabView(appSession.nodeViewTabId);
            this.$el.find("#node-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
        },
        
        createTopTabView: function (idOfTab) {
            if (this._currentTopTabView) {
                this._currentTopTabView.stopListening();
                this._currentTopTabView.remove();
            }
            if (idOfTab == "tab-node-details") {
                this._currentTopTabView = this.createDetailsTabView();
            }
            else if (idOfTab == "tab-node-content") {
                this._currentTopTabView = this.createContentTabView();
            }
            else if (idOfTab == "tab-node-configure") {
                this._currentTopTabView = this.createConfigureTabView();
            }
            else
            {
                this._currentTopTabView = this.createContentTabView();
                appSession.nodeViewTabId = "tab-node-content";
            }
        },
       removeNode: function() {
           this.stopListening();
           if (this._currentTopTabView != null) {
               this._currentTopTabView.stopListening();
               this._currentTopTabView.remove();
               this._currentTopTabView.unbind();
           }
           this.remove();
           this.unbind();
       },
        indicateActiveTopTab: function () {
            this.$el.find("#tab-node-details").removeClass("node-tab-button-active");
            this.$el.find("#tab-node-content").removeClass("node-tab-button-active");
            this.$el.find("#tab-node-configure").removeClass("node-tab-button-active");
            this.$el.find("#" + appSession.nodeViewTabId).addClass("node-tab-button-active");
        },
        
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        
        entityUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch({
                    success: function () {
                        self.rerender = true;
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

                window.appHelper.event_bus.trigger("nodeentityupdated"); 
            }
        },
        drawCompleteness: function (percent) {
            this.$el.find('.percent').html(Math.round(percent) + '%');

            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        render: function () {
            var self = this;
            console.log("Render Channel/publication View");

            this.$el.html(_.template(entityTemplate, { data: this.model.toJSON(), userName: appHelper.userName }));

            this.createTopTabView(appSession.nodeViewTabId);
         
            this.$el.find("#node-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
         
            this.stopListening(window.appHelper.event_bus);
            window.appHelper.event_bus.off('entityupdated');
            this.listenTo(appHelper.event_bus, 'entityupdated', this.entityUpdated);


           if (appHelper.hasTask != undefined) {

                if (!appHelper.TaskRelationTypes) {
                    this.$el.find("#add-task-button").hide();
                }

            } else {
                this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: "Task" });

                this.relationTypes.fetch({
                    success: function () {

                        self.TaskRelationType = _.find(self.relationTypes.models, function (model) {
                            return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                        });
                        if (!self.TaskRelationType) {
                            self.$el.find("#menu-node-add-task-button").hide();
                        }
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }


            this.tools = new Array();

            this.tools.push({ name: "menu-node-delete-button", title: "Delete channel", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });

            if (this.model.attributes.LockedBy == null || this.model.attributes.LockedBy == "") {
                this.tools.push({ name: "menu-node-lock-button", title: "Lock entity", icon: "fa fa-lock", text: "Lock", permission: "LockEntity" });
            } else {
                this.tools.push({ name: "menu-node-unlock-button", title: "Unlock entity", icon: "fa fa-unlock", text: "Unlock", permission: "LockEntity" });
            }

            var callback = function (access, id, self) {
                if (access) {
                    self.tools.push({ name: "menu-node-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                }
            };

            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);

            this.tools.push({ name: "menu-node-add-task-button", title: "Create Task", icon: "icon-entypo-fix-clipboard std-entypo-fix-icon", text: "Create Task", permission: "AddEntity" });

            this.tools.push({ name: "menu-node-show-info-button", title: "Show entity information", icon: "icon-entypo-fix-info-circled std-entypo-fix-icon", text: "System Info" });

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: this.tools }).$el);
        
            _.each(self.relations.models, function(subnode) {
                var row = "<a href=\"#node/"+ subnode.attributes.TargetId +  "\">" + subnode.attributes.TargetName + "</a><br/>";
                self.$el.find(".subnodes").append(row); 
            }); 

            this.drawCompleteness(this.model.attributes.Completeness);

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#edit-node-button"));
            permissionUtil.CheckPermissionForElement("ManageLinkRules", this.$el.find("#tab-node-configure"));
        
            return this; // enable chained calls
        }
    });

    return nodeView;
});