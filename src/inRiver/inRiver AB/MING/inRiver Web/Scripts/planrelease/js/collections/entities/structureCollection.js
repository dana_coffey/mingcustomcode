define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, backbone) {
    var structureCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.channelId = options.id;
        },
        url: function () {
            return '/api/structure/' + this.channelId;
        }
    });

    return structureCollection;
});
