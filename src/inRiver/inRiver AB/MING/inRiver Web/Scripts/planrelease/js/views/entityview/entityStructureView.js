﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'jstree',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
   'sharedjs/misc/permissionUtil',
  'collections/entities/structureCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/relation/relationModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/contextmenu/contextMenuView',
  'models/entity/entityModel',
  'views/nodeview/nodeView',
  'text!templates/entityview/entityStructureTemplate.html'

], function ($, _, backbone, jqueryui, jstree, modalPopup, inRiverUtil, permissionUtil, structureCollection, relationTypeCollection, relationModel, entityDetailSettingView, entityDetailSettingWrapperView, contextMenuView, entityModel, nodeView, entityStructureTemplate) {

    var entityStructureView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.loaded = false;
            
            this.entityModel = options.model;
            this.entityType = options.model.attributes.EntityType;

            if (this.entityType == "Channel" && !options.model.attributes.ChannelPublished) {
                this.entityType += "Unpublished";
            }
            // load all fields.
            this.model = new structureCollection({ id: this.entityModel.get("Id") });
            this.model.fetch(
            {
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #open": "onOpenNode",
            "click #add": "onAddNode",
            "click #addroot": "onAddRoot",
            "click #remove": "onRemoveNode",
            "click #viewcontent": "onViewContent",
            "click #expand": "expandAll",
            "click #collapse": "collapseAll",
        },
        expandAll: function () {
            $("#structure-row-container").jstree(true).open_all();
        },
        collapseAll: function () {
            $("#structure-row-container").jstree(true).close_all();
        },
        onViewContent: function (e) {
            //this.contextmenu.onHideTools(e);
            var self = this;

            if (self.nodeView) {
                self.nodeView.removeNode();
            }

            var ref = $("#structure-row-container").jstree(true),
                          sel = ref.get_selected();
            if (!sel.length) { return; }
            sel = sel[0];

            sel = sel.substring(sel.lastIndexOf("/") + 1);

            var model = new entityModel({ id: sel });
            model.fetch({
                url: "/api/entity/" + sel,
                success: function () {
                    model.id = model.attributes.id;
                    self.nodeView = new nodeView
                    ({
                        model: model,
                        channelId: self.model.channelId
                });

                    self.$el.find(".contentarea").html(self.nodeView.$el);
                    self.$el.find(".contentarea").show();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onRemoveNodeConfirmed: function (self) {
            var ref = $("#structure-row-container").jstree(true),
                sel = ref.get_selected(true);
            if (!sel.length) {
                return false;
            }
            sel = sel[0];

            self.relation = new relationModel({ id: -1 });

            var parent = sel.parent;

            if (parent == "#") {
                parent = self.entityModel.attributes.Id;
            } else {
                parent = parent.substring(parent.lastIndexOf("/") + 1);
            }

            var id = sel.id;
            id = id.substring(id.lastIndexOf("/") + 1);

            self.relation.fetch({
                url: "/api/relation/" + parent + "/" + id,
                success: function(model) {
                    model.destroy(
                    {
                        success: function() {
                            inRiverUtil.Notify("Relation successfully removed");
                        },
                        error: function(model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            ref.delete_node(sel);
            self.$el.find(".contentarea").hide();

            return true;
        },
        onRemoveNode: function (e) {

            var ref = $("#structure-row-container").jstree(true),
                      sel = ref.get_selected(true);

            if (!sel.length) {
                inRiverUtil.Notify("You must selected a Channel or Channel node first from the list below.");
                return false;
            }

            sel = sel[0];
            
            var self = this;
            inRiverUtil.NotifyConfirm("Confirm remove " + sel.text + " from " + this.entityModel.attributes.DisplayName, "Are you sure you want to remove this relation?", this.onRemoveNodeConfirmed, self);
        },
        onNodeRemoved: function (id) {
            var ref = $("#structure-row-container").jstree(true),
            sel = ref.get_selected(true);

            sel = sel[0];

            ref.delete_node(sel);

        },
        onAddNode: function (e) {
            var ref = $("#structure-row-container").jstree(true),
                sel = ref.get_selected(true);

            if (!sel.length) {
                inRiverUtil.Notify("You must selected a Channel or Channel node first from the list below.");
                return false;
            }
            sel = sel[0];

            this.ref = ref;

            var id = sel.id;
            id = id.substring(id.lastIndexOf("/") + 1);
            this.sel = id;
            this.path = sel.id;

            this.stopListening(appHelper.event_bus);

            if (sel.type == "channel" || sel.type == "publication") {
                this.listenTo(appHelper.event_bus, 'entitycreated', this.onAddRootComplete);
            } else {
                this.listenTo(appHelper.event_bus, 'entitycreated', this.onAddNodeComplete);
            }

            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.onNodeRemoved);
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: this.sibling,
                showInWorkareaOnSave: false
            });



        },
        onAddNodeComplete: function (model) {
            window.appHelper.event_bus.off('entitycreated');
           
            var targetid = this.sel;
            this.sel = this.ref.create_node(this.path, { "type": "node", "text": model.attributes.DisplayName, id: this.path + "/" + model.attributes.Id });
            var self = this;

            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.relationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: targetid, TargetId: this.targetIdToCreate, LinkEntityId: null });
            newModel.save([], {
                success: function (model) {
                    inRiverUtil.Notify("Relation successfully created");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        onAddRootComplete: function (model) {
            window.appHelper.event_bus.off('entitycreated');

            var self = this;
            this.sel = this.ref.create_node(this.sel, { "type": "node", "text": model.attributes.DisplayName, id: model.attributes.Id });

            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.toprelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: this.entityModel.attributes.Id, TargetId: this.targetIdToCreate, LinkEntityId: null });
            newModel.save([], {
                success: function (model) {
                    var i = 0;
                    if (self.ref == false) {
                        self.model.fetch(
                        {
                            success: function () {
                                self.render();
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        onClose: function () {
        },
        onNodeEntityUpdated: function() {
            var self = this;

            self.model.fetch(
                      {
                          success: function () {
                              self.render();
                          },
                          error: function (model, response) {
                              inRiverUtil.OnErrors(model, response);
                          }
                      });
        },
        render: function () {

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.onNodeRemoved);
            this.listenTo(window.appHelper.event_bus, 'nodeentityupdated', this.onNodeEntityUpdated);

            var self = this;

            if (this.entityModel.attributes.EntityType == "Channel") {
                this.sibling = "ChannelNode";
            } else {

                this.sibling = "Section";
            }

            this.$el.html(_.template(entityStructureTemplate, {}));
            $('#remove').hide();
            if (self.model.length == 0) {
                $("#structure-row-container").html("No nodes");
            } else {
                $("#structure-row-container").html("");
                $("#structure-row-container").jstree({
                    core: {
                        data: self.model.toJSON(),
                        animation: "100",
                        themes: { dots: false },
                        check_callback: function (operation, node, node_parent, node_position, more) {
                            switch (operation) {
                                case "create_node":
                                case "rename_node":
                                // in case of 'rename_node' node_position is filled with the new node name
                                case "delete_node":
                                case "move_node":
                                    return true;
                                case "copy_node":
                                default:
                                    return false;
                            }
                        }
                    },
                    plugins: [
                        "types", "wholerow", "dnd", "search", "state"
                    ],
                    types: {
                        "#": {
                            max_children: 1,
                            max_depth: 10,
                            valid_children: ["channel", "publication", "node"]
                        },
                        channel: {
                            icon: "/Icon?id=" + self.entityType,
                            valid_children: ["node"]
                        },
                        publication: {
                            icon: "/Icon?id=Publication",
                            valid_children: ["node"]
                        },
                        node: {
                            icon: "/Icon?id=" + self.sibling,
                            valid_children: ["node"]
                        }
                    },
                    search: {
                        show_only_matches: true
                    },
                    dnd: {
                        copy: false
                    }
                });

                var that = this;
                $("#structure-row-container").on("ready.jstree", function () {
                    that.loaded = true;

                });

                $("#structure-row-container").on('changed.jstree', function (e, data) {
                    var i, j, r = [];
                    for (i = 0, j = data.selected.length; i < j; i++) {
                        r.push(data.instance.get_node(data.selected[i]).text);
                    }
                    $('#event_result').html('Selected: ' + r.join(', '));

                    var selected = $("#structure-row-container").jstree(true).get_selected(true);

                    if (selected.length === 0 || selected[0].type === "channel")
                        $("#remove").hide();
                    else {
                        $("#remove").show();
                    }
                });
                var to = false;
                $('#treefilter').keyup(function () {
                    if (to) { clearTimeout(to); }
                    to = setTimeout(function () {
                        var v = $('#treefilter').val();
                        $("#structure-row-container").jstree(true).search(v);
                    }, 250);
                });

                $("#structure-row-container").on('select_node.jstree', function (node, selected, event) {

                    if (selected.node == undefined || selected.node.type == "channel" || selected.node.type == "publication") {
                        self.$el.find(".contentarea").hide();
                        $("#remove").hide();
                        return;
                    }

                    $("#remove").show();
                    node.stopPropagation();
                    if (that.loaded) {
                        that.onViewContent(node);
                    }
                });

                $("#structure-row-container").on('move_node.jstree', function (node, data) {

                    var currentNode = data.node.id;
                    var currentId = currentNode.substring(currentNode.lastIndexOf("/") + 1);
                    var newParent = data.parent;
                    var newParentId = newParent.substring(newParent.lastIndexOf("/") + 1);
                    var oldparent = data.old_parent;
                    var oldParentId = oldparent.substring(oldparent.lastIndexOf("/") + 1);

                    if (newParentId == oldParentId) {
                        //update sortorder
                        var relation = new relationModel({ id: -1 });


                        relation.fetch({
                            url: "/api/relation/" + oldParentId + "/" + currentId,
                            success: function(model) {

                                model.attributes.Index = data.position;
                                model.save([], {
                                    success: function(model) {
                                        //alert("ok");
                                    }
                                });
                            }
                        });
                    } else {

                        var toprelation = that.toprelationType;
                        if (newParentId != "#") {
                            var relation = new relationModel({ id: -1 });


                            relation.fetch({
                                url: "/api/relation/" + oldParentId + "/" + currentId,
                                success: function(model) {

                                    var newRelation = model.clone();

                                    newRelation.attributes.SourceId = newParentId;
                                    newRelation.id = 0;
                                    newRelation.attributes.Id = 0;

                                    if (newParentId == self.model.channelId) {
                                        newRelation.attributes.LinkType = toprelation.attributes.Id;
                                    }

                                    if (oldParentId == self.model.channelId) {
                                        newRelation.attributes.LinkType = that.relationType.attributes.Id;
                                    }
                                    newRelation.save(null, {
                                        type: "Post",
                                        success: function () {
                                            model.destroy();
                                        },
                                        error: function(model, response) {
                                            inRiverUtil.OnErrors(model, response);
                                        }
                                    });
                                },
                                error: function(model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        } else {
                            var relation = new relationModel({ id: -1 });


                            relation.fetch({
                                url: "/api/relation/" + oldparent + "/" + currentNode,
                                success: function(model) {
                                    var newRelation = model.clone();
                                    newRelation.attributes.SourceId = that.entityModel.id;
                                    
                                    newRelation.attributes.LinkType = toprelation.attributes.Id;
                                    newRelation.id = 0;
                                    newRelation.attributes.id = 0;

                                    newRelation.save(null, {
                                        type: "Post",
                                        url: "/api/relation",
                                        success: function() {
                                            model.destroy();
                                        },
                                        error: function(model, response) {
                                            inRiverUtil.OnErrors(model, response);
                                        }
                                    });
                                },
                                error: function(model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        }
                    }


                });



            }

            if (!this.relationTypes) {
                this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: self.entityModel.attributes.EntityType });

                this.relationTypes.fetch({
                    success: function () {

                        self.toprelationType = _.find(self.relationTypes.models, function (model) {
                            return model.attributes.SourceEntityTypeId == self.entityModel.attributes.EntityType && model.attributes.TargetEntityTypeId == self.sibling;

                        });

                        self.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: self.toprelationType.attributes.TargetEntityTypeId });

                        self.relationTypes.fetch({
                            success: function () {

                                self.relationType = _.find(self.relationTypes.models, function (model) {
                                    return model.attributes.TargetEntityTypeId == self.toprelationType.attributes.TargetEntityTypeId &&
                                        model.attributes.SourceEntityTypeId == self.toprelationType.attributes.TargetEntityTypeId;

                                });
                                if (!self.relationType) {
                                    self.$el.find(".add").hide();
                                }
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

            }

            appHelper.lstSourceid = 0;
            $(document)
          
         .on('dnd_stop.vakata', function (e, data) {
             e.stopPropagation();
             var t = $(data.event.target);

             if (t.closest('.jstree').length) {

                 if (t.closest("li").length) {
                     var treesourceId = t.closest("li")[0].id;
                     var sourceId = treesourceId.split(/[/]+/).pop();

                     var sourceSrc = t.find("i").css("background-image");
                     var sourceType = sourceSrc.substr(sourceSrc.indexOf("?id=") + 4).replace("\"", "").replace(")", "").replace("Unpublished", "");


                     if (data.helper.find(".multiples").length > 0) {
                         var waHandler = appHelper.appRouter.workAreaHandler;
                         var active = waHandler.getActiveWorkarea();
                         var entities = active.getSelectedEntityIds();

                         var cards = active.cards;

                         _.each(entities, function(entity) {

                             var dragged = _.find(cards, function(card) {
                                 return card.card.model.attributes.Id == entity;
                             });

                             var targetType = dragged.card.model.attributes.EntityType;

                             var relationType = _.find(self.relationTypes.models, function (model) {
                                 return model.attributes.TargetEntityTypeId == targetType &&
                                    model.attributes.SourceEntityTypeId == sourceType;

                             });
                             if (!relationType) {
                                 if (self.toprelationType.attributes.TargetEntityTypeId == targetType &&
                                     self.toprelationType.attributes.SourceEntityTypeId == sourceType) {
                                     relationType = self.toprelationType;
                                 }
                             }


                             if (relationType) {
                                 //lägg till i db.
                                 var newModel = new relationModel({ LinkType: relationType.attributes.Id, SourceId: sourceId, TargetId: entity, LinkEntityId: null });
                                 newModel.save([], {
                                     async: false,
                                     success: function (model) {
                                         var i = 0;

                                         //visuell feedback.
                                         if (targetType == sourceType) {
                                             var ref = $("#structure-row-container").jstree(true);
                                             ref.deselect_all();

                                             ref.select_node(treesourceId);
                                             sel = ref.get_selected(true);

                                             if (!sel.length) {
                                                 return false;
                                             }
                                             sel = sel[0];

                                             this.path = sel.id;

                                             sel = ref.create_node(this.path, { "type": "node", "text": model.attributes.SourceName, id: this.path + "/" + model.attributes.TargetId });
                                             self.onNodeEntityUpdated();
                                             
                                         } else {
                                             var ref = $("#structure-row-container").jstree(true);
                                             ref.deselect_all();
                                             ref.select_node(treesourceId);
                                             var sel = ref.get_selected();
                                             if (!sel.length) {
                                                 return false;
                                             }
                                             sel = sel[0];

                                             this.path = sel.id;

                                             sel = ref.create_node(this.path, { "type": "node", "text": model.attributes.SourceName, id: this.path + "/" + model.attributes.TargetId });

                                             self.onNodeEntityUpdated();
                                         }
                                     },
                                     error: function (model, response) {
                                         inRiverUtil.OnErrors(model, response);
                                     }
                                 });
                             } else {
                                 var ref = $("#structure-row-container").jstree(true);
                                 ref.deselect_all();
                                 ref.select_node(sourceId);
                                 var sel = ref.get_selected();

                                 $("#dropresult").show();
                                 $("#dropresult").text("Not possible to drop this type of entity here");

                                 $("#dropresult").delay(2500).fadeOut(1600).text("");
                             }

                         } ); 

                         //gör en sak.
                         return; 
                     }

                     var targetId = $(data.element)[0].id.replace("card-", "");
                     if (appHelper.lstSourceid == targetId) {
                         return;
                     }
                     appHelper.lstSourceid = targetId;

                     var imgSrc = $(data.element).find("img")[0].src;
                     var targetType = imgSrc.substr(imgSrc.indexOf("?id=") + 4).replace("\"", "");

                     var relationType = _.find(self.relationTypes.models, function (model) {
                         return model.attributes.TargetEntityTypeId == targetType &&
                            model.attributes.SourceEntityTypeId == sourceType;

                     });
                     if (!relationType) {
                         if (self.toprelationType.attributes.TargetEntityTypeId == targetType &&
                             self.toprelationType.attributes.SourceEntityTypeId == sourceType) {
                             relationType = self.toprelationType;
                         }
                     }


                     if (relationType) {
                         //lägg till i db.
                         var newModel = new relationModel({ LinkType: relationType.attributes.Id, SourceId: sourceId, TargetId: targetId, LinkEntityId: null });
                         newModel.save([], {
                             success: function (model) {
                                 var i = 0;

                                 //visuell feedback.
                                 if (targetType == sourceType) {
                                     var ref = $("#structure-row-container").jstree(true);
                                     ref.deselect_all();

                                     ref.select_node(treesourceId);
                                     sel = ref.get_selected(true);

                                     if (!sel.length) {
                                         return false;
                                     }
                                     sel = sel[0];

                                     this.path = sel.id;

                                     sel = ref.create_node(this.path, { "type": "node", "text": model.attributes.SourceName, id: this.path + "/" + model.attributes.TargetId });
                                     self.onNodeEntityUpdated();

                                 } else {
                                     var ref = $("#structure-row-container").jstree(true);
                                     ref.deselect_all();
                                     ref.select_node(treesourceId);
                                     var sel = ref.get_selected();
                                     if (!sel.length) {
                                         return false;
                                     }
                                     sel = sel[0];

                                     this.path = sel.id;

                                     sel = ref.create_node(this.path, { "type": "node", "text": model.attributes.SourceName, id: this.path + "/" + model.attributes.TargetId });
                                     self.onNodeEntityUpdated();

                                 }
                             },
                             error: function (model, response) {
                                 inRiverUtil.OnErrors(model, response);
                             }
                         });
                     } else {
                         var ref = $("#structure-row-container").jstree(true);
                         ref.deselect_all();
                         ref.select_node(sourceId);
                         var sel = ref.get_selected();

                         $("#dropresult").show();
                         $("#dropresult").text("Not possible to drop this type of entity here");

                         $("#dropresult").delay(2500).fadeOut(1600).text("");
                     }


                     var i = 0;

                 }
             }
         });

            permissionUtil.CheckPermissionForElement("AddLink", this.$el.find("#add"));
            permissionUtil.CheckPermissionForElement("DeleteLink", this.$el.find("#remove"));


            return this; // enable chained calls
        }
    });

    return entityStructureView;
});
