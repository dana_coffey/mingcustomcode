﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var connectorEventModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/connectorevent',
        
    });

    return connectorEventModel;

});
