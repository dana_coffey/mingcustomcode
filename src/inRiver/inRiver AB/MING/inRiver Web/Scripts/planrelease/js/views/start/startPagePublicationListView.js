define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',
  'text!templates/start/startPagePublicationListTemplate.html',
  'views/publications/publicationEntityView',
  'collections/entities/publicationCollection'
], function ($, _, backbone, jqueryui, inRiverUtil, startPagePublicationListTemplate, publicationEntityView, publicationCollection) {

    var startPagePublicationList = backbone.View.extend({
        initialize: function () {

            this.$el.html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            this.undelegateEvents();
            this.updatePublicationList();
        },
        entityCreated: function (model) {
            if (model.get("EntityType") == "Publication") {
                this.updatePublicationList();
            }
        },
        updatePublicationList: function () {
            var self = this;
            this.list = new publicationCollection();
            this.list.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ entityType: "Publication", sortStarredDirection: "DESC" })
            });
        },
        render: function () {
            var thisPage = this.$el.html(startPagePublicationListTemplate);
            if (this.list.models.length == 0) {
                this.$el.find("#publications-list").html("<div class='no-items-text'>There are no publications</div>");
            } else {
                _.each(this.list.models, function (model) {
                    var entityView = new publicationEntityView({ model: model });
                    thisPage.find('#publications-list').append(entityView.render().el);
                });
            }

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.entityCreated);
            this.listenTo(window.appHelper.event_bus, 'publicationstared', this.updatePublicationList);


            return this;
        }
    });

    return startPagePublicationList;
});

