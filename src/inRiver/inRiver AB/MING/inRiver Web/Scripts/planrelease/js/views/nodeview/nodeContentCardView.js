﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/templateprinting/TemplatePrintingSetupView',
  'text!templates/nodeview/nodeContentTemplate.html',
  'sharedjs/misc/permissionUtil'
], function ($, _, Backbone, alertify, modalPopup, inRiverUtil, completenessModel, contextMenuView,
    entityDetailSettingView, entityDetailSettingWrapperView, templatePrintingSetupView, relationTemplate, permissionUtil) {

    var entitycardView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.relation = options.relation;
            this.direction = options.direction;
            this.size = options.size;
            this.channelId = options.channelId;
            if (this.size == null || this.size == "") {
                this.size = "large"; 
            }
            this.render();
        },
        events: {
            "click #relation-remove": "onRelationRemove",
            "click #relation-inactivate": "onRelationActivate",
            "click #relation-activate": "onRelationInactivate",
            "click .completeness-small": "onShowCompletnessDetails",
            "click #star-button": "onEntityStar",
            "click #menu-open-in-enrich-button": "onEnrichOpenClick",
            "click #menu-edit-button": "onEditDetail",
            "click #preview-button": "onShowPreview",
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            var id = -1; 
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
            }
            window.location.href = "/app/enrich/index#entity/" + id;
        },
        onEditDetail: function () {
            var modal = new modalPopup();
            var id = -1;
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
            }
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: id
            });


        },
        onRelationActivate: function (e) {

            e.stopPropagation();
            this.onToggleTools(e);

            var modelToActivate = this.relation;

            modelToActivate.set("Active", "active");
            modelToActivate.save();
            this.render();
        },
        onRelationInactivate: function (e) {

            e.stopPropagation();
            this.onToggleTools(e);

            var modelToInactivate = this.relation;

            modelToInactivate.set("Active", "inactive");
            modelToInactivate.save();
            this.render();
        },
        onRelationRemove: function (e) {

            e.stopPropagation();
            this.onToggleTools(e);

            var callback = function (self) {
                var modelToDelete = self.relation;

                modelToDelete.destroy({
                    success: function () {

                        self.stopListening(appHelper.event_bus);
                        self.remove();
                        self.unbind();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            inRiverUtil.NotifyConfirm("Confirm", "Do you want to remove this relation?", callback, this);

        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();

            var model = this.relation;
            
            if (this.direction == "outbound") {
                model.id = this.relation.attributes.TargetEntity.Id;
            } else {
                model.id = this.relation.attributes.SourceEntity.Id;
            }
            inRiverUtil.ShowInlineCompleteness(model.id, $(e.currentTarget));
        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        onEntityStar: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            var id = -1;

            var starred = -1; 
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
                starred = this.relation.attributes.TargetEntity.Starred; 
            } else {
                id = this.relation.attributes.SourceEntity.Id;
                starred = this.relation.attributes.SourceEntity.Starred;
            }
            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    that.entityUpdated(id);
                }
            }

            if (starred == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + id, true);
                xmlhttp.send();

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + id, true);
                xmlhttp.send();
            }

        },
        onShowPreview: function (e) {
            e.stopPropagation();
            var that = this;
            var entityId = this.relation.get("TargetEntity").Id;
            var entityTypeId = this.relation.get("TargetEntityTypeId");
            var availableTemplates = _.filter(appData.planReleasePreviewTemplates, function(t) {
                return t.Properties &&
                    t.Properties.EntityTypes &&
                    t.Properties.Channels &&
                    _.contains(t.Properties.EntityTypes, entityTypeId) &&
                    _.contains(t.Properties.Channels, that.channelId);
            });
            var modal = new modalPopup();
            modal.popOut(new templatePrintingSetupView({ entityId: entityId, entityDisplayName: this.relation.get("TargetEntity").DisplayName, mode: "preview", templates: availableTemplates }), { size: "small", header: "Preview", usesavebutton: false });
        },
        entityUpdated: function (id) {
            var thisId = -1;
            if (this.direction == "outbound") {
                thisId = this.relation.attributes.TargetEntity.Id;
               } else {
                thisId = this.relation.attributes.SourceEntity.Id;
            }
            if (id == thisId) {

                var self = this;
                this.relation.url = "/api/relation/" + this.relation.attributes.SourceEntity.Id + "/" + this.relation.attributes.TargetEntity.Id + "/" + this.channelId;

                this.relation.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        render: function () {
            var that = this;

            this.$el.html($(_.template(relationTemplate, { model: this.relation.toJSON(), direction: this.direction, active: this.relation.attributes.IncludedInChannel ? this.relation.attributes["Active"] : "inactive", size: this.size })));

            var lockedBy;

            if (this.direction == "outbound") {
                lockedBy = this.relation.attributes.TargetEntity.LockedBy;
            } else {
                lockedBy = this.relation.attributes.SourceEntity.LockedBy;
            }

            var isLocked = inRiverUtil.isLocked(lockedBy);

            this.tools = new Array();

            if (!isLocked) {
                this.tools.push({ name: "menu-edit-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            if ((this.direction == "outbound" && this.relation.attributes.TargetEntity.Starred == "1") ||
                (this.direction == "inbound" && this.relation.attributes.SourceEntity.Starred == "1")) {
                this.tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });
            } else {
                this.tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }

            var callback = function (access, id, self) {
                if (access) {
                    self.tools.push({ name: "menu-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                }
            };

            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);

            if (this.relation.attributes["Active"] == "active") {
                this.tools.push({ name: "relation-activate", title: "Inactivate relation", icon: "fa fa-ban relation-activate ", text: "Inactivate relation", permission: "UpdateLink" });
            } else {
                this.tools.push({ name: "relation-inactivate", title: "Activate relation", icon: "fa fa-ban relation-inactivate ", text: "Activate relation", permission: "UpdateLink" });
            }
            
            this.tools.push({ name: "relation-remove", title: "Delete Entity", icon: "fa-trash-o fa", text: "Remove relation", permission: "DeleteLink" });

            // Check if there is a preview html template for this channel and entity type
            if (_.find(appData.planReleasePreviewTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && t.Properties.Channels && _.contains(t.Properties.EntityTypes, that.relation.get("TargetEntityTypeId")) && _.contains(t.Properties.Channels, that.channelId);
            })) {
                this.tools.push({ name: "preview-button", title: "Preview", icon: "fa-eye fa", text: "Preview" });
            }

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.relation.id,
                tools: this.tools
            }).$el);

            if (this.direction == "outbound") {
                this.drawCompleteness(this.relation.attributes.TargetEntity.Completeness);
            } else {
                this.drawCompleteness(this.relation.attributes.SourceEntity.Completeness);
            }
            return this; // enable chained calls
        } 
    });

    return entitycardView;
});
