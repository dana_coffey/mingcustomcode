define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'jquery-ui',
  'text!templates/start/startPageTemplate.html',
  'views/start/startPageChannelListView',
  'views/start/startPagePublicationListView',
  'views/start/startPageLatestEventListView'
], function ($, _, backbone,backboneforms,  jqueryui, startPageTemplate, startPageChannelListView, startPagePublicationListView, startPageLatestEventListView) {

    var startPlanReleasePage = backbone.View.extend({
        initialize: function () {

            var self = this;
            self.render();
        },
        render: function () {
            var startPage = this.$el.html(startPageTemplate);
            var channelList = new startPageChannelListView();
            startPage.find("#list-of-channels").append(channelList.$el);
            var publicationList = new startPagePublicationListView();
            startPage.find("#list-of-publications").append(publicationList.$el);
            var latestEventsList = new startPageLatestEventListView();
            startPage.find("#latest-events-from-connectors").append(latestEventsList.$el);
        }
    });

    return startPlanReleasePage;
});

