﻿define([
  'jquery',
  'underscore',
  'backbone',
  'views/appheader/appWprContextMenuView'
], function ($, _, backbone, appWprContextMenuView) {

    var appWprHeaderView = backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        render: function () {
            $("#context-menu-container").html(new appWprContextMenuView().el);
            return this;
        }
    });

    return appWprHeaderView;

});
