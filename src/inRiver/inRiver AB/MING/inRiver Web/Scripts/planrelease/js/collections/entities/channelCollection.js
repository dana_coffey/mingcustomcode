define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/entity/channelModel'
], function ($, _, backbone, channelModel) {
    var channelCollection = backbone.Collection.extend({
        model: channelModel,
        url: '/api/entity'
    });

    return channelCollection;
});
