define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',
  'text!templates/start/startPageChannelListTemplate.html',
  'sharedjs/views/channels/channelEntityView',
  'collections/entities/channelCollection'
], function ($, _, backbone, jqueryui, inRiverUtil,startPageChannelListTemplate, channelEntityView, channelCollection) {

    var startPageChannelList = backbone.View.extend({
        initialize: function () {

            this.$el.html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            this.undelegateEvents();
            this.updateChannelList();
        },
        entityCreated: function (model) {
            if (model.get("EntityType") == "Channel") {
                this.updateChannelList();
            }
        },
        updateChannelList: function () {
            var self = this;
            this.list = new channelCollection();
            this.list.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ entityType: "Channel", sortStarredDirection: "DESC" })
            });
        },
        render: function () {
            var thisPage = this.$el.html(startPageChannelListTemplate);
            if (this.list.models.length == 0) {
                this.$el.find("#channels-list").html("<div class='no-items-text'>There are no channels</div>");
            } else {
                _.each(this.list.models, function (model) {
                    var entityView = new channelEntityView({ model: model });
                    thisPage.find('#channels-list').append(entityView.render().el);
                });
            }

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.entityCreated);
            this.listenTo(window.appHelper.event_bus, 'channelstarred', this.updateChannelList);
            return this; // enable chained calls
        }
    });

    return startPageChannelList;
});

