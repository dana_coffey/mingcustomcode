﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var workareaModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
    });

    return workareaModel;

});
