﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'modalPopup',
  'jquery-ui-touch-punch',
  'sharedjs/views/panelsplitter/panelBorders',
  'sharedjs/views/entityview/entityDetailView',
  'views/entityview/entityStructureView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/entityview/entitySystemInformationView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'text!templates/entityview/entityTemplate.html'
], function ($, _, backbone, relationTypeCollection, entityModel, completenessModel, relationModel, modalPopup, jutp, panelBorders, entityDetailView, entityStructureView, entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, contextMenuView, entitySystemInformationView, inRiverUtil, permissionUtil, entityTemplate) {

    var entityView = backbone.View.extend({

        initialize: function (options) {
            var that = this;

            this.hasEmptyWorkArea = options.hasEmptyWorkArea; 
            //Ladda om entitetet, för vi vet ju för lite!
            var onDataHandler = function () {
                that.render();
            };

            if (this.model) {
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                var id = options.id;
                this.model = new entityModel(id);
                this.model.url = "/api/entity/" + id;
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

        },
        events: {
            "click #card-completeness-text": "onShowCompletnessDetails",
            "click #progress-rect-percentage": "onShowCompletnessDetails",
            "click .completeness": "onShowCompletnessDetails",
            "click #tab-structure": "onChangeTopTab",
            "click #tab-details": "onChangeTopTab",
            "click #menu-show-info-button": "onShowSystemInfo",
            "click #edit-button": "onEditDetail",
            "click #menu-lock-button": "onEntityLock",
            "click #menu-unlock-button": "onEntityUnlock",
            "click #star-button": "onEntityStar",
            "click #menu-delete-button": "onEntityDelete",
            "click #menu-add-task-button": "onAddTask",
            "click #publish-button": "onPublishClick",
            "click #unpublish-button": "onUnpublishClick",
            "click #menu-open-in-enrich-button": "onEnrichOpenClick",
        },

        onPublishClick: function(e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").hide();
                    $("#unpublish-button").show();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                }
            }
            
            xmlhttp.open("GET", "/api/channelaction/publish/" + this.model.id, true);
            xmlhttp.send();
        },
        onUnpublishClick: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").show();
                    $("#unpublish-button").hide();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                }
            }

            xmlhttp.open("GET", "/api/channelaction/unpublish/" + this.model.id, true);
            xmlhttp.send();
        },
        onAddTask: function (e) {
            e.stopPropagation();

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });
        },
        taskAddedNowLinkEntity: function (model) {
            var self = this;

            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: this.targetIdToCreate, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function () {
                    var i = 0;
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityClose: function () {
            appHelper.event_bus.trigger('entityareaclose');
            this.remove();
            this.unbind();
        },
        onEntityDelete: function () {
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);
            inRiverUtil.deleteEntity(this);
        },
        entityDeleted: function() {
            this.goTo("home");
        },
        onEditDetail: function () {
            var modal = new modalPopup();

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onEntityLock: function () {
            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    that.entityUpdated(that.model.id); 
                }
            }
            xmlhttp.open("GET", "/api/tools/lockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityUnlock: function () {
            var that = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    that.entityUpdated(that.model.id);
                }
            };

            xmlhttp.open("GET", "/api/tools/unlockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();

            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");

                e.currentTarget.title = "Star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star");
                $(e.currentTarget).addClass("icon-entypo-fix-star-empty");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");

                e.currentTarget.title = "Un-star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star-empty");
                $(e.currentTarget).addClass("icon-entypo-fix-star");
            }
        },
        onShowSystemInfo: function () {
            inRiverUtil.ShowEntityInfo(this.model.id);
        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },
        createDetailsTabView: function () {
            var contentSubView = new entityDetailSettingWrapperView({
                model: this.model
            });
            return contentSubView;
        },
        createStructureTabView: function () {
            var contentSubView = new entityStructureView({
                model: this.model
            });
            return contentSubView;

        },
        onChangeTopTab: function (e) {
            if (appSession.entityViewTopTabId == e.currentTarget.id) {
                return;
            }

            appSession.entityViewTopTabId = e.currentTarget.id; // remember default tab
            var urlTabName = appSession.entityViewTopTabId.replace("tab-", "");
            this.goTo("channel/" + this.id + "/" + urlTabName, { trigger: false });
            this.createTopTabView(appSession.entityViewTopTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
        },
        createTopTabView: function (idOfTab) {
            if (this._currentTopTabView) {
                this._currentTopTabView.remove();

                if (this._currentTopTabView.detailfields) {
                    for (index = 0; index < this._currentTopTabView.detailfields.length; index++) {
                        this._currentTopTabView.detailfields[index].stopListening(appHelper.event_bus);
                        this._currentTopTabView.detailfields[index].remove();
                        this._currentTopTabView.detailfields[index].unbind();
                    }
                }
            }
            if (idOfTab == "tab-details") {
                this._currentTopTabView = this.createDetailsTabView();
            } 
            if (idOfTab == "tab-structure") {
                this._currentTopTabView = this.createStructureTabView();
            }
        },
        indicateActiveTopTab: function () {
            this.$el.find("#tab-details").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-structure").removeClass("control-row-tab-button-active");
            this.$el.find("#" + appSession.entityViewTopTabId).addClass("control-row-tab-button-active");
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        entityUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch({
                    success: function () {
                        self.rerender = true;
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        drawCompleteness: function (percent) {
            this.$el.find('.percent').html(Math.round(percent) + '%');

            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        render: function () {
            var self = this;
            console.log("Render Channel/publication View");


            var icon = this.model.attributes.EntityType;
            if (icon == "Channel" && !this.model.attributes.ChannelPublished) {
                icon = "ChannelUnpublished";
            }


            this.$el.html(_.template(entityTemplate, { data: this.model.toJSON(), userName: appHelper.userName, icon:icon }));

            this.createTopTabView(appSession.entityViewTopTabId);
         
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
         
            this.stopListening(appHelper.event_bus);
            window.appHelper.event_bus.off('entityupdated');
            this.listenTo(appHelper.event_bus, 'entityupdated', this.entityUpdated);


            /*TODO*/
            if (appHelper.hasTask != undefined) {

                if (!appHelper.TaskRelationTypes) {
                    this.$el.find("#add-task-button").hide();
                }

            } else {
                this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: "Task" });

                this.relationTypes.fetch({
                    success: function () {

                        self.TaskRelationType = _.find(self.relationTypes.models, function (model) {
                            return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                        });
                        if (!self.TaskRelationType) {
                            self.$el.find("#add-task-button").hide();
                        }
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            this.tools = new Array();

            this.tools.push({ name: "menu-delete-button", title: "Delete channel", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });

            if (this.model.attributes.LockedBy == null || this.model.attributes.LockedBy == "") {
                this.tools.push({ name: "menu-lock-button", title: "Lock entity", icon: "fa fa-lock", text: "Lock", permission: "LockEntity" });
            } else {
                this.tools.push({ name: "menu-unlock-button", title: "Unlock entity", icon: "fa fa-unlock", text: "Unlock", permission: "LockEntity" });
            }

            var callback = function (access, id, self) {
                if (access) {
                    self.tools.push({ name: "menu-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                }
            };

            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);

            this.tools.push({ name: "menu-add-task-button", title: "Create Task", icon: "icon-entypo-fix-clipboard std-entypo-fix-icon", text: "Create Task", permission: "AddEntity" });

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: this.tools }).$el);

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#edit-button"));
            permissionUtil.CheckPermissionForElement("PublishChannel", this.$el.find("#unpublish-button"));
            permissionUtil.CheckPermissionForElement("PublishChannel", this.$el.find("#publish-button"));
           
            // Set borders, splitters and collapse
            panelBorders.initAllEvents();

            var collapsed = sessionStorage.getItem("workarea-collapsed");
            if (collapsed && collapsed.toString() == "true") {
                if ($("#workarea-header-label").hasClass("workarea-header-label-horisontal")) {
                    $("#workarea-header-toggle-button").click();
                }
            
            }
            this.drawCompleteness(this.model.attributes.Completeness);
            return this; // enable chained calls
        }
    });

    return entityView;
});