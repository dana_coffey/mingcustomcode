// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'sharedjs/views/topheader/topHeaderView',
  'views/start/startPageView',
  'views/entityview/entityView',
  'views/nodeview/nodeView',
  'views/appheader/appWprHeaderView',
  'sharedjs/views/workarea/workAreaHandler'

], function ($, _, backbone, jqueryui, topHeaderView, startPageView, entityView, nodeView, appWprHeaderView, workAreaHandler) {

    var AppRouter = backbone.Router.extend({
        routes: {
            // Define some URL routes
            "home": "home",
            "channel/:id(/:tab)": "entity",
            "publication/:id": "entity",
            "workarea?*queryString": "workarea",
            "workareasearch?*queryString": "workareasearch",
            "node/:id": "node",
            "selection/:id": "node",

            // Default
            '*actions': 'home',
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(newView.el);
        },
        closeView: function (specView) {
            if (specView) {

                if (specView._currentTopTabView && specView._currentTopTabView.detailfields) {
                    for (index = 0; index < specView._currentTopTabView.detailfields.length; index++) {
                        specView._currentTopTabView.detailfields[index].stopListening(window.appHelper.event_bus);
                        specView._currentTopTabView.detailfields[index].remove();
                        specView._currentTopTabView.detailfields[index].unbind();
                    }
                }

                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function (queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                        var aux = el.split('='), o = {};
                        if (aux.length >= 1) {
                            var val = undefined;
                            if (aux.length == 2)
                                val = aux[1];
                            o[aux[0]] = val;
                        }
                        return o;
                    }),
                    function (o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function () {
        console.log("init router");

        window.appHelper.event_bus = _({}).extend(backbone.Events);

        var appRouter = new AppRouter;

        window.appHelper.appRouter = appRouter;

        appRouter.workAreaHandler = new workAreaHandler({app:"planrelease"});
        window.appHelper.workAreaHandler = appRouter.workAreaHandler;


        var headerView = new topHeaderView({ app: "Plan & Release" });
        var appheaderView = new appWprHeaderView();

        appRouter.on('route:home', function (id) {

            if (this.startPageView != undefined) {
                this.closeView(this.startPageView);
            }
            appRouter.workAreaHandler.closeWorkAreas();
            appRouter.workAreaHandler.hideWorkareas();

            this.switchView(new startPageView());
        });

        appRouter.on('route:entity', function (id, tab) {
            var workareaCollection = undefined;
            var workareaEntities = undefined;

            if (tab) {
                appSession.entityViewTopTabId = "tab-" + tab;
            }
           
            var hasEmptyWorkArea;

            if (!appRouter.workAreaHandler.hasWorkareas()) {
                hasEmptyWorkArea = true;
                appRouter.workAreaHandler.reopenOrAddWorkAreas({ title: "Empty" });
                hasEmptyWorkArea = appRouter.workAreaHandler.hasEmptyWorkareas();


            } else {
                hasEmptyWorkArea = false;
            }


            this.switchView(new entityView({ id: id, queryview: this.workAreaView, hasEmptyWorkArea: hasEmptyWorkArea }));
            appRouter.workAreaHandler.setSmallWorkareas();
            appRouter.workAreaHandler.bindEvents();
            appRouter.workAreaHandler.setJsTreeDnd(true);
        });

        appRouter.on('route:workareasearch', function (queryString) {
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var workarea = appRouter.workAreaHandler.getActiveWorkarea();

            if (params.advsearch != undefined) {
                appRouter.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
            } else if (params.searchPhrase != undefined) {
                appRouter.workAreaHandler.search({ searchPhrase: params.searchPhrase });
            } else if (params.compressedEntities != undefined) {
                appRouter.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities });
            }
        });

        appRouter.on('route:workarea', function (queryString) {
            var params = this.parseQueryString(queryString);
            this.workAreaView = new workAreaView({ title: params.title, entities: params.entityIds, workareaId: params.workareaId, searchPhrase: params.searchPhrase, id: "1" });
            window.appHelper.mainWorkAreaView = this.workAreaView;
            this.switchView(this.workAreaView, $("#workarea-container"));
            this.workAreaView.setJsTreeDnd(false);
        });


        // Extend the View class to include a navigation method goTo
        backbone.View.prototype.goTo = function (loc) {
            appRouter.navigate(loc, true);
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        backbone.View.prototype.close = function () {

            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        backbone.history.start();
    };

    return {
        initialize: initialize
    };

});
