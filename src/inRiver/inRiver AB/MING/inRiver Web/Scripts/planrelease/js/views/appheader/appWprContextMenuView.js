﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/permissionUtil',
  'sharedjs/views/create/entityCreateView',
  'text!templates/shared/appContextMenuTemplate.html'
], function ($, _, backbone, permissionUtil,entityCreateView, appContextMenuTemplate) {

    var appWprContextMenuView = backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #close-tools-container": "onCloseTools"
        },
        onShowTools: function (e) {
            //e.stopPropagation();

            var $box = this.$el.find("#app-tools-container");

            $box.position({
                my: "right top",
                at: "right top+25",
                of: $("#tools-show")
            });

            $box.show(200);
        },
        deselect: function (e) {
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle();

            $box.position({
                my: "right top",
                at: "right top+25",
                of: $("#tools-show")
            });

            $(document).one('click', this.deselect.bind(this));
        },
        onCloseTools: function () {
            this.$el.find("#app-tools-container").hide(200);
        },
        createEntity: function (e) {
            this.onToggleTools(e);

            this.listenTo(appHelper.event_bus, 'entitycreated', this.entityCreated);

            //if (this._modalView) {
            //    this._modalView.remove();
            //}

            //this._modalView = new entityCreateView();

            //window.scrollTo(0, 0);
            //$("#modalWindow").html(this._modalView.el);
            //$("#modalWindow").show();
        },
        entityCreated: function (model) {
            if (model.attributes.EntityType != "Task") {
                this.goTo("entity/" + model.id);
            }
        },
        render: function () {
            this.$el.html(appContextMenuTemplate);
            if (permissionUtil.CheckPermission("AddEntity") == true) {
                var typeArray = new Array();
                typeArray.push("Channel");
                typeArray.push("Publication");
                this.$el.find("#create-entity-types").html(new entityCreateView({ contextmenu: this, selectedTypes: typeArray, showInWorkareaOnSave: false }).el);
            } else {
                this.$el.find(".app-tools").hide();
            }
            return this;
        }
    });

    return appWprContextMenuView;

});
