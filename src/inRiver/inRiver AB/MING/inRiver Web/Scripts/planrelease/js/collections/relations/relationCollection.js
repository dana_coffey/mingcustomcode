define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/relation/relationModel'
], function ($, _, backbone, relationModel) {
    var relationCollection = backbone.Collection.extend({
        model: relationModel,
        initialize: function (models, options) {
            this.channelId = options.channelId;
            this.entityId = options.entityId;
            this.direction = options.direction;
            this.entitytypeid = options.entitytypeid;
        },
        url: function () {
            var url = "/api/relation?id=" + this.entityId;
            if (this.channelId) {
                url += "&channelId=" + this.channelId;
            }
            url += "&direction=" + this.direction;
            if (this.entitytypeid) {
                url += "&entitytypeid=" + this.entitytypeid;
            }
            return url;
        }
    });

    return relationCollection;
});
