﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var entityTypeModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        schema: { // Used by backbone forms extension
            EntityType: { type: 'Select', title: "Entity Type", options: [] }
        },
        toString: function () {


            return this.get("Name").stringMap[appHelper.userLanguage];
        },
    });

    return entityTypeModel;

});
