define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, backbone) {
    var channelNodeEntitiesCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.channelId = options.channelId;
            this.channelNodeId = options.channelNodeId;
        },
        url: function () {
            return "/api/channelnodeentities/" + this.channelId + "/" + this.channelNodeId;
        }

    });
    return channelNodeEntitiesCollection;
});
