﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var publicationModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/entity',
        
    });

    return publicationModel;

});
