define([
  'jquery',
  'underscore',
  'backbone'
  //'models/entity/channelModel'
], function ($, _, backbone) {
    var channelEntityCollection = backbone.Collection.extend({
        //model: channelModel,
        initialize: function (options) {
            this.entityId = options.id;
        },
        url: function () {
            return '/api/channelentity/' + this.entityId;
        }

    });
    return channelEntityCollection;
});
