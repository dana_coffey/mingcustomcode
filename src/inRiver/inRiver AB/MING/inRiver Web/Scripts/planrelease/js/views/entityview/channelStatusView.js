﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'jstree',
  'chart-js',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
   'sharedjs/misc/permissionUtil',
  'collections/entities/structureCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/relation/relationModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/contextmenu/contextMenuView',
  'models/entity/entityModel',
  'views/nodeview/nodeView',
  'text!templates/entityview/channelStatusTemplate.html',
  'text!templates/entityview/channelEntityTypeStatusCardTemplate.html',
  'text!templates/entityview/channelOverallStatusCardTemplate.html',

], function ($, _, backbone, jqueryui, jstree, chartJs, modalPopup, inRiverUtil, permissionUtil, structureCollection, relationTypeCollection, relationModel, entityDetailSettingView, entityDetailSettingWrapperView, contextMenuView, entityModel, nodeView, channelStatusTemplate, channelEntityTypeStatusCardTemplate, channelOverallStatusCardTemplate) {

    var channelStatusView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.loaded = false;
            
            this.entityModel = options.model;
            this.entityType = options.model.attributes.EntityType;

            if (this.entityType == "Channel" && !options.model.attributes.ChannelPublished) {
                this.entityType += "Unpublished";
            }
            //// load all fields.
            //this.model = new structureCollection({ id: this.entityModel.get("Id") });
            //this.model.fetch(
            //{
            //    success: function () {
            //        that.render();
            //    },
            //    error: function (model, response) {
            //        inRiverUtil.OnErrors(model, response);
            //    }
            //});
            this.render();
        },
        events: {
            //"click #open": "onOpenNode",
        },
        onClose: function () {
        },
        render: function () {
            this.$el.html(_.template(channelStatusTemplate, { data: this.model.toJSON() }));

            var overallStatusTemplate = _.template(channelOverallStatusCardTemplate);
            this.$el.find("#card-container").append(overallStatusTemplate({ data: {} }));
            
            var data = {
                labels: [
                    "",
                    "",
                ],
                datasets: [
                    {
                        data: [300, 50],
                        backgroundColor: [
                            "#999",
                            "#eee",
                        ],
                        hoverBackgroundColor: [
                            "#999",
                            "#eee",
                        ]
                    }]
            };
            Chart.defaults.global.legend.display = false;

            var ctx = this.$el.find("#pie-chart-wrap");
            var myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data,
                options: {
                    responsive: false
                }
            });

            var cardTemplate = _.template(channelEntityTypeStatusCardTemplate);

            var data1 = {
                label: "Product",
                completeness: 79,
                nEntities: 2411,
                nCompletedEntities: 1877,
                measures: [
                    { label: "Product Description", value: 71 },
                    { label: "Image Exist", value: 30 },
                    { label: "Items Complete", value: 97 },
                ]
            };
            this.$el.find("#card-container").append(cardTemplate({ data: data1 }));

            var data2 = {
                label: "Item",
                completeness: 100,
                nEntities: 801,
                nCompletedEntities: 801,
                measures: [
                    { label: "Detail Image", value: 88 },
                    { label: "Translated", value: 45 },
                    { label: "Description Exists", value: 75 },
                ]
            };
            this.$el.find("#card-container").append(cardTemplate({ data: data2 }));
            
            var data3 = {
                label: "Campaign",
                completeness: 100,
                nEntities: 15,
                nCompletedEntities: 15,
                measures: [
                    { label: "Name Exists", value: 100 },
                    { label: "Products Complete", value: 80 },
                    { label: "Activity Complete", value: 5 },
                ]
            };
            this.$el.find("#card-container").append(cardTemplate({ data: data3 }));

            var data4 = {
                label: "Resource",
                completeness: 90,
                nEntities: 17680,
                nCompletedEntities: 15687,
                measures: [
                    { label: "Categorized", value: 30 },
                    { label: "Approved", value: 50 },
                    { label: "Retouch", value: 99 },
                ]
            };
            this.$el.find("#card-container").append(cardTemplate({ data: data4 }));

            return this; // enable chained calls
        }
    });

    return channelStatusView;
});
