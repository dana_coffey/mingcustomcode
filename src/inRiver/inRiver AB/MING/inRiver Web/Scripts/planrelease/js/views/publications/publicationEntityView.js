define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/publications/publicationEntityTemplate.html',
  'sharedjs/misc/permissionUtil'
], function ($, _, backbone, modalPopup, inRiverUtil, contextMenuView, entityDetailSettingView, entityDetailSettingWrapperView, publicationEntityTemplate, permissionUtil) {

    var entitycardView = backbone.View.extend({
        //tagName: 'div',

        initialize: function () {

        },
        events: {
            "click #star-button": "onEntityStar",
            "click .publication-entity-card-wrap": "onCardClick",
            "click #menu-edit-button": "onEditClick",
            "click #menu-open-in-enrich-button": "onEnrichOpenClick",
            "click #menu-publish-button": "onPublishClick",
            "click #menu-unpublish-button": "onUnpublishClick",
            "click #menu-delete-button": "onDeleteClick"
        },
        onEditClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });


        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onDeleteClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            inRiverUtil.deleteEntity(this);
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var that = this; 

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    that.entityUpdated(that.model.id);
                }
            }

            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");
            }
            appHelper.event_bus.trigger('publicationstared');

        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },
        onCardClick: function (e) {
            e.stopPropagation();
            this.goTo("#publication/" + this.model.id);
        },
        onClose: function () {
        },
        entityUpdated: function (id) {

            if (id == this.model.id) {

                var self = this;
                this.model.fetch({
                    success: function() {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                }); 
            }
        },
        entityDeleted: function (id) {
            if (id == this.model.id) {
                this.stopListening(window.appHelper.event_bus);
                this.remove();
                this.unbind();
            }
        },
        render: function ( ) {

            if (this.model == null) {
                return this;
            }

            var isPublished = false;
            _.each(this.model.attributes["Fields"], function (field) {
                if (field.FieldType == "ChannelPublished") {
                    if (field.Value != null && field.Value == "True") {
                        isPublished = true;
                    }
                }
            });

            this.publishedField = isPublished;
            this.$el.html(_.template(publicationEntityTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName, published: this.publishedField }));

            //add context menu

            this.tools = new Array();
            this.tools.push({ name: "menu-edit-button", title: "Edit channel information", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });

            if (this.model.attributes.Starred == "1") {
                this.tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star", text: "Unstar entity" });
            }
            else {
                this.tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty", text: "Star entity" });
            }
            
            var callback = function (access, id, self) {
                if (access) {
                    self.tools.push({ name: "menu-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                }
            };

            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);

            this.tools.push({ name: "menu-delete-button", title: "Delete channel", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: this.tools }).$el);

            this.listenTo(window.appHelper.event_bus, 'entityupdated', this.entityUpdated);
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);

            return this; // enable chained calls
        }
    });

    return entitycardView;
});
