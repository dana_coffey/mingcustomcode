define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'modalPopup',
  'views/entityview/entityView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/misc/inRiverUtil',
  'text!templates/entitycard/workareaCardTemplate.html',
  'sharedjs/misc/permissionUtil'
], function ($, _, Backbone, alertify, completenessModel, relationModel, modalPopup, entityView, entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, contextMenuView, inRiverUtil, workareaCardTemplate, permissionUtil) {

    var entitycardView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.type = options.type;
            this.taskRelations = options.taskRelations;
        },
        events: {
            "click .completeness-small": "onShowCompletnessDetails",
            "click #large-card-completeness-text": "onShowCompletnessDetails",
            "click #edit-button": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "click #enrich-button": "onEnrichOpenClick"
        },
        onEntityDelete: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);


            inRiverUtil.deleteEntity(this);

        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },

        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onEditDetail: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));

        },
        onClose: function () {
        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },

        render: function () {

            var self = this;

            if (this.model == null) {
                return this;
            }

            if (this.taskRelations != null) {
                this.TaskRelationType = _.find(this.taskRelations.models, function (model) {
                    return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                });
            }


            if (this.type == "large") {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: appHelper.userName, enableAddTask: this.TaskRelationType != undefined }));
            } else {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: appHelper.userName, enableAddTask: this.TaskRelationType != undefined }));
            }

            var isLocked = inRiverUtil.isLocked(this.model.attributes.LockedBy);

            //add context menu

            this.tools = new Array();

            if (!isLocked) {
                this.tools.push({ name: "edit-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            var callback = function (access, id, self) {
                if (access) {
                    self.tools.push({ name: "enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                }
            };

            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.model.id,
                tools: this.tools
            }).$el);

            this.drawCompleteness(this.model.attributes.Completeness);
            return this; // enable chained calls
        }
    });

    return entitycardView;
});
