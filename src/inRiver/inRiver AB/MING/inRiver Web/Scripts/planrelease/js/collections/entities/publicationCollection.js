define([
  'jquery',
  'underscore',
  'backbone',
  'models/entity/publicationModel'
], function ($, _, backbone, publicationModel) {
    var publicationCollection = backbone.Collection.extend({
        model: publicationModel,
        url: '/api/entity'
    });

    return publicationCollection;
});
