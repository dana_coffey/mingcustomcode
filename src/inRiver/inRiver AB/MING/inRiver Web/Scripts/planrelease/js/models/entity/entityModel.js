﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var entityModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/entity',
        
    });

    return entityModel;

});
