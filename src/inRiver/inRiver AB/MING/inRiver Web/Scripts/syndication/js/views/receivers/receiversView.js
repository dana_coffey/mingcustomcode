﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    /* Templates */
    'text!templates/receivers/receiversTemplate.html',
    'text!templates/receivers/receiversViewItemCaptionTemplate.html',
    'text!templates/receivers/receiversViewReceiverItemTemplate.html',
    'text!templates/receivers/receiversViewDistributionFormatItemTemplate.html',
    /* Models & Collections */
    'models/receiverModel',
    'collections/receiverCollection',
    'models/distributionFormatModel',
    'collections/distributionFormatCollection', 
    /* Views */
    'views/receivers/receiverItemView',
    'views/receivers/createEditReceiverModalPopupView'
], function ($, _, backbone, backboneforms, jqueryui, modalPopup, inRiverUtil,
    receiversTemplate, receiversViewItemCaption, receiversViewReceiverItem, receiversViewDistributionFormatItem,
    receiverModel, receiverCollection, distributionFormatModel, distributionFormatCollection,
    receiverItemView, createEditReceiverModalPopupView) {

    var receiversView = backbone.View.extend({
        initialize: function () {
            var self = this;

            this.selectedReceiverId = null;

            this.receiverItemIdBase = "receiversView-receiverItem";

            this.receivers = new receiverCollection();
           
            this.render();

            /*
            var receiver = new receiverModel();
            var map = {};
            map["Name"] = "Google v2";
            map["Xsd"] = "<schema>Xsd</schema>";
            map["XsdImport"] = "<schema>XsdImport</schema>";
            map["XsdImportFilename"] = "XsdImportFilename";
            receiver.set(map);

            var distributionFormats = new distributionFormatCollection();
            var distributionFormat = new distributionFormatModel();
            distributionFormat.set("Name", "MyXml");
            distributionFormats.add(distributionFormat);
            receiver.set("DistributionFormats", distributionFormats);

            receiver.save();*/

            this.receivers.fetch({
                success: function () {

                    self.receivers.each(
                        function(model, index) {
                            model.set("DistributionFormatsVisible", false);
                        });

                    self.render();
                }
            });
        },
        refetchReceivers: function(e) {
            var self = this;
            this.receivers.fetch({
                async: false,
                reset: true,
                success: function () {
                    self.receivers.each(
                        function (model, index) {
                            model.set("DistributionFormatsVisible", false);
                        });
                    self.render();
                }
            });
        },
        events: {
            "click span#deleteReceiver": "deleteReceiver",
            "click #createNewReceiverButton": "createNewReceiver"
        },
        createNewReceiver: function(e) {
            e.stopPropagation();

            var pop = new modalPopup();
            pop.popOut(
                new createEditReceiverModalPopupView({
                    callbackContext: this,
                    popupOwner: pop
                }),
                {
                    size: "medium",
                    header: "Create new Receiver",
                    usesavebutton: true,
                    onSave: this.onSaveReceiver,
                    onCancel: function (popup) {
                        popup.close();
                    }
                });
            $("#save-form-button").removeAttr("disabled");
        },
        onSaveReceiver: function (popup, subView) {
            subView.save();
        },
        deleteReceiver: function (e) {
            e.stopPropagation();
        },
        render: function () {
            //
            //  Stop listen to event_bus triggers.
            //
            this.stopListening(window.appHelper.event_bus);

            var self = this;
            this.$el.html(_.template(receiversTemplate));

            var listOfReceiversElement = self.$el.find("#receiversView-listOfReceivers");

            this.receivers.each(
                function(model, index) {

                    var receiverItemViewInstance = new receiverItemView({ receiverModel: model });

                    listOfReceiversElement.append(receiverItemViewInstance.$el);
                });

            this.listenTo(window.appHelper.event_bus, "syndication.receiversView.render", this.refetchReceivers);
            
            return this; // enable chained calls
        }
    });

    return receiversView;
});