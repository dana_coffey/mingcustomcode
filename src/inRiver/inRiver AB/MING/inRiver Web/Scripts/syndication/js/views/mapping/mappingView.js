define([
  "jquery",
  "underscore",
  "backbone",
  "backbone-forms",
  "jquery-ui",
  "jstree",
  "moment",
  "sharedjs/misc/inRiverUtil",
  "sharedjs/collections/fieldtypes/FieldTypesCollection",
  "sharedjs/collections/languages/LanguagesCollection",
  "collections/receiverCollection",
  "text!templates/mapping/mappingTemplate.html",
  "text!templates/mapping/mappingRowTemplate.html",
  "text!templates/mapping/mappingInputTemplate.html"

], function ($, _, backbone, backboneforms, jqueryui, jstree, moment, inRiverUtil, fieldTypeCollection, languagesCollection, receiverCollection, mappingTemplate, mappingRowTemplate, mappingInputTemplate) {

    var mappingView = backbone.View.extend({
        initialize: function () {
            var self = this;
            this.transformationsCollection = new Backbone.Collection(null, {
                 url: "/api/syndication/transformscript"
            });
            this.receiverCollection = new receiverCollection();
            this.languagesCollection = new languagesCollection();
            this.fieldTypeCollection = new Backbone.Collection(null, { url: "/api/syndication/entitytypes" });

            self.render();

            this.done = 0;
        },
        renderReceiverPicker: function () {
            var self = this;
            var receiverDropDownTemplate = this.$el.find("#receiver-picker");
            this.receiverCollection.each(function (receiver) {
                receiverDropDownTemplate.append("<option value='" + receiver.get("Id") + "'>" + receiver.get("Name") + "</option>");
            });
            receiverDropDownTemplate.change(function () {
                self.loadMapping(receiverDropDownTemplate.val());
            });
        },
        renderFieldTypePicker: function () {
            if (!this.fieldTypeCollection.isLoaded || !this.languagesCollection.isLoaded) {
                return;
            }

            var self = this;

            this.leftSidebar = this.$el.find("#field-type-picker");
            this.fieldTypeDictionary = {};
            var root = $("<ul/>");
            this.fieldTypeCollection.each(function(entityType) {
                var entityTypeElement = $("<li data-jstree='{\"type\":\"EntityType\"}'>" + entityType.get("DisplayName") + "</li>");
                var categoriesElement = $("<ul/>");
                _.each(entityType.get("Categories"), function(category) {
                    var categoryElement = $("<li data-jstree='{\"type\":\"Category\"}'>" + category.DisplayName + "</li>");
                    var fieldTypesElement = $("<ul/>");
                    _.each(category.FieldTypes, function (fieldType) {
                        var fieldTypeElement = $("<li id='" + fieldType.Id + "' data-jstree='{\"type\":\"FieldType\"}'>" + fieldType.DisplayName + "</li>");
                        fieldTypesElement.append(fieldTypeElement);
                        if (fieldType.DataType === "LocaleString" || fieldType.CVL && fieldType.CVL.DataType === "LocaleString") {
                            var languagesElement = $("<ul/>");
                            self.languagesCollection.each(function (language) {
                                languagesElement.append("<li id='" + fieldType.Id + "|" + language.id + "' data-jstree='{\"type\":\"Language\"}'>" + language.get("DisplayName") + "</li>");
                            });
                            fieldTypeElement.append(languagesElement);
                        }
                        self.fieldTypeDictionary[fieldType.Id] = fieldType;
                    });
                    categoryElement.append(fieldTypesElement);
                    categoriesElement.append(categoryElement);
                });
                entityTypeElement.append(categoriesElement);
                root.append(entityTypeElement);
            });
            self.leftSidebar.append(root);
            self.leftSidebar.jstree({
                core: {
                    themes: { dots: false, icons: false },
                    multiple: false
                },
                plugins: [
                    "wholerow", "dnd", "types"
                ],
                types: {
                    EntityType: {},
                    Category: {},
                    FieldType: {},
                    Language: {}
                },
                dnd: {
                    always_copy: true,
                    is_draggable: function(node) {
                        return node[0].type === "FieldType" || node[0].type === "Language";
                    }
                }
            });
            self.leftSidebar.on("select_node.jstree",
                    function (evt, data) {
                        var model = self.fieldTypeDictionary[data.selected[0]];
                        self.$el.find("#field-type-name").text(model.DisplayName);
                        self.$el.find("#field-type-data-type").text(model.DataType);
                        self.$el.find("#field-type-mandatory").text(model.Mandatory ? "Yes" : "No");
                        self.$el.find("#field-type-unique").text(model.Unique ? "Yes" : "No");
                    }
            );
            self.registerDragDropListeners();
        },
        registerDragDropListeners: function() {
            var self = this;

            $(document)
                .on("dnd_move.vakata", function (e, data) {
                    var t = $(data.event.target);
                    if (!t.closest(".jstree").length) {
                        if (t.closest(".input-value-control").length) {
                            data.helper.find(".jstree-icon").removeClass("jstree-er").addClass("jstree-ok");
                        }
                        else {
                            data.helper.find(".jstree-icon").removeClass("jstree-ok").addClass("jstree-er");
                        }
                    }
                })
                .on("dnd_stop.vakata", function (e, data) {
                    var t = $(data.event.target);
                    if (!t.closest(".jstree").length) {
                        var inputValueControl = t.closest(".input-value-control");
                        if (inputValueControl.length) {
                            var ids = data.data.nodes[0].split("|");
                            var fieldTypeId = ids[0];
                            var language = ids.length > 1 ? ids[1]: null;
                            inputValueControl.html(self.getInputValueControl(fieldTypeId, null, language));
                            self.mappingModel.set(inputValueControl.attr("id"), {
                                IsFieldType: true,
                                FieldType: { id: fieldTypeId, displayname: data.element.text },
                                Value: null,
                                Language: language
                            });
                            inputValueControl.find(".delete-field-type-mapping").click(function () {
                                self.mappingModel.set(inputValueControl.attr("id"), {
                                    IsFieldType: false,
                                    FieldType: null,
                                    Value: null,
                                    Language: null
                                });
                                inputValueControl.html(self.getInputValueControl(null, null, null));
                            });
                        }
                    }
                });
        },
        renderMappingTable: function () {
            if (!this.transformationsCollection.isLoaded || !this.mappingModel.isLoaded) {
                return;
            }

            var self = this;

            self.$el.find("#mapping-table tr:gt(0)").remove();

            _.each(this.mappingModel.get("MappingAttributeModels"), function(mappingAttributeModel, mappingAttributeIndex) {
                var transformationDropDown = self.transformationDropDownTemplate.clone();
                var row = $(mappingRowTemplate);
                var transformScriptModelPath = "MappingAttributeModels." + mappingAttributeIndex + ".TransformScript";

                row.attr("id", mappingAttributeIndex);
                row.find(".mapping-row-transformation").html(transformationDropDown);

                self.transformScriptChanged(mappingAttributeModel, row);

                transformationDropDown.val(self.getTransformScript(mappingAttributeModel).Id);
                transformationDropDown.change(function () {
                    var transformScriptModel = self.transformationsCollection.findWhere({ Id: parseInt($(this).val()) });
                    self.mappingModel.set(transformScriptModelPath, transformScriptModel ? transformScriptModel.toJSON() : null);
                });

                self.mappingModel.on("change:" + transformScriptModelPath + " change:" + transformScriptModelPath + ".Id", function () {
                    self.transformScriptChanged(self.mappingModel.get("MappingAttributeModels." + mappingAttributeIndex), row);
                });

                row.find(".mapping-row-output").html(mappingAttributeModel.OutgoingAttribute.DisplayName);

                self.$el.find("#mapping-table").append(row);
            });

            self.$el.find("tr").click(function () {
                var thisRow = this;
                $(thisRow).addClass("selected").siblings().removeClass("selected");

                var mappingAttributeModel = self.mappingModel.get("MappingAttributeModels." + thisRow.id);
                self.$el.find("#output-attribute-name").text(mappingAttributeModel.OutgoingAttribute.DisplayName);
                self.$el.find("#output-attribute-description").html(mappingAttributeModel.OutgoingAttribute.Description);
                self.$el.find("#output-attribute-type").text(mappingAttributeModel.OutgoingAttribute.TypeName);
                //self.$el.find("#output-attribute-required").text(mappingAttributeModel.output.required);
                //self.$el.find("#output-attribute-length").text(mappingAttributeModel.output.length);
                //self.$el.find("#output-attribute-repeated").text(mappingAttributeModel.output.repeated);

                self.showTransformScriptDetails(self.getTransformScript(mappingAttributeModel));
            });
        },
        transformScriptChanged: function (mappingAttributeModel, row) {
            var self = this;
            var transformScript = this.getTransformScript(mappingAttributeModel);

            self.showTransformScriptDetails(transformScript);

            row.find(".mapping-row-input").html("");
            _.each(transformScript.InParameters, function (inputModel, index) {
                var template = $(mappingInputTemplate);
                var modelPath = "MappingAttributeModels." + row.attr("id") + ".Inputs." + index;
                template.find(".input-value-control").attr("id", modelPath);
                template.find(".input-label").text(inputModel.ParameterName + ":");
                row.find(".mapping-row-input").append(template);
                if (mappingAttributeModel.Inputs[index]) {
                    template.find(".input-value-control").html(self.getInputValueControl(mappingAttributeModel.Inputs[index].FieldType ? mappingAttributeModel.Inputs[index].FieldType.id : null, mappingAttributeModel.Inputs[index].Value, mappingAttributeModel.Inputs[index].Language));
                }
                template.find(".input-value-control").change(function() {
                    self.mappingModel.set(modelPath, {
                        IsFieldType: false,
                        FieldType: null,
                        Value: $(this).find("input").val()
                    });
                });
                template.find(".delete-field-type-mapping").click(function () {
                    self.mappingModel.set(modelPath, {
                        IsFieldType: false,
                        FieldType: null,
                        Value: null
                    });
                    template.find(".input-value-control").html(self.getInputValueControl(null, null));
                });
                self.mappingModel.on("change:" + modelPath + ".Value change:" + modelPath + ".FieldType", function () {
                    self.inputValueChanged(mappingAttributeModel, row);
                });
            });
            this.validateMapping(mappingAttributeModel, row);
        },
        inputValueChanged: function (mappingAttributeModel, row) {
            this.validateMapping(mappingAttributeModel, row);
        },
        parseDateTime: function (dateTime) {
            return moment(dateTime, ["YYYY-MM-DD", "YYYY-MM-DDZZ", "HH:mm:ss", "HH:mm:ssZZ", "YYYY-MM-DDTHH:mm:ss", "YYYY-MM-DDTHH:mm:ssZZ"], true).isValid();
        },
        validateMapping: function (mappingAttributeModel, row) {
            var self = this;
            var transformIcon = "";
            var transformMessage = "";
            var transformScript = this.getTransformScript(mappingAttributeModel);
            var inputIndex = 0;
            var inputValidationElements = row.find(".mapping-row-input-message");
            var isValid = true;
            _.each(transformScript.InParameters, function(inParameter) {
                var inputIcon = "";
                var inputMessage = "";
                var input = mappingAttributeModel.Inputs[inputIndex];
                if (input) {
                    if (input.FieldType) {
                        var inputDataType = input.FieldType.DataType === "LocaleString" && fieldType.Language ? "String" : input.FieldType.DataType;

                        switch (inParameter.ParameterType) {
                            case "String":
                                inputIcon = self.getOkIcon();
                                break;
                            case "LocaleString":
                                if (inputDataType === "LocaleString") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A locale string is required.";
                                }
                                break;
                            case "Double":
                                if (inputDataType === "Double" || inputDataType === "Integer") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A decimal number is required.";
                                }
                                break;
                            case "Integer":
                                if (inputDataType === "Integer") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "An integer is required.";
                                }
                                break;
                            case "Boolean":
                                if (inputDataType === "Boolean") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A boolean is required.";
                                }
                                break;
                            case "DateTime":
                                if (inputDataType === "DateTime") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A datetime is required.";
                                }
                                break;
                            case "Xml":
                                if (inputDataType === "Xml") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "Xml is required.";
                                }
                                break;
                            case "File":
                                if (inputDataType === "File") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A file is required.";
                                }
                                break;
                            case "CVL":
                                if (inputDataType === "CVL") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A CVL is required.";
                                }
                                break;
                            default:
                                inputIcon = self.getWarningIcon();
                                inputMessage = "The expected format of this parameter is unknown. The mapping cannot be verified.";
                        }
                    } else if (input.Value) {
                        switch (inParameter.ParameterType) {
                            case "String":
                                inputIcon = self.getOkIcon();
                                break;
                            case "LocaleString":
                                inputIcon = self.getErrorIcon();
                                isValid = false;
                                inputMessage = "A field type with the LocaleString data type is required.";
                                break;
                            case "Double":
                                if (input.Value === parseFloat(input.Value).toString()) {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "A decimal number is required.";
                                }
                                break;
                            case "Integer":
                                if (input.Value === parseInt(input.Value).toString()) {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "An integer is required.";
                                }
                                break;
                            case "Boolean":
                                if (input.Value === "true" || input.Value === "false") {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "'true' or 'false' is required.";
                                }
                                break;
                            case "DateTime":
                                if (self.parseDateTime(input.Value)) {
                                    inputIcon = self.getOkIcon();
                                } else {
                                    inputIcon = self.getErrorIcon();
                                    isValid = false;
                                    inputMessage = "The DateTime needs to be specified in the following form \"YYYY-MM-DDThh:mm:ssZ\".";
                                }
                                break;
                            case "Xml":
                                inputIcon = self.getErrorIcon();
                                isValid = false;
                                inputMessage = "A field type with the XML data type is required.";
                                break;
                            case "File":
                                inputIcon = self.getErrorIcon();
                                isValid = false;
                                inputMessage = "A field type with the File data type is required.";
                                break;
                            case "CVL":
                                inputIcon = self.getErrorIcon();
                                isValid = false;
                                inputMessage = "A field type with the CVL data type is required.";
                                break;
                            default:
                                inputIcon = self.getWarningIcon();
                                inputMessage = "The expected format of this parameter is unknown. The mapping cannot be verified.";
                        }
                    }
                }

                self.updateValidationFeedback($(inputValidationElements[inputIndex++]), "mapping-row-input-message", inputIcon, inputMessage);
            });

            if (mappingAttributeModel.TransformScript) {
                switch (mappingAttributeModel.OutgoingAttribute.TypeName) {
                    case "String":
                        transformIcon = this.getOkIcon();
                        break;
                    default:
                        transformIcon = this.getWarningIcon();
                        transformMessage = "The format of the result of this transform is unknown. The mapping cannot be verified.";
                }
            }
            mappingAttributeModel.isValid = isValid;
            this.updateValidationFeedback(row.find(".mapping-row-transform-message"), "mapping-row-transform-message", transformIcon, transformMessage);
        },
        updateValidationFeedback: function (element, elementClass, iconClasses, message) {
            element.attr("class", elementClass + " fa " + iconClasses);
            element.attr("title", message);
        },
        getOkIcon: function() {
            return "fa-check-circle mapping-validation-ok";
        },
        getWarningIcon: function () {
            return "fa-question-circle mapping-validation-warning";
        },
        getErrorIcon: function () {
            return "fa-exclamation-triangle mapping-validation-error";
        },
        showTransformScriptDetails: function (transformScript) {
            this.$el.find("#transformation-name").text(transformScript.ScriptName);
            this.$el.find("#transformation-description").html(transformScript.Description);
            this.$el.find("#transformation-return-value").html(transformScript.ReturnDescription);
            this.$el.find("#transformation-script").html(transformScript.Script);
        },
        getTransformScript: function(mappingAttributeModel) {
            return mappingAttributeModel.TransformScript
                ? mappingAttributeModel.TransformScript
                : {
                    ScriptName: "None",
                    Description: "The input will be mapped directly to the output attribute without any transformation.",
                    Script: "None",
                    InParameters: [
                    {
                        ParameterName: "Input",
                        ParameterType: mappingAttributeModel.OutgoingAttribute.TypeName
                    }
                    ]
                };
        },
        getInputValueControl: function (fieldTypeId, value, language) {
            var control;
            if (fieldTypeId !== null && fieldTypeId !== undefined) {
                control = $("<div class='input-value-field-type' data-field-type='" + fieldTypeId + "'>" + this.fieldTypeDictionary[fieldTypeId].DisplayName + "<i class='fa fa-times delete-field-type-mapping' aria-hidden='true'/></div>");
                if (language !== null && language !== undefined) {
                    control.attr("data-language", language);
                    control.append("(" + this.languagesCollection.get(language).get("DisplayName") + ")");
                }
            } else {
                control = $("<input type='text'/>");
                control.val(value);
            }
            return control;
        },
        loadMapping: function (receiverId) {
            var self = this;
            this.mappingModel.set("id", receiverId);
            this.mappingModel.fetch({
                async: false,
                success: function (model) {
                    model.isLoaded = true;
                    self.renderMappingTable();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var self = this;
            this.$el.html(mappingTemplate);
            this.$el.find("#save-mapping").click(function () {
                var isValid = true;
                _.each(self.mappingModel.get("MappingAttributeModels"), function(mappingAttributeModel) {
                    if (!mappingAttributeModel.isValid) {
                        isValid = false;
                    }
                });
                var receiverId = self.mappingModel.get("id");
                var receiverModel = self.receiverCollection.get(receiverId);
                receiverModel.set("IsValidated", isValid);
                receiverModel.save();
                self.mappingModel.save(null, {
                    success: function () {
                        self.renderMappingTable();
                        inRiverUtil.Notify("Mapping successfully saved");
                    }
                });
            });
            this.receiverCollection.fetch({
                async: false,
                success: function (receiverCollection) {
                    self.renderReceiverPicker();
                    var MappingDeepModel = Backbone.DeepModel.extend({
                        urlRoot: "/api/syndication/mapping/"
                    });
                    self.mappingModel = new MappingDeepModel();
                    self.loadMapping(receiverCollection.models[0].get("Id"));
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.languagesCollection.fetch({
                async: false,
                success: function (collection) {
                    collection.isLoaded = true;
                    self.renderFieldTypePicker();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            this.fieldTypeCollection.fetch({
                async: false,
                success: function (collection) {
                    collection.isLoaded = true;
                    self.renderFieldTypePicker();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.transformationsCollection.fetch({
                async: false,
                success: function (collection) {
                    self.transformationDropDownTemplate = $("<select><option/></select>");
                    collection.each(function (transformation) {
                        if (transformation.get("ShowInUI")) {
                            self.transformationDropDownTemplate.append("<option value='" +transformation.get("Id") + "'>" +transformation.get("ScriptName") + "</option>");
                        }
                    });
                    collection.isLoaded = true;
                    self.renderMappingTable();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        }
    });

    return mappingView;
});

