define([
  'jquery',
  'underscore',
  'backbone',
  'models/receiverModel'
], function ($, _, backbone, receiverModel) {
    var receiverCollection = backbone.Collection.extend({
        model: receiverModel,
        url: '/api/syndication/receiver'
    });

    return receiverCollection;
});
