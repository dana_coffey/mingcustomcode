﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var distributionFormatModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/syndication/distributionFormat'
    });

    return distributionFormatModel;

});
