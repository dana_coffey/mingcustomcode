﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'text!templates/transformation/transformationTemplate.html'
], function ($, _, backbone, backboneforms, jqueryui, transformationTemplate) {

    var transformationView = backbone.View.extend({
        template: _.template(transformationTemplate),
        initialize: function() {
            var self = this;
            self.render();
        },
        render: function() {
            this.$el.html(this.template);

            return this;
        }
    });

    return transformationView;
});