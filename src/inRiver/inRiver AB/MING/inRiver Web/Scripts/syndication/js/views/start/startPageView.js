define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'jquery-ui',
  'views/start/startPageLatestResultView',
  'views/start/startPageWorkareaView',
  'text!templates/start/startPageTemplate.html'
], function ($, _, backbone, backboneforms, jqueryui, startPageLatestResultView, startPageWorkareaView, startPageTemplate) {

    var startPageView = backbone.View.extend({
        initialize: function () {
            var self = this;
            self.render();
        },
        render: function () {
            var startPage = this.$el.html(startPageTemplate);
            var latestResultView = new startPageLatestResultView();
            startPage.find("#list-of-latest-results").append(latestResultView.$el);
            var workareaView = new startPageWorkareaView();
            startPage.find("#list-of-workareas").append(workareaView.$el);
        }
    });

    return startPageView;
});

