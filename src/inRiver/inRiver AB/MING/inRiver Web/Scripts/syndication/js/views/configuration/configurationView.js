﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'sharedjs/misc/inRiverUtil',
  'jquery-ui',
  'views/mapping/mappingView',
  'views/transformation/transformationView',
  'views/receivers/receiversView',
  'text!templates/configuration/configurationTemplate.html'
], function ($, _, backbone, backboneforms, inRiverUtil, jqueryui, mappingView, transformationView, receiversView, configurationTemplate) {

    var configurationView = backbone.View.extend({
        template: _.template(configurationTemplate),
        initialize: function () {
            var self = this;
            self.render();
        },
        events: {
            "click #tab-mappings": "onChangeTab",
            "click #tab-transform": "onChangeTab",
            "click #tab-receivers": "onChangeTab"
        },
        onChangeTab: function (e) {
            var self = this;
            if (window.appSession.syndicationConfigurationTabId === e.currentTarget.id) {
                return;
            }

            //inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(this._currentTopTabView, function () {
                window.appSession.syndicationConfigurationTabId = e.currentTarget.id; // remember default tab
                var urlTabName = window.appSession.syndicationConfigurationTabId.replace("tab-", "");
                self.goTo("configuration/" + urlTabName, { trigger: false });
                self.createTabView(window.appSession.syndicationConfigurationTabId);
                self.$el.find("#syndication-configuration-main").html(self.currentTopTabView.el);
                self.indicateActiveTab();
            //});
        },
        createTabView: function (idOfTab) {
            if (this.currentTopTabView) {
                this.currentTopTabView.remove();
                this.currentTopTabView.close();
            }

            if (idOfTab === "tab-mappings") {
                this.currentTopTabView = new mappingView();
            } else if (idOfTab === "tab-transform" || idOfTab === undefined) {
                this.currentTopTabView = new transformationView();
            } else if (idOfTab === "tab-receivers") {
                this.currentTopTabView = new receiversView();
            }
            //if (this.currentTopTabView) {
            //    this.listenTo(this.currentTopTabView, "isUnsavedChanged", this.onIsUnsavedChanged);
            //}
        },
        indicateActiveTab: function () {
            this.$el.find("#tab-mappings").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-transform").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-receivers").removeClass("control-row-tab-button-active");

            this.$el.find("#" + window.appSession.syndicationConfigurationTabId).addClass("control-row-tab-button-active");
        },
        render: function () {
            this.$el.html(this.template());
            if (!window.appSession.syndicationConfigurationTabId) {
                window.appSession.syndicationConfigurationTabId = "tab-transform";
            }

            this.createTabView(window.appSession.syndicationConfigurationTabId);
            this.$el.find("#syndication-configuration-main").html(this.currentTopTabView.el);
            return this;
        }
    });

    return configurationView;
});

