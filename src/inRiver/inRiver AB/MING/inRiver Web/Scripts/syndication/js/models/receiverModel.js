﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var receiverModel = backbone.DeepModel.extend({
        idAttribute: "Id", // If set, when save a Update (HttpPut) will occure instead of Save (HttpPost)
        //urlRoot: "/api/syndication/receiver",
        url: "/api/syndication/receiver",
        initialize: function () {
        }
    });

    return receiverModel;

});
