﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/views/workarea/quickSearchView'
], function ($, _, Backbone, quickSearchView) {

    var appHeaderView = Backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            $("#search-container").html(new quickSearchView().el);
            return this;
        }
    });

    return appHeaderView;

});
