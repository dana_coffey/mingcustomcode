define([
  'jquery',
  'underscore',
  'backbone',
  'models/mappingModel'
], function ($, _, Backbone, mappingModel) {
    var mappingCollection = Backbone.Collection.extend({
        model: mappingModel,
        url: '/api/syndication/mapping'
    });

    return mappingCollection;
});
