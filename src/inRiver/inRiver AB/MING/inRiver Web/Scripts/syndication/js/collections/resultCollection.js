define([
  'jquery',
  'underscore',
  'backbone',
  'models/resultModel'
], function ($, _, Backbone, resultModel) {
    var resultCollection = Backbone.Collection.extend({
        model: resultModel,
        url: '/api/syndication/result/max/5'
    });

    return resultCollection;
});
