// Filename: router.js
define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/views/topheader/topHeaderView',
    'views/appheader/appHeaderView',
    'views/start/startPageView',
    'views/configuration/configurationView',
    'sharedjs/views/workarea/workAreaHandler',
    'text!templates/shared/syndicationNavigationTemplate.html'
], function ($, _, backbone, jqueryui, inRiverUtil, topHeaderView, appHeaderView, startPageView, configurationView, workAreaHandler, syndicationNavigationTemplate) {

    var appRouter = backbone.Router.extend({
        routes: {
            // Define some URL routes
            "home": "index",
            "dashboard(/:id)": "dashboard",
            "configuration(/:id)(/:tab)": "configuration",
            "workarea?*queryString": "workarea",
            "workareasearch?*queryString": "workareasearch"
        },
        index: function () {
            this.navigate("dashboard", { trigger: true, replace: true });
        },
        dashboard: function () {
            this.switchView(new startPageView());
        },
        configuration: function(id, tab) {
            if (tab) {
                window.appSession.syndicationConfigurationTabId = "tab-" + tab;
            } else {
                window.appSession.syndicationConfigurationTabId = "tab-mappings";
            }

            this.switchView(new configurationView());
        },
        switchView: function(newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(this._currentView.el);
            if (this._currentView.onInsertedInDOM) {
                this._currentView.onInsertedInDOM();
            }
        },
        closeView: function(specView) {
            if (specView) {
                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function(queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function(el) {

                        var key = el.substr(0, el.indexOf('='));
                        var value = el.substr(el.indexOf('=') + 1);
                        var o = {};
                        o[key] = value;
                        return o;
                    }),
                    function(o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function() {
        console.log("init router");
        window.appHelper.event_bus = _({}).extend(backbone.Events);

        window.appHelper.currentApp = "Syndication";

        // Just needs to initialise the header information
        var headerView = new topHeaderView({ app: "Syndication" });
        var appheaderView = new appHeaderView();

        $("#context-menu-container-left").html(_.template(syndicationNavigationTemplate)());

        window.app_router = new appRouter;
        window.app_router.workAreaHandler = new workAreaHandler({ app: "syndication" });
        window.appHelper.workAreaHandler = window.app_router.workAreaHandler;
        window.app_router.currentViewType = "";

        window.app_router.on('route', function (id, tab) {
            // Show current route in the top header navigation area
            $("#header-navigation-links-wrap a").removeClass("current");
            $("#header-navigation-links-wrap a[href='#" + id + "']").addClass("current");

            if (id !== "workarea") {
                window.app_router.workAreaHandler.closeWorkAreas();
                window.app_router.workAreaHandler.hideWorkareas();
            }
        });

        window.app_router.on('route:workareasearch', function (queryString) {
            window.app_router.navigate("", { trigger: false, replace: true }); // reset to "no route" to make sure other routes work properly (fix for bug 8154)

            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var workarea = window.app_router.workAreaHandler.getActiveWorkarea();

            if (params.advsearch != undefined) {
                window.app_router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
            } else if (params.searchPhrase != undefined) {
                window.app_router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
            } else if (params.compressedEntities != undefined) {
                inRiverUtil.Notify("Entity view are disabled in syndication. Use Enrich to look at entities.");
            }

        });

        window.app_router.on('route:workarea', function (queryString) {
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var rendered = false;


            if (!window.app_router.workAreaHandler.hasWorkareas()) {
                var title = params.title;
                if (title == undefined) {
                    title = "Empty";
                }

                if (params.workareaId != undefined && (params.isQuery != undefined && params.isQuery.toString() === "true")) {
                    window.app_router.workAreaHandler.openSavedQuery({ title: title, workareaId: params.workareaId });
                    rendered = true;
                } else if (params.workareaId != undefined) {
                    window.app_router.workAreaHandler.openSavedWorkarea({ title: title, workareaId: params.workareaId });
                    rendered = true;
                } else {
                    window.app_router.workAreaHandler.reopenOrAddWorkAreas({ title: title, compressedEntities: params.compressedEntities, workareaId: params.workareaId, searchPhrase: params.searchPhrase, advsearchPhrase: advsearchPhrase, hasworkareaSearch: true, isQuery: params.isQuery });
                    rendered = true;
                }

            }
            if (!rendered) {
                if (params.advsearch != undefined) {
                    window.app_router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
                } else if (params.searchPhrase != undefined) {
                    window.app_router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
                } else if (params.compressedEntities != undefined) {
                    window.app_router.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities });
                } else if (params.workareaId != undefined) {
                    if (params.isQuery != undefined && params.isQuery.toString() === "true") {
                        window.app_router.workAreaHandler.openSavedQuery({ title: params.title, workareaId: params.workareaId });
                    } else {
                        window.app_router.workAreaHandler.openSavedWorkarea({ title: params.title, workareaId: params.workareaId });
                    }
                } else {
                    window.app_router.workAreaHandler.renderResults();
                }
            }
            window.app_router.workAreaHandler.setWideWorkareas();
            window.app_router.workAreaHandler.bindEvents();
            this.switchView(window.app_router.workAreaHandler);
            window.app_router.currentViewType = "workarea";
        });

        // Extend the View class to include a navigation method goTo
        backbone.View.prototype.goTo = function(loc, options) {
            if (options && options.trigger != undefined) {
                if (options.trigger === false) {
                    window.app_router.navigate(loc, false);
                    return;
                }
            }

            window.app_router.navigate(loc, true);
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        backbone.View.prototype.close = function() {
            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        if (!Backbone.history.start()) {
            window.app_router.navigate("dashboard", { trigger: true, replace: true });
        }
    };

    // Extend the View class to include a navigation method goTo
    Backbone.View.prototype.goTo = function (loc, changeurl) {
        if (changeurl == undefined) {
            changeurl = { trigger: true };
        }

        // Check for unsaved changes in current view before navigating.
        // When trigger is false it means that the view is actually switched elsewhere
        // and therefore the check for unsaved changes must be handled there instead (only the
        // browser url will be affected when trigger is set to false).
        if (changeurl.trigger) {
            inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(window.app_router._currentView, function () {
                window.app_router.navigate(loc, changeurl);
            });
        } else {
            window.app_router.navigate(loc, changeurl);
        }
    };

    // Extend View with a close method to prevent zombie views (memory leaks)
    Backbone.View.prototype.close = function () {

        this.stopListening();
        this.$el.empty();
        this.unbind();
        if (this.onClose) {
            this.onClose();
        }
    };

    Backbone.View.prototype.renderLoadingSpinner = function () {
        this.$el.html(Backbone.LoadingSpinnerEl);
    };
    return {
        initialize: initialize
    };

});
