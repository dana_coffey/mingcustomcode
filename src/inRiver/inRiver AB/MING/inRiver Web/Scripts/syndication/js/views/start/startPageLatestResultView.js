﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'sharedjs/misc/inRiverUtil',
    'collections/resultCollection',
    'views/cards/latestResultCardView',
    'text!templates/start/startPageLatestResultTemplate.html'
], function ($, _, backbone, backboneforms, jqueryui, inRiverUtil, resultCollection, latestResultCardView, startPageLatestResultTemplate) {

    var startPageLatestResultView = backbone.View.extend({
        template: _.template(startPageLatestResultTemplate),
        initialize: function() {
            var self = this;
            self.render();
        },
        render: function() {
            var self = this;
            this.$el.html(this.template);

            this.collection = new resultCollection();
            this.collection.fetch({
                success: function () {
                    self.$el.find(".loadingspinner").hide();
                    if (self.collection.length === 0) {
                        self.$el.find("#latest-result-overview-dataview").append("<p>No records found...</p>");
                    } else {
                        var that = self;
                        _.each(self.collection.models, function (model) {
                            that.$el.find("#result-list").append(new latestResultCardView(model).el);
                        });
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            return this;
        }
    });

    return startPageLatestResultView;
});