﻿define([
    "jquery",
    "underscore",
    "backbone",
    "backbone-forms",
    "jquery-ui",
    "modalPopup",
    "sharedjs/misc/inRiverUtil",
    /* Templates */
    "text!templates/receivers/createEditReceiverTemplate.html",
    /* Views */
    "views/receivers/syndicationUploadXsdView",
    /* Models */
    "models/receiverModel",
    /* Collections */
    "collections/mappingCollection"
], function ($, _, backbone, backboneforms, jqueryui, modalPopup, inRiverUtil,
    createEditReceiverTemplate,
    syndicationUploadXsdView,
    receiverModel,
    mappingCollection) {

    var createEditReceiverView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            
            this.stopListening(window.appHelper.event_bus);

            this.popupOwner = options.popupOwner;
            
            this.model = new receiverModel();

            if (options.receiverId != undefined) {
                this.receiverId = options.receiverId;
                this.model.fetch({
                    async: false,
                    url: "/api/syndication/receiver/" + options.receiverId,
                    reset: true,
                    success: function() {
                        self.syndicationUploadViewDefaultXsd = new syndicationUploadXsdView({ receiverId: options.receiverId, type: "default" });
                        self.syndicationUploadViewImportXsd = new syndicationUploadXsdView({ receiverId: options.receiverId, type: "import" });
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.model.fetch({
                    async: false,
                    url: "/api/syndication/receiver/initializeEmpty",
                    reset: true,
                    success: function () {
                        self.model.unset("Id");
                        self.model.set("Xsd", "");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            /*
            if (this.model.get("Id") != undefined) {
                this.outgoingProperties.fetch({
                    async: false,
                    url: "/api/syndication/receiver/outgoingProperties/" + self.model.get("Id"),
                    reset: true,
                    success: function() {

                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }*/

            this.render();
        },
        refetchReceiver: function(type) {
            var self = this;
            self.rerender = false;
            this.model.fetch({
                async: false,
                url: "/api/syndication/receiver/" + self.receiverId,
                reset: true,
                success: function () {
                    self.rerender = true;
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            if (self.rerender) {
                self.syndicationUploadViewDefaultXsd.close();
                self.syndicationUploadViewImportXsd.close();
                self.syndicationUploadViewDefaultXsd = new syndicationUploadXsdView({ receiverId: self.receiverId, type: "default" });
                self.syndicationUploadViewImportXsd = new syndicationUploadXsdView({ receiverId: self.receiverId, type: "import" });
                self.render();
            }
        },
        inputReceiverName: function () {
            return this.$el.find("#inputReceiverName");
        },
        save: function () {
            var self = this;

            this.model.set("Name", this.inputReceiverName().val());

            this.model.save({}, {
                async: false, // wait for answer.
                success: function (receiver) {
                    self.receiverId = receiver.get("Id");
                    self.popupOwner.close();
                    inRiverUtil.Notify("Receiver successfully saved");
                    window.appHelper.event_bus.trigger('syndication.receiversView.render');
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var self = this;
            
            //
            //  Stop listen to event_bus triggers.
            //
            this.stopListening(window.appHelper.event_bus);

            this.$el.html(_.template(createEditReceiverTemplate));
            
            this.inputReceiverName().val(this.model.get("Name"));
            
            var uploadXsdContainer = this.$el.find("#syndication-createEdit-container-upload-xsd");
            if (this.syndicationUploadViewDefaultXsd != undefined) {
                uploadXsdContainer.append(this.syndicationUploadViewDefaultXsd.$el);
            } else {
                uploadXsdContainer.css("margin-top", "10px");
                uploadXsdContainer.append("<b>The Receiver must have a name and be saved once before any XSD files may be uploaded.</b>");
            }

            var uploadXsdImportContainer = this.$el.find("#syndication-createEdit-container-upload-xsd-import");
            if (this.syndicationUploadViewImportXsd != undefined) {
                uploadXsdImportContainer.append(this.syndicationUploadViewImportXsd.$el);
            } else {
                uploadXsdImportContainer.css("margin-top", "0px");
            }

            if (this.model.get("Id") != undefined) {
                // Show Outgoing properties and data around them like descriptions.
                var concatedXsdContainer = this.$el.find("#syndication-createEdit-container-concatedXsd");

                if (this.model.get("XsdImport") != "") {
                    concatedXsdContainer.append(document.createTextNode("\nImport Xsd\n------------------------------------------------------------------\n\n" + this.model.get("XsdImport") + "\n\n"));
                }

                concatedXsdContainer.append(document.createTextNode("\nXsd\n------------------------------------------------------------------\n\n" + this.model.get("Xsd")));
            }

            this.listenTo(window.appHelper.event_bus, "syndication.receiversView.xsdUploaded", this.refetchReceiver);
           
            return this;
        }
    });

    return createEditReceiverView;
});
