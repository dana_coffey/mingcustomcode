﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var resultModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/syndication/result/max/5'
    });

    return resultModel;

});
