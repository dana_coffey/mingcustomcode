﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'sharedjs/misc/inRiverUtil',
    /* Templates */
    'text!templates/distributionFormat/createDistributionFormatTemplate.html',
    /* Views */
    "views/distributionFormat/distributionFormatUploadXsltView",
    /* Models */
    'models/distributionFormatModel'
], function ($, _, backbone, backboneforms, jqueryui, inRiverUtil,
    createDistributionFormatTemplate,
    distributionFormatUploadXsltView,
    distributionFormatModel) {

    var createEditDistributionFormatModalPopupView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.popupOwner = options.popupOwner;
            this.receiverId = options.receiverId;

            this.model = new distributionFormatModel();

            if (options.distributionFormatId != undefined) {
                this.model.fetch({
                    async: false,
                    url: "/api/syndication/distributionFormat/" + options.distributionFormatId,
                    reset: true,
                    success: function () {
                        self.distributionFormatUploadXsltView
                            = new distributionFormatUploadXsltView(
                            {
                                receiverId: options.receiverId,
                                distributionFormatId: options.distributionFormatId
                            });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            
            this.render();
        },
        events: {
        },
        inputDistributionFormatName: function() {
            return this.$el.find("#inputDistributionFormatName");
        },
        inputDistributionFormatOutputFileExtension: function () {
            return this.$el.find("#inputDistributionFormatOutputFileExtension");
        },
        inputDistributionFormatContentType: function () {
            return this.$el.find("#inputDistributionFormatContentType");
        },
        save: function() {
            var self = this;

            this.model.set("ReceiverId", this.receiverId);
            this.model.set("Name", this.inputDistributionFormatName().val());
            this.model.set("OutputFileExtension", this.inputDistributionFormatOutputFileExtension().val());
            this.model.set("ContentType", this.inputDistributionFormatContentType().val());

            this.model.save({}, {
                async: false, // wait for answer.
                success: function () {
                    self.popupOwner.close();
                    inRiverUtil.Notify("Distribution format successfully saved");
                    window.appHelper.event_bus.trigger('syndication.receiversView.receiverItemView.renderDistrubtionFormats', self.receiverId);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var self = this;

            this.$el.html(_.template(createDistributionFormatTemplate));

            this.inputDistributionFormatName().val(this.model.get("Name"));

            if (this.distributionFormatUploadXsltView != undefined) {

                var uploadXsltContainer = this.$el.find("#syndication-createEdit-container-upload-xslt");

                uploadXsltContainer.append(this.distributionFormatUploadXsltView.$el);
            }

            return this;
        }
    });

    return createEditDistributionFormatModalPopupView;
});
