﻿define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-steps',
    'dropzone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/receivers/syndicationUploadTemplate.html'
], function (
    $,
    _,
    backbone,
    jquerySteps,
    Dropzone,
    inRiverUtil,
    syndicationUploadTemplate) {

    var syndicationUploadXsdView = backbone.View.extend({
        initialize: function (options) {
            this.receiverId = options.receiverId;
            this.type = options.type;
            this.render();
        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {
            var self = this;
            this.$el.html(syndicationUploadTemplate);

            this.myAwesomeDropzone = new Dropzone(this.$el.find("#media-dropzone")[0], {
                url: "/syndicationFileUpload/xsdFile",
                maxFilesize: 1000,
                parallelUploads: 1,
                thumbnailWidth: 10,
                thumbnailHeight: 10,
                autoProcessQueue: true,
                dictDefaultMessage: "Drop files here or Click to Browse",
                acceptedFiles: ".xsd",
                init: function () {
                    var thisDropzone = this;
                    var entityResourceArray = new Array();

                    this.on("processing", function () {
                        // thisDropzone.options.autoProcessQueue = true;
                    });

                    this.on("drop", function (file) {
                        // handle drop ?
                    });

                    this.on("addedfile", function (file) {
                        // Max one file to upload
                        if (this.files[1] != null) {
                            this.removeFile(this.files[0]);
                        }
                        self.$el.find("#media-dropzone").removeClass("dropzone-button");
                        self.$el.find("#media-dropzone").addClass("dropzone");
                        self.$el.find("#dropzone-buttons").show();
                        self.$el.find("#drop-zone-message").hide();
                    });

                    this.on("success", function (file, res) {
                        console.log("successfully uploaded file");

                        if (res > 0) {
                            entityResourceArray.push(res);
                            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                                setTimeout(function () {
                                    thisDropzone.removeAllFiles();
                                    window.appHelper.event_bus.trigger("syndication.receiversView.xsdUploaded", self.type);
                                }, 500);
                            }
                        } else {
                            file.previewElement.classList.add("dz-error");
                            file.previewElement.classList.remove("dz-success");

                            var ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                            var node;
                            var results = [];
                            for (var i = 0, len = ref.length; i < len; i++) {
                                node = ref[i];
                                results.push(node.textContent = "Filename already exists");
                            }
                            return results;
                        }
                    });

                    this.on("error", function (file, reason) {
                        var text = reason;

                        // Check if error is from server-side (else internal from Dropzone-component)
                        if (file.xhr != undefined) {
                            text = file.xhr.statusText;
                        }

                        inRiverUtil.NotifyError("Error when selecting file", text);
                    });

                    var submitButton = self.$el.find("#start-upload");
                    submitButton.on("click", function (e) {
                        thisDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    });

                    var closeButton = self.$el.find("#close-upload");
                    closeButton.on("click", function (e) {
                        thisDropzone.removeAllFiles();
                        $("#media-dropzone").removeClass("dropzone");
                        $("#media-dropzone").addClass("dropzone-button");
                        $("#dropzone-container").removeClass("bbm-wrapper");
                        $("#drop-zone2").removeClass("bbm-modal");
                        $("#ddrop-zone2 #dropzone-buttons").hide();
                    });

                    var uploadMediaContainer = self.$el.find("#upload-media-container");
                    var dropZoneContainer = self.$el.find("#dropzone-container");
                    var dropZone2 = self.$el.find("#dropzone-container #drop-zone2");
                    var mediaDropZone = self.$el.find("#dropzone-container #drop-zone2 #media-dropzone");
                    uploadMediaContainer.addClass("syndication-upload-xsd-uploadMediaContainer");
                    dropZoneContainer.addClass("syndication-upload-xsd-dropZoneContainer");
                    dropZone2.addClass("syndication-upload-xsd-dropZone2");
                    mediaDropZone.addClass("syndication-upload-xsd-mediaDropZone");

                    dropZone2.height("140px");
                },
                accept: function (file, done) {                    

                    if (!file.name.match(/.xsd$/)) {
                        done("Filename '" + file.name + "' must end with '.xsd'");
                        return;
                    }

                    done();
                }
            });

            this.$el.find("#receiverId").val(this.receiverId);
            this.$el.find("#type").val(this.type);

            return this;
        }
    });

    return syndicationUploadXsdView;
});
