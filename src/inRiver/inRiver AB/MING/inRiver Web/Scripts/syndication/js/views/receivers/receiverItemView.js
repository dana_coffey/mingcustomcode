﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    /* Templates */
    'text!templates/receivers/receiversViewItemCaptionTemplate.html',
    'text!templates/receivers/receiversViewReceiverItemTemplate.html',
    'text!templates/receivers/receiversViewDistributionFormatItemTemplate.html',
    'text!templates/distributionFormat/createDistributionFormatTemplate.html',
    /* Models & Collections */
    'models/receiverModel',
    'collections/receiverCollection',
    'models/distributionFormatModel',
    'collections/distributionFormatCollection',
    /* Views */
    'views/receivers/distributionFormatItemView',
    'views/receivers/createEditReceiverModalPopupView',
    'views/distributionFormat/createEditDistributionFormatModalPopupView',
    'sharedjs/views/contextmenu/contextMenuView'
], function ($, _, backbone, backboneforms, jqueryui, modalPopup, inRiverUtil,
    receiversViewItemCaptionTemplate, receiversViewReceiverItemTemplate, receiversViewDistributionFormatItemTemplate, createDistributionFormatTemplate,
    receiverModel, receiverCollection, distributionFormatModel, distributionFormatCollection,
    distributionFormatItemView, createEditReceiverModalPopupView, createEditDistributionFormatModalPopupView, contextMenuView) {

    var receiversItemView = backbone.View.extend({
        template: _.template(receiversViewReceiverItemTemplate),
        initialize: function(options) {
            var self = this;

            this.receiverItemIdBase = "receiversView-receiverItem";

            this.receiverDistributionFormats = new distributionFormatCollection();

            this.receiverModel = options.receiverModel; // The Receiver-model this View is an instance of.

            this.render();
        },
        events: {
            "click span#deleteReceiver": "ctxDeleteReceiver",
            "click span#addDistributionFormat": "ctxAddDistributionFormat",
            "click span#editReceiver": "ctxEditReceiver",
            "click span#toggleActiveReceiver": "ctxToggleActiveReceiver"
        },
        //
        //  A distribution format for a specific Receiver has been deleted.
        //  Get new list.
        //  
        deletedDistributionFormat: function(distributionFormatModel) {

            var self = this;

            if (distributionFormatModel.get("ReceiverId") != this.receiverModel.get("Id"))
                return;

            var visible = self.receiverModel.get("DistributionFormatsVisible");

            self.receiverDistributionFormats.fetch({
                url: "/api/syndication/distributionFormat/byReceiver/" + self.receiverModel.get("Id"),
                async: false,
                reset: true,
                success: function () {

                    self.receiverModel.set("DistributionFormatsVisible", visible);
                    self.receiverModel.set("DistributionFormats", self.receiverDistributionFormats.toJSON());

                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        ctxToggleActiveReceiver: function() {
            var self = this;

            this.receiverModel.fetch({
                async: false,
                reset: true,
                success: function () {
                    
                    var isActive = self.receiverModel.get("IsActive");
                    if (isActive == true) {
                        self.receiverModel.set("IsActive", false);
                    } else {
                        self.receiverModel.set("IsActive", true);
                    }

                    self.receiverModel.save(null, {
                        async: false,
                        success: function () {
                            self.render();
                        }
                    });
                }
            });
        },
        ctxDeleteReceiver: function (e) {
            e.stopPropagation();
        },
        ctxEditReceiver: function(e) {
            var pop = new modalPopup();
            pop.popOut(
                new createEditReceiverModalPopupView({
                    callbackContext: this,
                    popupOwner: pop,
                    receiverId: this.receiverModel.get("Id")
                }),
                {
                    size: "medium",
                    header: "Edit a Receiver",
                    usesavebutton: true,
                    onSave: this.onSaveReceiver,
                    onCancel: function (popup) {
                        popup.close();
                    }
                });
            $("#save-form-button").removeAttr("disabled");
        },
        ctxAddDistributionFormat: function (e) {
            e.stopPropagation();

            var pop = new modalPopup();
            pop.popOut(
                new createEditDistributionFormatModalPopupView({
                    callbackContext: this,
                    popupOwner: pop,
                    receiverId: this.receiverModel.get("Id")
                }),
                {
                    size: "medium",
                    header: "Add distribution format to receiver",
                    usesavebutton: true,
                    onSave: this.onSaveDistributionFormat,
                    onCancel: function (popup) {
                        popup.close();
                    }
                });
            $("#save-form-button").removeAttr("disabled");
        },
        onSaveDistributionFormat: function (popup, subView) {
            subView.save();
        },
        onSaveReceiver: function (popup, subView) {
            subView.save();
        },
        onClickShowDistributionFormats: function (forcedReload) {
            var self = this;
            this.forcedReload = false;

            if (forcedReload != undefined) {
                this.forcedReload = forcedReload;
            }

            if (!self.receiverModel.get("DistributionFormatsVisible") || this.forcedReload) {

                self.receiverDistributionFormats.fetch({
                    url: "/api/syndication/distributionFormat/byReceiver/" + self.receiverModel.get("Id"),
                    reset: true,
                    success: function () {

                        if (!self.forcedReload) {
                            self.receiverModel.set("DistributionFormatsVisible", true);
                        }

                        self.receiverModel.set("DistributionFormats", self.receiverDistributionFormats.toJSON());

                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.receiverModel.set("DistributionFormatsVisible", false);
                this.receiverModel.set("DistributionFormats", $([])); // Empty collection

                self.render();
            }
        },
        refetchReceiverDistributionFormats: function(receiverId) {
            // Check that the event is for us.
            if (this.receiverModel.get("Id") != receiverId)
                return;

            this.onClickShowDistributionFormats(true);
        },
        render: function () {
            var self = this;

            //
            //  Stop listen to event_bus triggers.
            //
            this.stopListening(window.appHelper.event_bus);

            //
            //  Render base template for view.
            //
            var receiverItemTemplate = _.template(receiversViewReceiverItemTemplate, {
                receiverId: self.receiverItemIdBase + self.receiverModel.get("Id"),
                header: "",
                name: self.receiverModel.get("Name"),
                isActive: self.receiverModel.get("IsActive"),
                isValidated: self.receiverModel.get("IsValidated")
            });

            this.$el.html(receiverItemTemplate);

            var recieverItemEl = this.$el.find("#" + self.receiverItemIdBase + self.receiverModel.get("Id"));

            //
            //  Add Context-Menu.
            //
            var tools = new Array();
            // tools.push({ name: "deleteReceiver", title: "Open Workarea", icon: "fa fa-folder-open-o", text: "Open" });
            tools.push({ name: "addDistributionFormat", title: "Add Distribution Format", icon: "fa fa-folder-open-o", text: "Add distribution format" });
            tools.push({ name: "editReceiver", title: "Edit Receiver", icon: "fa fa-folder-open-o", text: "Edit" });

            if (this.receiverModel.get("IsValidated") == true) {
                if (this.receiverModel.get("IsActive") == true) {
                    tools.push({ name: "toggleActiveReceiver", title: "Deactivate receiver", icon: "fa fa-folder-open-o", text: "Deactivate" });
                } else {
                    tools.push({ name: "toggleActiveReceiver", title: "Activate receiver", icon: "fa fa-folder-open-o", text: "Activate" });
                }
            }

            recieverItemEl.find("#contextmenu").html(new contextMenuView({
                id: 0,
                tools: tools
            }).$el);

            //
            //  Add Distribution-formats associated to the Receiver.
            // 
            var distributionFormatsItemEl = recieverItemEl.find("#receiversView-receiverItem-distributionFormats");

            var showCaption = "[+]Distribution formats";
            if (self.receiverModel.get("DistributionFormatsVisible")) {
                showCaption = "[-]Distribution formats";
            }

            distributionFormatsItemEl.append(
                _.template(receiversViewItemCaptionTemplate, {
                    caption: showCaption
                })
            );

            var distributionFormatHeaderEl = distributionFormatsItemEl.find("#receiversView-Item-caption");

            distributionFormatHeaderEl.click(function () {
                self.onClickShowDistributionFormats();
            });

            distributionFormatHeaderEl.mouseover(function () {
                $(this).addClass("receiversView-mouse-hand");
            });

            if (self.receiverModel.get("DistributionFormatsVisible")) {

                $.each(self.receiverModel.get("DistributionFormats"), // JSON-collection
                    function (index, model) {

                        var distributionFormatItemViewInstance = new distributionFormatItemView({ distributionFormatId: model.Id });

                        distributionFormatsItemEl.append(distributionFormatItemViewInstance.$el);
                    });
            }

            //
            //  Start listen to event_bus triggers again.
            //
            this.listenTo(window.appHelper.event_bus, "deletedDistributionFormat", this.deletedDistributionFormat);
            this.listenTo(window.appHelper.event_bus, "syndication.receiversView.receiverItemView.renderDistrubtionFormats", this.refetchReceiverDistributionFormats);

            return this; // enable chained calls
        }
    });

    return receiversItemView;
});