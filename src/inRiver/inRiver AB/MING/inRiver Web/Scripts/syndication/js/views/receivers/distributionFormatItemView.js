﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    /* Templates */
    'text!templates/receivers/receiversViewItemCaptionTemplate.html',
    'text!templates/receivers/receiversViewReceiverItemTemplate.html',
    'text!templates/receivers/receiversViewDistributionFormatItemTemplate.html',
    /* Models & Collections */
    'models/receiverModel',
    'collections/receiverCollection',
    'models/distributionFormatModel',
    'collections/distributionFormatCollection',
    /* Views */
    'views/distributionFormat/createEditDistributionFormatModalPopupView',
    'sharedjs/views/contextmenu/contextMenuView'
], function ($, _, backbone, backboneforms, jqueryui, modalPopup, inRiverUtil,
    receiversViewItemCaptionTemplate, receiversViewReceiverItemTemplate, receiversViewDistributionFormatItemTemplate,
    receiverModel, receiverCollection, distributionFormatModel, distributionFormatCollection,
    createEditDistributionFormatModalPopupView, contextMenuView) {

    var distributionFormatItemView = backbone.View.extend({
        template: _.template(receiversViewReceiverItemTemplate),
        initialize: function (options) {
            var self = this;

            self.distributionFormatItemIdBase = "distributionFormatItemIdBase";

            self.distributionFormatId = options.distributionFormatId;

            self.distributionFormatModel = new distributionFormatModel();

            self.distributionFormatModel.fetch({
                url: "/api/syndication/distributionFormat/" + self.distributionFormatId,
                success: function () {

                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click span#deleteDistributionFormat": "ctxDeleteDistributionFormat",
            "click span#editDistributionFormat": "ctxEditDistributionFormat"
        },
        ctxEditDistributionFormat: function(e) {
            var self = this;

            e.stopPropagation();

            var pop = new modalPopup();
            pop.popOut(
                new createEditDistributionFormatModalPopupView({
                    callbackContext: this,
                    popupOwner: pop,
                    receiverId: this.distributionFormatModel.get("ReceiverId"),
                    distributionFormatId: this.distributionFormatModel.get("Id")
                }),
                {
                    size: "medium",
                    header: "Edit distribution format for receiver",
                    usesavebutton: true,
                    onSave: this.onSaveDistributionFormat,
                    onCancel: function (popup) {
                        popup.close();
                    }
                });
            $("#save-form-button").removeAttr("disabled");
        },
        onSaveDistributionFormat: function (popup, subView) {
            subView.save();
        },
        ctxDeleteDistributionFormat: function (e) {
            var self = this;

            e.stopPropagation();

            this.distributionFormatModel.destroy({
                success: function (model, response) {

                    if (response == true) {
                        // Tell parent Receiver-view that the distributionFormat has been deleted.
                        window.appHelper.event_bus.trigger('deletedDistributionFormat', self.distributionFormatModel);
                    } 
                }
            });
        },
        render: function() {
            var self = this;

            self.$el.html(
                _.template(receiversViewDistributionFormatItemTemplate, {
                    distributionFormatId: self.distributionFormatItemIdBase + self.distributionFormatModel.get("Id"),
                    header: "",
                    name: self.distributionFormatModel.get("Name")
                })
            );

            var distributionFormatItemEl = this.$el.find("#" + self.distributionFormatItemIdBase + self.distributionFormatModel.get("Id"));

            //
            //  Add Context-Menu.
            //
            var tools = new Array();
            tools.push({ name: "deleteDistributionFormat", title: "Delete distribution format", icon: "fa fa-folder-open-o", text: "Delete" });
            tools.push({ name: "editDistributionFormat", title: "Edit distribution format", icon: "fa fa-folder-open-o", text: "Edit" });

            distributionFormatItemEl.find("#contextmenu").html(new contextMenuView({
                id: 0,
                tools: tools
            }).$el);

            return this;
        }
    });

    return distributionFormatItemView;
});
