// Filename: app.js
define([
  'jquery',
  'underscore',
  'backbone',
  'router', // Request router.js
  'alertify'
  
], function ($, _, Backbone, Router, alertify) {
    var initialize = function () {
        // Pass in our Router module and call it's initialize function
        Router.initialize();
        initApp();
    };

    function initApp() {
      
        alertify.set({ buttonReverse: true });
    }

    return {
        initialize: initialize
    };
});