﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var mappingModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/syndication/mapping'
    });

    return mappingModel;

});
