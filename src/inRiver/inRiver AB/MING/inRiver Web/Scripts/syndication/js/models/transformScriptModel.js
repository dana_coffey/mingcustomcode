﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var transformScriptModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/syndication/transformscript'
    });

    return transformScriptModel;

});
