﻿define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms',
    'jquery-ui',
    'sharedjs/misc/inRiverUtil',
    'text!templates/cards/latestResultCardTemplate.html'
], function ($, _, backbone, backboneforms, jqueryui, inRiverUtil, latestResultCardTemplate) {

    var latestSyndicationCardView = backbone.View.extend({
        template: _.template(latestResultCardTemplate),
        initialize: function (model) {
            this.model = model;
            this.render();
        },
        render: function () {
            this.$el.html(this.template({ data: this.model.toJSON() }));

            return this;
        }
    });

    return latestSyndicationCardView;
});