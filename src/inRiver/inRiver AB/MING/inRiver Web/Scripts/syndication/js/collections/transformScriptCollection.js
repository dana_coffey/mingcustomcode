define([
  'jquery',
  'underscore',
  'backbone',
  'models/transformScriptModel'
], function ($, _, Backbone, transformScriptModel) {
    var transformScriptCollection = Backbone.Collection.extend({
        model: transformScriptModel,
        url: '/api/syndication/transformscript'
    });

    return transformScriptCollection;
});
