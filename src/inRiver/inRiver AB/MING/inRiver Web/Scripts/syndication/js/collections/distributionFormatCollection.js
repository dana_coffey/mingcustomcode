﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/distributionFormatModel'
], function ($, _, backbone, distributionFormatModel) {
    var distributionFormatCollection = backbone.Collection.extend({
        model: distributionFormatModel,
        url: '/api/syndication/distributionFormat'
    });

    return distributionFormatCollection;
});

