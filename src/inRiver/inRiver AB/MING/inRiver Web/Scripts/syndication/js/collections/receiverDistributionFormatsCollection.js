﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/receiverDistributionFormatModel'
], function ($, _, backbone, distributionFormatModel) {
    var receiverDistributionFormatsCollection = backbone.Collection.extend({
        model: distributionFormatModel
    });

    return receiverDistributionFormatsCollection;
});
