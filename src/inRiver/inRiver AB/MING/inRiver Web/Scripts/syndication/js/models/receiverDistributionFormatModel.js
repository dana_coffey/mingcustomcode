﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var receiverDistributionFormatModel = backbone.DeepModel.extend({
        idAttribute: "Id", // If set, when save a Update (HttpPut) will occure instead of Save (HttpPost)
        initialize: function () {
        },
        urlRoot: '/api/syndication/distributionFormat/byReceiver'
    });

    return receiverDistributionFormatModel;

});
