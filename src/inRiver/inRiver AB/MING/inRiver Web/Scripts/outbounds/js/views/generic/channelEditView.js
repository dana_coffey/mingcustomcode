﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/connector/connectorSettingModel',
    'outboundjs/collections/settingsCollection',
    'outboundjs/collections/channelCollection',
    'text!outboundtemplates/generic/channelEditTemplate.html'
], function ($, _, backbone, inRiverUtil, settingsModel, settingsCollection, channelCollection, channelEditTemplate) {

    var channelEditView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            
            this.template = _.template(channelEditTemplate);

            this.connectorId = options.connectorId;
            var settings = new settingsCollection({ id: this.connectorId });
            settings.fetch({
                success: function(result) {
                    var channelSetting = _.find(result.models, function(model) {
                        return model.get("Key") == "CHANNEL_ID";
                    });
                    if (channelSetting == null || channelSetting.get("Value") == null) {
                        // Fallback
                        inRiverUtil.NotifyError("No channel");
                        that.channelId = "123";
                    } else {
                        that.channelId = channelSetting.get("Value");
                    }
                    that.onInit();
                }
            });

            this.allChannels = new channelCollection();
            this.allChannels.fetch({
                success: function () {
                    that.onInit();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ entityType: "Channel", sortStarredDirection: "DESC" })
        });
        },
        events: {
            "click #publish-channel": "onPublishChannel",
            "click #unpublish-channel": "onUnpublishChannel",
            "change #channel-list": "onChangeChannel"
        },
        onInit: function () {
            if (this.channelId == null || this.allChannels.length == 0) {
                return;
            }

            this.render();
            this.updateStatus();
        },
        onPublishChannel: function () {
            this.changePublished(true);
        },
        onUnpublishChannel: function () {
            this.changePublished(false);
        },
        onChangeChannel: function(e) {
            var selectedId = e.currentTarget.value;
            if (selectedId == null) {
                inRiverUtil.NotifyError("Error in selection.", "Failed to retrieve the selected item.");
                return;
            }

            _.each(this.settings, function(setting) {
                if (setting.Key == "CHANNEL_ID") {
                    setting.Value = selectedId;
                }
            });

            this.saveOrUpdateConnectorSettings("CHANNEL_ID", selectedId);
        },
        saveOrUpdateConnectorSettings: function (setting, value) {
            var that = this;
            var settings = new settingsCollection({ id: this.connectorId });
            settings.fetch({
                success: function (result) {
                    that.channelId = value;
                    var channelSetting = _.find(result.models, function (model) {
                        return model.get("Key") == setting;
                    });

                    channelSetting.set("Value", value);
                    channelSetting.save(null, {
                        success: function () {
                            inRiverUtil.Notify("Channel has been updated");
                            that.render();
                            that.updateStatus();
                        },
                        error: function (error, response) {
                            inRiverUtil.OnErrors(error, response);
                        }
                    });
                },
                error: function (result, response) {
                    inRiverUtil.OnErrors(result, response);
                }
            });
        },
        changePublished: function (publish) {
            var url;
            if (publish) {
                url = "/api/channelaction/publish/" + this.channelId;
            } else {
                url = "/api/channelaction/unpublish/" + this.channelId;
            }

            var that = this;
            $.ajax({
                url: url
            }).fail(function (model, response) {
                inRiverUtil.OnErrors(model, response);
            }).always(function () {
                var currentChannel = $.grep(that.allChannels.models, function (channel) {
                    return channel.get("Id") == that.channelId;
                });

                if (currentChannel[0].length == 0) {
                    inRiverUtil.NotifyError("Error for channel.", "Failed to find the specific channel from server.");
                    return;
                }

                var self = that;

                currentChannel[0].set("ChannelPublished", publish);

                currentChannel[0].fetch({
                    success: function () {
                        self.updateStatus();
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            });
        },
        updateStatus: function () {
            var that = this;
            var currentChannel = $.grep(this.allChannels.models, function (channel) {
                return channel.get("Id") == that.channelId;
            });
            
            if (currentChannel.length == 0) {
                inRiverUtil.NotifyError("Channel is missing.", "Either is the channel not set, or the previous selected channel has been removed.<br/><br/>Please change to a valid channel.");
                return;
            }

            if (currentChannel[0].get("ChannelPublished")) {
                this.$el.find("#publish-channel").css('display', 'none');
                this.$el.find("#unpublish-channel").css('display', 'inline');
            } else {
                this.$el.find("#publish-channel").css('display', 'inline');
                this.$el.find("#unpublish-channel").css('display', 'none');
            }

            //if (currentChannel[0].get("ChannelPublished")) {
            //    this.$el.find("#publish-channel").attr('disabled', 'disabled');
            //    this.$el.find("#unpublish-channel").removeAttr('disabled');
            //} else {
            //    this.$el.find("#publish-channel").removeAttr('disabled');
            //    this.$el.find("#unpublish-channel").attr('disabled', 'disabled');
            //}
        },
        render: function () {
            this.$el.html(channelEditTemplate);

            var that = this;
            _.each(this.allChannels.models, function (channel) {
                if (channel.get("Id") == that.channelId) {
                    that.$el.find("#channel-list").append($("<option selected></option>").attr("value", channel.get("Id")).text(channel.get("DisplayName")));
                } else {
                    that.$el.find("#channel-list").append($("<option></option>").attr("value", channel.get("Id")).text(channel.get("DisplayName")));
                }
            });
        }
    });

    return channelEditView;
});