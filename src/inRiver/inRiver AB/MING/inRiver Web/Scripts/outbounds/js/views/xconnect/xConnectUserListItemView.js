﻿define([
        'jquery',
        'underscore',
        'backbone',
        'modalPopup',
        'sharedjs/misc/inRiverUtil',
        'outboundjs/views/xconnect/xConnectUserEditView',
        'text!outboundtemplates/xconnect/xConnectUserListItemTemplate.html'
], function ($, _, backbone, modalPopup, inRiverUtil, xConnectUserEditView, xConnectUserListItemTemplate) {

    var xConnectUserListItemView = backbone.View.extend({
        tagName: "li",
        initialize: function(options) {
            this.template = _.template(xConnectUserListItemTemplate),
                this.model = options.model;
            this.collection = options.collection;
            this.model.on('change', this.render, this);
            this.render();
        },
        events: {
            "click #list-item-edit": "onEditUser",
        },
        openEditUserPopup: function (model) {
            var that = this;
            var pop = new modalPopup();
            pop.popOut(new xConnectUserEditView({
                model: model, collection: that.collection
            }), { size: "small", header: "Edit User", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        onEditUser: function() {
            this.openEditUserPopup(this.model);
        },
        render: function() {
            this.$el.html(this.template({ data: this.model.toJSON() }));
        }
    });

    return xConnectUserListItemView;
});
