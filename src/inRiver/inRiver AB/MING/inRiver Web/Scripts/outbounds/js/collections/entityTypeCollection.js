﻿define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, backbone) {
    var entityTypeCollection = backbone.Collection.extend({
        url: '/api/entitytype'
    });

    return entityTypeCollection;
});
