﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'outboundjs/misc/connectorRouting',
    'outboundjs/views/xconnect/xConnectView',
    'text!outboundtemplates/main/mainConfigurationTemplate.html'
], function ($, _, backbone, inRiverUtil, connectorRouting, xConnectView, mainConfigurationTemplate) {

    var xConnectConfigurationView = backbone.View.extend({
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(mainConfigurationTemplate);
            this.$el.find("#connectorConfiguration").append(connectorRouting.getConfigurationView(this.model.get("TypeName"), this.model).el);
        }
    });

    return xConnectConfigurationView;
});