﻿define([
    'jquery',
    'underscore',
    'backbone',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'outboundjs/models/xConnectUserModel',
    'outboundjs/collections/xConnectUserCollection',
    'outboundjs/views/xconnect/xConnectUserListItemView',
    'outboundjs/views/xconnect/xConnectUserEditView',
    'text!outboundtemplates/xconnect/xConnectUserTemplate.html'
], function ($, _, backbone, modalPopup, inRiverUtil, xConnectUserModel, xConnectUserCollection, xConnectUserListItemView, xConnectUserEditView, xConnectUserTemplate) {
    var xConnectUserView = backbone.View.extend({
        initialize: function (options) {
            var that = this;

            this.connectorId = options.connectorId;
            this.listenTo(window.appHelper.event_bus, 'user-collection-updated', this.render);
            this.userCollection = new xConnectUserCollection();
            this.userCollection.comparator = function(model) {
                return model.get("email").toLowerCase();
            }
            this.userCollection.fetch({
                success: function() {
                    that.render();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        events: {
            "click #list-of-users-add": "onAddUser"
        },
        openEditUserPopup: function (model) {
            var that = this;
            var pop = new modalPopup();
            pop.popOut(new xConnectUserEditView({
                model: model, collection: that.userCollection
            }), { size: "small", header: "Edit User", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        onAddUser: function () {
            var model = new xConnectUserModel();
            this.openEditUserPopup(model);
        },
        render: function () {
            var that = this;
            this.$el.html(xConnectUserTemplate);

            var ulEl = this.$el.find("#xconnect-user-list-wrap > ul");
            ulEl.empty();
            this.userCollection.sort();
            this.userCollection.each(function (model) {
                var userItemView = new xConnectUserListItemView({ model: model, collection: that.userCollection });
                ulEl.append(userItemView.el);
            });
        }
    });

    return xConnectUserView;
});