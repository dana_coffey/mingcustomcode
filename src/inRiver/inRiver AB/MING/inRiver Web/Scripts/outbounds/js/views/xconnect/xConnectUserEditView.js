﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!outboundtemplates/xconnect/xConnectUserEditTemplate.html'
], function ($, _, backbone, inRiverUtil, xConnectUserEditTemplate) {
    var xConnectUserEditView = backbone.View.extend({
        initialize: function (options) {
            this.model = options.model;
            this.userCollection = options.collection;

            this.template = _.template(xConnectUserEditTemplate);

            this.listenTo(window.appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(window.appHelper.event_bus, 'popupsave', this.onSave);

            this.render();
            if (this.model.isNew()) {
                this.doGenerateNewApiKey(this);
                this.$el.find("#reset-password").hide();
                this.$el.find("#delete-user").hide();
            }
        },
        events: {
            "click #generate-api-key": "onGenerateNewApiKey",
            "click #reset-password": "onResetPassword",
            "click #delete-user": "onDeleteUser",
        },
        onGenerateNewApiKey: function (e) {
            e.stopPropagation();
            inRiverUtil.NotifyConfirm("Confirm revoke api key", "Are you sure you want to revoke this api key?<br><br>Revoking an api key will break all usages of the inRiver Cross Connect API for this user. If an api key is revoked a new one will be generated.", this.doGenerateNewApiKey, this);
        },
        doGenerateNewApiKey: function (that) {
            var tmpModel = new backbone.Model();
            tmpModel.urlRoot = "/api/xconnect/newapikey";
            tmpModel.fetch({
                success: function () {
                    that.$el.find("#api-key").val(tmpModel.get("apiKey"));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onResetPassword: function (e) {
            e.stopPropagation();

            var that = this;
            inRiverUtil.NotifyConfirm("Reset Password", "Are you sure you want to reset the password for this user?<br><br>The user will not be able to login without setting a new password.", function () {
                that.doPasswordReset(that.model, "Password was reset! Now ask the user to set a new password by using this url:", function () { });
            }, this);
        },
        onDeleteUser: function (e) {
            e.stopPropagation();

            var that = this;
            inRiverUtil.NotifyConfirm("Delete User", "Are you sure you want to delete this user?<br><br>The user will not be able to use any inRiver Cross Connect features at all.", function () {
                that.model.destroy({
                    success: function () {
                        window.appHelper.event_bus.trigger('user-collection-updated');
                        that.done();
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }, this);
        },
        onSave: function () {
            var that = this;
            var errors = this.form.commit();
            if (errors != null) {
                return;
            }

            this.model.set({ apiKey: this.$el.find("#api-key").val() });

            var isNew = this.model.isNew();
            this.model.save(null, {
                success: function () {
                    if (isNew) {
                        that.userCollection.add(that.model);
                        window.appHelper.event_bus.trigger('user-collection-updated');
                        that.doPasswordReset(that.model, "User created! Now ask the user to set a password by using this url:", function () {
                            that.done();
                        });
                    } else {
                        that.done();
                    }
                },
                error: function (model, response) {
                    console.log(response.statusText);
                    self.$el.find("#edit-user-area").append('<div class="callbackerror">' + response.statusText + '</div>');
                }
            });
        },
        doPasswordReset: function (model, successMsg, successFn) {
            var tmpModel = new backbone.Model({}, {
                urlRoot: model.urlRoot + "/" + model.get("id") + "/passwordreset"
            });
            var that = this;
            tmpModel.fetch({
                method: "POST",
                success: function () {
                    var url = tmpModel.get("passwordResetUrl");
                    inRiverUtil.NotifyConfirm("User", successMsg + "<br><br><input type='text' size='58' value='" + url + "'>", function () { successFn(); }, this);
                    that.done();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            window.appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {
            this.$el.html(this.template({ data: this.model.toJSON() }));

            this.model.schema = {
                email: { validators: ['required', 'email'] },
                firstName: { title: 'First Name' },
                lastName: { title: 'Last Name' },
            };

            // Render user info form
            this.form = new Backbone.Form({
                model: this.model
            }).render();
            this.$el.find("#form-container").html(this.form.el);
        }
    });

    return xConnectUserEditView;
});