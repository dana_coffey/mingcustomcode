define([
  'underscore',
  'backbone'
], function(_, backbone) {
    
    var xConnectUserModel = backbone.Model.extend({
        urlRoot: "/api/xconnect/user",
        initialize: function () {
        },
    });

    return xConnectUserModel;

});
