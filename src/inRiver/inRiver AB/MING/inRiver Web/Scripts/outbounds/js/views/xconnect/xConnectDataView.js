﻿define([
    'jquery',
    'underscore',
    'backbone',
    'multiple-select',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'outboundjs/collections/entityTypeCollection',
    'outboundjs/collections/languageCollection',
    'outboundjs/collections/imageConfigurationCollection',
    'outboundjs/collections/settingsCollection',
    'outboundjs/views/xconnect/xConnectNameMappingView',
    'text!outboundtemplates/xconnect/xConnectDataTemplate.html'
], function ($, _, backbone, multipleSelect, modalPopup, inRiverUtil, entityTypeCollection, languageCollection, imageConfigurationCollection, settingsCollection, xConnectNameMappingView, xConnectDataTemplate) {

    var crossConnectDataView = backbone.View.extend({
        initialize: function(options) {
            var that = this;
            this.template = _.template(xConnectDataTemplate);
            this.render();
            this.connectorId = options.connectorId;

            this.connectorSettingsCollection = new settingsCollection({ id: this.connectorId });
            this.allEntityTypesCollection = new entityTypeCollection();
            this.allLanguagesCollection = new languageCollection();
            this.allImageConfigurationsCollection = new imageConfigurationCollection();
            this.allLinkTypesCollection = new Backbone.Collection(null, {
                url: "/api/linktype",
                model: Backbone.Model.extend({
                    idAttribute: "Id",
                    toString: function () { return this.get("SourceEntityTypeId") + " -> " + this.get("TargetEntityTypeId") + " (" + this.get("Id") + ")"; }
                })
            });

            this.connectorSettingsCollection.fetch({
                success: function () {
                    that.onInit();
                },
                error: this.onError
            });

            this.allEntityTypesCollection.fetch({
                success: function () {
                    that.onInit();
                },
                error: this.onError
            });
            this.allLanguagesCollection.fetch({
                success: function() {
                    that.onInit();
                },
                error: this.onError
            });
            this.allImageConfigurationsCollection.fetch({
                success: function () {
                    that.onInit();
                },
                error: this.onError
            });

            this.allLinkTypesCollection.fetch({
                reset: true,
                success: function () {
                    that.onInit();
                },
                error: this.onError
            });
            
            this.listenTo(window.appHelper.event_bus, 'connectorsettingsupdated', this.onSettingsUpdated);
        },
        events: {
            "change #edit-configure-excluded-field-types-filter": "updateExcludedFieldTypesDropdown",
            "click #edit-configure-mappings": "onEditMappings",
            "click #restart-connector": "onRestartConnector",
            "change #edit-configure-apikey": "updateApiKey",
            "change #edit-configure-resturl": "updateRestUrl"
        },
        onSettingsUpdated: function () {
            var that = this;
            this.connectorSettingsCollection = new settingsCollection({ id: this.connectorId });
            this.connectorSettingsCollection.fetch({
                reset: true,
                success: function () {
                    that.onInit();
                },
                error: function(result, response) {
                    inRiverUtil.OnErrors(result, response);
                }
            });
        },
        onError: function(model, response) {
            inRiverUtil.OnErrors(model, response);
        },
        onInit: function() {
            // Perform final init when all necessary collections are loaded
            if (this.connectorSettingsCollection.length <= 1 || this.allImageConfigurationsCollection.length == 0 ||
                this.allEntityTypesCollection.length == 0 || this.allLanguagesCollection.length == 0) {
                return;
            }

            var that = this;

            // Excluded Entity Types
            this.$el.find('#edit-configure-excluded-types').empty();
            this.allEntityTypesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Id"))
                    .text(t.get("DisplayName"));
                that.$el.find('#edit-configure-excluded-types').append(optionEl);
            });
            this.excludedEntityTypesWidget = this.$el.find('#edit-configure-excluded-types').multipleSelect({
                placeholder: "No entity types excluded",
                selectAll: false,
                onClick: function() {
                    that.updateExcludedEntityTypesFromDropdown();
                }
            });
            if (this.isValidConnectorSetting("EXCLUDED_ENTITY_TYPES")) {
                this.excludedEntityTypesWidget.multipleSelect("setSelects", JSON.parse(this.getConnectorSettingValue("EXCLUDED_ENTITY_TYPES")));
            }

            // Excluded Field Types
            this.excludedFieldTypesWidget = this.$el.find('#edit-configure-excluded-field-types').multipleSelect({
                placeholder: "No field types excluded",
                selectAll: false,
                onClick: function() {
                    that.updateExcludedFieldTypesFromDropdown();
                }
            });
            this.updateExcludedFieldTypesFilter();

            // Excluded Link Types
            this.$el.find('#edit-configure-excluded-link-types').empty();
            this.allLinkTypesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Id"))
                    .text(t.toString());
                that.$el.find('#edit-configure-excluded-link-types').append(optionEl);
            });
            this.excludedLinkTypesWidget = this.$el.find('#edit-configure-excluded-link-types').multipleSelect({
                placeholder: "No link types excluded",
                selectAll: false,
                onClick: function () {
                    that.updateExcludedLinkTypesFromDropdown();
                }
            });
            if (this.isValidConnectorSetting("EXCLUDED_LINK_TYPES")) {
                this.excludedLinkTypesWidget.multipleSelect("setSelects", JSON.parse(this.getConnectorSettingValue("EXCLUDED_LINK_TYPES")));
            }

            // Languages
            this.$el.find('#edit-configure-languages').empty();
            this.allLanguagesCollection.each(function(t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Name"))
                    .text(t.get("DisplayName"));
                that.$el.find('#edit-configure-languages').append(optionEl);
            });
            this.languagesWidget = this.$el.find('#edit-configure-languages').multipleSelect({
                selectAll: false,
                onClick: function() {
                    that.updateLanguagesFromDropdown();
                }
            });

            if (!this.isValidConnectorSetting("LANGUAGE_CONFIGURATION")) {
                // Select all by default
                this.saveOrUpdateConnectorSettings("LANGUAGE_CONFIGURATION", JSON.stringify(this.allLanguagesCollection.pluck("Name")));
            }
            var langJson = JSON.parse(this.getConnectorSettingValue("LANGUAGE_CONFIGURATION"));
            this.languagesWidget.multipleSelect("setSelects", langJson);

            this.$el.find("#edit-configure-resturl").val(this.getConnectorSettingValue("URL"));

            this.$el.find("#edit-configure-apikey").val(this.getConnectorSettingValue("MANAGEMENT_APIKEY"));

            // Resource Configuration
            if (this.getConnectorSettingValue("RESOURCE_CONFIGURATION") == null) {
                this.saveOrUpdateConnectorSettings("RESOURCE_CONFIGURATION", this.allImageConfigurationsCollection.first().get("Id"));
            }

            this.imageConfigurationForm = new Backbone.Form({
                model: this.getConnectorSettingsModel("RESOURCE_CONFIGURATION"),
                schema: {
                    Value: { title: "Resource Configuration", type: 'Select', options: this.allImageConfigurationsCollection.pluck("Id") },
                }
            }).render();
            this.imageConfigurationForm.on("change", function() {
                this.commit();
                this.model.save();
                inRiverUtil.Notify("Setting has been updated");
            });
            this.$el.find("#edit-configure-resource-configuration-form-container").html(this.imageConfigurationForm.el);
        },
        getNonExcludedEntityTypes: function() {
            var excludedEntityTypes = [];
            if (this.isValidConnectorSetting("EXCLUDED_ENTITY_TYPES")) {
                excludedEntityTypes = JSON.parse(this.getConnectorSettingValue("EXCLUDED_ENTITY_TYPES"));
            }

            // Get a list of entity types where the excluded types are removed
            var nonExcludedEntityTypes = this.allEntityTypesCollection.reject(function(v) {
                return _.contains(excludedEntityTypes, v.get("Id"));
            });

            return nonExcludedEntityTypes;
        },
        getNonExcludedFieldTypes: function(entityType, allFieldTypesForEntityTypeCollection) {
            var excludedFieldTypes = [];
            if (this.isValidConnectorSetting("EXCLUDED_FIELD_TYPES")) {
                var tmp = JSON.parse(this.getConnectorSettingValue("EXCLUDED_FIELD_TYPES"));
                if (tmp[entityType]) {
                    excludedFieldTypes = tmp[entityType];
                }
            }

            // Get a list of entity types where the excluded types are removed
            var nonExcludedFieldTypes = allFieldTypesForEntityTypeCollection.reject(function(v) {
                return _.contains(excludedFieldTypes, v.get("id"));
            });

            return nonExcludedFieldTypes;
        },
        updateExcludedFieldTypesFilter: function(m) {
            var that = this;
            if (m == undefined || m.get("Key") == "EXCLUDED_ENTITY_TYPES") { // m will be a model if we got called from a collection event

                var nonExcludedEntityTypes = this.getNonExcludedEntityTypes();

                // Populate the filter dropdown
                this.$el.find('#edit-configure-excluded-field-types-filter').html("");
                _.each(nonExcludedEntityTypes, function(t) {
                    var optionEl = $("<option></option>").attr("value", t.get("Id")).text(t.get("DisplayName"));
                    that.$el.find('#edit-configure-excluded-field-types-filter').append(optionEl);
                });

                // Select first entry
                if (nonExcludedEntityTypes.length > 0) {
                    this.$el.find('#edit-configure-excluded-field-types-filter').val(nonExcludedEntityTypes[0].get("Id"));
                }

                this.updateExcludedFieldTypesDropdown();
            }
        },
        updateExcludedFieldTypesDropdown: function() {
            var that = this;
            this.$el.find("#edit-configure-excluded-field-types").html("");
            var entityType = this.$el.find("#edit-configure-excluded-field-types-filter").val();

            if (!entityType) {
                // All entity types are excluded
                that.excludedFieldTypesWidget.multipleSelect('refresh');
                return;
            }

            // Download the full field type list for the current entity type, and then populate the dropdowns
            var fieldTypesCollection = new backbone.Collection(null, { url: "/api/fieldtype/" + entityType, model: backbone.Model.extend({ idAttribute: "Id" }) });
            fieldTypesCollection.fetch({
                reset: true,
                success: function() {
                    // Populate the field type dropdown
                    fieldTypesCollection.each(function(m) {
                        var optionEl = $("<option></option>")
                            .attr("value", m.get("id"))
                            .text(m.get("displayname"));
                        that.$el.find('#edit-configure-excluded-field-types').append(optionEl);
                    });
                    that.excludedFieldTypesWidget.multipleSelect('refresh');

                    // Check the appropriate items for this entity type
                    if (that.isValidConnectorSetting("EXCLUDED_FIELD_TYPES")) {
                        var tmp = JSON.parse(that.getConnectorSettingValue("EXCLUDED_FIELD_TYPES"));
                        if (tmp[entityType]) {
                            that.excludedFieldTypesWidget.multipleSelect('setSelects', tmp[entityType]);
                        }
                    }
                }
            });
        },
        isValidConnectorSetting: function(setting) {
            var result = $.grep(this.connectorSettingsCollection.models, function(e) {
                return e.Key != null && e.Key == setting || e.get != null && e.get("Key") == setting;
            });

            if (result.length == 0) {
                return false;
            }
            try {
                if (result[0].Value == "") {
                    return true;
                }
                if (result[0].Value == null) {
                    JSON.parse(result[0].get("Value"));
                } else {
                    JSON.parse(result[0].Value);
                }
            } catch (ex) {
                return false;
            }
            return true;
        },
        getConnectorSettingValue: function(setting) {
            var result = $.grep(this.connectorSettingsCollection.models, function(e) {
                return e.Key != null && e.Key == setting || e.get != null && e.get("Key") == setting;
            });

            if (result.length == 0) {
                return null;
            }

            var selected = result[0];
            _.each(result, function (values) {
                selected = values;
            });

            if (selected.Value == null) {
                return selected.get("Value");
            } else {
                return selected.Value;
            }
        },
        getConnectorSettingsModel: function(setting) {
            var result = $.grep(this.connectorSettingsCollection.models, function (e) {
                return e.Key != null && e.Key == setting || e.get != null && e.get("Key") == setting;
            });

            if (result.length == 0) {
                return null;
            }

            return result[0];
        },
        saveOrUpdateConnectorSettings: function(setting, value) {
            var channelSetting = _.find(this.connectorSettingsCollection.models, function (model) {
                return model.get("Key") == setting;
            });
            if (channelSetting == null) {
                var newSetting = this.connectorSettingsCollection.create({ connectId: this.connectorId, Key: setting, Value: value });
                newSetting.save(null, {
                    success: function() {
                        inRiverUtil.Notify("Setting has been updated");
                        window.appHelper.event_bus.trigger('connectorsettingsupdated');
                    },
                    error: function(error, response) {
                        inRiverUtil.OnErrors(error, response);
                    }
                });
            } else {
                channelSetting.set("Value", value);
                channelSetting.save(null, {
                    success: function() {
                        inRiverUtil.Notify("Setting has been updated");
                        window.appHelper.event_bus.trigger('connectorsettingsupdated');
                    },
                    error: function(error, response) {
                        inRiverUtil.OnErrors(error, response);
                    }
                });
            }
        },
        updateExcludedEntityTypesFromDropdown: function () {
            var selection = this.excludedEntityTypesWidget.multipleSelect('getSelects');
            this.saveOrUpdateConnectorSettings("EXCLUDED_ENTITY_TYPES", JSON.stringify(selection));
        },
        updateExcludedLinkTypesFromDropdown: function() {
            var selection = this.excludedLinkTypesWidget.multipleSelect('getSelects');
            this.saveOrUpdateConnectorSettings("EXCLUDED_LINK_TYPES", JSON.stringify(selection));
        },
        updateApiKey: function () {
            var newApiKey = this.$el.find('#edit-configure-apikey').val();
            this.saveOrUpdateConnectorSettings("MANAGEMENT_APIKEY", newApiKey);
        },
        updateRestUrl: function () {
            var newUrl = this.$el.find('#edit-configure-resturl').val();
            this.saveOrUpdateConnectorSettings("URL", newUrl);
        },
        updateExcludedFieldTypesFromDropdown: function () {
            // Get settings object
            var excludedFieldTypes = {};
            if (this.isValidConnectorSetting("EXCLUDED_FIELD_TYPES")) {
                excludedFieldTypes = JSON.parse(this.getConnectorSettingValue("EXCLUDED_FIELD_TYPES"));
                if (Object.prototype.toString.call(excludedFieldTypes) !== "[object Object]") excludedFieldTypes = {}; // We got a corrupt setting, reset it!
            }

            // Store new values for this entity type
            var currentEntityType = this.$el.find('#edit-configure-excluded-field-types-filter').val();
            var selection = this.excludedFieldTypesWidget.multipleSelect('getSelects');
            excludedFieldTypes[currentEntityType] = selection;

            // Save
            this.saveOrUpdateConnectorSettings("EXCLUDED_FIELD_TYPES", JSON.stringify(excludedFieldTypes));
        },
        updateLanguagesFromDropdown: function () {
            var selection = this.languagesWidget.multipleSelect('getSelects');
            this.saveOrUpdateConnectorSettings("LANGUAGE_CONFIGURATION", JSON.stringify(selection));
        },
        onEditMappings: function () {
            var pop = new modalPopup();
            pop.popOut(new xConnectNameMappingView({
                parent: this
            }), { size: "medium", header: "Mappings", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        onRestartConnector: function () {
            var that = this;
            this.$el.find("#restart-connector-spinner").show();

            $.ajax({
                type: "GET",
                url: "/api/connector/stop/" + this.connectorId,
            }).fail(function (model, response) {
                inRiverUtil.OnErrors(model, response);
                that.$el.find("#restart-connector-spinner").hide();
                window.appHelper.event_bus.trigger('connectorupdated');
            }).done(function () {
                $.ajax({
                    type: "GET",
                    url: "/api/connector/start/" + this.connectorId,
                }).fail(function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                    that.$el.find("#restart-connector-spinner").hide();
                    window.appHelper.event_bus.trigger('connectorupdated');
                }).done(function () {
                    that.$el.find("#restart-connector-spinner").hide();
                    window.appHelper.event_bus.trigger('connectorupdated');
                    inRiverUtil.Notify("The connector has been restarted.");
                });
            });
        },
        render: function () {
            this.$el.html(this.template);
        }
    });

    return crossConnectDataView;
});