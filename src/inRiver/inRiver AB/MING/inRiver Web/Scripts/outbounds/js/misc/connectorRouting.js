﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'outboundjs/views/xconnect/xConnectView'
], function ($, _, backbone, inRiverUtil, xConnectView) {
    var connectorRouter = {
        getConfigurationView: function (typeName, model) {
            var result = null;

            if (typeName == "inRiver.Connectors.xConnect.Rest") {
                result = new xConnectView({ model: model });
            }

            return result;
        }
    };

    return connectorRouter;
});