﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, inRiverUtil) {
    var connectorUtil = {
        isCrossConnect: function (typeName) {
            var result = false;
            if (typeName == "inRiver.Connectors.xConnect.Rest") {
                result = true;
            }

            return result;
        }
    };

    return connectorUtil;
});