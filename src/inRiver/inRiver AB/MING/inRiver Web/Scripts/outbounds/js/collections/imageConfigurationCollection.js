﻿define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, backbone) {
    var entityTypeCollection = backbone.Collection.extend({
        url: '/api/imageconfiguration/name'
    });

    return entityTypeCollection;
});
