﻿define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, backbone) {
    var entityTypeCollection = backbone.Collection.extend({
        url: '/api/language'
    });

    return entityTypeCollection;
});
