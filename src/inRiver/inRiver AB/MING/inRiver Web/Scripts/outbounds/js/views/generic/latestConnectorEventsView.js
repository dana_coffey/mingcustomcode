﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',
  'outboundjs/views/generic/connectorEventView',
  'sharedjs/collections/connectorevents/latestConnectorEventCollection',
  'text!outboundtemplates/generic/latestConnectorEventsTemplate.html'
], function ($, _, backbone, jqueryui, inRiverUtil, connectorEventView, latestConnectorEventCollection, latestConnectorEventsTemplate) {

    var latestConnectorEventsView = backbone.View.extend({
        initialize: function (options) {
            this.connectorId = options.connectorId;
            this.undelegateEvents();
            this.fetchEvents();

            this.listenTo(window.appHelper.event_bus, 'connectorupdated', this.connectorUpdated);
            this.listenTo(window.appHelper.event_bus, 'connectoradd', this.connectorUpdated);
            this.listenTo(window.appHelper.event_bus, 'connectordeleted', this.connectorDeleted);
        },
        connectorUpdated: function () {
            this.fetchEvents();
        },
        connectorDeleted: function () {
            this.fetchEvents();
        },
        fetchEvents: function () {
            var self = this;
            this.latestEventsCollection = new latestConnectorEventCollection();
            this.latestEventsCollection.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ id: this.connectorId, maxNumberOfEvents: 5 })
            });
        },
        render: function () {
            var channellist = this.$el.html(latestConnectorEventsTemplate);
            var models = this.latestEventsCollection.models;
            if (models.length == 0) {
                this.$el.find("#latest-event-list").html("<div class='no-items-text'>No events</div>");
            } else {
                _.each(models, function (model) {
                    var eventView = new connectorEventView({ model: model });
                    channellist.find('#latest-event-list').append(eventView.render().el);
                });
            }



            return this;
        }
    });

    return latestConnectorEventsView;
});

