﻿define([
  'jquery',
  'underscore',
  'backbone',
  'outboundjs/models/settingsModel'
], function ($, _, backbone, settingsModel) {
    var settingsCollection = backbone.Collection.extend({
        model: settingsModel,
        initialize: function (options) {
            this.id = options.id;
        },
        url: function () {
            return '/api/connectorsetting/' + this.id;
        }
    });

    return settingsCollection;
});
