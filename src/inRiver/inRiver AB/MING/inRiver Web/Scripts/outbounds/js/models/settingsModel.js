﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var settingsModel = backbone.Model.extend({
        idAttribute: "Key",
        url: function () {
            return '/api/connectorsetting/' + this.get("connectId");
        },
        initialize: function () {
        },
    });

    return settingsModel;

});
