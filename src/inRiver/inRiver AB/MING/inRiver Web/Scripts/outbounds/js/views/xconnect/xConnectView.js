﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'outboundjs/models/settingsModel',
    'outboundjs/collections/settingsCollection',
    'outboundjs/views/generic/channelEditView',
    'outboundjs/views/generic/latestConnectorEventsView',
    'outboundjs/views/xconnect/xConnectDataView',
    'outboundjs/views/xconnect/xConnectUserView',
    'text!outboundtemplates/xconnect/xConnectTemplate.html'
], function ($, _, backbone, inRiverUtil, settingsModel, settingsCollection, channelEditView, latestConnectorEventsView, xConnectDataView, xConnectUserView, xConnectTemplate) {

    var crossConnectView = backbone.View.extend({
        initialize: function () {
            var that = this;
            this.connectorId = this.model.get("Id");
            var collection = new settingsCollection({ id: this.connectorId });
            collection.fetch({
                success: function(result) {
                    that.settings = result.models;
                    that.render();
                },
                error: function() {
                    inRiverUtil.OnErrors(model, response);
                },
            });
        },
        render: function () {
            this.$el.html(xConnectTemplate);
            this.$el.find('#connector-channel-section').append(new channelEditView({ connectorId: this.connectorId, model: this.model, settings: this.settings }).el);
            this.$el.find('#connector-data-settings-section').append(new xConnectDataView({ connectorId: this.connectorId, settings: this.settings }).el);
            this.$el.find('#connector-event-section').append(new latestConnectorEventsView({ connectorId: this.connectorId }).el);
            this.$el.find('#connector-users-section').append(new xConnectUserView({ connectorId: this.connectorId }).el);
        }
    });

    return crossConnectView;
});