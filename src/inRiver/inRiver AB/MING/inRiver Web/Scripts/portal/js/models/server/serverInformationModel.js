﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var serverInformationModel = Backbone.Model.extend({
        url: '/api/serverInformation'
    });

    return serverInformationModel;
});
