define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'jquery-ui',
  'sharedjs/collections/apps',
  'sharedjs/views/appmenu/appMenuView',
  'sharedjs/views/usersettings/userSettingView',
  'sharedjs/views/settingsmenu/settingsMenu',
  'text!templates/home/userTemplate.html'
], function($, _, Backbone, modalPopup,jqueryui, apps, appMenuView, userSettingView,settingsMenu, topHeaderTemplate){

  var userView = Backbone.View.extend({
   
    initialize: function (options) {
     
        this.render();

        _.bindAll(this, "closeMenu");
    },
    events: {
        "click #user-settings-link": "onUserSettings",
         "click #mySettingsButton": "onToggleMenu"
    },

    onUserSettings: function (e) {
        e.stopPropagation();
        e.preventDefault();
      
        var modal = new modalPopup();
        modal.popOut(new userSettingView({ id: window.appHelper.userName }), { size: "small", header: "User Details", usesavebutton: true });
    },
      onToggleMenu: function(e) {
          e.stopPropagation();

          this.menu.toggle();

          $(document).one('click', this.closeMenu);
    

      },
      closeMenu: function() {
          this.menu.hide();
      },
    render: function () {
        
        this.app = _.where(apps, { name: this.appName });
        this.$el.html(_.template(topHeaderTemplate, { app: this.app[0] }));
        //this.$el.find("#signOut").prepend("<i class='fa fa-sign-out'></i> ");
        if (apps.length > 1) {
            this.$el.find("#appmenu-container").html(new appMenuView().el);
        } else {
            this.$el.find("#appMenu-launcher").hide();
        }
      //  settingsMenu.initializeSettingsMenu();
        
        this.listenTo(appHelper.event_bus, 'userupdated', this.render);

        this.menu = this.$el.find("#personalSettings > ul.dropdown-menu").menu();
        this.menu.hide();
        return this;
    }
  });

  return userView;
  
});
