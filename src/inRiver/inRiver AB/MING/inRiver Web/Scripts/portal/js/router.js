// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'views/start/startPageView',
  'sharedjs/views/topheader/topHeaderView'
], function ($, _, Backbone, jqueryui, startPageView, topHeaderView) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            // Define some URL routes
            "home": "home",
            // Default
            '*actions': 'home',
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(newView.el);
        },
        closeView: function (specView) {
            if (specView) {
                specView.close();
                specView.remove();
            }
        }
    });

    var initialize = function () {
        console.log("init router");

        appHelper.event_bus = _({}).extend(Backbone.Events);

        var app_router = new AppRouter;

     //   var headerView = new topHeaderView();

        app_router.on('route:home', function (id) {
            var startpageView = new startPageView();
            this.switchView(startpageView);
        });

        // Extend the View class to include a navigation method goTo
        Backbone.View.prototype.goTo = function (loc) {
            app_router.navigate(loc, true);
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        Backbone.View.prototype.close = function () {
            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        Backbone.history.start();
    };

    return {
        initialize: initialize
    };

});
