// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
    paths: {
        jquery: '../../libs/jquery/jquery-2.0.3.min',
        underscore: '../../libs/underscore/underscore',
        backbone: '../../libs/backbone/backbone-min',
        templates: '../templates',
        sharedtemplates: '../../shared/templates',
        sharedjs: '../../shared/js',
        'backbone-forms': '../../libs/backbone-forms/backbone-forms.amd',
        'bootstrap3': '../../libs/backbone-forms/bootstrap3',
        'jquery-ui': '../../libs/jquery-ui/jquery-ui.min',
        'deep-model': '../../libs/deep-model/deep-model',
        'text': '../../libs/text/text',
        'alertify': '../../libs/alertify/alertify',
        'backgrid': '../../libs/backgrid/backgrid-mod',
        'raphael': '../../libs/raphael/raphael-min',
        'morris': '../../libs/morris/morris.min',
        'moment': '../../libs/moment/moment.min',
        'localeStringEditor': 'sharedjs/extensions/localeStringEditor',
        'dateTimeEditor': 'sharedjs/extensions/dateTimeEditor',
        'modalPopup': '../../shared/js/views/modalpopup/modalPopup',
        'datetimepicker': '../../libs/datetimepicker/jquery.datetimepicker',
        'jstree': '../../libs/jstree/jstree.min',
        'multiple-select': '../../libs/multiple-select/multiple-select',
        'jquery-mousewheel': '../../libs/jquery.mousewheel/jquery.mousewheel',
    },
    shim: {
        underscore: { exports: '_' },
        backbone: { deps: ['underscore', 'jquery'], exports: 'Backbone' },
        'backgrid': { deps: ['jquery', 'backbone'], exports: 'Backgrid' },
        'deep-model': { deps: ['underscore', 'jquery'], exports: 'deep-model' },
        morris: { deps: ['jquery', 'raphael'], exports: 'Morris' },
        'datetimepicker': { deps: ['underscore', 'jquery'], exports: 'datetimepicker' },
        'dateTimeEditor': { deps: ['backbone-forms'], exports: 'dateTimeEditor' },
        'localeStringEditor': { deps: ['backbone-forms'], exports: 'localeStringEditor' },
        'jstree': { deps: ['jquery'], exports: 'jstree' }
       
    }
});

require([
  // Load our app module and pass it to our definition function
  'app', 'backbone-forms'

], function (App) {
    // The "app" dependency is passed in as "App"
    // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    App.initialize();
});