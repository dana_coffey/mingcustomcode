define([
  'jquery',
  'underscore',
  'backbone',
  'models/entityType/entityTypeStatisticsModel'
], function ($, _, Backbone, entityTypeStatisticsModel) {
    var entityTypeStatisticsCollection = Backbone.Collection.extend({
        model: entityTypeStatisticsModel,
        url: '/api/entitytypestatistics'
    });

    return entityTypeStatisticsCollection;
});
