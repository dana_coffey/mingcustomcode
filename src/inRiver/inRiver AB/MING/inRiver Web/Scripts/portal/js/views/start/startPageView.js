define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',

  'morris',
  'raphael',
  'sharedjs/misc/permissionUtil',
  'collections/entitytype/entityTypeStatisticsCollection',
  'models/server/serverInformationModel',
  'views/start/startPageWorkareaView',
  'views/start/userView',
  'sharedjs/views/start/startPageNotificationListView',

  'text!templates/home/homeTemplate.html'
], function ($, _, Backbone, jqueryui,inRiverUtil, Morris, Raphael, permissionUtil, entityTypeStatisticsCollection, serverInformationModel, startPageWorkareaView, userView, startPageNotificationListView, homeTemplate) {

    var startPageView = Backbone.View.extend({
        initialize: function (data) {
            var self = this;

            this.serverInformationModel = new serverInformationModel();

            this.collection = new entityTypeStatisticsCollection();
            this.collection.fetch({
                success: function () {

                    self.serverInformationModel.fetch({
                        success: function () {

                            self.render();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {

        },
        render: function () {
            var self = this;

            var sortedNewData = _.sortBy(self.collection.toJSON(), 'NewLastWeek');
            sortedNewData.reverse();

            self.$el.html(_.template(homeTemplate, { data: sortedNewData }));
            if (sortedNewData.length > 0 && sortedNewData[0].NewLastWeek > 0) {
                self.$el.find("#newEntitiesTitle").show();
            } else {
                self.$el.find("#newEntitiesTitle").hide();
            }
            var el = self.$el.find('#graph');

            var dataStat = new Array();
            var showTitle = false;
            _.each(self.collection.toJSON(), function (stat) {
                if (stat.UpdatedLastWeek > 0) {
                    showTitle = true;
                    dataStat.push({ value: stat.UpdatedLastWeek, label: stat.Name });
                }
            });
            var dataList = new Object(dataStat);

            Morris.Donut({
                element: el,
                data: dataList
            });

            self.$el.find('#graph').append(el);
            if (showTitle) {
                self.$el.find("#updateEntitiesTitle").show();
            } else {
                self.$el.find("#updateEntitiesTitle").hide();
            }

            showMoreLinks('#smallstat-row', 3);

            function showMoreLinks(id, startVisibleNumber) {
                var moreVisibleNumber = 100;
                var $more = $('<div id="showMoreLink"><i class="fa fa-caret-down"></i> Show more</div>');
                var $less = $('<div id="showMoreLink"><i class="fa fa-caret-up"></i> Show less</div>');
                if ($(id + ' li').length > startVisibleNumber) {
                    $(id + ' li').slice(startVisibleNumber).hide();
                } else {
                    $more.hide();
                }

                $more.click(function () {
                    $(id + ' li:hidden').slice(0, moreVisibleNumber).slideToggle();
                    if ($(id + ' li:hidden').length == 0) {
                        $more.hide();
                        $less.show();
                        $(id).after($less);
                    }
                });

                $less.click(function () {
                    $(id + ' li').slice(startVisibleNumber).slideToggle();
                    $less.hide();
                    $more.show();
                });

                $(id).after($more);
            }
            var wa = new startPageWorkareaView();
            var mynots = new startPageNotificationListView();
            $("#notifications").append(mynots.$el);

            self.$el.find("#about-version").html(this.serverInformationModel.attributes.Version);
            self.$el.find("#about-server-status").html(this.serverInformationModel.attributes.ServerStatus);
            self.$el.find("#about-master-language").html(this.serverInformationModel.attributes.MasterLanguage);
            self.$el.find("#about-server-name").html(this.serverInformationModel.attributes.ServerName);
            self.$el.find("#about-server-name").attr("title", this.serverInformationModel.attributes.ServerName);

            //Permissions:
            var callback = function (access, id, self) {
                if (!access) {
                    self.$el.find(id + " a").attr('disabled', 'disabled');
                    self.$el.find(id + " a").attr('href', '#');
                    self.$el.find(id).addClass("boxDisabled");
                    self.$el.find(id).attr("title", "App not available");
                    self.$el.find(id + " div").attr('title', "App not available");
                }
            };
            permissionUtil.GetUserPermission("inRiverCampaignPlanner", "#PlannerBox", callback, this);
            permissionUtil.GetUserPermission("ContentStore", "#ContentStore", callback, this);
            permissionUtil.GetUserPermission("SupplierOnboarding", "#SupplierOnboarding", callback, this);
            permissionUtil.GetUserPermission("Administrator,UpdateCVL", "#System", callback, this);
            permissionUtil.GetUserPermission("inRiverPrint", "#PrintAdmin", callback, this);
            permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);
            permissionUtil.GetUserPermission("inRiverSupply", "#Supply", callback, this);
            permissionUtil.GetUserPermission("inRiverPublish", "#Publish", callback, this);
            permissionUtil.GetUserPermission("inRiverPlanAndRelease", "#PlanAndRelease", callback, this);

            var userV = new userView();
            this.$el.find("#user-header").html(userV.$el);
        }
    });
    return startPageView;
});