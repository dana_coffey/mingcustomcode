define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
    'sharedjs/misc/inRiverUtil',

  'jstree',
  'sharedjs/models/workarea/workareaFolderModel',
  'sharedjs/collections/workareas/workareaCollection',
  'text!templates/home/startpageWorkareaTemplate.html'
], function ($, _, Backbone, jqueryui, inRiverUtil,jstree, workareaFolderModel, workareaCollection, startpageTemplate) {

    var startpageWorkareaView = Backbone.View.extend({
        initialize: function (data) {

            var self = this;

            this.undelegateEvents();

            this.workareas = new workareaCollection();
            this.workareas.fetch({

                success: function () {
                    
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {

        },
        render: function () {

            var self = this;
            this.$el.html(_.template(startpageTemplate, {}));
            $("#work-area-content").html("");
            $("#work-area-content").jstree({
                core: {
                    data: self.workareas.toJSON(),
                    animation: "100",
                    themes: { dots: false}
                },
                "plugins" : [
                       "types", "wholerow"
                ],
                "types": {
					"#": {
                        "max_children": 1,
                        "max_depth": 4,
                        "valid_children": ["personalroot", "sharedroot"]
                    },
					"personalroot": {
					    "icon": "/Images/folderdark.png",
                        "valid_children": ["default", "query"]
					},
					"sharedroot": {
					    "icon": "/Images/folderdark.png",
					    "valid_children": ["default", "query"]
					},
					"default": {
					    "icon": "/Images/folderlight.png",
					    "valid_children": ["default", , "query","file"]
					},
					"query": {
					    "icon": "/Images/folderquery.png",
					    "valid_children": ["default", , "query", "file"]
					},
                    "file": {
                        "valid_children": []
                    }
                },

             });



            $("#work-area-content").on("select_node.jstree",
                function (evt, data) {

                    if (data.node.id == "s_00000000-0000-0000-0000-000000000000") {
                        return;
                    }

                    if (data.node.id == "p_00000000-0000-0000-0000-000000000000") {
                        return;
                    }
                    //if (data.node.id == "empty") {
                    //    sessionStorage.removeItem("mainWorkarea");
                    //    sessionStorage.removeItem("secondWorkarea");

                    //    self.goTo("workarea?title=New workarea");
                    //    return; 
                    //}

                    var workareaFolder = new workareaFolderModel();

                    workareaFolder.url = "/api/workarea/" + data.node.id;
                    workareaFolder.fetch({
                        success: function(folder) {

                            var jsonData = folder.toJSON();
                            var folderType = "p";
                            if (jsonData.type == "Shared") {
                                folderType = "s";
                            }

                            if (jsonData.isquery) {
                                window.location.href = "/app/enrich/index#workarea?title=" + jsonData.text + "&workareaId="+ folderType +"_" + jsonData.id + "&isQuery=true";

                            } else {
                                window.location.href = "/app/enrich/index#workarea?title=" + jsonData.text + "&workareaId=" + folderType + "_" + jsonData.id;
                                //var entities = jsonData.folderentities.toString();

                                //$.post("/api/tools/compress", { '': entities }).done(function(result) {

                                    
                                //});
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
            );

            //$("#work-area-content").on("loaded.jstree",
            //    function (evt, data) {

            //        var depth = 2;
            //        data.instance.get_container().find('li').each(function (i) {
            //            if (data.instance.get_path($(this)).length <= depth) {
            //                data.instance.open_node($(this));
            //            }
            //        });
            //    }
            //);
        }
    });

    return startpageWorkareaView;
});

