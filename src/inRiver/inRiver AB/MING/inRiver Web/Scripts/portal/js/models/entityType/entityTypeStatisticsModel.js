﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var entityTypeStatisticsModel = Backbone.Model.extend({
        idAttribute: "EntityTypeId",
        initialize: function () {
        },
        toString: function () {
            return "testar";
        },
    });

    return entityTypeStatisticsModel;

});
