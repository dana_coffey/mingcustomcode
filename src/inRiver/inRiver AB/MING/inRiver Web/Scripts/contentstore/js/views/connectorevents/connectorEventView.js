define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/connectorevents/connectorEventTemplate.html'
], function ($, _, backbone, connectorEventTemplate) {

    var connectorEventView = backbone.View.extend({
        //tagName: 'div',

        initialize: function () {
        },
        events: {
            "click .connector-event-wrap": "onEventClick"
        },
        onEventClick: function (e) {
            e.stopPropagation();
            //window.location.href = "/app/enrich/index#entity/" + this.eventModel.id;
        },
        render: function ( ) {

            if (this.model == null) {
                return this;
            }

            var date = new Date(this.model.attributes["EventTime"]);
            this.$el.html(_.template(connectorEventTemplate, { data: this.model.toJSON(), eventTime: date.toLocaleString() }));

            return this; // enable chained calls
        }
    });

    return connectorEventView;
});
