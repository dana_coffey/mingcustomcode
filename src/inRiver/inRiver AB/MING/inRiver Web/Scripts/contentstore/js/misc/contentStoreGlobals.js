﻿define([
  'backbone'
], function (Backbone) {

    window.contentStoreGlobals = {
        init: function () {

            //Load global Content Store properties
            window.contentStoreGlobals.contentStoreCollection = new Backbone.Collection(null, { url: "/api/xconnect/contentstore" });
            window.contentStoreGlobals.allEntityTypesCollection = new Backbone.Collection(null, { url: "/api/entitytype", model: Backbone.Model.extend({ idAttribute: "Id" }) });
            window.contentStoreGlobals.allLinkTypesCollection = new Backbone.Collection(null, {
                url: "/api/linktype",
                model: Backbone.Model.extend({
                    idAttribute: "Id",
                    toString: function() { return this.get("SourceEntityTypeId") + " -> " + this.get("TargetEntityTypeId") + " (" + this.get("Id") + ")"; }
                })
            });
            window.contentStoreGlobals.allLanguagesCollection = new Backbone.Collection(appData.languages, { model: Backbone.Model.extend({ idAttribute: "Id" }) });
            window.contentStoreGlobals.allHtmlTemplatesCollection = new Backbone.Collection(null, { url: "/api/xconnect/htmltemplate" });
            window.contentStoreGlobals.allImageConfigurationsCollection = new Backbone.Collection(null, { url: "/api/imageconfiguration/name", model: Backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });
            window.contentStoreGlobals.allChannelsCollection = new Backbone.Collection(null, { url: "/api/channel", model: Backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("DisplayName"); } }) });
            window.contentStoreGlobals.allChannelsCollection.comparator =  function(model) {
                return model.get("DisplayName").toLowerCase();
            }
            window.contentStoreGlobals.currentContentStoreModel = null;
        }
    };
    return contentStoreGlobals;
});