define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'views/channel/channelView',
  'views/datatransferconfiguration/dataTransferConfigurationView',
  'views/setting/settingsView',
  'views/user/usersView',
  'text!templates/start/startPageTemplate.html',
  'sharedjs/collections/languages/LanguagesCollection'
], function (
    $,
    _,
    backbone,
    inRiverUtil,
    channelView,
    dataTransferConfigurationView,
    settingsView,
    usersView,
    startPageTemplate,
    languagesCollection) {

    /*StartPage*/
    var rootView = backbone.View.extend({
        initialize: function () {

            this.$el.html(startPageTemplate);
            
            this.listenToOnce(window.contentStoreGlobals.contentStoreCollection, "reset", this.onInit);
            window.contentStoreGlobals.contentStoreCollection.comparator = function (model) {
                return model.get("name");
            }

            window.contentStoreGlobals.contentStoreCollection.fetch({ reset: true, error: this.onError });

            this.listenTo(window.contentStoreGlobals.allEntityTypesCollection, "reset", this.onInit);
            window.contentStoreGlobals.allEntityTypesCollection.fetch({ reset: true, error: this.onError });

            this.listenTo(window.contentStoreGlobals.allLinkTypesCollection, "reset", this.onInit);
            window.contentStoreGlobals.allLinkTypesCollection.fetch({ reset: true, error: this.onError });

            this.listenTo(window.contentStoreGlobals.allHtmlTemplatesCollection, "reset", this.onInit);
            window.contentStoreGlobals.allHtmlTemplatesCollection.fetch({ reset: true, error: this.onError });
            
            this.listenTo(window.contentStoreGlobals.allImageConfigurationsCollection, "reset", this.onInit);
            window.contentStoreGlobals.allImageConfigurationsCollection.fetch({ reset: true, error: this.onError });

            this.listenTo(window.contentStoreGlobals.allChannelsCollection, "reset", this.onInit);
            window.contentStoreGlobals.allChannelsCollection.fetch({ reset: true, error: this.onError });
        },
        events: {
            "change #content-stores-dropdown": "onChangeContentStore",
            "click #content-stores-add": "onAddContentStore",
            "click #delete-content-store": "onDeleteContentStore"
        },
        onError: function (model, response) {
            var header = "Error in Content Store";
            var message = "Could not reach xConnect Server, please verify the connection/address and make sure it's online and try again.";
            if (response.status == null) {
                inRiverUtil.NotifyError(header, message, model.responseText);
                return;
            }
            inRiverUtil.NotifyError(header, message, response.responseText);
        },
        onInit: function (collection) {
            collection.isLoaded = true;

            // Perform final init when all necessary collections are loaded
            if (window.contentStoreGlobals.contentStoreCollection.isLoaded &&
                window.contentStoreGlobals.allImageConfigurationsCollection.isLoaded &&
                window.contentStoreGlobals.allEntityTypesCollection.isLoaded &&
                window.contentStoreGlobals.allHtmlTemplatesCollection.isLoaded &&
                window.contentStoreGlobals.allChannelsCollection.isLoaded) {

                if (window.contentStoreGlobals.contentStoreCollection.length > 0 &&
                    window.contentStoreGlobals.currentContentStoreModel == null) {

                    window.contentStoreGlobals.contentStoreCollection.sort({ reset: false});

                    window.contentStoreGlobals.currentContentStoreModel = window.contentStoreGlobals.contentStoreCollection.first();
                    this.listenTo(window.contentStoreGlobals.contentStoreCollection, "onChangeNameFinished", this.renderStaticContent);
                }

                this.listenTo(window.contentStoreGlobals.contentStoreCollection, "add remove", this.onContentStoreCollectionChanged);

                this.renderAll();
            }        
        },
        onContentStoreCollectionChanged: function () {
            if (!window.contentStoreGlobals.contentStoreCollection.get(window.contentStoreGlobals.currentContentStoreModel.get("id"))) {
                // Current content store was removed
                if (window.contentStoreGlobals.contentStoreCollection.length > 0) {
                    window.contentStoreGlobals.currentContentStoreModel = window.contentStoreGlobals.contentStoreCollection.first();
                }
                else {
                    window.contentStoreGlobals.currentContentStoreModel = null;
                }
            }

            this.renderAll();
        },
        onChangeContentStore: function (e) {
            window.contentStoreGlobals.currentContentStoreModel = window.contentStoreGlobals.contentStoreCollection.get(this.$el.find("#content-stores-dropdown").val());
            this.render();
        },
        onDeleteContentStore: function () {
            inRiverUtil.NotifyConfirm("Confirm Delete Content Store", "Are you sure you want to delete this Content Store?", function () {
                $.ajax({
                    type: "DELETE",
                    url: "/api/connector/remove/" + window.contentStoreGlobals.currentContentStoreModel.get("connectorId"),
                    //dataType: "application/json"
                }).fail(function () {
                    alert("Failed to delete connector!");
                }).done(function () {
                    // Will automatically send "DELETE" to /api/xconnect/contentstore/{storeId}
                    window.contentStoreGlobals.currentContentStoreModel.destroy();

                }, this);
            });
        },
        onAddContentStore: function () {
            var that = this;

            inRiverUtil.Prompt("Enter a name for your new Content Store", function (e, contentStoreName) {
                if (e) {

                    that.break = false;
                    var contentStoreNameLowerCase = contentStoreName.toLowerCase();
                    window.contentStoreGlobals.contentStoreCollection.each(function (model) {
                        if (contentStoreNameLowerCase == model.get("name").toLowerCase()) {
                            inRiverUtil.NotifyError("Can't create Content Store", "There is already a content store with that name.<br><br><i>Name is not case sensitive. '" + model.get("name") + "' are the same as '" + contentStoreName + "'.</i>");
                            that.break = true;
                            return;
                        }
                    });

                    if (that.break)
                        return;

                    var connectorTypeName = "inRiver.Connectors.xConnect.Rest";
                    var connectorId = that.getAvailableConnectorId(contentStoreName);

                    var channelId = 0;

                    if (window.contentStoreGlobals.allChannelsCollection.length > 0) {
                        channelId = window.contentStoreGlobals.allChannelsCollection.first().get("Id");
                    }

                    $.ajax({
                        type: "POST",
                        url: "/api/connector?" + jQuery.param({ 'id': connectorId, 'type': connectorTypeName, 'inbounds': false }),
                        //dataType: "application/json"
                    }).fail(function () {
                        alert("Failed to create connector!");
                    }).done(function () {
                        $.ajax({ data: { Key: "CHANNEL_ID", Value: channelId }, async: false, type: "POST", url: "/api/connectorsetting/" + connectorId, dataType: "application/json" });
                        $.ajax({ data: { Key: "RESOURCE_CONFIGURATION", Value: "Original" }, async: false, type: "POST", url: "/api/connectorsetting/" + connectorId, dataType: "application/json" });
                        $.ajax({ data: { Key: "LANGUAGE_CONFIGURATION", Value: JSON.stringify(window.contentStoreGlobals.allLanguagesCollection.pluck("Name")) }, async: false, type: "POST", url: "/api/connectorsetting/" + connectorId, dataType: "application/json" });
                        $.ajax({ data: { Key: "HTML_TEMPLATE_CONFIGURATION", Value: JSON.stringify([]) }, async: false, type: "POST", url: "/api/connectorsetting/" + connectorId, dataType: "application/json" });

                        // Start connector
                        $.ajax({ async: false, type: "GET", url: "/api/connector/start/" + connectorId, dataType: "application/json" });

                        window.contentStoreGlobals.currentContentStoreModel = window.contentStoreGlobals.contentStoreCollection.create({
                            name: contentStoreName,
                            channelId: channelId,
                            connectorId: connectorId,
                            backgroundColor: "#444",
                            textColor: "#ffffff",
                            defaultTypes: ["Product"]
                        }, {
                            wait: true
                        });

                        window.contentStoreGlobals.contentStoreCollection.fetch({ reset: true, error: this.onError, async: false });

                        window.contentStoreGlobals.currentContentStoreModel = window.contentStoreGlobals.contentStoreCollection.where({"name": contentStoreName})[0];

                        that.renderAll();
                    });
                }
            });
        },
        getAvailableConnectorId: function (contentStoreName) {
            var i = "";
            var connectorName = contentStoreName.replace(/[^a-zA-Z]/gi, '');
            while (1) {
                var connectorId = "ContentStore" + connectorName + i;
                var r = $.ajax({
                    type: "GET",
                    url: "/api/connector?name=" + connectorId,
                    async: false
                }).responseText;
                if (JSON.parse(r).Id == null) {
                    return connectorId;
                }
                if (i == "") i = 0;
                i++;
            }
        },
        renderStaticContent: function () {
            var selectEl = this.$el.find("#content-stores-dropdown");
            selectEl.html("");
            if (window.contentStoreGlobals.contentStoreCollection.length > 0) {
                window.contentStoreGlobals.contentStoreCollection.each(function (model) {
                    selectEl.append($('<option>', { value: model.get("id") }).text(model.get("name")));
                });
                selectEl.val(window.contentStoreGlobals.currentContentStoreModel.get("id"));
            }
        },
    renderAll: function () {
        this.renderStaticContent();
        this.render();
    },
    render: function () {
        if (window.contentStoreGlobals.currentContentStoreModel) {

            this.$el.find("#content-stores-content-area").show();

            this.$el.find("#content-stores-channel-section").html(new channelView().el);
            this.$el.find("#content-stores-data-settings-section").html(new dataTransferConfigurationView().el);
            this.$el.find("#content-stores-settings-section").html(new settingsView().el);
            this.$el.find("#content-stores-users-section").html(new usersView().el);

        } else {
            this.$el.find("#content-stores-content-area").hide();
        }
    }
});
     
return rootView;
});



