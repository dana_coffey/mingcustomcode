﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'text!templates/shared/appContextMenuTemplate.html'
], function ($, _, Backbone, modalPopup, appContextMenuTemplate) {

    var appContextMenuView = Backbone.View.extend({
        initialize: function (options) {
            this.undelegateEvents();
            this.render();
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
        },
        deselect: function (e) {
            if (e.target.id == "connector-name") {
                return;
            }
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle(200);
            $(document).one('click', this.deselect.bind(this));
        },
        render: function () {
            return this;
        }
    });

    return appContextMenuView;

});
