﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'simpleAjaxUploader',
  'multiple-select',
  'colpick',
  'text!templates/setting/settingsTemplate.html'
], function (
    $,
    _,
    backbone,
    inRiverUtil,
    simpleAjaxUploader,
    multipleSelect,
    colpick,
    settingsTemplate) {

    var settingsView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.allowedFileExtensions = ["jpg", "jpeg", "png", "gif", "tiff"];
            this.allowedMaxFileSizeInKiloBytes = 1024;

            this.template = _.template(settingsTemplate);

            window.contentStoreGlobals.currentContentStoreModel.on("change:textColor", this.updateUi, this);
            window.contentStoreGlobals.currentContentStoreModel.on("change:backgroundColor", this.updateUi, this);
            window.contentStoreGlobals.currentContentStoreModel.on("change:logoUrl", this.updateUi, this);

            this.render();

            this.createColorCtl(this.$el.find("#edit-content-store-pick-background-color"), "backgroundColor");
            this.createColorCtl(this.$el.find("#edit-content-store-pick-text-color"), "textColor");

            this.uploader = new ss.SimpleUpload({
                button: this.$el.find("#edit-content-store-file-upload"),
                url: '/api/xconnect/contentstore/' + window.contentStoreGlobals.currentContentStoreModel.get("id") + '/logo',
                allowedExtensions: this.allowedFileExtensions,
                maxSize: this.allowedMaxFileSizeInKiloBytes,
                //name: 'uploadfile', // Parameter name of the uploaded file
                multipart: true,
                onSubmit: function (filename, extension) {
                    that.$el.find("#file-upload-spinner").show();
                },
                onComplete: function (filename, response) {
                    that.$el.find("#file-upload-spinner").hide();
                    if (!response) {
                        alert('upload failed');
                    } else {
                        that.$el.find("#edit-content-store-logo-preview").hide();
                        window.contentStoreGlobals.currentContentStoreModel.set("logoUrl", JSON.parse(response).logoUrl);
                    }
                },
                onSizeError: function ( filename, fileSize ) {
                    alert("File '" + filename + "' have illegal file-size.\nMax allowed file-size = " + this._opts.maxSize + " kB.");
                },
                onExtError: function ( filename, extension ) {
                    alert("File '" + filename + "' have illegal file extension.\nAllowed extensions = '" + this._opts.allowedExtensions.join() + "'.");
                },
                onError: function (filename, errorType, status, statusText, response, uploadBtn, fileSize) {
                    alert("Unkown error occurred for '" + filename + "'.\n" + errorType + "\n" + status + "\n" + statusText + "\n");
                }
            });

            // Excluded Entity Types
            window.contentStoreGlobals.allEntityTypesCollection.each(function (t) {
                if (t.get("Id") == "Channel" || t.get("Id") == "ChannelNode") return;
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Id"))
                    .text(t.get("Id"));
                that.$el.find('#edit-content-store-default-entity-types').append(optionEl);
            });
            this.defaultEntityTypesWidget = this.$el.find('#edit-content-store-default-entity-types').multipleSelect({
                placeholder: "No entity types selected",
                selectAll: false,
                onClick: function () {
                    var selection = that.defaultEntityTypesWidget.multipleSelect('getSelects');
                    window.contentStoreGlobals.currentContentStoreModel.set({ "defaultTypes": selection });
                    window.contentStoreGlobals.currentContentStoreModel.save();
                }
            });
            if ($.isArray(window.contentStoreGlobals.currentContentStoreModel.get("defaultTypes"))) {
                this.defaultEntityTypesWidget.multipleSelect("setSelects", window.contentStoreGlobals.currentContentStoreModel.get("defaultTypes"));
            }

            // HTML Templates
            window.contentStoreGlobals.allHtmlTemplatesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("id"))
                    .text(t.get("name"));
                that.$el.find('#edit-content-store-html-templates').append(optionEl);
            });
            this.htmlTemplatesWidget = this.$el.find('#edit-content-store-html-templates').multipleSelect({
                selectAll: false,
                placeholder: "No html templates selected",
                onClick: function () {
                    var selection = _.map(that.htmlTemplatesWidget.multipleSelect('getSelects'), function (x) {
                        return parseInt(x);
                    });
                    window.contentStoreGlobals.currentContentStoreModel.set({ "htmlTemplates": selection });
                    window.contentStoreGlobals.currentContentStoreModel.save();
                }
            });
            if ($.isArray(window.contentStoreGlobals.currentContentStoreModel.get("htmlTemplates"))) {
                this.htmlTemplatesWidget.multipleSelect("setSelects", window.contentStoreGlobals.currentContentStoreModel.get("htmlTemplates"));
            }

            this.updateUi();
        },
        events: {
            "change #edit-content-store-name": "onChangeName",
            "keyup #edit-content-store-name": "onChangeName",
            "blur #edit-content-store-name": "onChangeNameFinished",
            "click #edit-content-store-pick-background-color": "onPickBackgroundColor",
            "click #edit-content-store-pick-text-color": "onPickTextColor",
            "click #delete-content-store": "onDeleteContentStore"
        },
        onChangeName: function (e) {
            window.contentStoreGlobals.currentContentStoreModel.set({ name: this.$el.find("#edit-content-store-name").val() });
            window.contentStoreGlobals.currentContentStoreModel.save();
        },
        onChangeNameFinished: function (e) {
            window.contentStoreGlobals.contentStoreCollection.trigger("onChangeNameFinished");
        },
        //updateXConnectTemplateData: function (ids, successFn) {
        //    var payLoad = [];
        //    _.each(ids, function (templateId) {
        //        var templateData = window.contentStoreGlobals.allHtmlTemplatesCollection.get(templateId).toJSON();
        //        payLoad.push(templateData);
        //    });
        //    $.ajax({
        //        type: "POST",
        //        url: "/api/xconnect/templateupdate",
        //        data: JSON.stringify(payLoad),
        //        contentType: "application/json",
        //    }).fail(function () {
        //        alert("Failed to update html templates!");
        //    }).done(function () {
        //        successFn();
        //    });
        //},
        onPickBackgroundColor: function (e) {
            this.$el.find("#edit-content-store-pick-background-color").colpickShow();
        },
        onPickTextColor: function (e) {
            this.$el.find("#edit-content-store-pick-text-color").colpickShow();
        },
        createColorCtl: function (el, attributeName) {
            el.colpick({
                color: window.contentStoreGlobals.currentContentStoreModel.get(attributeName),
                layout: 'hex',
                submit: 0,
                onChange: function (hsb, hex, rgb, elem, bySetColor) {
                    var map = {};
                    map[attributeName] = "#" + hex;
                    window.contentStoreGlobals.currentContentStoreModel.set(map);
                    window.contentStoreGlobals.currentContentStoreModel.save();
                }
            }).keyup(function () {
                var v = $(this).find("input").val();
                if (/(^#[0-9A-F]{6}$)/i.test(v)) {
                    $(this).colpickSetColor(v);
                }
            });
        },
        onDeleteContentStore: function () {
            inRiverUtil.NotifyConfirm("Confirm Delete Content Store", "Are you sure you want to delete this Content Store?", function () {
                $.ajax({
                    type: "DELETE",
                    url: "/api/connector/remove/" + window.contentStoreGlobals.currentContentStoreModel.get("connectorId"),
                    //dataType: "application/json"
                }).fail(function () {
                    alert("Failed to delete connector!");
                }).done(function () {
                    // Will automatically send "DELETE" to /api/xconnect/contentstore/{storeId}
                    window.contentStoreGlobals.currentContentStoreModel.destroy();

                }, this);
            });
        },
        updateUi: function () {
            this.$el.find("#edit-content-store-name").val(window.contentStoreGlobals.currentContentStoreModel.get("name"));

            var tmpEl1 = this.$el.find("#edit-content-store-pick-background-color");
            tmpEl1.find("input").css('border-color', window.contentStoreGlobals.currentContentStoreModel.get("backgroundColor"));
            tmpEl1.find("input").val(window.contentStoreGlobals.currentContentStoreModel.get("backgroundColor"));
            var tmpEl2 = this.$el.find("#edit-content-store-pick-text-color");
            tmpEl2.find("input").css('border-color', window.contentStoreGlobals.currentContentStoreModel.get("textColor"));
            tmpEl2.find("input").val(window.contentStoreGlobals.currentContentStoreModel.get("textColor"));

            if (window.contentStoreGlobals.currentContentStoreModel.get("logoUrl") != null) {
                this.$el.find("#edit-content-store-logo-preview").attr("src", "/api/xconnect" + window.contentStoreGlobals.currentContentStoreModel.get("logoUrl") + "?maxwidth=90&maxheight=90");
                this.$el.find("#edit-content-store-logo-preview").show();
            } else {
                this.$el.find("#edit-content-store-logo-preview").hide();
            }
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ data: window.contentStoreGlobals.currentContentStoreModel.toJSON() }));
        }
    });

    return settingsView;
});