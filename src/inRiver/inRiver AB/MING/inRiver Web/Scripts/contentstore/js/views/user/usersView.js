﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'views/user/userListItemView',
  'views/user/userEditView',
  'collections/xConnectUserCollection',
  'models/xConnectUserModel',
  'text!templates/user/userTemplate.html',
], function (
    $,
    _,
    backbone,
    modalPopup,
    userListItemView,
    userEditView,
    xConnectUserCollection,
    xConnectUserModel,
    userTemplate) {

    var usersView = backbone.View.extend({
        initialize: function () {
            this.userCollection = new xConnectUserCollection();

            this.listenTo(this.userCollection, 'reset', this.render);
            this.listenTo(this.userCollection, 'add', this.render);
            this.listenTo(this.userCollection, 'remove', this.render);

            window.appHelper.event_bus.off('openEditUserPopup');
            this.listenTo(window.appHelper.event_bus, 'openEditUserPopup', this.openEditUserPopup);

            this.userCollection.fetch({ reset: true });

            this.render();
        },
        events: {
            "click #list-of-users-add": "onAddUser"
        },
        onAddUser: function () {
            var model = new xConnectUserModel();
            this.openEditUserPopup(model);
        },
        openEditUserPopup: function (model) {
            var pop = new modalPopup();
            pop.popOut(new userEditView({
                model: model,
                userCollection: this.userCollection
            }), { size: "small", header: "Edit User", usesavebutton: true, enableSaveButton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        render: function () {

            this.$el.html(userTemplate);

            var ulEl = this.$el.find("#xconnect-user-list-wrap > ul");

            var usersList = [];

            _.each(this.userCollection.models, function (supplier) {
                usersList.push(supplier);
            });

            usersList.sort(function (a, b) {
                if (a.get("email") < b.get("email"))
                    return -1;
                else if (a.get("email") > b.get("email"))
                    return 1;
                else
                    return 0;
            });

            _.each(usersList, function (model) {
                var userItemView = new userListItemView({ model: model });
                ulEl.append(userItemView.el);
            });
        }
    });

    return usersView;
});