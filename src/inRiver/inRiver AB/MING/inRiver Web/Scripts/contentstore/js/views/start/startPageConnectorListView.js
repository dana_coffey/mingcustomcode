define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'text!templates/start/startPageConnectorListTemplate.html',
  'sharedjs/views/connector/connectorCardView',
  'sharedjs/collections/connector/connectorCollection'
], function ($, _, backbone, jqueryui, startPageConnectorListTemplate, connectorCardView, connectorCollection) {

    var startPageConnectorList = backbone.View.extend({
        initialize: function () {
            this.undelegateEvents();
            this.fetchCollection();

            this.listenTo(appHelper.event_bus, 'connectoradd', this.fetchCollection);
            this.listenTo(appHelper.event_bus, 'connectordeleted', this.fetchCollection);
        },
        fetchCollection: function () {
            var self = this;
            this.connectorCollection = new connectorCollection();
            this.connectorCollection.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                data: $.param({ inbounds: "false", outbounds: "true" })
            });
        },
        render: function () {
            var thisPage = this.$el.html(startPageConnectorListTemplate);
            var inbounds = this.connectorCollection.models;
            if (inbounds.length == 0) {
                this.$el.find("#connector-list").html("<div class='no-items-text'>There are no connectors to show, or the 'inRiver Connect' service is not running.</div>");
            } else {
                _.each(inbounds, function (model) {
                    var connectorView = new connectorCardView();
                    thisPage.find('#connector-list').append(connectorView.render(model).el);
                });
            }

            return this; // enable chained calls
        }

    });

    return startPageConnectorList;
});

