﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/mapping/mappingsItemTemplate.html',
], function ($, _, backbone, mappingsItemTemplate) {

    var settingSubView = backbone.View.extend({
        initialize: function (options) {
            this.template = _.template(mappingsItemTemplate);
            this.entityType = options.entityType;
            this.fieldType = options.fieldType;
            this.value = options.value;
            this.parent = options.parent;
            this.render();
        },
        events: {
            "click #mapping-item-remove": "onRemove",
            "change #mapping-item-value": "onChangeValue",
            "keyup #mapping-item-value": "onChangeValue",
            "blur #mapping-item-value": "onChangeValue",
        },
        onRemove: function () {
            this.parent.removeMapping(this.entityType, this.fieldType);
        },
        onChangeValue: function () {
            this.parent.changeMappingValue(this.entityType, this.fieldType, this.$el.find("#mapping-item-value").val());
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ entityType: this.entityType, fieldType: this.fieldType, value: this.value }));
        }
    });

    return settingSubView;
});