﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'text!templates/channel/channelTemplate.html',
], function ($, _, backbone, backboneforms, channelTemplate) {

    var channelView = backbone.View.extend({
        initialize: function () {
            this.template = _.template(channelTemplate);
            this.updateUi();
            this.listenTo(window.contentStoreGlobals.currentContentStoreModel, 'change:channelId', this.onChangeChannel);
            this.onInit();
        },
        onInit: function () {
            this.render();
            this.updateUi();
        },
        events: {
            "click #publish-channel": "onPublishChannel",
            "click #unpublish-channel": "onUnpublishChannel"
        },
        changePublished: function (b) {
            var url;
            if (b) {
                url = "/api/channelaction/publish/" + window.contentStoreGlobals.currentContentStoreModel.get("channelId");
            } else {
                url = "/api/channelaction/unpublish/" + window.contentStoreGlobals.currentContentStoreModel.get("channelId");
            }

            var that = this;
            $.ajax({
                url: url
            }).fail(function () {
                alert("Fail!");
            }).always(function () {
                var currentChannel = window.contentStoreGlobals.allChannelsCollection.get(window.contentStoreGlobals.currentContentStoreModel.get("channelId"));
                currentChannel.fetch({
                    success: function () {
                        that.updateUi();
                    }
                });
            });
        },
        onPublishChannel: function () {
            this.changePublished(true);
        },
        onUnpublishChannel: function () {
            this.changePublished(false);
        },
        onChangeChannel: function () {
            // Update channel in connector as well
            var newChannelId = window.contentStoreGlobals.currentContentStoreModel.get("channelId");
            var connectorId = window.contentStoreGlobals.currentContentStoreModel.get("connectorId");
            $.ajax({
                data: { Key: "CHANNEL_ID", Value: newChannelId },
                type: "POST",
                url: "/api/connectorsetting/" + connectorId,
                dataType: "application/json"
            });

            this.updateUi();
        },
        updateUi: function () {
            var currentChannel = window.contentStoreGlobals.allChannelsCollection.get(window.contentStoreGlobals.currentContentStoreModel.get("channelId"));

            if (currentChannel == null) {
                return;
            }

            //if (currentChannel.get("ChannelPublished")) {
            //    this.$el.find("#publish-channel").attr('disabled', 'disabled');
            //    this.$el.find("#unpublish-channel").removeAttr('disabled');
            //} else {
            //    this.$el.find("#publish-channel").removeAttr('disabled');
            //    this.$el.find("#unpublish-channel").attr('disabled', 'disabled');
            //}
            if (currentChannel.get("ChannelPublished")) {
                this.$el.find("#publish-channel").css('display', 'none');
                this.$el.find("#unpublish-channel").css('display', 'inline');
            } else {
                this.$el.find("#publish-channel").css('display', 'inline');
                this.$el.find("#unpublish-channel").css('display', 'none');
            }
        },
        render: function () {
            this.$el.html(this.template());

            this.form = new Backbone.Form({
                model: window.contentStoreGlobals.currentContentStoreModel,
                schema: {
                    channelId: { title: "Channel", type: 'Select', options: window.contentStoreGlobals.allChannelsCollection.sort() },
                }
            }).render();
            this.form.on("change", function (e) {
                this.commit();
                this.model.save();
            });
            this.$el.find("#form-container").html(this.form.el);
        }
    });

    return channelView;
});