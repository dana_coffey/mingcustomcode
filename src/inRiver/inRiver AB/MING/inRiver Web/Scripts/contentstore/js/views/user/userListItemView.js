﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/user/userListItemTemplate.html',
], function (
    $,
    _,
    backbone,
    userListItemTemplate) {

    var userListItemView = backbone.View.extend({
        tagName: "li",
        initialize: function (options) {
            this.template = _.template(userListItemTemplate),
            this.model = options.model;
            this.model.on('change', this.render, this);
            this.render();
        },
        events: {
            "change #is-assigned": "onChangeIsAssigned",
            "click #list-item-edit": "onEditUser",
            "click #list-item-link-edit": "onEditUser",
            "click #list-item-info-edit": "onEditUser"
        },
        onChangeIsAssigned: function (e) {
            var isAssigned = this.$el.find("#is-assigned").prop('checked');

            if (this.model.get("contentStoresId") == null) {
                this.model.set("contentStoresId", []);
            }

            if (!isAssigned) {
                this.model.set("contentStoresId", _.reject(this.model.get("contentStoresId"), function (item) {
                    return item.contentStoreId === window.contentStoreGlobals.currentContentStoreModel.get("id");
                }));
            } else {
                var langs = [];
                $("#edit-content-store-languages").each(function () {
                    langs.push(this.value);
                });
                this.model.get("contentStoresId").push({ contentStoreId: window.contentStoreGlobals.currentContentStoreModel.get("id"), language: langs[0] });
            }
            this.model.save();
        },
        onEditUser: function (e) {
            window.appHelper.event_bus.trigger('openEditUserPopup', this.model);
        },
        render: function () {
            this.$el.html(this.template({ data: this.model.toJSON() }));

            var isUserAssignedToCurrentContentStore = _.contains(_.pluck(this.model.get("contentStoresId"), "contentStoreId"), window.contentStoreGlobals.currentContentStoreModel.get("id"));
            this.$el.find("#is-assigned").prop('checked', isUserAssignedToCurrentContentStore);
        }
    });

    return userListItemView;
});