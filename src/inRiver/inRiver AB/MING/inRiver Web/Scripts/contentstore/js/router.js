// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'sharedjs/views/topheader/topHeaderView',
  'views/appheader/appHeaderView',
  'views/start/startPageView',
  'misc/contentStoreGlobals'

], function ($, _, backbone, jqueryui, topHeaderView, appSupHeaderView, startPageView, contentStoreGlobals) {

    var AppRouter = backbone.Router.extend({
        routes: {
            // Define some URL routes
            "home": "home",
            // Default
            '*actions': 'home',
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(newView.el);
        },
        closeView: function (specView) {
            if (specView) {
                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function (queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                        var aux = el.split('='), o = {};
                        if (aux.length >= 1) {
                            var val = undefined;
                            if (aux.length == 2)
                                val = aux[1];
                            o[aux[0]] = val;
                        }
                        return o;
                    }),
                    function (o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function () {
        console.log("init router");

        appHelper.event_bus = _({}).extend(backbone.Events);

        var appRouter = new AppRouter;

        var headerView = new topHeaderView({ app: "Content Store" });
        var appheaderView = new appSupHeaderView();

        contentStoreGlobals.init();

        appRouter.on('route:home', function (id) {
            if (this.startPageView != undefined) {
                this.closeView(this.startPageView);
            }

            this.switchView(new startPageView());
        });


        // Extend the View class to include a navigation method goTo
        backbone.View.prototype.goTo = function (loc) {
            appRouter.navigate(loc, true);
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        backbone.View.prototype.close = function () {

            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        backbone.history.start();
    };

    return {
        initialize: initialize
    };

});
