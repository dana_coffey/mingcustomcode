// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    jquery: '../../libs/jquery/jquery-2.0.3.min',
    underscore: '../../libs/underscore/underscore',
    backbone: '../../libs/backbone/backbone-min',
    templates: '../templates',
    sharedtemplates: '../../shared/templates',
    sharedjs: '../../shared/js',
    outboundjs: '../../outbounds/js',
    outboundtemplates: '../../outbounds/templates',
    'backbone-forms': '../../libs/backbone-forms/backbone-forms.amd',
    'backbone-modal': '../../libs/backbone-modal/backbone.modal',
    'jquery-ui': '../../libs/jquery-ui/jquery-ui.min',
    'jquery-ui-touch-punch': '../../libs/jquery.ui.touch-punch/jquery.ui.touch-punch',
    'alertify': '../../libs/alertify/alertify',
    'colpick': '../../libs/colpick/colpick',
    'backgrid': '../../libs/backgrid/backgrid-mod',
    'text': '../../libs/text/text',
    'deep-model': '../../libs/deep-model/deep-model',
    'datetimepicker': '../../libs/datetimepicker/jquery.datetimepicker',
    'simpleAjaxUploader': '../../libs/simpleajaxuploader/SimpleAjaxUploader',
    'dropzone': '../../libs/dropzone/dropzone-amd-module',
    'localeStringEditor': 'sharedjs/extensions/localeStringEditor',
    'dateTimeEditor': 'sharedjs/extensions/dateTimeEditor',
    'jstree': '../../libs/jstree/jstree.min',
    'modalPopup': '../../shared/js/views/modalpopup/modalPopup',
    'moment': '../../libs/moment/moment.min',
    'multiple-select': '../../libs/multiple-select/multiple-select',
    'jquery-mousewheel': '../../libs/jquery.mousewheel/jquery.mousewheel',
  },
  shim: {
      underscore: { exports: '_' },
      backbone: { deps: ['underscore', 'jquery'], exports: 'Backbone' },
      'backbone-modal': { deps: ['jquery', 'backbone'], exports: 'Modal' },
      'backbone-forms': { deps: ['jquery', 'backbone'], exports: 'backbone-forms' },
      'backgrid': { deps: ['jquery', 'backbone'], exports: 'Backgrid' },
      'deep-model': { deps: ['underscore', 'jquery'], exports: 'deep-model' },
      'datetimepicker': { deps: ['underscore', 'jquery'], exports: 'datetimepicker' },
      'dateTimeEditor': { deps: ['backbone-forms'], exports: 'dateTimeEditor' },
      'localeStringEditor': { deps: ['backbone-forms'], exports: 'localeStringEditor' },
      'jquery-ui-touch-punch': { deps: ['jquery-ui'], exports: 'jquery-ui-touch-punch' },
      'colpick': {deps: ['jquery'], exports: 'colpick'},
     // 'multipleSelect': {deps: ['jquery'], exports: 'multipleSelect'}
  }
});

require([
  // Load our app module and pass it to our definition function
  'app', 'backbone-forms'

], function (App) {
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    App.initialize();
});
