﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'text!templates/user/userEditTemplate.html',
], function ($, _, backbone, inRiverUtil, userEditTemplate) {

    var userEditView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.model = options.model;

            this.userCollection = options.userCollection;

            this.template = _.template(userEditTemplate);

            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.render();
            if (this.model.isNew()) {
                this.doGenerateNewApiKey(this);
                this.$el.find("#reset-password").hide();
                this.$el.find("#delete-user").hide();
            }
        },
        events: {
            "click #generate-api-key": "onGenerateNewApiKey",
            "click #reset-password": "onResetPassword",
            "click #delete-user": "onDeleteUser",
        },
        onGenerateNewApiKey: function () {
            inRiverUtil.NotifyConfirm("Confirm revoke api key", "Are you sure you want to revoke this api key?<br><br>Revoking an api key will break all usages of the inRiver xConnect API for this user. If an api key is revoked a new one will be generated.", this.doGenerateNewApiKey, this);
        },
        doGenerateNewApiKey: function (that) {
            var tmpModel = new backbone.Model();
            tmpModel.urlRoot = "/api/xconnect/newapikey";
            tmpModel.fetch({
                success: function () {
                    that.$el.find("#api-key").val(tmpModel.get("apiKey"));
                }
            });
        },
        onResetPassword: function () {
            var that = this;
            inRiverUtil.NotifyConfirm("Reset Password", "Are you sure you want to reset the password for this user?<br><br>The user will not be able to login without setting a new password.", function () {
                that.$el.find("#reset-password").text(" Please Wait");
                that.doPasswordReset(that.model, "Password was reset! Now ask the user to set a new password by using this url:", function () {
                    that.$el.find("#reset-password").text("Reset this user's password");
                });
            }, this);
        },
        onDeleteUser: function () {
            var that = this;
            inRiverUtil.NotifyConfirm("Delete User", "Are you sure you want to delete this user?<br><br>The user will not be able to use any inRiver xConnect features at all.", function () {
                that.model.destroy({
                    success: function () {
                        that.done();
                    }
                });
            }, this);
        },
        onSave: function () {
            var self = this;

            console.log("Saving user");
            var errors = this.form.commit();

            if (errors) {
                return;
            }

            this.model.set({ apiKey: this.$el.find("#api-key").val() });

            var isNew = this.model.isNew();
            this.model.save(null,
            {
                success: function(model, response) {
                    if (isNew) {
                        self.userCollection.add(self.model);
                        self.doPasswordReset(self.model,
                            "User was successfully created!<br /> Now ask the user to set a password by using this url:",
                            function() {
                                self.done();
                            });
                    } else {
                        self.done();
                    }
                },
                error: function (model, response) {
                    console.log(response.statusText);
                    self.$el.find("#edit-user-area").append('<div class="callbackerror">' + response.statusText + '</div>');
                }
        });
        },
        doPasswordReset: function (model, successMsg, successFn) {
            var tmpModel = new backbone.Model({}, {
                urlRoot: model.urlRoot + "/" + model.get("id") + "/passwordreset"
            });
            tmpModel.fetch({
                method: "POST",
                success: function () {
                    var url = tmpModel.get("passwordResetUrl");
                    inRiverUtil.NotifyConfirm("User", successMsg + "<br><br><input type='text' class='content-store-user-password-reset' value='" + url + "'>", function (b) { successFn(); }, this);
                }
            });
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ data: this.model.toJSON() }));

            this.model.schema = {
                email: { validators: ['required', 'email'] },
                firstName: { title: 'First Name', validators: ['required'] },
                lastName: { title: 'Last Name', validators: ['required'] },
            };

            // Render user info form
            this.form = new Backbone.Form({
                model: this.model
            }).render();
            this.$el.find("#form-container").html(this.form.el);
        }
    });

    return userEditView;
});