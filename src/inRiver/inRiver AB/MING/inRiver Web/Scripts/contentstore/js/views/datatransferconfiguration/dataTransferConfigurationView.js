﻿define([
  'jquery',
  'underscore',
  'backbone',
  'multiple-select',
  'modalPopup',
  'views/mapping/mappingView',
  'text!templates/datatransferconfiguration/dataTransferConfigurationTemplate.html',
], function ($, _, backbone, multipleSelect, modalPopup, mappingView, dataTransferConfigurationTemplate) {

    var dataTransferConfgurationView = backbone.View.extend({
        initialize: function (options) {
            this.template = _.template(dataTransferConfigurationTemplate);
            this.render();

            this.connectorSettingsCollection = new backbone.Collection({}, { url: "/api/connectorsetting/" + window.contentStoreGlobals.currentContentStoreModel.get("connectorId"), model: backbone.Model.extend({ idAttribute: "Key" }) });
            this.listenTo(this.connectorSettingsCollection, 'reset', this.onInit);
            this.connectorSettingsCollection.fetch({ reset: true });
        },
        events: {
            "change #edit-content-store-excluded-field-types-filter": "updateExcludedFieldTypesDropdown",
            "click #edit-content-store-mappings": "onEditMappings",
            "click #restart-connector": "onRestartConnector",
            "change #edit-content-store-apikey": "updateApiKey",
            "change #edit-content-store-resturl": "updateRestUrl"
        },
        getNonExcludedEntityTypes: function () {
            var excludedEntityTypes = [];
            if (this.isValidConnectorSetting("EXCLUDED_ENTITY_TYPES")) {
                excludedEntityTypes = JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_ENTITY_TYPES").get("Value"));
            }

            // Get a list of entity types where the excluded types are removed
            var nonExcludedEntityTypes = window.contentStoreGlobals.allEntityTypesCollection.reject(function (v) {
                return _.contains(excludedEntityTypes, v.get("Id"));
            });

            return nonExcludedEntityTypes;
        },
        getNonExcludedFieldTypes: function (entityType, allFieldTypesForEntityTypeCollection) {
            var excludedFieldTypes = [];
            if (this.isValidConnectorSetting("EXCLUDED_FIELD_TYPES")) {
                var tmp = JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_FIELD_TYPES").get("Value"));
                if (tmp[entityType]) {
                    excludedFieldTypes = tmp[entityType];
                }
            }

            // Get a list of entity types where the excluded types are removed
            var nonExcludedFieldTypes = allFieldTypesForEntityTypeCollection.reject(function (v) {
                return _.contains(excludedFieldTypes, v.get("id"));
            });

            return nonExcludedFieldTypes;
        },
        updateExcludedFieldTypesFilter: function (m) {
            var that = this;
            if (m == undefined || m.get("Key") == "EXCLUDED_ENTITY_TYPES") { // m will be a model if we got called from a collection event

                var nonExcludedEntityTypes = this.getNonExcludedEntityTypes();

                // Populate the filter dropdown
                this.$el.find('#edit-content-store-excluded-field-types-filter').html("");
                _.each(nonExcludedEntityTypes, function (t) {
                    var optionEl = $("<option></option>").attr("value", t.get("Id")).text(t.get("DisplayName"));
                    that.$el.find('#edit-content-store-excluded-field-types-filter').append(optionEl);
                });

                // Select first entry
                if (nonExcludedEntityTypes.length > 0) {
                    this.$el.find('#edit-content-store-excluded-field-types-filter').val(nonExcludedEntityTypes[0].get("Id"));
                }

                this.updateExcludedFieldTypesDropdown();
            }
        },
        updateExcludedFieldTypesDropdown: function () {
            var that = this;
            this.$el.find("#edit-content-store-excluded-field-types").html("");
            var entityType = this.$el.find("#edit-content-store-excluded-field-types-filter").val();

            if (!entityType) {
                // All entity types are excluded
                that.excludedFieldTypesWidget.multipleSelect('refresh');
                return;
            }

            // Download the full field type list for the current entity type, and then populate the dropdowns
            var fieldTypesCollection = new backbone.Collection(null, { url: "/api/fieldtype/" + entityType, model: backbone.Model.extend({ idAttribute: "Id" }) });
            fieldTypesCollection.fetch({
                reset: true,
                success: function (c) {
                    // Populate the field type dropdown
                    fieldTypesCollection.each(function (m) {
                        var optionEl = $("<option></option>")
                            .attr("value", m.get("id"))
                            .text(m.get("displayname"));
                        that.$el.find('#edit-content-store-excluded-field-types').append(optionEl);
                    });
                    that.excludedFieldTypesWidget.multipleSelect('refresh');

                    // Check the appropriate items for this entity type
                    if (that.isValidConnectorSetting("EXCLUDED_FIELD_TYPES")) {
                        var tmp = JSON.parse(that.connectorSettingsCollection.get("EXCLUDED_FIELD_TYPES").get("Value"));
                        if (tmp[entityType]) {
                            that.excludedFieldTypesWidget.multipleSelect('setSelects', tmp[entityType]);
                        }
                    }
                }
            });
        },
        isValidConnectorSetting: function (setting) {
            if (!this.connectorSettingsCollection.get(setting)) {
                return false;
            }
            try {
                JSON.parse(this.connectorSettingsCollection.get(setting).get("Value"));
            } catch (e) {
                return false;
            }
            return true;
        },
        onInit: function () {
            var that = this;

            // Excluded Entity Types
            window.contentStoreGlobals.allEntityTypesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Id"))
                    .text(t.get("DisplayName"));
                that.$el.find('#edit-content-store-excluded-types').append(optionEl);
            });
            this.excludedEntityTypesWidget = this.$el.find('#edit-content-store-excluded-types').multipleSelect({
                placeholder: "No entity types excluded",
                selectAll: false,
                onClick: function () {
                    that.updateExcludedEntityTypesFromDropdown();
                }
            });
            if (this.isValidConnectorSetting("EXCLUDED_ENTITY_TYPES")) {
                this.excludedEntityTypesWidget.multipleSelect("setSelects", JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_ENTITY_TYPES").get("Value")));
            }

            // Excluded Field Types
            this.excludedFieldTypesWidget = this.$el.find('#edit-content-store-excluded-field-types').multipleSelect({
                placeholder: "No field types excluded",
                selectAll: false,
                onClick: function () {
                    that.updateExcludedFieldTypesFromDropdown();
                }
            });
            this.updateExcludedFieldTypesFilter();

            // Excluded Link Types
            window.contentStoreGlobals.allLinkTypesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Id"))
                    .text(t.toString());
                that.$el.find('#edit-content-store-excluded-link-types').append(optionEl);
            });
            this.excludedLinkTypesWidget = this.$el.find('#edit-content-store-excluded-link-types').multipleSelect({
                placeholder: "No link types excluded",
                selectAll: false,
                onClick: function () {
                    that.updateExcludedLinkTypesFromDropdown();
                }
            });
            if (this.isValidConnectorSetting("EXCLUDED_LINK_TYPES")) {
                this.excludedLinkTypesWidget.multipleSelect("setSelects", JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_LINK_TYPES").get("Value")));
            }

            // Languages
            window.contentStoreGlobals.allLanguagesCollection.each(function (t) {
                var optionEl = $("<option></option>")
                    .attr("value", t.get("Name"))
                    .text(t.get("DisplayName"));
                that.$el.find('#edit-content-store-languages').append(optionEl);
            });
            this.languagesWidget = this.$el.find('#edit-content-store-languages').multipleSelect({
                selectAll: false,
                onClick: function () {
                    that.updateLanguagesFromDropdown();
                }
            });
            if (!this.isValidConnectorSetting("LANGUAGE_CONFIGURATION")) {
                // Select all by default
                this.connectorSettingsCollection.add({ Key: "LANGUAGE_CONFIGURATION", Value: JSON.stringify(window.contentStoreGlobals.allLanguagesCollection.pluck("Name")) }, { merge: true });
            }
            this.languagesWidget.multipleSelect("setSelects", JSON.parse(this.connectorSettingsCollection.get("LANGUAGE_CONFIGURATION").get("Value")));

            if (this.connectorSettingsCollection.get("URL")) {
                this.$el.find("#edit-content-store-resturl").val(this.connectorSettingsCollection.get("URL").get("Value"));
            } else {
                this.$el.find("#edit-content-store-resturl").val("");
            }

            if (this.connectorSettingsCollection.get("MANAGEMENT_APIKEY")) {
                this.$el.find("#edit-content-store-apikey").val(this.connectorSettingsCollection.get("MANAGEMENT_APIKEY").get("Value"));
            }

            // Resource Configuration
            if (!this.connectorSettingsCollection.get("RESOURCE_CONFIGURATION")) {
                this.connectorSettingsCollection.add({ Key: "RESOURCE_CONFIGURATION", Value: window.contentStoreGlobals.allImageConfigurationsCollection.first().get("Id") });
            }
            this.imageConfigurationForm = new Backbone.Form({
                model: this.connectorSettingsCollection.get("RESOURCE_CONFIGURATION"),
                schema: {
                    Value: { title: "Resource Configuration", type: 'Select', options: window.contentStoreGlobals.allImageConfigurationsCollection },
                }
            }).render();
            this.imageConfigurationForm.on("change", function (e) {
                this.commit();
                this.model.save();
            });
            this.$el.find("#edit-content-store-resource-configuration-form-container").html(this.imageConfigurationForm.el);

            // Initing finished
            this.listenTo(this.connectorSettingsCollection, 'add change:Value', this.updateExcludedFieldTypesFilter);
        },
        updateExcludedEntityTypesFromDropdown: function () {
            var selection = this.excludedEntityTypesWidget.multipleSelect('getSelects');
            this.connectorSettingsCollection.add({ Key: "EXCLUDED_ENTITY_TYPES", Value: JSON.stringify(selection) }, { merge: true });
            this.connectorSettingsCollection.get("EXCLUDED_ENTITY_TYPES").save();
        },
        updateExcludedLinkTypesFromDropdown: function () {
            var selection = this.excludedLinkTypesWidget.multipleSelect('getSelects');
            this.connectorSettingsCollection.add({ Key: "EXCLUDED_LINK_TYPES", Value: JSON.stringify(selection) }, { merge: true });
            this.connectorSettingsCollection.get("EXCLUDED_LINK_TYPES").save();
        },
        updateApiKey: function () {

            var newApiKey = this.$el.find('#edit-content-store-apikey').val();
            this.connectorSettingsCollection.get("MANAGEMENT_APIKEY").set("Value", newApiKey);

            this.connectorSettingsCollection.get("MANAGEMENT_APIKEY").save();
        },
        updateRestUrl: function () {
            var newUrl = this.$el.find('#edit-content-store-resturl').val();
            this.connectorSettingsCollection.get("URL").set("Value", newUrl);

            this.connectorSettingsCollection.get("URL").save();
        },
        updateExcludedFieldTypesFromDropdown: function () {
            // Get settings object
            var excludedFieldTypes = {};
            if (this.isValidConnectorSetting(this.connectorSettingsCollection.get("EXCLUDED_FIELD_TYPES"))) {
                excludedFieldTypes = JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_FIELD_TYPES").get("Value"));
                if (Object.prototype.toString.call(excludedFieldTypes) !== "[object Object]") excludedFieldTypes = {}; // We got a corrupt setting, reset it!
            }

            // Store new values for this entity type
            var currentEntityType = this.$el.find('#edit-content-store-excluded-field-types-filter').val();
            var selection = this.excludedFieldTypesWidget.multipleSelect('getSelects');
            excludedFieldTypes[currentEntityType] = selection;

            // Save
            this.connectorSettingsCollection.add({ Key: "EXCLUDED_FIELD_TYPES", Value: JSON.stringify(excludedFieldTypes) }, { merge: true });
            this.connectorSettingsCollection.get("EXCLUDED_FIELD_TYPES").save();
        },
        updateLanguagesFromDropdown: function () {
            var selection = this.languagesWidget.multipleSelect('getSelects');
            this.connectorSettingsCollection.add({ Key: "LANGUAGE_CONFIGURATION", Value: JSON.stringify(selection) }, { merge: true });
            this.connectorSettingsCollection.get("LANGUAGE_CONFIGURATION").save();
        },
        onEditMappings: function () {
            var pop = new modalPopup();
            pop.popOut(new mappingView({
                parent: this
            }), { size: "medium", header: "Rename fields", usesavebutton: true }); //Mappings
            $("#save-form-button").removeAttr("disabled");
        },
        onRestartConnector: function () {
            var that = this;
            this.$el.find("#restart-connector-spinner").show();
            var connectorId = window.contentStoreGlobals.currentContentStoreModel.get("connectorId");

            $.ajax({
                type: "GET",
                url: "/api/connector/stop/" + connectorId,
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert("Failed to stop connector!");
                that.$el.find("#restart-connector-spinner").hide();
            }).done(function () {
                $.ajax({
                    type: "GET",
                    url: "/api/connector/start/" + connectorId,
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert("Failed to start connector!");
                    that.$el.find("#restart-connector-spinner").hide();
                }).done(function () {
                    that.$el.find("#restart-connector-spinner").hide();
                });
            });
        },
        render: function () {
            this.$el.html(this.template({ data: window.contentStoreGlobals.currentContentStoreModel.toJSON() }));
        }
    });

    return dataTransferConfgurationView;
});