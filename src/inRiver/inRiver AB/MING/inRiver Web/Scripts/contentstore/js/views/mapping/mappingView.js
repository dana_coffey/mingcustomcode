﻿define([
  'jquery',
  'underscore',
  'backbone',
  'views/mapping/mappingSubView',
  'text!templates/mapping/mappingsTemplate.html',
], function ($, _, backbone, mappingSubView, mappingsTemplate) {

    var mappingView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.parent = options.parent;
            this.template = _.template(mappingsTemplate);

            if (!this.parent.connectorSettingsCollection.get("NEW_NAME_MAPPING")) {
                this.parent.connectorSettingsCollection.add({ Key: "NEW_NAME_MAPPING", Value: JSON.stringify({}) });
            }
            this.mappings = JSON.parse(this.parent.connectorSettingsCollection.get("NEW_NAME_MAPPING").get("Value"));

            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.render();
            this.updateUi();
        },
        events: {
            "change #add-new-mapping-entitytype-dropdown": "fillFieldTypesDropdown",
            "change #add-new-mapping-fieldtype-dropdown": "validateAddNewRow",
            "click #start-new-mapping": "onStartNewMapping",
            "click #cancel-new-mapping": "onCancelNewMapping",
            "click #add-new-mapping": "onAddNewMapping",
            "change #add-new-mapping-value": "validateAddNewRow",
            "keyup #add-new-mapping-value": "validateAddNewRow",
            "blur #add-new-mapping-value": "validateAddNewRow",
        },
        onStartNewMapping: function () {
            this.$el.find("#start-new-mapping").toggle();
            this.$el.find("#add-new-mapping-row").toggle();

            this.$el.find("#add-new-mapping-value").val("New name");
            this.validateAddNewRow();
        },
        onCancelNewMapping: function () {
            this.$el.find("#start-new-mapping").toggle();
            this.$el.find("#add-new-mapping-row").toggle();
        },
        onAddNewMapping: function () {
            this.$el.find("#start-new-mapping").toggle();
            this.$el.find("#add-new-mapping-row").toggle();

            //this.$el.find("#add-new-mapping-row").val("New name");
            var selectedEntityType = this.$el.find("#add-new-mapping-entitytype-dropdown").val();
            var selectedFieldType = this.$el.find("#add-new-mapping-fieldtype-dropdown").val();

            if (!this.mappings[selectedEntityType]) this.mappings[selectedEntityType] = {};

            this.mappings[selectedEntityType][selectedFieldType] = this.$el.find("#add-new-mapping-value").val();

            this.updateUi();
        },
        validateAddNewRow: function () {
            var isOk = this.$el.find("#add-new-mapping-entitytype-dropdown").val() && this.$el.find("#add-new-mapping-fieldtype-dropdown").val() && this.$el.find("#add-new-mapping-value").val();

            if (isOk) {
                this.$el.find("#add-new-mapping").removeAttr('disabled');
            } else {
                this.$el.find("#add-new-mapping").attr('disabled', 'disabled');
            }
        },
        updateUi: function () {
            var that = this;

            // Entity type dropdown
            var nonExcludedEntityTypes = this.parent.getNonExcludedEntityTypes();
            this.$el.find("#add-new-mapping-entitytype-dropdown").html("");
            _.each(nonExcludedEntityTypes, function (m) {
                var optionEl = $("<option></option>").attr("value", m.get("Id")).text(m.get("Id"));
                that.$el.find("#add-new-mapping-entitytype-dropdown").append(optionEl);
            });
            if (nonExcludedEntityTypes.length > 0) { // Select first entry
                this.$el.find("#add-new-mapping-entitytype-dropdown").val(nonExcludedEntityTypes[0].get("Id"));
            }

            this.fillFieldTypesDropdown();

            // Display current mappings
            this.$el.find("#mapping-list").html("");
            _.each(this.mappings, function (e, entityType) {
                _.each(e, function (value, fieldType) {
                    that.$el.find("#mapping-list").append(new mappingSubView({
                        entityType: entityType,
                        fieldType: fieldType,
                        value: value,
                        parent: that,
                    }).el);
                });
            });
        },
        fillFieldTypesDropdown: function () {
            var that = this;
            var selectedEntityType = this.$el.find("#add-new-mapping-entitytype-dropdown").val();

            // Download the full field type list for the current entity type, and then populate the dropdowns
            var fieldTypesCollection = new backbone.Collection(null, { url: "/api/fieldtype/" + selectedEntityType, model: backbone.Model.extend({ idAttribute: "Id" }) });
            fieldTypesCollection.fetch({
                reset: true,
                success: function (c) {
                    // Get a list of field types where the excluded types are removed
                    var nonExcludedFieldTypes = that.parent.getNonExcludedFieldTypes(selectedEntityType, fieldTypesCollection);

                    // Excluded field types we alrady have mappings for
                    var fieldTypesNotAlreadyMapped = nonExcludedFieldTypes;
                    if (that.mappings[selectedEntityType]) {
                        var mappedFieldTypes = Object.keys(that.mappings[selectedEntityType]);
                        fieldTypesNotAlreadyMapped = _.reject(fieldTypesNotAlreadyMapped, function (v) {
                            return _.contains(mappedFieldTypes, v.get("id"));
                        });
                    }

                    // Populate the field type dropdown
                    that.$el.find("#add-new-mapping-fieldtype-dropdown").html("");
                    _.each(fieldTypesNotAlreadyMapped, function (m) {
                        var optionEl = $("<option></option>")
                            .attr("value", m.get("id"))
                            .text(m.get("id"));
                        that.$el.find('#add-new-mapping-fieldtype-dropdown').append(optionEl);
                    });

                    that.validateAddNewRow();
                }
            });
        },
        onSave: function () {
            var that = this;
            this.parent.connectorSettingsCollection.add({ Key: "NEW_NAME_MAPPING", Value: JSON.stringify(this.mappings) }, { merge: true });
            this.parent.connectorSettingsCollection.get("NEW_NAME_MAPPING").save(null, {
                success: function (model, response) {
                    that.done();
                }
            });
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        removeMapping: function (entityType, fieldType) {
            delete this.mappings[entityType][fieldType];
            if (Object.keys(this.mappings[entityType]).length == 0) {
                delete this.mappings[entityType];
            }
            this.updateUi();
        },
        changeMappingValue: function (entityType, fieldType, value) {
            this.mappings[entityType][fieldType] = value;
        },
        render: function () {
            this.$el.html(this.template());
        }
    });

    return mappingView;
});