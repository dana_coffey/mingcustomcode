define([
  'jquery',
  'underscore',
  'backbone',
  'models/xConnectUserModel'
], function ($, _, backbone, xConnectUserModel) {
    var xConnectUserCollection = backbone.Collection.extend({
        model: xConnectUserModel,
        url: "/api/xconnect/user"
    });
    return xConnectUserCollection;
});
