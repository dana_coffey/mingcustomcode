define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var mappingModel = Backbone.Model.extend({
        initialize: function (options) {
            this.supplierId = options.supplierId;
        },
        urlRoot: function () {
            return "/api/xconnect/supplier/" + this.supplierId + "/mapping";
        },
    });

    return mappingModel;

});
