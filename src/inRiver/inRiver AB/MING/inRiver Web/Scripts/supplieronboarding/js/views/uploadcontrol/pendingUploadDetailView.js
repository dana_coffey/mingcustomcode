define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'collections/pendinguploads/pendingUploadEntityCollection',
  'views/uploadcontrol/pendingUploadEntityDetailView',
  'text!templates/uploadcontrol/pendingUploadDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, pendingUploadEntityCollection,pendingUploadEntityDetailView, pendingUploadDetailTemplate) {

        var pendingBatchView = backbone.View.extend({
            initialize: function (options) {
                var self = this; 
                this.model = options.model;
                this.parent = options.parent;
                this.supplierId = this.parent.supplierId; 

                this.batchId = this.model.attributes.batchId;
                this.entityCollection = new pendingUploadEntityCollection({ supplierId: this.supplierId, pendingImportId: this.model.id });

                this.entityCollection.fetch({
                    success: function() {
                        self.render();
                    }
                }); 
        },
        events: {
            "click #import-all": "doImportAll",
            "click #reject-all": "doRejectAll",
        },
        
        doImportAll: function () {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");


            self.$el.find("#cardarea").addClass("saving");
            self.$el.find("#detailarea").addClass("saving");

            this.$el.find("#loading").show();
            $.ajax({
                type: "POST",
                url: "/api/xconnectimportdata/" + self.supplierId + "/" + self.model.id,
                data:  JSON.stringify(self.entityCollection),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            }).fail(function () {
                self.$el.find("#cardarea").removeClass("saving");
                self.$el.find("#detailarea").removeClass("saving");

                alert("Failed to import entities!");

                self.$el.find("#import").removeClass("disabled");
                self.$el.find("#dispose").removeClass("disabled");
                self.$el.find("#loading").hide();

            }).done(function () {
              clearInterval(updater); 
                inRiverUtil.Notify("Entities imported successfully");
                self.parent.unbind();
                self.parent.remove();

                appHelper.event_bus.trigger('closepopup');
                appHelper.event_bus.trigger('historyupdated');

            });

           var updater =  window.setInterval(
                function() {
                    $.ajax({
                        type: "GET",
                        url: "/api/xconnectimportdata/",
                    }).fail(function() {
                        self.$el.find("#cardarea").removeClass("saving");
                        alert("Failed to delete connector!");
                    }).done(function(text) {
                        self.$el.find("#loading h2").html("Importing " + text + " entities");
                    });
                }, 2000);

        },
        doRejectAll: function() {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");

            inRiverUtil.NotifyConfirm("Discard Pending Import", "Do you want to discard this pending import", this.rejectedConfirmed, this);
        },
        rejectedConfirmed: function (x) {
            x.model.destroy({
                url: "/api/xconnect/supplier/" + x.model.attributes.supplierId + "/pending/" + x.model.attributes.id,
                success: function () {
                    inRiverUtil.Notify("Pending import discarded");
                    x.parent.unbind();
                    x.parent.remove();

                    window.appHelper.event_bus.trigger('closepopup');
                }

            });
        },
       render: function ( ) {
           var self = this;
           var batchId = self.model.attributes.batchId;
           this.$el.html(_.template(pendingUploadDetailTemplate, { model: this.model.toJSON() }));

           _.each(this.entityCollection.models, function (entity) {

               self.$el.find("#cardarea").append(new pendingUploadEntityDetailView({ model: entity }).$el);
               

           });

           this.$el.find("#loading").hide(); 
           return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
