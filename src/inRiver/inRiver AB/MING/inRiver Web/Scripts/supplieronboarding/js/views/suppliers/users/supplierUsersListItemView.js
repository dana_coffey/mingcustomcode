﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'views/suppliers/users/supplierEditUserView',
  'text!templates/suppliers/users/supplierUserListItemTemplate.html',
], function ($, _, Backbone, modalPopup, inRiverUtil, supplierEditUserView, supplierUserListItemTemplate) {

    var supplierUsersListView = Backbone.View.extend({
        tagName: "li",
        initialize: function (options) {

            this.currentSupplierModel = options.currentSupplierModel;

            this.template = _.template(supplierUserListItemTemplate),
            this.model = options.model;
            this.model.on('change', this.render, this);
            this.render();
        },
        events: {
            "change #is-assigned": "onChangeIsAssigned",
            "click #list-item-edit": "onEditUser",
            "click #list-item-delete": "onDeleteUser",
            "click .xconnect-list-item-label": "onEditUser"
        },
        onChangeIsAssigned: function (e) {
            var self = this;

            var isAssigned = this.$el.find("#is-assigned").prop('checked');
            if (!isAssigned) {
                this.model.set("supplierSitesId", _.reject(this.model.get("supplierSitesId"), function (item) {
                    return item.supplierId === self.currentSupplierModel.get("id");
                }));
            } else {

                if (this.model.get("supplierSitesId") == null) {
                    this.model.set("supplierSitesId", []);
                }

                this.model.get("supplierSitesId").push({ supplierId: self.currentSupplierModel.get("id") });
            }
            this.model.save();
        },
        onEditUser: function (e) {

            e.stopPropagation();

            var pop = new modalPopup();
            pop.popOut(new supplierEditUserView({
                model: this.model
            }), { size: "small", header: "Edit User", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        onDeleteUser: function () {
            var self = this;
            inRiverUtil.NotifyConfirm("Delete User", "Are you sure you want to delete this user?<br><br>The user will not be able to use any inRiver xConnect features at all.", function () {
                self.model.destroy({
                    success: function () {
                        self.undelegateEvents();
                        self.$el.removeData().unbind();
                        self.remove();
                        Backbone.View.prototype.remove.call(self);
                    }
                });
            }, this);
        },
        render: function () {
            var self = this;
            this.$el.html(this.template({ data: this.model.toJSON() }));

            var isUserAssignedToCurrentSupplier = _.contains(_.pluck(this.model.get("supplierSitesId"), "supplierId"), self.currentSupplierModel.get("id"));
            this.$el.find("#is-assigned").prop('checked', isUserAssignedToCurrentSupplier);
        }
    });

    return supplierUsersListView;

});