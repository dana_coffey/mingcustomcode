﻿define([
  'jquery',
  'underscore',
  'backbone',
  'colpick',
  'multiple-select',
  'simpleAjaxUploader',
  'sharedjs/collections/users/usersCollection',
  'sharedjs/models/setting/SettingModel',
  'text!templates/suppliers/settings/supplierSettingsTemplate.html',

], function ($, _, Backbone, colpick, multipleSelect, simpleAjaxUploader, usersCollection, SettingModel, supplierSettingsTemplate) {

    var userCollection = null;

    var supplierSettingsView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.currentSupplierModel = options.currentSupplierModel;
            this.supplierCollection = options.supplierCollection;

            this.render();

            this.currentSupplierModel.on("change:textColor", this.updateTextColor, this);
            this.currentSupplierModel.on("change:backgroundColor", this.updateBackground, this);

            this.createColorCtl(this.$el.find("#edit-supplier-onboarding-pick-background-color"), "backgroundColor");
            this.createColorCtl(this.$el.find("#edit-supplier-onboarding-pick-text-color"), "textColor");

            this.uploader = new ss.SimpleUpload({
                button: this.$el.find("#edit-supplier-onboarding-file-upload"),
                url: '/api/xconnect/supplier/' + self.currentSupplierModel.get("id") + '/logo',
                //name: 'uploadfile', // Parameter name of the uploaded file
                multipart: true,
                onSubmit: function (filename, extension) {
                    self.$el.find("#file-upload-spinner").show();
                    self.currentSupplierModel.set("logoName", filename);
                },
                onComplete: function (filename, response) {
                    self.$el.find("#file-upload-spinner").hide();
                    if (!response) {
                        alert('upload failed');
                    } else {
                        self.$el.find("#edit-supplier-onboarding-logo-preview").hide();
                        self.currentSupplierModel.set("logoUrl", JSON.parse(response).logoUrl);
                        self.updateLogo();
                    }
                }
            });

            userCollection = new usersCollection();
            userCollection.fetch(
            {
                success: function () {
                    self.updateUi();

                    $("#suppliers-settings-section .loadingspinner").remove();
                    $("div#suppliers-settings-section .box-content").show();
                }
            });
        },
        events: {
            "change #edit-supplier-onboarding-name": "onChangeName",
            "keyup #edit-supplier-onboarding-name": "onChangeName",
            "blur #edit-supplier-onboarding-name": "onChangeNameFinished",
            "click #edit-supplier-onboarding-pick-background-color": "onPickBackgroundColor",
            "click #edit-supplier-onboarding-pick-text-color": "onPickTextColor",
            "change #edit-supplier-onboarding-responsible": "onChangeResponsible",
            "change #edit-supplier-onboarding-relay": "onChangeRelay",
            "keyup #edit-supplier-onboarding-relay": "onChangeRelay"
        },
        onChangeName: function (e) {
            var self = this;
            this.currentSupplierModel.set({ name: this.$el.find("#edit-supplier-onboarding-name").val() });
            this.currentSupplierModel.save();
        },
        onChangeNameFinished: function (e) {
            this.currentSupplierModel.trigger("onChangeNameFinished");
        },
        onPickBackgroundColor: function (e) {
            this.$el.find("#edit-supplier-onboarding-pick-background-color").colpickShow();
        },
        onPickTextColor: function (e) {
            this.$el.find("#edit-supplier-onboarding-pick-text-color").colpickShow();
        },
        onChangeRelay: function (e) {
            this.currentSupplierModel.attributes.relay = this.$el.find("#edit-supplier-onboarding-relay")[0].checked;
            this.currentSupplierModel.save();
        },
        createColorCtl: function (el, attributeName) {
            var self = this;

            el.colpick({
                color: self.currentSupplierModel.get(attributeName),
                layout: 'hex',
                submit: 0,
                onChange: function (hsb, hex, rgb, elem, bySetColor) {
                    var map = {};
                    map[attributeName] = "#" + hex;
                    self.currentSupplierModel.set(map);
                    self.currentSupplierModel.save();
                }
            }).keyup(function () {
                var v = $(this).find("input").val();
                if (/(^#[0-9A-F]{6}$)/i.test(v)) {
                    $(this).colpickSetColor(v);
                }
            });
        },
        updateUi: function () {
            var self = this;
            this.$el.find("#edit-supplier-onboarding-name").val(this.currentSupplierModel.get("name"));

            var setModel = new SettingModel();
            setModel.url = "/api/setting/SUPPLIERONBOARDING_RESPONSIBLE_GROUPS";
            setModel.fetch({
                success: function (model, response) {
                    if (response) {
                        var rolesToDisplay = response.split(',');

                        self.createResponsibleDropdownWithGroups(rolesToDisplay);
                    }
                    else {
                        self.createResponsibleDropdown();
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });


            this.$el.find("#edit-supplier-onboarding-relay").prop("checked", this.currentSupplierModel.get("relay") == 1);

            if (this.currentSupplierModel.get("backgroundColor") != null) {
                var tmpEl1 = this.$el.find("#edit-supplier-onboarding-pick-background-color");
                tmpEl1.find("input").css('border-color', this.currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").val(this.currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").text(this.currentSupplierModel.get("backgroundColor"));
            }

            if (this.currentSupplierModel.get("textColor") != null) {
                var tmpEl2 = this.$el.find("#edit-supplier-onboarding-pick-text-color");
                tmpEl2.find("input").css('border-color', this.currentSupplierModel.get("textColor"));
                tmpEl2.find("input").val(this.currentSupplierModel.get("textColor"));
            }

            if (this.currentSupplierModel.get("logoUrl") != null) {
                this.$el.find("#edit-supplier-onboarding-logo-preview").attr("src", "/api/xconnect" + this.currentSupplierModel.get("logoUrl") + "?maxwidth=100&maxheight=100");
                this.$el.find("#edit-supplier-onboarding-logo-preview").show();
            } else {
                this.$el.find("#edit-supplier-onboarding-logo-preview").hide();
            }
        },
        createResponsibleDropdownWithGroups: function (rolesToDisplayArray) {
            var self = this;

            var uniqueRoles = [];

            _.each(userCollection.models, function (user) {
                _.each(user.get("Roles"), function (role) {
                    if (self.listContainsRoleId(uniqueRoles, role.Id).length == 0) {
                        uniqueRoles.push(role);
                    }
                });
            });

            var responsibleDropdownEl = self.$el.find("#edit-supplier-onboarding-responsible");

            var selectedOptions = [];

            _.each(uniqueRoles, function (role) {

                if (rolesToDisplayArray.length > 0 && !_.contains(rolesToDisplayArray, role.Name)) {
                    return;
                }

                optgroup = $("<optgroup></optgroup>").attr("id", role.Id).attr("label", role.Name);

                responsibleDropdownEl.append(optgroup);

                var roleUsers = self.filterUsersWithRoleId(userCollection.models, role.Id);

                var allUsersCheckedForGroup = roleUsers.length;
                var countUserForGroup = 0;

                _.each(roleUsers, function (user) {
                    var optionEl;
                    if (self.currentSupplierModel.get("responsible") != null) {
                        if (_.find(self.currentSupplierModel.get("responsible").split(","), function (name) {
                            return name == user.attributes.Username;
                        })) {
                            optionEl = $("<option></option>")
                            .attr("value", user.attributes.Username)
                            .attr("selected", "selected")
                            .text(user.attributes.FirstName + " " + user.attributes.LastName);

                            if (!_.contains(selectedOptions, user.attributes.Username)) {
                                selectedOptions.push(user.attributes.Username);
                            }

                            countUserForGroup++;
                        }
                        else {
                            optionEl = $("<option></option>").attr("value", user.attributes.Username).text(user.attributes.FirstName + " " + user.attributes.LastName);
                        }
                    }
                    else {
                        optionEl = $("<option></option>").attr("value", user.attributes.Username).text(user.attributes.FirstName + " " + user.attributes.LastName);
                    }

                    optgroup.append(optionEl);
                });

                if (allUsersCheckedForGroup == countUserForGroup) {
                    optgroup.attr('selected', true);
                }

            });

            var valuesInDropdownShouldBeIndented = true;

            self.instantiateMultipleSelect(valuesInDropdownShouldBeIndented, selectedOptions);
        },
        filterUsersWithRoleId: function (collection, id) {

            var usersForRole = [];

            _.each(collection, function (user) {

                var roleExists = _.find(user.get("Roles"), function (role) {

                    return role.Id === id;
                });

                if (roleExists) {
                    usersForRole.push(user);
                }

            });

            return usersForRole;
        },
        listContainsRoleId: function (arr, id) {

            var matches = _.filter(arr, function (value) {
                if (value.Id == id) {
                    return value;
                }
            });

            return matches;
        },
        createResponsibleDropdown: function () {
            var self = this;

            this.$el.find("#edit-supplier-onboarding-responsible").html("");
            _.each(userCollection.models, function (user) {
                var selected = "";
                if (self.currentSupplierModel.get("responsible") != null) {
                    if (_.find(self.currentSupplierModel.get("responsible").split(","), function (name) {
                        return name == user.attributes.Username;
                    })) {
                        selected = " selected=\"selected\"";
                    }
                }
                self.$el.find("#edit-supplier-onboarding-responsible").append("<option value=\"" + user.attributes.Username + "\"" + selected + ">" + user.attributes.FirstName + " " + user.attributes.LastName + "</option>");
            });

            var valuesInDropdownShouldBeIndented = false;

            self.instantiateMultipleSelect(valuesInDropdownShouldBeIndented);
        },
        instantiateMultipleSelect: function (containsGroups, selectedOptions) {
            var self = this;

            this.$el.find("#edit-supplier-onboarding-responsible").multipleSelect({
                width: 180,
                multipleWidth: 150,
                multiple: true,
                filter: true,
                styler: function (value) {
                    //Check if multiple select contains groups. 
                    //If not we dont want the values in dropdown to be indented.
                    if (containsGroups) {
                        return "margin-left:15px;";
                    }
                },
                onClick: function (e) {
                    var responsible = self.$el.find("#edit-supplier-onboarding-responsible").val();

                    if (responsible != null) {

                        var uniqueArray = responsible.filter(function (item, pos) {

                            if (!e.checked && item == e.value) {
                                return;
                            }

                            return responsible.indexOf(item) == pos;
                        })

                        self.currentSupplierModel.set({ responsible: uniqueArray.toString() });
                        self.currentSupplierModel.save();

                        self.$el.find("#edit-supplier-onboarding-responsible").multipleSelect('setSelects', uniqueArray);
                    }
                },
                onOptgroupClick: function (e) {
                    var responsible = self.$el.find("#edit-supplier-onboarding-responsible").val();

                    if (responsible != null) {

                        var uniqueArray = responsible.filter(function (item, pos) {

                            if (!e.checked && _.contains(view.children, item)) {
                                return;
                            }

                            return responsible.indexOf(item) == pos;
                        })

                        self.currentSupplierModel.set({ responsible: uniqueArray.toString() });
                        self.currentSupplierModel.save();

                        self.$el.find("#edit-supplier-onboarding-responsible").multipleSelect('setSelects', uniqueArray);
                    }
                }
            });

            this.$el.find("#edit-supplier-onboarding-responsible").val(this.currentSupplierModel.get("responsible"));
        },
        updateBackground: function () {
            if (this.currentSupplierModel.get("backgroundColor") != null) {
                var tmpEl1 = this.$el.find("#edit-supplier-onboarding-pick-background-color");
                tmpEl1.find("input").css('border-color', this.currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").val(this.currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").text(this.currentSupplierModel.get("backgroundColor"));
            }
        },
        updateTextColor: function () {

            if (this.currentSupplierModel.get("textColor") != null) {
                var tmpEl2 = this.$el.find("#edit-supplier-onboarding-pick-text-color");
                tmpEl2.find("input").css('border-color', this.currentSupplierModel.get("textColor"));
                tmpEl2.find("input").val(this.currentSupplierModel.get("textColor"));
            }
        },
        updateLogo: function () {
            if (this.currentSupplierModel.get("logoUrl") != null) {
                this.$el.find("#edit-supplier-onboarding-logo-preview").attr("src", "/api/xconnect" + this.currentSupplierModel.get("logoUrl") + "?maxwidth=100&maxheight=100");
                this.$el.find("#edit-supplier-onboarding-logo-preview").show();
            } else {
                this.$el.find("#edit-supplier-onboarding-logo-preview").hide();
            }
        },

        render: function () {
            var self = this;

            this.$el.empty();

            var sectionEl = $(_.template(supplierSettingsTemplate, { data: this.currentSupplierModel.toJSON() }));
            self.$el.append(sectionEl);
        }
    });

    return supplierSettingsView;

});