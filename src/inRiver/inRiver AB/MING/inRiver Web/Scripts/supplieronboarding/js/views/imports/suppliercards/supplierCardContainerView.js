﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'views/imports/suppliercards/supplierCardView',
  'text!templates/imports/suppliercards/supplierCardContainerTemplate.html',
], function ($, _, Backbone, inRiverUtil, supplierCardView, supplierCardContainerTemplate) {

    var supplierCardContainerView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.supplier = options.supplier;
        },
        events: {

        },
        render: function () {
            var self = this;
            
            var sectionEl = $(_.template(supplierCardContainerTemplate, { supplierId: this.supplier.id }));
            this.$el.append(sectionEl);

            var supplierCard = new supplierCardView({ supplier: this.supplier });
            this.$el.find("#supplier-card-" + this.supplier.id).append(supplierCard.el);

            return this;
        }
    });

    return supplierCardContainerView;
});