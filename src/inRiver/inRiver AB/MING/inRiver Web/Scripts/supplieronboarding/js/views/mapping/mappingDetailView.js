define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
    'sharedjs/misc/inRiverUtil',
 'text!templates/mapping/mappingDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, mappingDetailTemplate) {

        var pendingBatchView = backbone.View.extend({
            initialize: function (options) {
                var self = this; 
                this.model = options.model;
                this.parent = options.parent;
                this.supplierId = this.parent.supplierId;

                this.render();
                this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
                this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);
            },
        events: {
            "click #import": "doImport",
            "click #dispose": "doDispose",
        },
        
        onSave: function () {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");


            this.model.attributes.name = this.$el.find("#name").val(); 

            this.model.save([], {
                
                success: function() {
                    self.done();
                    //self.parent.model = self.model;
                    self.parent.render(); 

                    inRiverUtil.Notify("Mapping updated");
                }
            }); 

        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            window.appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
       render: function ( ) {
           var self = this;
           this.$el.html(_.template(mappingDetailTemplate, { model: this.model.toJSON() }));

          return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
