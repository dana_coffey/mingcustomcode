define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'collections/pendinguploads/pendingUploadCollection',
  'views/imports/pendingimports/importsPendingImportsView',
  'text!templates/imports/pendingimports/importsPendingImportsListTemplate.html'
], function ($, _, backbone, alertify, pendingUploadCollection, importsPendingImportsView, importsPendingImportsListTemplate) {

    var pendingBatchesView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.suppliers = options.suppliers;                        
            this.collection = new backbone.Collection();

            if (this.suppliers.length == 0)
            {
                this.render();
            }
            else {
                _.each(this.suppliers.models, function (supplier) {

                    var supplierPendingCollection = new pendingUploadCollection({ id: supplier.id });

                    supplierPendingCollection.fetch(
                    {
                        success: function () {
                            self.collection.add(supplierPendingCollection);

                            if (self.collection.length == self.suppliers.length) {
                                self.render();
                            }
                        }
                    });

                });
            }
        },
        events: {
            "click .connector-event-wrap": "onEventClick"
        },
        onEventClick: function (e) {
            e.stopPropagation();
        },
        render: function ( ) {
            var self = this; 

            $("#imports-pending-section .loadingspinner").remove();
            this.$el.empty();

            if (this.collection.length == 0) {
                $("#imports-pending-section").html("<div class='no-items-text'>Select supplier to show pending uploads.</div>");
            }
            else {

                var sectionEl = $(_.template(importsPendingImportsListTemplate, {}));
                self.$el.append(sectionEl);

                var pendingUploads = [];

                _.each(this.collection.models, function (supplier) {
                    _.each(supplier.get("models"), function (pendingUploadModel) {
                        pendingUploads.push(pendingUploadModel);
                    });
                });

                if (pendingUploads.length == 0) {
                    $("#imports-pending-section").html("<div class='no-items-text'>No pending imports.</div>");
                }
                else {
                    pendingUploads.sort(function (a, b) {
                        if (a.get("uploaded") > b.get("uploaded"))
                            return -1;
                        else if (a.get("uploaded") < b.get("uploaded"))
                            return 1;
                        else
                            return 0;
                    });

                    _.each(pendingUploads, function (model) {

                        var supplier = self.suppliers.get(model.get("supplierId"));

                        self.$el.find(".box-content table").append(new importsPendingImportsView({ model: model, supplierId: model.get("supplierId"), supplier: supplier }).$el);
                    });
                }
            }
            
            return this; // enable chained calls
        }
    });

    return pendingBatchesView;
});
