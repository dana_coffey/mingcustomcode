﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-steps',
  'dropzone',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/languages/LanguagesCollection',
  'models/mapping/fileImportConfigurationModel',
  'models/mapping/mappingModel',
  'text!templates/mapping/importDataTemplate.html',
  'text!templates/mapping/importConfigurationTemplate.html'
], function ($, _, backbone, jquerySteps, Dropzone, inRiverUtil, languagesCollection, fileImportConfigurationModel,
    mappingModel, importDataTemplate, importConfigurationTemplate) {

    var addMappingView = backbone.View.extend({
        initialize: function (options) {
            this.supplierId = options.supplierId;
            window.appHelper.languagesCollection = new languagesCollection();
            window.appHelper.languagesCollection.fetch();

            this.render();
        },
        events: {
            "click #showEmptyColumns": "onToggleShowEmptyColumns",
            "click #clearEmptyValue": "onToggleClearEmptyValues",
            "change #selectEntityType": "onSelectEntityType",
            "change .import-mapping-language": "onChangeLanguage",
            "change .import-mapping-fields": "onChangeField",
            "click #save-mapping": "showSaveMappingDialog",
            "click #do-save-mapping": "onSaveMapping",
            "click #do-cancel-mapping": "onCancelSaveMapping",
            "click #update-mapping": "onUpdateMapping",
            "click #delete-mapping": "onDeleteMapping",
            "change #config-mapping-name": "onMappingNameChange"
        },
        onToggleShowEmptyColumns: function () {
            var self = this;

            this.fileImportConfigurationModel.set("ShowEmptyColumns", this.$el.find("#showEmptyColumns")[0].checked);

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onChangeLanguage: function (e) {
            var newLanguage = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;

            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id == columnId;
            });

            column.LanguageCode = newLanguage;
            this.fileImportConfigurationModel.save();
        },
        onMappingNameChange: function (e) {
            this.fileImportConfigurationModel.attributes.name = this.$el.find("#config-mapping-name").val();

        },
        onChangeField: function (e) {
            var self = this;

            var newField = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;
            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id == columnId;
            });

            column.FieldTypeId = newField;

            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onToggleClearEmptyValues: function () {
            this.fileImportConfigurationModel.set("ClearEmptyValues", this.$el.find("#clearEmptyValue")[0].checked);
            this.fileImportConfigurationModel.save();
        },
        onSelectEntityType: function () {
            var self = this;

            this.fileImportConfigurationModel.set("EntityTypeId", this.$el.find("#selectEntityType").val());

            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        showSaveMappingDialog: function () {
            this.$el.find("#save-mapping-dialog").show();
            this.$el.find("#select-existing-mapping").hide();
        },
        onCancelSaveMapping: function () {
            this.$el.find("#save-mapping-dialog").hide();
            this.$el.find("#select-existing-mapping").show();
            this.$el.find("#config-mapping-name").val("");
        },
        onSaveMapping: function () {
            var self = this;

            //Hämta namn på mappningen. 
            if (this.$el.find("#config-mapping-name").val() == "") {
                this.$el.find("#save-mapping-name").addClass("error");
                this.$el.find("#config-mapping-name-error").show();
                return;
            }

            this.mappingModel = new mappingModel({ supplierId: this.supplierId });
            this.mappingModel.attributes.Name = this.$el.find("#config-mapping-name").val();
            this.mappingModel.attributes.EntityType = this.fileImportConfigurationModel.attributes.EntityTypeId;
            this.mappingModel.attributes.Columns = this.fileImportConfigurationModel.attributes.Columns;
            //   this.fileImportConfigurationMappingModel.id = this.fileImportConfigurationModel.id; 

            this.mappingModel.set("MappingName", this.$el.find("#config-mapping-name").val());
            //   this.fileImportConfigurationMappingModel.set("Action", "saveMapping");

            this.fileImportValidationModel = null;

            this.mappingModel.save(null, {
                success: function (model) {
                    inRiverUtil.Notify("Mapping successfully saved");
                    self.fileImportConfigurationModel.attributes.Id = model.attributes.Id;
                    self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                    self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                    self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;

                    appHelper.event_bus.trigger('closepopup');
                    appHelper.event_bus.trigger('mappingadded');
                    self.undelegateEvents();
                    self.$el.removeData().unbind();
                    self.remove();

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },

        onUpdateMapping: function () {
            var self = this;

            this.fileImportConfigurationMappingModel = new fileImportConfigurationMappingModel({
                MappingId: this.fileImportConfigurationModel.attributes.MappingId
            });


            this.fileImportConfigurationMappingModel.fetch({
                success: function () {
                    self.fileImportConfigurationMappingModel.attributes = self.fileImportConfigurationModel.attributes;
                    self.fileImportConfigurationMappingModel.save(null, {
                        success: function (model) {
                            inRiverUtil.Notify("Mapping successfully updated");
                            self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                            self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                            self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;
                            self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                            window.appHelper.event_bus.trigger('closepopup');
                            self.parent.reload();


                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        moveToConfiguration: function () {
            var self = this;

            this.fileImportConfigurationModel = new fileImportConfigurationModel({ Id: this.modelid });
            this.fileImportConfigurationModel.fetch({
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },

        finish: function () {
            this.onSaveMapping();


        },
        render: function () {
            var self = this;
            this.$el.html(_.template(importDataTemplate, {}));
            this.$el.find("#import-data-wizard").steps(
            {
                transitionEffect: "slideLeft",
                transitionEffectSpeed: 400,
                enableAllSteps: false,
                autoFocus: true,
                enableCancelButton: true,
                onCanceled: function () {
                    self.onCancel();
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (currentIndex == 0 && self.modelid == null) {
                        return false;
                    }

                    if (currentIndex == 0 && newIndex == 1) {
                        self.moveToConfiguration();
                    }

                    return true;
                },
                onFinished: function (event, currentIndex) {
                    self.finish();
                }
            }
            );

            this.myAwesomeDropzone = new Dropzone(this.$el.find("#import-dropzone")[0], {
                url: "/fileupload/file",
                maxFilesize: 1000,
                parallelUploads: 10,
                thumbnailWidth: 100,
                thumbnailHeight: 100,
                maxFiles: 1,
                autoProcessQueue: true,
                dictDefaultMessage: "Drop file here or Click to Browse",
                init: function () {
                    var thisDropzone = this;

                    this.on("processing", function () {
                        thisDropzone.options.autoProcessQueue = true;
                    });

                    this.on("addedfile", function (file) {
                        var files = thisDropzone.getAcceptedFiles();

                        if (files.length == 1) {
                            thisDropzone.removeFile(files[0]);
                        }

                        $("#import-dropzone").removeClass("dropzone-button");
                        $("#import-dropzone").addClass("dropzone");
                    });

                    this.on("success", function (file, id) {
                        self.fileImportValidationModel = null;
                        self.modelid = id;
                    });
                }
            });

            this.listenTo(window.appHelper.event_bus, 'validationstatus', this.validateStatus);

            return this;
        }
    });

    return addMappingView;
});
