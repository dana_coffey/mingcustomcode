define([
  'jquery',
  'underscore',
  'backbone',
  'models/pendingupload/pendigUploadEntityModel'
], function ($, _, backbone, pendigUploadEntityModel) {
    var pendingUploadEntityCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.pendingImportId = options.pendingImportId;
            this.supplierId = options.supplierId; 
        },
        model: pendigUploadEntityModel,
        url: function() {
            return "/api/xconnect/supplier/" + this.supplierId + "/pending/" + this.pendingImportId + "/entity";
            
        }
    });
    return pendingUploadEntityCollection;
});
