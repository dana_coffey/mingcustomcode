﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'collections/xConnectUserCollection',
  'models/xConnectUserModel',
  'text!templates/suppliers/users/supplierUserListTemplate.html',
  'views/suppliers/users/supplierUsersListItemView',
  'views/suppliers/users/supplierEditUserView'

], function ($, _, Backbone, modalPopup, xConnectUserCollection, xConnectUserModel, supplierUserListTemplate, supplierUsersListItemView, supplierEditUserView) {

    var userCollection = new xConnectUserCollection();

    var supplierUsersView = Backbone.View.extend({
        initialize: function (options) {
            this.$el.find("#suppliers-users-section").html();

            this.currentSupplierModel = options.currentSupplierModel;

            this.listenTo(userCollection, 'reset', this.render);
            this.listenTo(userCollection, 'add', this.render);
            this.listenTo(userCollection, 'remove', this.render);

            userCollection.fetch({ reset: true });
            //self.render();          
        },
        events: {
            "click #list-of-users-add": "onAddUser"
        },
        onAddUser: function () {
            var model = new xConnectUserModel();

            var pop = new modalPopup();
            pop.popOut(new supplierEditUserView({
                userCollection: userCollection,
                model: model
            }), { size: "small", header: "Create User", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
        render: function () {
            var self = this;

            $("#suppliers-users-section .loadingspinner").remove();
            this.$el.empty();

            var sectionEl = $(_.template(supplierUserListTemplate, { name: this.currentSupplierModel.get("name") }));
            self.$el.append(sectionEl);

            var ulEl = this.$el.find("#xconnect-user-list-wrap > ul");

            var usersList = [];

            _.each(userCollection.models, function (supplier) {
                usersList.push(supplier);
            });

            usersList.sort(function (a, b) {
                if (a.get("email") < b.get("email"))
                    return -1;
                else if (a.get("email") > b.get("email"))
                    return 1;
                else
                    return 0;
            });

            _.each(usersList, function (model) {
                var userItemView = new supplierUsersListItemView({ model: model, currentSupplierModel: self.currentSupplierModel });
                ulEl.append(userItemView.el);
            });
        }
    });

    return supplierUsersView;

});