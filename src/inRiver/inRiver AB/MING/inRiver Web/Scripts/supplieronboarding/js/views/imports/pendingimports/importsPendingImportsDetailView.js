define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/entities/entityCollection',
  'sharedjs/collections/fieldtypes/FieldTypesCollection',
  'sharedjs/collections/languages/LanguagesCollection',
  'collections/pendinguploads/pendingUploadEntityCollection',
  'views/imports/pendingimports/importsPendingImportsEntityDetailView',
  'text!templates/imports/pendingimports/importsPendingImportsDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, entityCollection, fieldTypesCollection, languagesCollection, pendingUploadEntityCollection, supplierPendingImportsUploadEntityDetailView, supplierPendingImportsUploadDetailTemplate) {

        var pendingBatchView = backbone.View.extend({
            initialize: function (options) {
                this.model = options.model;
                this.parent = options.parent;
                this.supplierId = this.parent.supplierId; 
                this.batchId = this.model.attributes.batchId;

                if (!window.appHelper.fieldTypesLoadedMap) {
                    window.appHelper.fieldTypesLoadedMap = {};
                }

                if (!window.appHelper.fieldTypeMap) {
                    window.appHelper.fieldTypeMap = {};
                }
                _.bindAll(this, "onScroll");

                this.render();
            },
        events: {
            "click #import-all": "doImportAll",
            "click #reject-all": "doRejectAll"
        },
        
        doImportAll: function () {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");


            self.$el.find("#cardarea").addClass("saving");
            self.$el.find("#detailarea").addClass("saving");

            this.$el.find("#loading").show();
            $.ajax({
                type: "POST",
                url: "/api/xconnectimportdata/" + self.supplierId + "/" + self.model.id,
                data:  JSON.stringify(self.entityCollection),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            }).fail(function (data) {
                self.$el.find("#cardarea").removeClass("saving");
                self.$el.find("#detailarea").removeClass("saving");

                alert(data.Message);

                self.$el.find("#import").removeClass("disabled");
                self.$el.find("#dispose").removeClass("disabled");
                self.$el.find("#loading").hide();

            }).done(function (data) {
                if (data.Success) {
                    clearInterval(updater);
                    inRiverUtil.Notify("Entities imported successfully");
                    self.parent.unbind();
                    self.parent.remove();

                    appHelper.event_bus.trigger('closepopup');
                    appHelper.event_bus.trigger('historyupdated', self.supplierId);
                } else {
                    self.$el.find("#cardarea").removeClass("saving");
                    self.$el.find("#detailarea").removeClass("saving");

                    alert(data.Message);

                    self.$el.find("#import").removeClass("disabled");
                    self.$el.find("#dispose").removeClass("disabled");
                    self.$el.find("#loading").hide();
                    appHelper.event_bus.trigger('historyupdated', self.supplierId);
                }
            });

           var updater =  window.setInterval(
                function() {
                    $.ajax({
                        type: "GET",
                        url: "/api/xconnectimportdata/"
                    }).fail(function() {
                        self.$el.find("#cardarea").removeClass("saving");
                        alert("Failed to import data");
                    }).done(function(text) {
                        self.$el.find("#loading h2").html("Importing " + text + " entities");
                    });
                }, 2000);

        },
        doRejectAll: function() {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");

            inRiverUtil.NotifyConfirm("Discard Pending Import", "Do you want to discard this pending import", this.rejectedConfirmed, this);
        },
        rejectedConfirmed: function (x) {
            x.model.destroy({
                url: "/api/xconnect/supplier/" + x.model.attributes.supplierId + "/pending/" + x.model.attributes.id,
                success: function () {
                    inRiverUtil.Notify("Pending import discarded");
                    x.parent.unbind();
                    x.parent.remove();

                    window.appHelper.event_bus.trigger('closepopup');
                }
            });
        },
        renderEntityViews: function (entityTypeId) {
            if (this.waitForEntityCollection
                || this.waitForLanguagesCollection
                || !this.oldEntityCollectionLoadedMap[entityTypeId]
                || !window.appHelper.fieldTypesLoadedMap[entityTypeId]) {

                return;
            }

            this.loadCards();
        },
        loadCards: function () {
            var self = this;
            var cardsLoaded = 0;
            _.each(self.entitiesByEntityTypeId, function (entities, entityTypeId) {
                if (self.oldEntityCollectionLoadedMap[entityTypeId] || window.appHelper.fieldTypesLoadedMap[entityTypeId]) {
                    _.each(entities, function (entity) {
                        if (!entity.loaded && cardsLoaded < 10) {
                            entity.loaded = true;
                            var oldEntity = self.oldEntityCollectionMap[entityTypeId].get(entity.get("entityId"));
                            if (oldEntity) {
                                entity.set("PictureUrl", oldEntity.get("PictureUrl"));
                            } else {
                                entity.set("PictureUrl", "/Images/nopicture50x50.png");   
                            }

                            self.$el.find("#cardarea").append(new supplierPendingImportsUploadEntityDetailView({ model: entity, supplierId: self.supplierId }).$el);
                            cardsLoaded++;
                        }
                    });
                }
            });
            setTimeout(function () {
                self.rendering = false;
                self.onScroll();
            }, 900);
        },
        onScroll: function () {
            var scrollTop = this.$el.find("#cardarea").scrollTop();
            var isScrollingDown = scrollTop > this.lastScrollTop;
            this.lastScrollTop = scrollTop;
            if (this.rendering) {
                return;
            }
            var self = this;
            var innerHeight = this.$el.find("#cardarea").outerHeight();
            var scrollHeight = this.$el.find("#cardarea")[0].scrollHeight;
            if (isScrollingDown && (scrollTop + innerHeight >= scrollHeight - 300) || scrollTop + innerHeight >= scrollHeight) {
                this.rendering = true;
                setTimeout(function() {
                        self.loadCards();
                    },
                    10);
            }
        },
        render: function () {
            var self = this;
            this.$el.html(_.template(supplierPendingImportsUploadDetailTemplate, { model: this.model.toJSON() }));
            self.$el.find("#cardarea").scroll(this.onScroll);
            this.waitForEntityCollection = true;
            this.waitForLanguagesCollection = !window.appHelper.languageMap;

            this.entityCollection = new pendingUploadEntityCollection({ supplierId: this.supplierId, pendingImportId: this.model.id });
            this.entityCollection.fetch({
                success: function () {
                    self.oldEntityCollectionMap = {};
                    self.oldEntityCollectionLoadedMap = {};
                    self.entitiesByEntityTypeId = self.entityCollection.groupBy(function (model) {
                        return model.get("entityTypeId");
                    });

                    self.waitForEntityCollection = false;
                    _.each(self.entitiesByEntityTypeId, function (entities, entityTypeId) {
                        var entityIds = _.map(entities, function (entity) {return entity.get("entityId")});
                        self.oldEntityCollectionMap[entityTypeId] = new entityCollection(entityIds);
                        self.oldEntityCollectionMap[entityTypeId].fetch({
                            data: {
                                type: "entities",
                                parameter: entityIds.toString()
                            },
                            type: "POST",
                            success: function () {
                                self.oldEntityCollectionLoadedMap[entityTypeId] = true;
                                self.renderEntityViews(entityTypeId);
                            }
                        });

                        if (!window.appHelper.fieldTypesLoadedMap[entityTypeId]) {
                            new fieldTypesCollection().fetch({
                                data: { id: entityTypeId },
                                success: function (collection) {
                                    window.appHelper.fieldTypeMap = collection.reduce(function (fieldTypeMap, fieldType) {
                                        fieldTypeMap[fieldType.get("id")] = fieldType;
                                        return fieldTypeMap;
                                    }, window.appHelper.fieldTypeMap);
                                    window.appHelper.fieldTypesLoadedMap[entityTypeId] = true;
                                    self.renderEntityViews(entityTypeId);
                                }
                            });
                        }
                    });
                }
            });
            if (this.waitForLanguagesCollection) {
                new languagesCollection().fetch({
                    success: function (collection) {
                        self.waitForLanguagesCollection = false;
                        window.appHelper.languageMap = collection.reduce(function (languageMap, language) {
                            languageMap[language.get("Name")] = language.get("DisplayName");
                            return languageMap;
                        }, {});
                        _.each(self.entitiesByEntityTypeId, function(entities, entityTypeId) {
                            self.renderEntityViews(entityTypeId);
                        });
                    }
                });
            }

            this.$el.find("#loading").hide();
            return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
