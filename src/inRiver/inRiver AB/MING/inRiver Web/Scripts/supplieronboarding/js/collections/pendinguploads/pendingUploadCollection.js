define([
  'jquery',
  'underscore',
  'backbone',
  'models/xConnectUserModel'
], function ($, _, backbone, xConnectUserModel) {
    var pendingUploadCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.supplierId = options.id;
        },
        model: xConnectUserModel,
        url: function() {
            return "/api/xconnect/supplier/" + this.supplierId + "/pending";
        }
    });
    return pendingUploadCollection;
});
