define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'collections/history/historyCollection',
    'views/historycontrol/historyView',

  'text!templates/historycontrol/historyListTemplate.html'
], function ($, _, backbone, alertify, historyCollection, historyView, historyTemplate) {

    var historyListView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.supplierId = options.id;
            this.collection = new historyCollection({ id: this.supplierId }); 

            var self = this;
            this.listenTo(window.appHelper.event_bus, "historyupdated", this.reload);

            this.collection.fetch(
            {
                success: function() {
                    self.render(); 
                }
            }); 

        },
        events: {
            "click .connector-event-wrap": "onEventClick"
        },
        reload: function() {
            var self = this;
            this.collection = new historyCollection({ id: this.supplierId });
            this.collection.fetch(
            {
                success: function() {
                    self.render(); 
                }
            }); 
        },
        onEventClick: function (e) {
            e.stopPropagation();
            //window.location.href = "/app/enrich/index#entity/" + this.eventModel.id;
        },

        render: function ( ) {
            var self = this; 

            this.$el.html(_.template(historyTemplate, {}));
            
            _.each(this.collection.models, function(model) {
                self.$el.find(".box-content ul").append(new historyView({model: model, supplierId: self.supplierId}).$el);

            }); 
            
            return this; // enable chained calls
        }
    });

    return historyListView;
});
