﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'models/pendingupload/pendigUploadEntityModel',
  'views/imports/pendingimports/importsPendingImportsListView',
  'views/imports/pendingimports/importsPendingImportsDetailView',
  'text!templates/imports/suppliercards/supplierCardTemplate.html',
], function ($, _, Backbone, modalPopup, pendigUploadEntityModel, importsPendingImportsListView, importsPendingImportsDetailView, supplierCardTemplate) {

    var supplierCardView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.supplier = options.supplier;
            this.pendingUploadModel = new pendigUploadEntityModel();

            this.latestSupplierActivity = new pendigUploadEntityModel();

            this.pendingUploadModel.url = "/api/xconnect/supplier/" + this.supplier.id + "/pending/latest";
            this.pendingUploadModel.fetch({
                success: function () {

                    self.render();
                }
            });
        },
        events: {
            "click #edit-supplier-button": "onEditSupplier",
            "click .latest-pending-upload-supplier-button": "clickLatestUpload"
        },
        clickLatestUpload: function (e) {
            var self = this;
            e.stopPropagation();

            if (this.pendingUploadModel.id) {

                var ParentModel = Backbone.Model.extend(
                    { idAttribute: 'supplierId' },
                    { supplierId: this.supplier.id }
                );

                window.appHelper.event_bus.off('closepopup');
                this.listenTo(window.appHelper.event_bus, 'closepopup', self.closeModalPopup);

                var pop = new modalPopup();
                pop.popOut(new importsPendingImportsDetailView({
                    model: this.pendingUploadModel, parent: ParentModel
                }), { size: "medium", header: "Pending Import", usesavebutton: false });

            }
        },
        closeModalPopup: function (e) {
            var self = this;

            $("#supplier-card-" + this.supplier.id + "").html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            $(".start-section-box #imports-pending-section").html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");

            var supplierCard = new supplierCardView({ supplier: self.supplier });
            $("#supplier-card-" + self.supplier.id).append(supplierCard.el);

            var pending = new importsPendingImportsListView({ suppliers: window.supplierOnboardingGlobals.selectedSuppliers });
            $(".start-section-box #imports-pending-section").append(pending.$el);
        },
        onEditSupplier: function (e) {
            var t = "";
            this.goTo("supplier/" + this.supplier.id);
        },
        render: function () {
            var self = this;

            $("#supplier-card-" + this.supplier.id + " .loadingspinner").remove();

            var sectionEl = $(_.template(supplierCardTemplate, { supplier: this.supplier.toJSON(), pendingUploadModelId: this.pendingUploadModel.id }));
            this.$el.append(sectionEl);

            if (this.supplier.get("logoUrl") != null) {
                this.$el.find(".supplier-logo-image").attr("src", "/api/xconnect" + this.supplier.get("logoUrl") + "?maxwidth=80&maxheight=80");
            } else {
                this.$el.find(".supplier-logo-image").attr("src", "/Images/nopicture50x50.png");
            }

            if (this.pendingUploadModel != null && this.pendingUploadModel.id) {
                this.$el.find(".card-text2 .card-text2-supplier-name").append(this.pendingUploadModel.get("name"));
            }
            else {
                this.$el.find(".card-text2 .card-text2-supplier-name").append("No pending imports.");
            }

            return this;
        }
    });

    return supplierCardView;
});