// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'misc/supplierOnboardingGlobals',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/topheader/topHeaderView',
  'views/appheader/appHeaderView',
  'views/imports/importsView',
  'views/suppliers/suppliersView',

], function ($, _, backbone, jqueryui, supplierOnboardingGlobals, inRiverUtil, topHeaderView, appHeaderView, importsView, suppliersView) {

    var AppRouter = backbone.Router.extend({
        routes: {
            // Define some URL routes
            "imports": "imports",
            "suppliers": "suppliers",
            "supplier/:id(/:tab)": "supplier"
        },
        imports: function (id) {
            this.switchView(new importsView());
        },
        suppliers: function (id) {
            if (id) {
                this.switchView(new suppliersView({ id: id }));
            }
            else {
                Backbone.Model.definitions = null;

                this.switchView(new suppliersView());
            }
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(newView.el);
        },
        closeView: function (specView) {
            if (specView) {
                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function (queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                        var aux = el.split('='), o = {};
                        if (aux.length >= 1) {
                            var val = undefined;
                            if (aux.length == 2)
                                val = aux[1];
                            o[aux[0]] = val;
                        }
                        return o;
                    }),
                    function (o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function () {
        console.log("init router");

        appHelper.event_bus = _({}).extend(backbone.Events);

        var appRouter = new AppRouter;

        var headerView = new topHeaderView({ app: "Supplier Onboarding" });
        var appheaderView = new appHeaderView();


        supplierOnboardingGlobals.init(); // set up all data

        appRouter.on('route:imports', function (id, tab) {
            $("#supplier-onboarding-navigation-links-wrap a").removeClass("current");
            $("#supplier-onboarding-navigation-links-wrap a[href='#imports']").addClass("current");
        });

        appRouter.on('route:suppliers', function (id, tab) {
            $("#supplier-onboarding-navigation-links-wrap a").removeClass("current");
            $("#supplier-onboarding-navigation-links-wrap a[href='#suppliers']").addClass("current");
        });

        appRouter.on('route:supplier', function (id, tab) {
            $("#supplier-onboarding-navigation-links-wrap a").removeClass("current");
            $("#supplier-onboarding-navigation-links-wrap a[href='#suppliers']").addClass("current");

            if (tab == "add") {
                this.switchView(new suppliersView({ id: id }));
            }
            else {

                Backbone.Model.definitions = null;
                this.switchView(new suppliersView({ id: id }));
            }
        });
       
        // Extend the View class to include a navigation method goTo
        Backbone.View.prototype.goTo = function (loc, changeurl) {
            if (changeurl == undefined) {
                changeurl = { trigger: true };
            }

            // Check for unsaved changes in current view before navigating.
            // When trigger is false it means that the view is actually switched elsewhere
            // and therefore the check for unsaved changes must be handled there instead (only the
            // browser url will be affected when trigger is set to false).
            if (changeurl.trigger) {
                inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(appRouter._currentView, function () {
                    appRouter.navigate(loc, changeurl);
                });
            } else {
                appRouter.navigate(loc, changeurl);
            }
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        backbone.View.prototype.close = function () {

            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        if (!Backbone.history.start()) {
            appRouter.navigate("imports", { trigger: true, replace: true });
        }
    };

    return {
        initialize: initialize
    };

});
