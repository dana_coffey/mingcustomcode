﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'views/imports/suppliercards/supplierCardContainerView',
], function ($, _, Backbone, inRiverUtil, supplierCardContainerView) {

    var supplierCardsContainerView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.suppliers = options.suppliers;

            this.render();
        },
        events: {

        },
        render: function () {
            var self = this;

            $("#imports-suppliers-section .loadingspinner").remove();

            if (this.suppliers.length == 0) {
                $("#imports-suppliers-section").html("<div class='no-items-text'>No suppliers selected</div>");
            }
            else {

                var count = this.suppliers.length;
                for (var i = 0; i < count; i++) {

                    var supplier = self.suppliers.models[i];
                    var supplierCard = new supplierCardContainerView({
                        supplier: supplier
                    });

                    $("#imports-suppliers-section").append(supplierCard.render().el);
                }
            }
        }
    });

    return supplierCardsContainerView;
});