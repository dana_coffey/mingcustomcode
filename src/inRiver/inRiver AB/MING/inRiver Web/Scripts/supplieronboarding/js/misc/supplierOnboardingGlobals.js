﻿define([
  'jquery',
  'underscore',
  'backbone'

], function ($, _, Backbone) {

    window.supplierOnboardingGlobals = {
        init: function () {
            window.supplierOnboardingGlobals.selectedSuppliers = new Backbone.Collection();
            window.supplierOnboardingGlobals.selectedSuppliers.comparator = "name";
        }
    }

    return supplierOnboardingGlobals;
});