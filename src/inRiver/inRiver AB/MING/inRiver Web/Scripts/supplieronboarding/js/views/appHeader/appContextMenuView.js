﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/permissionUtil',
  'views/suppliers/suppliersView',
  'text!templates/appHeader/appContextMenuTemplate.html'
], function ($, _, backbone, modalPopup, permissionUtil,suppliersView, appContextMenuTemplate) {

    var appContextMenuView = backbone.View.extend({
        initialize: function () {
            this.render();

            this.listenTo(appHelper.event_bus, 'openSupplierOnboardingToggleTools', this.onToggleTools);
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #close-tools-container": "deselect",
            "click #cancelSupplier": "deselect",
            "click #saveSupplier": "onSaveSupplier",
        },
        onSaveSupplier: function (e) {

            var self = this;

            var supplierName = this.$el.find("#supplier-name-input").val();

            Backbone.Model.definitions.supplierCollection.create({
                name: supplierName,
                backgroundColor: "#444",
                textColor: "#ffffff",
            }, {
                wait: true,
                success: function (model) {

                    Backbone.Model.definitions.supplierCollection.fetch({ reset: true });

                    Backbone.Model.definitions.currentSupplierId = model.id;
                    var $box = self.$el.find("#app-tools-container");
                    self.$el.find("#supplier-name-input")[0].value = "";
                    $box.hide();

                    self.goTo("supplier/" + model.id + "/add");
                }
            });

        },
        deselect: function (e) {
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
        },        
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle();

            $box.position({
                my: "right top",
                at: "right top+25",
                of: $("#tools-show")
            });

            $(document).mouseup(function (e) {
                if (!$box.is(e.target) // if the target of the click isn't the container...
                    && $box.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    $box.hide();
                    $box.unbind('click');
                }
            });

        },        
        render: function () {
            this.$el.html(appContextMenuTemplate);
            
            return this;
        }
    });

    return appContextMenuView;

});
