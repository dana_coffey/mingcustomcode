define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'collections/history/historyEntityCollection',
  'views/historycontrol/historyEntityDetailView',
  'text!templates/historycontrol/historyDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, historyEntityCollection, historyEntityDetailView, historyDetailTemplate) {

        var historyDetailView = backbone.View.extend({
            initialize: function (options) {
                var self = this; 
                this.model = options.model;
                this.parent = options.parent;
                this.supplierId = this.parent.supplierId; 

                this.batchId = this.model.attributes.batchId;
                this.entityCollection = new historyEntityCollection({ supplierId: this.supplierId, historyId: this.model.id });

                this.entityCollection.fetch({
                    success: function() {
                        self.render();
                    }
                }); 
        },
        events: {
            "click #import-all": "doImportAll",
            "click #delete": "doDelete",
        },
        doDelete: function () {
            var self = this;

            
            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");

            self.model.destroy({
                url: "/api/xconnect/supplier/" + self.model.attributes.supplierId + "/history/" + self.model.attributes.id,
                success: function () {
                    inRiverUtil.Notify("Upload history deleted successfully");
                    self.parent.unbind();
                    self.parent.remove();

                    window.appHelper.event_bus.trigger('closepopup');
                }

            });
        },
       render: function ( ) {
           var self = this;
           var batchId = self.model.attributes.batchId;
           this.$el.html(_.template(historyDetailTemplate, { model: this.model.toJSON() }));

           _.each(this.entityCollection.models, function (entity) {

               self.$el.find("#history-cardarea").append(new historyEntityDetailView({ model: entity }).$el);
               

           });

           this.$el.find("#loading").hide(); 
           return this; // enable chained calls
        }
    });

    return historyDetailView;
});
