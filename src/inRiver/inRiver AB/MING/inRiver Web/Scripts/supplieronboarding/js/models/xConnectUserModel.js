define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var xConnectUserModel = Backbone.Model.extend({
        urlRoot: "/api/xconnect/user",
        initialize: function () {
        },
    });

    return xConnectUserModel;

});
