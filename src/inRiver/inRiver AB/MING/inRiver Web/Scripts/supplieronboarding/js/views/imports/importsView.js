﻿define([
  'jquery',
  'underscore',
  'backbone',
  'multiple-select',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/usersetting/userSettingModel',
  'views/imports/pendingimports/importsPendingImportsListView',
  'views/imports/historyimports/importsHistoryImportsListView',
  'views/imports/suppliercards/supplierCardsContainerView',
  'text!templates/imports/importsContainersTemplate.html',
], function ($, _, Backbone, multipleSelect, inRiverUtil, userSettingModel, importsPendingImportsListView, importsHistoryImportsListView, supplierCardsContainerView, importsContainersTemplate) {

    var importsView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.supplierCollection = new Backbone.Collection(null, { url: "/api/xconnect/supplier" });
            this.userSettingSupplierFilterExists = false;


            this.supplierCollection.fetch({
                success: function (model, response) {

                    var key = "SupplierOnboardingSupplierFilter";
                    var settingModel = new userSettingModel(key);
                    settingModel.url = "/api/usersetting/" + key;
                    settingModel.fetch({
                        success: function (model, response) {
                            if (response != null && response !== "") {

                                if (window.supplierOnboardingGlobals.selectedSuppliers.length == 0) {
                                    
                                    window.supplierOnboardingGlobals.selectedSuppliers.reset();

                                    var supplierFilter = response.split(",");

                                    _.each(supplierFilter, function (filter) {
                                        var supplier = self.supplierCollection.get(filter);

                                        if (supplier != null) {
                                            window.supplierOnboardingGlobals.selectedSuppliers.push(supplier);

                                            if (!self.userSettingSupplierFilterExists) {
                                                self.userSettingSupplierFilterExists = true;    
                                            }
                                        }
                                    });
                                    window.supplierOnboardingGlobals.selectedSuppliers.sort();
                                }
                            }

                            self.preRender();
                            self.render();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.NotifyError("Supplier onBoarding failed to connect with xConnect", "Supplier onBoarding failed to connect with xConnect. Start xConnect and try again.");
                }
            });
        },
        events: {
            "click #save-supplier-filter": "saveSupplierFilter"
        },
        saveSupplierFilter: function (e) {
            var selectedSuppliers = window.supplierOnboardingGlobals.selectedSuppliers;

            var suppliersIdArray = [];

            _.each(selectedSuppliers.models, function (supplier) {
                if (!_.contains(suppliersIdArray, supplier.id)) {
                    suppliersIdArray.push(supplier.id);
                }
            });

            var settingModel = new userSettingModel();
            settingModel.save({
                SupplierOnboardingSupplierFilter: suppliersIdArray.toString()
            });

            inRiverUtil.Notify("Selection is saved.");
        },
        preRender: function () {
            var self = this;

            this.$el.find(".start-section-box #imports-pending-section").html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            this.$el.find(".start-section-box #imports-history-section").html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
            this.$el.find(".start-section-box #imports-suppliers-section").html("<div class=\"loadingspinner\"><img src=\"/Images/nine-squares-32x32.gif\" /></div>");
        },
        render: function () {
            var self = this;

            this.$el.html(importsContainersTemplate);
            this.$el.find("#imports-content-area").show();

            var dropdownEl = this.$el.find("#imports-supplier-dropdown");

            var suppliers = [];

            _.each(this.supplierCollection.models, function (supplier) {

                if (!self.userSettingSupplierFilterExists && window.supplierOnboardingGlobals.selectedSuppliers.length == 0) {

                    if (supplier.get("responsible")) {
                        var responsibles = supplier.get("responsible").split(",");

                        if (_.contains(responsibles, window.appHelper.userName)) {

                            optionEl = $("<option></option>")
                                        .attr("value", supplier.attributes.id)
                                        .attr("selected", "selected")
                                        .text(supplier.attributes.name);
                            dropdownEl.append(optionEl);

                            suppliers.push(supplier);
                        }
                        else {
                            optionEl = $("<option></option>")
                                        .attr("value", supplier.attributes.id)
                                        .text(supplier.attributes.name);
                            dropdownEl.append(optionEl);
                        }
                    }
                    else {
                        optionEl = $("<option></option>")
                                    .attr("value", supplier.attributes.id)
                                    .text(supplier.attributes.name);
                        dropdownEl.append(optionEl);
                    }                    
                }
                else {

                    if (window.supplierOnboardingGlobals.selectedSuppliers.length != 0) {

                        if (window.supplierOnboardingGlobals.selectedSuppliers.get(supplier.attributes.id) != null) {

                            optionEl = $("<option></option>")
                                .attr("value", supplier.attributes.id)
                                .attr("selected", "selected")
                                .text(supplier.attributes.name);
                            dropdownEl.append(optionEl);
                        }
                        else {
                            optionEl = $("<option></option>")
                                    .attr("value", supplier.attributes.id)
                                    .text(supplier.attributes.name);
                            dropdownEl.append(optionEl);
                        }
                    }
                    else {

                    }
                }
            });

            if (suppliers.length != 0) {
                _.each(suppliers, function (supplier) {
                    window.supplierOnboardingGlobals.selectedSuppliers.push(supplier);
                });
            }

            this.$el.find("#imports-supplier-dropdown").multipleSelect({
                width: 180,
                multipleWidth: 165,
                multiple: true,
                filter: true,
                onClick: function (e) {
                    var selectedSuppliersDropDown = self.$el.find("#imports-supplier-dropdown").val();

                    if (e.checked) {
                        //Add
                        var selectedSupplier = self.supplierCollection.get(e.value);

                        if (window.supplierOnboardingGlobals.selectedSuppliers.get(e.value) == null) {
                            window.supplierOnboardingGlobals.selectedSuppliers.push(selectedSupplier);
                        }
                    }
                    else {
                        //Remove
                        if (window.supplierOnboardingGlobals.selectedSuppliers.get(e.value)) {
                            window.supplierOnboardingGlobals.selectedSuppliers.remove(e.value);
                        }
                    }

                    window.supplierOnboardingGlobals.selectedSuppliers.sort();
                    self.preRender();
                    self.postRender();
                },
                onCheckAll: function (e) {
                    var selectedSuppliersValues = self.$el.find("#imports-supplier-dropdown").val()

                    if (selectedSuppliersValues && selectedSuppliersValues.length !== window.supplierOnboardingGlobals.selectedSuppliers.length) {
                        //Gör något
                        _.each(selectedSuppliersValues, function (supplierId) {
                            if (window.supplierOnboardingGlobals.selectedSuppliers.get(supplierId) == null) {
                                var selectedSupplier = self.supplierCollection.get(supplierId);

                                window.supplierOnboardingGlobals.selectedSuppliers.push(selectedSupplier);
                            }
                        });

                        self.preRender();
                        self.postRender();
                    }
                },
                onUncheckAll: function (e) {
                    window.supplierOnboardingGlobals.selectedSuppliers.reset();
                    self.preRender();
                    self.postRender();
                }
            });

            this.postRender();
        },

        postRender: function () {

            //PENDING
            var pending = new importsPendingImportsListView({ suppliers: window.supplierOnboardingGlobals.selectedSuppliers });
            this.$el.find(".start-section-box #imports-pending-section").append(pending.$el);

            //HISTORY
            var history = new importsHistoryImportsListView({ suppliers: window.supplierOnboardingGlobals.selectedSuppliers });
            this.$el.find(".start-section-box #imports-history-section").append(history.$el);

            //SUPPLIER CARDS

            var supplierCards = new supplierCardsContainerView({ suppliers: window.supplierOnboardingGlobals.selectedSuppliers });
            this.$el.find(".start-section-box #imports-suppliers-section").append(supplierCards.$el);

            return this;
        }
    });

    return importsView;

});