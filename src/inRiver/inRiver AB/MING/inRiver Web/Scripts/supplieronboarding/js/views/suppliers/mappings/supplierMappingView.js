define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'views/suppliers/mappings/supplierMappingDetailView',
  'text!templates/suppliers/mappings/supplierMappingTemplate.html'
], function ($, _, backbone, alertify, modalPopup, inRiverUtil, supplierMappingDetailView, mappingTemplate) {

    var mappingView = backbone.View.extend({
        tagName: "li",
        initialize: function (options) {
            this.model = options.model;
            this.supplierId = options.supplierId;
            this.model.supplierId = this.supplierId;
            this.render(); 
        },
        events: {
            "click #list-item-remove": "onDelete",
            "click": "view"
        },
        onDelete: function (e) {
            e.stopPropagation();

            inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete this mapping?", this.doDelete, this);
        },
        doDelete: function (x) {
            
            x.model.destroy({
                    success: function() {
                        inRiverUtil.Notify(x.model.attributes.name + " successfully deleted");
                        x.remove();
                        x.unbind();
                    }
                }
            );
        },
        view: function () {
            var pop = new modalPopup();
            pop.popOut(new supplierMappingDetailView({
                model: this.model, parent: this
            }), { size: "medium", header: "Mapping Overview", usesavebutton: true });
            $("#save-form-button").removeAttr("disabled");
        },
       
       render: function ( ) {
           this.$el.html(_.template(mappingTemplate, { model: this.model.toJSON() }));

           return this; // enable chained calls
        }
    });

    return mappingView;
});
