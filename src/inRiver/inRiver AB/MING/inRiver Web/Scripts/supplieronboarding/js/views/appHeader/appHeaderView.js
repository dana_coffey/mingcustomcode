﻿define([
  'jquery',
  'underscore',
  'backbone',
  'views/appheader/appContextMenuView',
  'views/appheader/appNavigationMenuView',
], function ($, _, Backbone, appContextMenuView, appNavigationMenuView) {

    var appHeaderView = Backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        render: function () {
            $("#context-menu-container").html(new appContextMenuView().el);
            $("#context-menu-container-left").html(new appNavigationMenuView().el);
            $("#context-menu-container-left").attr("title", "");

            return this;
        }
    });

    return appHeaderView;

});
