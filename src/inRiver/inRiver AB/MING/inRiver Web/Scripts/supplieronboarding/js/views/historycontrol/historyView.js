define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
   'modalPopup',
    'collections/history/historyEntityCollection',

  'views/historycontrol/historyDetailView',
'text!templates/historycontrol/historyTemplate.html'
], function ($, _, backbone, alertify, modalPopup,historyEntityCollection,  historyDetailView, historyTemplate) {

    var historyView = backbone.View.extend({
        tagName: "li",
        initialize: function (options) {
            this.model = options.model;
            this.supplierId = options.supplierId; 
            this.render(); 
        },
        events: {
            "click #open-item-in-workarea": "openWorkarea",
            "click": "view"
        },
        openWorkarea: function(e) {
            e.stopPropagation();
            this.entityCollection = new historyEntityCollection({ supplierId: this.supplierId, historyId: this.model.id });
            var self = this;

            this.entityCollection.fetch({
                success: function() {
                    //Get all entityIds.
                    
                    var entities = _(self.entityCollection.models).chain().pluck("attributes").pluck("entityId").value().join(',');
                    $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                        //self.onCancel();
                        window.location.href = "/app/enrich/#workarea?title=" + self.model.attributes.name + "&compressedEntities=" + compressed;
                    });

                }
            });


        },
        view: function () {
            var pop = new modalPopup();
            pop.popOut(new historyDetailView({
                model: this.model, parent: this
            }), { size: "medium", header: "Import History", usesavebutton: false });

        },
        
       render: function ( ) {
           this.$el.html(_.template(historyTemplate, { model: this.model.toJSON() }));

           return this; // enable chained calls
        }
    });

    return historyView;
});
