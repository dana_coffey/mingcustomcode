define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'collections/mappings/mappingCollection',
  'views/suppliers/mappings/supplierMappingView',
  'views/suppliers/mappings/supplierAddMappingView',
  'text!templates/suppliers/mappings/supplierMappingListTemplate.html'
], function ($, _, backbone, alertify, modalPopup, mappingCollection, supplierMappingView, supplierAddMappingView, mappingListTemplate) {

    var mappingListView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.supplierId = options.id;
            this.collection = new mappingCollection({ id: this.supplierId });

            this.listenTo(window.appHelper.event_bus, 'mappingadded'+this.supplierId, this.reload);

            var self = this; 

            this.collection.fetch(
            {
                success: function() {
                    self.render(); 
                }
            }); 

        },
        events: {
            "click #list-of-mapping-add": "onAddMapping"
        },
        onAddMapping: function () {
            var pop = new modalPopup();
            pop.popOut(new supplierAddMappingView({
                supplierId: this.supplierId
            }), { size: "medium", header: "Create Mapping", usesavebutton: false });
          
        },
        reload: function() {
            var self = this;

            this.collection.fetch(
            {
                success: function () {
                    self.render();
                }
            });
        },
        render: function ( ) {
            var self = this; 
           
            $("#supplier-mappings-section .loadingspinner").remove();
            this.$el.empty();

            var sectionEl = $(_.template(mappingListTemplate, { }));
            this.$el.append(sectionEl);

            //this.$el.html(_.template(mappingListTemplate, {}));
            
            _.each(this.collection.models, function(model) {
                self.$el.find(".box-content ul").append(new supplierMappingView({ model: model, supplierId: self.supplierId }).$el);

            }); 
            
            return this; // enable chained calls
        }
    });

    return mappingListView;
});
