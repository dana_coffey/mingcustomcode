﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fileImportModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/fileimportconfiguration/',
    });

    return fileImportModel;

});
