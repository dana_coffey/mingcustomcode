define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
    'sharedjs/collections/entities/entityCollection',
    'sharedjs/collections/fieldTypes/FieldTypesCollection',
  'text!templates/suppliers/mappings/supplierMappingDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, entityCollection, fieldTypesCollection, mappingDetailTemplate) {

    var pendingBatchView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.model = options.model;
            this.parent = options.parent;
            this.supplierId = this.parent.supplierId;

            this.FieldTypesCollection = new fieldTypesCollection();
            this.FieldTypesCollection.url = this.FieldTypesCollection.url + "/" + this.model.get("entityType");

            this.listenTo(this.FieldTypesCollection, 'reset', this.onInitFieldTypesCollection);
            this.FieldTypesCollection.fetch({ reset: true, cache: false });
            
            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);
        },
        events: {
            "click #import": "doImport",
            "click #dispose": "doDispose",
        },
        onInitFieldTypesCollection: function(collection) {
            collection.isLoaded = true;

            var self = this;

            if (this.FieldTypesCollection.isLoaded) {

                _.each(this.model.get("columns"),
                    function(column) {

                        var fieldTypeModel = _.find(self.FieldTypesCollection.models,
                            function(fieldTypeModel) {

                                if (column.fieldTypeId == null) {
                                    return null;
                                }
                                    
                                if (fieldTypeModel.get("id") == column.fieldTypeId) {
                                    return fieldTypeModel;
                                }
                            });

                        if (column.columnName != "sys_id") {
                            if (fieldTypeModel) {
                                column['displayName'] = fieldTypeModel.get("displayname");
                            } else {
                                column['displayName'] = "[" + column.fieldTypeId + "]";
                            }
                        } else {
                            column['displayName'] = "sys_id";
                        }
                    });

                this.render();  
            }
        },
        onSave: function () {
            var self = this;

            self.$el.find("#import").addClass("disabled");
            self.$el.find("#dispose").addClass("disabled");


            this.model.attributes.name = this.$el.find("#name").val();

            this.model.save([], {

                success: function () {
                    self.done();
                    //self.parent.model = self.model;
                    self.parent.render();

                    inRiverUtil.Notify("Mapping updated");
                }
            });

        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            window.appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },       
        render: function () {
            var self = this;

            if (this.model.get("specificationTemplateId")) {
                var collection = new entityCollection();
                collection.url = '/api/specificationtemplate/' + this.model.get("specificationTemplateId") + '/field';

                collection.fetch({
                    success: function (model, response) {
                        var modelWithSpecificationNames = new backbone.Model(self.model.attributes);

                        _.each(modelWithSpecificationNames.get("columns"), function (column) {
                            if (column.specificationFieldTypeId) {
                                _.each(model.models, function (value) {
                                    if (value.id == column.specificationFieldTypeId) {
                                        column.specificationFieldTypeId = value.get("DisplayName");
                                    }
                                });
                            }
                        });

                        self.$el.html(_.template(mappingDetailTemplate, { model: modelWithSpecificationNames.toJSON() }));
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            else {
                this.$el.html(_.template(mappingDetailTemplate, { model: this.model.toJSON() }));
            }

            return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
