define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'simpleAjaxUploader',
  'multipleSelect',
  'colpick',
  'modalPopup',
  'jquery-ui',
   'sharedjs/misc/inRiverUtil',
  'text!templates/start/startPageTemplate.html',
  'text!templates/user/xConnectUserListTemplate.html',
  'text!templates/user/xConnectUserListItemTemplate.html',
  'text!templates/user/xConnectUserEditTemplate.html',
   'models/xConnectUserModel',
  'collections/xConnectUserCollection',
  'views/uploadcontrol/pendingUploadListView',
  'views/historycontrol/historyListView',
  'views/mapping/mappingListView',
  'sharedjs/collections/users/usersCollection',
  'text!templates/suppliersettings/xConnectSupplierOnboardingEditTemplate.html'
], function ($, _, backbone, backboneforms, simpleAjaxUploader, multipleSelect, colpick, modalPopup, jqueryui, inRiverUtil, startPageTemplate, xConnectUserListTemplate, xConnectUserListItemTemplate, xConnectUserEditTemplate, xConnectUserModel, xConnectUserCollection, pendingUploadListView, historyListView, mappingListView, usersCollection, xConnectSupplierOnboardingEditTemplate) {

    var currentSupplierModel = null;

    var userCollection = new xConnectUserCollection();
    var supplierCollection = new backbone.Collection(null, { url: "/api/xconnect/supplier" });

    var startSupplyPage = backbone.View.extend({
        initialize: function () {

            var self = this;

            this.listenTo(supplierCollection, "reset", this.onIniting);
            supplierCollection.fetch({ reset: true });


        },
        events: {
            "change #suppliers-dropdown": "onChangeSupplier",
            "click #list-of-suppliers-add": "onSupplier"
        },
        onChangeSupplier: function (e) {
            currentSupplierModel = supplierCollection.get(e.currentTarget.value);
            this.render();
        },
        onIniting: function (collection) {
            collection.isLoaded = true;

            // Perform final init when all necessary collections are loaded
            if (supplierCollection.isLoaded && currentSupplierModel == null) {
                if (supplierCollection.length > 0) {
                    currentSupplierModel = supplierCollection.first();
                    this.listenTo(supplierCollection, "onChangeNameFinished", this.renderStaticContent);
                }
                // this.renderStaticContent();
                this.render();
            }
        },
        onSupplier: function () {
            var that = this;

            inRiverUtil.Prompt("Enter a name for your the Supplier", function (e, supplierName) {
                if (e) {
                    currentSupplierModel = supplierCollection.create({
                        name: supplierName,
                        backgroundColor: "#444",
                        textColor: "#ffffff",
                    }, {
                        wait: true,
                        success: function(model) {
                            supplierCollection.fetch({ reset: true });
                            currentSupplierModel = model;
                            that.render();
                        }

                    });
                }
            });
        },
        
        renderStaticContent: function () {
            var selectEl = this.$el.find("#suppliers-dropdown");
            selectEl.html("");
            supplierCollection.each(function (model) {
                selectEl.append($('<option>', { value: model.get("id") }).text(model.get("name")));
            });
            selectEl.val(currentSupplierModel.get("id"));
        },
        render: function () {
            var startPage = this.$el.html(startPageTemplate);

            var selectEl = this.$el.find("#suppliers-dropdown");
            selectEl.html("");
            supplierCollection.each(function (model) {
                selectEl.append($('<option>', { value: model.get("id") }).text(model.get("name")));
            });

            if (currentSupplierModel == null) {
                this.$el.find("#suppliers-content-area").hide();
                return this;
            }

            this.$el.find("#suppliers-content-area").show();

            selectEl.val(currentSupplierModel.get("id"));

            this.$el.find("#suppliers-users-section").html(new usersSubView().el);
            this.$el.find("#suppliers-settings-section").html(new settingsSubView().el);
            this.$el.find("#supplier-pending-batches").html(new pendingUploadListView({ id: currentSupplierModel.get("id") }).el);
            this.$el.find("#supplier-history").html(new historyListView({ id: currentSupplierModel.get("id") }).el);

            this.$el.find("#supplier-mappings").html(new mappingListView({ id: currentSupplierModel.get("id") }).el);
        }
    });

    var usersSubView = backbone.View.extend({
        initialize: function () {
            this.listenTo(userCollection, 'reset', this.render);
            this.listenTo(userCollection, 'add', this.render);
            this.listenTo(userCollection, 'remove', this.render);

            userCollection.fetch({ reset: true });

            this.render();
        },
        events: {
            "click #list-of-users-add": "onAddUser"
        },
        onAddUser: function () {
            var model = new xConnectUserModel();
            openAddUserPopup(model);
        },
        render: function () {
            this.$el.html(xConnectUserListTemplate);

            var ulEl = this.$el.find("#xconnect-user-list-wrap > ul");

            userCollection.each(function (model) {
                var userItemView = new xConnectUserListItemView({ model: model });
                ulEl.append(userItemView.el);
            });
        }
    });

    function openAddUserPopup(model) {
        var pop = new modalPopup();
        pop.popOut(new xConnectUserEditView({
            model: model
        }), { size: "small", header: "Add User", usesavebutton: true });
        $("#save-form-button").removeAttr("disabled");
    }

    function openEditUserPopup(model) {
        var pop = new modalPopup();
        pop.popOut(new xConnectUserEditView({
            model: model
        }), { size: "small", header: "Edit User", usesavebutton: true });
        $("#save-form-button").removeAttr("disabled");
    }

    var xConnectUserListItemView = backbone.View.extend({
        tagName: "li",
        //template: _.template(xConnectUserListItemTemplate).html(),
        initialize: function (options) {
            this.template = _.template(xConnectUserListItemTemplate),
                this.model = options.model;
            this.model.on('change', this.render, this);
            this.render();
        },
        events: {
            "change #is-assigned": "onChangeIsAssigned",
            "click #list-item-edit": "onEditUser",
            // "click #list-item-remove": "onRemoveUser",
        },
        onChangeIsAssigned: function (e) {
            var isAssigned = this.$el.find("#is-assigned").prop('checked');
            if (!isAssigned) {
                this.model.set("supplierSitesId", _.reject(this.model.get("supplierSitesId"), function (item) {
                    return item.supplierId === currentSupplierModel.get("id");
                }));
            } else {

                if (this.model.get("supplierSitesId") == null) {
                    this.model.set("supplierSitesId", []);
                }

                this.model.get("supplierSitesId").push({ supplierId: currentSupplierModel.get("id") });
            }
            this.model.save();
        },
        onEditUser: function (e) {
            openEditUserPopup(this.model);
        },
        render: function () {
            this.$el.html(this.template({ data: this.model.toJSON() }));

            var isUserAssignedToCurrentSupplier = _.contains(_.pluck(this.model.get("supplierSitesId"), "supplierId"), currentSupplierModel.get("id"));
            this.$el.find("#is-assigned").prop('checked', isUserAssignedToCurrentSupplier);
        }
    });

    var xConnectUserEditView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.model = options.model;

            this.template = _.template(xConnectUserEditTemplate);

            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            //this.languagesCollection = new languagesCollection();
            //this.listenTo(this.languagesCollection, 'reset', this.render);
            /*this.languagesCollection.fetch({
                //reset: true,
                success: function() {
                    that.render();
                }
            });*/

            this.render();
            if (this.model.isNew()) {
                this.doGenerateNewApiKey(this);
                this.$el.find("#reset-password").hide();
                this.$el.find("#delete-user").hide();
            }
        },
        events: {
            "click #generate-api-key": "onGenerateNewApiKey",
            "click #reset-password": "onResetPassword",
            "click #delete-user": "onDeleteUser"
        },
        onGenerateNewApiKey: function () {
            inRiverUtil.NotifyConfirm("Confirm revoke api key", "Are you sure you want to revoke this api key?<br><br>Revoking an api key will break all usages of the inriver Cross Connect API for this user. If an api key is revoked a new one will be generated.", this.doGenerateNewApiKey, this);
        },
        doGenerateNewApiKey: function (that) {
            var tmpModel = new backbone.Model();
            tmpModel.urlRoot = "/api/xconnect/newapikey";
            tmpModel.fetch({
                success: function () {
                    that.$el.find("#api-key").val(tmpModel.get("apiKey"));
                }
            });
        },
        onResetPassword: function () {
            var that = this;
            inRiverUtil.NotifyConfirm("Reset Password", "Are you sure you want to reset the password for this user?<br><br>The user will not be able to login without setting a new password.", function () {
                that.$el.find("#reset-password").text(" Please Wait");
                that.doPasswordReset(that.model, "Password was reset! Now ask the user to set a new password by using this url:", function() {
                    that.$el.find("#reset-password").text("Reset this user's password");
                });
            }, this);
        },
        onDeleteUser: function () {
            var that = this;
            inRiverUtil.NotifyConfirm("Delete User", "Are you sure you want to delete this user?<br><br>The user will not be able to use any inRiver xConnect features at all.", function () {
                that.model.destroy({
                    success: function () {
                        that.done();
                    }
                });
            }, this);
        },
        onSave: function () {
            console.log("Saving user");
            var that = this;
            var errors = this.form.commit();
            if (errors) {
                return; 
            }
            
            this.model.set({ apiKey: this.$el.find("#api-key").val() });

            var isNew = this.model.isNew();
            this.model.save(null, {
                success: function (model, response) {
                    if (isNew) {
                        userCollection.add(that.model);
                        that.doPasswordReset(that.model, "User created! Now ask the user to set a password by using this url:", function () {
                            that.done();
                        });
                    } else {
                        that.done();
                    }
                }
            });
        },
        doPasswordReset: function (model, successMsg, successFn) {
            var tmpModel = new backbone.Model({}, {
                urlRoot: model.urlRoot + "/" + model.get("id") + "/supplierpasswordreset"
            });
            tmpModel.fetch({
                method: "POST",
                success: function () {
                    var url = tmpModel.get("passwordResetUrl");
                    inRiverUtil.NotifyConfirm("User", successMsg + "<br><br><input type='text' class='supplier-user-password-reset' value='" + url + "'>", function (b) { successFn(); }, this);
                }
            });
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ data: this.model.toJSON() }));

            this.model.schema = {
                    email: { validators: ['required', 'email'] },
                    firstName: { title: 'First Name', validators: ['required'] },
                    lastName: { title: 'Last Name', validators: ['required'] },
                };
         
            // Render user info form
            this.form = new Backbone.Form({
                model: this.model
            }).render();
            this.$el.find("#form-container").html(this.form.el);
        }
    });


    var settingsSubView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.template = _.template(xConnectSupplierOnboardingEditTemplate);

            currentSupplierModel.on("change:textColor", this.updateTextColor, this);
            currentSupplierModel.on("change:backgroundColor", this.updateBackground, this);
            //  currentSupplierModel.on("change:logoUrl", this.updateLogo, this);

            this.render();

            this.createColorCtl(this.$el.find("#edit-supplier-onboarding-pick-background-color"), "backgroundColor");
            this.createColorCtl(this.$el.find("#edit-supplier-onboarding-pick-text-color"), "textColor");

            this.uploader = new ss.SimpleUpload({
                button: this.$el.find("#edit-supplier-onboarding-file-upload"),
                url: '/api/xconnect/supplier/' + currentSupplierModel.get("id") + '/logo',
                //name: 'uploadfile', // Parameter name of the uploaded file
                multipart: true,
                onSubmit: function (filename, extension) {
                    that.$el.find("#file-upload-spinner").show();
                    currentSupplierModel.set("logoName", filename);
                },
                onComplete: function (filename, response) {
                    that.$el.find("#file-upload-spinner").hide();
                    if (!response) {
                        alert('upload failed');
                    } else {
                        that.$el.find("#edit-supplier-onboarding-logo-preview").hide();
                        currentSupplierModel.set("logoUrl", JSON.parse(response).logoUrl);
                        that.updateLogo();
                    }
                }
            });

            this.usersCollection = new usersCollection();
            this.usersCollection.fetch(
            {
                success: function () {
                    that.updateUi();
                }
            });
        },
        events: {
            "change #edit-supplier-onboarding-name": "onChangeName",
            "keyup #edit-supplier-onboarding-name": "onChangeName",
            "blur #edit-supplier-onboarding-name": "onChangeNameFinished",
            "click #edit-supplier-onboarding-pick-background-color": "onPickBackgroundColor",
            "click #edit-supplier-onboarding-pick-text-color": "onPickTextColor",
            "change #edit-supplier-onboarding-responsible": "onChangeResponsible",
            "change #edit-supplier-onboarding-relay": "onChangeRelay",
            "keyup #edit-supplier-onboarding-relay": "onChangeRelay",
            "click #delete-supplier": "onDeleteSupplier"
        },
        onChangeName: function (e) {
            currentSupplierModel.set({ name: this.$el.find("#edit-supplier-onboarding-name").val() });
            currentSupplierModel.save();
        },
        onChangeNameFinished: function (e) {
            currentSupplierModel.trigger("onChangeNameFinished");
        },
        onPickBackgroundColor: function (e) {
            this.$el.find("#edit-supplier-onboarding-pick-background-color").colpickShow();
        },
        onPickTextColor: function (e) {
            this.$el.find("#edit-supplier-onboarding-pick-text-color").colpickShow();
        },
        onChangeRelay: function (e) {
            currentSupplierModel.attributes.relay = this.$el.find("#edit-supplier-onboarding-relay")[0].checked;
            currentSupplierModel.save();
        },
        onChangeResponsible: function (e) {
            var responsible = this.$el.find("#edit-supplier-onboarding-responsible").val();
            if (responsible != null) {
                currentSupplierModel.set({ responsible: responsible.toString() });
                currentSupplierModel.save();
            }
        },
        createColorCtl: function (el, attributeName) {
            el.colpick({
                color: currentSupplierModel.get(attributeName),
                layout: 'hex',
                submit: 0,
                onChange: function (hsb, hex, rgb, elem, bySetColor) {
                    var map = {};
                    map[attributeName] = "#" + hex;
                    currentSupplierModel.set(map);
                    currentSupplierModel.save();
                }
            }).keyup(function () {
                var v = $(this).find("input").val();
                if (/(^#[0-9A-F]{6}$)/i.test(v)) {
                    $(this).colpickSetColor(v);
                }
            });
        },
        onDeleteSupplier: function () {
            inRiverUtil.NotifyConfirm("Confirm Delete Supplier", "Are you sure you want to delete this Supplier?", function () {
                $.ajax({
                    type: "DELETE",
                    url: "/api/xconnect/supplier/" + currentSupplierModel.get("id"),
                    //dataType: "application/json"
                }).fail(function () {
                    alert("Failed to delete connector!");
                }).done(function () {
                        currentSupplierModel.destroy();
                    currentSupplierModel = null; 
                    supplierCollection.fetch({ reset: true });
                });
            }, this);
        },
        updateUi: function () {
            var self = this;
            this.$el.find("#edit-supplier-onboarding-name").val(currentSupplierModel.get("name"));

            this.$el.find("#edit-supplier-onboarding-responsible").html("");
            _.each(this.usersCollection.models, function (user) {
                var selected = "";
                if (currentSupplierModel.get("responsible") != null) {
                    if (_.find(currentSupplierModel.get("responsible").split(","), function (name) {
                        return name == user.attributes.Username;
                    })) {
                        selected = " selected=\"selected\"";
                    }
                }
                self.$el.find("#edit-supplier-onboarding-responsible").append("<option value=\"" + user.attributes.Username + "\"" + selected + ">" + user.attributes.FirstName + " " + user.attributes.LastName + "</option>");


            });

            this.$el.find("#edit-supplier-onboarding-responsible").multipleSelect({ width: 170 });
            this.$el.find("#edit-supplier-onboarding-responsible").val(currentSupplierModel.get("responsible"));
            this.$el.find("#edit-supplier-onboarding-relay").prop("checked", currentSupplierModel.get("relay") == 1);

            if (currentSupplierModel.get("backgroundColor") != null) {
                var tmpEl1 = this.$el.find("#edit-supplier-onboarding-pick-background-color");
                tmpEl1.find("input").css('border-color', currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").val(currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").text(currentSupplierModel.get("backgroundColor"));
            }

            if (currentSupplierModel.get("textColor") != null) {
                var tmpEl2 = this.$el.find("#edit-supplier-onboarding-pick-text-color");
                tmpEl2.find("input").css('border-color', currentSupplierModel.get("textColor"));
                tmpEl2.find("input").val(currentSupplierModel.get("textColor"));
            }

            if (currentSupplierModel.get("logoUrl") != null) {
                this.$el.find("#edit-supplier-onboarding-logo-preview").attr("src", "/api/xconnect" + currentSupplierModel.get("logoUrl") + "?maxwidth=100&maxheight=100");
                this.$el.find("#edit-supplier-onboarding-logo-preview").show();
            } else {
                this.$el.find("#edit-supplier-onboarding-logo-preview").hide();
            }
        },
        updateBackground: function () {
            if (currentSupplierModel.get("backgroundColor") != null) {
                var tmpEl1 = this.$el.find("#edit-supplier-onboarding-pick-background-color");
                tmpEl1.find("input").css('border-color', currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").val(currentSupplierModel.get("backgroundColor"));
                tmpEl1.find("input").text(currentSupplierModel.get("backgroundColor"));
            }
        },
        updateTextColor: function () {

            if (currentSupplierModel.get("textColor") != null) {
                var tmpEl2 = this.$el.find("#edit-supplier-onboarding-pick-text-color");
                tmpEl2.find("input").css('border-color', currentSupplierModel.get("textColor"));
                tmpEl2.find("input").val(currentSupplierModel.get("textColor"));
            }
        },
        updateLogo: function () {
            if (currentSupplierModel.get("logoUrl") != null) {
                this.$el.find("#edit-supplier-onboarding-logo-preview").attr("src", "/api/xconnect" + currentSupplierModel.get("logoUrl") + "?maxwidth=100&maxheight=100");
                this.$el.find("#edit-supplier-onboarding-logo-preview").show();
            } else {
                this.$el.find("#edit-supplier-onboarding-logo-preview").hide();
            }
        },

        render: function () {
            var that = this;
            this.$el.html(this.template({ data: currentSupplierModel.toJSON() }));
        }
    });

    return startSupplyPage;
});

