define([
  'jquery',
  'underscore',
  'backbone',
  'models/pendingupload/pendigUploadEntityModel'
], function ($, _, backbone, pendigUploadEntityModel) {
    var pendingUploadEntityCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.historyId = options.historyId;
            this.supplierId = options.supplierId; 
        },
        model: pendigUploadEntityModel,
        url: function() {
            return "/api/xconnect/supplier/" + this.supplierId + "/history/" + this.historyId + "/entity";
            
        }
    });
    return pendingUploadEntityCollection;
});
