define([
  'jquery',
  'underscore',
  'backbone',
  'models/mapping/mappingModel'
], function ($, _, backbone, mappingModel) {
    var mappingCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.supplierId = options.id;
        },
        model: mappingModel,
        url: function() {
            return "/api/xconnect/supplier/" + this.supplierId + "/mapping";
        }
    });
    return mappingCollection;
});
