define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
   'modalPopup',
  'views/uploadcontrol/pendingUploadDetailView',
'text!templates/uploadcontrol/pendingUploadTemplate.html'
], function ($, _, backbone, alertify, modalPopup, pendingUploadDetailView, pendingUploadTemplate) {

    var pendingUploadView = backbone.View.extend({
        tagName: "li",
        initialize: function (options) {
            this.model = options.model;
            this.supplierId = options.supplierId; 
            this.render(); 
        },
        events: {
            "click": "view"
        },
        view: function () {
            var pop = new modalPopup();
            pop.popOut(new pendingUploadDetailView({
                model: this.model, parent: this
            }), { size: "medium", header: "Pending Import", usesavebutton: false });

        },
        
       render: function ( ) {
           this.$el.html(_.template(pendingUploadTemplate, { model: this.model.toJSON() }));

           return this; // enable chained calls
        }
    });

    return pendingUploadView;
});
