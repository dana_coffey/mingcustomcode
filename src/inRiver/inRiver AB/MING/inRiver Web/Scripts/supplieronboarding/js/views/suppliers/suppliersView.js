﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'views/suppliers/users/supplierUsersListView',
  'views/suppliers/settings/supplierSettingsView',
  'views/suppliers/mappings/supplierMappingListView',
  'views/imports/pendingimports/importsPendingImportsListView',
  'views/imports/historyimports/importsHistoryImportsListView',
  'text!templates/suppliers/suppliersContainersTemplate.html',
], function ($, _, Backbone, inRiverUtil, supplierUsersListView, supplierSettingsView, supplierMappingListView, supplierPendingImportsUploadListView, suppliersHistoryImportsListView, suppliersContainersTemplate) {
    var suppliersView = Backbone.View.extend({
        initialize: function (options) {
            if (!Backbone.Model.definitions) {
                Backbone.Model.definitions = {}
            }

            if (!Backbone.Model.definitions.supplierCollection) {
                //Create SupplierOnboarding global collections of all suppliers.
                Backbone.Model.definitions.supplierCollection = new Backbone.Collection(null, { url: "/api/xconnect/supplier" });
            }

            if (!Backbone.Model.definitions.getCurrentSupplierModel) {
                Backbone.Model.definitions.getCurrentSupplierModel = function() {
                    return Backbone.Model.definitions.supplierCollection.get(Backbone.Model.definitions.currentSupplierId);
                }
            }

            if (options) {
                Backbone.Model.definitions.currentSupplierId = options.id;
            }

            this.listenTo(Backbone.Model.definitions.supplierCollection, "reset", this.onIniting);
            this.listenTo(Backbone.Model.definitions.supplierCollection, "onChangeNameFinished", this.renderStaticContent);

            if (Backbone.Model.definitions.supplierCollection.isLoaded) {
                this.onIniting(Backbone.Model.definitions.supplierCollection);
            } else {
                Backbone.Model.definitions.supplierCollection.fetch({
                    reset: true,
                    error: function () {
                        inRiverUtil.NotifyError("Supplier onBoarding failed to connect with xConnect", "Supplier onBoarding failed to connect with xConnect. Start xConnect and try again.");
                    }
                });
            }
        },
        events: {
            "change #suppliers-dropdown": "onChangeSupplier",
            "click #delete-supplier": "onDeleteSupplier",
            "click #create-supplier": "onCreateSupplier"
        },
        onCreateSupplier: function(){
            appHelper.event_bus.trigger('openSupplierOnboardingToggleTools');
        },
        onDeleteSupplier: function () {
            var currentSupplierModel = Backbone.Model.definitions.getCurrentSupplierModel();

            inRiverUtil.NotifyConfirm("Confirm Delete Supplier", "Are you sure you want to delete Supplier " + currentSupplierModel.get("name") + "?", function () {

                currentSupplierModel.destroy({
                    wait: true,
                    success: function () {
                        Backbone.Model.definitions.supplierCollection.fetch({ reset: true });
                    },
                    error: function () {
                        inRiverUtil.NotifyError("Failed to delete supplier","Remove all users before deleting the supplier.");
                    }
                });
            }, this);
        },
        onChangeSupplier: function (e) {
            Backbone.Model.definitions.currentSupplierId = e.currentTarget.value;
            this.render();
        },
        onIniting: function (collection) {
            collection.isLoaded = true;

            if (collection.length > 0 && !Backbone.Model.definitions.getCurrentSupplierModel() ) {
                Backbone.Model.definitions.currentSupplierId = collection.first().id;
            }

            this.render();
        },
        renderStaticContent: function () {
            var selectEl = this.$el.find("#suppliers-dropdown");
            selectEl.html("");
            Backbone.Model.definitions.supplierCollection.each(function (model) {
                selectEl.append($("<option>", { value: model.get("id") }).text(model.get("name")));
            });

            selectEl.val(Backbone.Model.definitions.currentSupplierId);
        },
        render: function () {
            this.$el.html(suppliersContainersTemplate);

            this.renderStaticContent();

            var currentSupplierModel = Backbone.Model.definitions.getCurrentSupplierModel();

            if (!currentSupplierModel) {
                this.$el.find("#suppliers-content-area").hide();
                return this;
            }

            this.$el.find("#suppliers-content-area").show();

            //USERS
            var users = new supplierUsersListView({ currentSupplierModel: currentSupplierModel });
            $(".start-section-box #suppliers-users-section").append(users.$el);

            //SETTINGS
            var settings = new supplierSettingsView({currentSupplierModel: currentSupplierModel, supplierCollection: Backbone.Model.definitions.supplierCollection});
            $(".start-section-box #suppliers-settings-section").append(settings.$el);
            
            //MAPPINGS
            var mapping = new supplierMappingListView({ id: Backbone.Model.definitions.currentSupplierId });
            $(".start-section-box #supplier-mappings-section").append(mapping.$el);
                        
            return this;
        }
    });

    return suppliersView;

});