define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/specification/specificationValueModel',
  'text!templates/imports/pendingimports/importsPendingImportsItemDetailTemplate.html',
  'text!templates/imports/pendingimports/importsPendingImportsFieldDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, fieldModel, specificationValueModel, importsPendingImportsItemDetailTemplate, importsPendingImportsFieldDetailTemplate) {

        var pendingBatchView = backbone.View.extend({
            initialize: function (options) {
                this.entityId = this.model.get("entityId");
                this.model = options.model;

                this.render();
            },
        events: {
            "click #view-details": "viewDetails",
            "click #reject": "reject"
        },
        viewDetails: function () {
            var self = this;

            this.waitForOldModel = this.entityId && !this.oldModel;
            this.waitForOldSpecificationModel = this.waitForOldModel;
            this.waitForFields = !this.model.get("fields").length;

            if (this.waitForOldModel) {
                this.oldModel = new fieldModel({ id: this.entityId });
                this.oldModel.fetch({
                    success: function () {
                        self.waitForOldModel = false;
                        self.renderDetails();
                    }
                });
            }

            if (this.waitForOldSpecificationModel) {
                self.oldSpecificationModel = new specificationValueModel({ entityId: self.entityId, type: self.model.attributes.entityTypeId });
                self.oldSpecificationModel.fetch({
                    success: function () {
                        self.waitForOldSpecificationModel = false;
                        self.renderDetails();
                    }
                });
            }

            if (this.waitForFields) {
                $.ajax({
                    type: "GET",
                    url: "/api/xconnect/supplier/" + this.options.supplierId + "/pending/" + this.model.id + "/entity/" + this.model.id
                }).done(function (model) {
                    self.model.set("fields", model.fields);
                    self.model.set("specificationFields", model.specificationFields);
                    self.waitForFields = false;
                    self.renderDetails();
                });
            }

            self.renderDetails();
        },
        reject: function () {

            inRiverUtil.NotifyConfirm("Discard Entity", "Do you want to discard this entity", this.rejectedConfirmed, this);

            this.model.attributes.reject = true;
            this.$el.find(".entity").addClass("rejected"); 

        },
        rejectedConfirmed: function(x) {
            $("#detailarea #" + x.model.id).parent().html("");

            x.model.destroy();
            x.remove();
            x.unbind(); 
        },
        renderDetails: function() {
            if (this.waitForFields || this.waitForOldModel || this.waitForOldSpecificationModel) {
                return;
            }

            var self = this;

            this.model.set("fields", _.sortBy(this.model.get("fields"), function (field) {
                return window.appHelper.fieldTypeMap[field.fieldType].get("index");
            }));

            var oldValueMap = {};
            if (this.oldModel) {
                _.each(self.oldModel.get("Fields"), function (field) {
                    oldValueMap[field.FieldType] = field.Value;
                });
            }

            _.each(this.model.get("fields"), function (field) {
                field.oldValue = oldValueMap[field.fieldType];
                field.valueMap = { "": field.value };
                if (field.value && field.value.charAt(0) === "{") {
                    try {
                        var ls = JSON.parse(field.value);
                        field.valueMap = ls.stringMap;
                    } catch (e) {
                    }
                }
            });

            var oldSpecificationValueMap = {};
            var oldSpecificationNameMap = {};
            if (self.oldSpecificationModel) {
                _.each(self.oldSpecificationModel.get("Categories"), function (category) {
                    _.each(category.SpecificationValues, function (specificationValue) {
                        oldSpecificationValueMap[specificationValue.FieldTypeId] = specificationValue.Value;
                        oldSpecificationNameMap[specificationValue.FieldTypeId] = specificationValue.Name;
                    });
                });
            }

            _.each(this.model.get("specificationFields"), function (field) {
                if (oldSpecificationValueMap[field.fieldType])
                {
                    field.oldValue = oldSpecificationValueMap[field.fieldType];
                }
                else {
                    field.oldValue = null;
                }

                field.name = oldSpecificationNameMap[field.fieldType] ? oldSpecificationNameMap[field.fieldType] : field.fieldType;
                field.valueMap = { "": field.value };
                if (field.value && field.value.charAt(0) === "{") {
                    try {
                        var ls = JSON.parse(field.value);
                        field.valueMap = ls.stringMap;
                    } catch (e) {
                    }
                }
            });

            $("#detailarea").html(_.template(importsPendingImportsFieldDetailTemplate, { data: this.model.attributes }));

            $("[name=accepted]").change(function (event) {
                var field = _.findWhere(self.model.get("fields"), { fieldType: event.target.value });
                var specification = false;
                if (!field) {
                    field = _.findWhere(self.model.get("specificationFields"), { fieldType: event.target.value });
                    specification = true;
                }
                field.accepted = event.target.checked;
                self.model.save(null, { headers: { fieldType: event.target.value, specification: specification } });
            });
        },
        render: function () {
            var self = this;
            var fileUrl = "";
            var media = self.model.get("media");
            var pictureUrl = self.model.get("PictureUrl");
            if (pictureUrl) {
                fileUrl = pictureUrl;
            }
            if (media && media.length > 0) {
                fileUrl = media[0].fileUrl;
                if (fileUrl[0] !== "/") {
                    fileUrl = "/" + fileUrl;
                }
                fileUrl = "/api/xconnect" + fileUrl;
            }
            if (fileUrl) {
                var fileExtension = fileUrl.slice((fileUrl.lastIndexOf(".") - 1 >>> 0) + 2);
                self.model.set("imgUrl", (_.contains(["jpg", "jpeg", "gif", "png"], fileExtension) || fileUrl.lastIndexOf("/picture", 0) === 0) ? fileUrl : "");
                self.model.set("fileUrl", fileUrl);
            }

            this.$el.html(_.template(importsPendingImportsItemDetailTemplate, { data: this.model.attributes }));
            return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
