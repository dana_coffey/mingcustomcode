define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'collections/pendinguploads/pendingUploadCollection',
    'views/uploadcontrol/pendingUploadView',

  'text!templates/uploadcontrol/pendingUploadListTemplate.html'
], function ($, _, backbone, alertify,pendingUploadCollection, pendingBatchView, pendingbatchesTemplate) {

    var pendingBatchesView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.supplierId = options.id;
            this.collection = new pendingUploadCollection({ id: this.supplierId }); 

            var self = this; 

            this.collection.fetch(
            {
                success: function() {
                    self.render(); 
                }
            }); 

        },
        events: {
            "click .connector-event-wrap": "onEventClick"
        },
        onEventClick: function (e) {
            e.stopPropagation();
            //window.location.href = "/app/enrich/index#entity/" + this.eventModel.id;
        },
        render: function ( ) {
            var self = this; 

            this.$el.html(_.template(pendingbatchesTemplate, {}));
            
            _.each(this.collection.models, function(model) {
                self.$el.find(".box-content ul").append(new pendingBatchView({model: model, supplierId: self.supplierId}).$el);

            }); 
            
            return this; // enable chained calls
        }
    });

    return pendingBatchesView;
});
