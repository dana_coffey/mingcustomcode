define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var pendigUploadEntityModel = Backbone.Model.extend({
        initialize: function () {
        },
    });

    return pendigUploadEntityModel;

});
