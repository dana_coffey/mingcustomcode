define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/field/fieldModel',
  'text!templates/historycontrol/historyItemDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, fieldModel, historyItemDetailTemplate) {

        var historyEntityDetailView = backbone.View.extend({
            initialize: function (options) {
                var self = this; 
                this.model = options.model;

                if (this.model.attributes.entityId > 0) {

                    this.oldModel = new fieldModel({ id: this.model.attributes.entityId });
                    this.oldModel.fetch(
                    {
                        success: function () {
                            self.render();
                        }
                    });
                } else {
                    this.render();
                }
            },
        events: {
            "click #history-view-details": "viewDetails",
            },
        viewDetails: function() {
            $("#history-detailarea").html(this.$el.find("#details-hidden-wrapper").html()); 
        },
        render: function ( ) {
           var self = this;
           var batchId = self.model.attributes.batchId;

           _.each(this.model.attributes.fields, function (field) {
               if (field.value != null && field.value.indexOf("{") == 0) {
                   try {
                       var ls = JSON.parse(field.value);
                       var keys = Object.keys(JSON.parse(field.value).stringMap);
                       field.displayValue = "";
                       _.each(keys, function (key) {
                           field.displayValue += key + " : " + ls.stringMap[key] + "<br/>";
                       });

                       if (field.displayValue.length > 0) {
                           field.displayValue = field.displayValue.substring(0, field.displayValue.length - 5);
                       }

                   } catch (e) {
                       field.displayValue = field.value;
                   }

               } else {
                   field.displayValue = field.value;
               }
           });

            if (self.oldModel != null) {
                self.model.attributes.currentPictureUrl = self.oldModel.attributes.PictureUrl;
            }

            this.$el.html(_.template(historyItemDetailTemplate, { data: this.model.toJSON() }));

           return this; // enable chained calls
        }
    });

        return historyEntityDetailView;
});
