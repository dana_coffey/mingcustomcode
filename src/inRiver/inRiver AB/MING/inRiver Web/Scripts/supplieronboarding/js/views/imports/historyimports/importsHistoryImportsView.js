define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'collections/history/historyEntityCollection',
  'views/imports/historyimports/importsHistoryImportsDetailView',
  'text!templates/imports/historyimports/importsHistoryImportsTemplate.html'
], function ($, _, backbone, alertify, modalPopup, inRiverUtil, historyEntityCollection, importsHistoryImportsDetailView, importsHistoryImportsTemplate) {

    var historyView = backbone.View.extend({
        tagName: "tr",
        initialize: function (options) {

            this.supplier = options.supplier;
            this.model = options.model;
            this.supplierId = options.supplierId; 
            this.render(); 
        },
        events: {
            "click #open-item-in-workarea": "openWorkarea",
            "click .xconnect-pending-item-date": "view",
            "click .xconnect-pending-item-name": "view",
            "click .xconnect-pending-item-count": "view",
            "click .xconnect-pending-item-by": "view",
            "click #delete-imported-hisitory": "deleteImportedHisitory"
        },
        deleteImportedHisitory: function () {

            var self = this;
            inRiverUtil.NotifyConfirm("Delete Import History", "Are you sure you want delete the imported history (" + self.model.attributes.name + ")?", function () {
                self.model.destroy({
                    url: "/api/xconnect/supplier/" + self.model.attributes.supplierId + "/history/" + self.model.attributes.id,
                    success: function () {
                        inRiverUtil.Notify("Upload history deleted successfully");

                        appHelper.event_bus.trigger('deleteImportHistory');
                    }
                });
            });
        },
        openWorkarea: function(e) {
            e.stopPropagation();
            this.entityCollection = new historyEntityCollection({ supplierId: this.supplierId, historyId: this.model.id });
            var self = this;

            this.entityCollection.fetch({
                success: function() {
                    var entities = _(self.entityCollection.models).chain().pluck("attributes").pluck("entityId").value().join(',');
                    $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                        window.location.href = "/app/enrich/#workarea?title=" + self.model.attributes.name + "&compressedEntities=" + compressed;
                    });
                }
            });
        },
        view: function (e) {
            e.stopPropagation();

            var pop = new modalPopup();
            pop.popOut(new importsHistoryImportsDetailView({
                model: this.model, parent: this
            }), { size: "medium", header: "Import History", usesavebutton: false });

        },
        
       render: function ( ) {

           var supplier = null;

           if (this.supplier)
           {
               supplier = this.supplier.toJSON();
           }

           this.$el.html(_.template(importsHistoryImportsTemplate, { model: this.model.toJSON(), supplier: supplier }));
           
           return this; // enable chained calls
        }
    });

    return historyView;
});
