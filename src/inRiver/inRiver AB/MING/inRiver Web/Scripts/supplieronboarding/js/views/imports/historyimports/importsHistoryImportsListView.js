define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'backbone-paginator',
  'collections/history/historyCollection',
  'models/xConnectUserModel',
  'views/imports/historyimports/importsHistoryImportsView',
  'text!templates/imports/historyimports/importsHistoryImportsListTemplate.html'
], function ($, _, backbone, alertify, backbonePaginator, historyCollection, xConnectUserModel, importsHistoryImportsView, importsHistoryImportsListTemplate) {

    var historyListView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.suppliers = options.suppliers;
            this.supplierIds = [];

            _.each(options.suppliers.models, function (supplier) {
                self.supplierIds.push(supplier.id);
            });

            var collection = backbone.PageableCollection.extend({
                model: xConnectUserModel,
                state: {
                    firstPage: 1,
                    currentPage: 1,
                    pageSize: 20
                },
                queryParams: {
                    currentPage: "page",
                    pageSize: "pageSize",
                    totalRecords: "totalRecords"
                }
            });

            this.paginationCollection = new collection();
            this.paginationCollection.url = "/api/xconnect/supplier/pagination";

            this.paginationCollection.fetch({
                data: { supplierIds: this.supplierIds },
                success: function () {

                    self.listenTo(self.paginationCollection, 'reset', self.renderItems);
                    self.render();
                    self.updateCtlState();

                    self.listenTo(window.appHelper.event_bus, 'deleteImportHistory', self.showFirstPage);
                    self.listenTo(window.appHelper.event_bus, 'historyupdated', self.reload);
                }
            });
        },
        events: {
            "click .connector-event-wrap": "onEventClick",
            "click #items-paginator-area-next-page-button": "onNextPage",
            "click #items-paginator-area-previous-page-button": "onPreviousPage",
            "click #page-no-button": "onGotoPage",
        },
        reload: function () {
            this.showFirstPage();
        },
        onEventClick: function (e) {
            e.stopPropagation();
        },
        showFirstPage: function () {            
            this.paginationCollection.getFirstPage({ reset: true, data: { supplierIds: this.supplierIds } });
        },
        onNextPage: function () {
            this.paginationCollection.getNextPage({ reset: true, data: { supplierIds: this.supplierIds } });
        },
        onPreviousPage: function () {
            this.paginationCollection.getPreviousPage({ reset: true, data: { supplierIds: this.supplierIds } });
        },
        onGotoPage: function (e) {
            this.paginationCollection.getPage(parseInt(e.target.value), { reset: true, data: { supplierIds: this.supplierIds } });

        },
        renderItems: function () {
            var self = this;
            this.updateCtlState();
            var ulEl = this.$el.find("#items-list");
            ulEl.html("");

            _.each(this.paginationCollection.models, function (model) {

                var supplier = self.suppliers.get(model.get("supplierId"));

                self.$el.find(".box-content table").append(new importsHistoryImportsView({ model: model, supplierId: model.get("supplierId"), supplier: supplier }).$el);
            });
        },
        updateCtlState: function(){
            var statusText = "";
            if (this.paginationCollection.state.totalRecords == 0) {
                statusText = "No items to display";
            } else if (this.paginationCollection.state.totalRecords == 1) {
                statusText = "Showing 1 item";
            } else if (this.paginationCollection.length == 1) {
                statusText = "Showing " + (((this.paginationCollection.state.currentPage - 1) * this.paginationCollection.state.pageSize) + 1) + " of " + this.paginationCollection.state.totalRecords + " items";
            } else {
                statusText = "Showing " + (((this.paginationCollection.state.currentPage - 1) * this.paginationCollection.state.pageSize) + 1) + "-" + Math.min(this.paginationCollection.state.totalRecords, ((this.paginationCollection.state.currentPage - 1) * this.paginationCollection.state.pageSize) + this.paginationCollection.state.pageSize) + " of " + this.paginationCollection.state.totalRecords + " items";
            }
            this.$el.find("#items-status-text").html(statusText);


            // Page buttons
            var buttonsEl = this.$el.find("#items-paginator-area-page-buttons-container");
            buttonsEl.html("");
            var pages = [];
            var start = Math.max(1, this.paginationCollection.state.currentPage - 2);
            var end = Math.min(start + 4, this.paginationCollection.state.totalPages);
            for (var i = start; i <= end; i++)
                pages.push(i);
            for (var j = 0; j < pages.length; j++) {
                var btn = $("<button>", {
                    html: pages[j],
                    id: "page-no-button",
                    value: pages[j],
                    disabled: pages[j] == this.paginationCollection.state.currentPage,
                    "class": "paging-button"
                });
                buttonsEl.append(btn);
            }

            // Prev / Next buttons
            if (this.paginationCollection.hasNextPage()) {
                this.$el.find("#items-paginator-area-next-page-button").css('visibility', 'visible');
            } else {
                this.$el.find("#items-paginator-area-next-page-button").css('visibility', 'hidden');
            }
            if (this.paginationCollection.hasPreviousPage()) {
                this.$el.find("#items-paginator-area-previous-page-button").css('visibility', 'visible');
            } else {
                this.$el.find("#items-paginator-area-previous-page-button").css('visibility', 'hidden');
            }
        },
        render: function () {
            var self = this;

            $("#imports-history-section .loadingspinner").remove();
            this.$el.empty();

            if (this.supplierIds.length === 0) {
                $("#imports-history-section").html("<div class='no-items-text'>Select supplier to show import history</div>");
            }
            else if (this.paginationCollection.length === 0) {
                $("#imports-history-section").html("<div class='no-items-text'>No import history</div>");
            }
            else {
                var sectionEl = $(_.template(importsHistoryImportsListTemplate, {}));
                self.$el.append(sectionEl);
                                

                _.each(this.paginationCollection.models, function (model) {

                    var supplier = self.suppliers.get(model.get("supplierId"));

                    self.$el.find(".box-content table").append(new importsHistoryImportsView({ model: model, supplierId: model.get("supplierId"), supplier: supplier }).$el);
                });
            }

            return this; // enable chained calls
        }
    });

    return historyListView;
});
