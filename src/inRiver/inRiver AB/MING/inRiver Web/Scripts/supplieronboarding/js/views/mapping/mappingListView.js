define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'collections/mappings/mappingCollection',
    'views/mapping/mappingView',
    'views/mapping/addMappingView',
    'text!templates/mapping/mappingListTemplate.html'
], function ($, _, backbone, alertify,modalPopup, mappingCollection, mappingView,addMappingView,  mappingListTemplate) {

    var mappingListView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.supplierId = options.id;
            this.collection = new mappingCollection({ id: this.supplierId });

            this.listenTo(window.appHelper.event_bus, 'mappingadded', this.reload);

            var self = this; 

            this.collection.fetch(
            {
                success: function() {
                    self.render(); 
                }
            }); 

        },
        events: {
            "click #list-of-mapping-add": "onAddMapping"
        },
        onAddMapping: function () {
            var pop = new modalPopup();
            pop.popOut(new addMappingView({
                supplierId: this.supplierId
            }), { size: "medium", header: "Add Mapping", usesavebutton: false });
          
        },
        reload: function() {
            var self = this; 
            this.collection.fetch(
            {
                success: function () {
                    self.render();
                }
            });
        },
        render: function ( ) {
            var self = this; 

            this.$el.html(_.template(mappingListTemplate, {}));
            
            _.each(this.collection.models, function(model) {
                self.$el.find(".box-content ul").append(new mappingView({model: model, supplierId: self.supplierId}).$el);

            }); 
            
            return this; // enable chained calls
        }
    });

    return mappingListView;
});
