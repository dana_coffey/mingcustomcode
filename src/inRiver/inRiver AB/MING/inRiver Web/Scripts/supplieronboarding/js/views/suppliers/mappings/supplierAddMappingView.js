﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-steps',
  'dropzone',
  'multiple-select',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/languages/LanguagesCollection',
  'sharedjs/collections/entities/entityCollection',
  'models/mapping/fileImportConfigurationModel',
  'models/mapping/mappingModel',
  'text!templates/suppliers/mappings/supplierImportDataTemplate.html',
  'text!templates/suppliers/mappings/supplierMappingConfigurationTemplate.html',
  'text!templates/suppliers/mappings/supplierMappingConfigurationTabsTemplate.html',
  'text!templates/suppliers/mappings/supplierMappingSpecificationSelectionTemplate.html',
  'text!templates/suppliers/mappings/supplierMappingConfigurationColumnDataTemplate.html',
  'text!templates/suppliers/mappings/supplierMappingConfigurationColumnDataSpecificationTemplate.html',
  'sharedjs/models/fieldtype/FieldTypeSettingModel'

], function ($, _, backbone, jquerySteps, dropzone, multipleSelect, inRiverUtil, languagesCollection, entityCollection, fileImportConfigurationModel,
    mappingModel, importDataTemplate, mappingConfigurationTemplate, mappingConfigurationTabsTemplate,
    mappingSpecificationSelectionTemplate, mappingConfigurationColumnDataTemplate, mappingConfigurationColumnDataSpecificationTemplate, fieldTypeSettingModel) {

    var addMappingView = backbone.View.extend({
        initialize: function (options) {
            this.supplierId = options.supplierId;
            window.appHelper.languagesCollection = new languagesCollection();
            window.appHelper.languagesCollection.fetch();

            this.fieldTypeSetting = null;
            this.DefaultTab = true;
            this.SpecificationId = null;

            this.render();
        },
        events: {
            "click #showEmptyColumns": "onToggleShowEmptyColumns",
            "click #clearEmptyValue": "onToggleClearEmptyValues",
            "change #selectMappingEntityType": "onSelectEntityType",
            "change #mapping-columndata-entitytype .import-mapping-language": "onChangeLanguage",
            "change #mapping-columndata-specification .import-mapping-language": "onChangeSpecificationLanguage",
            "click #save-mapping": "showSaveMappingDialog",
            "click #do-save-mapping": "onSaveMapping",
            "click #do-cancel-mapping": "onCancelSaveMapping",
            "click #update-mapping": "onUpdateMapping",
            "click #delete-mapping": "onDeleteMapping",
            "change #config-mapping-name": "onMappingNameChange",
            "click #import-specification-tab-entitytype": "onTabEntityTypeClick",
            "click #import-specification-tab-specification": "onTabSpecificationClick",
            "click #specification-remove": "removeSpecification"
        },
        onFiltering: function () {

            var filterString = this.$el.find("input#filter").val();

            if (filterString && filterString.length > 2) {
                $("div.item").hide();
                $("div.item").each(function (i, args) {
                    if (args.innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(args).show();
                });
            } else {
                $("div.item").show();
            }

        },
        removeSpecification: function () {

            this.SpecificationId = null;

            $("#supplier-onboarding-mapping-specification-list").multipleSelect("uncheckAll");
            this.$el.find("#specification-remove").hide();

            this.$el.find("#mapping-columndata").html(_.template(mappingConfigurationColumnDataTemplate, { data: this.fileImportConfigurationModel.toJSON() }));
            this.$el.find("#mapping-specification-tabs").html("");

            this.createFieldsMultipleSelectControl();

            this.setMappingControllsDisplay();

        },
        onSpecificationSelection: function (e) {
            var self = this;
            this.SpecificationId = e.value;
            self.$el.find("#mapping-specification-tabs").html(
                                _.template(mappingConfigurationTabsTemplate,
                                { data: self.fileImportConfigurationModel.toJSON() }));

            self.$el.find("#import-specification-tab-entitytype").addClass("current");

            if (!this.DefaultTab) {
                self.onTabSpecificationClick();
            }

            self.setMappingControllsDisplay();
        },
        deselect: function (e) {
            if (e.target.id == "filter" || e.target.id == "fields") {
                return;
            }

            this.$el.find("#specification-list:first").toggle(50);

            //var $box = this.$el.find("#app-tools-container");
            //$box.hide();
            $(document).unbind('click');
        },
        onTabSpecificationClick: function () {
            var self = this;
            this.DefaultTab = false;

            self.$el.find("#import-specification-tab-entitytype").removeClass("current");
            self.$el.find("#import-specification-tab-specification").addClass("current");

            if (this.SpecificationId) {

                this.collection = new entityCollection();
                this.collection.url = '/api/specificationtemplate/' + this.SpecificationId + '/field';

                this.collection.fetch({
                    success: function (model, response) {
                        self.$el.find("#mapping-columndata").html(_.template(mappingConfigurationColumnDataSpecificationTemplate,
                            {
                                data: self.fileImportConfigurationModel.toJSON(),
                                specificationFieldTypes: model
                            }));

                        self.createFieldsMultipleSelectControl();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

            }
        },
        onTabEntityTypeClick: function () {
            var self = this;
            this.DefaultTab = true;

            self.$el.find("#import-specification-tab-entitytype").addClass("current");
            self.$el.find("#import-specification-tab-specification").removeClass("current");

            self.$el.find("#mapping-columndata").html(_.template(mappingConfigurationColumnDataTemplate, { data: self.fileImportConfigurationModel.toJSON() }));

            self.createFieldsMultipleSelectControl();
        },
        onToggleShowEmptyColumns: function () {
            var self = this;

            this.fileImportConfigurationModel.set("ShowEmptyColumns", this.$el.find("#showEmptyColumns")[0].checked);

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(mappingConfigurationTemplate, {
                        data: self.fileImportConfigurationModel.toJSON(),
                        hasSpecification: self.fileImportConfigurationModel.attributes.HasSpecificationRelation
                    }));

                    self.setMappingControllsDisplay();

                    self.addSpecificationControls();
                    //self.addMappingColumnsAndFieldsControls();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onChangeLanguage: function (e) {
            var newLanguage = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;

            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id === columnId;
            });

            column.LanguageCode = newLanguage;
            this.fileImportConfigurationModel.save();
        },
        onChangeSpecificationLanguage: function (e) {
            var newLanguage = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;

            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id === columnId;
            });

            column.SpecificationLanguageCode = newLanguage;
            this.fileImportConfigurationModel.save();
        },
        onMappingNameChange: function (e) {
            this.fileImportConfigurationModel.attributes.name = this.$el.find("#config-mapping-name").val();

        },
        onChangeField: function (e) {
            var self = this;

            var newField = e.value;

            var columnId = e.instance.$parent[0].parentNode.id;
            var column = _.find(this.fileImportConfigurationModel.get("Columns"), function (item) {
                return item.Id === columnId;
            });

            var id = $(e)[0].instance.$el.closest("select").attr("id");

            //Check if field is selected in Specification tab.
            if (id === "import-mapping-fields-specification" && newField !== "") {
                column.SpecificationFieldTypeId = newField;
                column.IsSpecificationField = true;
            }
            else {
                column.FieldTypeId = newField;
                column.IsSpecificationField = false;
            }

            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function (model, response) {

                    //If we change field for first time to mapp with LocaleString field.
                    //The LanguageCode is empty. But needs to have a value and then we set the first Language.
                    if (response.Languages.length > 0) {

                        _.each(response.Columns, function (column) {
                            if (column.Localized && column.LanguageCode == "") {
                                column.LanguageCode = response.Languages[0].Name;
                            }

                            if (column.SpecificationLocalized && column.SpecificationLanguageCode == "") {
                                column.SpecificationLanguageCode = response.Languages[0].Name;
                            }
                        });
                    }

                    if (self.DefaultTab != undefined) {

                        if (self.DefaultTab) {
                            self.onTabEntityTypeClick();
                        }
                        else {
                            self.onTabSpecificationClick();
                        }
                    }
                    else {
                        self.$el.find("#import-configuration").html(_.template(mappingConfigurationTemplate, {
                            data: self.fileImportConfigurationModel.toJSON(),
                            hasSpecification: self.fileImportConfigurationModel.attributes.HasSpecificationRelation
                        }));

                        self.setMappingControllsDisplay();
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onToggleClearEmptyValues: function () {
            this.fileImportConfigurationModel.set("ClearEmptyValues", this.$el.find("#clearEmptyValue")[0].checked);
            this.fileImportConfigurationModel.save();
        },
        onSelectEntityType: function () {
            var self = this;

            this.fileImportConfigurationModel.set("EntityTypeId", this.$el.find("#selectMappingEntityType").val());
            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(mappingConfigurationTemplate, {
                        data: self.fileImportConfigurationModel.toJSON(),
                        hasSpecification: self.fileImportConfigurationModel.attributes.HasSpecificationRelation
                    }));

                    if (!self.fileImportConfigurationModel.attributes.HasSpecificationRelation) {
                        self.SpecificationId = null;
                    }

                    self.setMappingControllsDisplay();

                    self.addSpecificationControls();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        showSaveMappingDialog: function () {
            this.$el.find("#save-mapping-dialog").show();
            this.$el.find("#select-existing-mapping").hide();
        },
        onCancelSaveMapping: function () {
            this.$el.find("#save-mapping-dialog").hide();
            this.$el.find("#select-existing-mapping").show();
            this.$el.find("#config-mapping-name").val("");
        },
        onSaveMapping: function () {
            var self = this;

            //Hämta namn på mappningen. 
            if (this.$el.find("#config-mapping-name").val() == "") {
                this.$el.find("#save-mapping-name").addClass("error");
                this.$el.find("#config-mapping-name-error").show();
                return;
            }

            this.mappingModel = new mappingModel({ supplierId: this.supplierId });
            this.mappingModel.attributes.Name = this.$el.find("#config-mapping-name").val();
            this.mappingModel.attributes.EntityType = this.fileImportConfigurationModel.attributes.EntityTypeId;
            this.mappingModel.attributes.Columns = this.fileImportConfigurationModel.attributes.Columns;

            if (this.SpecificationId) {
                this.mappingModel.set("SpecificationTemplateId", parseInt(this.SpecificationId));
            }
            else {
                this.mappingModel.set("SpecificationTemplateId", null);
            }

            this.mappingModel.set("MappingName", this.$el.find("#config-mapping-name").val());

            this.fileImportValidationModel = null;

            this.mappingModel.save(null, {
                success: function (model) {
                    inRiverUtil.Notify("Mapping successfully saved");
                    self.fileImportConfigurationModel.attributes.Id = model.attributes.Id;
                    self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                    self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                    self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;

                    window.appHelper.event_bus.trigger('closepopup');
                    window.appHelper.event_bus.trigger('mappingadded' + self.supplierId);
                    self.undelegateEvents();
                    self.$el.removeData().unbind();
                    self.remove();

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onUpdateMapping: function () {
            var self = this;

            this.fileImportConfigurationMappingModel = new fileImportConfigurationMappingModel({
                MappingId: this.fileImportConfigurationModel.attributes.MappingId
            });


            this.fileImportConfigurationMappingModel.fetch({
                success: function () {
                    self.fileImportConfigurationMappingModel.attributes = self.fileImportConfigurationModel.attributes;
                    self.fileImportConfigurationMappingModel.save(null, {
                        success: function (model) {
                            inRiverUtil.Notify("Mapping successfully updated");
                            self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                            self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                            self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;
                            self.$el.find("#import-configuration").html(_.template(mappingConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON() }));
                            window.appHelper.event_bus.trigger('closepopup');
                            self.parent.reload();

                            self.setMappingControllsDisplay();
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        moveToConfiguration: function () {
            var self = this;

            var existingConfigurationName = "";

            if (this.fileImportConfigurationModel && this.fileImportConfigurationModel.get("name")) {
                existingConfigurationName = this.fileImportConfigurationModel.get("name");
            }

            this.fileImportConfigurationModel = new fileImportConfigurationModel({ Id: this.modelid });
            this.fileImportConfigurationModel.fetch({
                success: function () {

                    var columns = [];

                    _.each(self.fileImportConfigurationModel.get("Columns"), function (model) {
                        if (model.ColumnName !== "") {
                            columns.push(model);
                        }
                    });

                    self.fileImportConfigurationModel.set("Columns", columns);

                    self.$el.find("#import-configuration").html(_.template(mappingConfigurationTemplate, {
                        data: self.fileImportConfigurationModel.toJSON(),
                        hasSpecification: self.fileImportConfigurationModel.attributes.HasSpecificationRelation
                    }));

                    if (existingConfigurationName !== "") {
                        self.$el.find("#config-mapping-name")[0].value = existingConfigurationName;
                        self.fileImportConfigurationModel.set("name", existingConfigurationName);
                    }

                    self.$el.find("#import-configuration").addClass("noSpecificationControlls");

                    self.addSpecificationControls();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        addSpecificationControls: function () {
            var self = this;

            if (self.fileImportConfigurationModel.attributes.HasSpecificationRelation) {

                this.collection = new entityCollection();
                this.collection.url = '/api/specification/gettemplates?templateId=' + null;

                this.collection.fetch({
                    success: function (model, response) {

                        self.specifications = model.models;

                        self.$el.find("#mapping-specification").html(_.template(mappingSpecificationSelectionTemplate, { data: model.toJSON() }));
                        self.$el.find("#mapping-columndata").html(_.template(mappingConfigurationColumnDataTemplate, { data: self.fileImportConfigurationModel.toJSON() }));

                        self.$el.find("#supplier-onboarding-mapping-specification-list").html("");

                        var selectedSpecification = null;

                        _.each(model.models, function (specification) {
                            if (self.SpecificationId && self.SpecificationId === specification.get("Id")) {
                                self.$el.find("#supplier-onboarding-mapping-specification-list").append("<option value=\"" + specification.get("Id") + "\" selected=\"selected\">" + specification.get("DisplayName") + "</option>");

                                selectedSpecification = specification;
                            }
                            else {
                                self.$el.find("#supplier-onboarding-mapping-specification-list").append("<option value=\"" + specification.get("Id") + "\">" + specification.get("DisplayName") + "</option>");
                            }
                        });

                        self.createFieldsMultipleSelectControl();

                        self.$el.find("#supplier-onboarding-mapping-specification-list").multipleSelect({
                            width: 152,
                            multipleWidth: 152,
                            multiple: false,
                            single: true,
                            filter: true,
                            selectAll: false,
                            ellipsis: true,
                            onClick: function (e) {

                                self.$el.find("#specification-remove").show();

                                self.onSpecificationSelection(e);
                            }
                        });

                        //When changing steps we want to save selected specification if we go back to configuration step again.
                        if (selectedSpecification) {
                            var e = {
                                label: selectedSpecification.get("DisplayName"), value: selectedSpecification.get("Id")
                            }
                            self.onSpecificationSelection(e);
                        }
                        else {
                            self.$el.find("#specification-remove").hide();
                        }

                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            else {
                self.specifications = null;
                self.$el.find("#mapping-columndata").html(_.template(mappingConfigurationColumnDataTemplate, { data: self.fileImportConfigurationModel.toJSON() }));

                self.createFieldsMultipleSelectControl();
            }
        },
        createFieldsMultipleSelectControl: function () {
            var self = this;
            self.$el.find(".import-mapping-fields").multipleSelect({
                width: 152,
                multipleWidth: 152,
                multiple: false,
                single: true,
                filter: true,
                selectAll: false,
                ellipsis: true,
                onClick: function (e) {
                    self.onChangeField(e);
                }
            });
        },
        finish: function () {
            this.onSaveMapping();
        },
        setMappingControllsDisplay: function () {
            var self = this;

            if (self.SpecificationId === null) {
                self.$el.find("#import-configuration").addClass("noSpecificationControlls");
            } else {
                self.$el.find("#import-configuration").removeClass("noSpecificationControlls");
            }
        },
        render: function () {
            var self = this;
            this.$el.html(_.template(importDataTemplate, {}));
            this.$el.find("#import-data-wizard").steps(
            {
                transitionEffect: "slideLeft",
                transitionEffectSpeed: 400,
                enableAllSteps: false,
                autoFocus: true,
                enableCancelButton: true,
                onCanceled: function () {
                    self.onCancel();
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (currentIndex === 0 && self.modelid == null) {
                        return false;
                    }

                    if (currentIndex === 0 && newIndex === 1) {
                        self.moveToConfiguration();
                    }

                    return true;
                },
                onFinished: function () {
                    self.finish();
                }
            });

            this.myAwesomeDropzone = new dropzone(this.$el.find("#import-dropzone")[0], {
                url: "/fileupload/file",
                maxFilesize: 1000,
                parallelUploads: 10,
                thumbnailWidth: 100,
                thumbnailHeight: 100,
                maxFiles: 1,
                autoProcessQueue: true,
                dictDefaultMessage: "Drop file here or Click to Browse",
                init: function () {
                    var thisDropzone = this;

                    this.on("processing", function () {
                        thisDropzone.options.autoProcessQueue = true;
                    });

                    this.on("addedfile", function () {
                        var files = thisDropzone.getAcceptedFiles();

                        if (files.length === 1) {
                            thisDropzone.removeFile(files[0]);
                        }

                        $("#import-dropzone").removeClass("dropzone-button");
                        $("#import-dropzone").addClass("dropzone");
                    });

                    this.on("success", function (file, id) {
                        self.fileImportValidationModel = null;
                        self.modelid = id;
                    });

                    this.on("error", function (file, errorMessage, xhr) {

                        if (xhr == undefined) {
                            inRiverUtil.NotifyError("Upload failed", errorMessage, "Filename: " + file.name);
                        }
                        else {
                            if (xhr !== 0 && xhr.status === 409) {
                                inRiverUtil.NotifyError("Upload failed", xhr.statusText, "Filename: " + file.name);
                            } else {
                                inRiverUtil.NotifyError("Upload failed", "Upload of file failed", "");
                            }
                        }

                        // $("#import-dropzone").show(); ?
                    });
                },
                error: function (model) {
                    model.previewElement.classList.add("dz-error");
                    var ref = model.previewElement.querySelectorAll("[data-dz-errormessage]");
                    ref[0].innerText = model.xhr.statusText;
                },
                accept: function (file, done) {

                    // If the settings for RegEx haven't been loaded yet, time to load.
                    if (self.fieldTypeSetting == null) {
                        self.fieldTypeSetting = new fieldTypeSettingModel("ResourceFilename");
                        self.fieldTypeSetting.fetch({
                            async: false,   // Wait until RegEx data is loaded until we try to validate.
                            success: function (model, response) {
                                // We only want to fill this.attributes for the Model-object.
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    }

                    if (!self.fieldTypeSetting.ValidateRegExp(file.name)) {
                        done("Filename '" + file.name + "' has illegal characters");
                        return;
                    }

                    done();
                }
            });

            this.listenTo(window.appHelper.event_bus, 'validationstatus', this.validateStatus);

            return this;
        }
    });

    return addMappingView;
});
