﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fileImportConfigurationMappingModel = Backbone.Model.extend({
        idAttribute: "MappingId",
        initialize: function (options) {
            this.supplierId = options.supplierId; 

        },
        urlRoot: '/api/xconnect/supplier/' + this.supplierId + '/mapping',
    });

    return fileImportConfigurationMappingModel;

});
