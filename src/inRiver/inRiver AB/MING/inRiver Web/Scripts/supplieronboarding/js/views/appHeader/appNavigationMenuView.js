﻿define([
  'jquery',
  'underscore',
  'backbone',
    'modalPopup',
    'sharedjs/misc/permissionUtil',
    'text!templates/appHeader/navigationMenuTemplate.html'
], function ($, _, backbone, modalPopup, permissionUtil, navigationMenuTemplate) {

    var appContextMenuView = backbone.View.extend({
        initialize: function () {
            this.render();
        },
        events: {
        },             
        render: function () {
            this.$el.html(navigationMenuTemplate);
            
            return this;
        }
    });

    return appContextMenuView;

});
