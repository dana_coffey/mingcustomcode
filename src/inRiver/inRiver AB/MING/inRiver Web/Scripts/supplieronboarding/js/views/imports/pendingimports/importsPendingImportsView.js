define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'views/imports/pendingimports/importsPendingImportsDetailView',
  'text!templates/imports/pendingimports/importsPendingImportsTemplate.html'
], function ($, _, backbone, alertify, modalPopup, importsPendingImportsDetailView, importsPendingImportsTemplate) {

    var pendingUploadView = backbone.View.extend({
        tagName: "tr",
        initialize: function (options) {
            this.model = options.model;
            this.supplierId = options.supplierId;
            this.supplier = options.supplier;
            this.render(); 
        },
        events: {
            "click": "view"
        },
        view: function () {
            var pop = new modalPopup();
            pop.popOut(new importsPendingImportsDetailView({
                model: this.model, parent: this
            }), { size: "medium", header: "Pending Import", usesavebutton: false });

        },
        
       render: function ( ) {
           this.$el.html(_.template(importsPendingImportsTemplate, { model: this.model.attributes, supplier: this.supplier.attributes }));

           return this; // enable chained calls
        }
    });

    return pendingUploadView;
});
