define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
    'sharedjs/models/field/fieldModel',
  'text!templates/uploadcontrol/pendingUploadItemDetailTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, fieldModel, pendingUploadItemDetailTemplate) {

        var pendingBatchView = backbone.View.extend({
            initialize: function (options) {
                var self = this; 
                this.model = options.model;
                //this.parent = options.parent;
                //this.supplierId = this.parent.supplierId; 
                //this.batchId = this.model.attributes.batchId;

                if (this.model.attributes.entityId > 0) {

                    this.oldModel = new fieldModel({ id: this.model.attributes.entityId });
                    this.oldModel.fetch(
                    {
                        success: function () {
                            self.render();
                        }
                    });
                } else {
                    this.render();
                }
                
            },
        events: {
            "click #view-details": "viewDetails",
            "click #reject": "reject"
        },
        viewDetails: function() {
            $("#detailarea").html(this.$el.find("#details-hidden-wrapper").html()); 
        },
        reject: function () {

            inRiverUtil.NotifyConfirm("Discard Entity", "Do you want to discard this entity", this.rejectedConfirmed, this);

            this.model.attributes.reject = true;
            this.$el.find(".entity").addClass("rejected"); 

        },
        rejectedConfirmed: function(x) {
            $("#detailarea #" + x.model.id).parent().html("");

            x.model.destroy();
            x.remove();
            x.unbind(); 
        },
        render: function ( ) {
           var self = this;
           var batchId = self.model.attributes.batchId;

           _.each(this.model.attributes.fields, function (field) {
               if (field.value != null && field.value.indexOf("{") == 0) {
                   try {
                       var ls = JSON.parse(field.value);
                       var keys = Object.keys(JSON.parse(field.value).stringMap);
                       field.displayValue = ""; 
                       _.each(keys, function(key) {
                           field.displayValue += key + " : " + ls.stringMap[key] + "<br/>"; 
                       }); 

                       if (field.displayValue.length > 0) {
                           field.displayValue = field.displayValue.substring(0, field.displayValue.length - 5); 
                       }

                   } catch(e) {
                       field.displayValue = field.value; 
                   }

               } else {
                   field.displayValue = field.value; 
               }
               if (self.oldModel != null) {
                   var x = _.find(self.oldModel.attributes.Fields, function(oldfield) {
                       return field.fieldType == oldfield.FieldType;
                   });

                   self.model.attributes.currentPictureUrl = self.oldModel.attributes.PictureUrl; 


                   if (x != null) {
                       field.oldValue = x.Value;
                       if (typeof field.oldValue != 'string') {
                           try {
                               var oldkeys = Object.keys(field.oldValue);

                               ls = field.oldValue;
                               field.oldValue = "";
                               _.each(oldkeys, function (key) {

                                   if (keys != null && _.contains(keys, key)) {

                                       if (ls[key].length > 1) {
                                           field.oldValue += key + " : " + ls[key][1] + "<br/>";
                                       }
                                   }
                               });

                               if (field.oldValue.length > 0) {
                                   field.oldValue = field.oldValue.substring(0, field.oldValue.length - 5);
                               }

                           } catch (e) {
                               field.oldValue = field.oldValue;
                           }

                       }
                   }

               }

            }); 


           this.$el.html(_.template(pendingUploadItemDetailTemplate, { data: this.model.toJSON() }));



           return this; // enable chained calls
        }
    });

    return pendingBatchView;
});
