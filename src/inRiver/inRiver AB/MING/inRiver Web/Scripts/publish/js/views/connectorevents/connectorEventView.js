define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'text!templates/connectorevents/connectorEventTemplate.html'
], function ($, _, backbone, alertify, connectorEventTemplate) {

    var connectorEventView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {

            this.model = options.model;
        },
        events: {
            "click .connector-event-wrap": "onEventClick"
        },
        onEventClick: function (e) {
            e.stopPropagation();
            //window.location.href = "/app/enrich/index#entity/" + this.eventModel.id;
        },
        render: function ( ) {

            if (this.model == null) {
                return this;
            }

            this.$el.html(_.template(connectorEventTemplate, { data: this.model.toJSON() }));
          

            return this; // enable chained calls
        }
    });

    return connectorEventView;
});
