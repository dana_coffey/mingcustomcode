﻿define([
  'jquery',
  'underscore',
  'backbone',
  'views/appheader/appContextMenuView'
], function ($, _, Backbone, appContextMenuView) {

    var appHeaderView = Backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        render: function () {
            $("#context-menu-container").html(new appContextMenuView().el);
            //$("#search-container").html(new quickSearchView().el);
            return this;
        }
    });

    return appHeaderView;

});
