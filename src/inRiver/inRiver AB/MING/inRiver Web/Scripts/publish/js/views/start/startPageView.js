define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'jquery-ui',
  'text!templates/start/startPageTemplate.html',
  'views/start/startPageConnectorListView',
  'views/start/startPageLatestEventListView'
], function ($, _, backbone, backboneforms, jqueryui, startPageTemplate, startPageConnectorListView, startPageLatestEventListView) {

    var startSupplyPage = backbone.View.extend({
        initialize: function () {

            var self = this;
            self.render();
        },
        render: function () {
            var startPage = this.$el.html(startPageTemplate);
            var connectorList = new startPageConnectorListView();
            startPage.find("#list-of-connectors").append(connectorList.$el);
            var latestEventsList = new startPageLatestEventListView();
            startPage.find("#latest-events-from-connectors").append(latestEventsList.$el);
        }
    });

    return startSupplyPage;
});

