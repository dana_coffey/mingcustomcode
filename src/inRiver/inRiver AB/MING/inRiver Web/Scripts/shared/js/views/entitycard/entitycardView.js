define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/resource/resourceModel',
  'modalPopup',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/copyEntity/copyEntityWizzardView',
  'sharedjs/views/templateprinting/TemplatePrintingSetupView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'text!sharedtemplates/entitycard/entitycardTemplate.html',
  'text!sharedtemplates/entitycard/workareaCardTemplate.html',
  'text!sharedtemplates/workarea/workareaMultiselectCheckboxControlTemplate.html',
  'text!sharedtemplates/resource/resourceDownloadPopup.html'
], function ($, _, backbone, completenessModel, relationModel, resourceModel, modalPopup, entityDetailSettingView,
    entityDetailSettingWrapperView, completenessPopupView, contextMenuView, copyEntityWizzardView, templatePrintingSetupView,
    inRiverUtil, permissionUtil, entitycardTemplate, workareaCardTemplate, workareaMultiselectCheckboxControlTemplate, resourceDownloadPopup) {

    var entitycardView = backbone.View.extend(
        {
        initialize: function (options) {
            this.type = options.type;
            this.taskRelations = options.taskRelations;
            this.parent = options.parent;
            this.checked = false;

            this.disableOnCardClick = false;
            if (options.parent.handler.app == "planrelease") {
                this.disableOnCardClick = true;
            }

            this.listenTo(window.appHelper.event_bus, 'entityupdated-entity-card-view', this.entityUpdated);
        },
        events: {
            "click #card-completeness-text": "onShowCompletnessDetails",
            "click #large-card-completeness-text": "onShowCompletnessDetails",
            "click .completeness-small": "onShowCompletnessDetails",
            "click .card-wrap": "onCardClick",
            "click .card-wrap-large": "onCardClick",
            "click #edit-button": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "click #delete-button": "onEntityDelete",
            "click #task-button": "onAddTask",
            "click #download-button": "onDownloadResouce",
            "click #upload-button": "onUploadResource",
            "click #remove-button": "onRemoveEntity",
            "click .selected-card": "onCardSelected",
            "click #copy-button": "onCopyEntityClick",
            "click #menu-open-in-enrich-button": "onEnrichOpenClick",
            "click #pdf-button": "onDownloadPDF",
            "click #preview-button": "onShowPreview",
        },
        onCardSelected: function (e) {
            e.stopPropagation();

            var card = this.$el.find(".card-wrap-large");
            var checkBoxElement = card.find(".selected-card").find("i");

            if (card.hasClass("card-selected")) {
                card.removeClass("card-selected");

                checkBoxElement.removeClass("fa-check-square-o");
                checkBoxElement.removeClass("fa-square-o");
                checkBoxElement.addClass("fa-square-o");
                this.setChecked(false);

            } else {
                card.addClass("card-selected");

                checkBoxElement.removeClass("fa-square-o");
                checkBoxElement.removeClass("fa-check-square-o");
                checkBoxElement.addClass("fa-check-square-o");
                this.setChecked(true);
            }

            var parentElement = this.$el.parents("#search-result-container");

            if (parentElement.length > 0) {
                var selectedChildren = parentElement.find(".card-selected");

                if (selectedChildren.length > 0) {

                    var allSelectedChildren = parentElement.find(".card-wrap-large");

                    if (selectedChildren.length < allSelectedChildren.length) {
                        parentElement.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "deselectEntitycards", title: "Deselect selected entities", icon: "fa fa-minus-square-o", text: "Select All" }));
                    } else {
                        parentElement.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "deselectAllEntitycards", title: "Deselect all entities", icon: "fa fa-check-square-o", text: "Select All" }));
                    }

                } else {
                    parentElement.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "selectAllEntitycards", title: "Select all entities", icon: "fa fa-square-o", text: "Select All" }));
                }
                window.appHelper.event_bus.trigger("selectedEntitiesChanged");
            }

        },
        removeFromWorkArea:function() {
            this.parent.removeEntityFromWorkarea(this.model.id);
            this.unbind();
            this.remove();
        },
        onRemoveEntity: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            this.removeFromWorkArea();
        },

        onEntityDelete: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            inRiverUtil.deleteEntity(this);

        },
        onAddTask: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });
        },
        taskAddedNowLinkEntity: function (model) {
            this.targetIdToCreate = model.id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function () {
                    inRiverUtil.Notify("Relation successfully created");
                },
                error: function (result, response) {
                    inRiverUtil.OnErrors(result, response);
                }
            });
        },
        onDownloadResouce: function (e) {
            var self = this;
            e.stopPropagation();
            e.preventDefault();

            self.resource = new resourceModel({ id: self.model.id, urlRoot: '/api/resource' });
            self.resource.fetch({
                url: '/api/resource/' + this.model.id,
                success: function () {

                    //self.$el.append(_.template(resourceDownloadPopup, { resource: self.resource.toJSON() }));

                    var downloadElement = _.template(resourceDownloadPopup, { resource: self.resource.toJSON() });
                   // self.$el.append(downloadElement);

                    if (self.$el.find(".download-menu").length === 0) {
                        self.$el.append(downloadElement);
                    } else {
                        self.$el.find(".download-menu")[0] = downloadElement;
                    }

                    self.$el.find(".download-menu").menu();

                    var menu = self.$el.find(".download-menu").show().position({
                        my: "left top",
                        at: "left bottom",
                        of: e.currentTarget
                    });

                    menu.mouseleave(function () {
                         menu.hide();
                    });

                    $(document).one("click", function () {
                        menu.hide();
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });


        },
        onUploadResource: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.onToggleTools(e);
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },
        onEditDetail: function (e) {
            e.stopPropagation();
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var self = this;
            var xmlhttp = new XMLHttpRequest();
            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");
            }
            window.appHelper.event_bus.trigger('entityupdated', this.model.id);

            self.entityUpdated(this.model.id);
        },
        onShowPreview: function (e) {
            e.stopPropagation();
            inRiverUtil.openEntityPreview(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPreviewTemplates);
        },
        onDownloadPDF: function (e) {
            e.stopPropagation();
            inRiverUtil.createEntityPDF(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPDFTemplates);
        },
        onCardClick: function (e) {

            
            window.appHelper.event_bus.trigger("deselectAllEntityCards");
            this.setChecked(true);

            if (this.disableOnCardClick)
                return;

            e.stopPropagation();

            this.goTo("entity/" + this.model.id);
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        onClose: function () {
        },
        entityUpdated: function (id) {

            if (id == this.model.id) {

                var self = this;
                this.model.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

                this.stopListening(window.appHelper.event_bus, ["entityupdated-entity-card-view"]);
            }
        },
        entityDeleted: function (id) {
            if (id == this.model.id) {
                this.$el.html("");
                this.stopListening(window.appHelper.event_bus);
                this.remove();
                this.unbind();
            }

        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        getId: function () {
            return this.model.attributes.id;
        },
        getEntityType: function () {
            return self.model.attributes.EntityType;
        },
        updateShowDeleteAll: function () {
            var deleteselectedButton = $('#deleteselected');
            if ($(".card-selected").length) {
                deleteselectedButton.show();
            }
            else {
                deleteselectedButton.hide();
            }
        },
        setStatus: function (status) {
            var entityCardStatus = this.$el.find(".entity-card-status");
            entityCardStatus.removeClass("fa-spinner");
            entityCardStatus.removeClass("fa-spin");
            switch (status) {
                case "pending":
                    entityCardStatus.addClass("fa-spinner");
                    entityCardStatus.addClass("fa-spin");
                    entityCardStatus.prop("title", "Saving...");
                    break;
                case "ok":
                    entityCardStatus.addClass("fa-check");
                    entityCardStatus.addClass("entity-card-status-ok");
                    entityCardStatus.prop("title", "Successfully updated");
                    break;
                case "failed":
                    entityCardStatus.addClass("fa-close");
                    entityCardStatus.addClass("entity-card-status-failed");
                    entityCardStatus.prop("title", "Failed to update");
                    break;
                default:
                    break;
            }
        },
        setChecked: function (checked) {
            this.checked = checked;

            var card = this.$el.find(".card-wrap-large");
            var checkBoxElement = card.find(".selected-card").find("i");

            checkBoxElement.removeClass("fa-square-o");
            checkBoxElement.removeClass("fa-check-square-o");
            if (checked) {

                if (!card.hasClass("card-selected")) {
                    card.addClass("card-selected");
                }

                checkBoxElement.addClass("fa-check-square-o");
            } else {

                if (card.hasClass("card-selected")) {
                    card.removeClass("card-selected");
                }

                checkBoxElement.addClass("fa-square-o");
            }

            this.updateShowDeleteAll();
        },
        onCopyEntityClick: function (e) {
            //Open popup with wizzard.
            e.stopPropagation();

            this.modal = new modalPopup();
            this.modal.popOut(new copyEntityWizzardView({ id: this.model.id, entityTypeId: this.model.attributes.EntityType, model: this.model }), { size: "medium", header: "Copy " + this.model.attributes.EntityType });
        },
        renderContextMenu: function () {
            var that = this;
            var isLocked = inRiverUtil.isLocked(this.model.attributes.LockedBy);
            this.tools = new Array();

            if (!isLocked) {
                this.tools.push({ name: "edit-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            if (this.model.attributes.Starred == "1") {
                this.tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star", text: "Unstar" });

            }
            else {
                this.tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty", text: "Star" });
            }

            if (window.appHelper.currentApp != "Enrich") {
                if (document.getElementById('workarea-multiselect-container')) {

                    var callback = function (access, id, self) {
                        if (access) {
                            self.tools.push({ name: "menu-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                        }
                    };

                    permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this);                  
                }
            }

            if (!isLocked) {
                this.tools.push({ name: "delete-button", title: "Delete Entity", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });
            }

            if (this.TaskRelationType != undefined) {
                this.tools.push({ name: "task-button", title: "Create Task", icon: "icon-entypo-fix-clipboard std-entypo-fix-icon", text: "Create Task", permission: "AddEntity" });
            }

            if (this.model.attributes.EntityType == "Resource") {
                this.tools.push({ name: "download-button", title: "Download resource", icon: "fa-download fa", text: "Download" });
            }

            if (_.find(appData.enrichPreviewTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, that.model.get("EntityType"));
            })) {
                this.tools.push({ name: "preview-button", title: "Preview", icon: "fa-eye fa", text: "Preview" });
            }

            if (_.find(appData.enrichPDFTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, that.model.get("EntityType"));
            })) {
                this.tools.push({ name: "pdf-button", title: "PDF", icon: "fa-file-pdf-o fa", text: "PDF" });
            }

            this.tools.push({ name: "copy-button", title: "Copy Entity", icon: "fa fa-files-o", text: "Copy", permission: "CopyEntity" });

            if (this.parent != null && !this.parent.isQuery) {
                this.tools.push({ name: "remove-button", title: "Remove Entity from workarea", icon: "fa-close fa", text: "Remove from workarea", permission: "DeleteLink" });
            }

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.model.id,
                tools: this.tools
            }).$el);

        },
        render: function () {
            var self = this;

            if (this.model == null) {
                return this;
            }

            if (this.taskRelations != null) {
                this.TaskRelationType = _.find(this.taskRelations.models, function (model) {
                    return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                });
            }

            var icon = this.model.attributes.EntityType;
            if (icon == "Channel" && !this.model.attributes.ChannelPublished) {
                icon = "ChannelUnpublished";
            }


            if (this.type == "large") {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName, enableAddTask: this.TaskRelationType != undefined, icon: icon }));
            } else {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName, enableAddTask: this.TaskRelationType != undefined, icon: icon }));
            }

            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);


            if (!this.options.hideContextMenu) {
                this.renderContextMenu();
            }

            this.drawCompleteness(this.model.attributes.Completeness);

            window.appHelper.event_bus.trigger('cardrendered', this.model.id);

            this.setChecked(this.checked);

            return this; // enable chained calls
        }
    });

    return entitycardView;
});
