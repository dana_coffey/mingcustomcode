﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/field/fieldModel',
  'text!sharedtemplates/lightboard/channelEntityTemplate.html'

], function ($, _, Backbone, fieldModel, channelTemplate) {

    var lightboardPreviewChannelListView = Backbone.View.extend({
        initialize: function (options) {
            this.collection = options.collection;
            this.container = options.container; 

            this.render();

        },
        events: {
            "click .channel-entity-card-wrap": "onDetailsClick"
        },
        onDetailsClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            window.appHelper.event_bus.trigger('closepopup');
            this.goTo("entity/" + e.currentTarget.id.replace("channel-", ""));

        },
        render: function () {
            var self = this;
            var i = 0;
            this.$el.html("");

            _.each(self.collection.models, function (channel) {
                if (channel.attributes.parent == "#") {
                    self.$el.append(_.template(channelTemplate, { data: channel.toJSON(), Published: channel.attributes.published, TypeName: channel.attributes.channelType }));
                }
               
                i++;
            });

            return this; 
        }
    });

    return lightboardPreviewChannelListView;
});
