﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/connector/mappingBasedConnectorSettingListTemplate.html'
], function($, _, backbone, inRiverUtil, mappingBasedConnectorSettingListTemplate) {

    var mappingBasedConnectorSettingView = backbone.View.extend({
        initialize: function() {
            this.render();
        },
        events: {
        },
        waitForFrameToLoad: function (that) {
            setTimeout(function () {
                var frame = document.getElementById('mapping-based-frame');
                var state = frame.contentWindow.document.readyState;
                if (state == 'complete') {
                    frame.contentWindow.editConnector(that.model.get("Id"));
                } else {
                    that.waitForFrameToLoad(that);
                }
            }, 500);
        },
        render: function() {
            this.$el.html(mappingBasedConnectorSettingListTemplate);
            this.waitForFrameToLoad(this);
            return this;
        }
    });

    return mappingBasedConnectorSettingView;
});