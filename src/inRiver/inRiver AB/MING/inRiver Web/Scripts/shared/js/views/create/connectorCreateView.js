﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/connector/connectorTypeCollection',
  'sharedjs/models/connector/connectorModel',
  'text!templates/connector/connectorCreateTemplate.html',
  'text!templates/connector/connectorTypeTemplate.html'
], function ($, _, backbone, modalPopup, inRiverUtil, connectorTypeCollection, connectorModel, connectorCreateTemplate, connectorTypeTemplate) {

    var connectorCreateView = backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            var self = this;
            this.id = options.id;
            this.inbounds = options.inbounds;
            this.outbounds = options.outbounds;
            this.startPage = options.startPage;

            this.collection = new connectorTypeCollection();
            this.collection.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ inbounds: this.inbounds, outbounds: this.outbounds })
            });
        },
        events: {
            "click #create-connector": "createConnector",
            "click .connector-card-wrap": "activateType"
        },
        activateType: function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(e.currentTarget).addClass('active').siblings().removeClass('active');
        },
        createConnector: function (e) {
            var self = this;
            e.stopPropagation();
            e.preventDefault();

            var name = $("#connector-name").val();
            if (name == "") {
                inRiverUtil.NotifyError("Name missing","Connector name is mandatory");
                return;
            }

            var r = new RegExp("^[a-zA-Z0-9]*$");

            if (!r.test(name)) {
                inRiverUtil.NotifyError("Invalid Connector name", "Connector name can only contain alpha numerical characters.");
                return;
            }

            var type = $("#create-connector-types .active").attr('id');
            if (type == null) {
                inRiverUtil.NotifyError("Connector missing", "No connector selected");
                return;
            }
            this.model = new connectorModel();
            this.model.url = "/api/connector/?id=" + name + "&type=" + type + "&inbounds=" + this.inbounds;
            this.connectorName = name;
            this.model.save({}, {
                success: function() {
                    window.appHelper.event_bus.trigger('connectoradd');
                    if (self.startPage) {
                        self.goTo("home");
                    }
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(connectorCreateTemplate));

            var container = this.$el.find("#create-connector-types");
            if (this.collection.length == 0) {
                container.html("<div class='no-items-text'>There are no connectors to show, or the 'inRiver Connect' service is not running.</div>");
            } else {
                _.each(that.collection.models, function (model) {
                    var subsectionEl = $(_.template(connectorTypeTemplate, { data: model.toJSON() }));
                    container.append(subsectionEl.html());
                });
            }

            return that;
        }
    });

return connectorCreateView;
});