define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var SettingModel = Backbone.Model.extend({
        idAttribute: "Setting",
        urlRoot: '/api/setting',
        initialize: function () {

        },
        schema: { // Used by backbone forms extension
            Setting: { type: 'Text', validators: ['required'], editorAttrs: { disabled: true } },
            Value: { type: 'TextArea', title: 'Value' },
        }
    });

    return SettingModel;

});
