﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var connectorModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/connector/start',
        
    });

    return connectorModel;
});
