﻿define([
    'jquery',
    'underscore',
    'backbone',
    'alertify',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/collections/linkrule/linkRuleCollection',
    'sharedjs/collections/relations/relationTypeCollection',
    'sharedjs/collections/workareas/workareaCollection',
    'sharedjs/models/linkrule/linkRuleModel',
    'sharedjs/models/query/queryModel',
    'sharedjs/models/workarea/workareaFolderModel',
    'sharedjs/models/workarea/workareaModel',
    'sharedjs/views/advancedsearch/queryEditorView',
    'sharedjs/views/configureLinkRule/configureLinkRuleRowView',
    'sharedjs/views/configureLinkRule/configureLinkRuleQueryRowView',
    'text!sharedtemplates/configurelinkrule/configureLinkRuleTemplate.html'
], function ($, _, backbone, alertify, modalPopup, inRiverUtil, linkRuleCollection, relationTypeCollection, workareaCollection, linkRuleModel, queryModel, workareaFolderModel, workareaModel, queryEditorView, configureLinkRuleRowView, configureLinkRuleQueryRowView, configureLinkRuleTemplate) {

    var configureLinkRuleView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.id = options.model.id;

            if (options.direction) {
                this.direction = options.direction;
            } else {
                this.direction = "outbound";
            }

            this.targetEntityTypeId = options.targetEntityTypeId;
            this.selectedLinkTypeId = options.linkTypeId;

            this.relationsLoaded = false;
            this.rulesLoaded = false;
            this.linkType = undefined;

            this.rules = [];
            this.queries = [];

            this.relationTypes = new relationTypeCollection([], { entityTypeId: options.model.attributes.EntityType, direction: this.direction });
            this.relationTypes.fetch({
                success: function () {
                    self.relationsLoaded = true;
                    self.preRender();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            var url;

            if (this.selectedLinkTypeId) {
                url = '/api/linkrule/' + self.id + '/' + self.selectedLinkTypeId;
            } else {
                url = '/api/linkrule/' + self.id;
            }

            this.collection = new linkRuleCollection({ entityid: this.id });
            this.collection.fetch({
                success: function () {
                    self.rulesLoaded = true;
                    self.preRender();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                url: url
            });

            window.appHelper.event_bus.off('get-existing-queries');
            this.listenTo(window.appHelper.event_bus, 'get-existing-queries', this.test);
        },
        events: {
            "change #linktype": "onLinkTypeSelect",
            "click #AddRules": "onAddRule",
            "click #SaveDefinition": "onSaveDefinition",
            "click #DeleteDefinition": "onDeleteDefinition",
            "click #AddNewQuery": "onAddNewQuery",
            "click #UseExistingQuery": "onUseExistingQuery",
            "click #saveNewQuery": "onSaveQuery",
            "click #closeConfigureQuery": "onCloseQuery",
            "click #saveConfigureQuery": "onsaveConfigureQuery",
        },
        preRender: function () {
            if (this.relationsLoaded && this.rulesLoaded) {
                this.render();
            }
        },
        onAddRule: function () {
            if (this.entityType) {
                var configRow = new configureLinkRuleRowView({
                    entityType: this.entityType
                });
                this.$el.find("#rulearea").append(configRow.$el);

                this.rules.push(configRow);
            }
        },
        onLinkTypeSelect: function (e) {
            e.stopPropagation();
            var self = this;

            this.linkType = this.$el.find("#linktype").val();
            var linkTypeModel = _.find(this.relationTypes.models, function (num) {
                return num.attributes.Id == self.linkType;
            });

            if (linkTypeModel) {
                this.entityType = linkTypeModel.attributes.TargetEntityTypeId;
            }

            //Destroy all rules.
            _.each(this.rules, function (rule) {
                rule.remove();
            });

            this.rules = [];
        },
        onSaveDefinition: function () {
            if (this.linkType == undefined) {

                inRiverUtil.NotifyError("Validation error", "No link type selected");
                return;
            }

            var foundRuleBroken = false;
            $.each(this.rules, function (index, value) {
                value.deselect();
                if (!value.valueIsSet()) {
                    inRiverUtil.NotifyError("No value for rule", "A value must be set for rule " + value.ruleSystemLinkText());
                    foundRuleBroken = true;
                    return;
                }
            });

            if (foundRuleBroken)
                return;


            var operator = $("#operator").val();

            var self = this;
            if (this.linkrulemodel == undefined) {
                this.linkrulemodel = new linkRuleModel({ entityid: this.id, linktypeid: this.linkType, operator: operator, rules: [], queries: [] });
            } else {
                this.linkrulemodel.attributes.rules = [];
                this.linkrulemodel.attributes.queries = [];
            }

            $.each(this.rules, function (index, value) {
                var rule = value.toObject();
                if (rule != undefined) {
                    self.linkrulemodel.attributes.rules.push(rule);
                }
            });

            $.each(this.queries, function (index, value) {
                var query = value.toObject();
                if (query != undefined) {
                    self.linkrulemodel.attributes.queries.push(query);
                }
            });

            this.linkrulemodel.save([], {
                success: function () {

                    self.$el.find("#linktypenoedit").text(self.entityType);
                    self.$el.find("#linktypenoedit").show();
                    self.$el.find("#linktype").hide();

                    self.$el.find("#operatornoedit").text(self.linkrulemodel.get("operator"));
                    self.$el.find("#operatornoedit").show();
                    self.$el.find("#operator").hide();

                    inRiverUtil.Notify("Link rule saved.");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                url: '/api/linkrule/' + self.id + '/' + self.selectedLinkTypeId
            });
        },
        onDeleteDefinition: function () {
            var self = this;
            this.linkrulemodel.destroy({
                success: function () {
                    self.linkrulemodel = undefined;
                    self.linkType = undefined;
                    self.rules = [];
                    self.queries = [];
                    self.rulesLoaded = false;
                    self.collection.fetch({
                        success: function () {
                            self.rulesLoaded = true;
                            self.preRender();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                url: '/api/linkrule/' + self.id + '/' + self.linkType
            });
        },
        onAddNewQuery: function (e) {
            e.stopPropagation();
            this.emptyQueryEditorViewContainer();

            var predefinedEntityType = this.$el.find("#linktypenoedit").text();

            if (predefinedEntityType == undefined || predefinedEntityType == "") {
                this.queryViewHtml = new queryEditorView({});
            } else {
                this.queryViewHtml = new queryEditorView({ predefinedEntityType: predefinedEntityType });
            }
            this.queryViewHtml.$el.find(".popup-header-row").hide();
            this.queryViewHtml.$el.find("#search").hide();
            this.queryViewHtml.$el.find("#notifymewhen").hide();
            $(this.queryViewHtml.$el.find("#save")).attr('id', 'saveNewQuery');

            this.$el.find("#queryEditorViewContainer").append(this.queryViewHtml.$el);
            this.$el.find("#querycontainer .querytitle").text("New Query");

            this.$el.find("#querycontainer").show();

        },
        onSaveQuery: function () {
            var self = this;

            if (self.queryViewHtml.rows.length === 0) {
                return;
            }

            if (!self.queryViewHtml.validateForm()) {
                return;
            }

            if (this.workareas == undefined) {
                this.workareas = new workareaCollection();
                this.workareas.fetch({
                    success: function () {

                        self.queryViewHtml.renderSaveTree(self.workareas);
                        self.queryViewHtml.showSaveDialog();

                        $(self.queryViewHtml.$el.find("#saveQuery")).attr('id', 'saveConfigureQuery');

                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                self.queryViewHtml.renderSaveTree(self.workareas);
                self.queryViewHtml.showSaveDialog();

                $(self.queryViewHtml.$el.find("#saveQuery")).attr('id', 'saveConfigureQuery');
            }
        },
        onsaveConfigureQuery: function () {
            var self = this;

            var selectedNode = self.queryViewHtml.$el.find("#work-area-selection").jstree(true).get_selected(false);
            var name = self.queryViewHtml.$el.find("#queryname").val();

            if (selectedNode.length < 1 || (name == null || name == "")) {
                inRiverUtil.NotifyError("Missing requirements.", "Name and node must be selected");
                return;
            }

            this.queryModelType = $(selectedNode)[0].substring(0, 1);

            var querymodel = new queryModel();
            querymodel.attributes["Query"] = self.queryViewHtml.getQuery();
            querymodel.attributes["Name"] = name;
            querymodel.attributes["Text"] = name;

            querymodel.attributes["ParentId"] = selectedNode[0];
            querymodel.attributes["Username"] = window.appHelper.userName;

            querymodel.save(null, {
                success: function (model, response) {

                    var workAreaId;

                    if (self.queryModelType == "s") {
                        workAreaId = "s_" + response.Id;
                    } else {
                        workAreaId = "p_" + response.Id;
                    }

                    var workareaFolder = new workareaFolderModel();
                    workareaFolder.url = "/api/workarea/" + workAreaId;
                    workareaFolder.fetch({
                        success: function (folder) {

                            var folderId;

                            if (folder.attributes.type == "Shared") {
                                folderId = "s_" + folder.id;
                            } else {
                                folderId = "p_" + folder.id;
                            }

                            var configRow = new configureLinkRuleQueryRowView({
                                workAreaId: folderId,
                                workAreaName: folder.attributes.text
                            });

                            self.$el.find("#queryarea").append(configRow.$el);
                            self.queries.push(configRow);
                            self.onCloseQuery();
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.emptyQueryEditorViewContainer();
            this.$el.find("#querycontainer").hide();
        },
        onCloseQuery: function () {
            this.emptyQueryEditorViewContainer();
            this.$el.find("#querycontainer").hide();
            this.$el.find("#queryFilterContainer").hide();
        },
        test: function (workareaId) {
            var self = this;

            _.each(self.queries, function (query) {
                if (query.queryWorkAreaId == workareaId) {
                    for (var i = self.queries.length; i--;) {
                        if (self.queries[i] === query) {
                            self.queries.splice(i, 1);
                        }
                    }
                }
            });

            var style = self.$el.find("#querycontainer").css("display");

            if (style === 'block') {

                self.emptyQueryEditorViewContainer();
                self.$el.find("#querycontainer").hide();
                self.$el.find("#queryFilterContainer").hide();

                self.renderExistingQueryHtml();
                self.$el.find("#querycontainer .querytitle").text("Existing Query");
                self.$el.find("#queryFilterContainer").show();
                self.$el.find("#querycontainer").show();
            }
        },
        onUseExistingQuery: function () {

            this.emptyQueryEditorViewContainer();

            this.renderExistingQueryHtml();
            this.$el.find("#querycontainer .querytitle").text("Existing Query");
            this.$el.find("#queryFilterContainer").show();
            this.$el.find("#querycontainer").show();

        },
        emptyQueryEditorViewContainer: function () {
            this.$el.find("#queryEditorViewContainer").empty();
            this.$el.find("#queryEditorViewContainer").replaceWith(function () {
                return $('<div id="queryEditorViewContainer">').append();
            });
        },
        renderExistingQueryHtml: function () {
            var self = this;
            this.workareas = new workareaCollection();
            this.workareas.fetch({
                success: function () {

                    self.$el.find("#querycontainer").show();
                    self.filterOutNonSearchQueries();
                    self.renderExistingQueryJsTree();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        filterOutNonSearchQueries: function () {
            var self = this;
            var searchQueries = self.workareas;

            self.workareas = new workareaCollection();

            var queryRows = this.$el.find(".query-row");


            _.each(searchQueries.models, function (query) {
                if (query.attributes.isquery) {
                    query.attributes.parent = "#";

                    if (self.containsWorkareaId(self.queries, query.id).length == 0) {
                        if (self.containsId(queryRows, query.id).length == 0) {
                            if (query.id.substring(0, 1) != 'p') {
                                self.workareas.push(query);
                            }
                        }
                    }
                }
            });
        },
        containsWorkareaId: function (arr, id) {
            var matches = _.filter(arr, function (value) {
                if (value.queryWorkAreaId == id) {
                    return value;
                }
            });

            return matches;
        },
        containsId: function (arr, id) {
            var matches = _.filter(arr, function (value) {
                if (value.id == id) {
                    return value;
                }
            });

            return matches;
        },
        renderExistingQueryJsTree: function () {
            var self = this;

            this.$el.find("#queryEditorViewContainer").jstree({
                core: {
                    data: self.workareas.toJSON(),
                    animation: "100",
                    themes: { dots: false }
                },
                "plugins": [
                    "types", "wholerow", "search"
                ],
                "types": {
                    "#": {
                        "valid_children": ["query"]
                    },
                    "query": {
                        "icon": "/Images/folderquery.png"
                    }
                },
                "search": {
                    "show_only_matches": true
                }

            });

            var to = false;
            this.$el.find('#treefilter').keyup(function () {
                if (to) {
                    clearTimeout(to);
                }
                to = setTimeout(function () {
                    var v = self.$el.find('#treefilter').val();
                    self.$el.find("#queryEditorViewContainer").jstree(true).search(v);
                }, 250);
            });

            this.$el.find("#queryEditorViewContainer").on("select_node.jstree",
                function (evt, data) {

                    if (data.node.id == "s_00000000-0000-0000-0000-000000000000") {
                        return;
                    }

                    if (data.node.id == "p_00000000-0000-0000-0000-000000000000") {
                        return;
                    }

                    var workareaFolder = new workareaFolderModel();

                    workareaFolder.url = "/api/workarea/" + data.node.id;
                    workareaFolder.fetch({
                        success: function (folder, response) {

                            var folderId;

                            if (response.type == "Shared") {
                                folderId = "s_" + response.id;
                            } else {
                                folderId = "p_" + response.id;
                            }

                            var configRow = new configureLinkRuleQueryRowView({
                                workAreaId: folderId,
                                workAreaName: folder.attributes.text
                            });

                            self.$el.find("#queryarea").append(configRow.$el);
                            self.queries.push(configRow);
                            self.onCloseQuery();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
            );

            this.$el.find("#queryEditorViewContainer").on("loaded.jstree",
                function (evt, data) {

                    var depth = 2;
                    data.instance.get_container().find('li').each(function (i) {
                        if (data.instance.get_path($(this)).length <= depth) {
                            data.instance.open_node($(this));
                        }
                    });
                }
            );
        },
        render: function () {
            var self = this;

            this.$el.html(_.template(configureLinkRuleTemplate, {}));

            if (!this.selectedLinkTypeId) {

                var select = this.$el.find("#linktype");
                _.each(this.relationTypes.models, function (type) {

                    if (type.attributes.TargetEntityTypeId == "ChannelNode" || type.attributes.TargetEntityTypeId == "Section") {
                        return;
                    }
                    select.append("<option value=\"" + type.attributes.Id + "\">" + type.attributes.Name + "</option>");
                });
                this.$el.find("#linktypenoedit").hide();

                this.$el.find("#linktype").show();
            } else {

                self.$el.find("#linktypenoedit").text(this.targetEntityTypeId);
                self.$el.find("#linktypenoedit").show();
                self.$el.find("#linktype").hide();

                var linkType = _.find(this.relationTypes.models, function (num) {
                    return num.attributes.Id == self.selectedLinkTypeId;
                });

                if (linkType) {
                    this.entityType = linkType.attributes.TargetEntityTypeId;
                    this.linkType = self.selectedLinkTypeId;
                }
            }

            if (this.collection.models.length > 0) {

                this.linkrulemodel = this.collection.models[0];

                this.linkType = this.linkrulemodel.get("linktypeid");

                this.$el.find("#operator").val(this.linkrulemodel.get("operator"));

                if (!self.selectedLinkTypeId) {
                    //Denna ska fångas om vi är inne i plan and release.
                    select.val(this.linkrulemodel.attributes.linktypeid);

                    var linkTypeModel = _.find(this.relationTypes.models, function (num) {
                        return num.attributes.Id == self.linkrulemodel.attributes.linktypeid;
                    });

                    if (linkTypeModel) {
                        this.entityType = linkTypeModel.attributes.TargetEntityTypeId;
                    }

                    this.$el.find("#linktypenoedit").text(this.entityType);
                } else {
                    this.$el.find("#linktypenoedit").text(self.targetEntityTypeId);
                }

                this.$el.find("#linktypenoedit").show();
                this.$el.find("#linktype").hide();

                this.$el.find("#operatornoedit").text(this.linkrulemodel.get("operator"));
                this.$el.find("#operatornoedit").show();
                this.$el.find("#operator").hide();

                //Adda rules....

                $.each(this.linkrulemodel.attributes.rules, function (index, value) {

                    var configRow = new configureLinkRuleRowView({
                        entityType: self.entityType,
                        queryValue: value.value,
                        querySystem: value.fieldtypeid,
                        queryOperator: value.operator
                    });
                    self.$el.find("#rulearea").append(configRow.$el);

                    self.rules.push(configRow);

                });

                $.each(this.linkrulemodel.attributes.queries, function (index, value) {

                    //Get workarea with workAreaId

                    this.workareaModel = new workareaModel();
                    this.workareaModel.url = "/api/workarea/" + value.workareaid;
                    this.workareaModel.fetch({
                        success: function (model, response) {

                            var folderId;

                            if (response.type == "Shared") {
                                folderId = "s_" + response.id;
                            } else {
                                folderId = "p_" + response.id;
                            }

                            var configRow = new configureLinkRuleQueryRowView({
                                workAreaId: folderId,
                                workAreaName: response.text
                            });

                            self.$el.find("#queryarea").append(configRow.$el);
                            self.queries.push(configRow);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }

                    });
                });
            }
            return this;
        }
    });

    return configureLinkRuleView;
});
