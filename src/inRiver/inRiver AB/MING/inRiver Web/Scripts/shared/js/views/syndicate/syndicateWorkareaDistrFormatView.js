﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/collections/syndicate/distributionFormatCollection',
    'sharedjs/views/syndicate/syndicateSelectionControlView',
    'text!sharedtemplates/syndicate/syndicateWorkareaDistrFormatTemplate.html'
], function ($, _, backbone, inRiverUtil, distributionFormatCollection, syndicateSelectionControlView, syndicateWorkareaDistrFormatTemplate) {
    var syndicateWorkareaDistrFormatView = backbone.View.extend({
        template: _.template(syndicateWorkareaDistrFormatTemplate),
        initialize: function (options) {
            this.readyToSyndicate = false;
            this.controlId = options.controlId;
            this.receivers = options.receivers;
            this.distributionFormats = new distributionFormatCollection();
            this.render();
        },
        changeReceiverSelection: function (e) {
            e.stopPropagation();

            var self = this;
            var selectedReceiver = this.getSelectedRecevier();
            var controlDefinition = "distribution-format";
            if (selectedReceiver === '0') {
                this.getContainer().hide();
                this.readyToSyndicate = false;
                return;
            }

            this.getContainer(controlDefinition).empty();
            this.distributionFormats.fetch({
                url: "/api/syndication/distributionFormat/byReceiver/" + selectedReceiver,
                reset: true,
                success: function() {
                    self.createFormatSelectionControl(self.distributionFormats);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        changeFormatSelection: function(e) {
            e.stopPropagation();

            var selectedFormat = this.getSelectedDistributionFormat();
            if (selectedFormat > 0) {
                this.readyToSyndicate = true;
            } else {
                this.readyToSyndicate = false;
            }
        },
        getValueControl: function (controlDefinition) {
            return this.$el.find("#syndicate-select-value-" + controlDefinition + "-" + this.controlId);
        },
        getValue: function (controlDefinition) {
            var value = this.getValueControl(controlDefinition).val();
            return value ? value : [];
        },
        getSelectedRecevier: function () {
            var result = this.getValue("receiver");
            return result.length > 0 ? result[0] : "(none)";
        },
        getSelectedDistributionFormat: function () {
            var result = this.getValue("distribution-format");
            return result.length > 0 ? result[0] : "(none)";
        },
        getContainer: function (controlDefinition) {
            return this.$el.find("#syndicate-workarea-container-" + controlDefinition + "-" + this.controlId);
        },
        createReceiverSelectionControl: function (selectedValues) {
            var self = this;
            this.createSelectionControl("receiver", "Receiver:", false, this.receivers, selectedValues);
            this.getValueControl("receiver").change(function (e) { self.changeReceiverSelection(e) });
        },
        createFormatSelectionControl: function (collection) {
            var self = this;
            this.createSelectionControl("distribution-format", "Format:", false, collection, null);
            this.getValueControl("distribution-format").change(function (e) { self.changeFormatSelection(e) });
        },
        createSelectionControl: function (controlDefinition, caption, multiValue, values, selectedValues) {
            var selectionControlView = new syndicateSelectionControlView({
                Id: this.controlId,
                ControlDefinition: controlDefinition,
                Caption: caption,
                Values: values,
                Multivalue: multiValue,
                PossibleValues: selectedValues
            });
            this.getContainer(controlDefinition).html(selectionControlView.$el);
            this.getContainer(controlDefinition).show();
        },
        render: function () {
            var elements = this.template({ Definition: this.controlId });
            this.setElement(elements, true);
            this.createReceiverSelectionControl();
            return this;
        }
    });

    return syndicateWorkareaDistrFormatView;
});