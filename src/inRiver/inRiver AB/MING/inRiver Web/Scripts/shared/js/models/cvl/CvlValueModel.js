define([
  'underscore',
  'backbone'
], function (_, Backbone, LocalStringEditor) {

    var CvlValueModel = Backbone.Model.extend({
        idAttribute: "Key",
        urlRoot: function () {
            return '/api/cvlvalue/' + this.get("CVLId");

        },
        initialize: function(options){
            if (options && options.Value) {
                this.Value = options.Value; 
            }
        },
        schema: {
            // Used by backbone forms extension
            Id: { type: 'Hidden' },
            CVLId: { type: 'Hidden' },
            Keys: { type: 'Select', title: 'Keys', options: [] },
            Key: { type: 'Text', title: 'Key', editorAttrs: { disabled: true }, validators: ['required', { type: 'regexp', message: 'Value must not contain white space', regexp: /^\S*$/ }] },
            ParentKey: { type: 'Select', title: 'Parent Key', options: [] },
            Languages: { type: 'Select', title: 'Language', options: [] },
            Value: { type: 'Text', title: 'String Value' },
            LSValue: { type: 'Text', title: 'String Value' },
            Index: { type: 'Text', title: 'Index' }
        },
        toString: function () {
            return this.get("DisplayValue");
        },
    });

    return CvlValueModel;

});
