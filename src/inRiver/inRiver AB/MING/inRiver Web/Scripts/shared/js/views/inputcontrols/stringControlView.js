﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'text!sharedtemplates/inputcontrols/stringControlTemplate.html'
], function ($, _, backbone, alertify, stringControlTemplate) {

    var stringControlView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;
            if (options.Value == null) {
                this.fieldValue = "";
                this.defaultValue = "";
            } else {
                this.fieldValue = options.Value;
                this.defaultValue = _.clone(options.Value);
            }
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.focusField = this.inputField;
            this.entityId = options.EntityId;
            this.okToSave = false;
            this.saveButtonPressed = false;
            if (options.RegExp) {
                if (options.RegExp && options.RegExp.indexOf("^") < 0) {
                    options.RegExp = "^" + options.RegExp;
                }
                if (options.RegExp && options.RegExp.indexOf("$") < 0) {
                    options.RegExp = options.RegExp + "$";
                }
                this.regExp = new RegExp(options.RegExp);
            }

        },
        events: function () {
            var theEvents = {};
            theEvents["blur " + "#value-editing-" + this.fieldTypeId] = "onLostFocus";
            theEvents["keyup " + "#value-editing-" + this.fieldTypeId] = "onKeyUp";
            theEvents["click " + "#save-field-value-" + this.fieldTypeId] = "onSave";
            theEvents["click " + "#undo-field-value-" + this.fieldTypeId] = "onUndo";

            theEvents["keydown " + "#value-editing-" + this.fieldTypeId] = "onKeyDown";
            return theEvents;
        },
        onUndo: function () {
            if (this.saveButtonPressed) {
                return;
            }

            this.$el.find(this.inputField).val(this.defaultValue);
            this.fieldValue = this.defaultValue;
            this.$el.find(this.saveButton).removeClass('active');
            this.$el.find(this.undoButton).removeClass('active');
            this.okToSave = false;
            this.$el.remove();
            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo(e);
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            if (e.keyCode == 13) {
                this.onSave(e);
            }

            if (e.keyCode >= 48 && e.keyCode <= 105 || e.keyCode == 8) {
                window.appHelper.event_bus.trigger('fieldvalueupdated', this.fieldTypeId, this.fieldValue);
            }
        },
        onSave: function (e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);
            }
        },
        onKeyUp: function (e) {
            e.stopPropagation();

            if ( this.regExp) {
                this.checkRegExp(); 
            }
            this.fieldValue = this.$el.find(this.inputField).val();
            if (this.fieldValue != this.defaultValue) {
                this.$el.find(this.saveButton).addClass('active');
                this.$el.find(this.undoButton).addClass('active');

                this.okToSave = true;
            } else {
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');

                this.okToSave = false;
            }
        },
        checkRegExp: function () {
            
            this.fieldValue = this.$el.find(this.inputField).val();
            if (this.isOk(this.fieldValue) || this.fieldValue == this.defaultValue) {
                this.$el.parent().removeClass("error");
                this.$('[data-error]').empty();
                if (this.fieldValue != this.defaultValue) {
                    this.$el.find(this.saveButton).addClass('active');
                    this.$el.find(this.undoButton).addClass('active');

                    this.okToSave = true;
                } else {
                    this.$el.find(this.saveButton).removeClass('active');
                    this.$el.find(this.undoButton).removeClass('active');

                    this.okToSave = false;
                }
                this.$el.find(this.inputField).attr('title', '');
                this.validValue = true;
            } else {
                this.$el.parent().addClass("error");
                this.$('[data-error]').html("Value is not formated correct");
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.inputField).attr('title', 'Value is not formated correct');
                this.validValue = false;
                this.okToSave = false;
            }
        },
        isOk:function (test) {

            return  test.match(this.regExp);

            // return !$.isArray( test ) && (test - parseFloat( test ) + 1) >= 0;
        },
        onLostFocus: function (e) {
            var that = this;
            e.stopPropagation();
            
            // Use timeout to delay examination of historypopup until after blur/focus 
            // events have been processed.
            setTimeout(function () {
                if (!window.appSession.historyset) {
                    if (that.defaultValue == that.fieldValue) {
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                    }
                }
            },80);
        },
        render: function () {
            var template = _.template(stringControlTemplate, { Id: this.fieldTypeId, Value: this.fieldValue });
            this.setElement(template, true);
            this.$el.find(this.inputField).attr('size', this.$el.find(this.inputField).val().length + 10);
            return this;
        },
    });

    return stringControlView;
});

