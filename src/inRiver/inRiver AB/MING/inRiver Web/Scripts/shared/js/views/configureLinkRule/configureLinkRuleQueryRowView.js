﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/advancedSearchUtil',
  'sharedjs/collections/entityTypes/entityTypeCollection',
  'sharedjs/collections/channel/channelCollection',
  'sharedjs/collections/cvl/cvlValueCollection',
  'sharedjs/collections/fieldsets/fieldsetCollection',
  'sharedjs/collections/fieldtypes/FieldTypesCollection',
  'sharedjs/collections/languages/LanguagesCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'text!sharedtemplates/configurelinkrule/configureLinkRuleQueryRowTemplate.html'

], function ($, _, Backbone, inRiverUtil, advancedSearchUtil, entityTypeCollection, channelCollection, cvlValueCollection, fieldsetCollection, fieldTypesCollection, languagesCollection, relationTypeCollection, configureLinkRuleQueryRowTemplate) {
    var configureLinkRuleQueryRowView = Backbone.View.extend({
        initialize: function (options) {
            this.queryWorkAreaId = options.workAreaId;
            this.queryWorkAreaName = options.workAreaName;

            this.render();
        },
        toObject: function () {
            if (this.deleted) {
                return null;
            }

            var row = { ruledefinitionid: 0, workareaid: this.queryWorkAreaId, workareaname: this.queryWorkAreaName };

            return row;
        },
        events: {
            "click #query-remove-row": "onDelete"

        },
        onDelete: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.deleted = true;
            this.remove();
            this.unbind();
            window.appHelper.event_bus.trigger('get-existing-queries', this.queryWorkAreaId);
        },
        render: function () {
            this.$el.html(_.template(configureLinkRuleQueryRowTemplate, { rowId: this.rowId, name: this.queryWorkAreaName }));

            return this;
        }
    });

    return configureLinkRuleQueryRowView;
});
