﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'dropzone',
  'sharedjs/collections/resources/resourceCollection',
  'sharedjs/collections/imageconfiguration/imageConfigurationCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'modalPopup',
  'sharedjs/views/lightboard/lightboardPreview',
  'sharedjs/views/lightboard/lightboardCardView',
  'sharedjs/models/resource/resourceModel',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/usersetting/userSettingModel',
  'text!sharedtemplates/lightboard/lightboardTemplate.html',
  'text!sharedtemplates/lightboard/lightboardFieldTemplate.html',
  'text!sharedtemplates/dropzone/dropzoneTemplate.html',
  'sharedjs/models/fieldtype/FieldTypeSettingModel'
], function ($, _, Backbone, inRiverUtil, Dropzone, resourceCollection, imageConfigurationCollection, relationTypeCollection, modalPopup, lightboardPreview, lightboardCardView, resourceModel, fieldModel, userSettingModel, lightboardTemplate, lightboardFieldTemplate, dropzoneTemplate, fieldTypeSettingModel) {

    var lightboardView = Backbone.View.extend({

        initialize: function (options) {
            this.fieldTypeSetting = null;
            this.isSubEntities = false;
            this.entityType = options.type;

            var that = this;
            var onDataHandler = function(res, value) {
                that.isSubEntities = value == "True";
                that.getCollection();
            };

            var key = "ShowRelationResources";
            var settingModel = new userSettingModel(key);
            settingModel.url = "/api/usersetting/" + key;
            settingModel.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            if (window.appHelper.imageConfigurationsCollection == null) {

                window.appHelper.imageConfigurationsCollection = new imageConfigurationCollection();
                window.appHelper.imageConfigurationsCollection.fetch({async:false});

            }
        },
        getCollection: function () {
            var self = this;
            this.collection = new resourceCollection([], { Id: this.id, subEntities: this.isSubEntities });
            this.collection.fetch({
                success: function (m) {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #show-all": "onShowAll",
            //"click .view-details": "onDetailsClick",
            //"click .set-main-picture": "setMainPicture",
            //"click .download-button": "onDownload",
            "click #upload-button": "onUpload",
            "click .lightboard-card-wrap": "onPreview"
        },
        onPreview: function (e) {
          
            if (e.currentTarget.id == "dropzone-container") {
                return;
            }
            e.stopPropagation();
            e.preventDefault();

            var that = this;
           this.modal = new modalPopup();

            this.modal.popOut(new lightboardPreview({
                id: e.currentTarget.id, collection: this.collection, parentEntityId: this.id
            }), {size: "preview", header: "Preview"});
           

        },
        closePopup: function () {
            this.modal.popIn();
        },
        entityupdated: function (id) {
            if (id == this.id) {
                this.getCollection();
            }
        },
        onUpload: function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("#my-dropzone #resourceId").val(e.currentTarget.id);
            $("#my-dropzone #type").val("ReplaceResource");
            this.myAwesomeDropzone.options.maxFiles = 1;
            $(".dropzone-button").trigger("click");
            //this.openDropzoneModal();
        },
        onDownload: function (e) {
            e.stopPropagation();
            e.preventDefault();

            $.get("/api/tools/hasimageextension/" + e.currentTarget.id, function (hasImageExtension) {
                if (!hasImageExtension) {
                    $(".thumbnails ul#" + e.currentTarget.id + " li.Preview").hide();
                    $(".thumbnails ul#" + e.currentTarget.id + " li.Thumbnail").hide();
                    $(".thumbnails ul#" + e.currentTarget.id + " li.SmallThumbnail").hide();
                }
            });

            //$(".download-menu").menu();
            $(e.currentTarget).parent().parent().next().menu();

            var menu = $(e.currentTarget).parent().parent().next().show().position({
                my: "left top",
                at: "left bottom",
                of: e.currentTarget
            });

            $(document).one("click", function () {
                menu.hide();
            });
        },
        onDetailsClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.goTo("entity/" + e.currentTarget.id);
        },
        onShowAll: function (e) {
            e.stopPropagation();
            this.isSubEntities = $("#show-all").is(':checked');

            var settingModel = new userSettingModel();
            settingModel.save({
                ShowRelationResources: this.isSubEntities
            });

            this.getCollection();
        },
        setMainPicture: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var that = this;

            $.get("/api/tools/setmainpicture/" + this.id + "/" + e.currentTarget.id, function (result) {
                appHelper.event_bus.trigger('entityupdated', that.id);
            });

            this.getCollection();
        },
        onFieldsMetaData: function (thisModel) {
            var metaData = $('<div><h4>Metadata</h4></div>');
            var i = 0;
            _.each(thisModel.get("Fields"), function (f) {
                if (!f.IsHidden && (!f.ExcludeFromDefaultView || (f.ExcludeFromDefaultView && f.FieldSets.split(',').indexOf(thisModel.get("FieldSet"))))) {
                    var historyId = null;
                    if (f.Revision > 0) {
                        historyId = f.FieldType;
                    }
                    metaData.append(_.template(lightboardFieldTemplate, { key: f.FieldTypeDisplayName, value: f.DisplayValue, type: f.FieldDataType, isOddRow: i++ % 2 == 1, excludeFromDefaultView: f.ExcludeFromDefaultView, historyId: historyId }));
                }
            });
            return metaData;
        },
        openDropzoneModal: function (e) {

            $("#my-dropzone").removeClass("dropzone");
            $("#my-dropzone").addClass("dropzone-button");
            $("#dropzone-container").removeClass("bbm-wrapper");
            $("#drop-zone").removeClass("bbm-modal");

            //$("#drop-zone").hide();
            //$("#my-dropzone").removeClass("dropzone-button");
            //$("#my-dropzone").addClass("dropzone");
            //$("#dropzone-container").addClass("bbm-wrapper");
            //$("#drop-zone").addClass("bbm-modal");
            //$("#drop-zone #dropzone-buttons").show();
        },
        closeDropzoneModal: function () {
            this.myAwesomeDropzone.options.maxFiles = null;
            $("#my-dropzone").removeClass("dropzone");
            $("#my-dropzone").addClass("dropzone-button");
            $("#dropzone-container").removeClass("bbm-wrapper");
            $("#drop-zone").removeClass("bbm-modal");
            $("#drop-zone #dropzone-buttons").hide();
            $("#my-dropzone .dz-text").show();
        },
        render: function () {
            var self = this;

            if (this.collection == undefined) {
                return this;
            }

            this.$el.html(_.template(lightboardTemplate));

            _.each(this.collection.models, function(model) {

                var card = new lightboardCardView({ model: model, parentId: self.id, parententitytype: self.entityType });

                self.$el.find("ul.thumbnails").append(card.$el);


            });

            this.$el.find("ul.thumbnails").append("<div><li><div id=\"dropzone-container\" class=\"lightboard-card-wrap\"></div></li></div>");
            
            // Should not be possible to upload resources to entity with no Resource Linktype
            this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: this.entityType });

            this.relationTypes.fetch({
                success: function () {
                    self.TaskRelationType = _.find(self.relationTypes.models, function (model) {
                        return model.attributes.TargetEntityTypeId == "Resource";
                    });
                    if (!self.TaskRelationType) {

                        if (self.entityType == "Resource") {
                            self.$el.find("#dropzone-container").closest("li").hide();
                        } else {
                            self.$el.find("#dropzone-container").closest("li").remove();

                        }

                        if (self.$el.find("ul.thumbnails li").length == 0) {
                            self.$el.find("#lightboard-actions").html("");
                            self.$el.find("ul.thumbnails").append("<li style=\"margin-top: 5px;\"><i>No media to show</i></li>");
                        }
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            // Can only be one image to resource
            if (this.entityType == "Resource") {
                //this.$el.find("#dropzone-container").hide();
            }

            this.$el.find("#show-all").prop('checked', this.isSubEntities);

            var images = jQuery('.thumbnails img'); //caches the query when dom is ready
            var cards = jQuery(".lightboard-card-wrap"); 
            var CELL_WIDTH = 150;
            var ASPECT = 0.75;

            this.$el.find("#lightboard-slider").slider({
                step: 5,
                min: 80,
                max: 200,
                value: 100,
                slide: function (event, ui) {
                    var size = (CELL_WIDTH * ui.value / 100);
                    var cardHeight = 187 * ui.value / 100;
                    var cardWidth = cardHeight * ASPECT;
                    var width = size * ASPECT;
                    cards.stop(true).animate({ width: cardWidth + "px", height: cardHeight + "px" }, 20);


                    images.each(function() {
                        var maxWidth = width; 
                        var maxHeight = size;  
                        var ratio = 0;  
                        var imgwidth = $(this).width(); 
                        var imgheight = $(this).height();

                            ratio = imgheight / imgwidth; 
                        if ((imgwidth / maxWidth) > (imgheight / maxHeight)) {
                            imgwidth = maxWidth;
                            imgheight = imgwidth * ratio;
                        } else {
                            imgheight = maxHeight;
                            imgwidth = imgheight/ratio;
                        }

                        $(this).stop(true).animate({ 'width': imgwidth + "px", 'height': imgheight + "px" }, 20);
                        $(this).closest(".lightboard-wrapper").animate({ 'line-height': imgheight + "px" }, 20);
                        }
                    ); 
               }
            });


            this.$el.find("#dropzone-container").html(_.template(dropzoneTemplate, { entityId: this.id, type: "NewResourceAndLink" }));

            this.myAwesomeDropzone = new Dropzone("#my-dropzone", {
                url: "/imageupload/file",
                maxFilesize: 1000,
                parallelUploads: 10,
                autoProcessQueue: true,
                dictDefaultMessage: "Drop files here or Click to Browse",
                init: function () {
                    var thisDropzone = this;
                    var entityResourceArray = new Array();

                    this.on("addedfile", function (file) {
                        console.log("added file");
                        self.openDropzoneModal();
                    });

                    this.on("error", function (file, errorMessage, xhr) {
                        
                        if (xhr == undefined) {
                            inRiverUtil.NotifyError("Upload failed", errorMessage, "Filename: " + file.name);
                        }
                        else {
                            if (xhr !== 0 && xhr.status === 409) {
                                inRiverUtil.NotifyError("Upload failed", xhr.statusText, "Filename: " + file.name);
                            } else {
                                inRiverUtil.NotifyError("Upload failed", "Upload of file failed", "");
                            }
                        }

                        $("#drop-zone").show();
                    });

                    this.on("success", function (file, res) {
                        if (res == -1) {
                            inRiverUtil.NotifyError("Resource exists", "Resource with filename already exists", null); 
                            return; 
                        } 

                        console.log("successfully uploaded file");
                        entityResourceArray.push(res);
                        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                            setTimeout(function () {
                                thisDropzone.removeAllFiles();
                                //self.closeDropzoneModal();
                                appHelper.event_bus.trigger('entityupdated', self.id);
                                inRiverUtil.Notify("Upload successfully completed");
                            }, 500);
                        }
                    });
                    
                    $("#start-upload").hide();

                    var closeButton = self.$el.find("#close-upload");
                    closeButton.on("click", function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        thisDropzone.removeAllFiles();
                        self.closeDropzoneModal();
                    });
                },
                accept: function (file, done) {

                    // If the settings for RegEx haven't been loaded yet, time to load.
                    if (self.fieldTypeSetting == null) {
                        self.fieldTypeSetting = new fieldTypeSettingModel("ResourceFilename");
                        self.fieldTypeSetting.fetch({
                            async: false,   // Wait until RegEx data is loaded until we try to validate.
                            success: function (model, response) {
                                // We only want to fill this.attributes for the Model-object.
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    }

                    if (!self.fieldTypeSetting.ValidateRegExp(file.name)) {
                        done("Filename '" + file.name + "' has illegal characters");
                        return;
                    }

                    done();
                }
            });
            this.listenTo(appHelper.event_bus, 'entityupdated', this.entityupdated);
            return this; // enable chained calls
        }
    });

    return lightboardView;
});
