﻿define([
  'jquery',
  'underscore',
  'backbone',
  "multiple-select",
  'text!sharedtemplates/excelexport/excelExportCvlControlTemplate.html'
], function ($, _, backbone, multipleSelect, cvlControlTemplate) {
    var excelExportCvlControlView = backbone.View.extend({
        initialize: function (options) {
            this.controlDefinition = options.ControlDefinition;
            this.id = options.ControlDefinition;
            if (options.Id !== undefined) {
                this.id += "-" + options.Id;
            }
            this.values = options.Values;
            this.multivalue = options.Multivalue;
            this.inputField = "#export-excel-value-" + this.id;
            this.possibleValues = options.PossibleValues;
            this.caption = options.Caption;
            
            this.render();
        },
        render: function () {
            var self = this;
            var template = _.template(cvlControlTemplate, { Caption: this.caption, Definition: this.id });
            this.setElement(template, true);

            if (this.values != null) {
                _.each(this.values.models, function (value) {

                    var key;
                    var text;

                    if (self.controlDefinition === "type") {
                        if (value.attributes.Type != undefined) {
                            key = value.attributes.EntityTypeId + "$$" + value.attributes.Type + "$$" + value.attributes.Id;
                        } else if (value.attributes.EntityTypeId != undefined) {
                            key = value.attributes.EntityTypeId;
                        } else {
                            key = value.id;
                        }
                        text = value.attributes.DisplayName;
                    }
                    else if (self.controlDefinition === "configuration") {
                        key = value.attributes.Name;
                        text = value.attributes.Name;
                    }
                    else if (self.controlDefinition === "fieldset") {
                        key = value.attributes.Id;
                        text = value.attributes.Id;
                    }
                    else if (self.controlDefinition === "languages") {
                        key = value.id;
                        text = value.attributes.DisplayName;
                    }
                    else {
                        if (value.get("hidden")) {
                            return;
                        }
                        key = value.id;
                        text = value.attributes.displayname;
                    }

                    self.$el.find(self.inputField).append("<option value=\"" + key + "\">" + text + "</option>");
                });
            } else {
                self.$el.find(self.inputField).append("<option value=\"\" disabled>Please Select Type</option>");
            }

            this.multipleSelect = this.$el.find(self.inputField).multipleSelect({
                single: !this.multivalue,
                filter: true,
                width: 270,
                onBlur: function () {
                    self.onLostFocus();
                }
            });

            this.$el.find(self.inputField).multipleSelect("setSelects", _.isArray(this.possibleValues) ? this.possibleValues : [this.possibleValues]);

            return this;
        },
        onLostFocus: function (e) {
            if (e != null) {
                e.stopPropagation();
            }
        }
    });

    return excelExportCvlControlView;
});

