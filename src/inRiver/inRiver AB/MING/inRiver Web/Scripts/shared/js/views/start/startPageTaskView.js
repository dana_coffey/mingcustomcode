define([
  'jquery',
  'underscore',
  'backbone',
  'text!sharedtemplates/start/startpageTaskTemplate.html'
], function ($, _, Backbone,  startpageTaskTemplate) {

    var startPageTaskView = Backbone.View.extend({
        initialize: function (data) {
            this.model = data.model;
           
        },
        events: {
            "click": "onTaskClick"
        },
        onTaskClick: function (e) {
            e.stopPropagation();
            this.goTo("entity/" + this.model.Id);
        },
        render: function () {

            if (this.model == null) {
                return this;
            }

            this.$el.html(_.template(startpageTaskTemplate, { displayName: this.model.DisplayName, displayDescription: this.model.DisplayDescription }));

            return this;
        }
    });

    return startPageTaskView;
});

