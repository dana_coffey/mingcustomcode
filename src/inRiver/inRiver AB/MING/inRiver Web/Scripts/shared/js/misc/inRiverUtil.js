﻿
define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'deep-model',
  'backgrid',
  'moment',
  'alertify',
  'modalPopup',
  'sharedjs/models/entity/entityModel',
  'sharedjs/extensions/dateTimeEditor',
  'sharedjs/extensions/localeStringEditor',
  'sharedjs/extensions/checkboxDropdownEditor',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/models/field/fieldModel',
  'sharedjs/views/entityview/entitySystemInformationView'
], function ($, _, backbone, forms, deepmodel, backgrid, moment, alertify, modalPopup, entityModel,
    dateTimeEditor, localeStringEditor, checkboxDropdownEditor, completenessModel, completenessPopupView,
    fieldModel, entitySystemInformationView) {

    var inRiverUtil = {
        getBackboneFormsSchemaOptionsForField: function (field) {
            var editorAttrs = {};

            if (field.IsReadOnly) {
                editorAttrs = { readonly: 'readonly', class: 'readonly', disabled: 'disabled' };
            }

            if (field.IsReadOnly && field.FieldDataType === "DateTime")
            {
                editorAttrs = { disabled: 'disabled' };
            }

            // Validators 
            //Mandatory, 

            var validators = [];
            if (field.IsMandatory) {
                var markedMandatory = field.FieldTypeDisplayName.indexOf(" *");
                if (markedMandatory == -1) {
                    field.FieldTypeDisplayName = field.FieldTypeDisplayName + " *";
                }
                if (field.FieldDataType == "LocaleString") {
                    validators = [
                        function (value) {
                            var hasAtLeastOneValue = false;
                            _.each(_.values(value), function (v) {
                                if (v[1] != null && v[1] != "") {
                                    hasAtLeastOneValue = true;
                                }
                            });
                            return hasAtLeastOneValue ? null : {
                                type: 'localestring',
                                message: 'Must have a value in at least 1 language'
                            };
                        }
                    ];
                } else {
                    validators = [{ type: 'required', message: 'Value is mandatory' }];
                }
            }

            if (field.Settings && field.Settings.RegExp) {

                if (field.Settings.RegExp.indexOf("^") != 0) {
                    field.Settings.RegExp = "^" + field.Settings.RegExp;
                }
                if (field.Settings.RegExp.indexOf("$") < 0) {
                    field.Settings.RegExp = field.Settings.RegExp + "$";
                }
                var regEx = new RegExp(field.Settings.RegExp);
                validators.push({ type: 'regexp', message: 'Value not in correct format', regexp: regEx });
            }

            if (field.FieldDataType == "CVL") {
                editorAttrs.cvlId = field.CvlId;
                editorAttrs.parentCvlId = field.ParentCvlId;

                var possibleValues = [];
                if (!editorAttrs.parentCvlId) { // child cvl's will be populated later on...
                    possibleValues = this.createBackboneFormsPossibleCvlValuesArray(field.PossibleValues);
                }

                if (field.IsMultiValue) {
                    // Display data integrity problems (check if the current value actually exists within the possible values array)
                    _.each(field.Value, function (v) {
                        if (!_.contains(_.pluck(possibleValues, "val"), v)) {
                            possibleValues.splice(0, 0, { label: "Invalid value: [" + (v ? v : "(empty)") + "]", val: v, isValid: false });
                        }
                    });
                    if (possibleValues.length > field.MaxNumberOfInlineCvlValues || editorAttrs.parentCvlId) {
                        return { type: checkboxDropdownEditor, title: field.FieldTypeDisplayName, options: possibleValues, editorAttrs: editorAttrs, validators: validators, isMultivalue: field.IsMultiValue };
                    } else {
                        _.each(possibleValues, function (v) { if (v.isValid === false) v.label = "<span class='invalid-value'>" + v.label + "</span>"; });
                        return { type: "Checkboxes", title: field.FieldTypeDisplayName, options: possibleValues, editorAttrs: editorAttrs, validators: validators, isMultivalue: field.IsMultiValue };
                    }
                } else {
                    // Display data integrity problems (check if the current value actually exists within the possible values array)
                    if (field.Value && !_.contains(_.pluck(possibleValues, "val"), field.Value)) {
                        possibleValues.splice(0, 0, { label: "Invalid value: [" + field.Value + "]", val: field.Value, isValid: false });
                    }
                    if (possibleValues.length > field.MaxNumberOfInlineCvlValues || editorAttrs.parentCvlId) {
                        return { type: checkboxDropdownEditor, title: field.FieldTypeDisplayName, options: possibleValues, editorAttrs: editorAttrs, validators: validators, isMultivalue: field.IsMultiValue };
                    } else {
                        _.each(possibleValues, function (v) { if (v.isValid === false) v.label = "<span class='invalid-value'>" + v.label + "</span>"; });
                        return { type: 'Radio', title: field.FieldTypeDisplayName, options: possibleValues, editorAttrs: editorAttrs, validators: validators, isMultivalue: field.IsMultiValue };
                    }
                }
            } else if (field.FieldDataType == "LocaleString") {
                if (field.languages) {
                    editorAttrs.languages = field.languages;
                }
                return { type: localeStringEditor, title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
            } else if (field.FieldDataType == "DateTime") {
                return { type: dateTimeEditor, title: field.FieldTypeDisplayName, editorAttrs: editorAttrs, editorClass: "date-time-editor-text-box", validators: validators};
            } else if (field.FieldDataType == "Boolean") {

                if (field.IsMandatory) {
                    return {
                        type: 'Radio',
                        title: field.FieldTypeDisplayName,
                        options: [{
                            label: 'True', val: 'True'
                        }, {
                            label: 'False', val: 'False'
                        }],
                        validators: validators,
                        editorAttrs: editorAttrs
                    };
                } else {
                    return {
                        type: 'Radio',
                        title: field.FieldTypeDisplayName,
                        options: [{
                            label: '&#40;not set&#41;', val: ''
                        }, {
                            label: 'True', val: 'True'
                        }, {
                            label: 'False', val: 'False'
                        }],
                        validators: validators,
                        editorAttrs: editorAttrs
                    };
                }

            } else if (field.FieldDataType == "Integer") {
                validators.push({ type: 'regexp', message: 'Value must be a number', regexp: /^\-?[0-9]+$/ });
                return { type: 'Text', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
            } else if (field.FieldDataType == "Double") {
                var regEx = new RegExp("^\-?[0-9]+\.?[0-9]*$".replace(".", window.appHelper.commaseparator));
                validators.push({ type: 'regexp', message: 'Value must be a number. Use ' + window.appHelper.commaseparator + ' as separator', regexp: regEx });
                return { type: 'Text', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
            } else if (field.FieldDataType == "File") {
                validators.push({ type: 'regexp', message: 'Value must be a number', regexp: /^[0-9]+$/ });
                return { type: 'Number', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
            } else {
                if (field.Settings && field.Settings.NumberOfRows && field.Settings.NumberOfRows > 1) {
                    editorAttrs.rows = field.Settings.NumberOfRows;
                    editorAttrs['data-isMultiLine'] = true;
                    editorAttrs.cols = "80";
                    editorAttrs.style = "min-height: 0!important"; // make sure the height doesn't override the rows attribute
                    return { type: 'TextArea', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
                } else {
                    return { type: 'Text', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
                }
            }
        },
        getBackboneFormsSchemaOptionsForSpecificationValue: function (field) {
            var editorAttrs = {};

            if (field.IsReadOnly) {
                editorAttrs = { readonly: 'readonly', class: 'readonly', disabled: 'disabled' };
            }

            var validators = [];
            if (field.Mandatory) {
                var markedMandatory = field.Name.indexOf(" *");
                if (markedMandatory == -1) {
                    field.Name = field.Name + " *";
                }
                if (field.SpecificationDataType == "LocaleString") {
                    validators = [
                        function (value) {
                            var hasAtLeastOneValue = false;
                            _.each(_.values(value), function (v) {
                                if (v[1] != null && v[1] != "") {
                                    hasAtLeastOneValue = true;
                                }
                            });
                            return hasAtLeastOneValue ? null : {
                                type: 'localestring',
                                message: 'Must have a value in at least 1 language'
                            };
                        }
                    ];
                } else {
                    validators = [{ type: 'required', message: 'Value is mandatory' }];
                }
            }

            if (field.SpecificationDataType == "CVL") {
                editorAttrs.cvlId = field.CvlId;
                editorAttrs.parentCvlId = field.ParentCvlId;

                var possibleValues = [];
                if (!editorAttrs.parentCvlId) {
                    possibleValues = this.createBackboneFormsPossibleCvlValuesArray(field.PossibleValues);
                }

                if (field.IsMultiValue) {
                    // Display data integrity problems (check if the current value actually exists within the possible values array)
                    _.each(field.Value, function (v) {
                        if (!_.contains(_.pluck(possibleValues, "val"), v)) {
                            possibleValues.splice(0, 0, { label: "Invalid value: [" + (v ? v : "(empty)") + "]", val: v, isValid: false });
                        }
                    });
                    if (possibleValues.length > field.MaxNumberOfInlineCvlValues || editorAttrs.parentCvlId) {
                        return { type: checkboxDropdownEditor, title: field.Name, options: possibleValues, editorAttrs: editorAttrs, validators: validators, sort: false, isMultivalue: field.IsMultiValue };
                    } else {
                        _.each(possibleValues, function (v) { if (v.isValid === false) v.label = "<span class='invalid-value'>" + v.label + "</span>"; });
                        return { type: 'Checkboxes', title: field.Name, options: possibleValues, editorAttrs: editorAttrs, validators: validators, sort: false, isMultivalue: field.IsMultiValue };
                    }
                } else {
                    // Display data integrity problems (check if the current value actually exists within the possible values array)
                    if (field.Value && !_.contains(_.pluck(possibleValues, "val"), field.Value)) {
                        possibleValues.splice(0, 0, { label: "Invalid value: [" + field.Value + "]", val: field.Value, isValid: false });
                    }
                    if (possibleValues.length > field.MaxNumberOfInlineCvlValues || editorAttrs.parentCvlId) {
                        return { type: checkboxDropdownEditor, title: field.Name, options: possibleValues, editorAttrs: editorAttrs, validators: validators, sort: false, isMultivalue: field.IsMultiValue };
                    } else {
                        _.each(possibleValues, function (v) { if (v.isValid === false) v.label = "<span class='invalid-value'>" + v.label + "</span>"; });
                        return {
                            type: 'Radio', title: field.Name, options: possibleValues, editorAttrs: editorAttrs, validators: validators, sort: false, isMultivalue: field.IsMultiValue
                        };
                    }
                }
            } else if (field.SpecificationDataType == "LocaleString") {
                return {
                    type: localeStringEditor, title: field.Name, validators: validators, editorAttrs: editorAttrs
                };
            } else if (field.SpecificationDataType == "DateTime") {
                return {
                    type: dateTimeEditor, title: field.Name, editorAttrs: editorAttrs, validators: validators
                };
            } else if (field.SpecificationDataType == "Boolean") {
                return {
                    type: 'Radio',
                    title: field.Name,
                    options: [{
                        label: '&#40;not set&#41;', val: ''
                    }, {
                        label: 'True', val: 'True'
                    }, {
                        label: 'False', val: 'False'
                    }],
                    validators: validators,
                    editorAttrs: editorAttrs
                };
            } else if (field.SpecificationDataType == "Integer") {
                validators.push({
                    type: 'regexp', message: 'Value must be a number', regexp: /^\-?[0-9]+$/
                });
                return {
                    type: 'Text', title: field.Name, validators: validators, editorAttrs: editorAttrs
                };
            } else if (field.SpecificationDataType == "Double") {
                var regEx = new RegExp("^\-?[0-9]+\.?[0-9]*$".replace(".", window.appHelper.commaseparator));
                validators.push({
                    type: 'regexp', message: 'Value must be a number. Use ' + window.appHelper.commaseparator + ' as separator', regexp: regEx
                });
                return {
                    type: 'Text', title: field.Name, validators: validators, editorAttrs: editorAttrs
                };
            } else if (field.SpecificationDataType == "File") {
                validators.push({
                    type: 'regexp', message: 'Value must be a number', regexp: /^[0-9]+$/
                });
                return {
                    type: 'Number', title: field.Name, validators: validators, editorAttrs: editorAttrs
                };
            } else {
                if (field.Settings && field.Settings.NumberOfRows && field.Settings.NumberOfRows > 1) {
                    editorAttrs.rows = field.Settings.NumberOfRows;
                    editorAttrs['data-isMultiLine'] = true;
                    editorAttrs.cols = "80";
                    editorAttrs.style = "min-height: 0!important"; // make sure the height doesn't override the rows attribute
                    return { type: 'TextArea', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
                } else {
                    return { type: 'Text', title: field.FieldTypeDisplayName, validators: validators, editorAttrs: editorAttrs };
                }
            }
        },

        createFormsSchemaFromRestfulFields: function (model) {
            //assert(model.get('Fields'), "model must have a Fields attribute");
            var schema = {};

            var fields = model.get("Fields");
            for (var i = 0; i < fields.length; i++) {
                var editorKey = "Fields." + i.toString() + ".Value";
                schema[editorKey] = inRiverUtil.getBackboneFormsSchemaOptionsForField(fields[i]);
                schema[editorKey]["_internalName"] = fields[i].FieldType;
                schema[editorKey]["_internalEditorKey"] = editorKey;
                schema[editorKey]["_internalDataType"] = fields[i].FieldDataType;
            }
            return schema;
        },
        createFormsSchemaFromSpecification: function (model) {
            //assert(model.get('Fields'), "model must have a Fields attribute");
            var schema = {};

            var categories = model.get("Categories");
            for (var j = 0; j < categories.length; j++) {
                for (var i = 0; i < categories[j].SpecificationValues.length; i++) {
                    var editorKey = "Categories." + j + ".SpecificationValues." + i + ".Value";
                    schema[editorKey] = inRiverUtil.getBackboneFormsSchemaOptionsForSpecificationValue(categories[j].SpecificationValues[i]);
                    schema[editorKey]["_internalName"] = categories[j].SpecificationValues[i].FieldTypeId;
                    schema[editorKey]["_internalEditorKey"] = editorKey;
                    schema[editorKey]["_internalDataType"] = categories[j].SpecificationValues[i].SpecificationDataType;
                    schema[editorKey]["_categoryId"] = categories[j].SpecificationValues[i].CategoryId;
                }
            }
            return schema;
        },
        createFormsSchemaFromRestfulLinks: function (model, schema) {
            var links = model.get("InBoundLinks");
            var i;
            for (i = 0; i < links.length; i++) {
                schema["InBoundLinks." + i.toString() + ".Value"] = { type: 'InRiverLinks', title: 'Related ' + links[i].LinkTypeDisplayName };
            }

            links = model.get("OutBoundLinks");
            for (i = 0; i < links.length; i++) {
                schema["OutBoundLinks." + i.toString() + ".Value"] = { type: 'InRiverLinks', title: 'Related ' + links[i].LinkTypeDisplayName };
            }

            return schema;
        },
        findBackboneFormsEditorKey: function (schema, nameToLookFor, categoryId) {
            var v = _.find(schema, function (value) {
                if (value._categoryId != undefined) {
                    if (value._internalName == nameToLookFor && categoryId != null && value._categoryId == categoryId) {
                        return value._internalName;
                    }
                } else {
                    return value._internalName == nameToLookFor;
                }
            });
            return v ? v._internalEditorKey : null;
        },

        createBackboneFormsPossibleCvlValuesArray: function (rawArray) {
            var array = [];
            _.each(rawArray, function (e) {
                array.push({ val: e.Key, label: e.Value });
            });
            return array;
        },
        createValueForFormFieldComparison: function (v, fieldDataType) {
            if (v == "" || v == null) return null;
            var r = null;
            if(fieldDataType === "DateTime") {
                v = moment(v).unix(); // use unix time for comparisons
            } else if (inRiverUtil.isInt(v) || inRiverUtil.isFloat(v)) {
                v = v.toString();   // text editors are always strings so we have to convert numbers 
            }
            if (inRiverUtil.isArray(v)) {
                // Don't touch original value
                var newObject = jQuery.extend(true, [], v); // cloning
                newObject.sort();
                r = JSON.stringify(newObject);
            } 
            else {
                r = JSON.stringify(v);
            }
            r = r.replace(/(\\r\\n|\\n|\\r)/gm, "\\n"); // Some browsers seem to have different line breaks
            return r;
        },
        applyStripingToForm: function (el) {
            el.find("fieldset > div").filter(":even").addClass("inriver-fields-form-area-alternative-field-background");
        },
        applyCenteredLayoutToForm: function (el) {
            el.find("fieldset > div").wrap("<div></div>");
            el.find("fieldset > div > div").addClass("width-limited-container");
        },
        onConfirmDelete: function (self, withoutNotify) {
            var id = self.model.id;
            self.model.destroy({
                success: function () {

                    if (!withoutNotify) {
                        inRiverUtil.Notify(self.model.attributes.EntityTypeDisplayName + " was successfully deleted");
                    }

                    window.appHelper.event_bus.trigger('entitydeleted', id);
                    if (self.remove) {
                        self.remove();
                    }
                    if (self.unbind) {
                        self.unbind();
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onConfirmDeleteMany: function (cards) {
            for (var i = 0; i < cards.length; i++) {
                inRiverUtil.onConfirmDelete(cards[i], true);
            }

            inRiverUtil.Notify(cards[0].model.attributes.EntityTypeDisplayName + " items was successfully deleted");
        },
        deleteEntities: function (cards) {

            inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete selected " + cards[0].model.attributes.EntityTypeDisplayName + " items" + "?", this.onConfirmDeleteMany, cards);

        },
        deleteEntity: function (self) {

            inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete this " + self.model.attributes.EntityTypeDisplayName + "?", this.onConfirmDelete, self);

        },
        renderLoadingSpinner: function () {
            this.$el.html(backbone.LoadingSpinnerEl);
        },
        getUrlVars: function () {
            var vars = [], hash;
            var url = window.location.href.split('#')[0];
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getUrlVar: function (name) {
            return inRiverUtil.getUrlVars()[name];
        },
        DateTimeHelper: {
            // 86400000 = 1000ms * 60secs * 60mins * 24hours
            daysDiff: function (date1, date2) {
                window.assert(this.isDateObject(date1) && this.isDateObject(date2));
                return Math.ceil((date1.getTime() - date2.getTime()) / (86400000));
            },
            isDateObject: function (date) {
                return Object.prototype.toString.call(date) === '[object Date]';
            },
            addDays: function (date, nDays) {
                // assert(this.isDateObject(date));
                var r = new Date();
                r.setTime(date.getTime() + nDays * 86400000);
                return r;
            }
        },
        ShowInlineCompleteness: function (id, at) {
            var tmpModel = new completenessModel({ Id: id }); // id 0 means new object 
            tmpModel.urlRoot = "../../api/completeness/";

            var cd = $("#CompletenessDetails-" + id);
            if (cd.length == 0) {
                var x = at.after("<div id=\"CompletenessDetails-" + id + "\" class=\"CompletenessDetails\"></div>");
                cd = $("#CompletenessDetails-" + id);
            } else {
                cd.remove();
                return;
            }
            cd = cd[0];

            tmpModel.fetch({
                success: function () {

                    $(cd).html(new completenessPopupView({
                        model: tmpModel
                    }).$el);
                    $(cd).position(
                    {
                        my: "right top",
                        at: "left+30 top+20",
                        of: at
                    });

                    var close = function (e) {
                        if (e.currentTarget != cd) {
                            $("#CompletenessDetails-" + id).remove();
                        }
                    };

                    $(document).on('click', close.bind(this));

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        ShowCompleteness: function (id) {
            var tmpModel = new completenessModel({ Id: id }); // id 0 means new object 
            tmpModel.urlRoot = "../../api/completeness/";

            tmpModel.fetch({
                success: function () {

                    var modal = new modalPopup();

                    modal.popOut(new completenessPopupView({
                        model: tmpModel
                    }), { size: "small", header: tmpModel.attributes.EntityName });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        ShowEntityInfo: function (id) {
            var tmpModel = new entityModel({ Id: id }); // id 0 means new object 
            tmpModel.urlRoot = "../../api/entity/";

            tmpModel.fetch({
                success: function () {
                    var modal = new modalPopup();
                    modal.popOut(new entitySystemInformationView({ model: tmpModel }), { size: "small", header: tmpModel.attributes.EntityTypeDisplayName + " " + tmpModel.attributes.DisplayName });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        OnErrors: function (model, response) {
            if (response.status == null) {
                inRiverUtil.NotifyError(inRiverUtil.GetErrorHeaderFromHttpResponseCode(model.status), inRiverUtil.GetErrorMessageFromHttpResponseCode(model.status));
                return;
            }

            inRiverUtil.NotifyError(inRiverUtil.GetErrorHeaderFromHttpResponseCode(response.status), inRiverUtil.GetErrorMessageFromHttpResponseCode(response.status, response));
        },
        NotifyError: function (header, message, details) {
            //IF there already is an alert message, we dont show more...
            if ($(".alertify-alert").length > 0) {
                return;
            }

            if (details == null) {
                alertify.alert("<div class=\"alertify-alert-header\"><div>" + header + "</div></div><div class=\"alertify-alert-message\">" + message + "</div>");
            } else {
                // To show an extra link.
                var safestring = $('<div>').text(details).html();
                alertify.alert("<div class=\"alertify-alert-header\"><div>" + header +
                    "</div></div><div class=\"alertify-alert-message\">" + message +
                    "<br/><br/><a id=\"alertify-alert-show-detail-link\" onClick=\"$('#alertify-alert-details-information').show();$('#alertify-alert-hide-detail-link').show();$('#alertify-alert-show-detail-link').hide();\" style=\"color: #00f; cursor: pointer; font-size: 11px;\">Show details</a>" +
                    "<a id=\"alertify-alert-hide-detail-link\" onClick=\"$('#alertify-alert-details-information').hide();$('#alertify-alert-show-detail-link').show();$('#alertify-alert-hide-detail-link').hide();\" style=\"color: #00f; cursor: pointer; font-size: 11px; display: none;\">Hide details</a>" +
                    "<div id=\"alertify-alert-details-information\" style=\"display: none; font-size: 11px; color: #bbb\"><br\>" + safestring + "</div>");
            }
        },
        NotifyConfirm: function (header, message, callback, param) {
            alertify.confirm("<div class=\"alertify-confirm-header\"><div>" + header + "</div></div><div class=\"alertify-confirm-message\">" + message + "</div>", function (confirm) {
                if (confirm) {
                    callback(param);
                }
            });
        },
        Notify: function (message) {
            alertify.success(message);
        },
        Prompt: function (message, fn, defaultValue) {
            message = "<div class=\"alertify-prompt-header\"><div>" + message + "</div></div>";
            alertify.prompt(message, fn, defaultValue/*, "alertify-confirm-header"*/);
        },
        isInt: function (n) {
            return Number(n) === n && n % 1 === 0;
        },
        intOrNull: function (i) {
            if (!inRiverUtil.isInt(i)) {
                i = parseInt(i);
            }
            return inRiverUtil.isInt(i) ? i : null;
        },
        isFloat: function (n) {
            return n === Number(n) && n % 1 !== 0;
        },
        isArray: function (o) {
            return Object.prototype.toString.call(o) === '[object Array]';
        },
        isDateObject: function (date) {
            return Object.prototype.toString.call(date) === '[object Date]';
        },
        toRoundtripDateFormat: function (date) {
            if (date == null) return null;
            var r = moment(date);
            if (!r.isValid()) {
                return null;
            } else {
                return r.format(); // ISO compatible roundtrip format
            }
        },
        viewHasUnsavedChanges: function (view) {
            return view && _.isFunction(view.hasUnsavedChanges) && view.hasUnsavedChanges();
        },
        proceedOnlyAfterUnsavedChangesCheck: function (view, successFn) {
            if (inRiverUtil.viewHasUnsavedChanges(view)) {
                inRiverUtil.NotifyConfirm("Warning: Unsaved Changes", "You have made changes that are not saved. Do you want to continue and lose the changes?", function () {
                    successFn();
                });
            } else {
                successFn();
            }
        },
        isLocked: function (lockedBy) {
            if (lockedBy == null) {
                return false;
            }

            if (lockedBy == "") {
                return false;
            }

            return lockedBy.localeCompare(window.appHelper.userName) != 0;
        },
        GetErrorHeaderFromHttpResponseCode: function (code) {
            var result;
            switch (code) {
                case 400:
                    result = "Error in request";
                    break;
                case 401:
                    result = "Unauthorized";
                    break;
                case 403:
                    result = "Access denied";
                    break;
                case 408:
                    result = "Timeout";
                    break;
                default:
                    result = "Error";
                    break;
            }

            return result;
        },
        GetErrorMessageFromHttpResponseCode: function (code, response) {
            var result;
            switch (code) {
                case 400:
                    result = "Unexpected bad server request call.";
                    break;
                case 401:
                    result = "Server authentication failed to make that action. Try and re-login if this continue.";
                    break;
                case 403:
                case 406:
                case 409:
                    result = response.statusText;
                    break;
                case 408:
                    result = "The requested server call timed out. Please try again.";
                    break;
                default:
                    result = "An unexpected error occurred";
                    break;
            }

            return result;
        },
        openEntityPreview: function (templatePrintingSetupView, entityTypeId, entityId, entityDisplayName, templates) {
            var availableTemplates = _.filter(templates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, entityTypeId);
            });
            var modal = new modalPopup();
            modal.popOut(new templatePrintingSetupView({ mode: "preview", entityId: entityId, entityDisplayName: entityDisplayName, templates: availableTemplates }), { size: "small", header: "Preview", usesavebutton: false });
        },
        createEntityPDF: function (templatePrintingSetupView, entityTypeId, entityId, entityDisplayName, templates) {
            var availableTemplates = _.filter(templates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, entityTypeId);
            });
            var modal = new modalPopup();
            modal.popOut(new templatePrintingSetupView({ mode: "pdf", entityId: entityId, entityDisplayName: entityDisplayName, templates: availableTemplates }), { size: "small", header: "Create PDF", usesavebutton: false });
        },
        isEventListenedTo: function (eventName) {

            var existingEvent = window.appHelper.event_bus._events[eventName];

            return (window.appHelper.event_bus._events) ? !!window.appHelper.event_bus._events[eventName] : false;
        }
    };

    // Useful function when we don't know if the property name uses camel casing or not
    var getEx = function(p) {
        var tmp = this.get(p);
        if (tmp == undefined) {
            if (p[0] >= 'a' && p[0] <= 'z') {
                p = p[0].toUpperCase() + p.slice(1);
                return this.get(p);
            } else if (p[0] >= 'A' && p[0] <= 'Z') {
                p = p[0].toLowerCase() + p.slice(1);
                return this.get(p);
            }
        }
        return tmp;
    }
    Backbone.Model.prototype.getEx = getEx;
    Backbone.DeepModel.prototype.getEx = getEx;

    // Use moment.js for formatting
    $.datetimepicker.setDateFormatter({
        parseDate: function (date, format) {
            var d = moment(date, [format, moment.ISO_8601]);

            if (!d.isValid()) {
                d = moment(date);

                if (!d.isValid()) {
                    return false;
                }
            }

            return d.toDate();
        },
        formatDate: function (date, format) {
            return moment(date).format(format);
        }
    });

    window.globalUtil = {};
    window.globalUtil.toRoundtripDateFormat = inRiverUtil.toRoundtripDateFormat;

    return inRiverUtil;

});


Array.prototype.byValueEquals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}
