define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/fieldrevision/fieldRevisionModel'
], function ($, _, Backbone, entityModel) {
    var fieldRevisionCollection = Backbone.Collection.extend({
        model: entityModel,
        initialize: function (models, options) {
            this.entityId = options.entityId;
            this.fieldTypeId = options.fieldTypeId; 
        },
        url: function() {
                return '/api/fieldrevision?entityId=' + this.entityId + '&fieldTypeId=' + this.fieldTypeId;
        }
    });

    return fieldRevisionCollection;
});
