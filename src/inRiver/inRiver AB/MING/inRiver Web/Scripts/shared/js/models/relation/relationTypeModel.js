﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var relationModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.id = options.id; 
        },
        urlRoot: '/api/relationtype',
        
    });

    return relationModel;

});
