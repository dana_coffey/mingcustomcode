define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/connectorevent/connectorEventModel'
], function ($, _, backbone, connectorEventModel) {
    var latestConnectorEventCollection = backbone.Collection.extend({
        model: connectorEventModel,
        url: '/api/connectorevent/'
    });

    return latestConnectorEventCollection;
});
