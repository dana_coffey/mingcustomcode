define([
  'jquery',
  'underscore',
  'backbone',
  //'models/completeness/CompletenessDefinitionModel'
], function ($, _, Backbone, CompletenessDefinitionModel) {
    var CompletenessDefinitionCollection = Backbone.Collection.extend({
      //model: CompletenessDefinitionModel,
      url: '/api/completenessdefinition'
  });
 
  return CompletenessDefinitionCollection;
});
