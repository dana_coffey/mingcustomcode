define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/job/JobModel'
], function($, _, backbone, jobModel){
  var jobsCollection = backbone.Collection.extend({
      model: jobModel,
      url: '/api/job'
  });
 
  return jobsCollection;
});
