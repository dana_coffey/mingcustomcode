﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/syndicate/distributionFormatModel',
    'sharedjs/models/syndicate/syndicationWorkareaModel',
    'sharedjs/collections/syndicate/receiverCollection',
    'sharedjs/collections/relations/relationCollection',
    'sharedjs/collections/syndicate/syndicationEntityCollection',
    'sharedjs/views/syndicate/syndicateWorkareaDistrFormatView',
    'text!sharedtemplates/syndicate/syndicateWorkareaTemplate.html'
], function ($, _, backbone, inRiverUtil, distributionFormatModel, syndicationWorkareaModel, receiverCollection, relationCollection, syndicationEntityCollection, syndicateWorkareaDistrFormatView, syndicateWorkareaTemplate) {
    var syndicateWorkareaView = backbone.View.extend({
        template: _.template(syndicateWorkareaTemplate),
        initialize: function(options) {
            var self = this;
            this.items = options.items;
            this.products = options.products;
            this.workareaId = options.workareaId;

            this.receiversLoaded = false;
            this.itemsLoaded = false;
            this.productsLoaded = false;

            var syndicationEntities = new syndicationEntityCollection([], { workareaId: this.workareaId });
            syndicationEntities.fetch({
                success: function (callback) {
                    var that = self;
                    self.items = [];
                    self.products = [];
                    _.each(callback.models, function (model) {
                        if (model.get("EntityType") === "Item") {
                            that.items.push(model);
                        }

                        if (model.get("EntityType") === "Product") {
                            that.products.push(model);
                        }
                    });

                    self.itemsLoaded = true;
                    self.productsLoaded = true;
                    self.render();

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            //if (this.items.length > 0) {
            //    // Get products from the items
            //    this.products = [];
            //    var newItems = [];
            //    var count = 0;
            //    _.each(this.items, function(item) {
            //        var relations = new relationCollection([], { entityId: item.id, direction: 'inbound', entityType: 'Product' });
            //        relations.fetch({
            //            success: function(callback) {
            //                var that = self;
            //                count++;
            //                if (callback.models.length === 1) {
            //                    newItems.push(item);
            //                    _.each(callback.models, function(model) {
            //                        var result = $.grep(that.products, function(p) { return p.id === model.get("SourceId"); });
            //                        if (result.length === 0) {
            //                            that.products.push(model.get("SourceEntity"));
            //                        }
            //                    });
            //                }

            //                if (count === that.items.length) {
            //                    that.items = newItems;
            //                    that.itemsLoaded = true;
            //                    that.productsLoaded = true;
            //                    that.render();
            //                }
            //            },
            //            error: function(model, response) {
            //                inRiverUtil.OnErrors(model, response);
            //            }
            //        });
            //    });
            //} else {
            //    // Get all items from thwe products
            //    this.items = [];
            //    var newProducts = [];
            //    var count2 = 0;
            //    _.each(this.products, function (product) {
            //        var productRelations = new relationCollection([], { entityId: product.id, direction: 'outbound', entityType: 'Item' });
            //        productRelations.fetch({
            //            success: function (callback) {
            //                var that = self;
            //                count2++;
            //                if (callback.models.length > 0) {
            //                    newProducts.push(product);

            //                    _.each(callback.models, function(model) {
            //                        var result = $.grep(that.items, function(i) { return i.id === model.get("TargetId"); });
            //                        if (result.length === 0) {
            //                            that.items.push(model.get("TargetEntity"));
            //                        }
            //                    });
            //                }
            //                if (count2 === that.products.length) {
            //                    that.products = newProducts;
            //                    that.productsLoaded = true;
            //                    that.itemsLoaded = true;
            //                    that.render();
            //                }
            //            },
            //            error: function (model, response) {
            //                inRiverUtil.OnErrors(model, response);
            //            }
            //        });
            //    });
            //}

            this.receivers = new receiverCollection();
            this.receivers.fetch({
                success: function () {
                    self.receiversLoaded = true;
                    self.render();
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #start-syndicate": "onSyndicateButtonClicked"
        },
        onSyndicateButtonClicked: function(e) {
            e.stopPropagation();
            if (this.distributionFormatView == undefined || !this.distributionFormatView.readyToSyndicate) {
                return;
            }

            this.selectedFormat = this.distributionFormatView.getSelectedDistributionFormat();
            $.fileDownload("/api/syndication/workarea/" + this.workareaId + "/distributionformat/" + this.selectedFormat, {
                httpMethod: "POST",
                successCallback: function () {
                    window.appHelper.event_bus.trigger("closepopup");
                },
                failCallback: function (responseHtml, url) {
                    inRiverUtil.OnErrors(responseHtml, url);
                }
            });
        },
        render: function () {
            if (!(this.receiversLoaded && this.itemsLoaded && this.productsLoaded)) {
                this.$el.html("<div class='white-box' style='width: 100px; margin: auto;'><div class='loadingspinner'><img src='/Images/nine-squares-32x32.gif' /></div></div>");
                return this;
            }

            var showItems = this.items.length > 0;
            var showProducts = this.products.length > 0;
            this.$el.html(this.template({ ShowItemInfo: showItems, ShowProductInfo: showProducts, NumberOfItems: this.items.length, NumberOfProducts: this.products.length }));

            this.distributionFormatView = new syndicateWorkareaDistrFormatView({
                controlId: 1,
                receivers: this.receivers
            });

            this.$el.find("#syndication-formats-control-container").append(this.distributionFormatView.$el);
            this.stopListening(window.appHelper.event_bus);
            return this;
        }
    });

    return syndicateWorkareaView;
});