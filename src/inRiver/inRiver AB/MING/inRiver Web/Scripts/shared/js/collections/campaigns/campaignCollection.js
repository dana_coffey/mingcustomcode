﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/campaign/campaignModel'
], function ($, _, Backbone, campaignModel) {
    var campaignCollection = Backbone.Collection.extend({
        model: campaignModel,
        initialize: function (options) {
        },
        url: function () {
            return '/api/campaign';
        }
    });

    return campaignCollection;
});
