define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
    var imageConfigurationCollection = Backbone.Collection.extend({
        url: '/api/imageconfiguration/name'
    });

    return imageConfigurationCollection;
});
