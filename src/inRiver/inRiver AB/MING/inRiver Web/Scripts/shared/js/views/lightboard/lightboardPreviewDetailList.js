﻿define([
  'jquery',
  'underscore',
  'backbone',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/models/field/fieldModel',
  'text!sharedtemplates/lightboard/lightboardFieldTemplate.html'

], function ($, _, Backbone, inRiverUtil, fieldModel, lightboardFieldTemplate) {

    var lightboardPreviewDetailListView = Backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.entityModel = options.model;

            //load all fields.-
            this.model = new fieldModel({ id: this.entityModel.id });
            this.model.fetch(
            {
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
       
        render: function () {
            var that = this;
            this.$el.html(""); //_.template(entityDetailTemplate, { data: this.entityModel.toJSON() }));

            i = 0;
            var container = this.$el; //&.find("#details-row-container");
            _.each(this.model.get("Fields"), function (f) {
                if (!f.IsHidden && (!f.ExcludeFromDefaultView || (f.ExcludeFromDefaultView && _.contains(f.FieldSets.split(","), that.model.get("FieldSet"))))) {
                    container.append(_.template(lightboardFieldTemplate, { key: f.FieldTypeDisplayName, value: f.DisplayValue, type: f.FieldDataType, isOddRow: i++ % 2 == 1, excludeFromDefaultView: f.ExcludeFromDefaultView }));
                }
            });

            return this; // enable chained calls
        }
    });

    return lightboardPreviewDetailListView;
});
