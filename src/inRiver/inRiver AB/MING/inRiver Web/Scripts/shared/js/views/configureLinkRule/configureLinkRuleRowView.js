﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/advancedSearchUtil',
  'sharedjs/collections/entityTypes/entityTypeCollection',
  'sharedjs/collections/channel/channelCollection',
  'sharedjs/collections/cvl/cvlValueCollection',
  'sharedjs/collections/fieldsets/fieldsetCollection',
  'sharedjs/collections/fieldtypes/FieldTypesCollection',
  'sharedjs/collections/languages/LanguagesCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'text!sharedtemplates/configurelinkrule/configureLinkRuleRowTemplate.html'

], function ($, _, Backbone, inRiverUtil, advancedSearchUtil, entityTypeCollection, channelCollection, cvlValueCollection, fieldsetCollection, fieldTypesCollection, languagesCollection, relationTypeCollection, configureLinkRuleRowTemplate) {

    var configureLinkRuleRowView = Backbone.View.extend({
        initialize: function (options) {
            this.queryLanguage = options.queryLanguage;
            this.queryValue = options.queryValue;
            this.queryOperator = options.queryOperator;
            if (!this.queryOperator) {
                this.queryOperator = "Equal";
            }

            this.querySystem = options.querySystem;
            this.entityType = options.entityType;

            this.showLanguage = false;
            this.showValue = true;

            this.multivalue = true;

            $(document).on('click', this.eventDeselect.bind(this));
            this.render();
        },
        toObject: function () {
            if (this.deleted) {
                return null;
            }

            var row = { ruledefinitionid: 0, fieldtypeid: this.querySystem, value: this.queryValue, operator: this.queryOperator };

            return row;
        },
        valueIsSet: function () {
            if (this.isBoolValue) {
                return true;
            }

            if (!this.queryValue)
                return false;

            return true;
        },
        ruleSystemLinkText: function() {
            var val = this.$el.find("#rule-system-link").html();
            return val;
        },
        events: {
            "click #rule-system-link": "onSystemLink",
            "click #rule-language-link": "onLanguageLink",
            "click #rule-operator-link": "onOperatorLink",
            "click #rule-operator-list li": "onOperatorLinkSelection",

            "click #rule-value-link": "onValueLink",
            "click #rule-system-list div.category": "onCategoryClick",
            "click #rule-system-list div.showCategory": "onCategoryClick",
            "click #rule-system-list div": "onSystemLinkSelection",
            "click #rule-language-list li": "onLanguageLinkSelection",
            "click #rule-value-list input": "onValueLinkSelection",
            "click #rule-value-list div": "onValueLinkSelection",
            "keyup #filter": "onFiltering",
            "click #query-remove-row": "onDelete"
        },
        eventDeselect: function(element) {
            if (element != undefined && element.target.id == "filter") {
                return;
            }

            this.deselect();
        },
        deselect: function () {
            if (this.cvlId == null) {

                this.validated = true;
                this.inActiveState = false;
                var valueInput = this.$el.find("#rule-value-input").val();

                if (this.isIntValue) {
                    this.validated = false;
                    if (parseInt(valueInput) != NaN) {
                        this.validated = true;
                    } else {
                        this.$el.find("#rule-value-input").val("");
                    }
                }

                if (valueInput != "" && this.validated) {
                    this.queryValue = valueInput;
                    // this.$el.find("#value-link").addClass("value-bold");


                    this.$el.find("#rule-value-link").html(this.queryValue);
                }
                this.$el.find("#rule-value-input").hide();
                if (this.showValue == true) {
                    this.$el.find("#rule-value-link").show();
                }
            }
            $(".query-menu").hide();
        },
        onFiltering: function (e) {
            e.stopPropagation();

            var filterString = this.$el.find("input#filter").val();


            if (filterString && filterString.length > 2) {
                $("div.item").hide();
                $("div.item").each(function (i, e) {
                    if (e.innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(e).show();
                });
            } else {
                $("div.item").show();
            }

        },
        onCategoryClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var catid = e.currentTarget.id;

            if (catid.indexOf("-show") < 0) {
                catid += "-show";
            }
            this.$el.find("#" + catid + " i").toggle();
            var fields = this.$el.find("#" + catid + " i").parent().parent().find("#fields");
            fields.toggle();

        },

        onClose: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.remove();
            this.unbind();
        },
        onSystemLink: function (e) {
            e.stopPropagation();
            e.preventDefault();

            this.$el.find("#rule-system-list:first").toggle(50);
            this.$el.find("#rule-operator-list").hide();
            this.$el.find("#rule-value-list").hide();
        },
        onLanguageLink: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find("#rule-system-list").hide();
            this.$el.find("#rule-language-list:first").toggle(50);
            this.$el.find("#rule-value-list").hide();
        },
        onOperatorLink: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find("#rule-system-list").hide();
            this.$el.find("#rule-operator-list:first").toggle(50);
            this.$el.find("#rule-value-list").hide();
        },
        onValueLink: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find("#rule-system-list").hide();
            //   this.$el.find("#operator-list").hide();

            if (this.valuesList) {
                this.$el.find("#rule-value-list:first").toggle(50);
            } else {
                this.$el.find("#rule-value-input").show();
                this.$el.find("#rule-value-input").focus();
                this.$el.find("#rule-value-link").hide();
            }
        },
        onValueLinkSelection: function (e) {
            e.stopPropagation();
            if (!this.cvlId) {
                this.deselect();
                this.queryValue = e.currentTarget.id;
                this.$el.find("#rule-value-link").html(e.currentTarget.innerHTML);

            } else {

                if (e.currentTarget.className == "item") {
                    var checkbox = $(e.currentTarget).find("input#" + e.currentTarget.id)[0];
                    if (checkbox.checked) {
                        checkbox.checked = !checkbox.checked;
                    } else {
                        checkbox.checked = true;
                    }
                }

                var i = this.$el.find("#rule-value-list input:checked");

                var keys = "";
                var values = "";
                $.each(i, function (index, value) {

                    keys += value.id + ";";
                    values += value.value + ";";
                });
                if (keys.length > 0) {
                    keys = keys.substring(0, keys.length - 1);
                }
                if (values.length > 0) {
                    values = values.substring(0, values.length - 1);
                }

                this.queryValue = keys;

                if (values != null && values.length > 0) {
                    this.$el.find("#rule-value-link").html(values);
                } else {
                    this.$el.find("#rule-value-link").html("select value");
                }

            }

        },

        onLanguageLinkSelection: function (e) {
            e.stopPropagation();
            this.deselect();
            this.queryLanguage = e.currentTarget.id;
            this.$el.find("#rule-language-link").html(e.currentTarget.innerHTML);
        },
        onSystemLinkSelection: function (e) {
            if (e != null) {
                if (e.currentTarget.id == "filter") {
                    return;
                }
                if ($(e.currentTarget).hasClass("category") || $(e.currentTarget).hasClass("showCategory")) {
                    return;
                }

                e.stopPropagation();
                this.deselect();
            }
            var self = this;

            // Clear and reset all selections
            //   this.$el.find('#operator-list').empty();
            this.$el.find('#rule-value-list').empty();
            this.$el.find('#rule-language-list').empty();
            this.$el.find('#rule-language-link').hide();
            this.$el.find("#rule-value-input").datetimepicker("destroy");
            if (e) {
                this.$el.find("#rule-value-input").val("");
                this.$el.find("#rule-value-link").html("enter a value");
            } else {
                this.$el.find("#rule-value-input").val(this.queryValue);
                if (this.queryValue != null) {
                    this.$el.find("#rule-value-link").html(this.queryValue);
                } else {
                    this.$el.find("#rule-value-link").html("enter a value");
                }
            }
            this.$el.find("#rule-value-link").show();
            this.valuesList = false;
            this.showLanguage = false;
            if (e != null && this.systemQuery == undefined) {
                this.querySystem = e.currentTarget.id;
                this.$el.find("#rule-system-link").html(e.currentTarget.innerHTML);
            }

            var model = this.collection.get(this.querySystem);
            this.cvlId = model.get("cvlid");
            this.multivalue = model.get("multivalue");
            var operatorType = this.setValueType(model.get("datatype"));
            this.setOperatorsList(advancedSearchUtil.setNodeOperators(operatorType));
            return;


        },
        setValueType: function (valueType) {
            var self = this;
            this.isIntValue = false;
            this.isBoolValue = false;
            var operatorType;
            var values = {};

            this.$el.find("#rule-operator-link").html("Equals");

            switch (valueType) {
                case "String":
                case "Double":
                case "Xml":
                case "File":
                    operatorType = "set3";
                    return operatorType;
                    break;
                case "LocaleString":
                    var langCollection = new languagesCollection();
                    langCollection.fetch({
                        success: function (collection) {
                            _.each(collection.toJSON(), function (model) {
                                values[model.Name] = model.DisplayName;
                            });
                            self.setLanguagesList(values);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    operatorType = "set6";
                    return operatorType;
                    break;
                case "Boolean":
                    this.isBoolValue = true;
                    operatorType = "setBoolean";

                    if (this.queryOperator == null || this.queryOperator == "Equal") {
                        this.$el.find("#rule-operator-link").text("True");
                    } else {
                        this.$el.find("#rule-operator-link").text(this.queryOperator);
                    }

                    if (this.queryOperator == null) {
                        this.queryOperator = "IsTrue";
                    }

                    this.$el.find("#rule-value-link").hide();
                    this.showValue = false;

                    return operatorType;
                    break;
                case "Integer":
                case "CompletenessValue":
                    this.isIntValue = true;
                    operatorType = "set5";
                    return operatorType;
                    break;
                case "DateTime":
                    this.$el.find("#rule-value-input").datetimepicker({
                        format: appHelper.momentDateTimeFormat,
                        formatTime: appHelper.momentTimeFormat,
                        formatDate: appHelper.momentDateFormat,
                        lang: 'en'
                    });
                    operatorType = "set5";
                    return operatorType;
                    break;
                case "CVL":
                    var cvlValCollection = new cvlValueCollection([], { cvlId: self.cvlId });
                    cvlValCollection.fetch({
                        success: function (collection) {
                            self.setValuesList(collection);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    if (this.multivalue) {
                        operatorType = "multiCVL";
                    } else {
                        operatorType = "singleCVL";
                    }

                    if (this.queryOperator == null || this.queryOperator == "Equal") {
                        this.$el.find("#rule-operator-link").text("Contains Any selected");
                        this.queryOperator = "ContainsAny";
                    }

                    return operatorType;
                    break;

            }
        },
        setSystemList: function (list) {
            var self = this;
            if (list != undefined) {
                var i = 1;

                var filter = "<input id=\"filter\" class=\"filter\" placeholder=\"Filter\" type=\"text\" /><i class=\"fa fa-filter\" id=\"workareasearch\"></i>";

                self.$el.find('#rule-system-list')
                    .append($('<div>', { id: "filter" })
                        .html(filter));


                var currentCategory = "";
                var categorycount = 0;
                var section = $().html("");
                _.each(list, function (value, key) {
                    if (self.querySystem != null) {
                        if (key == self.querySystem) {
                            self.$el.find("#rule-system-link").html(value.field);
                            self.onSystemLinkSelection(null);
                        }
                    } else if (i == 1) {
                        self.$el.find("#rule-system-link").html(value.field);
                        self.querySystem = key;
                        self.onSystemLinkSelection(null);
                    }

                    if (currentCategory != value.categoryName) {

                        if (categorycount > 0) {
                            self.$el.find('#Category' + categorycount + " #fields").append(section);
                        }
                        categorycount++;
                        var html = "<div class=\"showCategory\" id=\"" + "Category" + categorycount + "-show\">" +
                            "<i class=\"fa fa-minus-circle\"></i><i class=\"fa fa-plus-circle\" style=\"display:none\"></i></div>" + value.categoryName + "<div id=\"fields\"></div>";

                        self.$el.find('#rule-system-list')
                            .append($('<div>', { id: "Category" + categorycount, class: "category" })
                                .html(html));

                        currentCategory = value.categoryName;
                        section = self.$el.find('#Category' + categorycount + " #fields");

                    }

                    section.append($('<div>', { id: key, class: "item" })
                            .text(value.field));
                    i++;
                });

                if (i < 15) {
                    self.$el.find('div#filter').hide();
                }

            }
        },
        setOperatorsList: function (operators) {
            var self = this;
            self.$el.find('#rule-operator-list').html("");
            if (operators != undefined && !$.isEmptyObject(operators)) {
                if (Object.keys(operators).length == 1) {
                    _.each(operators, function (value, key) {
                        self.$el.find("#rule-operator-link").hide();
                        self.$el.find("#rule-operator-value").show();
                        self.$el.find("#rule-operator-value").text(value);

                    });
                } else {
                    self.$el.find("#rule-operator-link").show();
                    self.$el.find("#rule-operator-value").hide();
                    var i = 1;
                    _.each(operators, function (value, key) {
                        if (self.queryOperator != null) {
                            if (key == self.queryOperator) {
                                self.$el.find("#rule-operator-link").html(value);
                                self.onOperatorLinkSelection(null);
                            }
                        } else if (i == 1) {
                            self.$el.find("#rule-operator-link").html(value);
                            self.queryOperator = key;
                            self.onOperatorLinkSelection(null);
                        }

                        self.$el.find('#rule-operator-list')
                            .append($('<li>', { id: key })
                                .text(value));
                        i++;
                    });
                }
            } else {
                self.$el.find('#rule-operator-link').hide();
            }
        },
        setValuesList: function (collection) {
            var self = this;
            if (collection != undefined) {
                this.valuesList = true;
                this.$el.find("#rule-value-link").html("select value");

                var sortedValues = collection.sortBy("Index");

                _.each(sortedValues, function (model) {
                    var checked = "";
                    var key = model.get("Key");
                    var value = model.get("DisplayValue");
                    if (self.queryValue != null && _.indexOf(self.queryValue.split(";"), key) > -1) {
                        checked = " checked=\"checked\"";
                    }

                    if (value == "") {
                        value = "[" + key + "]";
                    }

                    if (self.cvlId != null) {
                        self.$el.find('#rule-value-list').append("<div id=\"" + key + "\" class=\"item\"><input type=\"checkbox\" name=\"" + self.querySystem + "\" id=\"" + key + "\" value=\"" + value + "\"" + checked + "/>" + value + "</div>");
                    } else {
                        self.$el.find('#rule-value-list').append($('<div>', { id: key, class: "item" })
                            .text(value));
                    }
                });
                if (this.queryValue != undefined) {
                    var i = this.$el.find("#rule-value-list input:checked");

                    var values = "";
                    $.each(i, function (index, value) {
                        values += value.value + ";";
                    });
                    if (values.length > 0) {
                        values = values.substring(0, values.length - 1);

                        this.$el.find("#rule-value-link").html(values);
                    }

                }
            }
        },
        setLanguagesList: function (values) {
            var self = this;
            if (values != undefined) {
                var i = 1;
                var dataLang = appHelper.userLanguage;
                _.each(values, function (value, key) {
                    if (i == 1 && dataLang == null) {
                        self.$el.find("#rule-language-link").html(value);
                        self.queryLanguage = key;
                    }
                    if (dataLang == key) {
                        self.$el.find("#rule-language-link").html(value);
                        self.queryLanguage = key;
                    }
                    self.$el.find('#rule-language-list')
                    .append($('<li>', { id: key })
                        .text(value));
                    i++;
                });
                this.$el.find("#rule-language-link").show();
                this.showLanguage = true;
            }
        },
        setSubSystemList: function () {
            var self = this;
            var values = {};

            this.collection = new fieldTypesCollection();
            this.collection.fetch({
                data: { id: self.entityType },
                success: function () {
                    _.each(self.collection.toJSON(), function (model) {
                        var obj = { field: model.displayname, categoryName: model.categoryname }
                        values[model.id] = obj;
                    });

                    self.setSystemList(values);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onOperatorLinkSelection: function (e) {
            if (e != null) {
                e.stopPropagation();
                this.deselect();
                this.queryOperator = e.currentTarget.id;
                this.$el.find("#rule-operator-link").html(e.currentTarget.innerHTML);
            }

            if (this.querySystem == "EntityType" && this.queryOperator == "Equals" && this.queryValue != "") {
                this.$el.find("#query-plus-row").removeClass("hidden");
            } else {
                this.$el.find("#query-plus-row").addClass("hidden");
            }
            if (this.conditionType == "Relation" && this.queryValue != "" && this.queryOperator == "NotEmpty") {
                this.$el.find("#query-relation-plus-row").first().removeClass("hidden");
            } else {
                this.$el.find("#query-relation-plus-row").first().addClass("hidden");
            }

            if (this.queryOperator == "Empty" || this.queryOperator == "NotEmpty") {
                this.$el.find("#rule-value-link").hide();
                if (this.showLanguage == true) {
                    this.$el.find("#rule-language-link").hide();
                }
            } else {
                if (this.showValue == true) {
                    this.$el.find("#rule-value-link").show();
                }
                if (this.showLanguage == true) {
                    this.$el.find("#rule-language-link").show();
                }
            }

            if (this.isBoolValue && this.queryOperator == "IsTrue") {
                this.queryValue = true;
            } else if (this.isBoolValue && this.queryOperator == "IsFalse") {
                this.queryValue = false;
            }
            else if (this.isBoolValue && (this.queryOperator == "Empty" || this.queryOperator == "NotEmpty")) {
                this.queryValue = "";
            }
        },
        onDelete: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.deleted = true;
            this.remove();
            this.unbind();
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(configureLinkRuleRowTemplate, { rowId: this.rowId }));

            this.setSubSystemList();

            this.$el.find("#rule-system-list").hide();
            this.$el.find("#rule-operator-list").hide();
            this.$el.find("#rule-language-list").hide();
            this.$el.find("#rule-value-list").hide();

            this.$el.find("#rule-value-input").hide();

            return this;
        }
    });

    return configureLinkRuleRowView;
});
