﻿define([
  'jquery',
  'underscore',
  'backbone',
  "multiple-select",
  'text!sharedtemplates/syndicate/syndicateSelectionControlTemplate.html'
], function ($, _, backbone, multipleSelect, syndicateSelectionControlTemplate) {
    var syndicateSelectionControlView = backbone.View.extend({
        initialize: function (options) {
            this.controlDefinition = options.ControlDefinition;
            this.id = options.ControlDefinition;
            if (options.Id !== undefined) {
                this.id += "-" + options.Id;
            }
            this.values = options.Values;
            this.multivalue = options.Multivalue;
            this.inputField = "#syndicate-select-value-" + this.id;
            this.possibleValues = options.PossibleValues;
            this.caption = options.Caption;
            
            this.render();
        },
        render: function () {
            var self = this;
            var template = _.template(syndicateSelectionControlTemplate, { Caption: this.caption, Definition: this.id });
            this.setElement(template, true);

            if (this.values != null) {
                // To be able to clear the selection
                var key = 0;
                var text = '(No selection)';

                self.$el.find(self.inputField).append("<option value=\"" + key + "\">" + text + "</option>");
                _.each(this.values.models, function (value) {
                    key = value.attributes.Id;
                    text = value.attributes.Name;

                    self.$el.find(self.inputField).append("<option value=\"" + key + "\">" + text + "</option>");
                });
            } else {
                self.$el.find(self.inputField).append("<option value=\"\" disabled>Please Select</option>");
            }

            this.multipleSelect = this.$el.find(self.inputField).multipleSelect({
                single: !this.multivalue,
                filter: true,
                width: 230,
                onBlur: function () {
                    self.onLostFocus();
                }
            });

            this.$el.find(self.inputField).multipleSelect("setSelects", _.isArray(this.possibleValues) ? this.possibleValues : [this.possibleValues]);

            return this;
        },
        onLostFocus: function (e) {
            if (e != null) {
                e.stopPropagation();
            }
        }
    });

    return syndicateSelectionControlView;
});

