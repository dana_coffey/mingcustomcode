﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'multiple-select'
], function ($, _, Backbone, bbf, multipleSelect) {

    var checkboxDropdownEditor = Backbone.Form.editors.Base.extend({
        initialize: function(options) {
            // Call parent constructor
            Backbone.Form.editors.Base.prototype.initialize.call(this, options);
        },
        events: {
            //'change': 'onChange',
            //'focus': function () {
            //    this.trigger('focus', this);
            //},
            //'blur': function () {
            //    this.trigger('blur', this);
            //},
            //'keyup': "onChange"
        
        },
        onChange: function() {
            this.trigger('change', this);
        },
        onFocus: function() {
            this.trigger('focus', this);
        },
        onBlur: function() {
            this.trigger('blur', this);
        },
        setValue: function(value) {
            if (!value) value = [];
            if (!_.isArray(value)) {
                value = value.split(";");
            }
            this.$el.find("select").multipleSelect("setSelects", value);
        },
        getValue: function() {
            var r = this.$el.find("select").multipleSelect("getSelects");
            if (this.options.schema.isMultivalue === false) {
                if (r.length > 1) throw "Invalid dropdown state";
                return r.length === 0 ? null : r[0];
            } else {
                return r;
            }
        },
        setOptions: function(options) {
            var self = this;

            //If a collection was passed, check if it needs fetching
            if (options instanceof Backbone.Collection) {
                var collection = options;

                //Don't do the fetch if it's already populated
                if (collection.length > 0) {
                    this.renderOptions(options);
                } else {
                    collection.fetch({
                        success: function(collection) {
                            self.renderOptions(options);
                        }
                    });
                }
            }
            //If a function was passed, run it to get the options
            else if (_.isFunction(options)) {
                options(function(result) {
                        self.renderOptions(result);
                    },
                    self);
            }
            //Otherwise, ready to go straight to renderOptions
            else {
                this.renderOptions(options);
            }
        },
        renderOptions: function(options) {
            var $select = this.$el.find("select");

            var html = this._getOptionsHtml(options);

            //Insert options
            $select.html(html);
            this.$el.find("select").multipleSelect("refresh");

            //Select correct option
            this.setValue(this.value);
        },
        _getOptionsHtml: function(options) {
            var html;
            //Accept string of HTML
            if (_.isString(options)) {
                html = options;
            }
            //Or array
            else if (_.isArray(options)) {
                html = this._arrayToHtml(options);
            }
            //Or Backbone collection
            else if (options instanceof Backbone.Collection) {
                html = this._collectionToHtml(options);
            } else if (_.isFunction(options)) {
                var newOptions;

                options(function(opts) {
                        newOptions = opts;
                    },
                    this);

                html = this._getOptionsHtml(newOptions);
                //Or any object
            } else {
                html = this._objectToHtml(options);
            }

            return html;
        },
        /**
         * Transforms a collection into HTML ready to use in the renderOptions method
         * @param {Backbone.Collection}
         * @return {String}
         */
        _collectionToHtml: function(collection) {
            //Convert collection to array first
            var array = [];
            collection.each(function(model) {
                array.push({ val: model.id, label: model.toString() });
            });

            //Now convert to HTML
            var html = this._arrayToHtml(array);

            return html;
        },
        /**
         * Transforms an object into HTML ready to use in the renderOptions method
         * @param {Object}
         * @return {String}
         */
        _objectToHtml: function(obj) {
            //Convert object to array first
            var array = [];
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    array.push({ val: key, label: obj[key] });
                }
            }

            //Now convert to HTML
            var html = this._arrayToHtml(array);

            return html;
        },


        /**
         * Create the <option> HTML
         * @param {Array}   Options as a simple array e.g. ['option1', 'option2']
         *                      or as an array of objects e.g. [{val: 543, label: 'Title for object 543'}]
         * @return {String} HTML
         */
        _arrayToHtml: function(array) {
            var html = [];

            //Generate HTML
            _.each(array,
                function(option) {
                    if (_.isObject(option)) {
                        if (option.group) {
                            html.push('<optgroup label="' + option.group + '">');
                            html.push(this._getOptionsHtml(option.options))
                            html.push('</optgroup>');
                        } else {
                            var val = (option.val || option.val === 0) ? option.val : '';
                            html.push('<option value="' + val + '">' + option.label + '</option>');
                        }
                    } else {
                        html.push('<option>' + option + '</option>');
                    }
                },
                this);

            return html.join('');
        },
        render: function() {
            var that = this;

            var hasOnlyShortLabels = this.schema.options
                .length >
                5; // If this is set to true, we will display items in a grid for a better user experience
            var selEl = $("<select style='width: 300px; height: 10px;'></select>");
            _.each(this.schema.options,
                function(option) {
                    var optionEl = $("<option></option>")
                        .attr("value", option.val)
                        .text(option.label);
                    selEl.append(optionEl);
                    if (option.label.length > 5) {
                        hasOnlyShortLabels = false;
                    }
                });

            this.$el.append(selEl);

            this.$el.find("select")
                .multipleSelect({
                    placeholder: "(not set)",
                        single: this.options.schema.isMultivalue === false,
                        filter: true,
                        selectAll: true,
                        maxHeight: 200,
                        multiple: hasOnlyShortLabels,
                        onClick: function() {
                            that.onChange();
                        },
                        onCheckAll: function() {
                            that.onChange();
                        },
                        onUncheckAll: function() {
                            that.onChange();
                        },
                        onFocus: function() {
                            that.onFocus();
                        },
                        onBlur: function() {
                            that.onBlur();
                        },
                        styler: function (value) {
                            var tmp = _.findWhere(that.schema.options, { val: value });
                            if (tmp && tmp.isValid === false) {
                                return 'color: #bb7777;';
                            }
                        }
                });
          
            this.setValue(this.value);

            // Read only?
            if (this.schema.editorAttrs && this.schema.editorAttrs.readonly) {
                this.$el.find("li input").attr("disabled", true);
                this.$el.find(".ms-drop").css("background-color", "#f4f4f4");
                this.$el.find(".ms-parent button").css("background-color", "#f4f4f4");
                this.$el.find(".ms-parent button").css("border", "1px solid #ddd");
            }

            return this;
        }
    });


    return checkboxDropdownEditor;

});
