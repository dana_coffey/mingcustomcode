define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, backbone) {
    var linkRuleCollection = backbone.Collection.extend({
        initialize: function (options) {
            this.entityId = options.entityid;
        },
        url: function () {
            return '/api/linkrule/';
        }
    });

    return linkRuleCollection;
});
