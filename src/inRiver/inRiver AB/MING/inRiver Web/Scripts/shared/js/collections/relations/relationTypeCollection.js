define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/relation/relationTypeModel'
], function ($, _, backbone, relationTypeModel) {
    var relationTypeCollection = backbone.Collection.extend({
        model: relationTypeModel,
        initialize: function (models, options) {
            this.entityTypeId = options.entityTypeId;
            this.direction = options.direction;
        },
        url: function () {
            return '/api/relationtype?entityTypeId=' + this.entityTypeId + '&direction=' + this.direction;
        }
    });

    return relationTypeCollection;
});
