define([], function () {

    var appItems = [
        { url: "/app/portal", name: "Portal", icon: "fa fa-th", permission: "" },
        { url: "/app/supply", name: "Supply", icon: "fa fa-arrow-circle-o-right", permission: "inRiverSupply" },
        { url: "/app/enrich", name: "Enrich", icon: "fa fa-edit", permission: "inRiverEnrich" },
        { url: "/app/planrelease", name: "Plan & Release", icon: "fa fa-paperclip", permission: "inRiverPlanAndRelease" },
        { url: "/app/publish", name: "Publish", icon: "fa fa-arrow-circle-o-right", permission: "inRiverPublish" },
        { url: "/app/planner", name: "Planner", permission: "inRiverCampaignPlanner", iconImage: "/Images/planner.png" },
        //{ url: "/app/Media", name: "Media Browser", icon: "fa fa-search" },
        { url: "/app/print", name: "Print", icon: "fa fa-print", permission: "inRiverPrint" },
        { url: "/app/system", name: "System", icon: "fa fa-cog", permission: "Administrator" },
        { url: "/app/contentstore", name: "Content Store", permission: "ContentStore", iconImage: "/Images/contentstore.png" },
        { url: "/app/supplieronboarding", name: "Supplier Onboarding", permission: "SupplierOnboarding", iconImage: "/Images/supplieronboarding.png" }
    ];

    return appItems;
});