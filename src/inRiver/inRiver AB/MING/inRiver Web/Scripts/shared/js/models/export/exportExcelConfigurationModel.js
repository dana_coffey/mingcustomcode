﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var exportExcelConfigurationModel = Backbone.DeepModel.extend({
        idAttribute: "name",
        initialize: function (option) {
            this.name = option.name;
        },
        url: function() {
            return '/api/exportexcel/name/' + this.name;
        }
    });

    return exportExcelConfigurationModel;

});