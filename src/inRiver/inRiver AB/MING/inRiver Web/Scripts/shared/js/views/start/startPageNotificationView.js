define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/advancedsearch/queryEditorView',
  'text!sharedtemplates/start/startpageNotificationTemplate.html'
], function ($, _, Backbone, inRiverUtil, modalPopup, contextMenuView, queryEditorView, startpageNotificationTemplate) {

    var startPageTaskView = Backbone.View.extend({
        initialize: function (data) {
            this.model = data.model;

        },
        events: {
            "click #delete": "onDelete",
            "click #edit": "onEdit"
        },
        onDeleteConfirm: function (self) {
            self.model.destroy();
            self.remove();
            inRiverUtil.Notify("Notification successfully removed");
        },
        onDelete: function () {
            var self = this;
            inRiverUtil.NotifyConfirm("Confirm remove notification", "Do you want to remove this notification?", this.onDeleteConfirm, self);
        },
        onEdit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.modal = new modalPopup();
            this.modal.popOut(new queryEditorView({
                notification: this.model,
            }), { size: "medium", usesavebutton: false });
        },

        render: function () {

            if (this.model == null) {
                return this;
            }

            this.$el.html(_.template(startpageNotificationTemplate, { data: this.model.toJSON() }));
            var tools = new Array();
            tools.push({ name: "edit", title: "Edit notification", icon: "fa-pencil fa", text: "Update" });
            tools.push({ name: "delete", title: "Delete notification", icon: "fa-trash-o fa", text: "Delete" });

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: tools }).$el);
            return this;
        }
    });

    return startPageTaskView;
});

