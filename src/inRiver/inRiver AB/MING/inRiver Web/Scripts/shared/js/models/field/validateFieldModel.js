﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var validateFieldModel = backbone.Model.extend({
        initialize: function (options) {
            this.fieldTypeId = options.FieldTypeId;
            this.value = options.FieldData;
        },
        url: function() {
            return '/api/validateuniquefields/' + this.fieldTypeId + "?value="+ this.value;
        }
    });

    return validateFieldModel;

});