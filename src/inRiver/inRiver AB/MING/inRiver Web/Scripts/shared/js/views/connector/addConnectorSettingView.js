define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-modal',
    'alertify',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/connector/connectorSettingModel',
], function ($, _, backbone, modal, alertify, inRiverUtil, settingModel) {

    var AddSettingView = backbone.View.extend({
        initialize: function (data) {
            this.undelegateEvents();
            this.parent = data.parent;
            this.model = new settingModel();

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'popupcancel', this.close);
            this.listenTo(appHelper.event_bus, 'popupsave', this.save);

            this.render();
            //_.bindAll(this, "onContentChanged");
        },
        events: {
            "click #save-form-button": "save",
            "click": "changed",
            "change": "changed",
        },
        close: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        save: function () {
            var errors = this.form.commit(); // runs schema validation

            if (!errors) {
                var self = this;
                this.model.url = '/api/connectorsetting/' + this.parent.model.get("Id");
                this.model.save({
                    Key: this.form.fields.Key.editor.getValue(),
                    Value: this.form.fields.Value.editor.getValue()
                }, {
                    success: function () {
                        self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                        self.parent.collection.fetch();
                        inRiverUtil.Notify("Setting successfully added");
                        self.close();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    wait: true
                });
            }
        },
        onContentChanged: function () {
            this.save();
        },
        render: function () {
            var self = this;

            //this.$el.html(_.template(lightboardPreviewTemplate, { resources: this.collection.toJSON() }));

            this.form = new backbone.Form({
                model: this.model
            });

            // Create a modal view class
            /*var Modal = Backbone.Modal.extend({
                template: _.template(modalTemplate),
                cancelEl: '#closeModal',
            });

            // Render an instance of your modal
            var modalView = new Modal();
            this.$el.html(modalView.template({ data: "Add Setting" }));
            this.$el.html(modalView.render().el);

            this.$el.find(".modal_title").html("<h2>Add Setting</h2>");
            var $buttonAdd = $('<button class="btn btn-primary" type="button" name="save" id="addSetting">Add</button>');
            this.$el.find(".modal_bottombar").prepend($buttonAdd);*/
            this.$el.append(this.form.render().el);

            // Remove disabled when add
            this.$el.find("input[name='Key']").removeAttr("disabled");
            
            return this;
        }
    });

    return AddSettingView;
});
