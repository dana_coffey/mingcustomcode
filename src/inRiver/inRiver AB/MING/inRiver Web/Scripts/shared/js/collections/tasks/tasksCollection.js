define([
  'jquery',
  'underscore',
  'backbone',
  'models/task/taskModel'
], function ($, _, Backbone, taskModel) {
    var tasksCollection = Backbone.Collection.extend({
        model: taskModel
    });

    return tasksCollection;
});
