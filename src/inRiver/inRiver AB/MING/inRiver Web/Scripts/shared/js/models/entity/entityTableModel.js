﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var entityTableModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/entitytable',
        
    });

    return entityTableModel;

});
