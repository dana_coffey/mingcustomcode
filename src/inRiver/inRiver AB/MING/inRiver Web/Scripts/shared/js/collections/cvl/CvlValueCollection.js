define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/cvl/CvlValueModel'
], function ($, _, Backbone, CvlValueModel) {
    var CvlValueCollection = Backbone.Collection.extend({
        model: CvlValueModel,
        initialize: function (models, options) {
            this.cvlId = options.cvlId;
            this.parentCvlValueId = options.parentCvlValueId;
        },
        url: function () {
            if (this.parentCvlValueId) {
                return '/api/cvlvalue/' + this.cvlId + "?subId=" + this.parentCvlValueId;
            }

            return '/api/cvlvalue/' + this.cvlId;
        },
    });

    return CvlValueCollection;
});
