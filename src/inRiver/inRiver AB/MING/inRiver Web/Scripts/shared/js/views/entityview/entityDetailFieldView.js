﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inputControlUtil',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/fieldtype/remoteFieldTypeModel',
  'sharedjs/models/field/conflictedFieldModel',
  'sharedjs/views/fieldrevision/fieldRevisionView',
  'sharedjs/views/inputcontrols/noneEditModeControlView',
  'sharedjs/misc/permissionUtil',
  'text!sharedtemplates/entity/entityDataFieldTemplate.html',
  'text!sharedtemplates/inputcontrols/historyControlTemplate.html',
  'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, modalPopup, inputControlUtil, fieldModel, remoteFieldTypeModel, conflictedFieldModel, fieldRevisionView, noneEditModeControlView, permissionUtil, entityDataFieldTemplate, historyControlTemplate, inRiverUtil) {

    var entityDetailFieldView = Backbone.View.extend({
        initialize: function (options) {
            this.field = options.field;

            this.modifield = false;
            this.previousField = options.previousField;
            this.textValue = "";
            this.render();

            this.saveAll = false;
            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'fieldtabpressed', this.tabPressed);
            this.listenTo(window.appHelper.event_bus, 'fieldbacktabpressed', this.backTabPressed);
            this.listenTo(window.appHelper.event_bus, 'fieldeditmode', this.openEditModeByMouseClick);
            this.listenTo(window.appHelper.event_bus, 'fieldvaluenotupdated', this.fieldValueNotUpdated);
            this.listenTo(window.appHelper.event_bus, 'fieldvaluesave', this.onSave);
            this.listenTo(window.appHelper.event_bus, 'closepopup', this.onHistoryClose);
            this.listenTo(window.appHelper.event_bus, "saveAllEntityFields", this.onSaveAll);
        },
        events: {
            "click #view-history-button": "onViewHistoryClick"
        },
        onViewHistoryClick: function (e) {
            window.appSession.historyset = true;
            e.stopPropagation();
            e.preventDefault();
            var modal = new modalPopup();
            modal.popOut(new fieldRevisionView({ entityId: this.field.EntityId, fieldTypeId: this.field.FieldType, fieldDataType: this.field.FieldDataType }), { size: "small", header: "Field History" });
        },
        onHistoryClose: function () {
            if (this.control) {
                window.appSession.historyset = false;
                this.$el.find(this.control.inputField).focus();
            }
        },
        onSaveAll: function (entityId) {
            if (this.field.EntityId != entityId) {
                return;
            }
            if (this.control && this.control.fieldValue != this.control.defaultValue && this.control.okToSave == true) {
                this.saveAll = true;
                this.onSave(this.control.fieldTypeId, this.control.fieldValue, entityId);
            }
        },
        onSave: function (fieldTypeId, fieldvalue, entityId) {
            var self = this;
            if (this.field.EntityId != entityId) {
                return;
            }

            if (this.field.FieldType == fieldTypeId) {

                this.conflictedFieldModel = new conflictedFieldModel({ id: this.field.EntityId, subId: this.field.FieldType });
                this.conflictedFieldModel.attributes = this.field;
                this.conflictedFieldModel.url = '/api/validateentityfields/' + this.field.EntityId + '/' + this.field.FieldType;
                this.conflictedFieldModel.save({}, {
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    success: function (model, response) {
                        if (response.Result) {
                            var fieldType = self.field.FieldType;
                            var onSuccess = function (field) {
                                self.$el.find("#value").text(field.DisplayValue);
                                window.appHelper.fieldsToSave--;
                                self.field = field;
                                self.render();
                                inRiverUtil.Notify("Field has been updated");
                                window.appHelper.event_bus.trigger("entityupdated", field.EntityId);
                         
                            };

                            var val = fieldvalue;

                            if (self.field.FieldDataType != "LocaleString") {
                                $.ajax({
                                    type: "POST",
                                    url: "/api/entityfields/" + entityId + "/" + fieldType,
                                    data: { value: val },
                                    success: onSuccess,
                                    error: function (model, response) {
                                        inRiverUtil.OnErrors(model, response);
                                    },
                                    dataType: "json"
                                });
                            } else {
                                self.field.Value = JSON.stringify(val);

                                $.ajax({
                                    type: "POST",
                                    url: "/api/entityfields/" + entityId + "/" + fieldType,
                                    data: self.field,
                                    success: onSuccess,
                                    error: function (model, response) {
                                        inRiverUtil.OnErrors(model, response);
                                    },
                                    dataType: "json"
                                });
                            }
                        } else {
                            inRiverUtil.NotifyError("Field already update", "This field has been updated after it was loaded.<br> Please use Save all to resove conflict", null);
                        }
                    }

                });
            }
        },
        tabPressed: function (fieldTypeId) {
            if (this.previousField.FieldType == fieldTypeId && this.previousField != this.field) {
                if (!this.field.IsReadOnly) {
                    this.openEditMode(this.field.FieldType);
                }
            }
        },
        backTabPressed: function (fieldTypeId) {
            if (this.field.FieldType == fieldTypeId) {
                if (!this.field.IsReadOnly) {
                    this.openEditMode(this.field.FieldType);
                }
            }
        },
        fieldValueNotUpdated: function (fieldTypeId) {
            if (this.field.FieldType == fieldTypeId) {
                this.noEditControl = new noneEditModeControlView({ Id: this.field.FieldType, Value: this.field.DisplayValue, ReadOnly: this.field.IsReadOnly });
                this.appendControl(this.noEditControl);
            }
        },
        openEditMode: function (fieldTypeId) {
            if (this.field.IsReadOnly) {
                return;
            }
            if ($("#value-" + this.field.FieldType).length) {
                if (this.field.FieldType == fieldTypeId) {
                    this.noEditControl.close();
                    this.control = inputControlUtil.createEditControl(this.field, this.field.FieldDataType, this.field.FieldType, this.previousField.FieldType);
                    this.appendControl(this.control);
                }
            }
        },
        openEditModeByMouseClick: function (fieldTypeId) {
            if (this.field.IsReadOnly) {
                return;
            }
            if (this.field.FieldType == fieldTypeId) {
                this.noEditControl.close();
                this.control = inputControlUtil.createEditControl(this.field, this.field.FieldDataType, this.field.FieldType, this.previousField.FieldType);
                this.appendControl(this.control);
                this.$el.find(this.control.focusField).focus();
            }
        },
        appendControl: function (control) {
            this.$el.find("#field-wrapper-" + this.field.FieldType + " .value-placeholder").html(control.render().el);
        },
        appendHistoryControl: function () {
            var historyId = null;
            if (this.field.Revision > 0) {
                historyId = this.field.FieldType;
            }
            this.$el.find(".history-wrapper").append(_.template(historyControlTemplate, {
                historyId: historyId,
                type: this.field.FieldDataType
            }));
        },
        render: function () {
            this.$el.html(_.template(entityDataFieldTemplate, {
                Id: this.field.FieldType,
                Name: this.field.FieldTypeDisplayName,
                Value: this.field.DisplayValue,
                ExcludeFromDefaultView: this.field.ExcludeFromDefaultView,
                Description: this.field.FieldTypeDescription
            }));

            var displayValue = this.field.DisplayValue;
            if (this.field.IsMultiValue && displayValue != null) {
                displayValue = displayValue.toString().replace(/,/g, ', ');
            }

            var that = this;
            
            that.noEditControl = new noneEditModeControlView({ Id: that.field.FieldType, Value: displayValue, ReadOnly: that.field.IsReadOnly });
            that.appendControl(that.noEditControl);
            that.appendHistoryControl();

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#edit-value-icon"));

            return this; // enable chained calls
        }
    });

    return entityDetailFieldView;
});
