define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/connectorevent/connectorEventSessionModel'
], function ($, _, backbone, connectorEventSessionModel) {
    var connectorEventSessionCollection = backbone.Collection.extend({
        model: connectorEventSessionModel,
        url: '/api/connectoreventsession/'
    });

    return connectorEventSessionCollection;
});
