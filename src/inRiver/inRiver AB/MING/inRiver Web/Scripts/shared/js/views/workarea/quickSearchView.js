﻿define([
  'jquery',
  'jquery-ui',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/advancedsearch/queryEditorView',
  'text!sharedtemplates/workarea/quicksearchTemplate.html'
], function ($, jqueryui, _, Backbone, modalPopup, inRiverUtil, queryEditorView, quicksearchTemplate) {

    var quickSearchView = Backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        events: {
            "click #quicksearch": "quicksearch",
            "keyup #quicksearchquery": "keyPressEventHandler",
            "click #edit-query": "editQuery",
        },
        editQuery: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var that = this;
            this.modal = new modalPopup();

            this.modal.popOut(new queryEditorView({searchOrigin: "top" }), { size: "medium"});
            this.listenTo(appHelper.event_bus, 'popupclose', this.closePopup);
        },
        quicksearch: function () {
            var qsquery = $("#quicksearchquery").val();
           // this.goTo("workareasearch?title=Query&searchPhrase=" + qsquery);
            this.goTo("workarea?title=Query&searchPhrase=" + qsquery);
        },
        keyPressEventHandler: function (event) {
            if (event.keyCode == 13) {

                if (this.searchHandled) {
                    this.searchHandled = false;
                    return;
                }

                this.quicksearch();

                $(".ui-autocomplete").hide();
            }
        },
        render: function () {

            var self = this;

            this.$el.html(quicksearchTemplate);

            this.$el.find("#quicksearchquery").focus();

            this.$el.find("#quicksearchquery").autocomplete({
                minLength: 2,
                source: function (request, response) {
                    $.ajax({
                        url: '/api/tools/generateautosearch',
                        type: 'GET',
                        cache: false,
                        data: { id: $("#quicksearchquery").val() },
                        dataType: 'json',
                        success: function (json) {
                            response(json);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                focus: function (event, ui) {

                    return false;
                },
                select: function (event, ui) {
                    self.searchHandled = true;

                    $.post("/api/tools/compress", { '': ui.item.Id }).done(function (result) {
                        self.goTo("workareasearch?title=" + ui.item.Name + "&compressedEntities=" + result);
                    });
                    return false;
                },
            }).autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a><div class='quickSearchItemContainer'><div><img  class='quickSearchItemContainerImage' src='" + item.PictureUrl + "'/></div><div class='quickSearchItemContainerName'><img src='../icon?id=" + item.EntityType + "'/><span>" + item.Name + "</span></div><div class='quickSearchItemContainerDescription'>" + item.Description + "</div></div></a>")
                    .appendTo(ul);
            };

            return this;
        }
    });

    return quickSearchView;

});
