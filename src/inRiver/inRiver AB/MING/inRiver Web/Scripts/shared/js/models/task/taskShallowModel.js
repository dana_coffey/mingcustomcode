﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var taskShallowModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/taskShallow',
        
    });

    return taskShallowModel;

});
