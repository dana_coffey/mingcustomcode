define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/connector/connectorSettingModel'
], function ($, _, backbone, connectorSettingModel) {
    var connectorSettingsCollection = backbone.Collection.extend({
        model: connectorSettingModel,
      initialize: function (options) {
          this.id = options.id;
      },
      url: function () {
          return '/api/connectorsetting/' + this.id;
      }
    });
 
  return connectorSettingsCollection;
});
