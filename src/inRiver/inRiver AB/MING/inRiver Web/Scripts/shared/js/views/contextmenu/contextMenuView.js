﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/permissionUtil',
  'sharedjs/collections/apps',
  'text!sharedtemplates/contextmenu/contextMenuTemplate.html',
  'text!sharedtemplates/contextmenu/toolsItem.html'

], function ($, _, Backbone, alertify, permissionUtil,apps, contextMenuTemplate, toolsItem) {

    var contextMenuView = Backbone.View.extend({

        initialize: function (options) {
            this.tools = options.tools;
            this.id = options.id;
            this.linkId = options.linkId;
            this.render();
        },
        events: {
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "click .card-tools-header i": "onToggleTools",
        },
        deselect: function (e) {
            e.stopPropagation(); 
            var $box = this.$el.find("#card-tools-container");
            $box.hide();
        },
        onToggleTools: function (e) {
            e.stopPropagation();

            var $box = this.$el.find("#card-tools-container");
            var visible = $box.is(":visible");
            $(".card-tools-container").hide(200);
            if (!visible) {
              $box.show(200);
            }
            $(document).one('click', this.deselect.bind(this));
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(contextMenuTemplate, { id: this.id }));
            var container = this.$el.find("#tools");
            _.each(this.tools, function (tool) {
                if (!(tool.permission && permissionUtil.CheckPermission(tool.permission) == false)) {
                    container.append(_.template(toolsItem, { data: tool, id: that.id, linkId: that.linkId }));
                } 
            });

            if (container.length == 1) {
                if (container[0].innerHTML == "") {
                    container.append("<span></span>");
                }
            }

            return this;
        }
    });

    return contextMenuView;
});
