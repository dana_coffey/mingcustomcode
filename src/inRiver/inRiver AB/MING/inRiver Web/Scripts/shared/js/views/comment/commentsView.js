﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/comment/commentModel',
    'sharedjs/collections/comment/commentsCollection',
    'text!sharedtemplates/comment/commentContainerTemplate.html',
    'text!sharedtemplates/comment/commentThreadMessageTemplate.html',
    'sharedjs/models/user/UserInformationModel',
     'sharedjs/misc/permissionUtil'
], function ($, _, backbone, inRiverUtil, commentModel, commentsCollection, commentContainerTemplate, commentThreadMessageTemplate, userInformationModel, permissionUtil) {
    var commentsView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            self.entityId = options.id;

            //Check market field for selected entity.
            self.marketField = options.marketField;

            var currentUser = new userInformationModel();
            currentUser.fetch({
                success: function (model, response) {
                    var author = response.Firstname + " " + response.Lastname + " (" + response.Username + ")";
                    self.Author = author;

                    self.CommentsCollection = new commentsCollection({ id: self.entityId });
                    self.CommentsCollection.fetch({
                        success: function () {
                            self.render();
                        }
                    });
                },
                error: function (model, errorMessage) {
                    inRiverUtil.OnErrors(model, errorMessage);
                }
            });
        },
        events: {
            "click #comment-response-submit": "onSubmitComment",
            "click #comment-response-cancel": "onCancelComment",
            "click #comment-delete-thread": "onDeleteThreadComment",
            "click #show-all-comments": "showAllComments",
            "change #comment-market-dropdown": "onMarketSelection"
        },
        replaceAll: function (str, find, replace) {
            return str.replace(new RegExp(find, 'g'), replace);
        },
        onSubmitComment: function () {
            var self = this;

            var submitComment = $(this.el).find("#comment-response-container textarea")[0].value;
            if (submitComment == "") {
                inRiverUtil.Notify("Please add a comment before save.");
                return;
            }
            submitComment = this.replaceAll(submitComment, "\n", "\r\n");

            if (self.marketField) {

                var selectedMarket = $('#comment-market-dropdown').val();

                if (selectedMarket != null && selectedMarket != "General") {
                    submitComment = selectedMarket + ":" + submitComment;
                }

                var model = new commentModel({ EntityId: self.entityId, Author: self.Author, Text: submitComment });
                model.save([],
                {
                    success: function(response) {

                        self.CommentsCollection.add(response, { at: 0 });

                        var washedText = self.washTextComments(response.attributes.Text);

                        if (selectedMarket != null && selectedMarket != "General") {
                            if (selectedMarket == washedText.substring(0, 2)) {
                                washedText = washedText.substring(3);
                            }
                        }

                        var dateSplitterValue = response.attributes.Created.indexOf(".");
                        var date = response.attributes.Created.substring(0, dateSplitterValue);
                        response.attributes.Created = date;

                        var washedTimeCreated = self.washTimeCreated(response.attributes.Created);

                        var threadElement = _.template(commentThreadMessageTemplate,
                        {
                            username: response.attributes.Author,
                            text: washedText,
                            created: washedTimeCreated,
                            commentId: response.attributes.Id
                        });

                        var threadsContainer = self.template.find("#comment-threads-container");

                        if ($(threadsContainer.has(".comment-user-thread-container")).length == 0) {
                            threadsContainer.append(threadElement);
                        } else {
                            $(threadElement).insertBefore($(self.el).find(".comment-user-thread-container").first());
                        }

                        self.clickEventForComments(response.attributes.Id);

                        var textAreaElement = $(self.el).find("#comment-response-container textarea");
                        $(textAreaElement).empty();
                        $(textAreaElement).focus();

                    },
                    error: function(result, errorMessage) {
                        inRiverUtil.OnErrors(result, errorMessage);
                    }
                });
            } else {

                var model = new commentModel({ EntityId: self.entityId, Author: self.Author, Text: submitComment });

                model.save([],
                {
                    success: function(response) {
                        var washedText = self.washTextComments(response.attributes.Text);

                        var dateSplitterValue = response.attributes.Created.indexOf(".");
                        var date = response.attributes.Created.substring(0, dateSplitterValue);
                        response.attributes.Created = date;

                        var washedTimeCreated = self.washTimeCreated(response.attributes.Created);

                        var threadElement = _.template(commentThreadMessageTemplate,
                        {
                            username: response.attributes.Author,
                            text: washedText,
                            created: washedTimeCreated,
                            commentId: response.attributes.Id
                        });

                        var threadsContainer = self.template.find("#comment-threads-container");

                        if ($(threadsContainer.has(".comment-user-thread-container")).length == 0) {
                            threadsContainer.append(threadElement);
                        } else {
                            $(threadElement).insertBefore($(self.el).find(".comment-user-thread-container").first());
                        }

                        self.clickEventForComments(response.attributes.Id);

                        var textAreaElement = $(self.el).find("#comment-response-container textarea");
                        $(textAreaElement).val("");
                        $(textAreaElement).focus();

                        $("#comment-threads-container p").remove();


                    },
                    error: function(result, errorMessage) {
                        inRiverUtil.OnErrors(result, errorMessage);
                    }
                });
            }

        },
        onCancelComment: function () {
            window.appHelper.event_bus.trigger('closepopup');
        },
        onDeleteThreadComment: function () {
            var self = this;

            var comments = new commentModel({ Id: "" });
            comments.url = "/api/comment/deleteall/" + self.entityId;
            inRiverUtil.NotifyConfirm("Delete all comments", "This will remove all comments.<br><br>Are you sure?", function() {
                comments.destroy({
                    success: function () {
                        self.CommentsCollection.reset();

                        self.template.find("#comment-threads-container").empty();
                        self.template.find("#comment-showall-container").empty();
                    },
                    error: function (model, errorMessage) {
                        inRiverUtil.OnErrors(model, errorMessage);
                    }
                });
            });
        },
        clickEventForComments: function (id) {
            var self = this;
            this.$el.find("#delete-" + id).click({ id: id }, function (event) {

                inRiverUtil.NotifyConfirm("Confirm delete", "Are you sure you want to delete the comment?", function() {
                    var model = new commentModel({ Id: "" });
                    model.url = "/api/comment/delete/" + event.data.id;

                    model.destroy({
                        success: function () {
                            self.CommentsCollection.remove(id);
                            self.template.find("#thread-" + event.data.id).remove();
                        },
                        error: function (result, errorMessage) {
                            inRiverUtil.OnErrors(result, errorMessage);
                        }
                    });
                });
                this.$el.find("#text-" + id).click({ id: id }, function (event) {
                    var msg = event.target.textContent;
                    var textAreaElement = $(self.el).find("#comment-response-container textarea");
                    textAreaElement[0].value = msg;
                });
            });
        },
        showAllComments: function () {
            var self = this;

            self.template.find("#comment-threads-container").empty();
            self.template.find("#comment-showall-container").empty();

            _.each(this.CommentsCollection.models, function (model) {
                var washedText = self.washTextComments(model.attributes.Text);
                var washedTimeCreated = self.washTimeCreated(model.attributes.Created);
                var threadElement = _.template(commentThreadMessageTemplate, { username: model.attributes.Author, text: washedText, created: washedTimeCreated, commentId: model.attributes.Id });

                self.clickEventForComments(model.attributes.Id);
                self.template.find("#comment-threads-container").append(threadElement);
            });
        },
        onMarketSelection: function (e) {
            var self = this;
            var selectedMarket = $('#comment-market-dropdown').val();

            this.template.find("#comment-threads-container").empty();
            this.template.find("#comment-showall-container").empty();

            var markets = self.marketField.Value;

            var index = 0;

            _.every(this.CommentsCollection.models, function (model) {

                index++;

                var washedText = self.washTextComments(model.attributes.Text);
                var washedTimeCreated;
                var threadElement;

                if (selectedMarket == "General") {

                    washedTimeCreated = self.washTimeCreated(model.attributes.Created);

                    threadElement = _.template(commentThreadMessageTemplate, { username: model.attributes.Author, text: washedText, created: washedTimeCreated, commentId: model.attributes.Id });
                    self.template.find("#comment-threads-container").append(threadElement);
                    self.clickEventForComments(model.attributes.Id);

                    if (index == 10) {
                        self.template.find("#comment-showall-container").append("<div id='show-all-comments'>Show all comments</div>");
                    }

                } else {
                    if (selectedMarket == washedText.substring(0, 2)) {
                        washedText = washedText.substring(3);
                        washedTimeCreated = self.washTimeCreated(model.attributes.Created);

                        threadElement = _.template(commentThreadMessageTemplate, { username: model.attributes.Author, text: washedText, created: washedTimeCreated, commentId: model.attributes.Id });
                        self.template.find("#comment-threads-container").append(threadElement);
                        self.clickEventForComments(model.attributes.Id);

                        if (index == 10) {
                            self.template.find("#comment-showall-container").append("<div id='show-all-comments'>Show all comments</div>");
                        }
                    }
                }

                return index !== 10;
            });
        },
        washTextComments: function (text) {

            if (text == "") {
                return text;
            }

            if (text.indexOf("<") >= 0) {
                text = text.replace(/</g, "&lt;");
            }

            if (text.indexOf(">") >= 0) {
                text = text.replace(/>/g, "&gt;");
            }

            if (text.indexOf("\r\n") >= 0) {
                text = text.replace(/\r\n/g, "<br />");
            }

            return text;
        },
        washTimeCreated: function (created) {

            if (created.indexOf("T") >= 0) {
                created = created.replace("T", " ");
            }

            return created;
        },
        render: function() {
            var self = this;

            this.template = this.$el.html(_.template(commentContainerTemplate, { market: self.marketField }));

            permissionUtil.CheckPermissionForElement("AddComments", this.$el.find("#comment-response-submit"));
            permissionUtil.CheckPermissionForElement("DeleteComments", this.$el.find("#comment-delete-thread"));

            //Removes cancel if no permission for addcomments AND deletecomments
            if (!permissionUtil.CheckPermission("AddComments") && !permissionUtil.CheckPermission("DeleteComments")) {
                this.$el.find("#comment-response-cancel").hide();
            }

            //Hides textarea and submit if no permission for addcomments
            if (!permissionUtil.CheckPermission("AddComments")) {
                this.$el.find("#comment-response-container").hide();
                this.$el.find("#comment-response-submit").hide();
            }


            var index = 0;

            if (this.CommentsCollection.models.length === 0) {
                self.template.find("#comment-threads-container").append("<p>No comments available</p>");
            } 

            $("textarea").focus();
            if (self.marketField) {
                var selectedMarket = $('#comment-market-dropdown').val();

                _.every(this.CommentsCollection.models, function (model) {

                    index++;

                    var washedText = self.washTextComments(model.attributes.Text);
                    var washedTimeCreated = self.washTimeCreated(model.attributes.Created);

                    if (selectedMarket == "General") {
                        var washedTimeCreated = self.washTimeCreated(model.attributes.Created);

                        var threadElement = _.template(commentThreadMessageTemplate, { username: model.attributes.Author, text: washedText, created: washedTimeCreated, commentId: model.attributes.Id });
                        self.template.find("#comment-threads-container").append(threadElement);
                        self.clickEventForComments(model.attributes.Id);

                        if (index == 10) {
                            self.template.find("#comment-showall-container").append("<div id='show-all-comments'>Show all comments</div>");
                        }
                    }

                    return index !== 10;
                });
            } else {
                _.every(this.CommentsCollection.models, function (model) {

                    index++;

                    var washedText = self.washTextComments(model.attributes.Text);
                    var washedTimeCreated = self.washTimeCreated(model.attributes.Created);

                    var threadElement = _.template(commentThreadMessageTemplate, { username: model.attributes.Author, text: washedText, created: washedTimeCreated, commentId: model.attributes.Id });
                    self.template.find("#comment-threads-container").append(threadElement);
                    self.clickEventForComments(model.attributes.Id);

                    if (index == 10) {
                        self.template.find("#comment-showall-container").append("<div id='show-all-comments'>Show all comments</div>");
                    }

                    return index !== 10;
                });
            }

            return this;
        },
    });

    return commentsView;
});
