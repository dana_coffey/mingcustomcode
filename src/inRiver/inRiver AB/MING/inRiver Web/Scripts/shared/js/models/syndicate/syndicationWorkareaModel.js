﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var syndicationWorkareaModel = backbone.DeepModel.extend({
        initialize: function () {
        },
        urlRoot: '/api/syndication/workarea/distributionformat/'
    });

    return syndicationWorkareaModel;

});
