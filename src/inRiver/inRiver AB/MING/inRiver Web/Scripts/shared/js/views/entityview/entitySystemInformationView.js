﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'text!sharedtemplates/entity/entitySystemInformationTemplate.html'
], function ($, _, backbone, alertify, entitySystemInformationTemplate) {

    var entitySystemInformationView = backbone.View.extend({
        // This view operates without a collection
        //template: _.template($("#entity-settings-template").html()),
        initialize: function (options) {

            this.model = options.model; 
            this.render();
            
        },
        events: {
            "click #close-icon": "onClose",
        },
        onClose: function () {
            appHelper.event_bus.trigger('closepopup');
        },
        done: function () {
            $("#modalWindow").hide();
            this.remove();
        },
        render: function () {
            this.$el.html(_.template(entitySystemInformationTemplate, { data: this.model.toJSON() }));
            return this;
        },
    });

    return entitySystemInformationView;
});
