﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var receiverModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/syndication/receiver'
    });

    return receiverModel;

});
