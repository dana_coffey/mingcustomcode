﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fileModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/file/',

    });

    return fileModel;

});
