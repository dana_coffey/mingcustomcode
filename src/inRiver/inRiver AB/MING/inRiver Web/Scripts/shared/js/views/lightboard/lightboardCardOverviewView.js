﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/views/contextmenu/contextMenuView',
    'text!sharedtemplates/lightboard/lightboardOverviewCardTemplate.html'

], function ($, _, Backbone, contextMenuView, lightboardOverviewCardTemplate) {

    var lightboardCardOverviewView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.model = options.model;
            this.parentEntityId = options.parentId;
            this.render();
        },
        render: function () {

            this.$el.html(_.template(lightboardOverviewCardTemplate, { resource: this.model }));

            return this; // enable chained calls
        }
    });

    return lightboardCardOverviewView;
});