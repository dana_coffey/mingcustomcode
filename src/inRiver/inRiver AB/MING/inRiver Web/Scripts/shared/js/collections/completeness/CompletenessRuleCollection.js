define([
  'jquery',
  'underscore',
  'backbone',
  //'models/completeness/CompletenessRuleModel'
], function ($, _, Backbone, CompletenessRule) {
    var CompletenessRuleCollection = Backbone.Collection.extend({
        initialize: function (models, options) {
            if (options.id) {
                this.id = options.id;
            } else if (options.entitytype) {
                this.entitytype = options.entitytype;
            }
        },
        //model: CompletenessRule,
        url: function () {
            if (this.id) {
                return '/api/completenessrule/' + this.id;
            } else {
                return '/api/completenessrule?entitytype=' + this.entitytype;
            }
        }
});
 
  return CompletenessRuleCollection;
});
