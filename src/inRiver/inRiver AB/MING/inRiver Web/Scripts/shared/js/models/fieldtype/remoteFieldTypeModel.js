﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var remoteFieldTypeModel = backbone.DeepModel.extend({
        idAttribute: "id",
        initialize: function (options) {
            this.id = options.id;
            this.urlRoot = options.url;
        },
        urlRoot: '/api/fieldtype/?fieldTypeId=' + this.id,
    });

    return remoteFieldTypeModel;

});
