﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var completenessModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        //urlRoot: '/api/completeness',
        
    });

    return completenessModel;

});
