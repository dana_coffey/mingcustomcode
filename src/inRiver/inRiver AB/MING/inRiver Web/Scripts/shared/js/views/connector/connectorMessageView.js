define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/connector/connectorSettingModel',
  'sharedjs/collections/connectorevents/ConnectorEventSessionCollection',
  'sharedjs/collections/connectorevents/latestConnectorEventCollection',
], function ($, _, Backbone, alertify,inRiverUtil, SettingModel, connectorEventSessionCollection, latestConnectorEventCollection) {

    var connectorMessageView = Backbone.View.extend({

        initialize: function (options) {
            var self = this;
            this.undelegateEvents();
            this.id = options.id;

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'popupcancel', this.close);
            this.listenTo(appHelper.event_bus, 'popupsave', this.saveSetting);

            if (this.id != undefined) {
                this.collection = new latestConnectorEventCollection();
                this.collection.fetch({
                    success: function (collection) {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }, 
                    data: $.param({ id: this.id, maxNumberOfEvents: "5" })
                });
            } else {
                var events = this.model.get("ConnectorEvents");
                this.collection = new connectorEventSessionCollection();
                this.collection.reset(events);
                this.render();
            }
        },
        close: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            Backbone.View.prototype.remove.call(this);

        },
        render: function () {
            //this.$el.html(settingsTemplate);
            console.log("MessageListView Render");

            var columns = [
            { name: "EventTime", label: "Time", cell: "string" },
            {
                name: "IsError",
                label: "Status",
                cell: Backgrid.Cell.extend({
                    render: function() {
                        // Check for errors
                        var statusEvent = "OK";
                        if (this.model.get("IsError") == true) {
                            // Status
                            statusEvent = "Failed";
                            this.$el.addClass("errorRow");
                        }

                        // Put what you want inside the cell (Can use .html if HTML formatting is needed)
                        this.$el.html(statusEvent);
                        this.delegateEvents();
                        return this;
                    }
                })
            },
            { name: "Message", label: "Message", cell: "string" }
            /*{ name: "", cell: GridDeleteItem }*/
                        ];

            if (this.id == undefined) {
                var percentage = {
                    name: "Percentage", label:
                    "Complete", cell:
                    "string"
                }
                columns.push(percentage);
            }

            // Initialize a new Grid instance
            this.grid = new Backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped row-cursor-default"
            });

            this.$el.append(this.grid.render().$el);
            this.grid.$el.find(".errorRow").parent().addClass("errorrow");
            return this;
        }
    });

    return connectorMessageView;
});
