﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var queryModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/query',
        
    });

    return queryModel;

});
