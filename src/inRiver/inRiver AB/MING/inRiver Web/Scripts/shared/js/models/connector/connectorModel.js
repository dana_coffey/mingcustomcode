﻿define([
  'underscore',
  'backbone',
], function (_, Backbone) {

    var connectorModel = Backbone.Model.extend({
        idAttribute: "id",
        initialize: function (options) {
            //this.id = options.id; 
        },
        urlRoot: '/api/connector',
        
    });

    return connectorModel;

});
