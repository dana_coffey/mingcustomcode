define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/language/LanguageModel'
], function($, _, Backbone, LanguageModel){
  var LanguagesCollection = Backbone.Collection.extend({
      model: LanguageModel,
      url: '/api/language'
  });
 
  return LanguagesCollection;
});
