define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var NotificationModel = Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: '/api/notification',
    });

    return NotificationModel;
    
});
