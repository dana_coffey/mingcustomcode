﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/fieldrevision/fieldRevisionCollection',
  'sharedjs/views/fieldrevision/fieldRevisionValueView',
  'text!sharedtemplates/fieldrevision/fieldRevisionTemplate.html'
], function ($, _, backbone, inRiverUtil, fieldRevisionCollection, fieldRevisionValueView, fieldRevisionTemplate) {

    var fieldRevisionView = backbone.View.extend({
        initialize: function (options) {
            this.entityId = options.entityId;
            this.fieldTypeId = options.fieldTypeId;
            this.fieldDataType = options.fieldDataType; 
            var that = this; 

            var onDataHandler = function (collection) {
                that.render();
            };
           
            that.collection = new fieldRevisionCollection([], { entityId: this.entityId, fieldTypeId: this.fieldTypeId });
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //this.render();
        },
        events: {
            "click #close-button": "onClose",
        },
        onClose: function () {
            $("#modalWindow").hide();
            this.remove();
        },
        render: function () {
            this.$el.html(_.template(fieldRevisionTemplate, { entityId: this.entityId, fieldTypeId: this.fieldTypeId, fieldDataType: this.fieldDataType, isReadOnly: this.options.isReadOnly }));
            var that = this;

            if (this.collection.length > 0) {
                var container = this.$el.find("#revisions");
                _.each(this.collection.models, function(model) {
                    var fieldrevisionvalue = new fieldRevisionValueView({
                        model: model,
                        isReadOnly: that.options.isReadOnly,
                        fieldDataType: that.fieldDataType
                    });

                    container.append(fieldrevisionvalue.render().el);
                });
            }

            return this; // enable chained calls
        }
    });

    return fieldRevisionView;
});
