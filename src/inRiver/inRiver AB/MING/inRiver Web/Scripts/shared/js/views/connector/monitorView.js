﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  'alertify',
  'modalPopup',
  'moment',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/connector/connectorMessageView',
  'views/connectorevents/connectorEventView',
  'sharedjs/collections/connectorevents/ConnectorEventSessionCollection',
  'sharedjs/collections/connectorevents/ConnectorEventTypeCollection',
  'sharedjs/collections/connectorevents/latestConnectorEventCollection',
  'text!sharedtemplates/connectormonitor/monitorFilterTemplate.html',
  'text!sharedtemplates/connectormonitor/monitorTemplate.html'

], function ($, _, backbone, Backgrid, alertify, modalPopup, moment, inRiverUtil, connectorMessageView, connectorEventView, connectorEventSessionCollection, connectorEventTypeCollection, latestConnectorEventCollection, monitorFilterTemplate, monitorTemplate) {

    var monitorView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.undelegateEvents();
            this.model = options.model;

            this.initTemplates();

             //Reload every 20 seconds
            this.timer = setInterval(function () {
                self.reloadData();
            }, 20000);

            this.pastEventCollection = new connectorEventSessionCollection();

            this.pastEventCollection.on('rowClicked', function (model, selected) {
                event.stopPropagation();

                var modal = new modalPopup();

                modal.popOut(new connectorMessageView({
                    model: model,
                }), { size: "large", header: "Event Messages", usesavebutton: false });

                $("#save-form-button").removeAttr("disabled");
            });
        },
        events: {
            "change #filter-start-time": "reloadPast",
            "change #filter-end-time": "reloadPast",
            "change #filter-event-types": "reloadPast",
            "change #filter-status": "reloadPast",
        },
        reloadData: function () {
            var self = this;

            //// Get Ongoing events
            this.ongoingEventCollection = new connectorEventSessionCollection();
            this.ongoingEventCollection.fetch({
                success: function (collection) {
                    var i = 0; 
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ id: this.model.get("Id"), action: "ongoing" })
            });

            // Get Queued events
            this.queuedEventCollection = new connectorEventSessionCollection();
            this.queuedEventCollection.fetch({
                success: function (collection) {
                    if (collection.models.length == 0) {
                        self.template.find("#queued-events-list").html("<div class='no-items-text'>No events found</div>");
                    } else {
                        self.template.find("#queued-events-list").html("");
                        _.each(collection.models, function (model) {
                            var eventView = new connectorEventView({ data: model });
                            self.template.find("#queued-events-list").append(eventView.render().el);
                        });
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ id: this.model.get("Id") })
            });

            //// Get Latest events
            this.latestEventsCollection = new latestConnectorEventCollection();
            this.latestEventsCollection.fetch({
                success: function (collection) {
                    self.template.find("#latest-events-list").empty();
                    if (collection.models.length == 0) {
                        self.template.find("#latest-events-list").html("<div class='no-items-text'>No events found</div>");
                    } else {
                        self.template.find("#latest-events-list").html(""); 
                        _.each(collection.models, function (model) {
                            var eventView = new connectorEventView({ model: model });
                            self.template.find("#latest-events-list").append(eventView.render().el);
                        });
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ id: this.model.get("Id"), maxNumberOfEvents: "5" })
            });

            this.reloadPast();
        },
        reloadPast: function () {
            var eventTypeInput = this.$el.find("#filter-event-types").val();
            var statusInput = this.$el.find("#filter-status").val();
            var startTimeInput = this.$el.find("#filter-start-time").val();
            var endTimeInput = this.$el.find("#filter-end-time").val();

            var self = this;

            if (startTimeInput == undefined || endTimeInput == undefined) {
                return; 
            }

            this.pastEventCollection.fetch({
                success: function (collection) {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ id: this.model.get("Id"), type: eventTypeInput, status: statusInput, startTime: startTimeInput, endTime: endTimeInput })
            });
        },
        initTemplates: function () {
            var self = this;
            this.template = this.$el.html(monitorTemplate);

            this.eventTypes = new connectorEventTypeCollection();
            this.eventTypes.fetch({
                success: function () {
                    self.$el.find("#filter-container").html(_.template(monitorFilterTemplate, { eventtypes: self.eventTypes.toJSON() }));

                    self.$el.find("#filter-start-time").datetimepicker({
                        format: appHelper.momentDateTimeFormat,
                        formatTime: appHelper.momentTimeFormat,
                        formatDate: appHelper.momentDateFormat,
                        lang: 'en',
                        weeks: appHelper.showWeeksInDatePicker,
                        dayOfWeekStart: appHelper.firstDayOfWeek
                    });

                    self.$el.find("#filter-end-time").datetimepicker({
                        format: appHelper.momentDateTimeFormat,
                        formatTime: appHelper.momentTimeFormat,
                        formatDate: appHelper.momentDateFormat,
                        lang: 'en',
                        weeks: appHelper.showWeeksInDatePicker,
                        dayOfWeekStart: appHelper.firstDayOfWeek
                    });

                    var d = new Date();
                    d.setDate(d.getDate() - 1);
                    self.$el.find("#filter-start-time").val(moment(d).format(appHelper.momentDateTimeFormat));

                    self.reloadData();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            // Render ongoing events
            var columns = [
            { name: "ConnectorEventType", label: "Event Type", cell: "string" },
            { name: "Status", label: "Status", cell: "string" },
            { name: "Complete", label: "Complete", cell: "string" },
            { name: "StartTime", label: "Start Time", cell: "string" },
            { name: "EndTime", label: "End Time", cell: "string" },
            { name: "RunningTime", label: "Running Time", cell: "string" },
            {
                name: "Messages", label: "Messages", cell: Backgrid.Cell.extend({
                    events: {
                        //"click": "showRules"
                    },
                    showRules: function (e) {
                        e.stopPropagation();
                        e.preventDefault();

                        /*this.goTo("#completeness/" + this.model.id + "/rules");        */  
                    },
                    render: function () {
                        // Check for errors
                        if (this.model.get("Status") != "OK") {
                            this.$el.addClass("errorRow");
                        }

                        // Put what you want inside the cell (Can use .html if HTML formatting is needed)
                        this.$el.html("<i class='fa fa-info-circle'></i>");
                        this.delegateEvents();
                        return this;
                    }
                })
            }
            ];

            if (this.ongoingEventCollection.first() == null || this.ongoingEventCollection.first().get("ConnectorEvents") == null) {
                this.template.find("#ongoing-events-list").html("<div class='no-items-text'>No events found</div>");
            } else {
                // Initialize a new Grid instance
                this.grid1 = new Backgrid.Grid({
                    row: ClickableRow,
                    columns: columns,
                    collection: this.ongoingEventCollection,
                    className: "backgrid backgrid-striped"
                });

                this.template.find("#ongoing-events-list").html(this.grid1.render().$el);
                this.grid1.$el.find(".errorRow").parent().addClass("error");
            }

            // Render past events

            if (this.pastEventCollection.first() == null || this.pastEventCollection.first().get("ConnectorEvents") == null) {
                this.template.find("#past-events-list").html("<div class='no-items-text'>No events found</div>");
            } else {
                this.template.find("#past-events-list").html(""); 
                // Initialize a new Grid instance
                this.grid2 = new Backgrid.Grid({
                    row: ClickableRow,
                    columns: columns,
                    collection: this.pastEventCollection,
                    className: "backgrid backgrid-striped"
                });

                this.template.find("#past-events-list").html(this.grid2.render().$el);
                this.grid2.$el.find(".errorRow").parent().addClass("errorrow");
            }

            return this;
        }
    });

    return monitorView;
});
