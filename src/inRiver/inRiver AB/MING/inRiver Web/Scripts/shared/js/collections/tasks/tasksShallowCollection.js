define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/task/taskShallowModel'
], function ($, _, Backbone, taskShallowModel) {
    var tasksCollection = Backbone.Collection.extend({
        model: taskShallowModel
    });

    return tasksCollection;
});
