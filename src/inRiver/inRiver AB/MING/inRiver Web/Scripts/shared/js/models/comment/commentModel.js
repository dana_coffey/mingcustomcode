﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var commentModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {

        },
        urlRoot: "../../api/comment"
    });

    return commentModel;

});