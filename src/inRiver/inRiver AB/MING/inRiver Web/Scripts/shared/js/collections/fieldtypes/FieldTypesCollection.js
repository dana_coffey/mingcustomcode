define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/fieldtype/FieldTypeModel'
], function ($, _, Backbone, FieldTypeModel) {
  var FieldTypesCollection = Backbone.Collection.extend({
      model: FieldTypeModel,
      url: '/api/fieldtype'
  });
 
  return FieldTypesCollection;
});
