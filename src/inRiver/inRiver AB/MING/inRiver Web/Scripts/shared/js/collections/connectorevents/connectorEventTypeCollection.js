define([
  'jquery',
  'underscore',
  'backbone',
  //'sharedjs/models/connectorevent/connectorEventSessionModel'
], function ($, _, backbone, connectorEventModel) {
    var connectorEventTypeCollection = backbone.Collection.extend({
        //model: connectorEventModel,
        url: '/api/connectoreventtype/'
    });

    return connectorEventTypeCollection;
});
