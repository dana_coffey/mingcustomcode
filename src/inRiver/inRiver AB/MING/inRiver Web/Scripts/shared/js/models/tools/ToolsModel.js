﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var toolsModel = Backbone.Model.extend({
        idAttribute: "id",
        initialize: function (options) {
        }
    });

    return toolsModel;

});