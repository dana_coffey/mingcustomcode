define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/connector/connectorModel'
], function ($, _, backbone, connectorEventModel) {
    var connectorCollection = backbone.Collection.extend({
        model: connectorEventModel,
        url: '/api/connector/'
    });

    return connectorCollection;
});
