define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var userModel = backbone.Model.extend({
        idAttribute: 'Username',
        urlRoot: '/api/userinformation',
    });

    return userModel;
});
