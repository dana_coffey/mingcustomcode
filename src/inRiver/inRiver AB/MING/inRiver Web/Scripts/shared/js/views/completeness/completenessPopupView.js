﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/models/completeness/completenessModel',
  'text!sharedtemplates/completeness/completenessTemplate.html'
], function ($, _, backbone, alertify, completenessModel, completenessTemplate) {

    var completenessPopupView = backbone.View.extend({
        initialize: function (options) {
            this.model = options.model;
            this.render();
        },
        events: {
           "click #close-icon": "closepopup"
        },
        closepopup: function() {
            appHelper.event_bus.trigger('closepopup');
        },
        render: function () {
            // this.$el.html(this.template({ data: this.model.toJSON() }));
            this.$el.html(_.template(completenessTemplate, { data: this.model.toJSON() }));

            return this; // enable chained calls
        },
    });

    return completenessPopupView;
});

