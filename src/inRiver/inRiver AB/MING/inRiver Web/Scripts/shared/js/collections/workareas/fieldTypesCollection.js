﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/fieldtype/FieldTypeModel'
], function ($, _, backbone, fieldTypeModel) {
    var fieldTypeCollection = backbone.Collection.extend({
        model: fieldTypeModel,
        initialize: function (models, options) {
            this.entityTypeId = options.entityTypeId;
        },
        url: function () {
            return '/api/fieldtype?id=' + this.entityTypeId;
        }
    });

    return fieldTypeCollection;
});