﻿define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-steps',
    'jstree',
    'sharedjs/views/entitycard/entitycardView',
    'sharedjs/collections/fieldtypes/FieldTypesCollection',
    'sharedjs/collections/languages/languagesCollection',
    'sharedjs/models/entity/validateEntitiesModel',
    'sharedjs/models/field/validateFieldModel',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/entity/entityOverviewFieldTemplate.html',
    'text!sharedtemplates/massupdate/massUpdateWizzardTemplate.html',
    'text!sharedtemplates/massupdate/massUpdateWizzardStepChoiceTemplate.html',
    'text!sharedtemplates/massupdate/massUpdateWizzardStepDetailsTemplate.html',
    'text!sharedtemplates/massupdate/massUpdateWizzardStepResultTemplate.html',
    'text!sharedtemplates/massupdate/massUpdateFieldEditorTemplate.html'
], function ($, _, backbone, jquerySteps, jstree, entitycardView, fieldTypesCollection, languagesCollection, validateEntitiesModel, validateFieldModel, inRiverUtil, entityOverviewFieldTemplate, massUpdateWizzardTemplate, wizzardStepChoiceTemplate, wizzardStepDetailsTemplate, wizzardStepResultTemplate, massUpdateFieldEditorTemplate) {
    var massUpdateWizzardView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.EntityIds = options.ids;
            this.EntityTypeId = options.entityTypeId;
            this.EntityModels = options.models;
            this.RestfulEntiyFields = null;
            this.AllLinkedRestfulEntiyFields = [];
            this.Relations = [];
            this.SelectedLinkIds = [];

            this.fieldTypesCollection = new fieldTypesCollection();
            this.fieldTypesCollection.data = { id: options.entityTypeId };

            if (!window.appHelper.languagesCollection) {
                window.appHelper.languagesCollection = new languagesCollection();
            }

            this.collections = [this.fieldTypesCollection, window.appHelper.languagesCollection];

            _.each(this.collections, function (collection) {
                self.initCollection(collection);
            });

        },
        initCollection: function (collection) {
            if (collection.isLoaded) {
                this.onCollectionInitialized(collection);
                return;
            }
            this.listenTo(collection, "reset", this.onCollectionInitialized);
            collection.fetch({
                data: collection.data,
                reset: true
            });
        },
        onCollectionInitialized: function (collection) {
            collection.isLoaded = true;
            if (_.every(this.collections, function(otherCollection) {
                return otherCollection.isLoaded;
            })) {
                this.render();
            }
        },
        events: {
            'change .field-editor-wrap input': "checkField"
        },
        onCancel: function () {
            appHelper.event_bus.trigger("closepopup");
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        moveToStepChoiceView: function () {
            var self = this;
            this.$el.find("#mass-update-wizzard-choice").html(_.template(wizzardStepChoiceTemplate, {}));
            this.select = this.$el.find("#field-types-select");

            var categories = [];
            this.fieldTypesCollection.comparator = "index";
            this.fieldTypesCollection.sort();
            this.fieldTypesCollection.each(function (m) {
                if (m.get("unique") || m.get("readonly") || m.get("hidden")) {
                    return;
                }
                var id = m.get("id");
                var displayname = m.get("displayname");
                var categoryname = m.get("categoryname");

                var category = _.findWhere(categories, { id: categoryname });
                if (!category) {
                    category = {
                        id: categoryname,
                        text: categoryname,
                        children: [],
                        state: {
                            opened: true
                        }
                    };
                    categories.push(category);
                }

                var fieldType = {
                    id: id,
                    text: displayname
                };

                if (m.get("datatype") === "LocaleString") {
                    fieldType.children = window.appHelper.languagesCollection.map(function (languageModel) {
                        var displayName = languageModel.get("DisplayName");
                        return {
                            id: id + "/" + languageModel.id,
                            text: displayName
                        };
                    });
                }

                category.children.push(fieldType);

            });

            $(".search-input").keyup(function () {
                var searchString = $(this).val();
                self.select.jstree("search", searchString);
            });

            this.select.jstree({
                core: {
                    data: categories,
                    themes: { dots: false, icons: false }
                },
                checkbox: {
                    keep_selected_style: false
                },
                search: {
                    case_insensitive: true,
                    show_only_matches: true
                },
                plugins: ["wholerow", "checkbox", "search"]
            });

            return true;
        },
        moveToStepDetailView: function () {
            var self = this;

            var selectedFieldTypes = _.chain(this.select.jstree(true).get_bottom_selected()).map(function (selected) {
                var array = selected.split("/");
                return { id: array[0], language: array[1] };
            }).sortBy(function (fieldType) {
                var fieldTypeModel = self.fieldTypesCollection.get(fieldType.id);
                return fieldTypeModel.get("index");
            }).groupBy("id").value();

            if (_.isEmpty(selectedFieldTypes) ) {
                return false;
            }

            if (this.form) {
                this.form.commit();
            }

            var fieldValueDictionary = this.entityFieldsModel ? _.reduce(this.entityFieldsModel.get("Fields"), function(dictionary, field) {
                dictionary[field.FieldType] = field.Value;
                return dictionary;
            }, {}) : null;

            this.entityFieldsModel = new backbone.DeepModel();
            this.entityFieldsModel.idAttribute = "Id";
            this.entityFieldsModel.url = "/api/entityfields/" + this.EntityIds[0];
            this.entityFieldsModel.fetch({
                success: function(model) {
                    model.set("Fields", _.filter(model.get("Fields"), function (field) {
                        if (_.has(selectedFieldTypes, field.FieldType)) {
                            field.Value = fieldValueDictionary ? fieldValueDictionary[field.FieldType] : null;
                            field.languages = _.pluck(selectedFieldTypes[field.FieldType], "language");
                            return true;
                        }
                        return false;
                    }));

                    self.$el.find("#mass-update-wizzard-detail").html(_.template(wizzardStepDetailsTemplate, {}));

                    var schema = inRiverUtil.createFormsSchemaFromRestfulFields(model);
                    var formTemplate = self.getFieldValueFormTemplate(
                        _.map(_.keys(selectedFieldTypes), function (fieldTypeId) { return self.fieldTypesCollection.get(fieldTypeId) }),
                        schema);

                    self.schema = schema;

                    self.form = new backbone.Form({
                        template: _.template(formTemplate),
                        model: model,
                        schema: self.schema
                    }).render();

                    var editValuesContainer = self.$el.find("#mass-update-edit-values-form");
                    editValuesContainer.append(self.form.el);
                }
            });

            return true;
        },
        moveToStepVerifyView: function () {
            var self = this;
            var errors = this.form.validate();
            this.$el.find(".field-editor-validation-error-message").html("");
            if (errors) {
                _.each(Object.keys(errors), function (error) {
                    self.$el.find("div[data-id='" + error + "']").html("<i class='fa fa-warning'></i>&nbsp;" + errors[error].message);
                });
                return false;
            }

            this.form.commit();

            this.valueByFieldTypeId = _.reduce(self.entityFieldsModel.get("Fields"), function (dictionary, field) {
                dictionary[field.FieldType] = field.Value;
                return dictionary;
            }, {});

            this.$el.find("#mass-update-wizzard-verify").html(_.template(wizzardStepResultTemplate));

            $("#massupdate-total").text(this.EntityIds.length);
            $("#massupdate-inprogress").text(0);
            $("#massupdate-done").text(0);
            $("#massupdate-failed").text(0);

            _.each(this.EntityModels, function(model) {
                $("#mass-update-entity-cards-container").append(
                    "<div class='card-text1' title='" + model.get("DisplayName") + "'>" + model.get("DisplayName")
                    + "</div><div class='card-text2-large' title='" + model.get("DisplayDescription") + "'>" + model.get("DisplayDescription")
                    + "</div><div id='entity-status-" + model.id + "' class='entity-status fa'></div>");
            });

            this.renderFields();
            return true;
        },
        moveToStepResultView: function () {
            var entityStatus = $(".entity-status");
            entityStatus.addClass("fa-upload");
            entityStatus.addClass("entity-card-status-pending");
            entityStatus.prop("title", "Saving...");

            this.entityIdIndex = 0;
            this.inProgress = this.EntityIds.length;
            this.done = 0;
            this.failed = 0;
            for (var i = 0; i < Math.min(this.EntityIds.length, 20) ; i++) {
                this.saveNextEntity();
            }

            this.steps.find(".actions a:eq(2)").text("Close");
            this.steps.find(".actions a:eq(3)").css("display", "none");

            return false;
        },
        saveNextEntity: function () {
            var index = this.entityIdIndex++;
            if (index >= this.EntityIds.length) {
                return;
            }
            var self = this;
            var entityId = this.EntityIds[index];
            var entityStatus = $("#entity-status-" + entityId);

            $.ajax({
                type: "POST",
                url: "/api/massupdate",
                data: JSON.stringify({ EntityIds: [entityId], Fields: this.entityFieldsModel.get("Fields") }),
                dataType: "json",
                contentType: "application/json",
            }).always(function () {
                self.ongoingSaves--;
                self.saveNextEntity();
                entityStatus.removeClass("fa-upload");
                entityStatus.removeClass("entity-card-status-pending");
                $("#massupdate-inprogress").text(--self.inProgress);
            }).fail(function (xhr, status, error) {
                entityStatus.addClass("fa-close");
                entityStatus.addClass("entity-card-status-failed");
                entityStatus.prop("title", error);
                $("#massupdate-failed").text(++self.failed);
            }).done(function () {
                entityStatus.addClass("fa-check");
                entityStatus.addClass("entity-card-status-ok");
                entityStatus.prop("title", "Successfully updated");
                $("#massupdate-done").text(++self.done);
            });

        },
        renderFields: function () {
            var self = this;
            var container = this.$el.find("#mass-update-field-container");

            if (container.length > 0) {
                $(container)[0].innerHTML = "";
            }

            _.each(this.entityFieldsModel.get("Fields"), function (field) {
                var fieldType = self.fieldTypesCollection.get(field.FieldType);
                var text = "";

                if (fieldType.get("datatype") === "LocaleString") {
                    _.each(field.Value, function (languageValue) {
                        var fieldName = fieldType.get("displayname") + " - " + languageValue[0];
                        container.append("<div class='card-text1' title='" + fieldName + "'>" + fieldName
                            + "</div><div class='card-text2-large' title='" + languageValue[1] + "'>" + languageValue[1] + "</div>");
                    });
                    return;
                } else if (fieldType.get("datatype") === "CVL") {
                    var displayValueByValue = _.reduce(field.PossibleValues, function(dictionary, possibleValue) {
                        dictionary[possibleValue.Key] = possibleValue.Value;
                        return dictionary;
                    }, {});
                    if (_.isArray(field.Value)) {
                        _.each(field.Value, function (val) {
                            text += displayValueByValue[val] + ", ";
                        });
                        if (text.length > 2) {
                            text = text.substring(0, text.length - 2);
                        }
                    } else {
                        text = displayValueByValue[field.Value];
                    }
                } else {
                    text = field.Value;
                }

                container.append("<div class='card-text1' title='" + fieldType.get("displayname") + "'>" + fieldType.get("displayname")
                            + "</div><div class='card-text2-large' title='" + text + "'>" + text + "</div>");
            });
        },

        getFieldValueFormTemplate: function (fieldTypes, schema) {
            var formTemplate = "<form>";
            _.each(fieldTypes, function (fieldType) {
                var dataEditorKey = inRiverUtil.findBackboneFormsEditorKey(schema, fieldType.id);
                var editor = _.template(massUpdateFieldEditorTemplate, { type: fieldType.id, dataType: fieldType.get("datatype"), name: fieldType.get("displayname"), dataEditorKey: dataEditorKey, highlightedLabel: false });
                formTemplate += editor;
            });

            formTemplate += "</form>";

            return formTemplate;
        },
        checkField: function (e) {
            var self = this;
            var fieldTypeIdElement = $(e.currentTarget).closest('.field-editor-wrap');

            var fieldTypeId = fieldTypeIdElement[0].getAttribute('fieldTypeid');
            var fieldValue = e.currentTarget.value;

            _.each(self.entityFieldsModel.get("Fields"), function (field) {
                if (field.FieldType === fieldTypeId) {
                    field.Value = fieldValue;
                }
            });
        },
        finish: function () {
            var self = this;
            $.post("/api/tools/compress", { '': this.EntityIds.toString() }).done(function (result) {
                self.goTo("workareasearch?title=Updated&compressedEntities=" + result);
            });
            this.onCancel();
        },
        render: function () {
            var self = this;
            this.$el.html(_.template(massUpdateWizzardTemplate, {}));

            this.steps = this.$el.find("#mass-update-wizard").steps({
                transitionEffect: "slideLeft",
                transitionEffectSpeed: 400,
                enableAllSteps: false,
                autoFocus: true,
                enableCancelButton: true,
                onInit: function () {
                    self.moveToStepChoiceView();
                },
                onCanceled: function () {
                    self.onCancel();
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (newIndex < currentIndex) {
                        self.steps.find(".actions a:eq(3)").css("display", "block");
                        return true;
                    }

                    switch (newIndex) {
                        case 0:
                            return self.moveToStepChoiceView();
                        case 1:
                            return self.moveToStepDetailView();
                        case 2:
                            self.steps.find(".actions a:eq(2)").text("Update");
                            return self.moveToStepVerifyView();
                        default:
                            return false;
                    }
                },
                onFinished: function (event, currentIndex) {
                    if (self.steps.find(".actions a:eq(2)").text() === "Update") {
                        self.moveToStepResultView();
                        return false;
                    }

                    self.finish();
                    return true;
                },
                labels: {
                    loading: "Loading ..."
                }
            });
        }
    });

    return massUpdateWizzardView;
});