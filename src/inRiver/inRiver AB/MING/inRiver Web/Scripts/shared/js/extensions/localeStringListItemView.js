define([
  'jquery',
  'underscore',
  'backbone',
  'text!sharedtemplates/extension/localeStringListItemTemplate.html'
], function ($, _, backbone, localeStringListItemTemplate) {

    var localeStringListItemView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.id = options.Id;
            this.langKey = options.Key;
            this.language = options.Language;
            this.name = options.Name;
            this.readonly = options.Readonly; 
        },
        events: {
            "blur textarea": "onBlur",
            "keyup textarea": "onKeyUp"
        },
        onBlur: function(e) {
            e.stopPropagation();
            var text = $(e.currentTarget).val();
            // Fire away!
            window.appHelper.event_bus.trigger('lsvalueupdated', [this.langKey, [this.language, text], this.id]);
        },
        onKeyUp: function (e) {
            e.stopPropagation();
            var text = $(e.currentTarget).val();
            // Fire away!
            window.appHelper.event_bus.trigger('lsvalueupdated', [this.langKey, [this.language, text], this.id]);
        },
        render: function() {
            var template = _.template(localeStringListItemTemplate, { id: this.id, language: this.language, text: this.options.Text, name: this.name, readonly: this.readonly });
            this.setElement(template, true);

            return this;
        }
    });

    return localeStringListItemView;
  
});

