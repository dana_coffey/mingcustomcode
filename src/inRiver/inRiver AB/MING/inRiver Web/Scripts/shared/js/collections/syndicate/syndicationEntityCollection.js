define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/syndicate/syndicationEntityModel'
], function ($, _, backbone, syndicationEntityModel) {
    var syndicationEntityCollection = backbone.Collection.extend({
        model: syndicationEntityModel,
        initialize: function (models, options) {
            this.workareaId = options.workareaId;
        },
        url: function () {
            return '/api/syndication/workarea/' + this.workareaId + '/entity';
        }
    });

    return syndicationEntityCollection;
});
