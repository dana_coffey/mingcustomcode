﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var relationStructureModel = backbone.DeepModel.extend({
        idAttribute: "entityId",
        initialize: function () {
        },
        urlRoot: '/api/relations/structure',

    });

    return relationStructureModel;
});