﻿define([
    'jquery',
    'underscore',
    'backbone',
    '../../../../libs/filedownload/jquery.fileDownload',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/export/exportExcelConfigurationModel',
    'sharedjs/models/export/exportExcelModel',
    'sharedjs/models/entityType/entityTypeModel',
    'sharedjs/collections/export/exportExcelConfigurationCollection',
    'sharedjs/collections/entityTypes/entityTypeCollection',
    'sharedjs/collections/fieldsets/fieldsetCollection',
    'sharedjs/collections/languages/LanguagesCollection',
    'sharedjs/collections/relatedlinktypes/relatedLinkTypesCollection',
    'sharedjs/views/excelexport/excelExportEntityTypeView',
    'sharedjs/views/excelexport/excelExportCvlControlView',
    'text!sharedtemplates/excelexport/excelExportTemplate.html'
], function ($, _, backbone, fileDownload, inRiverUtil, exportExcelConfigurationModel, exportExcelModel, entityTypeModel, exportExcelConfigurationCollection, entityTypeCollection, fieldsetCollection, languagesCollection, relatedLinkTypesCollection, excelExportEntityTypeView, excelExportCvlControlView, excelExportTemplate) {
    var excelExportView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.notSetDisplayName = "(not set)";
            this.entityTypeIds = _.uniq(options.entitytypeIds);
            this.entityIds = options.entityIds;
            this.excelExportEntityTypeViews = [];

            this.entityTypeCollection = new entityTypeCollection();
            this.entityTypeCollection.fetch({
                success: function () {
                    self.entityTypeCollectionReady = true;
                    self.render();
                }
            });

            this.fieldSetCollection = new fieldsetCollection();
            this.fieldSetCollection.fetch({
                success: function () {
                    self.fieldSetCollectionReady = true;
                    self.render();
                }
            });

            this.languagesCollection = new languagesCollection();
            this.languagesCollection.fetch({
                success: function () {
                    self.languagesCollectionReady = true;
                    self.render();
                }
            });
        },
        events: {
            "click #excel-export": "doExcelExport",
            "click #excel-export-save-configuration": "doSaveExportConfiguration",
            "click #excel-export-save-configuration-as": "doShowConfigurationExportDialog",
            "click #cancelconfiguration": "doCancelExportConfigurationDialog",
            "click #saveconfiguration": "doSaveExportConfiguration",
            "change #export-excel-value-configuration": "doChangeExportConfiguration",
            "click #remove-confiugration-container": "removeConfiguration",
            "change [id^=export-excel-value-type]": "changeEntityTypeExcelExport"

           
        },
        lockView: function() {
            this.$el.find("#excel-loading-spinner-container").show();
            this.setDisabled(true);
        },
        unlockView: function () {
            this.setDisabled(false);
            this.$el.find("#excel-loading-spinner-container").hide();
        },
        setDisabled: function(disabled) {
            this.$el.find("#excel-export").prop("disabled", disabled);
            this.$el.find("#excel-export-save-configuration").prop("disabled", disabled);
        },
        getConfiguration: function() {
            return {
                EntityIds: this.entityIds,
                EntityTypeModels: _.filter(_.map(this.excelExportEntityTypeViews, function (view) {
                    return view.getExcelExportEntityTypeModel();
                }), function(configuration) {
                    return configuration && configuration.EntityTypeId !== "(none)";
                }),
                Languages: this.$el.find("#export-excel-value-languages").val()
            };
        },
        createConfigurationCvlControl: function (selectedValues) {
            this.createCvlControl("configuration", "Open saved setting:", false, this.exportConfigurationsCollection, selectedValues);
        },
        createLanguageCvlControl: function (selectedValues) {
            this.createCvlControl("languages", "Languages:", true, this.languagesCollection, selectedValues);
        },
        createCvlControl: function (controlDefinition, caption, multiValue, values, selectedValues) {
            var cvlControl = new excelExportCvlControlView({ ControlDefinition: controlDefinition, Caption: caption, Values: values, Multivalue: multiValue, PossibleValues: selectedValues });
            this.$el.find("#export-excel-container-" + controlDefinition).html(cvlControl.$el);
        },
        doExcelExport: function () {
            var self = this;
            var configuration = this.getConfiguration();
            if (configuration.EntityTypeModels.length > 0 && configuration.EntityTypeModels[0].EntityTypeId != null) {
                $.fileDownload("/api/exportexcel/download", {
                    httpMethod: "POST",
                    data: configuration,
                    successCallback: function () {
                        self.unlockView();
                    },
                    failCallback: function (responseHtml, url) {
                        self.unlockView();
                        inRiverUtil.OnErrors(responseHtml, url);
                    }
                });
            }

        },
        doCancelExportConfigurationDialog: function () {
            this.$el.find("#savedialog").hide();
        },
        doShowConfigurationExportDialog: function () {
            var self = this;
            inRiverUtil.Prompt("Enter a name for the export settings", function (e, name) {
                self.exportConfigurationsCollection = new exportExcelConfigurationCollection();
                self.exportConfigurationsCollection.fetch({
                    success: function (collection) {
                        var existingModel = collection.findWhere({ Name: name });
                        if (existingModel) {
                            inRiverUtil.NotifyConfirm("Save Configuration", "Saving a configuration with a name that already exist <br> will overwrite the existing configuration.", function () {
                                self.doSaveExportConfiguration(name);
                            }, this);
                        } else {
                            self.doSaveExportConfiguration(name);
                        }
                    }
                });
            });
        },
        doSaveExportConfiguration: function (name) {
            var self = this;

            this.configurationname = _.isString(name) ? name : self.$el.find("#export-excel-value-configuration").val()[0];

            if (this.configurationname === this.notSetDisplayName) {
               // inRiverUtil.Notify("Adding Default/default configuration is invalid.");
                return;
            }

            var existingModel = this.exportConfigurationsCollection.findWhere({ Name: self.configurationname });
            if (existingModel) {
                self.configurationModel = existingModel;
                self.saveConfiguration();
            } else {
                self.configurationModel = new exportExcelConfigurationModel({ Name: self.configurationname });
                self.exportConfigurationsCollection.add(self.configurationModel);
                self.saveConfiguration();
            }

        },
        saveConfiguration: function () {
            var self = this;
            this.configurationModel.set(this.getConfiguration());
            this.configurationModel.name = this.configurationModel.get("Name");

            self.configurationModel.save(null, {
                success: function () {
                    self.configurationModel.fetch({
                        success: function () {
                            var defaultConfiguration = new exportExcelModel();
                            defaultConfiguration.set("Name", self.notSetDisplayName);
                            self.exportConfigurationsCollection.add(defaultConfiguration, { at: 0 });

                            self.createConfigurationCvlControl(self.configurationModel.name);

                            self.$el.find("#excel-export-save-configuration").show();
                            self.$el.find("#remove-confiugration-container").show();

                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        createExcelExportEntityTypeView: function (collection, model, excelExportEntityTypeModels) {
            if (!excelExportEntityTypeModels && collection.length > 0) {
                model = { EntityTypeId: collection.models[0].id };
            }
            var exportEntityTypeView = new excelExportEntityTypeView({
                ControlDefinition: this.excelExportEntityTypeViews.length,
                FieldSetCollection: this.fieldSetCollection,
                EntityTypeCollection: collection,
                Model: model
            });
            this.excelExportEntityTypeViews.push(exportEntityTypeView);
            this.$el.find("#excelexport-entitytype-control-container").append(exportEntityTypeView.$el);
            if (model) {
                this.changeEntityTypeExcelExport(excelExportEntityTypeModels);
            }
        },
        changeEntityTypeExcelExport: function (excelExportEntityTypeModels) {
            var self = this;
            var ids = this.relatedLinksCollection ? this.relatedLinksCollection.sortBy(function(relatedLinksModel) {
                return relatedLinksModel.get("Type");
            }).map(function (relatedLinksModel) {
                var id = relatedLinksModel.get("EntityTypeId");
                if (relatedLinksModel.get("Type")) {
                    id += "$$" + relatedLinksModel.get("Type");
                }
                if (relatedLinksModel.get("Id")) {
                    id += "$$" + relatedLinksModel.get("Id");
                }
                return id;
            }) : [];
            var changedAtIndex = -1;
            ids = _.difference(ids, _.map(this.excelExportEntityTypeViews, function (exportEntityTypeView) {
                return exportEntityTypeView.getType();
            }));
            for (var index = 0; index < this.excelExportEntityTypeViews.length; index++) {
                var view = this.excelExportEntityTypeViews[index];
                if (view.hasTypeChanged()) {
                    view.confirmNewType();
                    changedAtIndex = index;
                    break;
                }
            }

            if (changedAtIndex < 0) {
                return;
            }

            var newIndex = changedAtIndex + 1;
            var model = excelExportEntityTypeModels && excelExportEntityTypeModels.length > newIndex
                ? excelExportEntityTypeModels[newIndex]
                : null;

            this.$el.find("#excelexport-entitytype-control-container").children().slice(newIndex).remove();
            this.excelExportEntityTypeViews = this.excelExportEntityTypeViews.slice(0, newIndex);

            if (this.excelExportEntityTypeViews[changedAtIndex].getType() === "(none)") {
                return;
            }

            if (changedAtIndex === 0) {
                this.relatedLinksCollection = new relatedLinkTypesCollection([], {
                     entityTypeId: this.excelExportEntityTypeViews[changedAtIndex].getEntityType()
                });
                this.relatedLinksCollection.fetch({
                    success: function (collection) {
                        if (collection.length) {
                            var defaultConfiguration = new entityTypeModel();
                            defaultConfiguration.set("DisplayName", "(not set)");
                            defaultConfiguration.set("EntityTypeId", "(none)");
                            defaultConfiguration.set("Id", "(none)");
                            collection.add(defaultConfiguration, { at: 0 });
                            self.createExcelExportEntityTypeView(collection, model, model ? excelExportEntityTypeModels : null);
                        }
                    }
                });
            } else if (ids.length > 1) {
                var filteredCollection = new Backbone.Collection(_.map(ids, function (id) {
                    var typeArray = id.split("$$");
                    return self.relatedLinksCollection.get(typeArray[typeArray.length - 1]);
                }));
                self.createExcelExportEntityTypeView(filteredCollection, model, model ? excelExportEntityTypeModels : null);
            }
        },
        doChangeExportConfiguration: function () {
            var self = this;
            var selectedConfiguration = self.$el.find("#export-excel-value-configuration").val();

            if (selectedConfiguration && (selectedConfiguration.length > 1 || selectedConfiguration.length === 1 && selectedConfiguration[0] === this.selectedConfiguration)) {
                return;
            }

            if (selectedConfiguration && selectedConfiguration.length === 1) {
                this.selectedConfiguration = selectedConfiguration[0];
            }

            if (!selectedConfiguration
                || selectedConfiguration.length === 0
                || selectedConfiguration[0] === this.notSetDisplayName) {
                this.render();
                return;
            }

            this.exportConfigurationsCollection = new exportExcelConfigurationCollection();
            this.exportConfigurationsCollection.fetch({
                success: function (collection) {
                    var configurations = collection.findWhere({ Name: selectedConfiguration[0] });
                    self.$el.find("#excelexport-entitytype-control-container").empty();
                    if (configurations) {
                        self.excelExportEntityTypeViews = [];
                        var entityTypeModels = configurations.get("EntityTypeModels");
                        self.createExcelExportEntityTypeView(self.selectedEntityTypeCollection, entityTypeModels[0], entityTypeModels);

                        self.createLanguageCvlControl(configurations.get("Languages"));
                    }

                    self.$el.find("#excel-export-save-configuration").show();
                    self.$el.find("#remove-confiugration-container").show();
                }
            });

        },
        removeConfiguration: function () {
            var self = this;

            var selectedConfiguration = self.$el.find("#export-excel-value-configuration").val();

            if (selectedConfiguration !== this.notSetDisplayName) {
                this.configurationModel = new exportExcelConfigurationModel({ name: selectedConfiguration });
                inRiverUtil.NotifyConfirm("Delete Configuration", "Are you sure you want to delete this configuration?", function () {
                    self.configurationModel.fetch({
                        success: function () {
                            self.configurationModel.destroy({
                                success: function () {
                                    self.render();

                                    self.$el.find("#excel-export-save-configuration").hide();
                                    self.$el.find("#remove-confiugration-container").hide();
                                },
                                error: function (model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        }
                    });
                }, this);
            }
        },
        render: function () {
            if (!(this.entityTypeCollectionReady
                && this.languagesCollectionReady
                && this.fieldSetCollectionReady)) {
                return null;
            }

            var self = this;

            this.template = this.$el.html(_.template(excelExportTemplate));
            this.template.find("#excel-export-save-configuration").hide();
            this.template.find("#remove-confiugration-container").hide();

            this.exportConfigurationsCollection = new exportExcelConfigurationCollection();
            this.exportConfigurationsCollection.fetch({
                success: function () {

                    var defaultConfiguration = new exportExcelModel();
                    defaultConfiguration.attributes.Name = self.notSetDisplayName;
                    self.exportConfigurationsCollection.add(defaultConfiguration, { at: 0 });

                    self.createConfigurationCvlControl(self.notSetDisplayName);
                }
            });

            this.selectedEntityTypeCollection = new Backbone.Collection();
            _.each(this.entityTypeCollection.models, function (entityType) {
                if (_.contains(self.entityTypeIds, entityType.id)) {
                    self.selectedEntityTypeCollection.add(entityType);
                }
            });

            this.excelExportEntityTypeViews = [];
            this.createExcelExportEntityTypeView(this.selectedEntityTypeCollection);

            var selectedLanguages = [];
            _.each(this.languagesCollection.models, function (language) {
                selectedLanguages.push(language.id);
            });
            this.createLanguageCvlControl(selectedLanguages);

            return this;
        }
    });

    return excelExportView;
});