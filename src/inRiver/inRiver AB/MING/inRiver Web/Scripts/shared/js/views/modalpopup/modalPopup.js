﻿define([
    'jquery',
    'underscore',
    'backbone',
    'text!sharedtemplates/modalpopup/modalPopupTemplate.html'],
function ($, _, backbone, modalPopupTemplate) {
    var modalPopup = backbone.View.extend({
        template: null,
        overdiv: null,
        initialize: function (options) {
            this.callbackContext = (options && options.callbackContext) || this;  // If callback context is supplied the onSave and onCancel will have its "this" set to this variable
        },
        popOut: function (backboneView, options) {
            //Options counld contain: size (small, large, preview), usesavebutton (otherwise x), header, description, id. All is optional!

            backboneView.parentPopup = this; // Allow child views to reference the popup

            var popup = this;
            if (options == null) {
                options = {};
            }
            var className = "modal-popup-square";
            var size = "small";

            if (options.size != null) {
                if (options.size === "large" || options.size === "preview") {
                    className = "modal-popup-square-large";
                } else if (options.size === "medium") {
                    className = "modal-popup-square-medium";
                } else if (options.size === "minimal") {
                    className = "modal-popup-square-minimal";
                }
                size = options.size;
            }

            this.onSave = options.onSave || null;
            this.onCancel = options.onCancel || null;

            var usesavebutton = false;
            if (options.usesavebutton != null && options.usesavebutton == true || this.onSave) {
                usesavebutton = true;
                className += " modal-popup-has-buttons";
            }
            var header = "";
            if (options.header != null) {
                header = options.header;
            }
            var description = "";
            if (options.description != null) {
                description = options.description;
            }
            var id = "";
            if (options.id) {
                id = options.id;
            }

            this.$el.html(_.template(modalPopupTemplate, { className: className, usesavebutton: usesavebutton, header: header, description: description, id: id, size: size }));

            //this.square.Code = this;

            this.$el.find("#contentarea").html(backboneView.$el);

            if (usesavebutton) {
                if (options.enableSaveButton) {
                    this.$el.find("#save-form-button").removeAttr("disabled");
                }
                this.$el.delegate("#cancel-form-button", "click", function () {
                    if (popup.onCancel) {
                        popup.onCancel.call(popup.callbackContext, popup, backboneView);
                    } else {
                        window.appHelper.event_bus.trigger('popupcancel');
                    }
                });
                this.$el.delegate("#save-form-button", "click", function () {
                    if (popup.onSave) {
                        popup.onSave.call(popup.callbackContext, popup, backboneView);
                    } else {
                        window.appHelper.event_bus.trigger('popupsave');
                    }
                });
            } else {
                $("body").delegate("#close-icon", "click", function () {
                    popup.close();
                });
            }

            //closebtn.innerHTML = "Close";
            //if (closeButtonClassName) {
            //    closebtn.className = closeButtonClassName; 
            //} else {
            //    closebtn.className = "modal-popup-close-button";
            //}
            //this.square.appendChild(closebtn);

            $("body").append(this.$el);

            window.appHelper.event_bus.off('closepopup');
            this.listenTo(window.appHelper.event_bus, 'closepopup', this.popIn);
            return this;
        },
        popIn: function () {
            $(".overdiv").remove();
            $(".modal-popup-square").remove();
            $(".modal-popup-square-large").remove();
            $(".modal-popup-square-medium").remove();
            $(".modal-popup-square-minimal").remove();
        },
        close: function () {
            appHelper.event_bus.trigger('closepopup');
        }
    });

    return modalPopup;
});