﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/entityTypes/entityTypeCollection',
  'sharedjs/models/setting/SettingModel',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!sharedtemplates/create/entityTypeListTemplate.html',
  'text!sharedtemplates/create/entityTypeTemplate.html'
], function ($, _, Backbone, modalPopup, inRiverUtil, entityTypeCollection, settingModel, entityDetailSettingWrapperView, entityTypeListTemplate, entityTypeTemplate) {

    var entityCreateView = Backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            var self = this;

            this.contextmenu = options.contextmenu;
            this.selectedTypes = options.selectedTypes;
            this.showInWorkareaOnSave = true;

            if (options.showInWorkareaOnSave != null) {
                this.showInWorkareaOnSave = options.showInWorkareaOnSave;
            }

            var onDataHandler = function (collection) {
                self.render();
            };

            self.collection = new entityTypeCollection();
            self.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #cancel-button": "onClose",
            "click .entitytypecard": "onEntityTypeSelected",
            "click .emptyWorkArea": "onOpenEmptyWorkarea"
        },
        onOpenEmptyWorkarea: function () {
            this.goTo("workarea?title=New workarea");
        },
        onEntityTypeSelected: function (e) {
            var id = e.currentTarget.id;

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: id,
                showInWorkareaOnSave: this.showInWorkareaOnSave
            });
            this.contextmenu.onToggleTools();
        },
        onClose: function () {
            $("#modalWindow").hide();
            this.remove();
        },
        render: function () {
            this.$el.html(_.template(entityTypeListTemplate));
            var that = this;
            var id = "DEFAULT_CREATE_ENTITIES";
            var setModel = new settingModel();
            setModel.url = "/api/setting/" + id;
            setModel.fetch({
                success: function (model, response) {
                    var entityArray = response.split(',');
                    if (that.selectedTypes != null && that.selectedTypes.length > 0) {
                        var newArray = new Array();
                        _.each(that.selectedTypes, function (selectedType) {
                            var found = false;
                            for (var i = 0; i < entityArray.length; i++) {
                                if (entityArray[i] == selectedType) {
                                    found = true;
                                    break;
                                }
                            }

                            if (found) {
                                newArray.push(selectedType);
                            }
                        });

                        entityArray = newArray;
                    }
                    var container = that.$el.find("#entity-type-area");
                    _.each(that.collection.models, function (model) {
                        if (jQuery.inArray(model.attributes.Id, entityArray) !== -1) {
                            var subsectionEl = $(_.template(entityTypeTemplate, { model: model.toJSON() }));
                            container.append(subsectionEl.html());
                        }
                    });

                    return that;
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        }
    });

    return entityCreateView;
});