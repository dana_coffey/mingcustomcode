﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/syndicate/distributionFormatModel'
], function ($, _, backbone, distributionFormatModel) {
    var distributionFormatCollection = backbone.Collection.extend({
        model: distributionFormatModel,
        url: '/api/syndication/distributionFormat'
    });

    return distributionFormatCollection;
});

