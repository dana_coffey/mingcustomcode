﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var relationByTypeModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.id = options.Id;
        },
        urlRoot: '/api/relation'
    });

    return relationByTypeModel;

});
