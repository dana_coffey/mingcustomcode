﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/field/conflictedFieldModel',
  'sharedjs/views/entityview/entityDetailFieldView',
  'sharedjs/views/fieldrevision/fieldRevisionView',
  'sharedjs/views/inputcontrols/noneEditModeControlView',
  'sharedjs/views/entityview/entityConflictView',
  'text!sharedtemplates/entity/entityDetailTemplate.html',
  'text!sharedtemplates/entity/entityCategoryTemplate.html',
  'text!sharedtemplates/entity/entityDataFieldTemplate.html',
  'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, modalPopup, fieldModel, conflictedFieldModel, entityDetailFieldView, fieldRevisionView, noneEditModeControlView, entityConflictView, entityDetailTemplate, entityCategoryTemplate, entityDataFieldTemplate, inRiverUtil) {

    var entityDetailView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.detailfields = [];
            this.entityModel = options.model;
            // load all fields
            this.model = new fieldModel({ id: this.entityModel.id });
            this.model.fetch(
            {
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #saveAll": "saveAll",
            "click #expand": "expandAll",
            "click #collapse": "collapseAll"
        },
        fieldValueUpdated: function (fieldTypeId, fieldValue) {
            this.$el.find("#saveAll").show();
            this.updateFieldValue(fieldTypeId, fieldValue);
            this.editedFields = jQuery.grep(this.editedFields, function (value) {
                return value != fieldTypeId;
            });
            this.editedFields.push(fieldTypeId);
        },
        fieldValueNotUpdated: function (fieldTypeId, fieldValue) {
            this.editedFields = jQuery.grep(this.editedFields, function (value) {
                return value != fieldTypeId;
            });
            if (this.editedFields.length == 0) {
                this.$el.find("#saveAll").hide();
            }
            this.updateFieldValue(fieldTypeId, fieldValue);
        },
        fieldValueNotActive: function (fieldTypeId, fieldValue, entityId) {
            if (entityId == this.model.id) {
                this.fieldValueNotUpdated(fieldTypeId, fieldValue);
            }
        },
        updateFieldValue: function (fieldTypeId, value) {
            _.each(this.model.get("Fields"), function (field) {
                if (field.FieldType == fieldTypeId) {
                    field.Value = value;
                }
            });
        },
        saveAll: function () {
            window.appHelper.fieldsToSave = 0;
            var that = this;
            //this.model.save({}, {
            //    success: function (model) {
            //        that.model = model;

            //        window.appHelper.event_bus.trigger("entityupdated", model.id);


            //        that.render();
            //    },
            //    error: function (model, response) {
            //        inRiverUtil.OnErrors(model, response);
            //    }
            //});


            //First validate fields. 
            that.conflictedFieldModel = new conflictedFieldModel({ id: this.model.id });
            that.conflictedFieldModel.attributes = this.model.attributes;

            that.conflictedFieldModel.save({}, {
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                success: function (model, response) {
                    if (response.Result) {
                        that.modelid = that.model.id;
                        that.model.save({}, {
                            success: function (xmodel) {
                                if (that.modelid < 1) {
                                    window.appHelper.event_bus.trigger('entitycreated', xmodel);
                                    inRiverUtil.Notify(xmodel.attributes.EntityTypeDisplayName + " successfully created");
                                    that.searchHandled = true;

                                    if (that.showInWorkareaOnSave) {
                                        $.post("/api/tools/compress", { '': xmodel.get("Id") }).done(function (result) {
                                            that.goTo("workareasearch?title=" + xmodel.get("DisplayName") + "&compressedEntities=" + result);
                                        });
                                    }
                                } else {
                                    window.appHelper.event_bus.trigger('entityupdated', xmodel.id);
                                }
                                this.model = xmodel;
                                window.appHelper.event_bus.trigger("entityupdated", xmodel.id);

                                that.render();
                            },
                            error: function (errorModel, errorResponse) {
                                inRiverUtil.OnErrors(errorModel, errorResponse);
                            }
                        });
                    } else {
                        var modal = new modalPopup();
                        modal.popOut(new entityConflictView({ model: that.model, validationResult: response, parent: that }), { size: "medium", header: model.attributes.EntityTypeDisplayName + " " + model.attributes.DisplayName });
                    }
                }
            });

        },
        onConflictSave: function () {
            var that = this;

            this.model.save({}, {
                success: function (xmodel) {
                    appHelper.event_bus.trigger('closepopup');

                    window.appHelper.event_bus.trigger('entityupdated', xmodel.id);
                    that.render(); 

                },
                error: function (model, response) {
                    appHelper.event_bus.trigger('closepopup');

                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        expandAll: function () {
            var categories = this.$el.find(".category-section-title");
            $.each(categories, function () {

                $(this).parent().find("#category-field-container").show();
                $(this).find("i.fa-minus-circle").show();
                $(this).find("i.fa-plus-circle").hide();

            });
        },
        collapseAll: function () {
            var categories = this.$el.find(".category-section-title");
            $.each(categories, function () {

                $(this).parent().find("#category-field-container").hide();
                $(this).find("i.fa-minus-circle").hide();
                $(this).find("i.fa-plus-circle").show();

            });
        },
        onClose: function () {
        },
        render: function () {
            window.appHelper.i = 0;
            var that = this;
            this.$el.html(_.template(entityDetailTemplate, { data: this.entityModel.toJSON() }));

            var isLocked = inRiverUtil.isLocked(this.model.attributes.LockedBy);

            var categories = _.groupBy(this.model.get("Fields"), "CategoryId");
            var numberOfCategories = 0;
            var keys = _.keys(categories);
            var numberOfFieldsInCategory = 0;
            var previousField;
            if (keys.length > 1) {
                _.each(keys, function (key) {
                    numberOfFieldsInCategory = 0;
                    numberOfCategories++;

                    var fieldView;
                    var sectionEl = $(_.template(entityCategoryTemplate, { sectionName: categories[key][0].CategoryName, sectionId: categories[key][0].CategoryId }));
                    var fieldContainer = sectionEl.find("#category-field-container");
                    _.each(categories[key], function (field) {
                        if (numberOfFieldsInCategory == 0 && previousField == null && !field.IsReadOnly) {
                            previousField = field;
                        }

                        if (field.IsReadOnly) {
                            if (!field.IsHidden && (!field.ExcludeFromDefaultView || (field.ExcludeFromDefaultView && field.FieldSets.split(',').indexOf(that.model.get("FieldSet")) > -1))) {
                                fieldView = $(_.template(entityDataFieldTemplate, { Id: field.FieldType, Name: field.FieldTypeDisplayName, Description: field.FieldTypeDescription, Value: field.DisplayValue, ExcludeFromDefaultView: field.ExcludeFromDefaultView }));
                                var control = new noneEditModeControlView({ Id: field.FieldType, Value: field.DisplayValue, ReadOnly: field.IsReadOnly });
                                fieldView.find(".value-placeholder").append(control.render().el);
                                fieldContainer.append(fieldView);
                                numberOfFieldsInCategory++;
                            }
                        } else {
                            if (!field.IsHidden && (!field.ExcludeFromDefaultView || (field.ExcludeFromDefaultView && field.FieldSets.split(',').indexOf(that.model.get("FieldSet")) > -1))) {
                                fieldView = new entityDetailFieldView({
                                    field: field,
                                    previousField: previousField
                                    //fieldset: that.model.get("FieldSet")
                                });

                                that.detailfields.push(fieldView);
                                fieldContainer.append(fieldView.$el);
                                previousField = field;

                                numberOfFieldsInCategory++;
                            }
                        }
                    });

                    if (numberOfFieldsInCategory > 0) {
                        that.$el.find("#details-row-container").append(sectionEl);
                    }
                });
            } else {
                numberOfFieldsInCategory = 0;
                var fieldView;

                var container = this.$el.find("#details-row-container");
                _.each(this.model.get("Fields"), function (field) {
                    if (numberOfFieldsInCategory == 0 && !field.IsReadOnly) {
                        previousField = field;
                    }

                    //var fieldView = new entityDetailFieldView({
                    //    field: field,
                    //    previousField: previousField
                    //    //fieldset: that.model.get("FieldSet")
                    //});

                    //that.detailfields.push(fieldView);
                    //container.append(fieldView.$el);
                    //previousField = field;

                    //numberOfFieldsInCategory++;

                    if (field.IsReadOnly) {
                        if (!field.IsHidden && (!field.ExcludeFromDefaultView || (field.ExcludeFromDefaultView && field.FieldSets.split(',').indexOf(that.model.get("FieldSet")) > -1))) {
                            fieldView = $(_.template(entityDataFieldTemplate, { Id: field.FieldType, Name: field.FieldTypeDisplayName, Description: field.FieldTypeDescription, Value: field.DisplayValue, ExcludeFromDefaultView: field.ExcludeFromDefaultView }));
                            var control = new noneEditModeControlView({ Id: field.FieldType, Value: field.DisplayValue, ReadOnly: field.IsReadOnly });
                            fieldView.find(".value-placeholder").append(control.render().el);
                            container.append(fieldView);
                            numberOfFieldsInCategory++;
                        }
                    } else {
                        if (!field.IsHidden && (!field.ExcludeFromDefaultView || (field.ExcludeFromDefaultView && field.FieldSets.split(',').indexOf(that.model.get("FieldSet")) > -1))) {
                            fieldView = new entityDetailFieldView({
                                field: field,
                                previousField: previousField
                                //fieldset: that.model.get("FieldSet")
                            });

                            that.detailfields.push(fieldView);
                            container.append(fieldView.$el);
                            previousField = field;

                            numberOfFieldsInCategory++;
                        }
                    }


                });
                this.$el.find("#collapse").hide();
                this.$el.find("#expand").hide();
            }

            var ids = that.$el.find(".category-section-wrap");

            for (var index = 0; index < ids.length; ++index) {
                showCategories('#' + ids[index].id);
            }

            function showCategories(id) {
                id = id.replace(" ", "-");

                var $id = that.$el.find(id);
                var $heading = that.$el.find(id + " .category-section-title");
                var $box = $id.find("#category-field-container");

                var $showTask = that.$el.find(id + '-show i');

                //$showTask.toggle();
                $heading.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            // Use another model to update.
            this.modifiedModel = _.clone(this.model);

            this.listenTo(window.appHelper.event_bus, 'fieldvalueupdated', this.fieldValueUpdated);
            this.listenTo(window.appHelper.event_bus, 'fieldvaluenotupdated', this.fieldValueNotUpdated);
            //this.listenTo(window.appHelper.event_bus, 'fieldvalueinvalid', this.fieldValueInvalid);

            // Only to keep the save all button visual in the right time.
            this.listenTo(window.appHelper.event_bus, 'fieldvaluesaved', this.fieldValueNotActive);
            this.editedFields = [];
            return this;
        }
    });

    return entityDetailView;
});
