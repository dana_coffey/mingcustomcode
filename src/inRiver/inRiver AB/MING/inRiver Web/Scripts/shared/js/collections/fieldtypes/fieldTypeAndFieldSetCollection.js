﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/fieldtype/FieldTypeAndFieldSetModel'
], function ($, _, backbone, fieldTypeAndFieldSetModel) {
    var relationTypeCollection = backbone.Collection.extend({
        model: fieldTypeAndFieldSetModel,
        initialize: function (models, options) {
            this.entityTypeId = options.entityTypeId;
        },
        url: function () {
            return '/api/FieldSetsForEntityType?entityTypeId=' + this.entityTypeId;
        }
    });

    return relationTypeCollection;
});