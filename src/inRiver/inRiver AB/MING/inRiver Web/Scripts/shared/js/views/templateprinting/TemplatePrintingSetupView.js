﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!sharedtemplates/templateprinting/TemplatePrintingSetupViewTemplate.html'
], function (
    $,
    _,
    Backbone,
    TemplatePrintingSetupViewTemplate) {

    var TemplatePrintingSetupView = Backbone.View.extend({
        template: _.template(TemplatePrintingSetupViewTemplate),
        initialize: function (options) {
            this.languageCollection = new Backbone.Collection(appData.languages, { model: Backbone.Model.extend({ idAttribute: "Name" }) });
            this.htmlTemplateCollection = new Backbone.Collection(this.options.templates, { model: Backbone.Model.extend({ idAttribute: "Id" }) });
            this.render();
        },
        events: {
            "click #create-pdf": "onCreatePdf",
            "click #show-preview": "onShowPreview",
            "click #cancel": "onClosePopup",
            "change #main-template": "onChangeMainTemplate",
        },
        onCreatePdf: function () {
            var that = this;
            var entities = [this.options.entityId];

            var data = {
                entities: entities,
                language: this.$el.find("#language").val(),
                mainTemplate: this.$el.find("#main-template").val(),
            }

            data.pdfFileName = this.htmlTemplateCollection.get(data.mainTemplate).get("Name") + " - " + this.options.entityDisplayName;

            $.fileDownload("/api/tools/templatepdfprint/", {
                httpMethod: "GET",
                data: data,
                prepareCallback: function (url) {
                    that.$el.find("#template-printing-setup-wrap").html("Preparing...");
                },
                successCallback: function (url) {
                    appHelper.event_bus.trigger('closepopup');
                },
                failCallback: function (responseHtml, url) {
                    appHelper.event_bus.trigger('closepopup');
                    alert("PDF Download failed...");
                }
            });
        },
        onShowPreview: function () {
            var url = "/app/templateprinting?templateId=" + this.$el.find("#main-template").val() + "&entities=" + this.options.entityId + "&language=" + this.$el.find("#language").val();
            window.open(url, '_blank');
            appHelper.event_bus.trigger('closepopup');
        },
        onClosePopup: function () {
            this.close();
        },
        onClose: function () {

        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ mode: this.options.mode }));

            var currentLanguage = appHelper.userLanguage;
            var languageOptionsHtml = "";
            this.languageCollection.each(function (model) {
                languageOptionsHtml += "<option value='" + model.get("Name") + "'" + (model.get("Name") === currentLanguage ? " selected" : "") + ">" + model.get("DisplayName") + "</option>";
            });
            this.$el.find("#language").html(languageOptionsHtml);

            this.htmlTemplateCollection.each(function (model) {
                that.$el.find("#main-template").append("<option value='" + model.get("Id") + "'>" + model.get("Name") + "</option>");
            });
            that.$el.find("#main-template").find('option:eq(0)').prop('selected', true);

            return this; // enable chained calls
        }
    });

    return TemplatePrintingSetupView;
});