﻿define([
  'jquery',
  'underscore',
  'backbone',
  "jquery-multiple-select",
  'text!sharedtemplates/workarea/workAreaExcelExportCvlControlTemplate.html'
], function ($, _, backbone, multipleSelect, cvlControlTemplate) {
    var workAreaExcelExportCvlControlView = backbone.View.extend({
        initialize: function (options) {
            this.controlDefinition = options.ControlDefinition;
            this.values = options.Values;
            this.multivalue = options.Multivalue;
            this.inputField = "#workarea-export-excel-value-" + this.controlDefinition;
            this.possibleValues = options.PossibleValues;
            
            this.render();
        },
        render: function () {
            var self = this;
            var template = _.template(cvlControlTemplate, { Definition: this.controlDefinition });
            this.setElement(template, true);

            if (this.multivalue) {
                this.$el.find(self.inputField).multipleSelect({
                    filter: true,
                    width: 270,
                    onBlur: function () {
                        self.onLostFocus();
                    }
                });

                if (this.values != null) {
                    _.each(this.values.models, function (value) {

                        var key;
                        var text;

                        if (self.controlDefinition == "select-fieldset" ||
                            self.controlDefinition == "include-fieldset") {
                            key = value.attributes.Id;
                            text = value.attributes.Id;
                        }
                        else if (self.controlDefinition == "select-languages") {
                            key = value.id;
                            text = value.attributes.DisplayName;
                        }
                        else {
                            key = value.id;
                            text = value.attributes.displayname;
                        }

                        if (self.possibleValues != undefined) {

                            if (!_.isArray(self.possibleValues)) {
                                if (self.possibleValues == key) {
                                    self.$el.find(self.inputField).append('<option value="' + key + '" selected>' + text + '</option>');
                                } else {
                                    self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                                }
                            } else {
                                if (_.contains(self.possibleValues, key)) {
                                    self.$el.find(self.inputField).append('<option value="' + key + '" selected>' + text + '</option>');
                                } else {
                                    self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                                }
                            }
                        } else {
                            self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                        }

                    });
                } else {
                    if (self.controlDefinition == "include-field" ||
                        self.controlDefinition == "include-language") {
                        self.$el.find(self.inputField).append('<option value="" disabled>Please Select Included Type</option>');
                    } else {
                        self.$el.find(self.inputField).append('<option value="" disabled>Please Select Type</option>');
                    }
                    
                }

            } else {
                this.$el.find(self.inputField).multipleSelect({
                    single: true,
                    width: 270,
                    onBlur: function () {
                        self.onLostFocus();
                    }
                });
                if (this.values != null) {
                    _.each(this.values.models, function(value) {

                        var key;
                        var text;

                        if (self.controlDefinition == "include-type") {
                            if (value.attributes.Type != undefined) {
                                key = value.attributes.EntityTypeId + "$$" + value.attributes.Type + "$$" + value.attributes.Id;
                            } else {
                                key = value.attributes.EntityTypeId;
                            }
                            
                            text = value.attributes.DisplayName;
                        }
                        else if (self.controlDefinition == "configuration") {
                            key = value.attributes.Name;
                            text = value.attributes.Name;
                        }
                        else if (self.controlDefinition == "select-type") {
                            key = value.id;
                            text = value.attributes.DisplayName;
                        }
                        else{
                            key = value.id;
                            text = value.attributes.displayname;
                        }

                        if (self.possibleValues != undefined) {

                            if (!_.isArray(self.possibleValues)) {
                                if (self.possibleValues == key) {
                                    self.$el.find(self.inputField).append('<option value="' + key + '" selected>' + text + '</option>');
                                } else {
                                    self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                                }
                            } else {
                                if (_.contains(self.possibleValues, key)) {
                                    self.$el.find(self.inputField).append('<option value="' + key + '" selected>' + text + '</option>');
                                } else {
                                    self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                                }
                            }
                        } else {
                            self.$el.find(self.inputField).append('<option value="' + key + '">' + text + '</option>');
                        }


                    });
                } else {
                    self.$el.find(self.inputField).append('<option value="" disabled>Please Select Type</option>');
                }
            }

            this.$el.find(self.inputField).multipleSelect('refresh');
            this.$el.find(self.inputField).multipleSelect('focus');

            return this;
        },
        onLostFocus: function (e) {
            if (e != null) {
                e.stopPropagation();
            }
        }
    });

    return workAreaExcelExportCvlControlView;
});

