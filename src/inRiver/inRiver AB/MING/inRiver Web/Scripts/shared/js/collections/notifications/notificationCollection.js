define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, backbone, channelModel) {
    var channelCollection = backbone.Collection.extend({
        //model: channelModel,
        url: '/api/notification'
    });

    return channelCollection;
});
