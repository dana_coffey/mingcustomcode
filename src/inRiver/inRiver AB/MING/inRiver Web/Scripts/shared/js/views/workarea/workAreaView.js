﻿define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui-touch-punch',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'jstree',
    'alertify',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/models/workarea/workareaModel',
    'sharedjs/collections/workareas/workareaCollection',
    'sharedjs/collections/relations/relationTypeCollection',
    'sharedjs/collections/entities/entityCollection',
    'sharedjs/collections/entities/entityTableCollection',
    'sharedjs/views/entitycard/entitycardView',
    'sharedjs/views/workarea/quickSearchWorkAreaView',
    'sharedjs/views/excelexport/excelExportView',
    'sharedjs/views/advancedsearch/queryEditorView',
    'sharedjs/views/panelsplitter/panelBorders',
    'sharedjs/views/entityview/entityTableView',
    'sharedjs/views/contextmenu/contextMenuView',
    'sharedjs/views/entityview/entityDetailSettingWrapperView',
    'sharedjs/views/massupdate/massUpdateWizzardView',
    'text!sharedtemplates/workarea/workareaHeaderTemplate.html',
    'text!sharedtemplates/workarea/workareaTemplate.html',
    'text!sharedtemplates/workarea/workareaMultiselectCheckboxControlTemplate.html',
    'text!sharedtemplates/workarea/entitytypeGroupTemplate.html',
    'text!sharedtemplates/relation/draggableRelationCardTemplate.html',
    'text!sharedtemplates/relation/draggableMultiRelationCardsTemplate.html'
], function ($, _, backbone, jqtp, modalPopup, inRiverUtil, jstree, alertify, relationModel, entityModel, workareaModel, workareaCollection, relationTypeCollection, entityCollection, entityTableCollection, entitycardView, quickSearchWorkAreaView, excelExportView, queryEditorView, panelBorders, entityTableView, contextMenuView, entityDetailSettingWrapperView, massUpdateWizzardView, workareaHeaderTemplate, workareaTemplate, workareaMultiselectCheckboxControlTemplate, entitytypeGroupTemplate, draggableRelationCardTemplate, draggableMultiRelationCardsTemplate) {

    var workAreaView = backbone.View.extend({
        initialize: function (data) {

            //States: 
            this.position = data.id;
            this.state = data.state;

            if (this.state == undefined) {
                this.state = "Active";
            }

            if (!data.mode) {
                this.mode = "card";
            }
            else {
                this.mode = data.mode;
            }


            this.includeFields = false;

            if (window.appSession.workAreadViewMode != null) {
                this.mode = window.appSession.workAreadViewMode;
            }

            this.collection = new entityCollection();
            this.handler = data.handler;
            //$("#loading-spinner-container").show();


            this.title = data.title;
            this.searchPhrase = data.searchPhrase;
            this.advsearchPhrase = data.advsearchPhrase;

            this.compressedEntities = data.compressedEntities;
            this.entityIds = data.entityIds;

            if (data.collection != undefined) {
                this.collection = data.collection;
            }

            this.workareaId = data.workareaId;
            this.isQuery = data.isQuery;

            this.hasWorkareaSearch = data.hasworkareaSearch;
            this.waCounter = window.appHelper.activeWorkareas;
            if (this.waCounter == null) {
                this.waCounter = 1;
                window.appHelper.activeWorkareas = 1;
            }

            this.supressSearch = data.supressSearch;

            if (!this.position) {
                this.position = 1;
            }

            this.entityCount = 0;
            this.page = 0;
            this.entitiesPerPage = 70;
            this.cards = [];

            _.bindAll(this, "loadNextPage");

            this.renderingCounter = 0;
            this.allrendered = false;

            if (this.compressedEntities != null) {
                this.performLoadCompressedEntities(data);
            }

            this.render();
            this.listenTo(window.appHelper.event_bus, 'cardrendered', this.cardRendered);
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);


            this.searching = false;
        },
        events: {
            "click span#editquery": "editQuery",
            "click span#exportExcelWorkarea": "showExportExcelDialog",
            "click span#massUpdate": "showMassUpdateDialog",
            "click span#removeFromWorkArea": "removeSelectedCardsFromWorkArea",
            "click #openworkarea": "showSavedWorkAreas",
            "click span#saveworkarea": "showSaveWorkAreaDialog",
            "click span#closeSavingWorkarea": "clickSomewhere",
            "click span#updateWorkarea": "updateWorkArea",
            "click span#deleteworkarea": "deleteWorkArea",
            "click span#deleteselected": "deleteSelectedEntities",
            "click span#addworkarea": "addWorkArea",
            "click span#closeworkarea": "closeWorkArea",
            "click span#switchworkarea": "switchWorkArea",
            "click span#saveWorkarea": "saveWorkArea",
            "click #showMode": "showMore",
            "click #clearWorkarea": "clearWorkarea",
            "click #activeworkarea": "activateWorkarea",
            "click #toggleView": "toggleView",
            "click #fieldsIncludedInTable": "fieldsIncludedInTable",
            "click #savedialog": "doNothing",
            "click .savedworkarea": "doNothing",
            "click .jstree": "doNothing",
            "click .jstree-icon": "doNothing",
            "click": "clickSomewhere",
            "click span#selectAllEntitycards": "selectAllEntityCards",
            "click span#deselectAllEntitycards": "deselectAllEntityCards",
            "click span#deselectEntitycards": "deselectAllEntityCards",
            "click span#addTaskForSelectedEntities": "addTaskForSelectedEntities"
        },
        doNothing: function (e) {
            e.stopPropagation();
        },
        clickSomewhere: function (e) {
            if (e.target.className.indexOf("jstree") > -1) {
                return;
            }

            this.$el.find("#exportToExceldialog").hide();
            this.$el.find("#savedialog").hide();
            this.$el.find(".savedworkarea").hide();
        },
        activateWorkarea: function (e) {
            e.stopPropagation();
            if (this.state == "Active") {
                return;
            } else {
                this.handler.switchActive(this);
            }

        },
        toggleView: function (e) {
            e.stopPropagation();

            this.setMultiSelectionSession();

            if (this.mode == "card") {
                this.mode = "table";
            } else {
                this.mode = "card";
            }
            this.setVisualMode();
            this.showResult();

            this.getMultiSelectionSession();
        },
        addTaskForSelectedEntities: function (e) {
            e.stopPropagation();

            var checkedCards = this.getSelectedEntityIds();
            if (checkedCards.length == 0) {
                inRiverUtil.Notify("You need to select at least one entity");
                return;
            }

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });
        },
        taskAddedNowLinkEntity: function (model) {
            this.stopListening(window.appHelper.event_bus);
            var checkedCards = this.getSelectedEntityIds();
            var count = checkedCards.length;
            _.each(this.cards, function (entityCard) {
                if (!entityCard.card.checked) {
                    return;
                }

                var linktype = entityCard.card.TaskRelationType.attributes.Id;
                var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: entityCard.card.model.id, LinkEntityId: null });
                newModel.save([], {
                    success: function () {
                        count = count - 1;
                        if (count == 0) {
                            inRiverUtil.Notify("Relationship with task done.");
                        }
                    },
                    error: function (result, response) {
                        count = count - 1;
                        inRiverUtil.OnErrors(result, response);
                    }
                });
            });
        },
        setMultiSelectionSession: function () {
            var checkedIds = [];
            if (this.cards && this.cards.length) {
                _.each(this.cards, function (cardElement) {
                    if (cardElement.card.checked) {
                        checkedIds.push(cardElement.card.model.id);
                    }
                });
            } else if (this.tableView) {
                checkedIds = this.tableView.getSelectedEntityIds();
            }
            window.appSession.multiSelection = { workareaId: this.workareaId, checkedIds: checkedIds }
        },
        getMultiSelectionSession: function () {
            if (window.appSession.multiSelection == null ||
                window.appSession.multiSelection.workareaId != this.workareaId ||
                window.appSession.multiSelection.checkedIds.length == 0) {
                return;
            }

            var checkedIds = window.appSession.multiSelection.checkedIds;
            if (this.mode == "card") {
                _.each(this.cards, function (cardElement) {
                    _.each(checkedIds, function (id) {
                        if (cardElement.card.model.id == id) {
                            cardElement.card.setChecked(true);
                        }
                    });
                });
            }
        },
        fieldsIncludedInTable: function (e) {
            e.stopPropagation();
            var icon = this.$el.find("#fieldsIncludedInTable i");
            if (this.includeFields) {
                this.includeFields = false;
                icon.removeClass("fa-check-square-o");
                icon.addClass("fa-square-o");
            } else {
                this.includeFields = true;
                icon.removeClass("fa-square-o");
                icon.addClass("fa-check-square-o");
            }

            this.showResult();
        },
        setVisualMode: function () {
            if (this.mode == "table") {
                var groups = this.collection.groupBy("EntityTypeDisplayName");
                var keys = _.keys(groups);
                if (keys.length == 1) {
                    this.$el.find("#fieldsIncludedInTable").show();
                    if (this.includeFields) {
                        this.$el.find("#fieldsIncludedInTable i").removeClass("fa-square-o");
                        this.$el.find("#fieldsIncludedInTable i").addClass("fa-check-square-o");
                    } else {
                        this.$el.find("#fieldsIncludedInTable i").removeClass("fa-check-square-o");
                        this.$el.find("#fieldsIncludedInTable i").addClass("fa-square-o");
                    }
                } else {
                    this.$el.find("#fieldsIncludedInTable").hide();
                }
                this.$el.find("#toggleView i").removeClass("fa-list");
                this.$el.find("#toggleView i").addClass("fa-th-large");
                this.$el.find("#toggleViewText").text("Card view");

                this.$el.find("#workarea-multiselect-container").hide();
            } else { // Card view
                this.$el.find("#fieldsIncludedInTable").hide();
                this.$el.find("#toggleView i").removeClass("fa-th-large");
                this.$el.find("#toggleView i").addClass("fa-list");
                this.$el.find("#toggleViewText").text("Table view");
                this.$el.find("#workarea-multiselect-container").show();

            }

            this.$el.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "selectAllEntitycards", title: "Select all entities", icon: "fa fa-square-o", text: "Select All" }));
            window.appSession.workAreadViewMode = this.mode;
        },
        setState: function (state) {
            this.state = state;
            if (this.handler.numberOfWorkareas == 1) {
                this.$el.find("#search-result-query").removeClass("activeworkarea");
                this.$el.find("#search-result-query").removeClass("passiveworkarea");
                this.$el.find("#search-result-query").removeClass("singleworkarea");
                this.$el.find("#search-result-query").addClass("singleworkarea");

                this.$el.find("#activeworkarea").hide();


            } else if (this.state == "Passive") {
                //Grå, ingen text.
                this.$el.find("#search-result-query").removeClass("activeworkarea");
                this.$el.find("#search-result-query").removeClass("passiveworkarea");
                this.$el.find("#search-result-query").removeClass("singleworkarea");
                this.$el.find("#search-result-query").addClass("passiveworkarea");
                if (this.handler.mode == "wide") {
                    this.$el.find("#activeworkarea").show();
                    this.$el.find("#activeworkarea i").removeClass("fa-square-o");
                    this.$el.find("#activeworkarea i").removeClass("fa-check-square-o");
                    this.$el.find("#activeworkarea i").addClass("fa-square-o");
                } else {
                    this.$el.find("#activeworkarea").hide();
                }

            } else {
                this.$el.find("#search-result-query").removeClass("activeworkarea");
                this.$el.find("#search-result-query").removeClass("passiveworkarea");
                this.$el.find("#search-result-query").removeClass("singleworkarea");
                this.$el.find("#search-result-query").addClass("activeworkarea");


                if (this.handler.mode == "wide") {
                    this.$el.find("#activeworkarea").show();
                    this.$el.find("#activeworkarea i").removeClass("fa-square-o");
                    this.$el.find("#activeworkarea i").removeClass("fa-check-square-o");
                    this.$el.find("#activeworkarea i").addClass("fa-check-square-o");
                } else {
                    this.$el.find("#activeworkarea").hide();
                }

            }
        },
        getWorkAreaState: function () {
            return {
                title: this.title,
                searchPhrase: this.searchPhrase,
                advsearchPhrase: this.advsearchPhrase,
                workareaId: this.workareaId,
                entityIds: this.entityIds,
                compressedEntities: this.compressedEntities,
                position: this.position,
                state: this.state,
                isQuery: this.isQuery
            };
        },
        setWorkAreaState: function (data) {
            this.title = data.title;
            this.searchPhrase = data.searchPhrase;
            this.advsearchPhrase = data.advsearchPhrase;
            this.workareaId = data.workareaId;
            this.entityIds = data.entityIds;
            this.compressedEntities = data.compressedEntities;
            this.position = data.position;
            this.state = data.state;

            performSearch(data);
        },
        addWorkArea: function () {
            this.handler.addWorkarea({ mode: this.mode });
        },
        switchWorkArea: function () {
            this.handler.switchWorkArea();
        },
        closeWorkArea: function () {
            this.handler.closeWorkArea(this.position);
        },
        deleteWorkArea: function () {
            this.handler.deleteWorkArea(this.position, this.workareaId);
        },
        getSelectedCards: function () {
            return _.filter(this.cards, function (card) {
                return card.card.checked;
            });
        },
        getSelectedEntityCards: function () {
            return _.map(this.getSelectedCards(), function (card) {
                return card.card;
            });
        },
        getSelectedModels: function () {
            if (this.mode === "table") {
                if (!this.tableView && this.handler.mainWorkAreaView) {
                    return _.map(this.handler.mainWorkAreaView.tableView.getSelectedEntities(), function (entity) {
                        return entity;
                    });
                }
                else {

                    if (this.tableView)
                    {
                        return _.map(this.tableView.getSelectedEntities(), function (entity) {
                            return entity;
                        });
                    }
                    else {
                        return [];
                    }
                }
            } else {

                if (this.handler.mainWorkAreaView) {
                    return _.map(this.handler.mainWorkAreaView.getSelectedCards(), function (card) {
                        return card.card.model;
                    });
                }
                else {
                    return _.map(this.getSelectedCards(), function (card) {
                        return card.card.model;
                    });
                }

            }

        },
        getSelectedEntityIds: function () {
            if (this.mode === "table") {
                return this.tableView.getSelectedEntityIds();
            } else {
                return _.map(this.getSelectedCards(), function (card) {
                    return card.card.model.attributes.Id;
                });
            }
        },
        deleteSelectedEntities: function () {
            var selectedCards;
            if (this.mode === "table") {
                var models = this.tableView.getSelectedEntities();
                selectedCards = _.map(models, function (model) {
                    return { model: model };
                });
            } else {
                selectedCards = this.getSelectedEntityCards();
            }
            if (selectedCards.length) {
                inRiverUtil.deleteEntities(selectedCards);
            }
        },
        onConfirmDeleteEntities: function (cards) {
            _.each(cards, function (card) {
                if (card.card.checked) {
                    inRiverUtil.onConfirmDelete(card.card);
                }
            });
        },
        getWorkareaEntities: function () {
            return this.collection.pluck("Id");
        },
        saveWorkArea: function () {
            //Check if data entered.
            var id = this.savetreeid;

            var selectedNode = this.$el.find(id).jstree(true).get_selected(false);
            var name = this.$el.find("#workareaname").val();
            var self = this;

            if (selectedNode.length < 1 || (name == null || name == "")) {
                inRiverUtil.NotifyError("Missing requirements.", "Name and node must be selected");
                return;
            }

            var allworkareas = new workareaCollection();
            allworkareas.fetch({
                success: function (collection) {
                    var alreadyExist = false;
                    _.each(collection.models, function (model) {
                        if (name == model.attributes["text"] && selectedNode[0] == model.attributes["parent"]) {
                            alreadyExist = true;
                        }
                    });

                    if (self.getWorkareaEntities().length >= 1000) {
                        inRiverUtil.NotifyError("Unable to save workarea", "Cannot add more than 1000 entities to a work area folder at a time.");

                        return;
                    }

                    if (alreadyExist) {
                        inRiverUtil.NotifyError("Already exists", "The name already exist.<br/><br/>Please select another name or another node.");
                        return;
                    }

                    self.$el.find("#savedialog").hide(200);
                    self.workareaModel = new workareaModel();
                    self.workareaModel.attributes["IsQuery"] = false;
                    self.workareaModel.attributes["Text"] = name;
                    self.workareaModel.attributes["parent"] = selectedNode[0];
                    self.workareaModel.attributes["Username"] = window.appHelper.userName;
                    self.workareaModel.attributes["folderentities"] = self.getWorkareaEntities();

                    if (self.workareaModel.attributes.parent.indexOf("p") == 0) {
                        self.workareaModel.attributes["type"] = "private";
                    } else {
                        self.workareaModel.attributes["type"] = "shared";
                    }

                    self.workareaModel.url = "/api/workarea/" + this.workareaId;
                    self.workareaModel.save(null, {
                        success: function (model) {

                            self.unbind();
                            var jsonData = model.toJSON();

                            var folderType = "p";
                            if (jsonData.parent != null && jsonData.parent.indexOf("s") == 0) {
                                folderType = "s";
                            }

                            var folderentities = jsonData.FolderEntities.toString();

                            $.post("/api/tools/compress", { '': folderentities }).done(function (result) {
                                self.goTo("workarea?title=" + jsonData.Text + "&workareaId=" + folderType + "_" + jsonData.Id + "&compressedEntities=" + result);

                                self.performSearch({
                                    title: jsonData.Text,
                                    workareaId: folderType + "_" + jsonData.Id,
                                    entityIds: folderentities
                                });

                            });
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    var box = self.$el.find("#card-tools-container");
                    box.toggle(200);

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        updateWorkArea: function () {
            var self = this;
            this.workareaModel = new workareaModel();
            this.workareaModel.url = "/api/workarea/" + this.workareaId;
            this.workareaModel.fetch({
                success: function (model) {
                    self.workareaModel.Id = model.attributes["id"];
                    self.workareaModel.attributes["folderentities"] = self.getWorkareaEntities();
                    self.workareaModel.save(null, {
                        type: "PUT"
                    });
                    self.$el.find("#savedialog").toggle(200);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }

            });
        },
        showSaveWorkAreaDialog: function (e) {
            e.stopPropagation();
            if (!this.$el.find("#savedialog").is(":visible")) {
                this.$el.find("#savedialog").show(200);
                this.$el.find("#savedialog").position({
                    my: "right top",
                    at: "left-200 top+20",
                    of: this.$el.find("#saveworkarea"),
                    collision: "flipfit"
                });

                if (this.workareaId != null) {
                    this.$el.find("#updateWorkarea").show();
                } else {
                    this.$el.find("#updateWorkarea").hide();
                }

            } else {
                this.$el.find("#savedialog").hide(200);
            }
        },
        showMassUpdateDialog: function () {
            var models = this.getSelectedModels();
            if (models.length) {
                this.modal = new modalPopup();
                this.modal.popOut(new massUpdateWizzardView({ ids: this.getSelectedEntityIds(), entityTypeId: models[0].get("EntityType"), models: models }), { size: "medium", header: "Mass Update" });
            }
        },
        showExportExcelDialog: function (e) {
            var self = this;
            e.stopPropagation();
            e.preventDefault();
            this.modal = new modalPopup();

            var entityTypeIds;

            var entityIds = this.getSelectedEntityIds();
            if (entityIds.length === 0) {
                entityIds = this.getWorkareaEntities();
            }

            if (self.mode == "table") {
                entityTypeIds = this.tableView.getSelectedEntityTypeIds();
            } else {
                entityTypeIds = self.$el.find(".card-section-wrap");
            }

            this.modal.popOut(new excelExportView({ model: this.model, entityIds: entityIds, entitytypeIds: _.pluck(entityTypeIds, "id") }), { size: "medium", usesavebutton: false });
        },
        showSavedWorkAreas: function (e) {
            e.stopPropagation();
            var id = "#" + this.$el.find(".savedworkarea")[0].id;

            this.$el.find(id).toggle();

            this.$el.find(id).position({
                of: this.$el.find("#contextmenu"),
                my: "right top",
                at: "right+3 top+15",
                collision: "flipfit"

            });
        },
        editQuery: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.modal = new modalPopup();
            var query = "";
            var workareaId = "";
            if (this.advsearchPhrase != null) {
                query = window.appHelper.advQuery;

            } else if (this.workareaId != null && this.isQuery == true) {

                workareaId = this.workareaId;

            }
            this.modal.popOut(new queryEditorView({
                advancedQuery: query,
                workareaId: workareaId
                /*id: e.currentTarget.id, collection: this.collection, parentEntityId: this.id*/
            }), { size: "medium", usesavebutton: false });
        },
        bindEvents: function () {

            this.delegateEvents();
            this.quicksearch.delegateEvents();
            this.showResult();
            this.enableDroppable();

            //if (this.savetreeid && this.$el.find(this.savetreeid).jstree()) {
            //    this.$el.find(this.savetreeid).jstree().destroy();
            //}
            //    this.renderSaveTree();
            if (this.workareatree) {
                this.populateWorkareaDropDown();
            }

            //this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);

        },
        preRender: function () {
            var self = this;

            this.setVisualMode();

            if (this.relationTypes == null) {
                this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: "Task" });

                this.relationTypes.fetch({
                    success: function () {
                        self.showResult();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.showResult();
            }
        },
        performSearch: function (data) {
            this.$el.find("#loading").show();
            this.allrendered = false;
            this.entityCount = 0;
            this.page = 0;
            this.title = data.title;
            this.searchPhrase = data.searchPhrase;
            this.advsearchPhrase = data.advsearchPhrase;
            this.entityIds = data.entityIds;
            this.compressedEntities = data.compressedEntities;
            this.isQuery = data.isQuery;
            if (data.workareaId != null) {
                this.workareaId = data.workareaId;
            }

            var that = this;
            if (data.collection != undefined) {
                this.collection = data.collection;
            }
            if (this.isQuery == "true") {
                this.isQuery = true;
            }


            if (this.entityIds != undefined && this.collection != undefined) {
                this.$el.find("#deleteworkarea").show();
                this.$el.find("#editquery").hide();
                that.collection.fetch({
                    //url: "/api/entity/getentities",
                    data: {
                        type: "entities",
                        parameter: that.entityIds,
                    },
                    type: 'POST',
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    success: function () {
                        that.preRender();
                    }
                });
            } else if (this.workareaId != null && this.isQuery == true) {
                this.collection.fetch({
                    data: {
                        type: "savedquery",
                        parameter: this.workareaId,
                    },
                    type: 'POST',
                    success: function () {
                        that.preRender();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                this.$el.find("#editquery").css('display', 'block');
                this.$el.find("#deleteworkarea").show();
            } else if (this.compressedEntities != undefined && this.collection != undefined) {


            } else {
                this.$el.find("#editquery").hide();
                this.$el.find("#deleteworkarea").hide();
                this.preRender();
            }
        },
        newSearchSetup: function () {
            this.$el.find("#loading").show();
            this.allrendered = false;
            this.entityCount = 0;
            this.page = 0;
            this.workareaId = null;
            this.compressedEntities = null;

        },
        performQuickSearch: function (data) {
            var that = this;
            this.searching = true;
            this.newSearchSetup();
            this.title = data.title;
            this.searchPhrase = data.searchPhrase;

            this.$el.find("#editquery").hide();
            this.$el.find("#deleteworkarea").hide();

            this.collection.fetch({
                data: {
                    type: "search",
                    parameter: that.searchPhrase,
                },
                type: 'POST',
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                success: function () {
                    that.searching = false;
                    that.preRender();
                }
            });
        },
        performQuerySearch: function (data) {
            var that = this;
            this.searching = true;
            this.newSearchSetup();
            this.title = data.title;
            this.advsearchPhrase = data.advsearchPhrase;

            this.$el.find("#deleteworkarea").hide();
            this.$el.find("#editquery").css('display', 'block');

            this.collection.fetch({
                data: {
                    type: "advsearch",
                    parameter: that.advsearchPhrase,
                },
                type: 'POST',
                success: function () {
                    that.searching = false;
                    that.preRender();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);

                    that.$el.find("#loading").hide();
                }
            });
        },
        performLoadCompressedEntities: function (data) {
            var that = this;
            this.searching = true;
            this.newSearchSetup();
            this.title = data.title;
            this.compressedEntities = data.compressedEntities;

            this.$el.find("#deleteworkarea").show();
            this.$el.find("#editquery").hide();
            if (!this.collection) {
                this.collection = new entityCollection();
            }

            this.entityIds = $.post("/api/tools/decompress", { '': this.compressedEntities }).done(function (result) {
                that.entityIds = result;

                that.collection.fetch({
                    data: {
                        type: "entities",
                        parameter: result,
                    },
                    type: 'POST',
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    success: function () {
                        that.searching = false;
                        that.preRender();
                    }
                });

            });
        },
        openSavedQuery: function (data) {
            var that = this;
            this.searching = true;
            this.newSearchSetup();
            this.title = data.title;
            this.workareaId = data.workareaId;
            this.isQuery = true;

            that.collection.fetch({
                data: {
                    type: "savedquery",
                    parameter: that.workareaId,
                },
                type: 'POST',
                success: function () {
                    that.searching = false;
                    that.preRender();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            this.$el.find("#editquery").css('display', 'block');
            this.$el.find("#deleteworkarea").show();
        },
        openSavedWorkarea: function (data) {
            var that = this;
            this.searching = true;
            this.newSearchSetup();
            this.title = data.title;
            this.workareaId = data.workareaId;

            that.collection.fetch({
                data: {
                    type: "savedworkarea",
                    parameter: that.workareaId,
                },
                type: 'POST',
                success: function () {
                    that.searching = false;
                    that.preRender();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            this.$el.find("#editquery").hide();
            this.$el.find("#deleteworkarea").show();
        },
        clearWorkarea: function () {
            this.$el.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "selectAllEntitycards", title: "Select all entities", icon: "fa fa-square-o", text: "Select All" }));
            this.collection = new entityCollection();
            this.title = "Workarea";
            this.workareaId = undefined;
            this.searchPhrase = "";
            this.advsearchPhrase = null;
            this.compressedEntities = null;
            this.isQuery = false;

            this.entityIds = "";

            this.$el.find("#editquery").hide();
            this.$el.find("#deleteworkarea").hide();

            this.newSearchSetup();
            this.showResult();
            this.$el.find("#loading").hide();
        },
        showMore: function () {
            var that = this;
            //this.$el.find("#loading").show();
            // Vilka avdelningar är öppna


            var openEntityTypesDivs = this.$el.find(".card-section-container");
            var openEntityTypes = [];
            _.each(openEntityTypesDivs, function (x) {
                if ($(x).is(":visible")) {
                    openEntityTypes.push($(x).closest(".card-section-wrap")[0].id);
                }

            });
            var unrendered = _.filter(this.cards, function (row) {
                return row.rendered == false && _.contains(openEntityTypes, row.section);

            });

            this.page++;
            var from = this.page * this.entitiesPerPage;
            var to = (this.page + 1) * this.entitiesPerPage;

            var toRender = _.first(unrendered, this.entitiesPerPage);
            _.each(toRender, function (row) {
                that.renderingCounter++;
                setTimeout(function () {
                    row.card.render();
                }, 20);
                row.rendered = true;
            });
            var unrenderlist = _.find(this.cards, function (row) { return row.rendered == false; });
            if (unrenderlist == null || unrenderlist.lenght == 0) {
                this.allrendered = true;
            }
            this.openEntityTypes = undefined;
            this.setDragable();
            this.$el.find("#loading").delay(1000).hide(0);
            setTimeout(function () {
                that.rendering = false;
            }, 900);


        },
        showAllCardEntities: function () {
            var that = this;

            var openEntityTypesDivs = this.$el.find(".card-section-container");
            var openEntityTypes = [];
            _.each(openEntityTypesDivs, function (x) {
                if ($(x).is(":visible")) {
                    openEntityTypes.push($(x).closest(".card-section-wrap")[0].id);
                }

            });
            var unrendered = _.filter(this.cards, function (row) {
                return row.rendered == false && _.contains(openEntityTypes, row.section);

            });

            var toRender = _.first(unrendered, this.cards.length);
            _.each(toRender, function (row) {
                that.renderingCounter++;
                setTimeout(function () {
                    row.card.render();
                }, 20);
                row.rendered = true;
            });
            var unrenderlist = _.find(this.cards, function (row) { return row.rendered == false; });
            if (unrenderlist == null || unrenderlist.lenght == 0) {
                this.allrendered = true;
            }
            this.openEntityTypes = undefined;
            this.setDragable();
            this.$el.find("#loading").delay(1000).hide(0);
            setTimeout(function () {
                that.rendering = false;
            }, 900);
        },
        loadNextPage: function () {
            var self = this;
            if (this.mode == "table") {
                return;
            }


            if (!this.rendering && this.$el.find(".card-wrap-large").not(".ui-draggable").not(".js-draggable").length > 0) {
                this.setDragable(); //this.createDraggable(this.$el.find(".card-wrap-large").not(".ui-draggable").not(".js-draggable"));
            }

            if (this.allrendered == true || this.rendering == true) {
                return;
            }
            if (this.cardSection == null) {
                this.cardSection = this.$el.find(".card-section-wrap").length;
            }

            var scrollTop = this.$el.find("#search-result-container").scrollTop();
            var innerHeight = this.$el.find("#search-result-container").innerHeight();
            var scrollHeight = this.$el.find("#search-result-container")[0].scrollHeight;
            if ((scrollTop + innerHeight >= scrollHeight - 300)) {
                this.$el.find("#loading").show();
                this.rendering = true;
                setTimeout(function () {
                    self.showMore();
                },
                    10);


            } else if (this.cardSection > 1) {
                if (this.openEntityTypes == undefined) {

                    //Get first open unrendered item 
                    var openEntityTypesDivs = $(".card-section-container:visible").closest(".card-section-wrap");
                    self.openEntityTypes = [];
                    $.each(openEntityTypesDivs, function () {
                        self.openEntityTypes.push(this.id);
                    });
                    self.firstUndedered = _.find(this.cards, function (row) {

                        return row.rendered == false && _.contains(self.openEntityTypes, row.section);
                    });
                }

                if (self.firstUndedered == undefined) {
                    return;
                }

                //Check if its type is in area and should be rendered... if so. showMore()!
                var entitytTypeScrollHeight = this.$el.find("#" + self.firstUndedered.section)[0].scrollHeight;

                if ((scrollTop + innerHeight >= entitytTypeScrollHeight - 300)) {
                    this.$el.find("#loading").show();
                    this.showMore();
                }

            }
        },
        cardRendered: function () {
            this.renderingCounter--;
            this.setDragable();
        },
        entityDeleted: function (id) {

            var self = this;

            var modelToDelete = _.find(this.collection.models, function (model) {
                return model.attributes.Id == id;
            });

            if (modelToDelete) {
                this.collection.remove(modelToDelete);
            }

            if (this.mode === "table") {
                this.showResult();
                return;
            }

            var cardToRemove = _.find(self.cards, function (card) {
                return card.card.model.id == id;
            });

            if (cardToRemove == null) {
                return;
            }

            this.cards = _.reject(self.cards, function (card) {
                return card.card.model.id == id;
            });

            this.entityIds = _.map(self.cards, function (card) {
                return card.card.model.id;
            }).toString();

            if (this.mode == "card") {
                var text = self.$el.find("#" + cardToRemove.section + " .card-section-title-text").text();
                var count = text.substring(text.indexOf("(") + 1).replace(")", "");

                var newcount = 0;
                if (Number(count) > 0) {
                    newcount = Number(count) - 1;
                }

                this.$el.find("#" + cardToRemove.section + " .card-section-title-text").text(self.$el.find("#" + cardToRemove.section + " .card-section-title-text").text().replace(count, newcount));

                var resultcount = 0;

                if (self.entityCount > 0) {
                    resultcount = self.entityCount - 1;
                }

                this.$el.find("#search-result-query").html(_.template(workareaHeaderTemplate, {
                    title: this.title + " (" + resultcount + ")",
                    position: this.position
                }));

            }
        },
        showResult: function (from, to) {
            var self = this;

            this.setMultiSelectionSession();

            this.cardSection = null;
            this.cards = [];
            this.allrendered = false;
            this.page = 0;

            if (this.workareaId == null) {
                this.$el.find("#deleteworkarea").hide();
            }

            window.appHelper.event_bus.off('entityupdated-entity-card-view');

            this.$el.find("#loading").show();
            this.entityCount = 0;

            if (from == undefined) {
                from = this.page * this.entitiesPerPage;
            }

            if (to == undefined) {
                to = (this.page + 1) * this.entitiesPerPage;
            }

            if (this.workareaId != null) {
                $("#updateWorkarea").show();
            } else {
                $("#updateWorkarea").hide();
            }

            // Empty current search result
            this.$el.find("#cards-container").html("");

            var resultcount = 0;
            if (this.collection) {
                resultcount = this.collection.length;
            }

            this.$el.find("#search-result-query").html(_.template(workareaHeaderTemplate, {
                //title: this.title + " - " + resulttype + " (" + resultcount + ")", position: this.position
                title: this.title + " (" + resultcount + ")",
                position: this.position
            }));

            var idList = this.collection.pluck("Id");
            this.entityIds = idList.toString();

            if (this.mode == "table") {

                if (this.tableView) {
                    this.tableView.dispose();
                }

                var checkedIds = [];
                if (window.appSession.multiSelection != null ||
                    window.appSession.multiSelection.workareaId == this.workareaId) {
                    checkedIds = window.appSession.multiSelection.checkedIds;
                }

                if (this.includeFields) {
                    var entityTable = new entityTableCollection();
                    entityTable.fetch({
                        data: { ids: idList },
                        type: "POST",
                        success: function (models) {
                            var entities = self.collection.groupBy("EntityTypeDisplayName");
                            var entityKeys = _.keys(entities);
                            if (entityKeys.length > 1) {

                                self.includeFields = false;
                                self.setVisualMode();
                            }

                            self.tableView = new entityTableView({ collection: models, includeFields: self.includeFields, placement: "workarea", clickable: true, checkedIds: checkedIds });
                            self.$el.find("#cards-container").empty();
                            self.$el.find("#cards-container").append(self.tableView.el);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                            return;
                        }
                    });
                } else {

                    var MainAreaCollection = new Backbone.Collection();
                    if (!self.tableView && self.handler.mainWorkAreaView != null && self.handler.mainWorkAreaView.mode == "table" && self.handler.mainWorkAreaView.tableView) {

                        _.each(self.handler.mainWorkAreaView.tableView.getSelectedEntities(), function (model) {
                            MainAreaCollection.push(model);

                        });

                        checkedIds = self.handler.mainWorkAreaView.tableView.getSelectedEntityIds();
                    }
                    else {
                        MainAreaCollection = this.collection;
                    }

                    self.tableView = new entityTableView({ collection: MainAreaCollection, includeFields: this.includeFields, placement: "workarea", clickable: true, checkedIds: checkedIds });
                    self.$el.find("#cards-container").empty();
                    self.$el.find("#cards-container").append(this.tableView.el);

                }

            } else {
                var groups = this.collection.groupBy("EntityTypeDisplayName");

                var count = 0;
                var keys = _.keys(groups);
                _.each(keys, function (key) {

                    count++;
                    var sectionEl = $(_.template(entitytypeGroupTemplate, { sectionName: groups[key][0].get("EntityTypeDisplayName"), sectionId: groups[key][0].get("EntityType"), entitytypecount: groups[key].length }));

                    _.each(groups[key], function (model) {
                        self.entityCount++;

                        var rendered = false;
                        var entitycard = new entitycardView({
                            model: model,
                            type: "large",
                            taskRelations: self.relationTypes,
                            parent: self
                        });

                        sectionEl.find(".card-section-container").append(entitycard.el);
                        if (self.entityCount >= from && self.entityCount < to) {
                            self.renderingCounter++;
                            entitycard.render();
                            rendered = true;
                        }

                        self.cards.push({ id: self.entityCount, rendered: rendered, section: entitycard.model.attributes.EntityType, card: entitycard });
                    });
                    self.$el.find("#cards-container").append(sectionEl);
                });

                this.getMultiSelectionSession();
            }

            if (to < resultcount) {
                this.$el.find("#showMode").show();
            } else {
                this.$el.find("#showMode").hide();
                this.allrendered = true;
            }

            var ids = self.$el.find(".card-section-wrap");

            for (var index = 0; index < ids.length; ++index) {
                showEntityTypes('#' + ids[index].id);
            }

            function showEntityTypes(id) {
                id = id.replace(" ", "-");
                var $heading = self.$el.find(id + " .card-section-title");
                var $box = self.$el.find(id + " .card-section-container");

                var $showTask = self.$el.find(id + '-show i');

                $heading.click(function () {
                    $box.toggle(200);
                    this.openEntityTypes = undefined;
                    $showTask.toggle();
                    setTimeout(function () { self.showMore(); }, 500);
                });
            }

            $("#loading-spinner-container").hide();

            // Enable draggable
            //this.stopListening(window.appHelper.event_bus);
            //this.listenTo(window.appHelper.event_bus, 'cardrendered', this.cardRendered);

            this.setDragable();


            this.handler.storeWorkAreaStates();

            if (this.handler.mode == "wide") {
                this.$el.find("#search-result-container").scroll(this.loadNextPage);
            } else {
                this.$el.find("#main-client-area").scroll(this.loadNextPage);
            }

            if (!this.searching) {
                this.$el.find("#loading").hide();
            }

            this.handler.setCollapsedTitle();

            return this;
        },
        setDragable: function () {
            var that = this;
            if (!this.jstreednd || this.jstreednd == false) {
                this.createDraggable(this.$el.find(".card-wrap"));
                this.createDraggable(this.$el.find(".card-wrap-large"));
            } else {
                if (this.$el.find(".card-wrap-large").not(".ui-draggable").not(".js-draggable").length > 0) {
                    _.each(this.$el.find('.card-section-wrap').not(".ui-draggable").not(".js-draggable"), function (section) {

                        if (section.id == "ChannelNode" || section.id == "Section") {
                            $(section).find(".card-wrap").addClass("js-draggable");
                            $(section).find(".card-wrap-large").addClass("js-draggable");

                            $(section).find(".card-wrap").on('mousedown', function (e) {
                                var x = that.getSelectedEntityIds();
                                if (x.length > 1) {

                                    var ids = "";
                                    _.each(x, function (cardid) {
                                        ids = ids + cardid + ",";
                                    });

                                    var entityId = x[0];
                                    var entity = that.collection.get(entityId);
                                    var icon = entity.attributes.EntityType;
                                    if (icon == "Channel" && !entity.attributes.ChannelPublished) {
                                        icon = "ChannelUnpublished";
                                    }
                                    var completeness = String.fromCharCode(65 + Math.round(entity.attributes.Completeness / 5));

                                    var $copy = $(_.template(draggableMultiRelationCardsTemplate, { data: entity.toJSON(), mode: that.handler.mode, icon: icon, completeness: completeness, ids: ids }));

                                    return $.vakata.dnd.start(e, { 'jstree': true, 'obj': $(this), 'nodes': [{ id: true, text: $(section).text() }] }, $copy.html());
                                }

                                return $.vakata.dnd.start(e, { 'jstree': true, 'obj': $(this), 'nodes': [{ id: true, text: $(section).text() }] }, '<div id="jstree-dnd" class="jstree-defaultx">' + $(this)[0].outerHTML + '</div>');
                            });

                            $(section).find(".card-wrap-large").on('mousedown', function (e) {
                                var x = that.getSelectedEntityIds();
                                if (x.length > 1) {
                                    var ids = "";
                                    _.each(x, function (cardid) {
                                        ids = ids + cardid + ",";
                                    });

                                    var entityId = x[0];
                                    var entity = that.collection.get(entityId);
                                    var icon = entity.attributes.EntityType;
                                    if (icon == "Channel" && !entity.attributes.ChannelPublished) {
                                        icon = "ChannelUnpublished";
                                    }
                                    var completeness = String.fromCharCode(65 + Math.round(entity.attributes.Completeness / 5));

                                    var $copy = $(_.template(draggableMultiRelationCardsTemplate, { data: entity.toJSON(), mode: that.handler.mode, icon: icon, completeness: completeness, ids: ids }));

                                    return $.vakata.dnd.start(e, { 'jstree': true, 'obj': $(this), 'nodes': [{ id: true, text: $(section).text() }] }, '<div id="jstree-dnd" class="jstree-defaulty">' + $copy.html() + '</div>');
                                }
                                return $.vakata.dnd.start(e, { 'jstree': true, 'obj': $(this), 'nodes': [{ id: true, text: $(section).text() }] }, '<div id="jstree-dnd" class="jstree-defaulty">' + $(this)[0].outerHTML + '</div>');
                            });
                        } else {
                            that.createDraggable($(section).find(".card-wrap"));
                            that.createDraggable($(section).find(".card-wrap-large"));
                        }
                    }
                    );
                }
            }
        },
        setJsTreeDnd: function (jstreednd) {
            this.jstreednd = jstreednd;
        },
        populateWorkareaDropDown: function () {
            var self = this;

            if (this.$el == undefined || this.$el == null) {
                return;
            }

            if (!this.workareatree || !$.jstree.reference(this.workareatree)) {
                if (!this.workareatree) {
                    var id = "#savedworkarea";
                    while ($(id).length > 0) {
                        id = id + "1";
                    }
                    this.$el.find("#savedworkarea").attr("id", id.replace("#", ""));
                    this.workareatree = id;
                }
                id = this.workareatree;

                //this.workareas = new workareaCollection();
                //this.workareas.fetch({
                //    success: function () {

                self.$el.find(id).html("");
                self.$el.find(id).jstree({
                    core: {
                        data: self.workareas.toJSON(),
                        animation: "100",
                        themes: { dots: false }
                    },
                    "plugins": [
                        "types", "wholerow"
                    ],
                    "types": {
                        "#": {
                            "max_children": 1,
                            "max_depth": 4,
                            "valid_children": ["personalroot", "sharedroot"]
                        },
                        "personalroot": {
                            "icon": "/Images/folderdark.png",
                            "valid_children": ["default", "query"]
                        },
                        "sharedroot": {
                            "icon": "/Images/folderdark.png",
                            "valid_children": ["default", "query"]
                        },
                        "default": {
                            "icon": "/Images/folderlight.png",
                            "valid_children": ["default", , "query", "file"]
                        },
                        "query": {
                            "icon": "/Images/folderquery.png",
                            "valid_children": ["default", , "query", "file"]
                        },
                        "file": {
                            "valid_children": []
                        }
                    },

                });

                self.$el.find(id).on("select_node.jstree",
                    function (evt, data) {
                        evt.stopPropagation();

                        if (data.node.id == "s_00000000-0000-0000-0000-000000000000") {
                            return;
                        }

                        if (data.node.id == "p_00000000-0000-0000-0000-000000000000") {
                            return;
                        }
                        self.$el.find(id).toggle();
                        var workareaFolder = new workareaModel();

                        workareaFolder.url = "/api/workarea/" + data.node.id;
                        workareaFolder.fetch({
                            success: function (folder) {

                                var jsonData = folder.toJSON();
                                var folderType = "p";
                                if (jsonData.type == "Shared") {
                                    folderType = "s";
                                }

                                if (jsonData.isquery) {
                                    self.workareaId = folderType + "_" + jsonData.id;
                                    self.performSearch({
                                        title: jsonData.text,
                                        entityIds: null,
                                        entities: null,
                                        isQuery: true,
                                        workareaId: folderType + "_" + jsonData.id
                                    });

                                } else {
                                    this.entities = jsonData.folderentities.toString();
                                    self.workareaId = folderType + "_" + jsonData.id;
                                    self.performSearch({
                                        title: jsonData.text,
                                        entityIds: this.entities,
                                        entities: null,
                                        isQuery: false
                                    });
                                }
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    }
                );
            }
            //self.$el.find(".savedworkareas").html(workareahtml);
            if (!self.savetreeid || !$.jstree.reference(self.savetreeid)) {
                self.renderSaveTree();
            }
            //    },
            //});
        },
        renderSaveTree: function () {
            var self = this;

            if (this.workareas.length == 0) {
                return;
            }

            if (this.savetreeid == undefined || this.savetreeid == null) {
                var id = "#work-area-save-tree";
                while ($(id).length > 0) {
                    id = id + "1";
                }
                this.savetreeid = id;

                this.$el.find("#work-area-save-tree").attr("id", id.replace("#", ""));
            }

            this.$el.find(this.savetreeid).html("");
            this.$el.find(this.savetreeid).jstree({
                core: {
                    data: self.workareas.toJSON(),
                    animation: "100",
                    themes: { dots: false }
                },
                "plugins": [
                    "types", "wholerow"
                ],
                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 4,
                        "valid_children": ["personalroot", "sharedroot"]
                    },
                    "personalroot": {
                        "icon": "/Images/folderdark.png",
                        "valid_children": ["default", "query"]
                    },
                    "sharedroot": {
                        "icon": "/Images/folderdark.png",
                        "valid_children": ["default", "query"]
                    },
                    "default": {
                        "icon": "/Images/folderlight.png",
                        "valid_children": ["default", , "query", "file"]
                    },
                    "query": {
                        "icon": "/Images/folderquery.png",
                        "valid_children": ["default", , "query", "file"]
                    },
                    "file": {
                        "valid_children": []
                    }
                },
            });

        },
        enableDroppable: function () {
            var self = this;
            this.$el.find("#search-result-container").droppable({
                greedy: true,
                tolerance: "pointer",
                accept: function (draggable) {
                    var drager = draggable[0].id;

                    if (drager == "") {
                        //kolla om det är från en tabell.

                        var gridDiv = $(draggable[0]).closest("#entity-table-container");
                        if (gridDiv.length > 0) {
                            var grid = gridDiv.dxDataGrid('instance');
                            var entities = grid.getSelectedRowKeys();
                            if (entities && entities.length > 0) {
                                var okToContinue = false;
                                var i = 0;
                                var entityIds = self.entityIds;
                                if (!entityIds) {
                                    entityIds = "";
                                }

                                while (!okToContinue && i < entities.length) {

                                    var y = _.find(entityIds.split(","), function (id) {
                                        return id == entities[i].Id;
                                    });

                                    // var x = $(this).find("#" + entities[i].Id);
                                    okToContinue = !y; // || !x || x.length == 0;
                                    i++;
                                }

                                return okToContinue;
                            }

                        }
                    }


                    var workarea = $(draggable[0]).closest("div#cards-container");
                    var cards;
                    if (!$(draggable[0]).hasClass("card-selected")) {
                        cards = draggable[0];
                    } else {
                        cards = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");
                    }


                    if (cards && cards.length > 1) {
                        var okToContinue2 = false;
                        var i2 = 0;
                        var entityIds2 = self.entityIds;

                        while (!okToContinue2 && i2 < cards.length) {
                            if (!entityIds2) {
                                var x2 = $(this).find("#" + cards[i2].id.replace("card-", ""));
                                okToContinue2 = !x2 || x2.length == 0;
                            } else {
                                var y2 = _.find(entityIds2.split(","), function (id) {
                                    return id == cards[i2].id.replace("card-", "");
                                });

                                okToContinue2 = !y2;
                            }

                            i2++;
                        }

                        return okToContinue2;
                    }

                    if (drager == null || drager == "") {
                        return false;
                    }

                    //Check if drabbable object exists in target. If its not possible to addit again
                    var x = $(this).find("#" + drager);
                    if (x.length > 0) {
                        return false;
                    }
                    return true;
                },
                activeClass: "workarea-container-active",
                hoverClass: "workarea-dropable-hover",
                drop:
                function (event, ui) {
                    self.entityDropped(event, ui, self);
                }
            });

        },
        entityDropped: function (event, ui, self) {
            var that = this;
            event.stopPropagation();

            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {



                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                var workareaview;
                if (self.cid != self.handler.mainWorkAreaView.cid) {
                    workareaview = self.handler.mainWorkAreaView;
                } else {
                    workareaview = self.handler.secondWorkAreaView;
                }

                if (!workareaview) {
                    return;
                }


                var cards = workareaview.getSelectedEntityIds();
                var entitiesToAdd = [];

                if (cards.length == 0) {
                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var entities = grid.getSelectedRowKeys();
                        _.each(entities, function (entity) {
                            entitiesToAdd.push(entity.Id);
                        });

                        that.addEntitiesToWorkArea(entitiesToAdd);
                    }

                } else {
                    _.each(cards, function (card) {

                        //entitiesToAdd.push(card.id.replace("card-", ""));
                        entitiesToAdd.push(card);

                    });

                    that.addEntitiesToWorkArea(entitiesToAdd);

                }
            } else {

                //kolla om det finns en LinkEntityTypeId om s? m?ste man koppla till l?nkentitet.
                var typeObj = $(ui.draggable[0]).closest(".card-section-wrap");

                if (typeObj.length == 0) {
                    var gridDiv2 = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv2.length > 0) {
                        var grid2 = gridDiv2.dxDataGrid('instance');
                        var entities2 = grid2.getSelectedRowKeys();
                        _.each(entities2, function (entity) {

                            var entityType2 = entity.EntityType;
                            var entityTypeDisplayName2 = entity.EntityTypeDisplayName;

                            var addedEntity2 = entity.Id;

                            that.addEntityToWorkArea(addedEntity2, entityType2, entityTypeDisplayName2);

                        });
                    }

                } else {

                    var entityType = "";
                    var entityTypeDisplayName = "";
                    if (typeObj[0]) {
                        entityType = typeObj[0].id;
                        entityTypeDisplayName = $(typeObj[0]).find(".card-section-title-text").text();
                        entityTypeDisplayName = entityTypeDisplayName.substring(0, entityTypeDisplayName.indexOf("("));
                    }

                    var addedEntity = ui.draggable[0].id.replace("card-", "");

                    this.addEntityToWorkArea(addedEntity, entityType, entityTypeDisplayName);
                }
            }
        },
        addEntityToWorkArea: function (id, entityType, entityTypeDisplayName) {
            var self = this;

            if (entityType != "") {
                this.model = new entityModel(id);
                this.model.url = "/api/entity/" + id;
                this.model.fetch({
                    success: function (model) {
                        var entitycard = new entitycardView({
                            model: model,
                            type: "large",
                            taskRelations: self.relationTypes,
                            parent: self
                        });


                        if (self.$el.find("#" + entityType).length > 0) {

                            var existingEntity = self.$el.find("#" + entityType + " .card-section-container").find("#card-" + model.id);

                            if (existingEntity.length == 0) {

                                self.$el.find("#" + entityType + " .card-section-container").append(entitycard.render().el);

                                //R?kna upp
                                var text = self.$el.find("#" + entityType + " .card-section-title-text").text();
                                var count = text.substring(text.indexOf("(") + 1).replace(")", "");
                                var newcount = Number(count) + 1;
                                self.$el.find("#" + entityType + " .card-section-title-text").text(self.$el.find("#" + entityType + " .card-section-title-text").text().replace(count, newcount));
                            }

                        } else {

                            self.$el.find("#cards-container").append($(_.template(entitytypeGroupTemplate, { sectionName: entityTypeDisplayName, sectionId: entityType, entitytypecount: 1 })));
                            self.$el.find("#" + entityType).append(entitycard.render().el);
                        }
                        self.entityIds += (self.entityIds.length ? "," : "") + id;

                        self.collection.fetch({
                            data: {
                                type: "entities",
                                parameter: self.entityIds,
                            },
                            type: 'POST',
                            error: function (result, response) {
                                inRiverUtil.OnErrors(result, response);
                            },
                            success: function () {
                                self.preRender();
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        addEntitiesToWorkArea: function (entities) {
            var self = this;

            var entityIds = _.uniq(entities);
            var entityIdsAsString = (entityIds.length ? entityIds : this.getWorkareaEntities()).toString();
            self.entityIds = _.union(entityIds, self.entityIds.split(",")).toString();

            //entityupdated-entity-card-view
            window.appHelper.event_bus.off('entityupdated-entity-card-view');

            var addCollection = new entityCollection();
            addCollection.fetch({
                data: {
                    type: "entities",
                    parameter: entityIdsAsString
                },
                type: 'POST',
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                success: function (result) {

                    self.collection.add(addCollection.models);
                    _.each(result.models, function (model) {

                        var entitycard = new entitycardView({
                            model: model,
                            type: "large",
                            taskRelations: self.relationTypes,
                            parent: self
                        });

                        if (self.$el.find("#" + model.attributes.EntityType).length > 0) {

                            var existingEntity = self.$el.find("#" + model.attributes.EntityType + " .card-section-container").find("#card-" + model.id);

                            if (self.mode == "table") {
                                existingEntity = self.entityIds.split(",");
                            }

                            if (existingEntity.length == 0) {

                                self.$el.find("#" + model.attributes.EntityType + " .card-section-container").append(entitycard.render().el);

                                //R?kna upp
                                var text = self.$el.find("#" + model.attributes.EntityType + " .card-section-title-text").text();
                                var count = text.substring(text.indexOf("(") + 1).replace(")", "");
                                var newcount = Number(count) + 1;

                                self.$el.find("#" + model.attributes.EntityType + " .card-section-title-text").text(self.$el.find("#" + model.attributes.EntityType + " .card-section-title-text").text().replace(count, newcount));
                            }

                        } else {

                            self.$el.find("#cards-container").append($(_.template(entitytypeGroupTemplate, { sectionName: model.attributes.EntityTypeDisplayName, sectionId: model.attributes.EntityType, entitytypecount: 1 })));
                            self.$el.find("#" + model.attributes.EntityType).append(entitycard.render().el);
                        }
                    });

                    self.preRender();
                }
            });
        },
        createDraggable: function (cards) {
            var self = this;

            cards.draggable({
                revert: "invalid",
                opacity: 0.7,
                helper: function () {

                    var selected = self.getSelectedEntityIds();
                    var entityId = $(this)[0].id.replace("card-", "");
                    var entity = self.collection.get(entityId);
                    var icon = entity.attributes.EntityType;
                    if (icon == "Channel" && !entity.attributes.ChannelPublished) {
                        icon = "ChannelUnpublished";
                    }
                    var completeness = String.fromCharCode(65 + Math.round(entity.attributes.Completeness / 5));


                    if (selected.length > 0) {
                        //card is selected, drag all selected cards
                        entity = self.collection.get(selected[0]);
                        var $copyInner = $(_.template(draggableMultiRelationCardsTemplate, { data: entity.toJSON(), mode: self.handler.mode, icon: icon, completeness: completeness, ids: "relations" }));
                        $copyInner.css("z-index", "100"); // for allowing dragging on to the merchandising imagemap
                        $copyInner.css('position', "");  // for allowing dragging on to the merchandising imagemap (removing position style puts the element in the default z-index stack)
                        $copyInner.css('opacity', 0.7);
                        return $copyInner;
                    }

                    var $copy = $(_.template(draggableRelationCardTemplate, { data: entity.toJSON(), mode: self.handler.mode, icon: icon, completeness: completeness }));
                    $copy.css("z-index", "100"); // for allowing dragging on to the merchandising imagemap
                    $copy.css('position', "");  // for allowing dragging on to the merchandising imagemap (removing position style puts the element in the default z-index stack)
                    $copy.css('opacity', 0.7);

                    return $copy;
                },
                cursorAt: {
                    top: -2,
                    left: -2
                },
                containment: "window",
                appendTo: "body",
                scroll: true
            });
        },
        selectAllEntityCards: function () {

            var self = this;

            this.$el.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, {
                name: "deselectAllEntitycards", title: "Deselect all entities", icon: "fa fa-check-square-o", text: "Select All"
            }));

            if (this.mode === "table") {
                if (this.tableView.grid) {
                    this.tableView.grid.selectAll();
                }
            } else {
                _.each(self.cards, function (cardElement) {
                    cardElement.card.setChecked(true);
                });
            }

            window.appHelper.event_bus.trigger("selectedEntitiesChanged");
        },
        deselectAllEntityCards: function () {
            if (this.mode === "table") {
                if (this.tableView.grid) {
                    this.tableView.grid.deselectAll();
                }
            } else {
                _.each(this.cards, function (cardElement) {
                    cardElement.card.setChecked(false);
                });
            }

            this.$el.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "selectAllEntitycards", title: "Select all entities", icon: "fa fa-square-o", text: "Select All" }));
            window.appHelper.event_bus.trigger("selectedEntitiesChanged");
        },
        removeSelectedCardsFromWorkArea: function () {
            _.each(this.getSelectedEntityCards(), function (card) {
                card.removeFromWorkArea();
            });
        },
        removeEntityFromWorkarea: function (id) {
            var self = this;
            var cardToRemove = _.find(self.cards, function (card) {
                return card.card.model.id == id;
            });

            if (cardToRemove == null) {
                return;
            }

            this.cards = _.reject(self.cards, function (card) {
                return card.card.model.id == id;
            });

            this.entityIds = _.map(self.cards, function (card) {
                return card.card.model.id;
            }).toString();

            this.collection.remove(id);

            if (this.mode == "card") {
                var text = self.$el.find("#" + cardToRemove.section + " .card-section-title-text").text();
                var count = text.substring(text.indexOf("(") + 1).replace(")", "");

                var newcount = 0;
                if (Number(count) > 0) {
                    newcount = Number(count) - 1;
                }

                this.$el.find("#" + cardToRemove.section + " .card-section-title-text").text(self.$el.find("#" + cardToRemove.section + " .card-section-title-text").text().replace(count, newcount));

                var resultcount = 0;

                if (this.entityCount > 0) {
                    resultcount = this.entityCount - 1;
                }

                this.$el.find("#search-result-query").html(_.template(workareaHeaderTemplate, {
                    title: this.title + " (" + resultcount + ")",
                    position: this.position
                }));

                this.entityCount--;
                inRiverUtil.Notify("Entity removed from workarea");
            }
        },
        selectedEntitiesChanged: function (selectedEntities) {
            var showMassUpdate = false;
            var showRemoveFromWorkArea = false;
            var deleteEntities = false;

            if (!selectedEntities) {
                selectedEntities = this.getSelectedModels();
            }

            if (selectedEntities.length) {
                var firstEntityTypeId = selectedEntities[0].get("EntityType");
                if (_.every(selectedEntities, function (entity) {
                    return entity.get("EntityType") === firstEntityTypeId;
                })) {
                    showMassUpdate = true;
                }

                showRemoveFromWorkArea = true;
                deleteEntities = true;
            }

            if (showMassUpdate) {
                this.$el.find("span#massUpdate").show();
            } else {
                this.$el.find("span#massUpdate").hide();
            }

            if (showRemoveFromWorkArea) {
                this.$el.find("span#removeFromWorkArea").show();
            } else {
                this.$el.find("span#removeFromWorkArea").hide();
            }

            if (deleteEntities) {
                this.$el.find("span#deleteselected").show();
            } else {
                this.$el.find("span#deleteselected").hide();
            }
        },
        render: function () {
            this.$el.html(_.template(workareaTemplate, {
                name: this.title,
                workareaId: this.workareaId
            }));

            if (this.workareaId && this.isQuery == true) {
                this.openSavedQuery({ title: this.title, workareaId: this.workareaId });
            } else if (this.workareaId) {
                this.openSavedWorkarea({ title: this.title, workareaId: this.workareaId });
            }
            else if (this.entityIds) {
                //this.newSearchSetup();
                this.showResult();
            }


            this.quicksearch = new quickSearchWorkAreaView();
            this.$el.find("#workarea-search-container").html(this.quicksearch.el);
            this.$el.find("#workareasearchquery").val(this.searchPhrase);
            //   this.$el.find("#workareaicon").addClass("workarea" + this.position); 

            //add context menu
            var tools = new Array();
            tools.push({ name: "openworkarea", title: "Open Workarea", icon: "fa fa-folder-open-o", text: "Open" });
            tools.push({ name: "editquery", title: "Edit query", icon: "fa fa-pencil", text: "Edit query" });
            tools.push({ name: "saveworkarea", title: "Save Workarea", icon: "fa fa-save", text: "Save" });
            tools.push({ name: "addTaskForSelectedEntities", title: "Create task for selection", icon: "icon-entypo-fix-clipboard std-entypo-fix-icon", text: "Create Task", permission: "AddEntity" });
            tools.push({ name: "exportExcelWorkarea", title: "Export to Excel", icon: "fa icon-entypo-fix-attach", text: "Export" });
            tools.push({ name: "massUpdate", title: "Mass update", icon: "fa fa-list-alt", text: "Mass update", permission: "UpdateEntity" });
            tools.push({ name: "deleteworkarea", title: "Delete Workarea", icon: "fa fa-trash-o", text: "Delete Workarea" });
            tools.push({ name: "deleteselected", title: "Delete Selection", icon: "fa fa-trash-o", text: "Delete Selection", permission: "DeleteEntity" });
            tools.push({ name: "removeFromWorkArea", title: "Remove from workarea", icon: "fa fa-close", text: "Remove from workarea" });

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: 0,
                tools: tools
            }).$el);

            if (!this.hasWorkareaSearch) {
                this.$el.find("#workarea-header-container").hide();
                this.$el.find("#workarea-search-container").hide();
            }

            this.workareas = new workareaCollection();
            var self = this;
            this.workareas.fetch({
                success: function () {
                    self.populateWorkareaDropDown();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.setVisualMode();
            this.enableDroppable();
            this.listenTo(window.appHelper.event_bus, 'cardrendered', this.cardRendered);
            this.listenTo(window.appHelper.event_bus, 'selectedEntitiesChanged', this.selectedEntitiesChanged);
            this.listenTo(window.appHelper.event_bus, 'deselectAllEntityCards', this.deselectAllEntityCards);

            this.handler.setWorkareaPosition(this, this.position, this.state);

            this.$el.find("#workarea-multiselect-control").html(_.template(workareaMultiselectCheckboxControlTemplate, { name: "selectAllEntitycards", title: "Select all entities", icon: "fa fa-square-o", text: "Select All" }));

            this.selectedEntitiesChanged();

            return this;
        }
    });

    return workAreaView;
});
