﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var copyEntityModel = backbone.Model.extend({
        initialize: function (options) {
        },
        urlRoot: '/api/copyentity'
    });

    return copyEntityModel;

});