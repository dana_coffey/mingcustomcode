define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, backbone) {
    var channelCollection = backbone.Collection.extend({
        //model: channelModel,
        url: '/api/channel'
    });

    return channelCollection;
});
