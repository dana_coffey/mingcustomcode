﻿define([
  'jquery',
  'underscore',
  'backbone',
  "multiple-select",
  'text!sharedtemplates/inputcontrols/cvlControlTemplate.html'
], function ($, _, backbone, multipleSelect, cvlControlTemplate) {

    var cvlControlView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;
            this.multiValue = options.MultiValue;
            this.possibleValues = options.PossibleValues;
            if (options.Value == null) {
                this.fieldValue = "";
                this.defaultValue = "";     
            } else {
                var that = this; 
                this.fieldValue = "";
                if (typeof options.Value === 'string') {
                    this.fieldValue = options.Value; 
                } else {
                    $.each(options.Value, function() {
                        that.fieldValue += this + ";";
                    });
                }

                this.defaultValue = _.clone(this.fieldValue);
            }
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.okToSave = false;
            this.saveButtonPressed = false;
            this.entityId = options.EntityId;
            this.focusField = this.inputField;
        },
        events: function () {
            var theEvents = {};
            theEvents["click " + this.saveButton] = "onSave";
            theEvents["click " + this.undoButton] = "onUndo";
            theEvents["blur " + this.inputField] = "onLostFocus";
            theEvents["change " + this.inputField] = "onSelectionChange";
            theEvents["keydown " + this.inputField] = "onKeyDown";
            return theEvents;
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo();
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            if (e.keyCode == 13) {
                this.onSave(e);
            }
        },
        onUndo: function() {
            if (this.saveButtonPressed) {
                return;
            }

            this.$el.find(this.inputField).val(this.defaultValue);
            this.fieldValue = this.defaultValue; 
            this.$el.find(this.saveButton).removeClass('active');
            this.$el.find(this.undoButton).removeClass('active');
            this.okToSave = false;

            this.$el.remove();
            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onSave: function (e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);
            }
        },
        onSelectionChange: function (e) {
            e.stopPropagation();
            var self = this;
            var firstRound = true;
            this.fieldValue = "";
            var inputValue = this.$el.find(this.inputField).val();

            if (this.multiValue) {
                _.each(inputValue, function (key) {
                    if (!firstRound) {
                        self.fieldValue = self.fieldValue + ";";
                    }
                    self.fieldValue = self.fieldValue + key;
                    firstRound = false;
                });
            } else {
                this.fieldValue = inputValue.toString();
            }

            if (this.fieldValue != this.defaultValue) {
                this.$el.find(this.saveButton).addClass('active');
                this.$el.find(this.undoButton).addClass('active');

                this.okToSave = true;
                window.appHelper.event_bus.trigger('fieldvalueupdated', this.fieldTypeId, this.fieldValue);
            } else {
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');

                this.okToSave = false;
            }
        },
        onLostFocus: function (e) {
            var that = this;
            if (e != null) {
                e.stopPropagation();
            }

            // Use timeout to delay examination of historypopup until after blur/focus 
            // events have been processed.
            setTimeout(function () {
                if (!window.appSession.historyset) {
                    if (that.defaultValue.toString() == that.fieldValue.toString()) {
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                    }
                }
            }, 80);
        },
        render: function () {
            var self = this;
            var template = _.template(cvlControlTemplate, { Id: this.fieldTypeId, Value: this.fieldValue });
            this.setElement(template, true);
            if (this.multiValue) {
                this.$el.find(self.inputField).multipleSelect({
                    filter: true,
                    width: 270,
                    onKeyDown: function(e) {
                        self.onKeyDown(e);
                    },
                    onBlur: function() {
                        self.onLostFocus();
                    }
                });

                var keys = self.fieldValue.split(';');
                _.each(this.possibleValues, function (cvlValue) {
                    if ($.inArray(cvlValue, keys) > -1) {
                        self.$el.find(self.inputField).append('<option value="' + cvlValue.Key + '" selected>' + cvlValue.Value + '</option>');
                    } else {
                        self.$el.find(self.inputField).append('<option value="' + cvlValue.Key + '">' + cvlValue.Value + '</option>');
                    }
                });

            } else {
                this.$el.find(self.inputField).multipleSelect({
                    single: true,
                    width: 270,
                    onKeyDown: function (e) {
                        self.onKeyDown(e);
                    },
                    onBlur: function () {
                        self.onLostFocus();
                    }
                });
                _.each(this.possibleValues, function(cvlValue) {
                    if (cvlValue.Key == self.fieldValue) {
                        self.$el.find(self.inputField).append('<option value="' + cvlValue.Key + '" selected>' + cvlValue.Value + '</option>');
                    } else {
                        self.$el.find(self.inputField).append('<option value="' + cvlValue.Key + '">' + cvlValue.Value + '</option>');
                    }
                });
            }
            this.$el.find(self.inputField).multipleSelect('refresh');
            this.$el.find(self.inputField).multipleSelect('focus');
            //console.log(" Focus on CVL " + this.fieldTypeId);

            return this;
        },
    });

    return cvlControlView;
});

