﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/field/fieldModel',
  'text!sharedtemplates/lightboard/lightboardInboundTemplate.html'
 

], function ($, _, Backbone, fieldModel,  lightboardInboundTemplate) {

    var lightboardPreviewRelationListView = Backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.collection = options.collection;

            this.render();
        },
        events: {
            "click .relation-card": "onDetailsClick"
        },
        onDetailsClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            window.appHelper.event_bus.trigger('closepopup');
            this.goTo("entity/" + e.currentTarget.id.replace("card-", ""));

        },
        render: function () {
            
            var that = this;
            this.$el.html("");
            _.each(that.collection.models, function (model) {

                var icon = model.attributes.SourceEntity.EntityType;
                if (icon == "Channel" && !model.attributes.ChannelPublished) {
                    icon = "ChannelUnpublished";
                }

                that.$el.append(_.template(lightboardInboundTemplate, { model: model.toJSON(), icon: icon }));
                
            });

            return this; // enable chained calls
        }
    });

    return lightboardPreviewRelationListView;
});
