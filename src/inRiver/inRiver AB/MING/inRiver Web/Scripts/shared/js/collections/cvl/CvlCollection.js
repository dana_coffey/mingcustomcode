define([
  'jquery',
  'underscore',
  'backbone',
  'models/cvl/CvlModel'
], function($, _, Backbone, CvlModel){
  var CvlCollection = Backbone.Collection.extend({
      model: CvlModel,
      url: '/api/cvl'
  });
 
  return CvlCollection;
});
