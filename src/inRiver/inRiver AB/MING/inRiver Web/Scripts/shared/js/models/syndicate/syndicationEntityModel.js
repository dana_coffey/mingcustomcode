﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var syndicationEntityModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.workareaId = options.workareaId;
        },
        urlRoot: '/api/syndication/workarea/' + this.workareaId + '/entity'
    });

    return syndicationEntityModel;

});
