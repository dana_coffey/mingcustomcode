﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'text!sharedtemplates/inputcontrols/booleanControlTemplate.html'
], function ($, _, backbone, alertify, booleanControlTemplate) {

    var booleanControlView = backbone.View.extend({
        initialize: function(options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;
            if (options.Value == null || options.Value == "") {
                this.fieldValue = false;
                this.defaultValue = null;
            } else {
                this.fieldValue = options.Value == true;
                this.defaultValue = _.clone(options.Value == true);
            }
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.focusField = this.inputField;
            this.entityId = options.EntityId;
            this.okToSave = false;
            this.saveButtonPressed = false;
        },
        events: function() {
            var theEvents = {};
            theEvents["blur " + this.inputField] = "onLostFocus";
            theEvents["click " + this.inputField] = "onClick";
            theEvents["click " + this.saveButton] = "onSave";
            theEvents["click " + this.undoButton] = "onUndo";
            theEvents["keydown " + this.inputField] = "onKeyDown";
            return theEvents;
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo();
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            if (e.keyCode == 13) {
                this.onSave(e);
            }
        },
        onUndo: function () {
            if (this.saveButtonPressed) {
                return;
            }

            this.$el.find(this.inputField).val(this.defaultValue);
            this.fieldValue = this.defaultValue;
            this.$el.find(this.saveButton).removeClass('active');
            this.$el.find(this.undoButton).removeClass('active');

            this.okToSave = false;
            this.$el.remove();
            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onSave: function (e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);
            }
        },
        onClick: function (e) {
            e.stopPropagation();
            this.fieldValue = this.$el.find(this.inputField).is(':checked');
            if (this.fieldValueUpdated()) {
                this.$el.find(this.saveButton).addClass('active');
                this.$el.find(this.undoButton).addClass('active');
                window.appHelper.event_bus.trigger('fieldvalueupdated', this.fieldTypeId, this.fieldValue);
                this.okToSave = true;
            } else {
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');
                window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
                this.okToSave = false;
            }
        },
        fieldValueUpdated: function () {
            var result = false;
            if (this.fieldValue != this.defaultValue) {
                result = true;
            }

            return result;
        },
        onLostFocus: function (e) {
            var that = this;
            e.stopPropagation();

            // Use timeout to delay examination of historypopup until after blur/focus 
            // events have been processed.
            setTimeout(function () {
                if (!window.appSession.historyset) {
                    if (that.fieldValueUpdated()) {
                        window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    }
                }
            }, 80);
        },
        render: function () {
            var template = _.template(booleanControlTemplate, { Id: this.fieldTypeId });
            this.setElement(template, true);
            if (this.fieldValue) {
                this.$el.find(this.inputField).each(function () {
                    this.checked = true;
                });
            }
            return this;
        },
    });

    return booleanControlView;
});

