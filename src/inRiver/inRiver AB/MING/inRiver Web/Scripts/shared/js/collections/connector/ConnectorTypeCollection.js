define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/connector/connectorModel'
], function ($, _, backbone, connectorEventModel) {
    var connectorTypeCollection = backbone.Collection.extend({
        //model: connectorEventModel,
        url: '/api/connectortype/'
    });

    return connectorTypeCollection;
});
