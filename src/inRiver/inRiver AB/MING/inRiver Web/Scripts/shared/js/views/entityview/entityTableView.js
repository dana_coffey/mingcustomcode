﻿define([
    'jquery',
    'underscore',
    'backbone',
    'devextreme',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/entity/entityTableTemplate.html',
    'text!sharedtemplates/relation/draggableRelationCardTemplate.html',
    'text!sharedtemplates/relation/draggableMultiRelationCardsTemplate.html'
], function ($, _, backbone, devextreme, inRiverUtil, entityTableTemplate, draggableRelationCardTemplate, draggableMultiRelationCardsTemplate) {
    var entityTableView = backbone.View.extend({
        initialize: function (options) {
            this.collection = options.collection;
            this.includeFields = options.includeFields;
            this.placement = options.placement;
            this.direction = options.direction;
            this.clickable = options.clickable;
            this.checkedIds = options.checkedIds;

            this.render();
        },
        events: {},
        dispose: function (e) {
            this.$el.find("#entity-table-container").html("");

            this.undelegateEvents();

            this.$el.removeData().unbind();

            this.remove();
            Backbone.View.prototype.remove.call(this);
            this.unbind();

        },
        rowClick: function (e) {
            if (!this.clickable || e.data == null || e.data.items !== undefined) {
                return;
            }

            if (this.placement == "relations") {
                if (this.direction == "inbound") {
                    this.goTo("entity/" + e.data.SourceId);
                } else {
                    this.goTo("entity/" + e.data.TargetId);
                }
            } else {
                if (window.appHelper.workAreaHandler.app == "enrich") {
                    this.goTo("entity/" + e.data.Id);
                }
            }
        },
        getSelectedEntityIds: function () {
            return this.grid ? this.grid.getSelectedRowKeys() : [];
        },
        getSelectedEntityTypeIds: function () {
            var entityTypes = [];
            var rows = this.grid.getSelectedRowsData();

            if (rows.length != 0) {
                _.each(rows, function (row) {
                    if (_.findWhere(entityTypes, { 'id': row.EntityType }) == undefined) {
                        entityTypes.push({ 'id': row.EntityType });
                    }
                });
            } else {
                _.each(this.collection.models, function (row) {

                    if (_.findWhere(entityTypes, { 'id': row.attributes.EntityType }) == undefined) {
                        entityTypes.push({ 'id': row.attributes.EntityType });
                    }
                });
            }

            return entityTypes;
        },
        getSelectedEntities: function () {
            if (!this.grid) {
                return null;
            }
            var self = this;

            var collect = [];

            _.each(this.grid.getSelectedRowKeys(), function (key) {

                _.each(self.collection.models,
                    function (entity) {
                        if (entity.get("Id") == key) {
                            collect.push(entity);
                            return;
                        }
                    });
            });

            return collect;
        },
        setSelectedEntities: function (e) {
            var self = this;
            if (!this.firstTime) {
                return;
            }

            var keys = [];
            var index = 0;
            _.each(this.collection.toJSON(), function (model) {
                _.each(self.checkedIds, function (id) {
                    if (model.Id == id) {
                        var key = self.grid.getKeyByRowIndex(index);
                        keys.push(key);
                    }
                });

                index++;
            });

            this.grid.selectRows(keys, true);
            this.firstTime = false;
        },
        initDragging: function () {
            var self = this;
            this.$el.find('.myRow').draggable({
                revert: "invalid",
                opacity: 0.7,
                helper: function (e) {

                    var rows = self.grid.getSelectedRowKeys();

                    if (rows == null || rows.length == 0) {
                        return "";
                    }

                    var entityId = rows[0].Id;
                    var entity = self.collection.get(entityId);
                    var icon = entity.attributes.EntityType;
                    if (icon == "Channel" && !entity.attributes.ChannelPublished) {
                        icon = "ChannelUnpublished";
                    }
                    var completeness = String.fromCharCode(65 + Math.round(entity.attributes.Completeness / 5));

                    if (rows.length > 1) {
                        var $copy = $(_.template(draggableMultiRelationCardsTemplate, { data: entity.toJSON(), mode: "small", icon: icon, completeness: completeness, ids: "relations" }));
                        $copy.css("z-index", "100"); // for allowing dragging on to the merchandising imagemap
                        $copy.css('position', ""); // for allowing dragging on to the merchandising imagemap (removing position style puts the element in the default z-index stack)
                        $copy.css('opacity', 0.7);
                        return $copy;
                    }

                    var $copy = $(_.template(draggableRelationCardTemplate, { data: entity.toJSON(), mode: "small", icon: icon, completeness: completeness }));
                    $copy.css("z-index", "100"); // for allowing dragging on to the merchandising imagemap
                    $copy.css('position', ""); // for allowing dragging on to the merchandising imagemap (removing position style puts the element in the default z-index stack)
                    $copy.css('opacity', 0.7);
                    return $copy;
                },
                appendTo: 'body',
                scroll: false,
                disabled: true,
            });
        },
        initSortable: function () {
            var self = this;
            this.$el.find('.myRow').draggable({
                helper: 'clone',
                start: function (event, ui) {
                    var $originalRow = $(this),
                        $clonedRow = ui.helper;
                    var $originalRowCells = $originalRow.children(),
                        $clonedRowCells = $clonedRow.children();
                    for (var i = 0; i < $originalRowCells.length; i++)
                        $($clonedRowCells.get(i)).width($($originalRowCells.get(i)).width());
                    $clonedRow
                        .width($originalRow.width())
                        .addClass('drag-helper');
                }
            });
            this.$el.find('.myRow').droppable({
                drop: function (event, ui) {
                    if (!$(ui.draggable).hasClass("myRow")) {
                        return;
                    }
                    var draggingRowKey = ui.draggable.data('keyValue');
                    var targetRowKey = $(this).data('keyValue');
                    var draggingIndex = null,
                        targetIndex = null;

                    // API and save order
                    var xmlhttp = new XMLHttpRequest();
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4) {
                            self.collection.stopListening();
                            self.collection.fetch({
                                success: function () {
                                    // setTimeout(function() { self.grid.refresh(); }, 10000); 
                                }
                            });
                        }

                    }

                    xmlhttp.open("GET", "/api/tools/updaterelationsortorder/" + draggingRowKey.Id + "/" + targetRowKey.Index, true);
                    xmlhttp.send();


                }
            });

        },
        render: function () {
            var self = this;
            this.selectedRows = "";
            this.firstTime = true;

            var models = this.collection.toJSON();

            var selectionMode = 'multiple';
            if (this.placement == 'relations') {
                selectionMode = 'none';
            }

            this.$el.html(_.template(entityTableTemplate, {}));
            if (this.includeFields) {
                setTimeout(function () {
                    self.grid = self.$el.find("#entity-table-container").dxDataGrid({
                        dataSource: models,
                        showColumnLines: true,
                        showRowLines: true,
                        groupPanel: {
                            visible: true
                        },
                        onRowClick: function (e) {
                            self.rowClick(e);
                        },
                        onSelectionChanged: function (e) {
                            if (e.selectedRowKeys == null || e.selectedRowKeys.length == 0) {
                                self.$el.find('.myRow').draggable("option", "disabled", true);
                            } else {
                                self.$el.find('.myRow').draggable("option", "disabled", false);
                            }
                            window.appHelper.event_bus.trigger("selectedEntitiesChanged", self.getSelectedEntities());
                        },
                        columnAutoWidth: true,
                        allowColumnResizing: true,
                        paging: false,
                        selection: {
                            mode: selectionMode
                        },
                        sorting: { mode: 'multiple' },
                        loadPanel: { text: 'Loading data...', showIndicator: false },
                        onContentReady: function (e) {
                            if (self.firstTime) {
                                self.setSelectedEntities(e);
                                self.grid.option({
                                    loadPanel: { enabled: false }
                                });
                            }
                        }
                    }).dxDataGrid('instance');
                }, 1);
            } else {
                var columnList;
                if (this.placement == "relations") {
                    if (this.direction == 'inbound') {
                        columnList = [
                            { dataField: 'SourceEntityTypeId', caption: 'Entity Type', groupIndex: 0 },
                            { dataField: 'LinkEntityName', caption: 'Link Entity', groupIndex: 1 },
                            { dataField: 'TargetName', caption: 'Name' },
                            { dataField: 'TargetDescription', caption: 'Description' },
                            { dataField: 'Active', caption: 'Status' },
                            { dataField: 'Index', caption: 'Index', sortIndex: 0, sortOrder: 'asc' }
                        ];
                    } else {
                        columnList = [
                            { dataField: 'TargetEntityTypeId', caption: 'Entity Type', groupIndex: 0 },
                            { dataField: 'LinkEntityName', caption: 'Link Entity', groupIndex: 1 },
                            { dataField: 'SourceName', caption: 'Name' },
                            { dataField: 'SourceDescription', caption: 'Description' },
                            { dataField: 'Active', caption: 'Status' },
                            { dataField: 'Index', caption: 'Index', sortIndex: 0, sortOrder: 'asc' }
                        ];
                    }
                } else {
                    columnList = [
                        'EntityType',
                        { dataField: 'DisplayName', caption: 'Name', width: 400 },
                        { dataField: 'DisplayDescription', caption: 'Description' },
                        { dataField: 'Completeness', caption: 'Completeness' },
                        'CreatedDate',
                        'ModifiedDate'];
                }
                setTimeout(function () {
                    self.grid = self.$el.find("#entity-table-container").dxDataGrid({
                        dataSource: {
                            store: {
                                data: models,
                                type: "array",
                                key: "Id"
                            }
                        },
                        showColumnLines: true,
                        showRowLines: true,
                        groupPanel: {
                            visible: true
                        },
                        columns: columnList,
                        onRowClick: function (e) {
                            self.rowClick(e);
                        },
                        onSelectionChanged: function (e) {
                            if (e.selectedRowKeys == null || e.selectedRowKeys.length == 0) {
                                self.$el.find('.myRow').draggable("option", "disabled", true);
                            } else {
                                self.$el.find('.myRow').draggable("option", "disabled", false);
                            }
                            window.appHelper.event_bus.trigger("selectedEntitiesChanged", _.map(e.selectedRowKeys, function (key) {
                                return self.collection.get(key);
                            }));
                        },
                        allowColumnResizing: true,
                        paging: false,
                        selection: {
                            mode: selectionMode
                        },
                        //onRowPrepared: function(rowElement, rowInfo) {
                        //    if (rowInfo.rowType != 'data')
                        //        return;
                        //    rowElement
                        //        .addClass('myRow')
                        //        .data('keyValue', rowInfo.key);
                        //},
                        sorting: { mode: 'multiple' },
                        loadPanel: { text: 'Loading data...', showIndicator: false },
                        selectedRowKeys: self.checkedIds,
                        //onContentReady: function (e) {
                        //    if (this.placement == 'relations') {
                        //        self.initSortable();
                        //    } else {
                        //        self.initDragging(e.element);
                        //    }

                        //    if (self.firstTime) {
                        //        self.grid.option({
                        //            loadPanel: { enabled: false }
                        //        });
                        //        if (self.placement == 'relations') {
                        //            self.initSortable();
                        //        } else {
                        //            self.initDragging(e.element);
                        //        }

                        //    }
                        //}
                    }).dxDataGrid('instance');
                }, 1);
            }
        }
    });
    return entityTableView;
});