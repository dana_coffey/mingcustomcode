﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/advancedsearch/queryEditorView',
  'text!sharedtemplates/workarea/workareaSearchTemplate.html'
], function ($, _, backbone, modalPopup, inRiverUtil, queryEditorView, workAreaSearchTemplate) {

    var quickSearchWorkAreaView = backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        events: {
            "click #workareasearch": "quicksearch",
            "keyup #workareasearchquery": "keyPressEventHandler",
            "click #edit-query": "editQuery"
        },
        editQuery: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var that = this;
            this.modal = new modalPopup();

            this.modal.popOut(new queryEditorView({ searchOrigin: "workarea" }), { size: "medium" });
            this.listenTo(appHelper.event_bus, 'popupclose', this.closePopup);
        },
        quicksearch: function () {
            var qsquery = $("#workareasearchquery").val();
            Backbone.history.loadUrl("workareasearch?title=Query&searchPhrase=" + qsquery);
        },
        keyPressEventHandler: function (event) {
            if (event.keyCode == 13) {

                if (self.searchHandled) {
                    self.searchHandled = false;
                    return;
                }

                this.quicksearch();

                $(".ui-autocomplete").hide();
            }
        },
        render: function () {

            var that = this; 
            this.$el.html(workAreaSearchTemplate);

            //this.$el.find("#workareasearchquery").focus();

            this.$el.find("#workareasearchquery").autocomplete({
                minLength: 2,
                source: function (request, response) {
                    $.ajax({
                        url: '/api/tools/generateautosearch',
                        type: 'GET',
                        cache: false,
                        data: { id: $("#workareasearchquery").val() },
                        dataType: 'json',
                        success: function (json) {
                            response(json);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                focus: function (event, ui) {

                    return false;
                },
                select: function (event, ui) {
                    that.searchHandled = true;

                    $.post("/api/tools/compress", { '': ui.item.Id }).done(function (result) {

                        //that.goTo("workareasearch?title=" + ui.item.Name + "&entityIds=" + result);
                       Backbone.history.loadUrl("workareasearch?title=" + ui.item.Name + "&compressedEntities=" + result);
                    });
                    return false;
                }
            }).autocomplete("instance")._renderItem = function (ul, item) {
               
                return $("<li>")
                    .append("<a><div class='quickSearchItemContainer'><div><img  class='quickSearchItemContainerImage' src='" + item.PictureUrl + "'/></div><div class='quickSearchItemContainerName'><img src='../icon?id=" + item.EntityType + "'/><span>" + item.Name + "</span></div><div class='quickSearchItemContainerDescription'>" + item.Description + "</div></div></a>")
                    .appendTo(ul);
            };
            
            return this;
        }
    });

    return quickSearchWorkAreaView;
});
