﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/models/completeness/completenessModel',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/models/field/fieldModel',
    'sharedjs/models/resource/resourceModel',
    'modalPopup',
    'sharedjs/views/entityview/entityDetailSettingView',
    'sharedjs/views/completeness/completenessPopupView',
    'sharedjs/views/lightboard/lightboardPreviewDetail',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/lightboard/lightboardPreviewTemplate.html',
    'text!sharedtemplates/entitycard/workareaCardTemplate.html',
    'text!sharedtemplates/lightboard/resourceDownloadPopup.html'
], function ($, _, Backbone, completenessModel, relationModel, entityModel, fieldModel, resourceModel, modalPopup, entityDetailSettingView, completenessPopupView,lightboardPreviewDetail, inRiverUtil, lightboardPreviewTemplate, workareaCardTemplate, resourceDownloadPopup) {

    var lightboardPreview = Backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.collection = options.collection;
            this.selectedId = options.id;
            this.parentEntityId = options.parentEntityId;
            this.render();
        },
        events: {
            "click .lightboard-card-wrap": "onCardClick",
        },
        onCardClick: function (e) {
            e.stopPropagation();
            this.selectedId = e.currentTarget.id;

            if (this.lbDetail) {
                this.lbDetail.remove();
            }
            this.lbDetail = new lightboardPreviewDetail({ collection: this.collection, id: this.selectedId, parentEntityId: this.parentEntityId });

            this.$el.find(".lightboard-details-area").html(this.lbDetail.$el);
        },
       
        render: function () {
            this.$el.html(_.template(lightboardPreviewTemplate, { resources: this.collection.toJSON() }));
            
            this.lbDetail = new lightboardPreviewDetail({ collection: this.collection, id: this.selectedId, parentEntityId: this.parentEntityId }); 

            this.$el.find(".lightboard-details-area").html(this.lbDetail.$el);

            return this; // enable chained calls
        }
    });

    return lightboardPreview;
});
