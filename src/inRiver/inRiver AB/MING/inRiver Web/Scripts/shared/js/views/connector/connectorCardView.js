define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
   'modalPopup',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/connector/connectorMessageView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/connector/startConnectorModel',
  'sharedjs/models/connector/stopConnectorModel',
  'sharedjs/models/connector/removeConnectorModel',
  'text!sharedtemplates/connector/connectorCardTemplate.html'
], function ($, _, backbone, alertify, modalPopup, contextMenuView, connectorMessageView, inRiverUtil, startConnectorModel, stopConnectorModel, removeConnectorModel, connectorCardTemplate) {

    var connectorCardView = backbone.View.extend({
        //tagName: 'div',

        initialize: function () {

        },
        events: {
            "click #menu-start-button": "onStartClick",
            "click #menu-stop-button": "onStopClick",
            "click #menu-delete-button": "onDeleteClick",
            "click #menu-events-button": "onEventsClick",
            "click .connector-card-wrap": "onCardClick"
        },
        onCardClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            this.goTo("connector/" + e.currentTarget.id);

        },
        onEventsClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            var modal = new modalPopup();

            modal.popOut(new connectorMessageView({
                id: this.model.get("Id"),
            }), { size: "large", header: "Event Messages", usesavebutton: false });

            $("#save-form-button").removeAttr("disabled");
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onDeleteClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            var confirmCallback = function (self) {
                var that = self;
                self.removeConnector = new removeConnectorModel({ Id: self.model.get("Id") });
                self.removeConnector.destroy({
                    success: function () {
                        window.appHelper.event_bus.trigger('connectordeleted', that.model.get("Id"));
                        that.remove();
                        that.unbind();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            inRiverUtil.NotifyConfirm("Confirm delete","Do you want to remove this connector?", confirmCallback, this);
        },
        onStartClick: function (e) {
            e.stopPropagation();
            var self = this;
            this.startConnector = new startConnectorModel({ Id: self.model.get("Id") });
            this.startConnector.fetch({
                success: function (model) {
                    self.startedfetched = true;
                    window.appHelper.event_bus.trigger('connectorupdated', self.model.get("Id"));
                    self.render(model);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            this.onToggleTools(e);
        },
        onStopClick: function (e) {
            e.stopPropagation();
            var self = this;
            this.stopConnector = new stopConnectorModel({ Id: self.model.get("Id") });
            this.stopConnector.fetch({
                success: function (model) {
                    self.startedfetched = true;
                    window.appHelper.event_bus.trigger('connectorupdated', self.model.get("Id"));
                    self.render(model);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            this.onToggleTools(e);
        },
        onClose: function () {
        },
        render: function (model) {
            this.model = model;
            if (this.model == null) {
                return this;
            }

            this.$el.html(_.template(connectorCardTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName }));

            //add context menu
            var tools = new Array();
            if (this.model.attributes["IsStarted"]) {
                tools.push({ name: "menu-stop-button", title: "Stop connector", icon: "fa fa-toggle-on", text: "Stop", permission: "StartStopConnector" });
            } else {
                tools.push({ name: "menu-start-button", title: "Start connector", icon: "fa fa-toggle-off", text: "Start", permission: "StartStopConnector" });
            }

            tools.push({ name: "menu-events-button", title: "Monitor the connector events", icon: "icon-entypo-fix-flag std-entypo-fix-icon", text: "Events" });
            tools.push({ name: "menu-delete-button", title: "Delete connector", icon: "fa-trash-o fa", text: "Delete", permission: "AddDeleteConnector" });

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: tools }).$el);

            return this; // enable chained calls
        }
    });

    return connectorCardView;
});
