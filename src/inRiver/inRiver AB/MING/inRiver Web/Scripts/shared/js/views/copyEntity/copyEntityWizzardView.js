﻿define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-steps',
    'jstree',
    'sharedjs/models/entity/copyEntityModel',
    'sharedjs/models/relation/relationStructureModel',
    'sharedjs/models/entity/validateEntitiesModel',
    'sharedjs/models/field/validateFieldModel',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/field/fieldModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/copyEntity/copyEntityWizzardTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardStepChoiceTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardStepDetailsTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardDetailUniqueFieldsTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardStepValidationTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardStepResultTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityFieldEditorTemplate.html',
    'text!sharedtemplates/copyEntity/copyEntityWizzardValidLabelTemplate.html'
], function ($, _, backbone, jquerySteps, jstree, copyEntityModel, relationStructureModel, validateEntitiesModel, validateFieldModel, relationModel, fieldModel, entityModel, inRiverUtil, copyEntityWizzardTemplate, wizzardStepChoiceTemplate, wizzardStepDetailsTemplate, wizzardDetailUniqueFieldsTemplate, wizzardStepValidationTemplate, wizzardStepResultTemplate, copyEntityFieldEditorTemplate, copyEntityWizzardValidLabelTemplate) {
    var copyEntityWizzardView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.EntityId = options.id;
            this.EntityTypeId = options.entityTypeId;
            this.Level = null;
            this.AllRelations = new relationModel({ Id: this.EntityId });

            this.RestfulEntiyFields = null;
            this.AllLinkedRestfulEntiyFields = [];
            this.Relations = [];
            this.SelectedLinkIds = [];

            this.model = new entityModel({ Id: this.EntityId });
            this.model.fetch({
                success: function() {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click input:radio": "getChoice",
            'change .field-editor-wrap input': 'checkField'
        },
        getChoice: function (e) {

            var checkBoxEl;
            var group;

            if (e) {
                checkBoxEl = $(e.currentTarget);

                if (checkBoxEl.is(":checked")) {

                    group = this.$el.find("#copy-entity-wizzard-choice-tools input:radio");
                    $(group).prop("checked", false);
                    checkBoxEl.prop("checked", true);
                    this.copyEntityChoiceValid = checkBoxEl;
                } else {
                    checkBoxEl.prop("checked", false);
                    this.copyEntityChoiceValid = undefined;
                }

            } else {
                group = this.$el.find("#copy-entity-wizzard-choice-tools input:radio");
                checkBoxEl = this.$el.find("#" + this.copyEntityChoiceValid[0].id);

                $(group).prop("checked", false);
                checkBoxEl.prop("checked", true);
                this.copyEntityChoiceValid = checkBoxEl;
            }

            if (checkBoxEl.is(":checked") && checkBoxEl.attr('id') != "cbx-choice-copy-entity") {
                this.includeRelations = true;

            } else {
                this.includeRelations = false;
            }

        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        moveToStepChoiceView: function () {

            this.$el.find("#copy-entity-wizzard-choice").html(_.template(wizzardStepChoiceTemplate, { entityTypeId: this.EntityTypeId, name: this.model.attributes.DisplayName }));

            if (this.copyEntityChoiceValid) {
                this.getChoice();
            }
        },
        moveToStepDetailView: function () {
            var self = this;

            this.$el.find("#copy-entity-wizzard-detail").html(_.template(wizzardStepDetailsTemplate, {}));

            if (this.includeRelations) {

                this.structure = new relationStructureModel({ entityId: this.EntityId });
                this.structure.fetch({
                    success: function (relationsStructure) {

                        self.$el.find("#copy-entity-wizzard-label-links").html("<label>All Relations</label>");

                        self.$el.find("#copy-entity-wizzard-details-tools-outbound-links").jstree({
                            core: {
                                data: relationsStructure.toJSON(),
                                animation: "100",
                                themes: { dots: false, icons: false },
                            },
                            'checkbox': {
                                three_state: false,
                                cascade: 'down'
                            },
                            'plugins': ["checkbox", "state"]
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.$el.find("#copy-entity-wizzard-label-links").html("<label>You have selected copy \"" + this.model.attributes.DisplayName + "\" with data. Continue to Validation step.</label>");
            }
        },
        moveToStepValidationView: function () {
            var self = this;

            this.$el.find("#copy-entity-wizzard-validation").html(_.template(wizzardStepValidationTemplate, {}));

            var entityIds = [];

            entityIds.push(this.id);

            if (this.copyEntityChoiceValid.attr('id') == "cbx-choice-copy-entity") {
                this.Level = "1";
            } else if (this.copyEntityChoiceValid.attr('id') == "cbx-choice-copy-entity-with-links") {
                this.Level = "2";
            } else if (this.copyEntityChoiceValid.attr('id') == "cbx-choice-copy-entity-copy-links") {
                this.Level = "3";
            }

            if (this.copyEntityChoiceValid.attr('id') == "cbx-choice-copy-entity-copy-links" ||
                this.copyEntityChoiceValid.attr('id') == "cbx-choice-copy-entity-with-links") {
                var jsTreeElement = this.$el.find("#copy-entity-wizzard-details-tools-outbound-links").jstree(true);
                var selectedNodes = jsTreeElement.get_bottom_checked();

                _.each(selectedNodes, function (nodeId) {
                    var entityId = nodeId.split("_")[1];
                    if (!_.contains(entityIds, entityId)) {
                        var linkId = jsTreeElement._model.data[nodeId];

                        if (!_.contains(self.SelectedLinkIds, linkId.original.linkid)) {
                            self.SelectedLinkIds.push(linkId.original.linkid);
                        }

                        entityIds.push(entityId);
                    }
                });

                this.AllRelations.fetch();
            }

            var ValidateEntitiesModel = new validateEntitiesModel();

            if (this.Level == "3") {
                ValidateEntitiesModel.set({ EntityIds: entityIds });
            } else {

                var entityIds2 = [];

                entityIds2.push(this.id);
                ValidateEntitiesModel.set({ EntityIds: entityIds2 });
            }

            ValidateEntitiesModel.save(null, {
                success: function (fieldModels) {
                    var selfSelf = this;
                    var continueValidationStep = true;

                    _.each(fieldModels.changed, function (model) {
                        if (!model.RestfulEntityFields) {
                            return;
                        }

                        var fieldModelBackbone = new fieldModel(model.RestfulEntityFields);

                        var validFieldModel;

                        if (model.RestfulEntityFields.Id == self.id) {
                            self.RestfulEntiyFields = fieldModelBackbone;
                            validFieldModel = fieldModelBackbone;
                        } else {

                            var foundFieldModel = self.containsFieldModelId(self.AllLinkedRestfulEntiyFields, fieldModelBackbone.attributes.Id);

                            if (foundFieldModel.length == 0) {
                                self.AllLinkedRestfulEntiyFields.push(fieldModelBackbone);
                                validFieldModel = fieldModelBackbone;
                            } else {

                                validFieldModel = foundFieldModel[0];
                            }
                        }

                        if (model.UniqueFields.length > 0) {
                            var schema = inRiverUtil.createFormsSchemaFromRestfulFields(validFieldModel);
                            var formTemplate = self.handleUniqueFields(model.UniqueFields, schema);

                            this.schema = schema;

                            this.form = new backbone.Form({
                                template: _.template(formTemplate),
                                model: validFieldModel,
                                schema: this.schema
                            }).render();

                            if (this.form == undefined) {
                                var t = "";
                            }

                            selfSelf.displayDescription = validFieldModel.get('DisplayDescription');

                            if (displayDescription.length > 50) {
                                selfSelf.displayDescription = selfSelf.displayDescription.substring(0, 100) + "...";
                            }

                            if (this.displayDescription.length == 0) {
                                selfSelf.displayDescription = "";
                            } else {
                                selfSelf.displayDescription = "(" + selfSelf.displayDescription + ")";
                            }

                            var uniqueFieldsContainer = self.$el.find("#copy-entity-wizzard-validation-tools-unique-fields");

                            uniqueFieldsContainer.append("<label>" + self.getDisplayName(validFieldModel) + "</label>");

                            $(this.form.el).find(".field-editor-wrap input").addClass('notValid');//
                            $(this.form.el).find(".copy-entity-field-editor-label").prepend("<i class='fa fa-info-circle validation-error' title='Unique field needs to be modified'></i>");

                            uniqueFieldsContainer.append(this.form.el);

                            if (continueValidationStep) {
                                continueValidationStep = false;
                            }
                        }

                        if (model.MandatoryFields.length > 0) {
                            var errorFieldsContainer = self.$el.find("#copy-entity-wizzard-validation-tools-unique-fields");
                            self.NotValidMandatoryField = true;

                            _.each(model.MandatoryFields, function (field) {
                                errorFieldsContainer.append("<div class='field-editor-wrap'><div class='field-editor-label-wrap'><div class='copy-entity-field-editor-label extra-top-margin'><i class='fa fa-info-circle validation-error' title='Mandatory field is missing value'></i>" + field.FieldTypeDisplayName + "</div></div><div data-editors='Fields.0.Value'><label class='mandatory-missing'>Missing value</label></div></div>");
                            });

                            if (continueValidationStep) {
                                continueValidationStep = false;
                            }
                        }
                    });

                    if (continueValidationStep) {
                        self.$el.find("#copy-entity-wizzard-label-fields").html("<label>No validation errors. Continue to Copy step.</label>");
                        self.continueValidationStep = true;
                    } else {
                        self.continueValidationStep = false;

                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        getDisplayName: function (model) {

            var name = "[" + model.attributes.Id + "]";

            if (model.attributes.DisplayName != null && model.attributes.DisplayName != "") {
                name = model.attributes.DisplayName;
            }

            if (model.attributes.DisplayDescription != null && model.attributes.DisplayDescription != "") {
                name = name + " - " + model.attributes.DisplayDescription;
            }

            return name;
        },
        moveToStepResultView: function () {
            this.$el.find("#copy-entity-wizzard-result").html(_.template(wizzardStepResultTemplate, { copyEntity: this.RestfulEntiyFields.attributes, entities: this.AllLinkedRestfulEntiyFields, level: this.Level }));
        },
        handleUniqueFields: function (uniqueFields, schema) {
            var formTemplate = "<form>";

            _.each(uniqueFields, function (field) {
                var dataEditorKey = inRiverUtil.findBackboneFormsEditorKey(schema, field.FieldType);
                var editor = _.template(copyEntityFieldEditorTemplate, { id: field.EntityId, type: field.FieldType, dataType: field.FieldDataType, name: field.FieldTypeDisplayName, description: field.FieldTypeDescription.replace(/\"/g, '&quot;'), dataEditorKey: dataEditorKey, revision: field.Revision, shouldTrackChanges: field.ShouldTrackChanges, highlightedLabel: false });
                formTemplate += editor;
            });

            formTemplate += "</form>";

            return formTemplate;
        },
        checkField: function (e) {
            var self = this;
            var selfE = e;

            var fieldTypeIdElement = $(e.currentTarget).closest('.field-editor-wrap');

            var fieldTypeId = fieldTypeIdElement[0].getAttribute('fieldTypeid');
            this.entityId = fieldTypeIdElement[0].getAttribute('entityid');
            var fieldValue = e.currentTarget.value;

            var fieldModel = new validateFieldModel({ FieldTypeId: fieldTypeId, FieldData: fieldValue });

            fieldModel.fetch({
                success: function (model) {

                    if (model.attributes.IsUnique == true) {

                        var fieldModel = null;

                        if (self.RestfulEntiyFields.attributes.Id == self.entityId) {
                            fieldModel = self.RestfulEntiyFields;
                        } else {

                            var existingModel = self.containsFieldModelId(self.AllLinkedRestfulEntiyFields, self.entityId);

                            if (existingModel.length > 0) {
                                fieldModel = existingModel[0];
                            }
                        }
                        if (fieldModel) {
                            _.each(fieldModel.attributes.Fields, function (field) {
                                if (field.FieldType == model.fieldTypeId) {
                                    field.Value = model.value;
                                }
                            });
                        }

                        $(selfE.currentTarget).removeClass('notValid');
                        $(selfE.currentTarget).removeClass('valid');
                        $(selfE.currentTarget).addClass('valid');
                    } else {
                        $(selfE.currentTarget).removeClass('notValid');
                        $(selfE.currentTarget).removeClass('valid');
                        $(selfE.currentTarget).addClass('notValid');
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        validateAllUniqueFields: function () {
            var validationChecker = $(this.$el).find(".field-editor-wrap .notValid");

            if (validationChecker.length > 0 || this.NotValidMandatoryField) {
                this.continueFromValidationStep = false;
            } else {
                this.continueFromValidationStep = true;
            }
        },
        finish: function () {
            var self = this;

            var allRelationsArray = [];

            _.each(this.AllRelations.attributes, function (relation) {
                allRelationsArray.push(relation);
            });

            var copyEntity = new copyEntityModel();
            copyEntity.set({ RestfulEntityFields: this.RestfulEntiyFields });

            copyEntity.set({ LinkedRestfulEntityFields: this.AllLinkedRestfulEntiyFields });

            copyEntity.set({ AllRestfulRelations: allRelationsArray });
            copyEntity.set({ AllLinkIds: self.SelectedLinkIds });
            copyEntity.set({ Level: this.Level });

            var spinner = this.$el.find("#loading-spinner-container");
            spinner.show();
            
            copyEntity.save(null, {
                success: function (entityCopied) {
                    spinner.hide();

                    $.post("/api/tools/compress", { '': entityCopied.get("Id") }).done(function (result) {
                        self.goTo("workareasearch?title=" + entityCopied.get("DisplayName") + "&compressedEntities=" + result);
                    });

                    self.onCancel();
                },
                error: function (model, response) {
                    spinner.hide();
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        containsFieldModelId: function (list, id) {

            var matches = _.filter(list, function (value) {
                if (value.attributes.Id == id) {
                    return value;
                }
            });

            return matches;
        },
        render: function () {
            var self = this;
            this.$el.html(_.template(copyEntityWizzardTemplate, {}));

            this.$el.find("#copy-entity-wizard").steps({
                transitionEffect: "slideLeft",
                transitionEffectSpeed: 400,
                enableAllSteps: false,
                autoFocus: true,
                enableCancelButton: true,
                onInit: function () {
                    self.moveToStepChoiceView();
                },
                onCanceled: function () {
                    self.onCancel();
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (currentIndex == 1 && newIndex == 0) {
                        self.moveToStepChoiceView();
                    }

                    if (newIndex == 1 && !self.copyEntityChoiceValid) {
                        return false;
                    }

                    if (newIndex == 1 && self.copyEntityChoiceValid) {
                        self.moveToStepDetailView();
                    }

                    if (newIndex == 2) {
                        self.moveToStepValidationView();
                    }

                    if (currentIndex == 2 && newIndex == 3) {

                        self.validateAllUniqueFields();

                        if (self.continueFromValidationStep == false) {
                            return false;
                        }

                        self.moveToStepResultView();
                    }

                    return true;
                },
                onFinished: function (event, currentIndex) {
                    self.finish();
                },
                labels: {
                    loading: "Loading ..."
                }
            });
        }
    });

    return copyEntityWizzardView;
});