﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/comment/commentModel'
], function ($, _, Backbone, commentModel) {
    var promotionFromCampaignCollection = Backbone.Collection.extend({
        model: commentModel,
        initialize: function (options) {
            this.entityId = options.id;
        },
        url: function () {
            return '../../api/comment/' + this.entityId;
        }
    });

    return promotionFromCampaignCollection;
});