﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'datetimepicker',
  'text!sharedtemplates/inputcontrols/datetimeControlTemplate.html'
], function ($, _, backbone, alertify, datetimepicker, datetimeControlTemplate) {

    var datetimeControlView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;
            if (options.Value == null) {
                this.fieldValue = "";
                this.defaultValue = "";
            } else {
                this.fieldValue = options.Value;
                this.defaultValue = _.clone(options.Value);
            }
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.focusField = this.inputField;
            this.calenderOpened = false;

            this.validValue = true;
            this.entityId = options.EntityId;
            this.okToSave = false;
            this.saveButtonPressed = false;
        },
        events: function () {
            var theEvents = {};
            theEvents["blur " + this.inputField] = "onLostFocus";
            theEvents["click " + this.saveButton] = "onSave";
            theEvents["click " + this.undoButton] = "onUndo";
            theEvents["keydown " + this.inputField] = "onKeyDown";
            theEvents["change " + this.inputField] = "onChanged";
            theEvents["click " + this.inputField] = "onClick";

            return theEvents;
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                if (this.fieldValue != this.defaultValue) {
                    this.$el.find(this.inputField).val(this.defaultValue);
                    this.fieldValue = this.defaultValue;
                }
                this.onLostFocus(e);
                return;
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            if (e.keyCode == 13) {
                this.onSave(e);
            }

            if (this.calenderOpened) {
                this.$el.find(this.inputField).datetimepicker("destroy");
                this.calenderOpened = false;
                //console.log("tab close");
            }

            if (!this.calenderOpened) {
                this.$el.find(this.inputField).datetimepicker({
                    format: window.appHelper.momentDateTimeFormat,
                    lang: 'en',
                    weeks: appHelper.showWeeksInDatePicker,
                    dayOfWeekStart: appHelper.firstDayOfWeek
                });
                this.$el.find(this.inputField).datetimepicker("show");
                //console.log("tab open");
                this.calenderOpened = true;
            }
        },
        onClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            //console.log("open? " + this.calenderOpened + " val: " + this.$el.find(this.inputField).val());
            if (this.calenderOpened) {
                 this.$el.find(this.inputField).datetimepicker("destroy");
                this.calenderOpened = false;
                //console.log("click close");
            } else if (!this.calenderOpened) {
                this.$el.find(this.inputField).datetimepicker({
                    format: window.appHelper.momentDateTimeFormat,
                    lang: 'en',
                    weeks: appHelper.showWeeksInDatePicker,
                    dayOfWeekStart: appHelper.firstDayOfWeek
                });
                this.$el.find(this.inputField).datetimepicker("show");

                this.calenderOpened = true;
                console.log("click open");
            }
        },
        onUndo: function () {
            if (this.saveButtonPressed) {
                return;
            }

            this.$el.find(this.inputField).val(this.defaultValue);
            this.fieldValue = this.defaultValue;
            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onSave: function (e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');
            }
        },
        isDateTime: function (test) {
            return true;
        },
        onChanged: function (e) {
            var tempValue = this.$el.find(this.inputField).val();
          
            this.fieldValue = tempValue;

            if (this.isDateTime(this.fieldValue) || this.fieldValue == this.defaultValue) {
                this.$el.parent().removeClass("error");
                this.$('[data-error]').empty();
                if (this.fieldValue != this.defaultValue) {
                    this.$el.find(this.saveButton).addClass('active');
                    this.$el.find(this.undoButton).addClass('active');
                    window.appHelper.event_bus.trigger('fieldvalueupdated', this.fieldTypeId, this.fieldValue);

                    this.okToSave = true;
                } else {
                    this.$el.find(this.saveButton).removeClass('active');
                    this.$el.find(this.undoButton).removeClass('active');
                    window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);

                    this.okToSave = false;
                }
                this.$el.find(this.inputField).attr('title', '');
                this.validValue = true;
            } else {
                this.$el.parent().addClass("error");
                this.$('[data-error]').html("Only date/time format are allowed");
                this.$el.find(this.saveButton).removeClass('green-icon-button');
                this.$el.find(this.inputField).attr('title', 'Only date/time format are allowed');
                this.validValue = false;
                this.okToSave = false;
            }
        },
        onLostFocus: function (e) {
            var that = this;
            e.stopPropagation();

            var tempValue = this.$el.find(this.inputField).val();
            //if (tempValue.length > 10) {
            //    tempValue = tempValue.replace(" ", "T"); 
            //}

            this.fieldValue = tempValue; 
            if (this.isDateTime(this.fieldValue) || this.fieldValue == this.defaultValue) {
                this.$el.parent().removeClass("error");
                this.$('[data-error]').empty();
                if (this.fieldValue != this.defaultValue) {
                    this.$el.find(this.saveButton).addClass('active');
                    this.$el.find(this.undoButton).addClass('active');

                    this.okToSave = true;
                } else {
                    this.$el.find(this.saveButton).removeClass('active');
                    this.$el.find(this.undoButton).removeClass('active');

                    this.okToSave = false;
                }
                this.$el.find(this.inputField).attr('title', '');
                this.validValue = true;
            } else {
                this.$el.parent().addClass("error");
                this.$('[data-error]').html("Only date/time format are allowed");
                this.$el.find(this.saveButton).removeClass('green-icon-button');
                this.$el.find(this.inputField).attr('title', 'Only date/time format are allowed');
                this.validValue = false;
                this.okToSave = false;
            }

            // Use timeout to delay examination of historypopup until after blur/focus 
            // events have been processed.
            setTimeout(function () {
                if (!window.appSession.historyset) {
                    if (that.defaultValue == that.fieldValue && that.validValue) {
                        that.$el.find(that.inputField).datetimepicker("hide");
                        that.calenderOpened = false;
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        that.$el.find(that.inputField).datetimepicker("hide");
                        that.calenderOpened = false;
                        if (that.validValue) {
                            window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                        } else {
                            window.appHelper.event_bus.trigger('fieldvalueinvalid', that.fieldTypeId, that.fieldValue);
                        }
                    }
                }
            }, 80);
        },
        render: function () {
            var template = _.template(datetimeControlTemplate, { Id: this.fieldTypeId, Value: this.fieldValue.replace("T", " ") });
            this.setElement(template, true);
            //this.$el.find(this.inputField).datetimepicker({
            //    format: window.appHelper.momentDateTimeFormat,
            //    lang: 'en',
            //    closeOnDateSelect: true,
            //    weeks: appHelper.showWeeksInDatePicker,
            //    dayOfWeekStart: appHelper.firstDayOfWeek
            //});
            return this;
        },
    });

    return datetimeControlView;
});

