﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var fieldModel = Backbone.DeepModel.extend({
        idAttribute: "id",
        initialize: function (options) {
            this.id = options.id; 
        },
        urlRoot: '/api/entityfields',
        
    });

    return fieldModel;

});
