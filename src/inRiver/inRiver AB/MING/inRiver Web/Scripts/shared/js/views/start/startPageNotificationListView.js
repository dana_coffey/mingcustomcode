define([
  'jquery',
  'underscore',
  'backbone',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/notifications/notificationCollection',
  'sharedjs/views/start/startPageNotificationView'
], function ($, _, Backbone, inRiverUtil, notificationCollection, startPageNotificationView) {

    var startPageMyNotificationListView = Backbone.View.extend({
        initialize: function (data) {
            var self = this;
            this.undelegateEvents();
            this.listenTo(window.appHelper.event_bus, "notificationsupdated", this.reload);
            this.notifications = new notificationCollection();
            this.notifications.fetch({
                data: { username: window.appHelper.userName },
                success: function () {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        close: function () {

            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            Backbone.View.prototype.remove.call(this);

        },
        events: {

        },
        reload: function () {

            $("#notificationtitle").hide();
            $("#notificationseparator").hide();
            var sectionEl = $("#notifications");

            sectionEl.show();

            var self = this;
            $("#notifications .loadingspinner").show();
            $("#notifications .white-box").html("");

            this.notifications.fetch({
                data: { username: window.appHelper.userName },
                success: function () {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {

            var sectionEl = $("#notifications");

            $("#notifications .loadingspinner").hide();

            var self = this;
            this.$el.empty();

            if (this.notifications.models.length == 0) {
                sectionEl.find(".white-box").append("<div class='no-items-text'>No notifications</div>");
                //sectionEl.hide();
                //$("#notificationtitle").hide();
                //$("#notificationseparator").hide();
                return this;
            }

            _.each(this.notifications.models, function (model) {
                var notification = new startPageNotificationView({
                    model: model,
                });

                sectionEl.find(".white-box").append(notification.render().el);
            });

            return this;
        }
    });

    return startPageMyNotificationListView;
});

