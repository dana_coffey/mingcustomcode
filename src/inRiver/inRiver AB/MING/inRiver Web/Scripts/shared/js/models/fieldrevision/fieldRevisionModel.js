﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var entityModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        
    });

    return entityModel;

});
