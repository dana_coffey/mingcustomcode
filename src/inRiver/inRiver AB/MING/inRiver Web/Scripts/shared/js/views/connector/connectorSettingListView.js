define([
  'jquery',
  'underscore',
  'backbone',
  'backgrid',
  //'extensions/GridDeleteItem',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
     'sharedjs/misc/inRiverUtil',

  'sharedjs/models/connector/connectorSettingModel',
  'sharedjs/collections/connectorsettings/connectorSettingsCollection',
  'sharedjs/views/connector/connectorSettingView',
  'sharedjs/views/connector/addConnectorSettingView',
  'text!sharedtemplates/connector/connectorSettingListTemplate.html'
], function ($, _, backbone, backgrid, permissionUtil, modalPopup, inRiverUtil, connectorSettingModel, connectorSettingsCollection, connectorSettingView, addConnectorSettingView, connectorSettingListTemplate) {

    var connectorSettingListView = backbone.View.extend({
        initialize: function() {
            //Get Settings
            var that = this;
            this.undelegateEvents();

            var onDataHandler = function() {
				
				// PRFT : Code modified for masking sensetive data for key. : Start
                var settingsKey = ["InsitePassword", "InsiteFtpPassword", "InforOdbcPassword", "CarrierApiPassword", "CarrierApiBearerTokenPassword", "FtpPassword", "AccessKeyId", "SecretAccessKey", "RoleArn"];

				that.collection.models.forEach(function (lineItem) {
                    settingsKey.forEach(function (settingKey) {
						
						if (lineItem.attributes.Key == settingKey) {
                            lineItem.attributes.Value = '****'
                        }
                    });
                });

                // PRFT : Code modified for masking sensitive data for key. : End
				
                that.render();
            };

            that.collection = new connectorSettingsCollection(([], { id: this.model.get("Id") }));
            that.collection.fetch({
                success: onDataHandler,
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            if (permissionUtil.CheckPermission("AddDeleteConnector")) {

                that.collection.on('rowClicked', function(model, selected) {
                    event.stopPropagation();
                    var modal = new modalPopup();
                    modal.popOut(new connectorSettingView({
                        model: model,
                    }), { size: "small", header: "Edit Setting", usesavebutton: true });

                    $("#save-form-button").removeAttr("disabled");
                });
            }
            //console.log("SettingListView init");

            this.listenTo(backbone, 'clickedAnywhere', function() { this.onUnloadSettingView(); });
        },

        events: {
            "click #newSetting": "newSetting",
            "click #edit-panel": "onClick"
        },
        newSetting: function() {
            var modal = new modalPopup();

            modal.popOut(new addConnectorSettingView({
                parent: this,
            }), { size: "small", header: "Create Setting", usesavebutton: true });

            $("#save-form-button").removeAttr("disabled");
        },
        onUnloadSettingView: function() {
            //console.log("onUnloadSettingView");
            if (this._editCvlView) {
                $("#edit-panel").slideUp(200, "swing");
                backbone.trigger('closeEditPanel');
            }
            this._editCvlView = null;

        },
        onClick: function() {
            event.stopPropagation();

        },
        render: function() {
            this.$el.html(connectorSettingListTemplate);

            var columns = [
                { name: "Key", label: "Setting", cell: "string" },
                { name: "Value", label: "Value", cell: "string" }
            ];

            // Initialize a new Grid instance
            this.grid = new backgrid.Grid({
                row: ClickableRow,
                columns: columns,
                collection: this.collection,
                className: "backgrid backgrid-striped"
            });

            this.$el.find("#settings-list").append(this.grid.render().$el);

            permissionUtil.CheckPermissionForElement("AddDeleteConnector", this.$el.find("#newSetting"));


            return this;
        }
    });

    return connectorSettingListView;
});
