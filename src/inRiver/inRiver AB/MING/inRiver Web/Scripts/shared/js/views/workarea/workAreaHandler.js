define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui-touch-punch',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'jstree',
    'alertify',
    'sharedjs/collections/workareas/workareaCollection',
    'sharedjs/models/entity/entityModel',
    'sharedjs/collections/relations/relationTypeCollection',
    'sharedjs/collections/entities/entityCollection',
    'sharedjs/views/entitycard/entitycardView',
    'sharedjs/views/workarea/quickSearchWorkAreaView',
    'sharedjs/views/advancedsearch/queryEditorView',
    'sharedjs/models/workarea/workareaModel',
    'sharedjs/views/workarea/workAreaView',
    'sharedjs/views/panelsplitter/panelBorders'
], function ($, _, backbone, jqtp, modalPopup,inRiverUtil,  jstree, alertify, workareaCollection, entityModel, relationTypeCollection, entityCollection, entitycardView, quickSearchWorkAreaView, queryEditorView, workareaModel, workAreaView, panelBorders) {

    var workAreaHandler = backbone.View.extend({
        initialize: function(option) {
            this.mainWorkAreaView = null;
            this.secondWorkAreaView = null;

            this.app = "enrich"; 
            if (option.app) {
                this.app = option.app;
            }

            if (window.appHelper.mainWorkAreaView != null) {
                this.mainWorkAreaView = window.appHelper.mainWorkAreaView;
            }

            if (window.appHelper.secondWorkAreaView != null) {
                this.secondWorkAreaView = window.appHelper.secondWorkAreaView;
            }

            if (this.app == "enrich") {
                this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.closeEntityarea);
                this.listenTo(window.appHelper.event_bus, 'entityareaclose', this.closeEntityarea);
            }
            this.mode = "wide";
            this.numberOfWorkareas = 0;
         
        },
        events: {

        },

        //Do stuff: 

        search: function(data) {
            var title = "Search";
            var workarea; 
            if (data.workarea) {
                workarea = data.workarea; 
            } else {
                workarea = this.getActiveWorkarea();
            }

            workarea.performQuickSearch({ title: title, searchPhrase: data.searchPhrase });
        },
        querysearch: function(data) {
            var title = "Query";
            var workarea; 
            if (data.workarea) {
                workarea = data.workarea; 
            } else {
                workarea = this.getActiveWorkarea();
            }

            if (data.advsearchPhrase == null) {
                data.advsearchPhrase = workarea.advsearchPhrase;
            }

            workarea.performQuerySearch({ title: title, advsearchPhrase: data.advsearchPhrase });


        },
        openSavedWorkarea: function(data) {
            var title = data.title;
            var workarea; 
            if (data.workarea) {
                workarea = data.workarea; 
            } else {
                workarea = this.getActiveWorkarea();
            }
            workarea.openSavedWorkarea({ title: title, workareaId: data.workareaId });
        },
        openSavedQuery: function(data) {
            var title = data.title;
            var workarea; 
            if (data.workarea) {
                workarea = data.workarea; 
            } else {
                workarea = this.getActiveWorkarea();
            }

            workarea.openSavedQuery({ title: title, workareaId: data.workareaId });
        },
        openNotifyMeQuery: function(data) {
            var title = "Search";
            var workarea = this.getActiveWorkarea();

            workarea.performQuickSearch({ title: title, searchPhrase: data.searchPhrase });
        },
        openEmptyWorkarea: function(data) {
           // var title = "Empty";
            var workarea; 
            if (data.workarea) {
                workarea = data.workarea; 
            } else {
                workarea = this.getActiveWorkarea();
            }

            workarea.clearWorkarea();
        },
        openWorkareaWithEntites: function(data) {
            var title = data.title;
            var workarea = this.getActiveWorkarea();

            workarea.performLoadCompressedEntities({ title: title, compressedEntities: data.compressedEntities });
        },
        loadData: function(workarea) {
            if (workarea.workareaId) {
                if (workarea.isQuery) {
                    this.openSavedQuery({ workarea: workarea, title: workarea.title, workareaId: workarea.workareaId });
                } else {
                    this.openSavedWorkarea({ workarea: workarea, title: workarea.title, workareaId: workarea.workareaId });
                }
            } else if (workarea.searchPhrase) {
                this.search({searchPhrase: workarea.searchPhrase, workarea:workarea});
            } else if (workarea.advsearchPhrase) {
                this.querysearch({ advsearchPhrase: workarea.advsearchPhrase, workarea: workarea });
            } else if (workarea.compressedEntities) {
                this.openWorkareaWithEntites({ compressedEntities: workarea.compressedEntities, workarea: workarea });
            }
        },

        //Visualize
        showTwoWorkareas: function () {
            if (this.mode == "wide") {
                $("#workarea-left").addClass("workarea-container-split");
                $("#workarea-right").addClass("workarea-container-split");
            }
            if (this.mode == "wide") {
                $("#workarea-right").show();
            } else {
                $("#workarea-right").hide();
            }
            $("#workarea-left").show();

            this.mainWorkAreaView.$el.find("#closeworkarea").show();
            this.mainWorkAreaView.$el.find("#addworkarea").hide();
            this.secondWorkAreaView.$el.find("#closeworkarea").show();
            this.secondWorkAreaView.$el.find("#addworkarea").hide();

            if (this.mode == "wide") {
                this.mainWorkAreaView.$el.find("#switchworkarea").hide();
                this.secondWorkAreaView.$el.find("#switchworkarea").hide();
                this.mainWorkAreaView.$el.find("#activeworkarea").show();
                this.secondWorkAreaView.$el.find("#activeworkarea").show();
            } else {
                this.mainWorkAreaView.$el.find("#switchworkarea").show();
                this.secondWorkAreaView.$el.find("#switchworkarea").show();
                this.mainWorkAreaView.$el.find("#activeworkarea").hide();
                this.secondWorkAreaView.$el.find("#activeworkarea").hide();
            }
            this.numberOfWorkareas = 2;

        },
        showOneWorkarea: function() {
            $("#workarea-left").removeClass("workarea-container-split");
            $("#workarea-right").removeClass("workarea-container-split");
            $("#workarea-right").hide();
            $("#workarea-left").show();

            this.mainWorkAreaView.$el.find("#closeworkarea").hide();
            this.mainWorkAreaView.$el.find("#addworkarea").show();
            this.mainWorkAreaView.$el.find("#switchworkarea").hide();
            this.mainWorkAreaView.$el.find("#activeworkarea").hide();
            this.numberOfWorkareas = 1;
        },
        hideWorkareas: function () {
            $("#workarea-right").hide();
            $("#workarea-right").html("");
            $("#workarea-left").hide();
            $("#workarea-left").html("");
        },
        setSmallWorkareas: function () {
            if (this.mainWorkAreaView != null) {
                this.mainWorkAreaView.$el.find("#search-result-container").addClass("search-result-container-small");
                this.mainWorkAreaView.$el.find("#workarea-search-container").show();
                this.mainWorkAreaView.$el.find("#workarea-header-container").show();
             
            }
            if (this.secondWorkAreaView != null) {
                this.secondWorkAreaView.$el.find("#search-result-container").addClass("search-result-container-small");
                this.secondWorkAreaView.$el.find("#workarea-search-container").show();
                this.secondWorkAreaView.$el.find("#workarea-header-container").show();
            }

            $("#workarea-left").removeClass("workarea-container-split");
            $("#workarea-right").removeClass("workarea-container-split");
            $("#workarea-left").show();
            $("#workarea-right").hide();
            if (this.numberOfWorkareas == 2) {
                this.mainWorkAreaView.$el.find("#switchworkarea").show();
                this.secondWorkAreaView.$el.find("#switchworkarea").show();
                this.mainWorkAreaView.$el.find("#activeworkarea").hide();
                this.secondWorkAreaView.$el.find("#activeworkarea").hide();
                
            }
            this.mode = "small";

          
        },
        setWideWorkareas: function () {

            if (this.mainWorkAreaView != null) {
                this.mainWorkAreaView.$el.find("#search-result-container").removeClass("search-result-container-small");
                this.mainWorkAreaView.$el.find("#workarea-search-container").hide();
                this.mainWorkAreaView.$el.find("#workarea-header-container").hide();

                if (this.mainWorkAreaView.$el.find("#search-result-container").resizable("instance")) {
                    this.mainWorkAreaView.$el.find("#search-result-container").resizable("destroy");
                }
                this.mainWorkAreaView.$el.find("#search-result-container").removeAttr("style");

            }
            if (this.secondWorkAreaView != null) {
                this.secondWorkAreaView.$el.find("#search-result-container").removeClass("search-result-container-small");
                this.secondWorkAreaView.$el.find("#workarea-search-container").hide();
                this.secondWorkAreaView.$el.find("#workarea-header-container").hide();
                this.secondWorkAreaView.$el.find("#search-result-container").removeAttr("style");
            }

            if (this.mainWorkAreaView != null && this.secondWorkAreaView != null) {
                $("#workarea-left").addClass("workarea-container-split");
                $("#workarea-right").addClass("workarea-container-split");
                $("#workarea-right").show();

            }
            if (this.numberOfWorkareas == 2) {
                this.mainWorkAreaView.$el.find("#switchworkarea").hide();
                this.secondWorkAreaView.$el.find("#switchworkarea").hide();
                this.mainWorkAreaView.$el.find("#activeworkarea").show();
                this.secondWorkAreaView.$el.find("#activeworkarea").show();
            }

            $("#workarea-left").show();
            this.mode = "wide";
            //this.$el.find("#workarea-header-container").hide();
        },
        closeEntityarea: function (id) {
            var self = this;
            if (id != null) {
                if ($("#entity-area").length && $("#entity-area").length > 0) {
                    var entid = $("#main-container div")[0].id;
                    if (entid != id) {
                        return;
                    }
                }
            }
            if (this.numberOfWorkareas == 2) {

                if (this.mainWorkAreaView.position == 2) {
                    this.switchWorkArea();
                }

            }
            this.setWideWorkareas(); 



            // Only if workarea open...
            if ($("#entity-area").length && $("#entity-area").length > 0) {
                if (this.mainWorkAreaView.workareaId) {
                    if (this.mainWorkAreaView.isQuery) {
                        self.goTo("workarea?title=" + self.mainWorkAreaView.title + "&workareaId=" + self.mainWorkAreaView.workareaId + "&isQuery=true", false);
                    } else {
                        self.goTo("workarea?title=" + self.mainWorkAreaView.title + "&workareaId=" + self.mainWorkAreaView.workareaId, false);
                        
                    }
                } else if (this.mainWorkAreaView.searchPhrase) {
                    this.goTo("workarea?title=Query&searchPhrase=" + this.mainWorkAreaView.searchPhrase, false);
                } else if (this.mainWorkAreaView.advsearchPhrase) {
                    this.goTo("workarea?title=Query&advsearch=" + Math.random(10000), false);
                } else if (this.mainWorkAreaView.title && this.mainWorkAreaView.compressedEntities) {
                    self.goTo("workarea?title=" + self.mainWorkAreaView.title + "&compressedEntities=" + this.mainWorkAreaView.compressedEntities, false);
                }
            }
            window.appSession.entityViewTabId = ""; 
        },
        renderResults: function () {
            if (this.mainWorkAreaView != null) {
                this.mainWorkAreaView.showResult();
            }
            if (this.secondWorkAreaView != null) {
                this.secondWorkAreaView.showResult();
            }
        },
        setJsTreeDnd: function (value) {
            if (this.mainWorkAreaView != null) {
                this.mainWorkAreaView.setJsTreeDnd(value);
            }
            if (this.secondWorkAreaView != null) {
                this.secondWorkAreaView.setJsTreeDnd(value);
            }
        },

        setCollapsedTitle: function () {

            var container = $('#search-result-container'); 

            if (container && container.offset() && container.offset().left == 15) {
                
                var text = $("#search-result-query h4").text();

                if ($("#search-result-query h4").length > 1) {
                    text = text.replace(")", ") | ");
                }

                $('#workarea-header-label span').html(text);
            }
        },
        //Open/create etc.
        switchActive: function(workarea) {
            if (!this.mainWorkAreaView || ! this.secondWorkAreaView) {
                return; 
            }

            if (workarea == this.mainWorkAreaView) {
                this.setWorkareaPosition(this.mainWorkAreaView, this.mainWorkAreaView.position, "Active");
                this.setWorkareaPosition(this.secondWorkAreaView, this.secondWorkAreaView.position, "Passive");
            } else {
                this.setWorkareaPosition(this.mainWorkAreaView, this.mainWorkAreaView.position, "Passive");
                this.setWorkareaPosition(this.secondWorkAreaView, this.secondWorkAreaView.position, "Active");
            }

         

        }, 

        reopenOrAddWorkAreas: function (options) {
            var maindata = this.getSessionStorage("mainWorkarea");
            var seconddata = this.getSessionStorage("secondWorkarea");


            //1. If search, do it. 
            //   If empty add workarea
            //   Otherwise restore

            if (options.workareaId != undefined || options.searchPhrase != undefined || options.advsearchPhrase != undefined || options.compressedEntities != undefined || options.entityIds != undefined) {
                options.handler = this; 

                options.id = 1;
                var workArea = new workAreaView(options);
                this.loadData(workArea);

                $("#workarea-left").html(workArea.$el);
                   this.mainWorkAreaView = workArea;
                window.appHelper.mainWorkAreaView = workArea;

                this.setSessionStorage("mainWorkarea", workArea.getWorkAreaState());
                this.showOneWorkarea();

                this.setWorkareaPosition(workArea, 1, "Active");


            }else if (maindata != null) {
                maindata.handler = this;
                maindata.id = 1; 
                var mainWorkArea = new workAreaView(maindata);
                this.loadData(mainWorkArea);

                $("#workarea-left").html(mainWorkArea.$el);
                this.mainWorkAreaView = mainWorkArea;
                window.appHelper.mainWorkAreaView = mainWorkArea;
                
                if (this.mode == "small") {
                    this.mainWorkAreaView.$el.find("#search-result-container").addClass("search-result-container-small");
                    this.mainWorkAreaView.$el.find("#workarea-search-container").show();
                    this.mainWorkAreaView.$el.find("#workarea-header-container").show();

                }


                if (seconddata != null) {
                    seconddata.handler = this;
                    seconddata.id = 2;
                    var secondWorkArea = new workAreaView(seconddata);
                    this.loadData(mainWorkArea);

                    $("#workarea-right").html(secondWorkArea.$el);
                    this.secondWorkAreaView = secondWorkArea;
                    window.appHelper.secondWorkAreaView = secondWorkArea;
                    this.showTwoWorkareas();

                    if (this.mode == "small") {
                        this.secondWorkAreaView.$el.find("#search-result-container").addClass("search-result-container-small");
                        this.secondWorkAreaView.$el.find("#workarea-search-container").show();
                        this.secondWorkAreaView.$el.find("#workarea-header-container").show();

                    }

                    if (this.mainWorkAreaView.jstreednd==true) {
                        this.secondWorkAreaView.setJsTreeDnd(true);
                    }


                } else {
                    this.showOneWorkarea();
                }


            } else {
                this.addWorkarea(options);
            }

        },
        addWorkarea: function(options) {
            var supressSearch = false;

            if (options != undefined) {
                supressSearch = options.supressSearch; 
            }

            var mode = "card";

            if (options != undefined && options.mode)
            {
                mode = options.mode;
            }

            var workArea = new workAreaView({ title: "New Workarea", handler: this, supressSearch: supressSearch, state: "Active", mode: mode });
            
            if (this.mainWorkAreaView == null) {
                $("#workarea-left").html(workArea.$el);
                
                this.mainWorkAreaView = workArea;
                window.appHelper.mainWorkAreaView = workArea;
                this.setSessionStorage("mainWorkarea", workArea.getWorkAreaState());
                this.showOneWorkarea();

                this.setWorkareaPosition(workArea, 1, "Active");

            } else {
                $("#workarea-right").html(workArea.$el);
                this.secondWorkAreaView = workArea;
                window.appHelper.secondWorkAreaView = workArea;
                this.setSessionStorage("secondWorkarea", workArea.getWorkAreaState());

                this.showTwoWorkareas();
                this.setWorkareaPosition(workArea, 2, "Active");
                this.mainWorkAreaView.setState("Passive");

                if (this.mainWorkAreaView.$el.find("#search-result-container").hasClass("search-result-container-small")) {
                    this.secondWorkAreaView.$el.find("#search-result-container").addClass("search-result-container-small");
                    this.secondWorkAreaView.$el.find("#workarea-search-container").show();
                    this.secondWorkAreaView.$el.find("#workarea-header-container").show();
                }

                if (this.mainWorkAreaView.jstreednd) {
                    this.secondWorkAreaView.setJsTreeDnd(true); 
                }
            }

            workArea.clearWorkarea(); 
            return workArea;
        },
        setWorkareaPosition: function(workarea, position, state) {

            workarea.position = position;
            workarea.setState(state); 

        },
        closeWorkAreas: function () {
            if (this.secondWorkAreaView != null) {
                this.closeWorkArea(2);

            }

            if (this.mainWorkAreaView != null) {
                this.closeWorkArea(1);
                this.clearSessionStorage("mainWorkarea");
            }

            this.numberOfWorkareas = 0;
        },
        closeWorkArea: function (position) {
            var workarea;
            if (position == 1) {
                workarea = this.mainWorkAreaView;
                this.mainWorkAreaView = null;
                if (this.secondWorkAreaView != null) {
                    var newFirst = this.secondWorkAreaView;

                    $("#workarea-left").html(newFirst.$el);
                    $("#workarea-right").html("");
                   
                    newFirst.bindEvents();
                    this.mainWorkAreaView = newFirst;
                    window.appHelper.mainWorkAreaView = newFirst;
                    this.secondWorkAreaView = null;
                    window.appHelper.secondWorkAreaView = null;
                    this.clearSessionStorage("secondWorkarea");
                    this.setSessionStorage("mainWorkarea", newFirst.getWorkAreaState());
                    this.showOneWorkarea();
                    //newFirst.position = 1;
                    this.setWorkareaPosition(newFirst, 1, "Active");
                } else {
                    this.numberOfWorkareas = 0;
                }
                window.appHelper.mainWorkAreaView = this.mainWorkAreaView;
            } else {
                workarea = this.secondWorkAreaView;
                this.secondWorkAreaView = null;
                window.appHelper.secondWorkAreaView = null;
                this.clearSessionStorage("secondWorkarea");
                if (this.mainWorkAreaView) {
                    this.showOneWorkarea();
                    this.setWorkareaPosition(this.mainWorkAreaView, 1, "Active");
                }
            }
            if (workarea != null) {
                workarea.unbind();
                workarea.remove();
            }

        },
        switchWorkArea: function () {

            if (this.mainWorkAreaView != null && this.secondWorkAreaView != null) {

                if (this.mainWorkAreaView != null && this.mainWorkAreaView.savetreeid && this.mainWorkAreaView.$el.find(this.mainWorkAreaView.savetreeid).jstree(true)) {
                    this.mainWorkAreaView.$el.find(this.mainWorkAreaView.savetreeid).jstree(true).destroy();
                }

                if (this.mainWorkAreaView != null && this.mainWorkAreaView.workareatree && this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).jstree(true)) {
                    this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).jstree(true).destroy();
                } else if (this.mainWorkAreaView != null && this.mainWorkAreaView.workareatree) {
                    this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).html();
                }

                if (this.secondWorkAreaView != null && this.secondWorkAreaView.savetreeid && this.secondWorkAreaView.$el.find(this.secondWorkAreaView.savetreeid).jstree(true)) {
                    this.secondWorkAreaView.$el.find(this.secondWorkAreaView.savetreeid).jstree(true).destroy();
                }
                if (this.secondWorkAreaView != null && this.secondWorkAreaView.workareatree && this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).jstree(true)) {
                    this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).jstree(true).destroy();
                } else if (this.secondWorkAreaView != null && this.secondWorkAreaView.workareatree) {
                    this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).html();
                }

                var newFirst = this.secondWorkAreaView;
                var newSec = this.mainWorkAreaView;

                $("#workarea-left").html(newFirst.$el);
                $("#workarea-right").html(newSec.$el);

                this.setWorkareaPosition(newFirst, 1, "Active");
                this.setWorkareaPosition(newSec, 2, "Passive");
            
                this.secondWorkAreaView = newSec;
                this.mainWorkAreaView = newFirst;

                this.bindEvents();

                window.appHelper.mainWorkAreaView = newFirst;
                window.appHelper.secondWorkAreaView = newSec;

                if (this.mode == "small") {
                    panelBorders.initAllEvents();
                }
            }
        },
        deleteWorkArea: function (position, workareaId) {
            var confirmCallback = function (self) {
                self.workareaModel = new workareaModel();
                self.workareaModel.url = "/api/workarea/" + workareaId;
                self.workareaModel.fetch({
                    success: function () {
                        self.workareaModel.destroy({
                            success: function () {
                                self.remove();
                                self.unbind();
                                if (position != null) {
                                    self.closeWorkArea(position);
                                    self.addWorkarea();
                                } else {
                                    self.goTo("workarea?title=Workarea");
                                }
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            if (workareaId) {
                inRiverUtil.NotifyConfirm("Confirm action", "Do you want to delete this workarea?", confirmCallback, this);
            }
        },
        hasWorkareas: function () {
            return this.mainWorkAreaView != null; 
        },
        getActiveWorkarea: function() {
            if (this.numberOfWorkareas == 0) {
                this.addWorkarea();
            }
            if (this.numberOfWorkareas == 1) {
                return this.mainWorkAreaView;
            } else {
                if (this.mode == "small") {
                    return this.mainWorkAreaView; 
                }

                if (this.mainWorkAreaView.state == "Active") {
                    return this.mainWorkAreaView;
                } else if (this.secondWorkAreaView.state == "Active") {
                    return this.secondWorkAreaView;
                } else {
                    this.mainWorkAreaView.setState("Active");
                    this.secondWorkAreaView.setState("Passive");
                    this.storeWorkAreaStates();
                    return this.mainWorkAreaView; 
                }
            }
        },
        hasEmptyWorkareas: function () {
            var wa = this.getSessionStorage("mainWorkarea");

            if ( wa != null && (wa.entityIds != null || wa.workareaId != null)) {
                return false; 
            }

            if (this.mainWorkAreaView != null) {
                return this.mainWorkAreaView.entityIds == null && this.mainWorkAreaView.workareaId == null; 
            }
            return true; 

        },
        bindEvents: function () {

            this.stopListening(window.appHelper.event_bus);
            if (this.app == "enrich") {
                this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.closeEntityarea);
                this.listenTo(window.appHelper.event_bus, 'entityareaclose', this.closeEntityarea);
            }

            if (this.mainWorkAreaView != null && this.mainWorkAreaView.savetreeid && this.mainWorkAreaView.$el.find(this.mainWorkAreaView.savetreeid).jstree(true)) {
                this.mainWorkAreaView.$el.find(this.mainWorkAreaView.savetreeid).jstree(true).destroy();
            }

            if (this.mainWorkAreaView != null && this.mainWorkAreaView.workareatree && this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).jstree(true)) {
                this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).jstree(true).destroy();
            } else if (this.mainWorkAreaView != null && this.mainWorkAreaView.workareatree) {
                this.mainWorkAreaView.$el.find(this.mainWorkAreaView.workareatree).html();
            }

            if (this.secondWorkAreaView != null && this.secondWorkAreaView.savetreeid && this.secondWorkAreaView.$el.find(this.secondWorkAreaView.savetreeid).jstree(true)) {
                this.secondWorkAreaView.$el.find(this.secondWorkAreaView.savetreeid).jstree(true).destroy();
            }
            if (this.secondWorkAreaView != null && this.secondWorkAreaView.workareatree && this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).jstree(true)) {
                this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).jstree(true).destroy();
            } else if (this.secondWorkAreaView != null && this.secondWorkAreaView.workareatree) {
                this.secondWorkAreaView.$el.find(this.secondWorkAreaView.workareatree).html();
            }

            if (this.mainWorkAreaView != null) {
                this.mainWorkAreaView.bindEvents();
            }
            if (this.secondWorkAreaView != null) {
                this.secondWorkAreaView.bindEvents();
            }
        },

        //session storage
        storeWorkAreaStates: function () {
            if (this.mainWorkAreaView != null) {
                this.setSessionStorage("mainWorkarea", this.mainWorkAreaView.getWorkAreaState()); 
            }
            if (this.secondWorkAreaView != null) {
                this.setSessionStorage("secondWorkarea", this.secondWorkAreaView.getWorkAreaState());
            
            }
        },
        setSessionStorage: function(name, data) {
            if (typeof (Storage) !== "undefined") {

                sessionStorage.setItem(name, JSON.stringify(data)); 
               
            } else {
                // Sorry! No Web Storage support..
            }
        },
        clearSessionStorage: function (name) {
            if (typeof (Storage) !== "undefined") {

                sessionStorage.removeItem(name);

            } else {
                // Sorry! No Web Storage support..
            }
        },
        getSessionStorage: function(name) {
            if (typeof (Storage) !== "undefined") {

                var data = sessionStorage.getItem(name);
                if (data == null) {
                    return null;
                }

                return JSON.parse(data); 

            } else {
                return null; 
            }
        }

    });

    return workAreaHandler;
});
