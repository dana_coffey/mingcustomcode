define([
  'jquery',
  'underscore',
  'backbone',
  //'models/user/UserModel'
], function($, _, Backbone, UserModel){
  var usersCollection = Backbone.Collection.extend({
      //model: UserModel,
      url: '/api/user'
  });
 
  return usersCollection;
});
