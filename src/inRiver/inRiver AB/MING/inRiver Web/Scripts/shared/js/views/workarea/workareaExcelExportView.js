﻿define([
    'jquery',
    'underscore',
    'backbone',
    'modalPopup',
    '../../../../libs/filedownload/jquery.fileDownload',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/models/export/exportExcelConfigurationModel',
    'sharedjs/models/export/exportExcelModel',
    'sharedjs/models/entityType/entityTypeModel',
    'sharedjs/collections/export/exportExcelConfigurationCollection',
    'sharedjs/collections/entityTypes/entityTypeCollection',
    'sharedjs/collections/fieldsets/fieldsetCollection',
    'sharedjs/collections/languages/LanguagesCollection',
    'sharedjs/collections/relatedlinktypes/relatedLinkTypesCollection',
    'sharedjs/collections/workareas/fieldTypesCollection',
    'sharedjs/collections/fieldtypes/fieldTypeAndfieldSetCollection',
    'sharedjs/views/workarea/workAreaExcelExportCvlControlView',
    'text!sharedtemplates/workarea/workareaExcelExportTemplate.html',
    'text!sharedtemplates/workarea/workareaFieldsetCVLControlTemplate.html',
    'text!sharedtemplates/export/excelDownloadTemplate.html'
], function ($, _, backbone, modalPopup, fileDownload, inRiverUtil, exportExcelConfigurationModel, exportExcelModel, entityTypeModel, exportExcelConfigurationCollection, entityTypeCollection, fieldsetCollection, languagesCollection, relatedLinkTypesCollection, fieldTypesCollection, fieldTypeAndfieldSetCollection, workAreaExcelExportCvlControlView, workareaExcelExportTemplate, workareaFieldsetCvlControlTemplate, excelDownloadTemplate) {
    var excelExportView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.entityTypeIds = [];

            for (var index = 0; index < options.entitytypeIds.length; ++index) {
                if (!_.contains(this.entityTypeIds, options.entitytypeIds[index].id)) {
                    this.entityTypeIds.push(options.entitytypeIds[index].id);
                }
            }

            //this.entityIdsCollection = options.entityIds;
            this.entityIds = options.entityIds;
            this.firstFieldSetsCollection = new Backbone.Collection();
            this.firstFieldCollection = new Backbone.Collection();

            this.secondFieldSetsCollection = new Backbone.Collection();
            this.secondFieldCollection = new Backbone.Collection();

            this.entityTypeCollection = new entityTypeCollection();

            this.entityTypeCollection.fetch({
                success: function () {
                    self.render();
                }
            });

            this.languagesCollection = new languagesCollection();

            this.languagesCollection.fetch({
                success: function () {
                    self.render();
                }
            });
        },
        events: {
            "click #excel-export": "doExcelExport",
            "click #excel-export-save-configuration": "doShowConfigurationExportDialog",
            "click #cancelconfiguration": "doCancelExportConfigurationDialog",
            "click #saveconfiguration": "doSaveExportConfiguration",
            "change #workarea-export-excel-value-select-type": "changeEntityTypeExcelExport",
            "change #workarea-export-excel-value-include-type": "changeSecondEntityTypeExcelExport",
            "change #workarea-export-excel-value-select-fieldset": "changeFirstFieldSetCvlControlExcelExport",
            "change #workarea-export-excel-value-include-fieldset": "changeSecondFieldSetCvlControlExcelExport",
            "change #workarea-export-excel-value-configuration": "doChangeExportConfiguration",
            "click #removeConfiguration": "removeConfiguration",
        },
        doExcelExport: function () {
            var self = this;
            var entitTypeIdValue = this.$el.find("#workarea-export-excel-value-select-type").val();

            if (entitTypeIdValue != null) {

                this.$el.find("#excel-loading-spinner-container").show();
                this.$el.find("#excel-export").prop("disabled", true);
                this.$el.find("#excel-export-save-configuration").prop("disabled", true);

                var fieldTypeIdValues = this.$el.find("#workarea-export-excel-value-select-field").val();
                var fieldSetValues = this.$el.find("#workarea-export-excel-value-select-fieldset").val();
                var languageValues = this.$el.find("#workarea-export-excel-value-select-languages").val();

                var includedEntityTypeValue = this.$el.find("#workarea-export-excel-value-include-type").val();
                var includedFieldSetValues = this.$el.find("#workarea-export-excel-value-include-fieldset").val();
                var includedFieldTypeValues = this.$el.find("#workarea-export-excel-value-include-field").val();

                var entityType = entitTypeIdValue[0];
                var fieldtypeAsString = "";
                var fieldSetAsString = "";
                var languageAsString = "";

                if (fieldTypeIdValues != null) {
                    fieldtypeAsString = fieldTypeIdValues.join(';');
                }

                if (fieldSetValues != null) {
                    fieldSetAsString = fieldSetValues.join(';');
                }

                if (languageValues != null) {
                    languageAsString = languageValues.join(';');
                }

                var includedEntityType = "";
                var includedFieldTypeAsString = "";
                var includedFieldSetAsString = "";

                if (includedEntityTypeValue != null && includedEntityTypeValue.length != 0) {
                    includedEntityType = includedEntityTypeValue[0];

                    if (includedFieldTypeValues != null) {
                        includedFieldTypeAsString = includedFieldTypeValues.join(';');
                    }

                    if (includedFieldSetValues != null) {
                        includedFieldSetAsString = includedFieldSetValues.join(';');
                    }
                }

                var entitiesAsString;

                entitiesAsString = this.entityIds.join(';');

                $.fileDownload("/api/exportexcel/download", {
                    httpMethod: "POST",
                    data: {
                        entitytypeid: entityType,
                        fieldtypeids: fieldtypeAsString,
                        fieldsets: fieldSetAsString,
                        languages: languageAsString,
                        relatedentitytypeid: includedEntityType,
                        relatedfieldtypeids: includedFieldTypeAsString,
                        relatedfieldsets: includedFieldSetAsString,
                        entities: entitiesAsString
                    },
                    successCallback: function () {
                        self.$el.find("#excel-export").prop("disabled", false);
                        self.$el.find("#excel-export-save-configuration").prop("disabled", false);

                        self.$el.find("#excel-loading-spinner-container").hide();
                    },
                    failCallback: function (responseHtml, url) {
                        self.$el.find("#excel-export").prop("disabled", false);
                        self.$el.find("#excel-export-save-configuration").prop("disabled", false);

                        self.$el.find("#excel-loading-spinner-container").hide();

                        inRiverUtil.OnErrors(responseHtml, url);
                    }
                });
            }
        },
        doCancelExportConfigurationDialog: function () {
            this.$el.find("#savedialog").hide();
        },
        doShowConfigurationExportDialog: function () {

            this.$el.find("#savedialog").show();
            this.$el.find("#savedialog").position({
                of: this.$el.find("#excelExport"),
                my: "left top",
                at: "left bottom+10"
            });
        },
        doSaveExportConfiguration: function () {
            var self = this;
            var entitTypeIdValue = this.$el.find("#workarea-export-excel-value-select-type").val();
            var fieldTypeIdValues = this.$el.find("#workarea-export-excel-value-select-field").val();
            var fieldSetValues = this.$el.find("#workarea-export-excel-value-select-fieldset").val();
            var languageValues = this.$el.find("#workarea-export-excel-value-select-languages").val();

            var includedEntityTypeValue = this.$el.find("#workarea-export-excel-value-include-type").val();
            var includedFieldSetValues = this.$el.find("#workarea-export-excel-value-include-fieldset").val();
            var includedFieldTypeValues = this.$el.find("#workarea-export-excel-value-include-field").val();

            this.configurationname = this.$el.find("#configurationname").val();

            if (this.configurationname == "Default"
                || this.configurationname == "default") {
                inRiverUtil.Notify("Adding Default/default configuration is invalid.");
                return;
            }

            this.configurationModel = new exportExcelConfigurationModel({ name: this.configurationname });
            if (entitTypeIdValue != null && entitTypeIdValue.length != 0) {
                this.configurationModel.attributes.EntityTypeId = entitTypeIdValue[0];
            }

            this.configurationModel.attributes.FieldTypeIds = fieldTypeIdValues;
            this.configurationModel.attributes.FieldSetIds = fieldSetValues;
            this.configurationModel.attributes.Languages = languageValues;

            if (includedEntityTypeValue != null && includedEntityTypeValue.length != 0) {
                this.configurationModel.attributes.IncludedEntityTypeId = includedEntityTypeValue[0];
                this.configurationModel.attributes.IncludedFieldTypeIds = includedFieldTypeValues;
                this.configurationModel.attributes.IncludedFieldSetIds = includedFieldSetValues;
            }
            
            this.exportConfigurationsCollection = new exportExcelConfigurationCollection();

            this.exportConfigurationsCollection.fetch({
                success: function () {

                    var configurationNames = [];

                    _.each(self.exportConfigurationsCollection.models, function(model) {
                        var configurationName = model.attributes.Name;

                        if (!_.contains(configurationNames, configurationName)) {
                            configurationNames.push(configurationName);
                        }
                    });

                    if (_.contains(configurationNames, self.configurationModel.name)) {
                        inRiverUtil.NotifyConfirm("Save Configuration", "Saving a configuration with a name that already exist <br> will overwrite the existing configuration.", function () {
                            self.saveConfiguration();
                        }, this);
                    } else {
                        self.saveConfiguration();
                    }
                }
            });

        },
        saveConfiguration: function () {
            var self = this;

            self.configurationModel.save(null, {
                success: function () {
                    self.$el.find("#savedialog").hide();

                    self.configurationModel.fetch({
                        success: function (configurationModel) {

                            var configurationNames = self.exportConfigurationsCollection.pluck("Name");

                            if (!_.contains(configurationNames, configurationModel.attributes.Name)) {
                                self.exportConfigurationsCollection.add(configurationModel);
                            } else {

                                _.each(self.exportConfigurationsCollection.models, function(model) {
                                    if (model.attributes.Name == configurationModel.attributes.Name) {
                                        self.exportConfigurationsCollection.remove(model);
                                        self.exportConfigurationsCollection.add(configurationModel);
                                        return;
                                    }
                                });

                            }

                            var defaultConfiguration = new exportExcelModel();
                            defaultConfiguration.attributes.Name = "Default";
                            self.exportConfigurationsCollection.add(defaultConfiguration, { at: 0 });

                            self.selectTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "configuration", Values: self.exportConfigurationsCollection, Multivalue: false, PossibleValues: self.configurationname });
                            self.template.find("#export-configuration-export-control-container").html(self.selectTypeCvlControl.$el);

                            self.$el.find("#export-configuration-export-control-container").css('float', 'left');
                            self.$el.find("#remove-confiugration-container").show();

                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        changeFirstFieldSetCvlControlExcelExport: function () {
            var self = this;
            var selectTypeElement = this.$el.find("#workarea-export-excel-value-select-type").val();
            var firstFieldSetValues = this.$el.find("#workarea-export-excel-value-select-fieldset").val();

            var fieldSetForEntityTypeCollection = new Backbone.Collection();

            //FIELDSET
            self.fieldsetCollection = new fieldsetCollection();
            this.fieldSetCollection.fetch({
                success: function () {

                    self.firstFieldSetsCollection = fieldSetForEntityTypeCollection;

                    _.each(self.fieldSetCollection.models, function (value) {

                        if (selectTypeElement[0] == value.attributes.EntityTypeId) {
                            fieldSetForEntityTypeCollection.add(value);
                        }
                    });
                }
            });

            this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: selectTypeElement[0] });

            this.fieldTypesCollection.fetch({
                success: function () {

                    if (fieldSetForEntityTypeCollection.length != 0) {

                        self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: selectTypeElement[0] });

                        self.fieldtypeAndFieldsetCollection.fetch({
                            success: function () {
                                var fielSetToNotFilterOut = [];
                                var fielSetToFilterOut = [];
                                var fieldset = [];

                                if (firstFieldSetValues != null && firstFieldSetValues.length > 0) {
                                    _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {
                                        if (_.contains(firstFieldSetValues, fieldsetModel.attributes.fieldsetid)) {
                                            fielSetToNotFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                        }

                                        if (!_.contains(firstFieldSetValues, fieldsetModel.attributes.fieldsetid)) {
                                            fielSetToFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                        }

                                    });

                                    _.each(fielSetToFilterOut, function (fieldSetNotToUse) {
                                        if (!_.contains(fielSetToNotFilterOut, fieldSetNotToUse)) {
                                            fieldset.push(fieldSetNotToUse);
                                        }
                                    });
                                } else {
                                    //filtrera ut alla 
                                    _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {

                                        if (!_.contains(fieldset, fieldsetModel.attributes.fieldtypeid)) {
                                            fieldset.push(fieldsetModel.attributes.fieldtypeid);
                                        }
                                    });
                                }

                                var possibleValues = [];
                                var filteredFieldTypeCollection = new Backbone.Collection();

                                _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                    if (!_.contains(fieldset, fieldTypeValue.id)) {
                                        if (firstFieldSetValues != null) {
                                            possibleValues.push(fieldTypeValue.id);
                                        }

                                        filteredFieldTypeCollection.add(fieldTypeValue);
                                    }
                                });

                                self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: possibleValues });
                                self.template.find("#select-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                self.firstFieldCollection = filteredFieldTypeCollection;
                            }
                        });
                    }
                    else {
                        if (self.fieldTypesCollection.length != 0) {
                            this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: self.fieldTypesCollection, Multivalue: true });
                            self.template.find("#select-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);

                            self.firstFieldCollection = self.fieldTypesCollection;
                        }
                    }
                }
            });

        },
        changeSecondFieldSetCvlControlExcelExport: function () {
            var self = this;
            var selectTypeElement = this.$el.find("#workarea-export-excel-value-include-type").val();
            selectTypeElement = selectTypeElement[0].split("$$");

            var secondFieldSetValues = this.$el.find("#workarea-export-excel-value-include-fieldset").val();

            var fieldSetForEntityTypeCollection = new Backbone.Collection();

            //FIELDSET
            self.fieldsetCollection = new fieldsetCollection();
            this.fieldSetCollection.fetch({
                success: function () {

                    self.firstFieldSetsCollection = fieldSetForEntityTypeCollection;

                    _.each(self.fieldSetCollection.models, function (value) {

                        if (selectTypeElement[0] == value.attributes.EntityTypeId) {
                            fieldSetForEntityTypeCollection.add(value);
                        }
                    });
                }
            });

            this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: selectTypeElement[0] });

            this.fieldTypesCollection.fetch({
                success: function () {

                    if (fieldSetForEntityTypeCollection.length != 0) {

                        self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: selectTypeElement[0] });

                        self.fieldtypeAndFieldsetCollection.fetch({
                            success: function () {
                                var fielSetToNotFilterOut = [];
                                var fielSetToFilterOut = [];
                                var fieldset = [];

                                if (secondFieldSetValues != null && secondFieldSetValues.length > 0) {
                                    _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {
                                        if (_.contains(secondFieldSetValues, fieldsetModel.attributes.fieldsetid)) {
                                            fielSetToNotFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                        }

                                        if (!_.contains(secondFieldSetValues, fieldsetModel.attributes.fieldsetid)) {
                                            fielSetToFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                        }

                                    });

                                    _.each(fielSetToFilterOut, function (fieldSetNotToUse) {
                                        if (!_.contains(fielSetToNotFilterOut, fieldSetNotToUse)) {
                                            fieldset.push(fieldSetNotToUse);
                                        }
                                    });
                                } else {
                                    _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {

                                        if (!_.contains(fieldset, fieldsetModel.attributes.fieldtypeid)) {
                                            fieldset.push(fieldsetModel.attributes.fieldtypeid);
                                        }
                                    });
                                }
                                var filteredFieldTypeCollection = new Backbone.Collection();
                                var possibleValues = [];

                                _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                    if (!_.contains(fieldset, fieldTypeValue.id)) {
                                        if (secondFieldSetValues != null) {
                                            possibleValues.push(fieldTypeValue.id);
                                        }

                                        filteredFieldTypeCollection.add(fieldTypeValue);
                                    }
                                });

                                self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: possibleValues });
                                self.template.find("#included-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                self.firstFieldCollection = filteredFieldTypeCollection;
                            }
                        });
                    }
                    else {
                        if (self.fieldTypesCollection.length != 0) {
                            this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: self.fieldTypesCollection, Multivalue: true });
                            self.template.find("#included-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);

                            self.firstFieldCollection = self.fieldTypesCollection;
                        }
                    }
                }
            });
        },
        changeEntityTypeExcelExport: function () {
            var self = this;

            var selectTypeElement = this.$el.find("#workarea-export-excel-value-select-type").val();
            this.fieldSetCollection = new fieldsetCollection();

            var fieldSetForEntityTypeCollection = new Backbone.Collection();

            //FIELDSET
            this.fieldSetCollection.fetch({
                success: function () {

                    self.firstFieldSetsCollection = fieldSetForEntityTypeCollection;

                    _.each(self.fieldSetCollection.models, function (value) {

                        if (selectTypeElement[0] == value.attributes.EntityTypeId) {
                            fieldSetForEntityTypeCollection.add(value);
                        }
                    });

                    if (fieldSetForEntityTypeCollection.length != 0) {
                        self.$el.find("#workarea-first-fieldset-container").empty();
                        self.$el.find("#workarea-first-fieldset-container").append(_.template(workareaFieldsetCvlControlTemplate, { Id: "select-fieldset" }));
                        this.selectFieldSetCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-fieldset", Values: fieldSetForEntityTypeCollection, Multivalue: true });
                        self.template.find("#select-fieldset-excel-export-control-container").html(this.selectFieldSetCvlControl.$el);
                    } else {
                        self.$el.find("#workarea-first-fieldset-container").empty();
                    }
                }
            });

            //FIELDS
            this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: selectTypeElement[0] });

            this.fieldTypesCollection.fetch({
                success: function () {

                    if (fieldSetForEntityTypeCollection.length != 0) {

                        self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: selectTypeElement[0] });

                        self.fieldtypeAndFieldsetCollection.fetch({
                            success: function () {

                                var filteredFieldTypeCollection = new Backbone.Collection();

                                var fieldSetFieldTypes = [];

                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldTypeWithFieldSet) {
                                    fieldSetFieldTypes.push(fieldTypeWithFieldSet.attributes.fieldtypeid);
                                });

                                var selectedFieldTypes = [];

                                _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                    if (!_.contains(fieldSetFieldTypes, fieldTypeValue.id)) {
                                        filteredFieldTypeCollection.add(fieldTypeValue);
                                        selectedFieldTypes.push(fieldTypeValue.id);
                                    }
                                });

                                self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: selectedFieldTypes });
                                self.template.find("#select-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                self.firstFieldCollection = filteredFieldTypeCollection;
                            }
                        });
                    }
                    else {
                        if (self.fieldTypesCollection.length != 0) {

                            var selectedFieldTypes = [];

                            _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                selectedFieldTypes.push(fieldTypeValue.id);
                            });

                            this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: self.fieldTypesCollection, Multivalue: true, PossibleValues: selectedFieldTypes });
                            self.template.find("#select-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);

                            self.firstFieldCollection = self.fieldTypesCollection;
                        }
                    }
                }
            });

            //SECOND ENTITYTYPE
            this.relatedLinksCollection = new relatedLinkTypesCollection([], { entityTypeId: selectTypeElement[0] });

            this.relatedLinksCollection.fetch({
                success: function () {

                    var defaultConfiguration = new entityTypeModel();
                    defaultConfiguration.attributes.DisplayName = "(none)";
                    defaultConfiguration.attributes.EntityTypeId = "(none)";
                    self.relatedLinksCollection.add(defaultConfiguration, { at: 0 });

                    this.includeTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-type", Values: self.relatedLinksCollection, Multivalue: false });
                    self.template.find("#include-data-excel-export-control-container").html(this.includeTypeCvlControl.$el);
                }
            });

            this.includeFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: null, Multivalue: true });
            this.template.find("#included-fields-excel-export-control-container").html(this.includeFieldCvlControl.$el);

            self.$el.find("#workarea-second-fieldset-container").empty();

        },
        changeSecondEntityTypeExcelExport: function () {
            var self = this;

            var selectIncludeTypeElement = this.$el.find("#workarea-export-excel-value-include-type").val();

            if (selectIncludeTypeElement[0] == "-1") {
                return;
            }

            if (selectIncludeTypeElement[0] == "(none)") {

                var selectTypeElement = this.$el.find("#workarea-export-excel-value-select-type").val();

                var selectedLanguages = [];

                _.each(this.languagesCollection.models, function (language) {
                    selectedLanguages.push(language.id);
                });

                this.selectLanguageCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-languages", Values: this.languagesCollection, Multivalue: true, PossibleValues: selectedLanguages });
                this.template.find("#language-excel-export-control-container").html(this.selectLanguageCvlControl.$el);

                this.relatedLinksCollection = new relatedLinkTypesCollection([], { entityTypeId: selectTypeElement[0] });

                this.relatedLinksCollection.fetch({
                    success: function () {

                        var defaultConfiguration = new entityTypeModel();
                        defaultConfiguration.attributes.DisplayName = "(none)";
                        defaultConfiguration.attributes.EntityTypeId = "(none)";
                        self.relatedLinksCollection.add(defaultConfiguration, { at: 0 });

                        this.includeTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-type", Values: self.relatedLinksCollection, Multivalue: false });
                        self.template.find("#include-data-excel-export-control-container").html(this.includeTypeCvlControl.$el);
                    }
                });

                this.template.find("#workarea-second-fieldset-container").empty();

                this.includeFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: null, Multivalue: true });
                this.template.find("#included-fields-excel-export-control-container").html(this.includeFieldCvlControl.$el);

                return;
            }

            selectIncludeTypeElement = selectIncludeTypeElement[0].split("$$");

            var fieldSetForEntityTypeCollection = new Backbone.Collection();

            //FIELDSET
            this.fieldSetCollection.fetch({
                success: function () {

                    self.secondFieldSetsCollection = fieldSetForEntityTypeCollection;

                    _.each(self.fieldSetCollection.models, function (value) {

                        if (selectIncludeTypeElement[0] == value.attributes.EntityTypeId) {
                            fieldSetForEntityTypeCollection.add(value);
                        }
                    });

                    if (fieldSetForEntityTypeCollection.length != 0) {
                        self.$el.find("#workarea-second-fieldset-container").empty();
                        self.$el.find("#workarea-second-fieldset-container").append(_.template(workareaFieldsetCvlControlTemplate, { Id: "include-fieldset" }));
                        this.selectFieldSetCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-fieldset", Values: fieldSetForEntityTypeCollection, Multivalue: true });
                        self.template.find("#include-fieldset-excel-export-control-container").html(this.selectFieldSetCvlControl.$el);
                    } else {
                        self.$el.find("#workarea-second-fieldset-container").empty();
                    }
                }
            });

            //FIELDTYPES
            this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: selectIncludeTypeElement[0] });

            this.fieldTypesCollection.fetch({
                success: function () {
                    if (fieldSetForEntityTypeCollection.length != 0) {

                        self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: selectIncludeTypeElement[0] });

                        self.fieldtypeAndFieldsetCollection.fetch({
                            success: function () {

                                var filteredFieldTypeCollection = new Backbone.Collection();

                                var fieldSetFieldTypes = [];

                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldTypeWithFieldSet) {
                                    fieldSetFieldTypes.push(fieldTypeWithFieldSet.attributes.fieldtypeid);
                                });

                                var selectedFieldTypes = [];

                                _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                    if (!_.contains(fieldSetFieldTypes, fieldTypeValue.id)) {
                                        filteredFieldTypeCollection.add(fieldTypeValue);
                                        selectedFieldTypes.push(fieldTypeValue.id);
                                    }
                                });

                                self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: selectedFieldTypes });
                                self.template.find("#included-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                self.secondFieldCollection = filteredFieldTypeCollection;
                            }
                        });
                    }
                    else {
                        if (self.fieldTypesCollection.length != 0) {

                            var selectedFieldTypes = [];

                            _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                                selectedFieldTypes.push(fieldTypeValue.id);
                            });

                            this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: self.fieldTypesCollection, Multivalue: true, PossibleValues: selectedFieldTypes });
                            self.template.find("#included-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);

                            self.secondFieldCollection = self.fieldTypesCollection;
                        }
                    }
                }
            });

        },
        doChangeExportConfiguration: function () {
            var self = this;
            var selectedConfiguration = self.$el.find("#workarea-export-excel-value-configuration").val();
            this.exportConfigurationsCollection = new exportExcelConfigurationCollection();

            if (selectedConfiguration == "Default") {
                this.render();
                return;
            }

            this.exportConfigurationsCollection.fetch({
                success: function () {
                    _.each(self.exportConfigurationsCollection.models, function (configurations) {
                        if (configurations.attributes.Name == selectedConfiguration[0]) {

                            //EntityType1
                            this.selectTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-type", Values: self.entityTypeCollection, Multivalue: false, PossibleValues: configurations.attributes.EntityTypeId });
                            self.template.find("#type-excel-export-control-container").html(this.selectTypeCvlControl.$el);

                            //FieldSet1
                            self.fieldSetCollection = new fieldsetCollection();

                            self.firstFieldSetForEntityTypeCollection = new Backbone.Collection();
                            self.fieldSetCollection.fetch({
                                success: function () {

                                    _.each(self.fieldSetCollection.models, function (value) {

                                        if (configurations.attributes.EntityTypeId == value.attributes.EntityTypeId) {
                                            self.firstFieldSetForEntityTypeCollection.add(value);
                                        }
                                    });

                                    self.firstFieldSetsCollection = self.firstFieldSetForEntityTypeCollection;

                                    if (self.firstFieldSetForEntityTypeCollection.length != 0) {
                                        self.$el.find("#workarea-first-fieldset-container").empty();
                                        self.$el.find("#workarea-first-fieldset-container").append(_.template(workareaFieldsetCvlControlTemplate, { Id: "select-fieldset" }));
                                        self.selectFieldSetCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-fieldset", Values: self.firstFieldSetForEntityTypeCollection, Multivalue: true, PossibleValues: configurations.attributes.FieldSetIds });
                                        self.template.find("#select-fieldset-excel-export-control-container").html(self.selectFieldSetCvlControl.$el);
                                    } else {
                                        self.$el.find("#workarea-first-fieldset-container").empty();
                                    }

                                    //FieldType1
                                    if (configurations.attributes.EntityTypeId != null) {
                                        self.firstFieldTypesCollection = new fieldTypesCollection([], { entityTypeId: configurations.attributes.EntityTypeId });

                                        self.firstFieldTypesCollection.fetch({
                                            success: function () {

                                                if (self.firstFieldSetForEntityTypeCollection.length != 0) {

                                                    self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: configurations.attributes.EntityTypeId });

                                                    self.fieldtypeAndFieldsetCollection.fetch({
                                                        success: function () {
                                                            var fielSetToNotFilterOut = [];
                                                            var fielSetToFilterOut = [];
                                                            var fieldset = [];

                                                            if (configurations.attributes.FieldSetIds != null && configurations.attributes.FieldSetIds.length > 0) {
                                                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {
                                                                    if (_.contains(configurations.attributes.FieldSetIds, fieldsetModel.attributes.fieldsetid)) {
                                                                        fielSetToNotFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }

                                                                    if (!_.contains(configurations.attributes.FieldSetIds, fieldsetModel.attributes.fieldsetid)) {
                                                                        fielSetToFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }

                                                                });

                                                                _.each(fielSetToFilterOut, function (fieldSetNotToUse) {
                                                                    if (!_.contains(fielSetToNotFilterOut, fieldSetNotToUse)) {
                                                                        fieldset.push(fieldSetNotToUse);
                                                                    }
                                                                });
                                                            } else {
                                                                //filtrera ut alla 
                                                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {

                                                                    if (!_.contains(fieldset, fieldsetModel.attributes.fieldtypeid)) {
                                                                        fieldset.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }
                                                                });
                                                            }
                                                            var filteredFieldTypeCollection = new Backbone.Collection();

                                                            _.each(self.firstFieldTypesCollection.models, function (fieldTypeValue) {
                                                                if (!_.contains(fieldset, fieldTypeValue.id)) {
                                                                    filteredFieldTypeCollection.add(fieldTypeValue);
                                                                }
                                                            });

                                                            self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: configurations.attributes.FieldTypeIds });
                                                            self.template.find("#select-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                                            self.firstFieldCollection = filteredFieldTypeCollection;
                                                        }
                                                    });
                                                } else {
                                                    if (self.firstFieldTypesCollection.length != 0) {
                                                        self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: self.firstFieldTypesCollection, Multivalue: true, PossibleValues: configurations.attributes.FieldTypeIds });
                                                        self.template.find("#select-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                                        self.firstFieldCollection = self.firstFieldTypesCollection;
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });

                            //Languges
                            this.selectLanguageCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-languages", Values: self.languagesCollection, Multivalue: true, PossibleValues: configurations.attributes.Languages });
                            self.template.find("#language-excel-export-control-container").html(this.selectLanguageCvlControl.$el);

                            //EntityType2
                            if (configurations.attributes.EntityTypeId != null) {
                                self.secondEntityTypeCollection = new relatedLinkTypesCollection([], { entityTypeId: configurations.attributes.EntityTypeId });

                                self.secondEntityTypeCollection.fetch({
                                    success: function () {

                                        var defaultConfiguration = new entityTypeModel();
                                        defaultConfiguration.attributes.DisplayName = "(none)";
                                        defaultConfiguration.attributes.EntityTypeId = "(none)";
                                        self.secondEntityTypeCollection.add(defaultConfiguration, { at: 0 });
                                        
                                        self.includeTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-type", Values: self.secondEntityTypeCollection, Multivalue: false, PossibleValues: configurations.attributes.IncludedEntityTypeId });
                                        self.template.find("#include-data-excel-export-control-container").html(self.includeTypeCvlControl.$el);
                                    }
                                });
                            }

                            //FieldSet2
                            self.secondFieldSetForEntityTypeCollection = new Backbone.Collection();
                            self.fieldSetCollection.fetch({
                                success: function () {

                                    var includedEntityTypeId = configurations.attributes.IncludedEntityTypeId.split("$$")[0];

                                    _.each(self.fieldSetCollection.models, function (value) {

                                        if (includedEntityTypeId == value.attributes.EntityTypeId) {
                                            self.secondFieldSetForEntityTypeCollection.add(value);
                                        }
                                    });

                                    self.secondFieldSetsCollection = self.secondFieldSetForEntityTypeCollection;

                                    if (self.secondFieldSetForEntityTypeCollection.length != 0) {
                                        self.$el.find("#workarea-second-fieldset-container").empty();
                                        self.$el.find("#workarea-second-fieldset-container").append(_.template(workareaFieldsetCvlControlTemplate, { Id: "include-fieldset" }));
                                        self.selectFieldSetCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-fieldset", Values: self.secondFieldSetForEntityTypeCollection, Multivalue: true, PossibleValues: configurations.attributes.IncludedFieldSetIds });
                                        self.template.find("#include-fieldset-excel-export-control-container").html(self.selectFieldSetCvlControl.$el);
                                    } else {
                                        self.$el.find("#workarea-second-fieldset-container").empty();
                                    }

                                    //FieldType2
                                    if (includedEntityTypeId != null) {

                                        self.secondfieldTypesCollection = new fieldTypesCollection([], { entityTypeId: includedEntityTypeId });

                                        self.secondfieldTypesCollection.fetch({
                                            success: function () {
                                                if (self.secondFieldSetForEntityTypeCollection.length != 0) {

                                                    self.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: includedEntityTypeId });

                                                    self.fieldtypeAndFieldsetCollection.fetch({
                                                        success: function () {
                                                            var fielSetToNotFilterOut = [];
                                                            var fielSetToFilterOut = [];
                                                            var fieldset = [];

                                                            if (configurations.attributes.IncludedFieldSetIds != null && configurations.attributes.IncludedFieldSetIds.length > 0) {
                                                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {
                                                                    if (_.contains(configurations.attributes.IncludedFieldSetIds, fieldsetModel.attributes.fieldsetid)) {
                                                                        fielSetToNotFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }

                                                                    if (!_.contains(configurations.attributes.IncludedFieldSetIds, fieldsetModel.attributes.fieldsetid)) {
                                                                        fielSetToFilterOut.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }

                                                                });

                                                                _.each(fielSetToFilterOut, function (fieldSetNotToUse) {
                                                                    if (!_.contains(fielSetToNotFilterOut, fieldSetNotToUse)) {
                                                                        fieldset.push(fieldSetNotToUse);
                                                                    }
                                                                });
                                                            } else {
                                                                //filtrera ut alla 
                                                                _.each(self.fieldtypeAndFieldsetCollection.models, function (fieldsetModel) {

                                                                    if (!_.contains(fieldset, fieldsetModel.attributes.fieldtypeid)) {
                                                                        fieldset.push(fieldsetModel.attributes.fieldtypeid);
                                                                    }
                                                                });
                                                            }
                                                            var filteredFieldTypeCollection = new Backbone.Collection();

                                                            _.each(self.secondfieldTypesCollection.models, function (fieldTypeValue) {
                                                                if (!_.contains(fieldset, fieldTypeValue.id)) {
                                                                    filteredFieldTypeCollection.add(fieldTypeValue);
                                                                }
                                                            });

                                                            self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: filteredFieldTypeCollection, Multivalue: true, PossibleValues: configurations.attributes.IncludedFieldTypeIds });
                                                            self.template.find("#included-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                                            self.firstFieldCollection = filteredFieldTypeCollection;
                                                        }
                                                    });
                                                } else {
                                                    if (self.secondfieldTypesCollection.length != 0) {
                                                        self.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: self.secondfieldTypesCollection, Multivalue: true, PossibleValues: configurations.attributes.IncludedFieldTypeIds });
                                                        self.template.find("#included-fields-excel-export-control-container").html(self.selectFieldCvlControl.$el);

                                                        self.secondFieldCollection = self.fieldTypesCollection;
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });

                    self.$el.find("#export-configuration-export-control-container").css('float', 'left');
                    self.$el.find("#remove-confiugration-container").show();
                }
            });

        },
        removeConfiguration: function () {
            var self = this;

            var selectedConfiguration = self.$el.find("#workarea-export-excel-value-configuration").val();

            if (selectedConfiguration != "Default") {
                this.configurationModel = new exportExcelConfigurationModel({ name: selectedConfiguration });
                inRiverUtil.NotifyConfirm("Delete Configuration", "Are you sure you want to delete this configuration?", function () {
                    self.configurationModel.fetch({
                        success: function () {
                            self.configurationModel.destroy({
                                success: function () {
                                    self.render();

                                    self.$el.find("#export-configuration-export-control-container").css('float', 'none');
                                    self.$el.find("#remove-confiugration-container").hide();
                                },
                                error: function (model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        }
                    });
                }, this);
            }
        },
        render: function () {
            if (this.entityTypeCollection.length == 0 ||
                this.languagesCollection.length == 0) {
                return null;
            }

            var self = this;

            this.template = this.$el.html(_.template(workareaExcelExportTemplate));

            this.template.find("#export-configuration-export-control-container").css('float', 'none');
            this.template.find("#remove-confiugration-container").hide();

            this.exportConfigurationsCollection = new exportExcelConfigurationCollection();

            this.exportConfigurationsCollection.fetch({
                success: function () {

                    var defaultConfiguration = new exportExcelModel();
                    defaultConfiguration.attributes.Name = "Default";
                    self.exportConfigurationsCollection.add(defaultConfiguration, { at: 0 });

                    self.selectTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "configuration", Values: self.exportConfigurationsCollection, Multivalue: false });
                    self.template.find("#export-configuration-export-control-container").html(self.selectTypeCvlControl.$el);
                }
            });

            var selectedEntityTypeCollection = new Backbone.Collection();

            _.each(this.entityTypeCollection.models, function (entityTypeModel) {
                if (_.contains(self.entityTypeIds, entityTypeModel.id)) {
                    selectedEntityTypeCollection.add(entityTypeModel);
                }
            });

            if (self.entityTypeIds.length == 1) {

                this.selectTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-type", Values: selectedEntityTypeCollection, Multivalue: false, PossibleValues: self.entityTypeIds });
                this.template.find("#type-excel-export-control-container").html(this.selectTypeCvlControl.$el);

                this.setSelectedFieldTypes(self.entityTypeIds[0]);
            } else {
                this.selectTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-type", Values: selectedEntityTypeCollection, Multivalue: false });
                this.template.find("#type-excel-export-control-container").html(this.selectTypeCvlControl.$el);

                this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: this.fieldTypesCollection, Multivalue: true });
                this.template.find("#select-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);
            }

            var selectedLanguages = [];

            _.each(this.languagesCollection.models, function (language) {
                selectedLanguages.push(language.id);
            });

            this.selectLanguageCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-languages", Values: this.languagesCollection, Multivalue: true, PossibleValues: selectedLanguages });
            this.template.find("#language-excel-export-control-container").html(this.selectLanguageCvlControl.$el);

            this.includeTypeCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-type", Values: this.relationTypeCollection, Multivalue: false });
            this.template.find("#include-data-excel-export-control-container").html(this.includeTypeCvlControl.$el);

            this.includeFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "include-field", Values: this.fieldTypesCollection, Multivalue: true });
            this.template.find("#included-fields-excel-export-control-container").html(this.includeFieldCvlControl.$el);

            return this;


        },
        setSelectedFieldTypes: function (entityTypeId) {
            var self = this;

            this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: entityTypeId });

            this.fieldTypesCollection.fetch({
                success: function () {

                    var selectedFieldTypes = [];

                    _.each(self.fieldTypesCollection.models, function (fieldTypeValue) {
                        selectedFieldTypes.push(fieldTypeValue.id);
                    });

                    this.selectFieldCvlControl = new workAreaExcelExportCvlControlView({ ControlDefinition: "select-field", Values: self.fieldTypesCollection, Multivalue: true, PossibleValues: selectedFieldTypes });
                    self.template.find("#select-fields-excel-export-control-container").html(this.selectFieldCvlControl.$el);

                    self.changeEntityTypeExcelExport();
                }
            });
        }
    });

    return excelExportView;
});