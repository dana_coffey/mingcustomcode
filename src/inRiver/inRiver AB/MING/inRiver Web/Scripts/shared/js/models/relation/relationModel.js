﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var relationModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.id = options.Id;
        },
        urlRoot: '/api/relation',
        
    });

    return relationModel;

});
