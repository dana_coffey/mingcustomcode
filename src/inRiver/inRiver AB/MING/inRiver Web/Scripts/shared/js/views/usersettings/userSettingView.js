define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/models/user/UserInformationModel',
  'text!sharedtemplates/usersettings/usersettingTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, userInformationModel, userSettingTemplate) {

    var userSettingView = Backbone.View.extend({

        initialize: function (options) {
            this.model = new userInformationModel();
            var that = this;

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.model.fetch({
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                  inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #save-form-button": "onSave",
            "click #cancel-form-button": "onCancel",
            "keyup #user-settings-firstname": "onDataUpdate",
            "keyup #user-settings-lastname": "onDataUpdate",
            "keyup #user-settings-email": "onDataUpdate",
            "change #user-settings-data-language": "onDataUpdate",
            "change #user-settings-model-language": "onDataUpdate",
            "keyup #user-settings-password1": "onDataUpdate",
            "keyup #user-settings-password2": "onDataUpdate"
        },
        onSave: function () {

            this.model.set({ FirstName: $("#user-settings-firstname").val() });
            this.model.set({ DataLanguage: $("#user-settings-data-language").val() });
            this.model.set({ ModelLanguage: $("#user-settings-model-language").val() });
            this.model.set({ Password: $("#user-settings-password1").val() });

            var authenticationType = this.model.get("AuthenticationType");

            if (authenticationType == "ActiveDirectory") {
                this.model.set({ LastName: $("#user-settings-lastname").text() });
                this.model.set({ Email: $("#user-settings-email").text() });
            }
            else {
                this.model.set({ LastName: $("#user-settings-lastname").val() });
                this.model.set({ Email: $("#user-settings-email").val() });
            }

            var that = this;

            this.model.save({
                success: that.done(),
                error: function(model, response) {
                   inRiverUtil.OnErrors(model, response);
                }
            });

            window.appHelper.userDisplayName = this.model.get("FirstName") + " " + this.model.get("LastName"); 

            window.appHelper.event_bus.trigger('userupdated');
        },
        onDataUpdate: function () {

            /*validate email*/

            if ($("#user-settings-password1").val() != $("#user-settings-password2").val()) {
                $("#user-settings-row-password1").addClass("error");
                $("#user-settings-row-password2").addClass("error");
                $("#save-form-button").attr("disabled", true);

                return;

            } else {
                $("#user-settings-row-password1").removeClass("error");
                $("#user-settings-row-password2").removeClass("error");
            }

            //if ($("#user-settings-email").val() != this.model.get("Email")) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if ($("#user-settings-email").val() != '' && !re.test($("#user-settings-email").val())) {

                $("#user-settings-row-email").addClass("error");
                $("#save-form-button").attr("disabled", true);

                return;
            } else {
                $("#user-settings-row-email").removeClass("error");
            }
            // }

            /*validate password*/

            var isupdated = false;

            if ($("#user-settings-firstname").val() != this.model.get("Firstname")) {
                isupdated = true;
            }

            if ($("#user-settings-lastname").val() != this.model.get("Lastname")) {
                isupdated = true;
            }

            if ($("#user-settings-email").val() != this.model.get("Email")) {
                isupdated = true;
            }

            if ($("#user-settings-data-language").val() != this.model.get("DataLanguage")) {
                isupdated = true;
            }

            if ($("#user-settings-model-language").val() != this.model.get("ModelLanguage")) {
                isupdated = true;
            }

            if ($("#user-settings-password1").val() != undefined && $("#user-settings-password1").val() != "") {
                isupdated = true;
            }

            if (isupdated) {
                $("#save-form-button").removeAttr("disabled");
            } else {
                $("#save-form-button").attr("disabled", true);
            }
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {

            this.$el.html(_.template(userSettingTemplate, { data: this.model.toJSON(), languages: this.model.get("AllLanguages") }));

            return this;
        }
    });

    return userSettingView;
});
