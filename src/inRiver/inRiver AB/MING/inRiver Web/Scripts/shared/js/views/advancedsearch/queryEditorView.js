﻿define([
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'sharedjs/misc/inRiverUtil',
  'jstree',
  'sharedjs/models/query/queryModel',
  'sharedjs/models/notification/notificationModel',
  'sharedjs/views/advancedsearch/queryRowView',
  'sharedjs/collections/workareas/workareaCollection',
  'text!sharedtemplates/query/queryEditorTemplate.html',
], function ($, _, backbone, moment, inRiverUtil, jstree, queryModel, notificationModel, queryRowView, workareaCollection, queryEditorTemplate) {

    var queryEditorView = backbone.View.extend({
        initialize: function (options) {
            var self = this;
            this.isSavedQuery = false;
            this.query = options.advancedQuery;
            this.predefinedEntityType = options.predefinedEntityType;

            if (this.query && this.query != "") {
                this.query = JSON.parse(this.query);
            }

            this.workareaId = options.workareaId;
            this.searchOrigin = "workarea";
            if (options.searchOrigin) {
                this.searchOrigin = options.searchOrigin;
            }
            this.notification = options.notification;
            this.notDate = "";
            this.notCount = ""; 

            this.rowId = 1;
            this.rows = [];
            if (this.notification != null) {

                this.query = this.notification.attributes.query; 
                this.render();

            } else if (this.workareaId != null && this.workareaId != "") {
                this.querymodel = new queryModel({ Id: this.workareaId });
                this.isSavedQuery = true;

                this.querymodel.fetch({
                    success: function (model) {

                        self.query = model.attributes.Query;
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

            } else {
                this.render();
            }
        },
        events: {
            "click #add-condition": "addCondition",
            "click #close-icon": "closepopup",
            "click #search": "doSearch",
            "click #query-remove-row": "removeCondition",
            "click #save": "doSave",
            "click #delete": "doDelete",
            "click #saveQuery": "doSaveQuery",
            "click #cancelQuery": "doCloseSaveQuery",
            "click #updateQuery": "doUpdateQuery",
            "click #notifymewhen": "doShowNotifyWhen",
            "click #cancelNotifyMeWhen": "doCloseNotifyMeWhen",
            "click #saveNotifyMeWhen": "doSaveNotification",
            "click #updateNotification": "doUpdateNotification",
            "change #notificationcondition": "onNotificationConditionChange",
            "click #deleteNotification": "doDeleteNotification"
            
        },
        cleanSystemList: function (list) {

            var currentList = [];
            _.each(this.rows, function (row) {
                
                if (!row.subview) {
                    currentList.push(row.querySystem);
                }
            });

            var resList = {};

            _.each(list, function (item, key) {
                if (!_.contains(currentList, key)) {
                    resList[key] = item; 
                }

            }); 

            return resList;

        },
        hasRelationCondition: function () {
            _.each(this.rows, function (row) {

                if (row.subview && row.conditionType == "Relation") {
                    return true; 
                }
            });

            return false; 
        },
        doSearch: function () {

            if (!this.validateForm()) {
                return; 
            }

            //Hämta ut frågan...
            var query = this.getQuery();

            window.appHelper.advQuery = JSON.stringify(query);

            this.closepopup();
            if (this.searchOrigin == "top") {
                this.goTo("workarea?title=Query&advsearch=" + Math.random(10000));
            } else {

                backbone.history.loadUrl("workareasearch?title=Query&advsearch=" + Math.random(10000));
                //this.goTo("workareasearch?title=Query&advsearch=" + Math.random(10000));
            }

            this.remove();
        },
        doDelete: function () {
            var confirmCallback = function (self) {
                var that = self;
                self.querymodel.destroy({
                    success: function() {
                        that.remove();
                        that.unbind();
                        that.closepopup();
                        that.goTo("workarea?title=Empty");
                        inRiverUtil.Notify("Workarea successfully deleted");
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            if (this.querymodel) {
                inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete this workarea?", confirmCallback, this);
            }
        },
        doSave: function () {
            if (!this.validateForm()) {
                return;
            }
            
            var self = this;
           
            if (this.workareas == undefined) {
                this.workareas = new workareaCollection();
                this.workareas.fetch({
                    success: function () {
                        self.renderSaveTree();
                        self.showSaveDialog();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.showSaveDialog(); 
            }
        },
        doCloseSaveQuery: function () {
            this.$el.find("#savedialog").hide();
        },
        doCloseNotifyMeWhen: function () {
            this.$el.find("#notifymewhendialog").hide();
        },
        doShowNotifyWhen: function () {
            this.doCloseSaveQuery(); 
            this.$el.find("#notifymewhendialog").show();
            //this.$el.find("#notifymewhendialog").position({
            //    of: this.$el.find("#queryEditor"),
            //    my: "left top",
            //    at: "left bottom+10"
            //}); 
        },
        doSaveNotification: function () {
            if (!this.validateForm()) {
                return;
            }

            this.$el.find("#notificationname").parent().find("#row-error").html("");
            this.$el.find("#notificationname").closest("div").removeClass("error");
            this.$el.find("#notificationparameter").parent().find("#row-error").html("");
            this.$el.find("#notificationparameter").closest("div").removeClass("error");

            //Check if data entered.
            var name = this.$el.find("#notificationname").val();
            var self = this;

            var checked = $("input[name=notifyby]:checked");
            var checkedlist = "";
            $.each(checked, function () {

                checkedlist += this.value + ",";

            });
            if (checkedlist.length > 0) {
               checkedlist = checkedlist.substring(0, checkedlist.length - 1); 
            }


            this.notificationmodel = new notificationModel();
            this.notificationmodel.set("query", this.getQuery());
            this.notificationmodel.set("name", name);
            this.notificationmodel.set("condition", this.$el.find("#notificationcondition").val());
            if (this.notificationmodel.get("condition") === "NotEmpty") {
                var date = this.$el.find("#notificationparameter").datetimepicker("getValue");
                this.notificationmodel.set("parameter", inRiverUtil.toRoundtripDateFormat(date));
            } else {
                this.notificationmodel.set("parameter", this.$el.find("#notificationparameter").val());
            }
            this.notificationmodel.set("username", window.appHelper.userName);
            this.notificationmodel.set("notificationtype", checkedlist);
            this.notificationmodel.set("status", "Active"); 

            //Validate input: 
            var cont = true; 
            if (name == "") {
                this.$el.find("#notificationname").parent().find("#row-error").html("Must have a name!");
                this.$el.find("#notificationname").closest("div").addClass("error");
                cont = false;
            }
            if (checkedlist.length === 0) {
                this.$el.find("#notifyby").parent().find("#row-error").html("Select at least one option!");
                this.$el.find("#notifyby").closest("div").addClass("error");
                cont = false;
            }
            if (this.notificationmodel.attributes.condition == "Exceeds" && isNaN(this.notificationmodel.attributes.parameter)) {
                this.$el.find("#notificationparameter").parent().find("#row-error").html("Must be a number!"); 
                this.$el.find("#notificationparameter").closest("div").addClass("error");
                cont = false; 
            }
            if (this.notificationmodel.attributes.condition == "NotEmpty" && this.notificationmodel.attributes.parameter == null) {
                this.$el.find("#notificationparameter").parent().find("#row-error").html("Must be a date!");
                this.$el.find("#notificationparameter").closest("div").addClass("error");
                cont = false;
            }
            if (cont == false) {
                return; 
            }


            this.notificationmodel.save(null, {
                success: function () {
                    inRiverUtil.Notify("Notification successfully created");
                    window.appHelper.event_bus.trigger("notificationsupdated");

                    self.unbind();
                    self.closepopup();
                    self.remove();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        doUpdateNotification: function () {
            var self = this;

            this.$el.find("#notificationname").parent().find("#row-error").html("");
            this.$el.find("#notificationname").closest("div").removeClass("error");
            this.$el.find("#notificationparameter").parent().find("#row-error").html("");
            this.$el.find("#notificationparameter").closest("div").removeClass("error");

            var checked = $("input[name=notifyby]:checked");
            var checkedlist = "";
            $.each(checked, function () {

                checkedlist += this.value + ",";

            });
            if (checkedlist.length > 0) {
                checkedlist = checkedlist.substring(0, checkedlist.length - 1);
            }

            this.notification.set("query", this.getQuery());
            this.notification.set("name", this.$el.find("#notificationname").val());
            this.notification.set("condition", this.$el.find("#notificationcondition").val());
            if (this.notification.get("condition") === "NotEmpty") {
                var date = this.$el.find("#notificationparameter").datetimepicker("getValue");
                this.notification.set("parameter", inRiverUtil.toRoundtripDateFormat(date));
            } else {
                this.notification.set("parameter", this.$el.find("#notificationparameter").val());
            }
            this.notification.set("username", window.appHelper.userName);
            this.notification.set("notificationtype", checkedlist);

            var statusChecked = $("input[id=notificationstatus]:checked");
            this.notification.set("status", statusChecked.val());

            //Validate input: 
            var cont = true;
            if (this.notification.attributes.name == "") {
                this.$el.find("#notificationname").parent().find("#row-error").html("Must have a name!");
                this.$el.find("#notificationname").closest("div").addClass("error");
                cont = false;
            }
            if (this.notification.attributes.condition == "Exceeds" && isNaN(this.notification.attributes.parameter)) {
                this.$el.find("#notificationparameter").parent().find("#row-error").html("Must be a number!");
                this.$el.find("#notificationparameter").closest("div").addClass("error");
                cont = false;
            }
            if (cont == false) {
                return;
            }

            this.notification.save(null, {
                success: function () {
                    inRiverUtil.Notify("Notification successfully updated");
                    window.appHelper.event_bus.trigger("notificationsupdated");
                    self.unbind();
                    self.closepopup();
                    self.remove();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        doDeleteNotificationConfirm: function (self) {
                    self.notification.destroy();
                    window.appHelper.event_bus.trigger("notificationsupdated");
                    self.closepopup();
            inRiverUtil.Notify("Notification successfully removed");
        },
        doDeleteNotification: function () {
            var self = this;
            inRiverUtil.NotifyConfirm("Confirm remove notification", "Do you want to remove this notification?", this.doDeleteNotificationConfirm, self);
        },
        onNotificationConditionChange: function () {
            var val = this.$el.find("#notificationcondition").val();
            if (val === "NotEmpty") {
                this.notCount = this.$el.find("#notificationparameter").val();
                this.$el.find("#notificationparameter").val(this.notDate);

                this.$el.find("#notificationparameter").datetimepicker({
                    format: appHelper.momentDateTimeFormat,
                    formatTime: appHelper.momentTimeFormat,
                    formatDate: appHelper.momentDateFormat,
                    lang: 'en',
                    weeks: appHelper.showWeeksInDatePicker,
                    dayOfWeekStart: appHelper.firstDayOfWeek
                });

            } else {
                this.notDate = this.$el.find("#notificationparameter").val();
                this.$el.find("#notificationparameter").val(this.notCount);

                this.$el.find("#notificationparameter").datetimepicker('destroy');
            }
        },
        showSaveDialog: function () {
            this.doCloseNotifyMeWhen();
            this.$el.find("#savedialog").show();
        },
        doSaveQuery: function () {
            //Check if data entered.
            var selectedNode = this.$el.find("#work-area-selection").jstree(true).get_selected(false);
            var name = this.$el.find("#queryname").val();
            var self = this;

            if (selectedNode.length < 1 || (name == null || name == "")) {
                inRiverUtil.NotifyError("Missing requirements.", "Name and node must be selected");
                return;
            }

            var allworkareas = new workareaCollection();
            allworkareas.fetch({
                success: function (collection) {
                    var alreadyExist = false;
                    _.each(collection.models, function(model) {
                        if (name == model.attributes["text"] && selectedNode[0] == model.attributes["parent"]) {
                            alreadyExist = true;
                        }
                    });

                    if (alreadyExist) {
                        inRiverUtil.NotifyError("Already exists", "The name already exist.<br/><br/>Please select another name or another node.");
                    } else {
                        self.querymodel = new queryModel();
                        self.querymodel.attributes["Query"] = self.getQuery();
                        self.querymodel.attributes["Name"] = name;
                        self.querymodel.attributes["Text"] = name;

                        self.querymodel.attributes["ParentId"] = selectedNode[0];
                        self.querymodel.attributes["Username"] = window.appHelper.userName;

                        self.querymodel.save(null, {
                            success: function (model) {

                                self.unbind();
                                var jsonData = model.toJSON();

                                var folderType = "p";
                                if (jsonData.Username == null || jsonData.sername == "") {
                                    folderType = "s";
                                }
                                self.closepopup();

                                if (self.searchOrigin == "top") {
                                    self.goTo("workarea?title=" + jsonData.Text + "&workareaId=" + folderType + "_" + jsonData.Id + "&isQuery=true");
                                } else {
                                    self.goTo("workareasearch?title=" + jsonData.Text + "&workareaId=" + folderType + "_" + jsonData.Id + "&isQuery=true");
                                }

                                self.remove();

                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }

                        });
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        doUpdateQuery: function () {
            var self = this;

            this.querymodel.attributes["Query"] = this.getQuery();

            this.querymodel.save(null, {
                success: function (model) {

                    self.unbind();
                    var jsonData = model.toJSON();

                    var folderType = "p";
                    if (jsonData.Username == null || jsonData.Username == "") {
                        folderType = "s";
                    }
                    self.closepopup();

                    var workarea = window.appHelper.workAreaHandler.mainWorkAreaView;
                    var secWorkarea = window.appHelper.workAreaHandler.secondWorkAreaView;

                    if (workarea && workarea.workareaId == folderType + "_" + jsonData.Id) {
                        workarea.openSavedQuery({ title: jsonData.Text, workareaId: folderType + "_" + jsonData.Id });
                    } else if (secWorkarea && secWorkarea.workareaId == folderType + "_" + jsonData.Id) {
                        secWorkarea.openSavedQuery({ title: jsonData.Text, workareaId: folderType + "_" + jsonData.Id });
                    } else {
                        window.appHelper.workAreaHandler.getActiveWorkarea().openSavedQuery({ title: jsonData.Text, workareaId: folderType + "_" + jsonData.Id });
                    }


                    
                    //self.goTo("workarea?title=" + jsonData.Text + "&workareaId=" + folderType + "_" + jsonData.Id + "&isQuery=true", true);
                    self.remove();

                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }

            });
        },
        getQuery: function () {
            var self = this;
            var query = [];

            _.each(this.rows, function (row) {
                row.deselect(); 

                var direction = "";
                var sourceEntityTypeId = "";
                var targetEntityTypeId = "";

                if (row.conditionType == "Relation") {

                    if (row.querySystem.indexOf("i") == 0) {
                        direction = "InBound";
                        sourceEntityTypeId = row.entityType;
                        targetEntityTypeId = row.relationEntityType;

                    } else {
                        direction = "OutBound";
                        sourceEntityTypeId = row.relationEntityType;
                        targetEntityTypeId = row.entityType;

                    }
                    row.querySystem = row.querySystem.substring(1);
                }

                if (row.conditionType == "SpecificationData") {
                    if (row.querySystem.substring(0, 1) == "s") {
                        row.querySystem = row.querySystem.substring(1);
                    }
                }

                var dataType = "";
                var datetimeIntervals = false;

                if (row.isDateTimeValue) {
                    datetimeIntervals = row.datetimeIntervals;
                    dataType = "DateTime";
                }

                if (datetimeIntervals) {
                    row.queryValue = row.datetimeIntervalsValue;
                }

                query.push({
                    type: row.conditionType,
                    id: row.querySystem,
                    operator: row.queryOperator,
                    value: row.queryValue,
                    completenessDefinition: row.completenessDefinition,
                    completenessGroup: row.completenessGroup,
                    completenessRule: row.completenessRule,
                    language: row.queryLanguage,
                    relationdirection: direction,
                    relationtargetentitytypeid: sourceEntityTypeId,
                    relationsourceentitytypeid: targetEntityTypeId,
                    datatype: dataType,
                    datetimeInterval: datetimeIntervals
                });
            });

            return query;
        },
        validateForm: function () {
            var valid = true;

            _.each(this.rows, function (row) {
                row.deselect();
                var rowValid = row.validate();
                valid = valid && rowValid;
            });
            return valid; 
        },
        closepopup: function () {
            $.each(this.rows, function (index, row) {
                row.remove();
                row.unbind();
            });
            window.appHelper.event_bus.trigger('closepopup');
        },
        addCondition: function () {
            var row = new queryRowView({ rowId: this.rowId, parent: this, subview: false, type: "System", predefinedEntityType: this.predefinedEntityType });
            if (row.querySystem == undefined) {
                return;
            }

            this.rows.push(row);
            this.$el.find("#query-view").append(row.el);
            this.rowId++;
        },
        addSubCondition: function (parent) {
            var row = new queryRowView({ mainRowId: parent.rowId, rowId: this.rowId, parent: this, entityType: parent.entityType, entityId: parent.entityId, type: parent.type, subview: true });
            this.rows.push(row);
            this.$el.find("#query-view #" + parent.rowId).append(row.el);
            this.rowId++;
        },
        removeCondition: function (e) {
            if ($(e.currentTarget).closest(".query-row .sub-row").length == 0) {
                if (this.$el.find("#contentwrapper").find(".query-row:not(.sub-row)").length < 2)
                    return;
            }

            var rowId = $(e.currentTarget).closest(".query-row")[0].id;
            var rowToDelete = _.find(this.rows, function (row) {
                return row.rowId == rowId;
            });

            if (rowToDelete.conditionType == "Relation") {
                this.$el.find("#query-plus-row #Relation").removeClass("hidden");
            }

            // If main condition, remove childs
            if (rowToDelete.mainrowId == undefined)
            {
                var rows = this.rows;
                var subRowToDelete = undefined;
                do
                {
                    var subRowToDelete = _.find(rows, function (row) {
                        return row.mainrowId == rowToDelete.rowId;
                    });

                    if (subRowToDelete != undefined) {
                        subRowToDelete.onClose(e);

                        var pos = this.rows.indexOf(subRowToDelete);
                        this.rows.splice(pos, 1);
                    }
                }
                while (subRowToDelete != undefined)
            }

            rowToDelete.onClose(e);

            var pos = this.rows.indexOf(rowToDelete);
            this.rows.splice(pos, 1);
        },
        renderSaveTree: function (workareas) {
            var self = this;

            if (this.$el.find("#work-area-selection").jstree(true) != false) {
                this.$el.find("#work-area-selection").jstree(true).destroy(); 
            }
           
            if (workareas) {
                self.workareas = workareas;
            }

            this.$el.find("#work-area-selection").html("");
            this.$el.find("#work-area-selection").jstree({
                core: {
                    data: self.workareas.toJSON(),
                    animation: "100",
                    themes: { dots: false }
                },
                "plugins": [
                       "types", "wholerow"
                ],
                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 4,
                        "valid_children": ["personalroot", "sharedroot"]
                    },
                    "personalroot": {
                        "icon": "/Images/folderdark.png",
                        "valid_children": ["default", "query"]
                    },
                    "sharedroot": {
                        "icon": "/Images/folderdark.png",
                        "valid_children": ["default", "query"]
                    },
                    "default": {
                        "icon": "/Images/folderlight.png",
                        "valid_children": ["default", , "query", "file"]
                    },
                    "query": {
                        "icon": "/Images/folderquery.png",
                        "valid_children": ["default", , "query", "file"]
                    },
                    "file": {
                        "valid_children": []
                    }
                },
            });
        },
        render: function () {
            var that = this;
            var name = "";
            if (this.isSavedQuery) {
                name = this.querymodel.attributes["Text"]; 
            }
            var notificationname = null;
            var notificationparameter = null;
            var notificationcondition = null;
            var notifyby = null;
            var notificationstatus = null; 
            if (this.notification != null) {
                notificationname = this.notification.attributes.name; 
                notificationparameter = this.notification.attributes.parameter;
                notifyby = this.notification.attributes.notificationtype;
                notificationcondition = this.notification.attributes.condition;
                notificationstatus = this.notification.attributes.status; 
            }

            this.$el.html(_.template(queryEditorTemplate, {
                name: name,
                isSavedQuery: this.isSavedQuery,
                notificationname: notificationname,
                notificationparameter: notificationparameter,
                notifyby: notifyby,
                notificationcondition: notificationcondition,
                notificationstatus: notificationstatus
            }));

            if (this.query == null || this.query == "") {
                this.addCondition();
            } else {

                var lastSystemRow = 0;
                var lastRelationRow = 0;
                var lastSpecificationsRow = 0;
                var entityTypeId = "";
                var relationType = "";
                var entityId = "";

                _.each(this.query, function (inRow) {
                    var row;
                    if (inRow.type == "System") {
                        row = new queryRowView({
                            rowId: that.rowId,
                            parent: that,
                            subview: false,
                            type: "System",
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value
                        });
                        that.rows.push(row);

                        row.querySystem = inRow.id;

                        lastSystemRow = that.rowId;

                        if (row.querySystem == "EntityType") {
                            entityTypeId = row.queryValue;

                            $.ajax({
                                url: "/api/tools/hasSpecificationLink/" + entityTypeId,
                                async: false,
                                dataType: 'json',
                                success: function (specificationExists) {
                                    if (specificationExists) {
                                        $(row.el).find("#Specifications").removeClass("hidden");
                                    } else {
                                        if (!$(row.el).find("#Specifications").hasClass("hidden")) {
                                            $(row.el).find("#Specifications").addClass("hidden");
                                        }
                                    }
                                }
                            });
                        }

                        that.$el.find("#query-view").append(row.el);
                        that.rowId++;
                    } else if (inRow.type == "Data" && entityTypeId != null) {
                        row = new queryRowView({
                            mainRowId: lastSystemRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: entityTypeId,
                            type: "Data",
                            subview: true,
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value,
                            queryLanguage: inRow.language,                            
                            queryCriteria: inRow.queryCriteria,
                            datetimeInterval: inRow.datetimeInterval
                        });
                        that.rows.push(row);
                       
                        that.$el.find("#query-view #" + lastSystemRow).append(row.el);

                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Completeness").addClass("hidden");

                        that.rowId++;

                    } else if (inRow.type == "Completeness" && entityTypeId != null) {
                        row = new queryRowView({
                            mainRowId: lastSystemRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: entityTypeId,
                            type: "Completeness",
                            subview: true,
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value,
                            completenessDefinition: inRow.completenessDefinition,
                            completenessGroup: inRow.completenessGroup,
                            completenessRule: inRow.completenessRule
                        });
                        that.rows.push(row);
                        that.$el.find("#query-view #" + lastSystemRow).append(row.el);

                        that.$el.find("#query-view #" + that.rowId).find("#Data").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Completeness").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");

                        that.rowId++;
                    } else if (inRow.type == "Relation") {

                        var querySystem = "";
                        if (inRow.relationdirection == "InBound") {
                            querySystem = "i";
                        } else {
                            querySystem = "o";
                        }
                        querySystem += inRow.id;
                        row = new queryRowView({
                            mainRowId: lastSystemRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: entityTypeId,
                            type: "Relation",
                            subview: true,
                            querySystem: querySystem,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value,
                        });
                        that.rows.push(row);

                        // 8059: When adding a 'relation' the parent-row should not be able to add anymore 'Relations'. One relation per System-row.
                        that.$el.find("#query-view #" + lastSystemRow).find("#Relation").addClass("hidden");
                      
                        that.$el.find("#query-view #" + lastSystemRow).append(row.el);

                        // 8059: For a relation, hide 
                        that.$el.find("#query-view #" + that.rowId).find("#Data").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Completeness").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");
                        
                        lastRelationRow = that.rowId;

                        if (inRow.relationdirection == "InBound") {
                            relationType = inRow.relationsourceentitytypeid;
                        } else {
                            relationType = inRow.relationtargetentitytypeid;
                        }

                        that.rowId++;

                    } else if (inRow.type == "RelationData") {
                        row = new queryRowView({
                            mainRowId: lastRelationRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: relationType,
                            type: "RelationData",
                            subview: true,
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value,
                            queryLanguage: inRow.language,
                            datetimeInterval: inRow.datetimeInterval
                        });
                        that.rows.push(row);

                        that.$el.find("#query-view #" + lastRelationRow).append(row.el);

                        // 8059: For a sub-condition, it should only be possible to add new Data-row as sub-row.
                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");

                        that.rowId++;
                    } else if (inRow.type === "Specifications") {
                        row = new queryRowView({
                            mainRowId: lastSystemRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: "Specification",
                            type: "Specifications",
                            subview: true,
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value
                        });
                        entityId = inRow.id;
                        that.rows.push(row);
                        that.$el.find("#query-view #" + lastSystemRow).append(row.el);

                        // 8059: Hide all standard icons for a Specification-row
                        that.$el.find("#query-view #" + that.rowId).find("#Data").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Completeness").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");

                        lastSpecificationsRow = that.rowId;
                        that.rowId++;
                    } else if (inRow.type === "SpecificationData") {
                        row = new queryRowView({
                            mainRowId: lastSpecificationsRow,
                            rowId: that.rowId,
                            parent: that,
                            entityType: "Specification",
                            type: "SpecificationData",
                            subview: true,
                            entityId: entityId,
                            querySystem: inRow.id,
                            queryOperator: inRow.operator,
                            queryValue: inRow.value,
                            queryLanguage: inRow.language
                        });
                        that.rows.push(row);

                        // 8059: For a SpecificationData-row or a RelationData-row, it should only be possible to add new Data-row as sub-row.
                        that.$el.find("#query-view #" + that.rowId).find("#Relation").addClass("hidden");
                        that.$el.find("#query-view #" + that.rowId).find("#Specification").addClass("hidden");

                        that.$el.find("#query-view #" + lastSpecificationsRow).append(row.el);
                        that.rowId++;
                    }
                });

            
            }
            if (this.notification) {
                this.doShowNotifyWhen();
                if (this.notification.get("condition") === "NotEmpty") {
                    this.$el.find("#notificationparameter").datetimepicker({
                        value: this.notification.get("parameter"),
                        format: appHelper.momentDateTimeFormat,
                        formatTime: appHelper.momentTimeFormat,
                        formatDate: appHelper.momentDateFormat,
                        lang: 'en',
                        weeks: appHelper.showWeeksInDatePicker,
                        dayOfWeekStart: appHelper.firstDayOfWeek
                    });
                }
            }

            return this;
        }
    });

    return queryEditorView;
});