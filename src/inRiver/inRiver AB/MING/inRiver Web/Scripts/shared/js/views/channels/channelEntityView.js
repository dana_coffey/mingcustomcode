define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!sharedtemplates/channels/channelEntityTemplate.html',
  'sharedjs/misc/permissionUtil',
], function ($, _, backbone, modalPopup, contextMenuView, inRiverUtil, entityDetailSettingView, entityDetailSettingWrapperView, channelEntityTemplate, permissionUtil) {

    var entitycardView = backbone.View.extend({
        
        initialize: function (options) {
        },
        events: {
            "click #menu-edit-button": "onEditClick",
            "click #menu-open-in-enrich-button": "onEnrichOpenClick",
            "click #menu-publish-button": "onPublishClick",
            "click #menu-unpublish-button": "onUnpublishClick",
            "click #star-button": "onEntityStar",
            "click #menu-delete-button": "onDeleteClick",
            "click .channel-entity-card-wrap": "onCardClick",
            "click #menu-open-in-planrelease-button": "onPlanReleaseOpenClick"
        },
        onEditClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onPublishClick: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").hide();
                    $("#unpublish-button").show();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                    self.entityUpdated(self.model.id);
                }
            };

            xmlhttp.open("GET", "/api/channelaction/publish/" + this.model.id, true);
            xmlhttp.send();
        },
        onUnpublishClick: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#publish-button").show();
                    $("#unpublish-button").hide();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                    self.entityUpdated(self.model.id);
                }
            };

            xmlhttp.open("GET", "/api/channelaction/unpublish/" + this.model.id, true);
            xmlhttp.send();
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onDeleteClick: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            inRiverUtil.deleteEntity(this);
        },
        onEntityStar: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                    self.entityUpdated(self.model.id);
                }
            }

            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");
            }
            window.appHelper.event_bus.trigger('channelstarred');

        },
        onEnrichOpenClick: function (e) {
            e.stopPropagation();
            window.location.href = "/app/enrich/index#entity/" + this.model.id;
        },
        onPlanReleaseOpenClick: function (e) {
            //e.stopPropagation();
            window.location.href = "/app/planrelease/index#channel/" + this.model.id;
        },
        onCardClick: function (e) {
            e.stopPropagation();

            if (window.appHelper.currentApp == "Enrich") {
                this.onEnrichOpenClick();
            } else {
                this.onPlanReleaseOpenClick();
            }
        },
        onClose: function () {
        },
        entityUpdated: function (id) {

            if (id == this.model.id) {

                var self = this;
                this.model.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        entityDeleted: function (id) {
            if (id == this.model.id) {
                this.stopListening(window.appHelper.event_bus);
                this.remove();
                this.unbind();
            }
        },
        render: function () {
            if (this.model == null) {
                return this;
            }

            var isPublished = this.model.attributes["ChannelPublished"];

            var icon = this.model.attributes.EntityType;
            if (icon == "Channel" && !isPublished) {
                icon = "ChannelUnpublished";
            }

            this.publishedField = isPublished;
            this.$el.html(_.template(channelEntityTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName, published: this.publishedField, icon: icon }));

            //add context menu
            this.tools = new Array();

            this.tools.push({ name: "menu-edit-button", title: "Edit channel information", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });

            if (this.model.attributes.Starred == "1") {
                this.tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });

            } else {
                this.tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }

            if (window.appHelper.currentApp != "Enrich") {

                //Permissions:
                var callback = function (access, id, self) {
                    if (access) {
                        self.tools.push({ name: "menu-open-in-enrich-button", title: "Open details in the Enrich app", icon: "fa fa-list", text: "Open in Enrich" });
                    }
                };

                permissionUtil.GetUserPermission("inRiverEnrich", "#Enrich", callback, this); 
            }

            if (window.appHelper.currentApp == "Enrich") {

                var callback = function (access, id, self) {
                    if (access) {
                        self.tools.push({ name: "menu-open-in-planrelease-button", title: "Open details in the Plan & Release app", icon: "fa fa-list", text: "Open in Plan & Release" });
                    }
                };

                permissionUtil.GetUserPermission("inRiverPlanAndRelease", "#PlanAndRelease", callback, this);
            }

            this.tools.push({ name: "menu-delete-button", title: "Delete channel", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });

            if (window.appHelper.currentApp != "Enrich") {
                if (this.publishedField) {
                    this.tools.push({ name: "menu-unpublish-button", title: "Unpublish channel", icon: "fa fa-toggle-on", text: "Unpublish", permission: "PublishChannel" });
                } else {
                    this.tools.push({ name: "menu-publish-button", title: "Publish channel", icon: "fa fa-toggle-off", text: "Publish", permission: "PublishChannel" });
                }
            }

            this.$el.find("#contextmenu").html(new contextMenuView({ id: this.model.id, tools: this.tools }).$el);

            this.listenTo(window.appHelper.event_bus, 'entityupdated', this.entityUpdated);
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.entityDeleted);

            return this; // enable chained calls
        }
    });

    return entitycardView;
});
