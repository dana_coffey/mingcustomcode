﻿define([
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil'
], function (_, Backbone, inRiverUtil) {

    var FieldTypeSettingModel = Backbone.Model.extend({
        idAttribute: "id",
        urlRoot: '/api/FieldTypeSetting',
        loadedSettings: false,
        resourceFilenameRegExp: null,
        initialize: function (fieldTypeId) {
            this.url =  this.urlRoot + '/' + fieldTypeId;
        },
        ValidateRegExp: function (fileName) {
            var that = this;

            if (!this.loadedSettings) {
                if (this.get("Id") != null) {
                    if (this.get("Settings") != null) {
                        $.each(this.get("Settings"), function (setting, value) {
                            if (setting == "RegExp") {
                                that.resourceFilenameRegExp = value;
                                return true;
                            }
                        });
                    }
                }

                this.loadedSettings = true;
            }

            if (this.resourceFilenameRegExp != null) {
                if (fileName.search(this.resourceFilenameRegExp)) {
                    return false;
                }
            }

            return true;
        }
    });

    return FieldTypeSettingModel;
});