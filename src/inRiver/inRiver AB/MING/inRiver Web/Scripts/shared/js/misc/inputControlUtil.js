﻿/// <reference path="../views/entityview/entityDetailSettingView.js" />
define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/views/inputcontrols/noneEditModeControlView',
  'sharedjs/views/inputcontrols/stringControlView',
  'sharedjs/views/inputcontrols/integerControlView',
  'sharedjs/views/inputcontrols/doubleControlView',
  'sharedjs/views/inputcontrols/booleanControlView',
  'sharedjs/views/inputcontrols/localeStringControlView',
  'sharedjs/views/inputcontrols/blobTextControlView',
  'sharedjs/views/inputcontrols/cvlControlView',
  'sharedjs/views/inputcontrols/datetimeControlView'

], function ($, _, backbone, noneEditModeControlView, stringControlView, integerControlView, doubleControlView, booleanControlView,
    localeStringControlView, blobTextControlView, cvlControlView, datetimeControlView) {

    var inputControlUtil = {
        createEditControl: function (field, dataType, fieldTypeId, previousFieldTypeId) {
            var value;
            var regexp = null;
            if (field.Settings && field.Settings.RegExp) {
                regexp = field.Settings.RegExp;
            }

            switch (dataType) {
                case "String":
                    if (field.Value != null) {
                        value = field.Value;
                    }
                  


                    this.stringControl = new stringControlView({ Id: fieldTypeId, Value: value, PreviousId: previousFieldTypeId, EntityId: field.EntityId, RegExp: regexp});

                    return this.stringControl;
                case "Integer":
                case "File":
                    value = "";
                    if (field.Value != null) {
                        value = field.Value;
                    }

                    this.integerControl = new integerControlView({ Id: fieldTypeId, Value: value, PreviousId: previousFieldTypeId, EntityId: field.EntityId });

                    return this.integerControl;
                case "Double":
                    value = "";
                    if (field.Value != null) {
                        value = field.Value;
                    }

                    this.doubleControl = new doubleControlView({
                        Id: fieldTypeId,
                        Value: value,
                        PreviousId: previousFieldTypeId, EntityId: field.EntityId
                    });

                    return this.doubleControl;
                case "DateTime":
                    value = "";
                    if (field.Value != null) {
                        value = field.Value;
                    }

                    this.dateTimeControl = new datetimeControlView({
                        Id: fieldTypeId,
                        Value: value,
                        PreviousId: previousFieldTypeId, EntityId: field.EntityId
                    });

                    return this.dateTimeControl;
                case "Boolean":
                    value = null;
                    if (field.Value != null && field.Value != "") {
                        value = field.Value;
                        if (value.toString() == "") {
                            value = false; 
                        }

                        var isBoolean = _.isBoolean(value);
                        if (!isBoolean)
                        {
                            if (value.toLowerCase() != "") {
                                value = JSON.parse(value.toLowerCase());
                            }
                        }
                    }

                    this.booleanControl = new booleanControlView({ Id: fieldTypeId, Value: value, PreviousId: previousFieldTypeId, EntityId: field.EntityId });

                    return this.booleanControl;
                case "CVL":
                    value = "";
                    if (field.Value != null) {
                        value = field.Value;
                    }

                    //if (this.cvlControl == null) {
                    this.cvlControl = new cvlControlView({ Id: fieldTypeId, Value: value, PreviousId: previousFieldTypeId, MultiValue: field.IsMultiValue, PossibleValues: field.PossibleValues, EntityId: field.EntityId });
                    //}

                    return this.cvlControl;
                case "LocaleString":
                    value = {}
                    if (field.Value != null) {
                        value = field.Value;
                    } else {
                        /*_.each(field.SystemLanguages, function (language) {
                            value[language] = "";
                        });*/
                    }

                    this.localeStringControl = new localeStringControlView({
                        Id: fieldTypeId,
                        Value: value,
                        PreviousId: previousFieldTypeId, EntityId: field.EntityId
                    });

                    return this.localeStringControl;
                case "Xml":
                    value = "";
                    if (field.Value != null) {
                        value = field.Value;
                    }

                    this.blobTextControl = new blobTextControlView({ Id: fieldTypeId, Value: value, PreviousId: previousFieldTypeId, EntityId: field.EntityId });
                    return this.blobTextControl;
            }
        }
    };
    return inputControlUtil;

});