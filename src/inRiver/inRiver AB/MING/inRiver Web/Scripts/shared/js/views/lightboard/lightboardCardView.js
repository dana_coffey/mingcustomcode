﻿define([
    'jquery',
    'underscore',
    'backbone',
    'dropzone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/views/contextmenu/contextMenuView',
    'text!sharedtemplates/lightboard/lightboardCardTemplate.html',
    'text!sharedtemplates/resource/resourceDownloadPopup.html'
], function ($, _, Backbone, Dropzone, inRiverUtil, contextMenuView, lightboardCardTemplate, resourceDownloadPopup) {

    var lightboardCardView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.model = options.model;
            this.parentEntityId = options.parentId;
            this.parententitytype = options.parententitytype;

            this.render();

        },
        events: {
            "click #view-details": "onDetailsClick",
            "click #set-main-picture": "setMainPicture",
            "click #download-button": "onDownload",
            "click #edit-resource-in-tool": "onEditinAssApp",
            "click #delete-resource": "onResourceRemove",
            "click #upload-button": "onUpload",
        },
        onEditinAssApp: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var link = document.createElement('a');
            link.href = "../picture/download?id=" + this.model.attributes.ResourceFileId + "&size=Original";
            link.download = "../picture/download?id=" + this.model.attributes.ResourceFileId + "&size=Original";
            document.body.appendChild(link);
            link.click();
        },
        onUpload: function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("#my-dropzone #resourceId").val(this.model.id);
            $("#my-dropzone #type").val("ReplaceResource");

            $("#my-dropzone").trigger("click");

        },

        setMainPicture: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var that = this;

            $.get("/api/tools/setmainpicture/" + this.parentEntityId + "/" + this.model.id, function (result) {
                appHelper.event_bus.trigger('entityupdated', that.parentEntityId);
                inRiverUtil.Notify("Main picture has been updated");
            });

            //this.getCollection();
        },
        onDownload: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var self = this;


            var downloadElement = _.template(resourceDownloadPopup, { resource: self.model.toJSON() });
            //self.$el.append(downloadElement);

            if (this.$el.find(".download-menu").length === 0) {
                self.$el.append(downloadElement);
            } else {
                this.$el.find(".download-menu")[0] = downloadElement;
            }


            this.$el.find(".download-menu").menu();
            //$(e.currentTarget).parent().parent().next().menu();

            var menu = this.$el.find(".download-menu").show().position({
                my: "left top",
                at: "left bottom",
                of: e.currentTarget
            });

            menu.mouseleave(function () {
                menu.hide();
            });

            $(document).one("click", function () {
                menu.hide();
            });
            
          

        },
        onResourceRemoveConfirmed: function (self) {
            self.model.url = "/api/resource/" + self.model.id;
            self.model.destroy();
            appHelper.event_bus.trigger('entityupdated', self.parentEntityId);
            appHelper.event_bus.trigger('popupclose');

            inRiverUtil.Notify("Resource successfully deleted");
        },
        onResourceRemove: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var self = this;
            inRiverUtil.NotifyConfirm("Confirm delete resource", "Are you sure you want to delete this resource?", this.onResourceRemoveConfirmed, self);
        },
        onDetailsClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.goTo("entity/" + this.model.id + "/overview");
            appHelper.event_bus.trigger('popupclose');
        },
        render: function () {
            this.$el.html(_.template(lightboardCardTemplate, { resource: this.model.toJSON() }));

            // add context menu
            var tools = new Array();
            tools.push({ name: "view-details", title: "View Entity Overview", icon: "fa fa-list", text: "Overview" });

            if (this.parententitytype != "Resource") {
                tools.push({ name: "delete-resource", title: "Delete Entity", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });
            }

            tools.push({ name: "edit-resource-in-tool", title: "open in associated application", icon: "fa-external-link fa", text: "Open in application" });
            tools.push({ name: "set-main-picture", title: "Set as entity main picture", icon: "fa-picture-o fa ", text: "Set main picture", permission: "UpdateEntity" });

            if (this.model.attributes.ResourceFileId != null) {
                tools.push({ name: "upload-button", title: "Replace file", icon: "fa-upload fa", text: "Replace file", permission: "AddFile" });
            } else {
                tools.push({ name: "upload-button", title: "Upload file", icon: "fa-upload fa", text: "Upload file", permission: "AddFile" });
            }
            tools.push({ name: "download-button", title: "Download resource file", icon: "fa-download fa ", text: "Download" });

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.model.id,
                tools: tools
            }).$el);

            return this; // enable chained calls
        }
    });

    return lightboardCardView;
});
