﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'text!sharedtemplates/inputcontrols/blobTextControlTemplate.html'
], function ($, _, backbone, alertify, blobTextControlTemplate) {

    var blobTextControlView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;
            if (options.Value == null) {
                this.fieldValue = "";
                this.defaultValue = "";
            } else {
                this.fieldValue = options.Value;
                this.defaultValue = _.clone(options.Value);
            }
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.focusField = this.inputField;

            this.entityId = options.EntityId; 

            this.okToSave = false;
            this.saveButtonPressed = false;
        },
        events: function () {
            var theEvents = {};
            theEvents["blur " + this.inputField] = "onLostFocus";
            theEvents["keyup " + this.inputField] = "onKeyUp";
            theEvents["click " + this.saveButton] = "onSave";
            theEvents["click " + this.undoButton] = "onUndo";
            theEvents["keydown " + this.inputField] = "onKeyDown";
            return theEvents;
        },
        onKeyDown: function (e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo();
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            if (e.keyCode == 13) {
                this.onSave(e);
            }
        },
        onUndo: function () {
            if (this.saveButtonPressed) {
                return;
            }

            this.$el.find(this.inputField).val(this.defaultValue);
            this.fieldValue = this.defaultValue;
            this.$el.find(this.saveButton).removeClass('active');
            this.$el.find(this.undoButton).removeClass('active');

            this.okToSave = false;
            this.$el.remove();
            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onSave: function (e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);
            }
        },
        onKeyUp: function (e) {
            e.stopPropagation();
            this.fieldValue = this.$el.find(this.inputField).val();
            if (this.fieldValue != this.defaultValue) {
                this.$el.find(this.saveButton).addClass('active');
                this.$el.find(this.undoButton).addClass('active');

                this.okToSave = true;
            } else {
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');

                this.okToSave = false;
            }
        },
        onLostFocus: function (e) {
            var that = this;
            e.stopPropagation();

            // Use timeout to delay examination of historypopup until after blur/focus 
            // events have been processed.
            setTimeout(function () {
                if (!window.appSession.historyset) {
                    if (that.defaultValue == that.fieldValue) {
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                    }
                }
            }, 80);
        },
        render: function () {
            var template = _.template(blobTextControlTemplate, { Id: this.fieldTypeId, Value: this.fieldValue });
            this.setElement(template, true);
            return this;
        },
    });

    return blobTextControlView;
});

