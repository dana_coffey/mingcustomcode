﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/languages/LanguagesCollection',
  'text!sharedtemplates/inputcontrols/localeStringControlTemplate.html'
], function ($, _, backbone, alertify,inRiverUtil, languagesCollection, localeStringControlTemplate) {

    var localeStringControlView = backbone.View.extend({
        initialize: function(options) {
            this.fieldTypeId = options.Id;
            this.prevFieldId = options.PreviousId;

            this.selectedLanguage = window.appHelper["userLanguage"];
            if (options.Value == null) {
                this.fieldValue = "";
                this.defaultValue = "";
            } else {
                this.fieldValue = options.Value;
                this.defaultValue = jQuery.extend(true, {}, options.Value);
            }
            this.languageField = "#value-editing-lang-" + this.fieldTypeId;
            this.inputField = "#value-editing-" + this.fieldTypeId;
            this.saveButton = "#save-field-value-" + this.fieldTypeId;
            this.undoButton = "#undo-field-value-" + this.fieldTypeId;
            this.focusField = this.inputField;
            this.entityId = options.EntityId;
            this.okToSave = false;
            this.saveButtonPressed = false;
        },
        events: function() {
            var theEvents = {};
            theEvents["keydown " + this.languageField] = "onKeyDownLanguage";
            theEvents["keydown " + this.inputField] = "onKeyDownText";
            theEvents["blur " + this.languageField] = "onBlur";
            theEvents["blur " + this.inputField] = "onBlur";
            theEvents["change " + this.languageField] = "onLanguageSelectedChange";
            theEvents["keyup " + this.inputField] = "onKeyUpText";
            theEvents["click " + this.saveButton] = "onSave";
            theEvents["click " + this.undoButton] = "onUndo";
          
            return theEvents;
        },
       onKeyDownText: function(e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo();
            }

            if (e.shiftKey && e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldbacktabpressed', this.prevFieldId);
                return;
            }

            //if (e.keyCode == 13) {
            //    this.onSave(e);
            //}
        },
        onKeyDownLanguage: function(e) {
            e.stopPropagation();

            if (e.keyCode == 27) {
                this.onUndo(e);
            }

            if (e.shiftKey && e.keyCode == 9) {
                return;
            }

            if (e.keyCode == 9) {
                window.appHelper.event_bus.trigger('fieldtabpressed', this.fieldTypeId);
                return;
            }

            //if (e.keyCode == 13) {
            //    this.onSave(e);
            //}
        },
        onUndo: function(e) {
            if (this.saveButtonPressed) {
                return;
            }

            var that = this;
            _.each(this.fieldValue, function(value, key) {
                value[1] = that.defaultValue[key][1];
            });


            this.onLanguageSelectedChange(e);
            this.$el.find(this.inputField).val(this.fieldValue);
            this.$el.find(this.saveButton).removeClass('active');
            this.$el.find(this.undoButton).removeClass('active');
            this.okToSave = false;
            this.$el.remove();

            window.appHelper.event_bus.trigger('fieldvaluenotupdated', this.fieldTypeId, this.fieldValue);
        },
        onSave: function(e) {
            e.stopPropagation();
            if (this.okToSave) {
                this.saveButtonPressed = true;
                window.appHelper.event_bus.trigger('fieldvaluesave', this.fieldTypeId, this.fieldValue, this.entityId);

                this.defaultValue = jQuery.extend(true, {}, this.fieldValue);
            }
        },
        onLanguageSelectedChange: function(e) {
            if (e != null) {
                e.stopPropagation();
            }

            this.selectedLanguage = this.$el.find(this.languageField).val();
            var inputValue;

            if (this.fieldValue.stringMap) {
                inputValue = this.fieldValue.stringMap[this.selectedLanguage];
            } else {
                inputValue = this.fieldValue[this.selectedLanguage][1];
            }

            this.$el.find(this.inputField).val(inputValue);
        },
        onKeyUpText: function(e) {
            e.stopPropagation();

            var languageValue = this.selectedLanguage;

            if (!jQuery.isEmptyObject(this.fieldValue)) {
                if (this.fieldValue.stringMap) {
                    languageValue = this.selectedLanguage;
                } else {
                    languageValue = this.selectedLanguage;
                }
            }

            this.fieldValue[languageValue][1] = this.$el.find(this.inputField).val();
            if (!this.isSameLocaleString(this.fieldValue, this.defaultValue)) {
                this.$el.find(this.saveButton).addClass('active');
                this.$el.find(this.undoButton).addClass('active');

                this.okToSave = true;
            } else {
                this.$el.find(this.saveButton).removeClass('active');
                this.$el.find(this.undoButton).removeClass('active');

                this.okToSave = false;
            }
        },
        onLeftByTab: function(id) {
            if (id == this.fieldTypeId) {
                this.onLostFocus();
            }
        },
        onLeftByBackTab: function(id) {
            if (id == this.prevFieldId) {
                this.onLostFocus();
            }
        },
        onBlur: function(e) {
            // Chrome needs to use this function. 
            var x = $(document.activeElement);
            if (x.length == 0) {
                return;
            }
            var id = x[0].id;


            //If id empty the body elemetnt got the focus while jumping to the language dropdown. there fore it should be like this! Ralf
            if (id != "" && id != this.inputField.replace("#", "") && id != this.languageField.replace("#", "")) {
                this.onLostFocus();
            }
        },
        onLostFocus: function() {
            var that = this;
            //if (this.$el.find(this.languageField).is(':focus') || this.$el.find(this.inputField).is(':focus')) {
            //    // still in the control.
            //    return;
            //}

            //Use timeout to delay examination of historypopup until after blur/focus 
            //events have been processed.
            setTimeout(function() {
                if (!window.appSession.historyset) {
                    if (that.isSameLocaleString(that.fieldValue, that.defaultValue)) {
                        that.$el.remove();
                        window.appHelper.event_bus.trigger('fieldvaluenotupdated', that.fieldTypeId, that.fieldValue);
                    } else {
                        window.appHelper.event_bus.trigger('fieldvalueupdated', that.fieldTypeId, that.fieldValue);
                    }
                }
            }, 80);
        },
        isSameLocaleString: function(first, second) {
            var result = true;
            _.each(first, function(value, key) {
                if ((second[key] == null && value != null) || second[key][1] != value[1]) {
                    result = false;
                }
            });

            return result;
        },
        render: function() {
            var self = this;
            var values = {};
            var inputValue;

            if (!jQuery.isEmptyObject(this.fieldValue)) {
                if (this.fieldValue.stringMap) {
                    inputValue = this.fieldValue.stringMap[this.selectedLanguage];
                } else {
                    inputValue = this.fieldValue[this.selectedLanguage][1];
                }
            }

            var template = _.template(localeStringControlTemplate, { Id: this.fieldTypeId, Value: inputValue });
            this.setElement(template, true);

            self.$el.find(self.languageField).empty();

            var langCollection = new languagesCollection();
            langCollection.fetch({
                success: function(collection) {
                    _.each(collection.toJSON(), function(model) {
                        values[model.Name] = model.DisplayName;
                        if (self.selectedLanguage == model.Name) {
                            self.$el.find(self.languageField).append('<option value="' + model.Name + '" selected>' + model.DisplayName + '</option>');
                        } else {
                            self.$el.find(self.languageField).append('<option value="' + model.Name + '">' + model.DisplayName + '</option>');
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.$el.find(this.inputField).each(function() {
                this.focus();
            });


            this.listenTo(window.appHelper.event_bus, 'fieldtabpressed', this.onLeftByTab);
            this.listenTo(window.appHelper.event_bus, 'fieldbacktabpressed', this.onLeftByBackTab);

            return this;
        },
    });

    return localeStringControlView;
});

