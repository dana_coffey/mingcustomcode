define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/entity/entityTableModel'
], function ($, _, backbone, entityTableModel) {
    var entityTableCollection = backbone.Collection.extend({
        model: entityTableModel,
        url: '/api/entitytable'
    });

    return entityTableCollection;
});
