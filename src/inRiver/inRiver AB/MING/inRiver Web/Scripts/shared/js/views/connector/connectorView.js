﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'outboundjs/misc/connectorUtil',
  'sharedjs/views/panelsplitter/panelBorders',
  'sharedjs/models/connector/connectorModel',
  'sharedjs/models/connector/startConnectorModel',
  'sharedjs/models/connector/stopConnectorModel',
  'sharedjs/models/connector/removeConnectorModel',
  'sharedjs/views/connector/connectorSettingListView',
  'sharedjs/views/connector/connectorMessageView',
  'sharedjs/views/connector/monitorView',
  'sharedjs/views/connector/mappingBasedconnectorSettingView',
  'outboundjs/views/main/mainConfigurationView',
  'text!sharedtemplates/connector/connectorTemplate.html'
], function ($, _, backbone, alertify, modalPopup, inRiverUtil, permissionUtil, connectorUtil, panelBorders, connectorModel, startConnectorModel, stopConnectorModel,
    removeConnectorModel, connectorSettingListView, connectorMessageView, monitorView, mappingBasedconnectorSettingView, mainConfigurationView, connectorTemplate) {

    var connectorView = backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            var that = this;

            var onDataHandler = function () {
                that.render();
            };

            if (this.model) {
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                var id = options.id;
                this.model = new connectorModel(id);
                this.model.url = "/api/connector/?name=" + id;
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        events: {
            "click #delete-button": "onDeleteClick",
            "click #start-button": "onStartClick",
            "click #stop-button": "onStopClick",
            "click #view-events": "onViewEvents",
            "click #tab-monitor": "onChangeTopTab",
            "click #tab-details": "onChangeTopTab",
            "click #tab-config": "onChangeTopTab",
            "click #edit-button": "onEditDetail",
        },
        onViewEvents: function () {
            event.stopPropagation();

            var modal = new modalPopup();

            modal.popOut(new connectorMessageView({
                id: this.model.get("Id"),
            }), { size: "large", header: "Event Messages", usesavebutton: false });

            $("#save-form-button").removeAttr("disabled");
        },
        onDeleteClick: function () {
            var confirmCallback = function (self) {
                var that = self;
                self.removeConnector = new removeConnectorModel({ Id: self.model.get("Id") });
                self.removeConnector.destroy({
                    success: function () {
                        window.appHelper.event_bus.trigger('connectordeleted', that.model.get("Id"));
                        that.remove();
                        that.unbind();
                        that.goTo("home");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            inRiverUtil.NotifyConfirm("Confirm delete","Do you want to remove this connector?", confirmCallback, this);
        },
        onStartClick: function () {
            var self = this;
            this.startConnector = new startConnectorModel({ Id: this.model.get("Id") });
            this.startConnector.fetch({
                success: function (model) {
                    self.startedfetched = true;
                    window.appHelper.event_bus.trigger('connectorupdated', self.model.get("Id"));
                    self.render(model);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onStopClick: function () {
            var self = this;
            this.stopConnector = new stopConnectorModel({ Id: this.model.get("Id") });
            this.stopConnector.fetch({
                success: function (model) {
                    self.startedfetched = true;
                    window.appHelper.event_bus.trigger('connectorupdated', self.model.get("Id"));
                    self.render(model);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        createDetailsTabView: function () {
            var contentSubView;
            if (this.model.get("TypeName") == "inRiver.Connectors.Inbound.Extended.Files") {
                contentSubView = new mappingBasedconnectorSettingView({
                    model: this.model
                });
            } else {
                contentSubView = new connectorSettingListView({
                    model: this.model
                });
            }
            return contentSubView;
        },
        createMonitorTabView: function () {
            var contentSubView = new monitorView({
                model: this.model
            });
            return contentSubView;
        },
        createConfigurationTabView: function () {
            var configurationSubView = new mainConfigurationView({
                model: this.model
            });
            return configurationSubView;
        },
        onChangeTopTab: function (e) {
            if (window.appSession.entityViewTopTabId == e.currentTarget.id) {
                return;
            }

            window.appSession.entityViewTopTabId = e.currentTarget.id; // remember default tab
            var urlTabName = window.appSession.entityViewTopTabId.replace("tab-", "");
            this.goTo("connector/" + this.id + "/" + urlTabName, { trigger: false });
            this.createTopTabView(window.appSession.entityViewTopTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
        },
        createTopTabView: function (idOfTab) {
            if (this._currentTopTabView) {

                if (this._currentTopTabView.timer) {
                    clearInterval(this._currentTopTabView.timer);
                }


                this._currentTopTabView.undelegateEvents();

                this._currentTopTabView.$el.removeData().unbind();

                this._currentTopTabView.remove();
                Backbone.View.prototype.remove.call(this._currentTopTabView);
                this._currentTopTabView.unbind();
              
            }
            if (idOfTab == "tab-details") {
                this._currentTopTabView = this.createDetailsTabView();
            }
            if (idOfTab == "tab-monitor") {
                this._currentTopTabView = this.createMonitorTabView();
            }
            if (idOfTab == "tab-config") {
                this._currentTopTabView = this.createConfigurationTabView();
            }
        },
        indicateActiveTopTab: function () {
            if (this.model.get("Inbound")) {
                this.$el.find("#tab-details").removeClass("control-row-tab-button-active");
                this.$el.find("#tab-monitor").removeClass("control-row-tab-button-active");
                this.$el.find("#tab-details").addClass("control-row-tab-button-active");
                this.$el.find("#tab-monitor").hide();
            } else {
                this.$el.find("#tab-details").removeClass("control-row-tab-button-active");
                this.$el.find("#tab-monitor").removeClass("control-row-tab-button-active");
                this.$el.find("#tab-config").removeClass("control-row-tab-button-active");
                this.$el.find("#" + window.appSession.entityViewTopTabId).addClass("control-row-tab-button-active");
            }

            if (!connectorUtil.isCrossConnect(this.model.get("TypeName"))) {
                this.$el.find("#tab-config").removeClass("control-row-tab-button-active");
                this.$el.find("#tab-config").hide();
            }
        },
        render: function (model) {
            if (model != null && model.attributes.Id != null) {
                this.model = model;
            }
            //console.log("Render Connector View");
            this.$el.html(_.template(connectorTemplate, { data: this.model.toJSON(), userName: window.appHelper.userName }));
            this.createTopTabView(window.appSession.entityViewTopTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTopTab();
            permissionUtil.CheckPermissionForElement("StartStopConnector", this.$el.find("#stop-button"));
            permissionUtil.CheckPermissionForElement("StartStopConnector", this.$el.find("#start-button"));
            permissionUtil.CheckPermissionForElement("AddDeleteConnector", this.$el.find("#delete-button"));


            panelBorders.initAllEvents();
            return this; // enable chained calls
        }
    });

    return connectorView;
});