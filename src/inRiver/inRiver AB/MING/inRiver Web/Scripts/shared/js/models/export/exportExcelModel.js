﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var exportExcelModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/exportexcel'
    });

    return exportExcelModel;

});