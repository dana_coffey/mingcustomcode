﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var validateEntitiesModel = backbone.Model.extend({
        initialize: function (options) {
        },
        urlRoot: '/api/validateuniquefields'
    });

    return validateEntitiesModel;

});