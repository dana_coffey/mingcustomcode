﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'text!sharedtemplates/entity/entityContextSettingsWrapperTemplate.html',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, modalPopup, fieldModel,
    entityDetailSettingView, entityContextSettingsWrapperTemplate,
    entityDetailSettingWrapperView, inRiverUtil) {

    var entityContextSettingsWrapperView = backbone.View.extend({
        tagName: "div",
        initialize: function(options) {
            var that = this;
            this.detailfields = [];
            this.showInWorkareaOnSave = true;

            if (options.showInWorkareaOnSave != null) {
                this.showInWorkareaOnSave = options.showInWorkareaOnSave;
            }

            this.listenTo(appHelper.event_bus, 'entityupdated', this.entityUpdated);

            // This is a bit strange but this view is loaded with different types of models
            // so we have to check if we have the Fields attribute... if not, then load it.
            // This should be cleaned up made and consistent!
            if (!options.isNew && !options.model.get("Fields")) {
                this.model = new fieldModel({ id: options.model.id });
                this.model.fetch({
                    success: function() {
                        that.render();
                    },
                    error: function(model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.model = options.model;
                // If creating a new entity a model will be created by the subview
                that.render();
            }
        },
        events: {
            "click #entity-detail-settings-save-all-button": "onSaveAll",
            "click #entity-detail-settings-undo-all-button": "onUndoAll",
        },
        onSaveAll: function() {
            window.appHelper.event_bus.trigger('entity-details-save');
        },
        onUndoAll: function () {
            if (this.fieldSetDropdownForm) {
                this.fieldSetDropdownForm.setValue({ FieldSet: this.fieldSetDropdownForm.fields.FieldSet.editor.value }); // Undo fieldset dropdown
                this.fieldSetDropdownForm.commit();
            }
            window.appHelper.event_bus.trigger('entity-details-undo');
            this.onIsUnsavedChanged();
        },
        onClose: function() {
            //if (this.entityDetailSettingSubView) {
            //    this.entityDetailSettingSubView.close();
            //}
        },
        hasUnsavedChanges: function() { // called from the router to prevent the user from losing changes
            //var isUnsaved = this.entityDetailSettingSubView.isUnsaved;
            //if (this.fieldSetDropdownForm) {
            //    isUnsaved = isUnsaved || inRiverUtil.createValueForFormFieldComparison(this.fieldSetDropdownForm.fields.FieldSet.editor.value) != inRiverUtil.createValueForFormFieldComparison(this.model.get("FieldSet"));
            //}
            //return isUnsaved;
        },
        onIsUnsavedChanged: function () {
            var isUnsaved = this.hasUnsavedChanges();
            if (!this.options.parentIsPopup) {
                if (isUnsaved) {
                    this.$el.find("#entity-detail-settings-save-all-button").show();
                    this.$el.find("#entity-detail-settings-undo-all-button").show();
                } else {
                    this.$el.find("#entity-detail-settings-save-all-button").hide();
                    this.$el.find("#entity-detail-settings-undo-all-button").hide();
                }
            } else {
                if (isUnsaved) {
                    this.$el.find("#entity-detail-settings-undo-all-button").show();
                } else {
                    this.$el.find("#entity-detail-settings-undo-all-button").hide();
                }
            }
        },
        entityUpdated: function(a,b,c) {
            this.renderIframe();
        },
        renderIframe: function() {
            this.$el.find("iframe").attr("src", "/app/templateprinting?entities=" + this.model.id + "&templateId=10&language=sv");
        },
        onIframeLoaded: function () {
            var that = this;
            var editors = this.$el.find("iframe").contents().find("span[inriver-field-editor]");
            editors.each(function(ix, el) {
                $(el).html("<img style='max-height: 16px; cursor: pointer;' src='/images/context-edit-pencil.png'>");
            });
            editors.on("click", function (e) {
                var fieldName = $(this).attr("inriver-field-editor");
                entityDetailSettingWrapperView.prototype.showAsPopup({
                    model: that.model,
                    fieldTypesFilter: [fieldName],
                    minimalMode: true,
                    showHiddenFields: true,
                });
            });
        },
        render: function () {
            //window.appHelper.i = 0;
            var that = this;
            this.$el.html(_.template(entityContextSettingsWrapperTemplate, { data: this.model.toJSON() }));

            //this.entityDetailSettingSubView = new entityDetailSettingView({ model: this.model, onIsUnsavedChange: this.onIsUnsavedChanged, parentIsPopup: this.options.parentIsPopup, isNew: this.options.isNew, showInWorkareaOnSave: this.showInWorkareaOnSave, fieldTypesFilter: this.options.fieldTypesFilter });
            //this.listenTo(this.entityDetailSettingSubView, "entityDetailsIsUnsavedChange", this.onIsUnsavedChanged);
            //this.$el.find("#details-view-wrap").html(this.entityDetailSettingSubView.el);

            //if (this.model.get("FieldSetsDict")) {
            //    var editorAttrs = {};
            //    if (this.model.get("IsReadOnly")) {
            //        editorAttrs = { readonly: 'readonly', class: 'readonly', disabled: 'disabled' };
            //    }

            //    var schema = {
            //        FieldSet: { title: "Fieldset", type: "Select", options: this.model.get("FieldSetsDict"), editorAttrs: editorAttrs }
            //    };

            //    var formTemplate = "<form id='entity-detail-settings-fieldset-form'>";
            //    formTemplate += "<span>Fieldset</span><div data-editors='FieldSet'></div>";
            //    formTemplate += "</form>";

            //    // Fieldsets dropdown
            //    this.fieldSetDropdownForm = new Backbone.Form({
            //        template: _.template(formTemplate),
            //        model: this.model,
            //        schema: schema
            //    }).render();
            //    this.fieldSetDropdownForm.on('FieldSet:change', function (a, b) {
            //        that.fieldSetDropdownForm.commit();
            //        that.onIsUnsavedChanged();
            //    });
            //    this.$el.find("#entity-detail-settings-fieldset-dropdown-wrap").html(this.fieldSetDropdownForm.el);
            //}

            $('iframe').on("load", function () {
                that.onIframeLoaded();
            });

            this.renderIframe();

            return this;
        },
    });

    return entityContextSettingsWrapperView;
});
