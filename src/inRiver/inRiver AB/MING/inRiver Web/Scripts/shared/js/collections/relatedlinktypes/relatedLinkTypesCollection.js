﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/entityType/entityTypeModel'
], function ($, _, backbone, entityTypeModel) {
    var relatedLinkTypeCollection = backbone.Collection.extend({
        model: entityTypeModel,
        initialize: function (models, options) {
            this.entityTypeId = options.entityTypeId;
        },
        url: function () {
            return '/api/relatedentitytypes?entityTypeId=' + this.entityTypeId;
        }
    });

    return relatedLinkTypeCollection;
});