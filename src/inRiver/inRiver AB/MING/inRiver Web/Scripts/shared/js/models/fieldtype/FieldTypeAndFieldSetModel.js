﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fieldTypeFieldSetModelModel = Backbone.Model.extend({
        idAttribute: "id",
        initialize: function () {
        },
        schema: { // Used by backbone forms extension
            FieldType: { type: 'Select', title: "Field Type and Field set", options: [] }
        },
        toString: function () {
            return this.get("Id");
        },
    });

    return fieldTypeFieldSetModelModel;

});