define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'jquery-ui',
  'sharedjs/collections/apps',
  'sharedjs/views/appmenu/appMenuView',
  'sharedjs/views/usersettings/userSettingView',
  'sharedjs/views/settingsmenu/settingsMenu',
  'text!sharedtemplates/header/topHeaderTemplate.html'
], function ($, _, Backbone, modalPopup,jqueryui, apps, appMenuView, userSettingView, settingsMenu, topHeaderTemplate) {

    var topHeaderView = Backbone.View.extend({
        el: $("#top-header"),
        initialize: function (options) {
            if (options != undefined) {
                this.appName = options.app;
            } else {
                this.appName = undefined;
            }

            _.bindAll(this, "closeMenu");

            this.render();
        },
        events: {
            "click #user-settings-link": "onUserSettings",
            "click #mySettingsButton": "onToggleMenu"
        },

        onUserSettings: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var modal = new modalPopup();
            modal.popOut(new userSettingView({ id: window.appHelper.userName }), { size: "small", header: "User Details", usesavebutton: true });
        },
        onToggleMenu: function (e) {

            e.stopPropagation();
            this.menu.menu('widget').toggle();

            $(document).one('click', this.closeMenu);


        },
        closeMenu: function () {
            if (this.menu != null) {
                this.menu.menu('widget').hide();
            }
        },
        render: function () {

            this.app = _.where(apps, { name: this.appName });
            this.$el.html(_.template(topHeaderTemplate, { app: this.app[0] }));
            //this.$el.find("#signOut").prepend("<i class='fa fa-sign-out'></i> ");
            if (apps.length > 1) {
                this.$el.find("#appmenu-container").html(new appMenuView().el);
            } else {
                this.$el.find("#appMenu-launcher").hide();
            }
            //  settingsMenu.initializeSettingsMenu();

            this.listenTo(appHelper.event_bus, 'userupdated', this.render);

            this.menu = $("#personalSettings > ul.dropdown-menu").menu();
            this.menu.menu('widget').hide();
            return this;
        }
    });

    return topHeaderView;

});
