﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/collections/relatedlinktypes/relatedLinkTypesCollection',
  'sharedjs/collections/workareas/fieldTypesCollection',
  'sharedjs/collections/fieldtypes/fieldTypeAndfieldSetCollection',
  'sharedjs/views/excelexport/excelExportCvlControlView',
  'text!sharedtemplates/excelexport/excelExportEntityTypeTemplate.html'
], function ($, _, backbone, relatedLinkTypesCollection, fieldTypesCollection, fieldTypeAndfieldSetCollection, excelExportCvlControlView, excelExportEntityTypeTemplate) {
    var excelExportEntityTypeView = backbone.View.extend({
        initialize: function(options) {
            this.controlId = options.ControlDefinition;
            this.fieldSetCollection = options.FieldSetCollection;
            this.model = options.Model;
            this.entityTypeCollection = options.EntityTypeCollection;

            this.render();
        },
        events: {
            "change #export-excel-value-type-": "changeEntityTypeExcelExport",
            "change #export-excel-value-fieldset": "changeFieldSetExcelExport"
        },
        getValueControl: function(controlDefinition) {
            return this.$el.find("#export-excel-value-" + controlDefinition + "-" + this.controlId);
        },
        getValue: function (controlDefinition) {
            var value = this.getValueControl(controlDefinition).val();
            return value ? value : [];
        },
        getType: function () {
            var type = this.getValue("type");
            return type.length > 0 ? type[0] : "(none)";
        },
        getEntityType: function () {
            return this.getType().split("$$")[0];
        },
        getOldType: function() {
            return this.oldType;
        },
        hasTypeChanged: function() {
            return this.oldType !== this.getType();
        },
        confirmNewType: function() {
            this.oldType = this.getType();
        },
        getContainer: function(controlDefinition) {
            return this.$el.find("#export-excel-container-" + controlDefinition + "-" + this.controlId);
        },
        createTypeCvlControl: function (selectedValues) {
            var self = this;
            this.createCvlControl("type", this.controlId ? "Add related entity type:" : "Entity type:", false, this.entityTypeCollection, selectedValues);
            this.getValueControl("type").change(function() { self.changeEntityTypeExcelExport() });
        },
        createFieldCvlControl: function (selectedValues) {
            this.createCvlControl("field", "Fields:", true, this.filterFieldTypeCollection, selectedValues);
        },
        createFieldSetCvlControl: function (selectedValues) {
            var self = this;
            this.createCvlControl("fieldset", "Fieldsets:", true, new Backbone.Collection(this.fieldSetCollection.where({ EntityTypeId: this.getEntityType() })), selectedValues);
            this.getValueControl("fieldset").change(function () { self.changeFieldSetCvlControlExcelExport() });
        },
        createCvlControl: function (controlDefinition, caption, multiValue, values, selectedValues) {
            var cvlControl = new excelExportCvlControlView({ Id: this.controlId, ControlDefinition: controlDefinition, Caption: caption, Values: values, Multivalue: multiValue, PossibleValues: selectedValues });
            this.getContainer(controlDefinition).html(cvlControl.$el);
        },
        changeFieldSetCvlControlExcelExport: function(model) {
            var self = this;
            var selectedEntityType = this.getEntityType();
            var entityTypeFieldSets = new Backbone.Collection(this.fieldSetCollection.where({ EntityTypeId: selectedEntityType }));
            var selectedFieldSetIds = this.getValue("fieldset");
            var fieldTypeIds = this.fieldTypesCollection.pluck("id");
            var selectedFieldTypeIds;
            if (entityTypeFieldSets.length > 0) {
                this.fieldtypeAndFieldsetCollection = new fieldTypeAndfieldSetCollection([], { entityTypeId: selectedEntityType });
                this.fieldtypeAndFieldsetCollection.fetch({
                    success: function () {
                        var fieldSetFieldTypeIds = self.fieldtypeAndFieldsetCollection.pluck("fieldtypeid");
                        var generalFieldTypeIds = _.difference(fieldTypeIds, fieldSetFieldTypeIds);
                        var filteredFieldSetFieldTypes = self.fieldtypeAndFieldsetCollection.filter(function (fieldTypeAndFieldset) {
                            return _.contains(selectedFieldSetIds, fieldTypeAndFieldset.get("fieldsetid"));
                        });
                        var filteredFieldTypeIds = _.union(generalFieldTypeIds, _.map(filteredFieldSetFieldTypes, function (fieldSetFieldType) {
                            return fieldSetFieldType.get("fieldtypeid");
                        }));
                        var filteredFieldTypes = self.fieldTypesCollection.filter(function (fieldTypeValue) {
                            return _.contains(filteredFieldTypeIds, fieldTypeValue.id);
                        });
                        selectedFieldTypeIds = model && model.FieldTypeIds ? model.FieldTypeIds : filteredFieldTypeIds;
                        self.filterFieldTypeCollection = new Backbone.Collection(filteredFieldTypes);
                        self.createFieldCvlControl(selectedFieldTypeIds);
                    }
                });
            } else if (this.fieldTypesCollection.length > 0) {
                if (model && model.FieldTypeIds) {
                    selectedFieldTypeIds = model.FieldTypeIds;
                } else {
                    selectedFieldTypeIds = fieldTypeIds;
                }
                this.filterFieldTypeCollection = this.fieldTypesCollection;
                this.createFieldCvlControl(selectedFieldTypeIds);
            }
        },
        changeEntityTypeExcelExport: function (model) {
            var self = this;
            var entityTypeFieldSets = new Backbone.Collection(this.fieldSetCollection.where({ EntityTypeId: this.getEntityType() }));
            this.getContainer("fieldset").empty();
            this.getContainer("field").empty();
            if (entityTypeFieldSets.length > 0) {
                this.createFieldSetCvlControl(model ? model.FieldSetIds : null);
            }
            var entityType = this.getEntityType();
            if (entityType !== "(none)") {
                this.fieldTypesCollection = new fieldTypesCollection([], { entityTypeId: this.getEntityType() });
                this.fieldTypesCollection.fetch({
                    success: function () {
                        self.changeFieldSetCvlControlExcelExport(model);
                    }
                });
            }
        },
        getExcelExportEntityTypeModel: function () {
            var typeValue = this.getValue("type");
            if (typeValue.length) {
                var type = typeValue[0].split("$$");
                return type[0] === "(none)" ? null : {
                    EntityTypeId: type[0],
                    FieldSetIds: this.getValue("fieldset"),
                    FieldTypeIds: this.getValue("field"),
                    LinkDirection: type.length > 1 ? type[1] : null,
                    LinkTypeId: type.length > 2 ? type[2] : null
                };
            }
            return null;
        },
        render: function () {
            var template = _.template(excelExportEntityTypeTemplate, { Definition: this.controlId });
            this.setElement(template, true);
            if (this.model) {
                var selectedValues = this.model.EntityTypeId;
                if (this.model.LinkDirection) {
                    selectedValues += "$$" + this.model.LinkDirection;
                }
                if (this.model.LinkTypeId) {
                    selectedValues += "$$" + this.model.LinkTypeId;
                }
                this.createTypeCvlControl(selectedValues);
                this.changeEntityTypeExcelExport(this.model);
            } else {
                this.createTypeCvlControl();
            }
        }
    });

    return excelExportEntityTypeView;
});

