﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/models/entity/entityModel',
  'text!sharedtemplates/inputcontrols/noneEditModeControlTemplate.html'
], function ($, _, backbone, alertify, entityModel, noneEditModeControlTemplate) {

    var noneEditModeControlView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeId = options.Id;
            this.fieldValue = options.Value;
            this.readOnly = options.ReadOnly;
        },
        events: {
            "click #edit-value-icon": "onClick"
        },
        onClick: function (e) {
            e.stopPropagation();
            if (this.readOnly) {
                return; 
            }
            window.appHelper.event_bus.trigger('fieldeditmode', this.fieldTypeId);
            this.$el.remove();
        },
        close: function() {
            this.$el.remove();
        },
        visible: function() {
            if (this.$el.length) {
                return true;
            }

            return false;
        },
        render: function () {

            var template = _.template(noneEditModeControlTemplate, { Id: this.fieldTypeId, Value: this.fieldValue, ReadOnly: this.readOnly });
            this.setElement(template, true);

           // this.$el.html(template); 
            return this;
        },
    });

    return noneEditModeControlView;
});

