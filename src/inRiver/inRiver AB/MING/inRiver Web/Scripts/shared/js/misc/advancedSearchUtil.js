﻿define([
  'jquery',
  'underscore',
  'backbone'

], function ($, _, Backbone) {

    var advancedSearchUtil = {
        setupOperatorValuesForSystemMenu: function (linkId) {
            var operatorType;
            var valueType;
            switch (linkId) {
                case "SystemId":
                    operatorType = "set1";
                    valueType = "Integer";
                    break;
                case "Channel":
                    operatorType = "set1";
                    valueType = "Channels";
                    break;
                case "EntityType":
                    operatorType = "set2";
                    valueType = "EntityTypes";
                    break;
                case "FieldSet":
                    operatorType = "set4";
                    valueType = "Fieldsets";
                    break;
                case "Created":
                case "LastModified":
                    operatorType = "set3";
                    valueType = "DateTime";
                    break;
                case "CreatedBy":
                case "ModifiedBy":
                case "LockedBy":
                    operatorType = "set2";
                    valueType = "Users";
                    break;
                case "Completeness":
                    operatorType = "setCompleteness";
                    valueType = "Completeness";
                    break;
                case "CompletenessValue":
                    operatorType = "set3";
                    valueType = "Integer";
                    break;
            }

            return operatorType + "," + valueType;
        },
        setOperators: function (operatorType) {
            var operators = {};
            switch (operatorType) {
                case "set1":
                    operators = { "Equals": "Equals" };
                    break;
                case "set2":
                    operators = { "Equals": "Equals", "NotEqual": "Not Equals"};
                    break;
                case "set3":
                    operators = { "Equals": "Equals", "NotEqual": "Not Equals", "GreaterThan": "Greater Than", "GreaterThanOrEqual": "Greater Than Or Equals", "LessThan": "Less Than", "LessThanOrEqual": "Less Than Or Equals", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
                case "set4":
                    operators = { "Equals": "Equals", "ContainsAny": "Contains Any", "NotEqual": "Not Equals", "NotContainsAny": "Not Contains Any", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
                case "set5":
                    operators = { "Equals": "Equals", "NotEqual": "Not Equals", "GreaterThan": "Greater Than", "GreaterThanOrEqual": "Greater Than Or Equals", "LessThan": "Less Than", "LessThanOrEqual": "Less Than Or Equals", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
                case "set6":
                    operators = { "Equals": "Equals", "NotEqual": "Not Equals", "BeginsWith": "Begins With", "Contains": "Contains", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
                case "multiCVL":
                    operators = { "ContainsAll": "Contains All", "ContainsAny": "Contains Any", "NotContainsAll": "Not Contains All", "NotContainsAny": "Not Contains Any", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;

                case "setExisting":
                    operators = { "NotEmpty": "Existing", "Empty": "Non Existing" };
                    break;
                case "setCompleteness":
                    operators = { "IsTrue": "Complete", "IsFalse": "Incomplete" };
                    break;
                case "setBoolean":
                    operators = { "IsTrue": "True", "IsFalse": "False", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
            }
            return operators;
        },
        setNodeOperators: function (operatorType) {
            var operators = "";
            switch (operatorType) {
                case "set1":
                    operators = { "Equals": "Equals" };
                    break;
                case "set2":
                    operators = { "Equals": "Equals" };
                    break;
                case "set3":
                    operators = { "Equals": "Equals" };
                    break;
                case "set4":
                    operators = { "Equals": "Equals"};
                    break;
                case "set5":
                    operators = { "Equals": "Equals" };
                    break;
                case "set6":
                    operators = { "Equals": "Equals" };
                    break;
                case "singleCVL":
                    operators = {"ContainsAny": "Contains Any  selected" };
                    break;
                case "multiCVL":
                    operators = { "ContainsAll": "Contains All  selected", "ContainsAny": "Contains Any  selected" };
                    break;

                case "setExisting":
                    operators = { "NotEmpty": "Existing", "Empty": "Non Existing" };
                    break;
                case "setCompleteness":
                    operators = { "IsTrue": "Complete", "IsFalse": "Incomplete" };
                    break;
                case "setBoolean":
                    operators = { "IsTrue": "True", "IsFalse": "False", "Empty": "Empty", "NotEmpty": "Not Empty" };
                    break;
            }
            return operators;
        },
        getMainSystemList: function () {
            var systemList = {
                "EntityType": "Entity Type",
                "Channel": "Channel",
                "SystemId": "System Id",
                "FieldSet": "Fieldset",
                "LastModified": "Last Modified",
                "Created": "Created",
                "CreatedBy": "Created By",
                "ModifiedBy": "Modified By",
                "LockedBy": "Locked By",
                "CompletenessValue": "Completeness Value"
            };
            return systemList;
        }
    };
    return advancedSearchUtil;

});