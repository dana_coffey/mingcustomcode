define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/usersetting/UserSettingModel'
], function($, _, Backbone, UserSettingModel){
  var UserSettingCollection = Backbone.Collection.extend({
      model: UserSettingModel,
      url: '/api/usersetting'
  });
 
  return UserSettingCollection;
});
