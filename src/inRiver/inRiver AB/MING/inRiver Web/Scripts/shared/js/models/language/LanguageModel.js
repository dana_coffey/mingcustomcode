define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var LanguageModel = Backbone.Model.extend({
        idAttribute: 'Name',
        urlRoot: '/api/language',
        defaults: {
        },
        schema: { // Used by backbone forms extension
        },
        toString: function () {
            return this.get("DisplayName") + " (" + this.get("Name") + ")";
        },
    });

    return LanguageModel;

});
