﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'text!sharedtemplates/entity/entityDetailSettingWrapperTemplate.html',
  'sharedjs/misc/inRiverUtil'
], function ($, _, backbone, modalPopup, fieldModel, entityDetailSettingView, entityDetailSettingWrapperTemplate, inRiverUtil) {

    var entityDetailSettingWrapperView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.detailfields = [];
            this.showInWorkareaOnSave = true;
            this._minimalMode = !!options.minimalMode;

            if (options.showInWorkareaOnSave != null) {
                this.showInWorkareaOnSave = options.showInWorkareaOnSave;
            }

            if (options.isLinkEntityEdit) {
                this.isLinkEntityEdit = options.isLinkEntityEdit;
            }

            // This is a bit strange but this view is loaded with different types of models
            // so we have to check if we have the Fields attribute... if not, then load it.
            // This should be cleaned up made and consistent!
            if (!options.isNew && !options.model.get("Fields")) {
                this.model = new fieldModel({ id: options.model.id });
                this.model.fetch({
                    success: function () {
                        that.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.model = options.model;
                // If creating a new entity a model will be created by the subview
                that.render();
            }
        },
        events: {
            "click #entity-detail-settings-save-all-button": "onSaveAll",
            "click #entity-detail-settings-undo-all-button": "onUndoAll",
            "click #entity-detail-settings-expand": "onExpandAll",
            "click #entity-detail-settings-collapse": "onCollapseAll"
        },
        onSaveAll: function () {
            window.appHelper.event_bus.trigger('entity-details-save');
        },
        onUndoAll: function () {
            if (this.fieldSetDropdownForm) {
                this.fieldSetDropdownForm.setValue({ FieldSet: this.fieldSetDropdownForm.fields.FieldSet.editor.value }); // Undo fieldset dropdown
                this.fieldSetDropdownForm.commit();
            }
            window.appHelper.event_bus.trigger('entity-details-undo');
            this.onIsUnsavedChanged();
        },
        onExpandAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").show();
            categories.find("i.fa-minus-circle").show();
            categories.find("i.fa-plus-circle").hide();
        },
        onCollapseAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").hide();
            categories.find("i.fa-minus-circle").hide();
            categories.find("i.fa-plus-circle").show();
        },
        onClose: function () {
            if (this.entityDetailSettingSubView) {
                this.entityDetailSettingSubView.close();
            }
        },
        SaveChangesBeforeSwitchingFieldset: function () {
            window.appHelper.event_bus.trigger('entity-details-save-before-switching-fieldset');
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes

            if (this.entityDetailSettingSubView == undefined)
            {
                return false;
            }

            var isUnsaved = this.entityDetailSettingSubView.isUnsaved;
            if (this.fieldSetDropdownForm) {
                isUnsaved = isUnsaved || inRiverUtil.createValueForFormFieldComparison(this.fieldSetDropdownForm.fields.FieldSet.editor.value) != inRiverUtil.createValueForFormFieldComparison(this.model.get("FieldSet"));
            }
            return isUnsaved;
        },
        onIsUnsavedChanged: function () {
            //console.log('entityDetailSettingWrapperView::onIsUnsavedChanged');
            var isUnsaved = this.hasUnsavedChanges();
            if (!this.options.parentIsPopup) {
                if (isUnsaved) {
                    this.$el.find("#entity-detail-settings-save-all-button").show();
                    this.$el.find("#entity-detail-settings-undo-all-button").show();
                } else {
                    this.$el.find("#entity-detail-settings-save-all-button").hide();
                    this.$el.find("#entity-detail-settings-undo-all-button").hide();
                }
            } else {
                if (isUnsaved) {
                    this.$el.find("#entity-detail-settings-undo-all-button").show();
                } else {
                    this.$el.find("#entity-detail-settings-undo-all-button").hide();
                }
            }
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(entityDetailSettingWrapperTemplate, { data: this.model.toJSON() }));

            this.entityDetailSettingSubView = new entityDetailSettingView({
                model: this.model,
                onIsUnsavedChange: this.onIsUnsavedChanged,
                parentIsPopup: this.options.parentIsPopup,
                isNew: this.options.isNew,
                showInWorkareaOnSave: this.showInWorkareaOnSave,
                fieldTypesFilter: this.options.fieldTypesFilter,
                showHiddenFields: this.options.showHiddenFields,
            });
            this.listenTo(this.entityDetailSettingSubView, "entityDetailsIsUnsavedChange", this.onIsUnsavedChanged);
            this.$el.find("#details-view-wrap").html(this.entityDetailSettingSubView.el);

            if (this.model.get("FieldSetsDict")) {
                var editorAttrs = {};
                if (this.model.get("IsReadOnly")) {
                    editorAttrs = { readonly: 'readonly', class: 'readonly', disabled: 'disabled' };
                }

                var schema = {
                    FieldSet: { title: "Fieldset", type: "Select", options: this.model.get("FieldSetsDict"), editorAttrs: editorAttrs }
                };

                var formTemplate = "<form id='entity-detail-settings-fieldset-form'>";
                formTemplate += "<span>Fieldset</span><div data-editors='FieldSet'></div>";
                formTemplate += "</form>";

                // Fieldsets dropdown
                this.fieldSetDropdownForm = new Backbone.Form({
                    template: _.template(formTemplate),
                    model: this.model,
                    schema: schema
                }).render();
                this.fieldSetDropdownForm.on('FieldSet:change', function (a, b) {
                    inRiverUtil.Notify("Field values for any old fieldset will be cleared.");
                    that.SaveChangesBeforeSwitchingFieldset();
                    that.fieldSetDropdownForm.commit();
                    that.onIsUnsavedChanged();
                });
                this.$el.find("#entity-detail-settings-fieldset-dropdown-wrap").html(this.fieldSetDropdownForm.el);
            }

            if (this._minimalMode) {
                this.$el.find(".details-view-tools-row").hide();
                this.entityDetailSettingSubView.$el.find(".category-section-title").hide();
            }

            return this;
        },
        showAsPopup: function (options) {
            var that = this;
            this.isNew = false;
            this.isLinkEntityEdit = false;


            if (options.isLinkEntityEdit) {
                this.isLinkEntityEdit = options.isLinkEntityEdit;
            }

            var createPopup = function () {
                var header = that.model.attributes.EntityTypeDisplayName + " " + that.model.attributes.DisplayName;
                if (that.isNew) {
                    header = "Create " + that.model.attributes.EntityTypeDisplayName;
                }

                var modal = new modalPopup();
                modal.popOut(new entityDetailSettingWrapperView({
                    model: that.model,
                    isNew: that.isNew,
                    parentIsPopup: true,
                    fieldTypesFilter: options.fieldTypesFilter,
                    minimalMode: options.minimalMode,
                    showHiddenFields: options.showHiddenFields,
                    isLinkEntityEdit: that.isLinkEntityEdit
                }), { size: options.minimalMode ? "minimal" : "large", header: header, description: that.model.attributes.DisplayDescription, usesavebutton: true, onSave: that.onSave });
            }

            if (options.model) {
                this.model = options.model;
                createPopup();
            } else {
                if (this.entityId == 0) {
                    this.isNew = true;
                    this.model = new fieldModel({ id: options.id, entityTypeId: options.entityTypeId });
                    this.model.url = "/api/entityfields/0/" + options.entityTypeId;
                } else {
                    this.model = new fieldModel({ id: options.id }); // id 0 means new object 
                }
                this.model.fetch({
                    success: function () {
                        createPopup();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onSave: function (model, view) {
            window.appHelper.event_bus.trigger('popupsave');

            if (view.isLinkEntityEdit) {
                window.appHelper.event_bus.trigger('entityupdated-relation-view', view.model.id);
            }
        },
        showCreateEntityPopup: function (options) {
            var size = "large";
            if (options.size) {
                size = options.size;
            }

            var showInWorkareaOnSave = true;

            if (options.showInWorkareaOnSave != null) {
                showInWorkareaOnSave = options.showInWorkareaOnSave;
            }

            this.model = new fieldModel({ id: options.id, entityTypeId: options.entityTypeId });
            this.model.url = "/api/entityfields/0/" + options.entityTypeId;

            var that = this;
            this.model.fetch({
                success: function () {
                    var header = "Create " + that.model.attributes.EntityTypeDisplayName;
                    var modal = new modalPopup();

                    modal.popOut(new entityDetailSettingWrapperView(
                    {
                        model: that.model,
                        isNew: true,
                        showInWorkareaOnSave: showInWorkareaOnSave,
                        parentIsPopup: true,
                    }), { size: size, header: header, description: "", usesavebutton: true });

                    if ($(modal)[0].$el.find("#save-form-button").prop('disabled')) {
                        $(modal)[0].$el.find("#save-form-button").removeAttr("disabled");
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
    });

    return entityDetailSettingWrapperView;
});
