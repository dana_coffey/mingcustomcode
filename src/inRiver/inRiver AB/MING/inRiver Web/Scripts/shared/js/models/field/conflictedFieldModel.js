﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var conflictedFieldModel = Backbone.DeepModel.extend({
        idAttribute: "id",
        initialize: function (options) {
            this.id = options.id; 
            this.subId = options.subId; 
        },
        urlRoot: '/api/validateentityfields',
    });

    return conflictedFieldModel;

});
