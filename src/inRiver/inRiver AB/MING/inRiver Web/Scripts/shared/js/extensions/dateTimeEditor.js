﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'datetimepicker',
  'moment',
], function ($, _, Backbone, bbf, datetimepicker, moment) {

    var dateTimeEditor = Backbone.Form.editors.Text.extend({
        initialize: function (options) {
            // Call parent constructor
            Backbone.Form.editors.Base.prototype.initialize.call(this, options);
        },
        events: {
            'change': 'onChange',
            'focus': function () {
                this.trigger('focus', this);
            },
            'blur': function () {
                this.trigger('blur', this);
            },
            'keyup': "onChange"
        },
        onChange: function () {
            this.trigger('change', this);
        },
        // The set value must correctl
        setValue: function (value) {
            if (value != null) {
                this.$el.val(value);
                this.$el.datetimepicker('setOptions', { value: value });
            }
        },
        getValue: function () {
            var tmpVal = this.$el.datetimepicker("getValue");
            var d = globalUtil.toRoundtripDateFormat(tmpVal); // will create an ISO date string
            return d;
        },
        render: function () {
            // Call the parent's render method
            Backbone.Form.editors.Text.prototype.render.call(this);
            this.$el.attr("type", "text"); // set type attribute so proper styling can occur

            var readonly = false;
            if (this.schema.editorAttrs && this.schema.editorAttrs.readonly) {
                readonly = true;
            }

            if (!readonly) {

                var opts = {
                    value: globalUtil.toRoundtripDateFormat(this.value),
                    format: appHelper.momentDateTimeFormat,
                    formatTime: appHelper.momentTimeFormat,
                    formatDate: appHelper.momentDateFormat,
                    lang: 'en',
                    weeks: appHelper.showWeeksInDatePicker,
                    dayOfWeekStart: appHelper.firstDayOfWeek
                };

                // Check for RelatedDateFieldType field setting and use the value of that field as the default date for the date picker
                var relatedFieldType = this.options.model.get(this.options.schema._internalEditorKey.replace(".Value", "") + ".Settings.RelatedDateFieldType");
                if (relatedFieldType) {
                    var relatedField = _.findWhere(this.options.model.get("Fields"), { "FieldType": relatedFieldType });
                    if (relatedField && relatedField.Value) {
                        var momentDate = moment(relatedField.Value);
                        if (momentDate.isValid()) {
                            opts.defaultDate = momentDate.toDate();
                        }
                    }
                }

                this.$el.datetimepicker(opts);
            }

            return this;
        }

        
    });


    return dateTimeEditor;

});
