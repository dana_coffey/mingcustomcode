define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, backbone) {
    var fieldsetCollection = backbone.Collection.extend({
        url: '/api/fieldset'
    });

    return fieldsetCollection;
});