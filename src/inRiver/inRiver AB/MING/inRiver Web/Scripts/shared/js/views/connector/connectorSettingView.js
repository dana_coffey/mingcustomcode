define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
       'sharedjs/misc/inRiverUtil',
  'sharedjs/models/connector/connectorSettingModel',
  'sharedjs/collections/connectorsettings/connectorSettingsCollection',
  'text!sharedtemplates/connector/connectorSettingTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, connectorSettingModel, connectorSettingsCollection, connectorSettingTemplate) {

  var connectorSettingView = backbone.View.extend({
 
      initialize: function (data) {
          this.undelegateEvents();

          this.stopListening(appHelper.event_bus);
          this.listenTo(appHelper.event_bus, 'popupcancel', this.close);
          this.listenTo(appHelper.event_bus, 'popupsave', this.saveSetting);

          this.render();
      },
      events: {
          "click": "onClick"
      },
      close: function () {
          appHelper.event_bus.trigger('closepopup');
          this.undelegateEvents();
          this.$el.removeData().unbind();
          this.remove();
          backbone.View.prototype.remove.call(this);

      },
      saveSetting: function () {
          var errors = this.form.commit(); // runs schema validation

          if (!errors) {
              var self = this;

              this.model.save({
                  Key: this.form.fields.Key.editor.getValue(),
                  Value: this.form.fields.Value.editor.getValue()
              }, {
                  success: function () {
                      inRiverUtil.Notify("Setting has been updated");
                      self.close();
                      self.model.fetch(); // to make sure we get a complete description of the model from server (with role descriptions etc)
                  },
                  error: function (model, response) {
                      inRiverUtil.OnErrors(model, response);
                  }
              });
          }
      },
      deleteSetting: function() {
          //Delete model
          this.model.collection.remove(this.model);
          this.model.destroy();
          $("#edit-panel").slideUp(200, "swing");

          //Delete view
          this.remove();
      },
      onContentChanged: function () {
          this.saveSetting();
      },
      onClick: function () {
          event.stopPropagation();

      },
      render: function () {
          this.form = new backbone.Form({
              model: this.model
          });

          this.$el.append(this.form.render().el);
      }
  });

  return connectorSettingView;
});
