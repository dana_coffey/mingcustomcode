﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/permissionUtil',
  'sharedjs/collections/apps',
  'text!sharedtemplates/appmenu/appMenuTemplate.html'

], function ($, _, Backbone, alertify, permissionUtil, apps, appMenuTemplate) {

    var appMenuView = Backbone.View.extend({
        el: $("#appmenu-container"),
        initialize: function (options) {
            this.render();
        },
        events: {
            "click #app-background": "onAppBackground",
        },
        onAppBackground: function (e) {
            $("#appMenu, #app-background").toggleClass("active");
            $("#appMenu").css('top', '');
            $("#top-header-hide-gap").removeClass("top-header-hide-gap-grey");
        },
        render: function () {
            this.menuapps = [];
            var self = this;

            _.each(apps, function(app) {

                if (!app.permission || app.permission == "") {
                    self.menuapps.push(app);
                } else {
                    var callback = function(granted, app, self) {
                        if (granted == true) {
                            self.menuapps.push(app);
                        }
                    }
                    permissionUtil.GetUserPermission(app.permission, app, callback, self);
                }


            }); 


            this.$el.html(_.template(appMenuTemplate, { items: this.menuapps }));

            $("#appMenu-launcher").click(function () {
                $("#appMenu, #app-background").toggleClass("active");
                $("#appMenu").animate({
                    top: "1in",
                }, 300, function () {
                    // Animation complete.
                });

                $("#top-header-hide-gap").addClass("top-header-hide-gap-grey");
            });

            $(document).ready(function () {
                $("body").removeClass();
            });


            return this;
        }
    });

    return appMenuView;
});
