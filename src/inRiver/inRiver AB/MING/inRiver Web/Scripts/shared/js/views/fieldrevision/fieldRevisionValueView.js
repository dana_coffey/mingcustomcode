﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
   'sharedjs/misc/permissionUtil',
  'text!sharedtemplates/fieldrevision/fieldRevisionValueTemplate.html'
], function ($, _, Backbone, inRiverUtil,permissionUtil, fieldRevisionValueTemplate) {

    var fieldRevisionValueView = Backbone.View.extend({
        tagName: 'tr',
        initialize: function (options) {
            this.model = options.model;
            this.fieldDataType = options.fieldDataType;
            this.render();
        },
        events: {
            "click #restore-button": "onRestore"
        },
        onRestoreConfirmed: function (self) {
           
            window.appHelper.event_bus.trigger('updateFieldRevision', self.model);
        },
        onRestore: function (e) {
            e.stopPropagation();

            inRiverUtil.NotifyConfirm("Confirm restore value", "Are you sure you want to set this value as current value?", this.onRestoreConfirmed, this);
        },
        render: function () {
            var x = this.model.toJSON();
            this.$el.html(_.template(fieldRevisionValueTemplate, { data: x, fieldDataType: this.fieldDataType, isReadOnly: this.options.isReadOnly }));

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#restore-button"));

            return this; // enable chained calls
        }
    });

    return fieldRevisionValueView;
});
