﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var connectorEventSessionModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/connectoreventsession',
        
    });

    return connectorEventSessionModel;

});
