﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'alertify',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/field/conflictedFieldModel',
  'sharedjs/models/cvl/cvlValueModel',
  'sharedjs/models/entity/entityModel',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/extensions/dateTimeEditor',
  'sharedjs/extensions/localeStringEditor',
  'sharedjs/views/entityview/entityConflictView',
  'sharedjs/views/fieldrevision/fieldRevisionView',
  'text!sharedtemplates/entity/entityDetailSettingTemplate.html',
  'text!sharedtemplates/entity/entityCategoryTemplate.html',
  'text!sharedtemplates/entity/fieldEditorTemplate.html'
], function ($, _, backbone, backboneforms, alertify, fieldModel, conflictedFieldModel, cvlValueModel, entityModel, inRiverUtil, dateTimeEditor, localeStringEditor, editConflictView, fieldRevisionView, entityDetailSettingTemplate, entityCategoryTemplate, fieldEditorTemplate) {

    var entityDetailSettingView = backbone.View.extend({
        // This view operates without a collection
        initialize: function (options) {
            this.model = options.model;
            this.isNew = options.isNew;
            this.showInWorkareaOnSave = true;

            if (options.showInWorkareaOnSave != null) {
                this.showInWorkareaOnSave = options.showInWorkareaOnSave;
            }

            // Field types should be treated case insensitive
            if (options.fieldTypesFilter) options.fieldTypesFilter = _.map(options.fieldTypesFilter, function (v) { return v.toLowerCase(); });

            if (options.parentIsPopup) {
                window.appHelper.event_bus.off('popupcancel');
                window.appHelper.event_bus.off('popupsave');
                this.listenTo(window.appHelper.event_bus, 'popupcancel', this.onCancel);
                this.listenTo(window.appHelper.event_bus, 'popupsave', this.onSave);
            } else {

                window.appHelper.event_bus.off('entity-details-save');
                this.listenTo(window.appHelper.event_bus, 'entity-details-save', this.onSave);
            }

            this.listenTo(window.appHelper.event_bus, 'entity-details-save-before-switching-fieldset', this.onSaveChangesBeforeSwitchingFieldset);

            window.appHelper.event_bus.off('entity-details-undo');
            window.appHelper.event_bus.off('updateFieldRevision');

            this.listenTo(window.appHelper.event_bus, 'entity-details-undo', this.onUndoAllChanges);
            this.listenTo(window.appHelper.event_bus, 'updateFieldRevision', this.updateFieldRevision);

            this.entityId = options.id;
            this.entityTypeId = options.entityTypeId;

            if (this.model == null) {
                var that = this;
                this.isNew = true;
                this.model = new fieldModel({ id: options.id, entityTypeId: options.entityTypeId });
                this.model.url = "/api/entityfields/0/" + options.entityTypeId;
                this.model.fetch({
                    success: function () {
                        that.preRender();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                this.preRender();
            }
        },
        events: {
            "click #save-form-button": "onSave",
            "click #cancel-form-button": "onCancel",
            "click #add-task-button": "onNewTask",
            "change input[type=radio]": "onRadioChange",
            "change li > input[type=checkbox]": "onRadioChange",
            "change select": "onSelectChange",
            "change textarea": "onTextareaChange",
            "keyup": "onKeyUp",
            "keydown": "onKeyDown",
            "submit": "submit",
            "click .category-section-title": "onToggleCategory",
            "click #view-history-button": "onShowHistory",
            "click #hide-history-button": "onHideHistory",
            "click #field-editor-undo": "onUndoField",
            "change .select-all-cvl-values-checkbox": "onSelectAllChange"
        },
        clearNotSelectedFieldSetFields: function () {
            var that = this;
            _.each(this.model.get("Fields"), function (field) {
                if (field.FieldSets != null && !that.isFieldInCurrentFieldset(field)) {
                    field.Value = null;
                }
            });
        },
        isEventListenedTo: function (eventName) {
            return (window.appHelper.event_bus._events) ? !!window.appHelper.event_bus._events[eventName] : false;
        },
        onNewTask: function () {
            app.Router.navigate("tasksettings/0/link/" + this.model.id, { trigger: true });
        },
        onClose: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        onRadioChange: function (e) {
        },
        onKeyUp: function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (event.keyCode == 9) {
                return;
            }

            // Enter
            if (event.keyCode == 13) {
                // Do not run save if we are in a multiline prop
                if (!event.originalEvent.target.hasAttribute('data-ismultiline') &&
                    !event.originalEvent.target.hasAttribute('data-ismultiline').value) {
                    this.onSave();
                    return;
                }
            }
        },
        onKeyDown: function (event) {
            event.stopPropagation();

            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        this.onSave();
                        break;
                }
            }
        },
        onSelectChange: function () {

        },
        onSelectAllChange: function (e) {
            var that = this;
            var isChecked = $(e.currentTarget).prop("checked");
            var fieldEditor = this.form.fields[$(e.currentTarget).closest("div").attr("data-editors")].editor;
            var valueToSet = null;
            if (isChecked) {

                var fieldType = $(e.currentTarget).closest(".field-editor-wrap").attr("fieldtypeid");
                var fieldData = _.findWhere(this.model.get("Fields"), { FieldType: fieldType });

                // check if we are at specifications
                if (!fieldData) {
                    fieldData = _.map(this.model.get("Categories"),
                        function (category) {
                            return _.findWhere(category.SpecificationValues, { FieldTypeId: fieldType });
                        });
                    fieldData = _.find(fieldData,
                        function (data) {
                            return data;
                        });
                }

                valueToSet = _.pluck(fieldData.PossibleValues, "Key");
            } else {
                valueToSet = [];
            }
            fieldEditor.setValue(valueToSet);
            fieldEditor.trigger("change", fieldEditor);

        },
        onFormFieldChange: function (form, fieldEditor) {
            //console.log("entityDetailSettingView::onFormFieldChange[fieldEditor.key=" + fieldEditor.key + "]");
            if (this.isFormFieldEditorValueChanged(fieldEditor)) {
                fieldEditor.$el.closest(".field-editor-wrap").find(".field-editor-save-undo-wrapper").show();
                fieldEditor.$el.closest(".field-editor-wrap").find("#view-history-button").hide();
            } else {
                fieldEditor.$el.closest(".field-editor-wrap").find(".field-editor-save-undo-wrapper").hide();
                fieldEditor.$el.closest(".field-editor-wrap").find("#view-history-button").show();
            }
            if (form.schema[fieldEditor.schema._internalEditorKey].type == "Checkboxes") {
                this.updateSelectAllCheckbox(fieldEditor);
            }

            var cvlId = fieldEditor.schema.editorAttrs.cvlId;
            if (cvlId) {
                this.maybeUpdateChildCvls(cvlId);
            }

            this.updateIsUnsaved();
        },
        findFieldEditorFromCvlId: function (cvlId) {
            var fieldEditor = _.find(this.form.fields, function (field) {
                return field.schema.editorAttrs.cvlId === cvlId;
            });
            return fieldEditor;
        },
        findChildCvlFieldEditorsFromCvlId: function (parentCvlId) {
            var fieldEditors = _.filter(this.form.fields, function (field) {
                return field.schema.editorAttrs.parentCvlId === parentCvlId;
            });
            return fieldEditors;
        },
        maybeUpdateChildCvls: function (parentCvlId) {
            var that = this;

            var childCvlFieldEditors = this.findChildCvlFieldEditorsFromCvlId(parentCvlId);
            if (!childCvlFieldEditors) {
                return;
            } else {
                // We have child cvls to update
                var parentCvlFieldEditor = this.findFieldEditorFromCvlId(parentCvlId);
                if (!parentCvlFieldEditor) {
                    // Field editor seems to be hidden...
                    return;
                }
                var parentValues = parentCvlFieldEditor.getValue(); // not sure this works properly with multivalue cvls...
                if (!$.isArray(parentValues)) {
                    parentValues = [parentValues];
                }

                // Update all child cvl field editors
                _.each(childCvlFieldEditors, function (childCvlFieldEditor) {
                    // Find cvl id and data field in model for getting all possible values
                    var childCvlId = childCvlFieldEditor.schema.editorAttrs.cvlId;
                    var modelField = _.findWhere(that.model.get("Fields"), { CvlId: childCvlId });

                    // Save current value and re-apply it later
                    var currentValue = childCvlFieldEditor.getValue(); //modelField.Value;

                    // Filter out possible values based on parent cvl values
                    var possibleValuesForParentCvlValues = [];
                    _.each(modelField.PossibleValues, function (possibleValue) {

                        if (possibleValue.Key != "" && (possibleValue.ParentKey == null || possibleValue.ParentKey == "")) {
                            possibleValuesForParentCvlValues.push(possibleValue);
                        } else {
                            if (_.contains(parentValues, possibleValue.ParentKey) || !possibleValue.Key /* (not set)-item */) {
                                possibleValuesForParentCvlValues.push(possibleValue);
                            }    
                        }
                    });

                    // Set options in child cvl field editor
                    var optionsToSet = inRiverUtil.createBackboneFormsPossibleCvlValuesArray(possibleValuesForParentCvlValues);
                    childCvlFieldEditor.editor.setOptions(optionsToSet);

                    // Assure that the old field value is a possible value
                    if (childCvlFieldEditor.schema.isMultivalue) {
                        currentValue = _.intersection(currentValue, _.pluck(optionsToSet, "val"));
                    } else {
                        currentValue = _.contains(_.pluck(optionsToSet, "val"), currentValue) ? currentValue : "";
                    }
                    childCvlFieldEditor.setValue(currentValue);

                    // Ripple
                    that.maybeUpdateChildCvls(childCvlId);
                });
            }
        },
        updateSelectAllCheckbox: function (fieldEditor) {
            var checkBoxEl = fieldEditor.$el.closest(".field-editor-wrap").find(".select-all-cvl-values-checkbox");
            var allChecked = fieldEditor.$el.closest("ul").find("input[type='checkbox']:checked").length == fieldEditor.$el.closest("ul").find("input[type='checkbox']").length;
            checkBoxEl.prop('checked', allChecked);
        },
        updateIsUnsaved: function () {
            var that = this;
            var tmpIsUnsaved = false;

            //console.log("entityDetailSettingView::updateIsUnsaved");

            $.each(this.form.fields, function (index, field) {
                if (!field.schema.editorAttrs.readonly) {
                    tmpIsUnsaved = tmpIsUnsaved || that.isFormFieldEditorValueChanged(field.editor);
                    if (tmpIsUnsaved) {
                        return false;
                    }
                }
            });

            this.isUnsaved = tmpIsUnsaved;
            this.trigger("entityDetailsIsUnsavedChange");
            if (this.options.parentIsPopup) {
                if (!this.isUnsaved && !this.isNew) {
                    $("#save-form-button").attr("disabled", "disabled");
                } else {
                    $("#save-form-button").removeAttr("disabled");
                }
            }
        },
        submit: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.onSave();
        },
        onSaveChangesBeforeSwitchingFieldset: function () {
            this.model.set(this.form.getValue());
        },
        onSave: function () {
            //console.log("entityDetailSettingView::onSave");
            var that = this;
            this.$el.find(".field-editor-validation-error-message").html("");
            var errors = this.form.validate();
            if (errors == null) {
                this.form.commit();
                this.clearNotSelectedFieldSetFields();
                if (this.options.parentIsPopup) { // prevent saving several times which could result in multiple entities created
                    $("#save-form-button").attr("disabled", "disabled");
                }
                this.saveModelToServer(this.options.parentIsPopup ? true : false);

                // Reset the previous values in all editors to the saved model (this will make the Undo feature work properly)
                _.each(this.form.fields, function (field) {
                    field.editor.value = that.model.get(field.key);
                });

                $(".field-editor-wrap .field-editor-save-undo-wrapper").hide();
                $(".field-editor-wrap #view-history-button").show();
            } else {
                _.each(Object.keys(errors), function (error) {
                    that.$el.find("div[data-editors='" + error + "']").closest("fieldset").find(".field-editor-validation-error-message").html("<i class='fa fa-warning'></i>&nbsp;" + errors[error].message);
                });
                // Scroll to first error
                that.$el.find("div[data-editors='" + Object.keys(errors)[0] + "']").closest(".field-editor-wrap")[0].scrollIntoView();
            }
        },
        saveModelToServer: function (closeWhenFinished) {
            var that = this;

            //console.log("entityDetailSettingView::saveModelToServer");

            if (this.model.get("Fields")) {
                //First validate fields. 
                that.conflictedFieldModel = new conflictedFieldModel({ id: this.model.id });
                that.conflictedFieldModel.attributes = this.model.attributes;

                that.conflictedFieldModel.save({}, {
                    async: false,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    success: function (model, response) {
                        if (response.Result) {
                            that.modelid = that.model.id;
                            that.model.save({}, {
                                success: function (xmodel) {
                                    that.isUnsaved = false;
                                    if (that.modelid < 1) {
                                        inRiverUtil.Notify(xmodel.attributes.EntityTypeDisplayName + " successfully created");
                                        that.searchHandled = true;

                                        if (that.showInWorkareaOnSave) {
                                            $.post("/api/tools/compress", { '': xmodel.get("Id") }).done(function (result) {
                                                that.goTo("workareasearch?title=" + xmodel.get("DisplayName") + "&compressedEntities=" + result);
                                            });
                                        }

                                        window.appHelper.event_bus.trigger('entitycreated', xmodel);
                                    } else {
                                        window.appHelper.event_bus.trigger('entityupdated', xmodel.id);

                                        window.appHelper.event_bus.trigger('entityupdated-entity-card-view', xmodel.id);

                                        that.entityUpdated(xmodel.id);
                                    }

                                    if (closeWhenFinished) {
                                        that.done();
                                    }
                                },
                                error: function (errorModel, errorResponse) {
                                    inRiverUtil.OnErrors(errorModel, errorResponse);
                                }
                            });
                        } else {
                            that.editConflict = new editConflictView({ model: that.model, validationResult: response, parent: that });
                            $("#conflictContainer").html(that.editConflict.el);
                        }
                    }
                });
            } else if (this.model.get("Categories")) {
                that.model.save({}, {
                    success: function (xmodel) {
                        that.isUnsaved = false;
                        that.trigger("saveSuccessful", {});
                        that.render();
                    },
                    error: function (errorModel, errorResponse) {
                        inRiverUtil.OnErrors(errorModel, errorResponse);
                    }
                });
            }
        },
        isFormFieldEditorValueChanged: function (fieldEditor) {
            var dataType = fieldEditor.form.schema[fieldEditor.key]._internalDataType;

            var oldValue;
            var currentValue;

            //console.log("entityDetailSettingView::isFormFieldEditorValueChanged");

            if (dataType === "DateTime") {
                oldValue = inRiverUtil.createValueForFormFieldComparison(globalUtil.toRoundtripDateFormat(fieldEditor.value), dataType);
                currentValue = inRiverUtil.createValueForFormFieldComparison(globalUtil.toRoundtripDateFormat(fieldEditor.getValue()), dataType);
            } else {
                oldValue = inRiverUtil.createValueForFormFieldComparison(fieldEditor.value, dataType);
                currentValue = inRiverUtil.createValueForFormFieldComparison(fieldEditor.getValue(), dataType);
            }

            //var oldValue = inRiverUtil.createValueForFormFieldComparison(this.model.get(fieldEditor.key), dataType);
            //if (oldValue != currentValue) {  // Don't remove this code!!!!! Very useful to uncomment when debugging this view
            //    console.log(fieldEditor.form.schema[fieldEditor.key].title + " (" + fieldEditor.key + "): "  + oldValue + "->" + currentValue);
            //}
            return oldValue != currentValue;
        },
        onUndoAllChanges: function () {
            var that = this;
            _.each(this.form.fields, function (field) {
                if (that.isFormFieldEditorValueChanged(field.editor)) {
                    that.onUndoField(null, field.editor.key);
                }
            });
        },
        onUndoField: function (e, internalKey) {
            // Get internal key from the event object if not supplied
            if (!internalKey) {
                internalKey = $(e.currentTarget).closest(".field-editor-wrap").find("div[data-editors]").attr("data-editors");
            }
            var fieldEditor = this.form.getEditor(internalKey);
            fieldEditor.setValue(this.model.get(fieldEditor.key) ? this.model.get(fieldEditor.key) : ""); // Resets to previous value          
            fieldEditor.trigger("change", fieldEditor);
            fieldEditor.$el.closest(".field-editor-wrap").find(".field-editor-save-undo-wrapper").hide();
            fieldEditor.$el.closest(".field-editor-wrap").find("#view-history-button").show();
            this.updateIsUnsaved();

        },
        onFieldSetChanged: function () {
            //console.log("onFieldSetChanged");
            this.render();
        },
        onConflictSave: function () {
            var that = this;

            this.model.save({}, {
                success: function (xmodel) {
                    if (that.model.id < 1) {
                        window.appHelper.event_bus.trigger('entitycreated', xmodel);
                    } else {
                        window.appHelper.event_bus.trigger('entityupdated', xmodel.id);
                    }
                    that.done();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onConfirmCancel: function (that) {
            that.done();
        },
        onCancel: function () {
            var that = this;
            if (this.isUnsaved) {
                inRiverUtil.NotifyConfirm("Unsaved changes", "You have modified this " + this.model.attributes.EntityTypeDisplayName + ". Close and lose changes?", this.onConfirmCancel, that);
            } else {
                that.done();
            }
        },
        done: function () {
            if (this.options.parentIsPopup) {
                window.appHelper.event_bus.trigger('closepopup');
            } 

            this.close();   
        },
        entityUpdated: function (id) {
            //console.log("Entity updated!");
            if (id == this.model.id) {
                var self = this;
                self.render();
                inRiverUtil.Notify(self.model.attributes.EntityTypeDisplayName + " has been updated");
            }
        },
        updateFieldRevision: function (revisionFieldModel) {
            var internalKey = "";
            var internalDataType = "";

            _.each(this.form.fields, function (test) {

                if (test.schema._internalName === revisionFieldModel.get("FieldTypeId")) {
                    internalKey = test.schema._internalEditorKey;
                    internalDataType = test.schema._internalDataType;
                }
            });

            var fieldEditor = this.form.getEditor(internalKey);

            var revisionFieldModelData;

            if (internalDataType === "CVL") {
                revisionFieldModelData = revisionFieldModel.get("Data").split(";");
            } else {
                revisionFieldModelData = revisionFieldModel.get("Data");
            }

            fieldEditor.setValue(revisionFieldModelData, revisionFieldModel.get("Language")); // Resets to previous value              

            fieldEditor.trigger("change", fieldEditor);
        },
        preRender: function () {
            var self = this;
            self.listenTo(self.model, "change:FieldSet", self.onFieldSetChanged); // Fieldset may be modified outside of this view
            self.render();
        },
        onToggleCategory: function (e) {
            var el = $(e.currentTarget);
            el.next().toggle(200);
            el.find(".showCategory > i").toggle();
        },
        onShowHistory: function (e) {
            var el = $(e.currentTarget);
            var editorWrap = el.closest(".field-editor-wrap");
            editorWrap.find("#view-history-button").hide();
            editorWrap.find("#hide-history-button").show();
            editorWrap.find(".field-editor-history-wrap").show();
            var isReadOnly = _.findWhere(this.model.get("Fields"), { FieldType: el.attr("fieldtypeid") }).IsReadOnly;
            editorWrap.find(".field-editor-history-container").html(new fieldRevisionView({ entityId: this.model.get("Id"), fieldTypeId: el.attr("fieldtypeid"), fieldDataType: el.attr("fielddatatype"), isReadOnly: isReadOnly }).el);
        },
        onHideHistory: function (e) {
            var el = $(e.currentTarget);
            var editorWrap = el.closest(".field-editor-wrap");
            editorWrap.find("#view-history-button").show();
            editorWrap.find("#hide-history-button").hide();
            editorWrap.find(".field-editor-history-wrap").hide();
            editorWrap.find(".field-editor-history-container").html("");
            //el.closest(".field-editor-wrap").find(".field-editor-history-wrap").append("<h1>asdfasdfasdf</h1>");
        },
        entityHasFieldset: function () {
            return this.model.get("FieldSet");
        },
        isFieldInCurrentFieldset: function (field) {
            if (!this.entityHasFieldset()) {
                return false; // No fieldset specified
            } else {
                return field.FieldSets && _.contains(field.FieldSets.split(","), this.model.get("FieldSet"));
            }
        },
        isFieldDisplayable: function (field) {
            if (this.options.fieldTypesFilter && !_.contains(this.options.fieldTypesFilter, field.FieldType.toLowerCase())) {
                return false;
            }
            if (!this.options.showHiddenFields) {
                if (field.IsHidden) {
                    return false;
                }
                if (field.ExcludeFromDefaultView && !(this.entityHasFieldset() && this.isFieldInCurrentFieldset(field))) {
                    return false;
                }
            }
            return true;
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(entityDetailSettingTemplate, { data: this.model.toJSON(), isNew: this.isNew }));

            var schema = null, formTemplate = "";

            if (this.model.get("Fields")) { // Entity fields
                // Create form using dynamic schema
                schema = inRiverUtil.createFormsSchemaFromRestfulFields(this.model);

                // Entity fields
                var categoryIds = _.uniq(_.pluck(this.model.get("Fields"), "CategoryId"));
                var categoryNames = [];
                _.each(categoryIds, function (cateogryId) {
                    var categoryName = _.where(that.model.get("Fields"), { CategoryId: cateogryId })[0].CategoryName;
                    categoryNames[cateogryId] = categoryName;
                });

                // Create forms template
                formTemplate += "<form>";
                _.each(categoryIds, function (categoryId) {
                    var categoryEl = $(_.template(entityCategoryTemplate, { sectionId: categoryId, sectionName: categoryNames[categoryId] }));

                    // Create field placeholders in the forms template
                    var fieldContainerEl = categoryEl.find("#category-field-container");
                    var fieldsInCategory = _.where(that.model.get("Fields"), { CategoryId: categoryId });
                    var categoryIsEmpty = true;
                    _.each(fieldsInCategory, function (field) {
                        if (that.isFieldDisplayable(field)) {
                            var dataEditorKey = inRiverUtil.findBackboneFormsEditorKey(schema, field.FieldType);
                            var editor = _.template(fieldEditorTemplate, { type: field.FieldType, dataType: field.FieldDataType, name: field.FieldTypeDisplayName, description: field.FieldTypeDescription.replace(/\"/g, '&quot;'), dataEditorKey: dataEditorKey, revision: field.Revision, shouldTrackChanges: field.ShouldTrackChanges, highlightedLabel: that.entityHasFieldset() && that.isFieldInCurrentFieldset(field), showSelectAllCheckbox: schema[dataEditorKey].type == "Checkboxes" && schema[dataEditorKey].editorAttrs.disabled != "disabled" });
                            fieldContainerEl.append(editor);
                            categoryIsEmpty = false;
                        } else {
                            // Make sure the template and the schema are properly synced
                            delete schema[inRiverUtil.findBackboneFormsEditorKey(schema, field.FieldType)];
                        }
                    });

                    if (!categoryIsEmpty) {
                        formTemplate += categoryEl.html();
                    }
                });
                formTemplate += "</form>";
            } else if (this.model.get("Categories")) {   // Specification 
                // Create form using dynamic schema
                schema = inRiverUtil.createFormsSchemaFromSpecification(this.model);

                // Create forms template
                formTemplate += "<form>";
                _.each(this.model.get("Categories"), function (category) {

                    var categoryEl = $(_.template(entityCategoryTemplate, { sectionId: category.Id, sectionName: category.Name }));

                    // Create field placeholders in the forms template
                    var categoryHasDisplayedContent = false;
                    var fieldContainerEl = categoryEl.find("#category-field-container");
                    _.each(category.SpecificationValues, function (field) {
                        var dataEditorKey = inRiverUtil.findBackboneFormsEditorKey(schema, field.FieldTypeId, field.CategoryId);
                        if (dataEditorKey) { // Check if the value is in the form schema (it may be excluded)
                            var editor = _.template(fieldEditorTemplate, { type: field.FieldTypeId, dataType: field.DataType, name: field.Name, description: "", dataEditorKey: dataEditorKey, revision: null, shouldTrackChanges: field.ShouldTrackChanges, unit: field.Unit, showSelectAllCheckbox: schema[dataEditorKey].type == "Checkboxes" && schema[dataEditorKey].editorAttrs.disabled != "disabled" });
                            fieldContainerEl.append(editor);
                            categoryHasDisplayedContent = true;
                        } else {
                            // Make sure the template and the schema are properly synced
                            delete schema[dataEditorKey];
                        }
                    });

                    // Render this category only if it has fields in it 
                    if (categoryHasDisplayedContent) {
                        formTemplate += categoryEl.html();
                    }
                });
                formTemplate += "</form>";
            }

            this.schema = schema;
            this.form = new backbone.Form({
                template: _.template(formTemplate),
                model: this.model,
                schema: schema
            }).render();

            // Register for change events on each field editor
            _.each(this.form.fields, function (field) {
                field.editor.previousValue = null; // Workaround for a bug in backbone forms (previousValue was set to "" which means that removing the all content in one operation wouldn't trigger a change) 
                that.form.listenTo(that.form, field.key + ":change", function (form, fieldEditor, extra) {
                    //console.log("that.form.listenTo:" + extra);
                    that.onFormFieldChange(form, fieldEditor);
                });
                if (that.form.schema[field.editor.schema._internalEditorKey].type === "Checkboxes") {
                    that.updateSelectAllCheckbox(field.editor);
                }
            });

            _.each(this.form.$el.find("ul[disabled]"), function (ul) {
                $(ul).find("input").prop('disabled', true);
            });

            inRiverUtil.applyCenteredLayoutToForm(this.form.$el);
            this.$el.find("#inriver-fields-form-area").append(this.form.el);

            //Check if there is any parent/child cvls...
            _.each(this.model.get("Fields"), function (field) {
                if (field.CvlId) {
                    that.maybeUpdateChildCvls(field.CvlId);
                }
            });

            var j = $("#modalWindow").is(":visible");
            if (!j) {
                $("#modalWindow").show();
            }

            return this; // enable chained calls
        }
    });

    return entityDetailSettingView;
});