﻿define([
  'underscore',
  'backbone',
], function (_, Backbone) {

    var connectorSettingModel = Backbone.Model.extend({
        idAttribute: "Key",
        //urlRoot: '/api/connectorsetting',
        url: function () {
            return '/api/connectorsetting/' + this.get("connectId");
        },
        initialize: function () {

        },
        schema: { // Used by backbone forms extension
            Key: { type: 'Text', title: 'Setting', validators: ['required'], editorAttrs: { disabled: true } },
            Value: { type: 'TextArea', title: 'Value' },
        }
    });

    return connectorSettingModel;

});
