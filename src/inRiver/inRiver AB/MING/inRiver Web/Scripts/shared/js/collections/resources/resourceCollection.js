define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/resource/resourceModel'
], function ($, _, Backbone, resourceModel) {
    var resourceCollection = Backbone.Collection.extend({
        model: resourceModel,
        initialize: function (models, options) {
            this.entityId = options.Id;
            this.subEntities = options.subEntities;
        },
        url: function () {
            return '/api/resource?entityId=' + this.entityId + '&subEntities=' + this.subEntities;
        }
    });

    return resourceCollection;
});
