define([
    'jquery',
    'underscore'
], function($, _) {
    var panelBorders = {
        initAllEvents: function () {
            // Init collapse panel and splitters
            this.initCollapseHorizontal();
            this.initHorisontalSplitter();
            this.initVerticalSplitter();
        },
        initHorisontalSplitter: function () {
            var searchResultElement = $("#search-result-container");
            var waHeaderElement = $("#workarea-header");
            var maxBorderCellWidth = $(document).innerWidth() / 2;
            var minBorderCellWidth = 100;

            var reziseColumns = function() {
                var leftWidth = $(searchResultElement).outerWidth(true)-15;
                var entityAreaElement = document.getElementById("entity-area");

                if (leftWidth < 30) {
                    leftWidth = 30; 
                }

                if (entityAreaElement) {
                    entityAreaElement.style.left = leftWidth + 5 + 'px';
                }

                if (waHeaderElement && waHeaderElement[0]) {
                    waHeaderElement[0].style.width = leftWidth + 'px';
                }
            }
            $(searchResultElement).resizable({
                maxWidth: maxBorderCellWidth,
                minWidth: minBorderCellWidth,
                handles: 'e',
                resize: function (event, ui) {
                    reziseColumns();
                }
            });

            reziseColumns();
            $(window).resize(function () {
                reziseColumns();
            });
        },
        initVerticalSplitter: function () {
            var lowerElement = $('#entityDetailLower');
            var upperElement = $('#entityDetailUpper');
            var upperDetailsElement = $('#entity-top-info-container');
            var bottomDetailsElement = $('#entity-bottom-info-container');
            var pageElement = $('#entity-area');
            
            var mainAreaElement = $('#entity-area');
            var maxBorderCellHeight = mainAreaElement.innerHeight() - 200;
            var minBorderCellHeight = 40;

            pageElement.height($(window).height() - 100);

            var resizeRows = function() {
                var totalHeigth = $(pageElement).outerHeight(true);
                var lowerHeigth = $(lowerElement).outerHeight(true);
                upperElement[0].height = totalHeigth - lowerHeigth + 'px';
                bottomDetailsElement.height(lowerHeigth - 140);

                upperDetailsElement.height(totalHeigth - lowerHeigth - 150);
            };
            $(lowerElement).resizable({
                maxHeight: maxBorderCellHeight,
                minHeight: minBorderCellHeight,
                handles: 'n',
                resize: function (event, ui) {
                    resizeRows();
                }
            });

            resizeRows();
            $(window).resize(function () {
                resizeRows();
            });
        },
        initCollapseHorizontal: function () {
            var workareaHeaderElement = $('#workarea-header');
            var workareaHeaderLabelElement = $('#workarea-header-label');
            var workareaHeaderLabelSpanElement = $('#workarea-header-label span');
            var workareaHeaderLabelVertClass = $('.workarea-header-label-vertical');
            var searchResultElement = $('#search-result-container');
            var entityAreaElement = $('#entity-area');
            var toggleButton = $("#workarea-header-toggle-button");
            var toggleAction = function () {
                var hideWidth = searchResultElement.width();
                var curwidth = searchResultElement.offset();
                if (curwidth.left == 15) {
                    searchResultElement.animate({ marginLeft: "-" + hideWidth +15 + "px" }, 100);
                    entityAreaElement.animate({ left: "35px" }, 100);
                    // Switch direction through classes.
                    $(toggleButton).removeClass('fa-angle-left');
                    $(toggleButton).addClass('fa-angle-right');
                    var text = $("#search-result-query h4").text();
                    if ($("#search-result-query h4").length > 1) {
                        text = text.replace(")", ") | ");
                    }
                    
                    workareaHeaderLabelSpanElement.html(text);
                    workareaHeaderLabelSpanElement.show();
                    $(workareaHeaderLabelElement).removeClass('workarea-header-label-horisontal');
                    $(workareaHeaderLabelElement).addClass('workarea-header-label-vertical');
                    $(workareaHeaderElement).removeClass('workarea-header-horisontal');
                    $(workareaHeaderElement).addClass('workarea-header-vertical');
                    $(workareaHeaderElement).width(""); 

                    // Change tooltip
                    $(toggleButton).attr('title', 'Expand');
                    $(workareaHeaderLabelElement).attr('title', 'Expand');
                    $(workareaHeaderElement).height(searchResultElement.height());
                    sessionStorage.setItem("workarea-collapsed", true);
                } else {
                    searchResultElement.animate({ marginLeft: "0" }, 100);
                    entityAreaElement.animate({ left: hideWidth + "px" }, 100);

                    // Switch direction through classes.
                    $(toggleButton).removeClass('fa-angle-right');
                    $(toggleButton).addClass('fa-angle-left');
                    workareaHeaderLabelSpanElement.html("");
                    workareaHeaderLabelSpanElement.hide();
                    $(workareaHeaderLabelElement).removeClass('workarea-header-label-vertical');
                    $(workareaHeaderLabelElement).addClass('workarea-header-label-horisontal');
                    $(workareaHeaderElement).removeClass('workarea-header-vertical');
                    $(workareaHeaderElement).addClass('workarea-header-horisontal');

                    // Change tooltip
                    $(toggleButton).attr('title', 'Collapse');
                    $(workareaHeaderLabelElement).attr('title', '');
                    $(workareaHeaderElement).height('24px');

                    // Resize the entity area panel
                    var leftWidth = $(searchResultElement).outerWidth(true);
                    document.getElementById("entity-area").style.left = leftWidth -5 + 'px';
                    $(workareaHeaderElement).width(searchResultElement.width() - 15);
                    sessionStorage.setItem("workarea-collapsed", false);
                }
            }
           

            $(toggleButton).click(toggleAction);
            $("#workarea-header-label").click(toggleAction);
        }
    };
    return panelBorders;

});
