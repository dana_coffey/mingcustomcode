define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone, UserModel){
  var userPermissionsCollection = Backbone.Collection.extend({
      url: '/api/userpermission'
  });
 
  return userPermissionsCollection;
});
