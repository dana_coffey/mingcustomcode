﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/export/exportExcelConfigurationModel'
], function ($, _, Backbone, exportExcelConfigurationModel) {
    var exportExcelConfigurationCollection = Backbone.Collection.extend({
        model: exportExcelConfigurationModel,
        url: "/api/exportexcel",
    });

    return exportExcelConfigurationCollection;
});