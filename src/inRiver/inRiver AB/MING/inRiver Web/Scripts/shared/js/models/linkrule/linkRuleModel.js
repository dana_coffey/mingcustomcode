﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var linkRuleModel = Backbone.Model.extend({
        idAttribute: "id",
        initialize: function () {
        },
        url: "/api/linkrule"
    });

    return linkRuleModel;

});
