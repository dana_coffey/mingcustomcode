define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var UserSettingModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/usersetting',
        initialize: function () {

        },
        defaults: {
        },
        schema: { // Used by backbone forms extension
            DataLanguage: { type: 'Select', options: [], validators: ['required'] },
            ModelLanguage: { type: 'Select', options: [], validators: ['required'] },
        },
        toString: function () {
            return this.get("Id");
        },
    });

    return UserSettingModel;

});
