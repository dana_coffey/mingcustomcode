define([
  'jquery',
  'underscore',
  'backbone',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/tasks/tasksCollection',
  'sharedjs/views/start/startPageTaskView',
  'text!sharedtemplates/start/startpageTaskStatusTemplate.html',
  'text!sharedtemplates/start/startpageTaskGroupTemplate.html'
], function ($, _, Backbone, inRiverUtil, tasksCollection, startPageTaskView, startpageTaskStatusTemplate, startpageTaskGroupTemplate) {

    var startPageTaskListView = Backbone.View.extend({
        initialize: function (data) {
            var self = this;

            this.title = data.title;
            this.tasktype = data.tasktype;

            this.urlSuffix = "getmytasks";
            if (this.tasktype == "createdtask") {
                this.urlSuffix = "getmycreatedtasks";
            } else if (this.tasktype == "groupstasks") {
                this.urlSuffix = "groupstasks";
            }

            this.undelegateEvents();

            if (this.tasktype === "groupstasks") {
                $.get("/api/taskShallow/", {}, function (result) {

                    self.RoleAndTasksCollection = [];

                    self.RoleCollection = result;
                    self.roleIndex = 0;
                    self.totalTasks = 0;

                    self.tasksCollections = new Backbone.Collection();

                    _.each(result, function (restfulRole) {

                        self.GroupId = restfulRole.Id;
                        self.GroupName = restfulRole.Name;

                        self.tasksCollection = new Backbone.Collection();
                        self.tasksCollection.url = "/api/taskShallow/" + self.urlSuffix + "/" + restfulRole.Id;
                        self.listenTo(self.tasksCollection, "reset", self.onInitRoleCollection);
                        self.tasksCollections.add(self.tasksCollection.fetch({ reset: true, async: false }));
                    });
                });
            } else {
                this.tasksCollection = new Backbone.Collection();
                this.tasksCollection.url = "/api/taskShallow/" + self.urlSuffix;
                self.listenTo(this.tasksCollection, "reset", self.onInitTasks);
                this.tasksCollection.fetch({ reset: true, async: false });
            }
        },
        events: {

        },
        onInitTasks: function (collection) {

            this.taskCollection = collection;

            this.render();
        },
        onInitRoleCollection: function (collection) {
            var self = this;

            self.roleIndex++;
            self.RoleAndTasksCollection.push({ Id: self.GroupId, Name: self.GroupName, list: collection });

            _.each(collection.models, function (groupTaskElement) {
                self.totalTasks += groupTaskElement.get("Total");
            });

            if (self.roleIndex === self.RoleCollection.length) {

                this.render();
            }
        },
        taskCreated: function (model) {
            //if (model.attributes["EntityType"] == "Task") {
            //    var self = this;

            //    this.tasks.fetch({
            //        url: "/api/task/" + this.urlSuffix,
            //        success: function () {
            //            self.render();
            //        },
            //        error: function (model, response) {
            //            inRiverUtil.OnErrors(model, response);
            //        }
            //    });
            //}
        },
        createGroupTaskSection: function () {
            var self = this;

            _.each(self.RoleAndTasksCollection, function (roleAndTasks) {

                var roleId = roleAndTasks.Id;
                var groupName = roleAndTasks.Name;

                var groupTotal = 0;

                _.each(roleAndTasks.list.models, function (taskModel) {
                    groupTotal += taskModel.get("Total");
                });

                var groupId = self.tasktype + "-" + groupName;

                var groupSectionEl = $(_.template(startpageTaskGroupTemplate, { headertitle: groupName, id: groupId, count: groupTotal }));
                self.$el.append(groupSectionEl);

                _.each(roleAndTasks.list.models, function (taskModel) {

                    var tasks = taskModel.get("Tasks");
                    var total = taskModel.get("Total");
                    var categoryId = taskModel.get("TaskCategoryId");

                    var categoryIdWithoutSpace = categoryId.replace(/\s+/g, '');

                    var subgroupId = self.tasktype + "-" + groupName + "-" + categoryIdWithoutSpace;

                    var taskStatusSectionEl = $(_.template(startpageTaskStatusTemplate, { headertitle: categoryId, count: total, id: subgroupId }));

                    if (tasks.length > 0) {

                        var taskCount = 0;
                        _.each(tasks, function (model) {
                            taskCount++;

                            if (taskCount === 10 && total > 10) {
                                var task = new startPageTaskView({ model: model });
                                $(taskStatusSectionEl).find("#" + subgroupId).append(task.render().el);

                                $(taskStatusSectionEl).find("#" + subgroupId).append("<div class='showAllTasks' id='show-all-" + subgroupId + "'>Show all...</div>");

                                $(taskStatusSectionEl).find("#show-all-" + subgroupId).click({ status: categoryId, roleId: roleId }, function (event) {

                                    var query = [];

                                    query.push({
                                        type: "Data",
                                        id: "TaskAssignedGroupTask",
                                        operator: "Equals",
                                        value: event.data.roleId
                                    });

                                    query.push({
                                        type: "Data",
                                        id: "TaskStatus",
                                        operator: "Equals",
                                        value: event.data.status
                                    });

                                    var searchQuery = JSON.stringify(query);

                                    window.appHelper.advQuery = searchQuery;
                                    Backbone.history.loadUrl("workareasearch?title=Query&advsearch=" + Math.random(10000));
                                });

                            } else {
                                var task = new startPageTaskView({ model: model });
                                $(taskStatusSectionEl).find("#" + subgroupId).append(task.render().el);
                            }
                        });

                        $(groupSectionEl).find("#" + groupId).append(taskStatusSectionEl);
                    }

                    handleSubGroupToggleClick("#" + subgroupId);

                    function handleSubGroupToggleClick(id) {
                        var $heading = self.$el.find(id + '-heading');
                        var $showTask = self.$el.find(id + '-show i');

                        $heading.click(function () {
                            $(id).slideToggle(200);
                            $showTask.toggle();
                        });
                    }
                });

                handleGroupToggleClick("#" + groupId);

                function handleGroupToggleClick(id) {
                    var $heading = self.$el.find(id + '-group-heading');
                    var $showTask = self.$el.find(id + '-group-show i');

                    $heading.click(function () {
                        $(id).slideToggle(200);
                        $showTask.toggle();
                    });
                }
            });

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'entitycreated', self.taskCreated);

        },
        createTaskSection: function () {
            var self = this;

            var groupIndex = 0;

            _.each(this.taskCollection.models, function (taskModel) {
                var tasks = taskModel.get("Tasks");
                var total = taskModel.get("Total");
                var categoryId = taskModel.get("TaskCategoryId");

                var sectionElementId = self.tasktype + "-" + groupIndex;

                if (tasks.length > 0) {
                    var sectionEl = $(_.template(startpageTaskStatusTemplate, { headertitle: categoryId, count: total, id: sectionElementId }));

                    self.$el.append(sectionEl);

                    var taskCount = 0;
                    _.every(tasks, function (model) {

                        taskCount++;

                        if (taskCount === 10 && total > 10) {
                            var task = new startPageTaskView({ model: model });
                            $(sectionEl).find("#" + sectionElementId).append(task.render().el);

                            $(sectionEl).find("#" + sectionElementId).append("<div class='showAllTasks' id='show-all-" + sectionElementId + "'>Show all...</div>");

                            $(sectionEl).find("#show-all-" + sectionElementId).click({ status: categoryId, tasktype: self.tasktype }, function (event) {

                                var query = [];

                                var taskTypeId = "TaskAssignedTo";

                                if (event.data.tasktype === "createdtask") {
                                    taskTypeId = "TaskCreatedBy";
                                }

                                query.push({
                                    type: "Data",
                                    id: taskTypeId,
                                    operator: "Equals",
                                    value: window.appHelper.userName
                                });

                                query.push({
                                    type: "Data",
                                    id: "TaskStatus",
                                    operator: "Equals",
                                    value: event.data.status
                                });

                                var searchQuery = JSON.stringify(query);

                                window.appHelper.advQuery = searchQuery;
                                Backbone.history.loadUrl("workareasearch?title=Query&advsearch=" + Math.random(10000));
                            });

                        } else {
                            var task = new startPageTaskView({ model: model });

                            $(sectionEl).find("#" + sectionElementId).append(task.render().el);
                        }

                        return taskCount !== 10;
                    });
                }

                groupIndex++;
            });

            var ids = self.$el.find(".tasks-list");

            for (var index = 0; index < ids.length; ++index) {

                if (index > 10) {


                    return;
                }

                showTasks('#' + ids[index].id);
            }

            function showTasks(id) {
                id = id.replace(" ", "-");
                var $heading = self.$el.find(id + '-heading');
                var $showTask = self.$el.find(id + '-show i');

                $heading.click(function () {
                    $(id).slideToggle(200);
                    $showTask.toggle();
                });
            }

            self.stopListening(appHelper.event_bus);
            self.listenTo(appHelper.event_bus, 'entitycreated', self.taskCreated);

        },
        render: function () {

            $("#" + this.tasktype + " .loadingspinner").remove();

            this.$el.empty();

            if (this.tasktype === "groupstasks") {

                if (this.tasksCollections.models.length === 0) {

                    $("#" + this.tasktype + " .white-box").html("<div class='noresult'>No tasks</div>");
                    return this;
                }

                this.createGroupTaskSection();

            } else {
                if (this.taskCollection.models.length === 0) {

                    $("#" + this.tasktype + " .white-box").html("<div class='noresult'>No tasks</div>");
                    return this;
                }

                this.createTaskSection();
            }

            return this;
        }
    });

    return startPageTaskListView;
});

