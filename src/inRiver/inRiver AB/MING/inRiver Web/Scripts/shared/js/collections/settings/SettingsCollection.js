define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/setting/SettingModel'
], function($, _, backbone, settingModel){
  var settingsCollection = backbone.Collection.extend({
      model: settingModel,
      url: '/api/setting'
  });
 
  return settingsCollection;
});
