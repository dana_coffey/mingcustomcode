define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/relation/relationByTypeModel'
], function ($, _, backbone, relationByTypeModel) {
    var relationByTypeCollection = backbone.Collection.extend({
        model: relationByTypeModel,
        initialize: function (models, options) {
            this.entityId = options.entityId;
            this.direction = options.direction;
            this.entityType = option.entityType;
        },
        url: function () {
            return '/api/relation?id=' + this.entityId + '&direction=' + this.direction + '&entitytypeid=' + this.entityType;
        }
    });

    return relationByTypeCollection;
});
