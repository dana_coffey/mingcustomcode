﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'views/entityview/entityView',
  'text!templates/entity/entityFieldTemplate.html'
], function ($, _, Backbone, alertify, inRiverUtil, entityView, entityFieldTemplate) {

    var entityDetailFieldView = Backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.field = options.field;
            this.fieldset = options.fieldset;
            this.render();
        },
        events: {
            "click .width-limited-container": "onClick",
            "click #save-button": "onSave",
            "change": "onChange"
        },
        onChange: function (e) {
            this.$el.find("#save-button").removeAttr("disabled");
        },
        onSave: function (e) {
            var that = this;
            var entityId = this.field.EntityId;
            var fieldType = this.field.FieldType;

            this.field.Value = $("#" + entityId + "_" + fieldType).val();
            var onSuccess = function (field) {
                that.$el.find("#value").text(field.DisplayValue);
                appHelper.event_bus.trigger('entityupdated', that.field.EntityId);
                inRiverUtil.Notify("Field has been updated");
            };

            $.ajax({
                type: "POST",
                url: "/api/entityfields/" + entityId + "/" + fieldType,
                data: { value: this.field.Value },
                success: onSuccess,
                dataType: "json"
            });
        },
        onClick: function (e) {
            e.stopPropagation();
            this.$el.find(".editrow").toggle(200);
        },
        render: function () {
            var f = this.field;

            if (!f.IsHidden && (!f.ExcludeFromDefaultView || (f.ExcludeFromDefaultView && f.FieldSets.split(',').indexOf(this.fieldset)))) {
                var historyId = null;
                if (f.Revision > 0) {
                    historyId = f.FieldType;
                }
                this.$el.html(_.template(entityFieldTemplate, { key: f.FieldTypeDisplayName, value: f.DisplayValue, type: f.FieldDataType, isOddRow: appHelper.i++ % 2 == 1, excludeFromDefaultView: f.ExcludeFromDefaultView, historyId: historyId }));

                if (f.FieldDataType == "LocaleString") {

                } else if (f.FieldDataType == "CVL") {

                } else {

                    this.$el.find(".editor").append("<input type=\"text\" value=\"" + f.Value + "\" id=\"" + this.field.EntityId + "_" + this.field.FieldType + "\">");
                }
            }

            return this; // enable chained calls
        }
    });

    return entityDetailFieldView;
});
