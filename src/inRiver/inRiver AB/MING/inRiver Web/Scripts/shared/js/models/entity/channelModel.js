﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, backbone) {

    var channelModel = backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/entity',
        
    });

    return channelModel;

});
