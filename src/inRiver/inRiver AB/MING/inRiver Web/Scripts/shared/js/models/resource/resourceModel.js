﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var resourceModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        //urlRoot: '/api/resource',
        
    });

    return resourceModel;

});
