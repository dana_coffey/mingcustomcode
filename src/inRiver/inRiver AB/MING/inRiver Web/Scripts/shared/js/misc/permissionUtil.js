﻿/// <reference path="../views/entityview/entityDetailSettingWrapperView.js" />
define([
  'jquery',
  'underscore',
  'sharedjs/misc/inRiverUtil'

], function ($, _, inRiverUtil) {

    var permissionUtil = {
        GetUserPermission: function (permission, id, callback, sender) {
            var granted = permissionUtil.hasAnyPermission(permission);
            callback(granted, id, sender);
        },
        CheckPermissionForElement: function (permission, element) {
            var granted = permissionUtil.hasAllPermissions(permission);
            if (!granted) {
                element.hide();
            }
        },
        CheckPermission: function (permission) {
            var granted = permissionUtil.hasAllPermissions(permission);
            return granted;
        },
        hasAllPermissions: function (permissions) {
            if (window.appHelper.isAdministrator) {
                return true;
            }
            var permissionList = permissions.split(",");
            var hasAllPermissions = _.every(permissionList, function (permission) {
                var hasPermission = _.contains(window.appHelper.permissions, permission);
                return hasPermission;
            });

            return hasAllPermissions;
        },
        hasAnyPermission: function (permissions) {
            if (window.appHelper.isAdministrator) {
                return true;
            }
            return _.any(_.intersection(permissions.split(","), window.appHelper.permissions));
        }
    };

    return permissionUtil;

});

