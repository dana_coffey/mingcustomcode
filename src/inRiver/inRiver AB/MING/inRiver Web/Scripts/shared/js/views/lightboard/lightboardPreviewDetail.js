﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/collections/relations/relationCollection',
    'sharedjs/collections/channel/channelEntityCollection',
    'sharedjs/models/completeness/completenessModel',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/models/field/fieldModel',
    'sharedjs/models/resource/resourceModel',
    'sharedjs/models/file/fileModel',

    'modalPopup',
    'sharedjs/views/lightboard/lightboardPreviewDetailList',
    'sharedjs/views/lightboard/lightboardPreviewRelationList',
    'sharedjs/views/lightboard/lightboardPreviewChannelList',

    'sharedjs/views/entityview/entityDetailSettingView',
    'sharedjs/views/completeness/completenessPopupView',
    'sharedjs/misc/permissionUtil',
    'sharedjs/misc/inRiverUtil',
    'text!sharedtemplates/lightboard/lightboardPreviewDetailTemplate.html',
    'text!sharedtemplates/resource/resourceDownloadPopup.html'
], function ($, _, Backbone, relationCollection, channelEntityCollection, completenessModel, relationModel, entityModel, fieldModel, resourceModel, fileModel, modalPopup, lightboardPreviewDetailListView, lightboardPreviewRelationListView, lightboardPreviewChannelListView, entityDetailSettingView, completenessPopupView, permissionUtil, inRiverUtil, lightboardPreviewDetailTemplate, resourceDownloadPopup) {

    var lightboardPreviewDetail = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.collection = options.collection;
            var selectedId = options.id;
            this.parentEntityId = options.parentEntityId;
            
            this.selectedResource = _.find(options.collection.models, function (model) {
                return model.id == selectedId;
            });
            this.selectedFile = new fileModel(selectedId);
            if (this.selectedResource.attributes["ResourceFileId"]) {
                this.selectedFile.url = "/api/file/" + this.selectedResource.attributes["ResourceFileId"];
            } else {
                this.selectedFile.url = "/api/file/-1";
            }
            this.selectedEntity = new entityModel(selectedId);
            this.selectedEntity.url = "/api/entity/" + selectedId;


            this.relations = new relationCollection([], { entityId: selectedId, direction: "inbound" });
            this.channels = new channelEntityCollection(([], { id: options.id }));
            var self = this;
           
            this.selectedEntity.fetch({
                success: function () {

                    self.selectedEntity.id = self.selectedEntity.attributes.Id;

                    self.selectedFile.fetch({
                        success: function () {

                            self.relations.fetch({
                                success: function () {
                                    self.channels.fetch({
                                        success: function () {
                                            self.render();
                                        },
                                        error: function (model, response) {
                                            inRiverUtil.OnErrors(model, response);
                                        }
                                    });
                                },
                                error: function (model, response) {
                                    inRiverUtil.OnErrors(model, response);
                                }
                            });
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click .view-details": "onDetailsClick",
            "click .set-main-picture-popup": "setMainPicture",
            "click .download-button": "onDownload",
            "click .edit-resource-in-tool": "onEditinAssApp",

            "click .upload-button": "onUpload",
            "click .delete-resource": "onResourceRemove",
        },
        onEditinAssApp: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var link = document.createElement('a');
            link.href = "../picture/download?id=" + this.selectedResource.attributes.ResourceFileId + "&size=Original";
            link.download = "../picture/download?id=" + this.selectedResource.attributes.ResourceFileId + "&size=Original";
            document.body.appendChild(link);
            link.click();
        },
        onUpload: function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("#my-dropzone #resourceId").val(e.currentTarget.id);
            $("#my-dropzone #type").val("ReplaceResource");
            this.myAwesomeDropzone.options.maxFiles = 1;
            $("#my-dropzone").trigger("click");
            //this.openDropzoneModal();
        },
        setMainPicture: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var that = this;

            $.get("/api/tools/setmainpicture/" + this.parentEntityId + "/" + this.id, function (result) {

                inRiverUtil.Notify("Main picture successfully updated");

                appHelper.event_bus.trigger('entityupdated', that.parentEntityId);
            });

            //this.getCollection();
        },
        onDownload: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var self = this;

            //self.$el.append(_.template(resourceDownloadPopup, { resource: self.selectedResource.toJSON() }));

            //var downloadElement = _.template(resourceDownloadPopup, { resource: self.selectedResource.toJSON() });
            //self.$el.append(downloadElement);

            //if (self.selectedResource.get("DownloadConfigurations").length > 0) {
            //    var downloadElement = _.template(resourceDownloadPopup, { resource: self.selectedResource.toJSON() });
            //    self.$el.append(downloadElement);
            //}

            var downloadElement = _.template(resourceDownloadPopup, { resource: self.selectedResource.toJSON() });
            // self.$el.append(downloadElement);

            if (this.$el.find(".download-menu").length === 0) {
                self.$el.append(downloadElement);
            } else {
                self.$el.find(".download-menu")[0] = downloadElement;
            }

            $(".download-menu").menu();
            //$(e.currentTarget).parent().parent().next().menu();

            var menu = $(".download-menu").show().position({
                my: "left top",
                at: "left bottom",
                of: e.currentTarget
            });

            menu.mouseleave(function () {
                menu.hide();
            });

            $(document).one("click", function () {
                menu.hide();
            });
        },
        onResourceRemoveConfirmed: function (self) {
            self.model = self.collection.get(self.id);
            self.model.url = "/api/resource/" + self.model.id;
            self.model.destroy();

            appHelper.event_bus.trigger('entitydeleted', self.id);
            appHelper.event_bus.trigger('entityupdated', self.parentEntityId);
/*
             if (self.collection.length == 0) {
                appHelper.event_bus.trigger('entitydeleted', self.parentEntityId);
               
            }
            else {
                appHelper.event_bus.trigger('entityupdated', self.parentEntityId);


            }*/

            appHelper.event_bus.trigger('closepopup');

            inRiverUtil.Notify("Resource successfully deleted");
        

        },
        onResourceRemove: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var self = this;
            inRiverUtil.NotifyConfirm("Confirm delete resource", "Are you sure you want to delete this resource?", this.onResourceRemoveConfirmed, self);
         },
        onDetailsClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            window.appHelper.event_bus.trigger('closepopup');
            this.goTo("entity/" + e.currentTarget.id);

        },
        render: function () {
            var self = this;
            this.$el.html(_.template(lightboardPreviewDetailTemplate, { resource: this.selectedResource.toJSON(), file: this.selectedFile.toJSON() }));

            if(!permissionUtil.CheckPermission("DeleteEntity")) {
                $(".delete-resource").hide();
            }

            $(".preview-area img").load(function () {
                var height = $(this).height();
                var width = $(this).width();

                var divHeight = self.$el.find(".preview-area").height();
                var divWidth = self.$el.find(".preview-area").width();


                if (divHeight < height || divWidth < width) {
                    alert("Den passar inte");
                }
            });
            var entDet = new lightboardPreviewDetailListView({ model: this.selectedEntity });

            this.$el.find("#metadata-area").html(entDet.$el);

            if (this.relations.length == 0) {
                this.$el.find("#inbound-area").hide();
                this.$el.find("#inbound").hide();
            }


            var entRel = new lightboardPreviewRelationListView({
                collection: this.relations
            });
            this.$el.find("#inbound-area").html(entRel.$el);


            if (this.channels.length == 0) {
                this.$el.find("#channel-area").hide();
                this.$el.find("#channel").hide();
            }

            var entChannels = new lightboardPreviewChannelListView({
                collection: this.channels
            });
            this.$el.find("#channel-area").html(entChannels.$el);


            var ids = self.$el.find(".section-title");

            for (index = 0; index < ids.length; ++index) {

                show('#' + ids[index].id);
            }

            function show(id) {
                id = id.replace(" ", "-");
                var $heading = self.$el.find(id);
                var $showTask = self.$el.find(id + '-show i');

                $heading.click(function () {
                    self.$el.find(id + "-area").slideToggle(200);
                    $showTask.toggle();
                });
            }

            return this; // enable chained calls
        }
    });

    return lightboardPreviewDetail;
});
