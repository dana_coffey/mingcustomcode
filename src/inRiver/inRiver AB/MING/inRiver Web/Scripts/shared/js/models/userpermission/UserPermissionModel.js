define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var userPermissionModel = Backbone.Model.extend({
        idAttribute: 'permission',
        urlRoot: '/api/userpermission',
    });

    return userPermissionModel;
});

