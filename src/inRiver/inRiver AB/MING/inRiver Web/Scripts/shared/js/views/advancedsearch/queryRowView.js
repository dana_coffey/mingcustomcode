﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/entityTypes/entityTypeCollection',
  'sharedjs/collections/entities/entityCollection',
  'sharedjs/collections/fieldtypes/FieldTypesCollection',
  'sharedjs/collections/fieldsets/fieldsetCollection',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/collections/completeness/CompletenessDefinitionCollection',
  'sharedjs/collections/completeness/CompletenessRuleCollection',
  'sharedjs/collections/channel/channelCollection',
  'sharedjs/collections/cvl/cvlValueCollection',
  'sharedjs/collections/languages/LanguagesCollection',
  'sharedjs/collections/users/usersCollection',
  'sharedjs/misc/advancedSearchUtil',
    'text!sharedtemplates/query/queryRowTemplate.html',
    'text!sharedtemplates/query/datetimeIntervalsValueInputTemplate.html'

], function ($, _, backbone, alertify, inRiverUtil, entityTypeCollection, entityCollection, fieldTypeCollection, fieldsetCollection, relationTypeCollection, completenessDefinitionCollection, completenessRuleCollection, channelCollection, cvlValueCollection, languagesCollection, usersCollection, advancedSearchUtil, queryRowTemplate, datetimeIntervalsValueInputTemplate) {

    var queryRowView = backbone.View.extend({
        initialize: function (options) {
            var self = this;

            this.mainrowId = options.mainRowId;
            this.rowId = options.rowId;
            this.parent = options.parent;
            this.conditionType = options.type;
            this.subview = options.subview;
            this.completenessDefinition = options.completenessDefinition;
            this.completenessGroup = options.completenessGroup;
            this.completenessRule = options.completenessRule;
            this.queryLanguage = options.queryLanguage;

            if (options.datetimeInterval) {
                this.datetimeIntervals = true;
                this.datetimeIntervalsValue = options.queryValue;
            }

            if (options.queryCriteria) {
                this.dataQueryCriteria = options.queryCriteria;
            }

            if (this.queryLanguage) {
                this.localeStringLanguage = this.queryLanguage;
            }

            this.queryValue = options.queryValue;
            this.queryOperator = options.queryOperator;
            this.predefinedEntityType = options.predefinedEntityType;

            if (this.queryOperator == "Equal") {
                this.queryOperator = "Equals";
            } else if (!this.queryOperator) {
                this.queryOperator = "Equals";
            }

            this.querySystem = options.querySystem;
            this.relationDirection = "";
            this.relationEntityType = "";
            this.entityType = "";
            this.showLanguage = false;
            this.showValue = true;
            if (options.entityType != undefined) {
                this.entityType = options.entityType;
            }
            if (options.entityId != undefined) {
                this.entityId = options.entityId;
            }
            $(document).on('click', this.deselect.bind(this));
            this.listenTo(window.appHelper.event_bus, 'clearsublist', this.clearSubItems);

            this.collection = new completenessDefinitionCollection(null, {});
            this.collection.fetch({
                async: false,
                url: "/api/completeness/definition",
                success: function (model, response) {

                    if (response.length > 0) {
                        self.completenessExists = true;
                    } else {
                        self.completenessExists = false;
                    }

                    self.render();
                },
                error: function (model, errorMessage) {
                    inRiverUtil.OnErrors(model, errorMessage);
                }

            });
        },
        deselect: function (element) {

            if (element != undefined && (element.target.id == "filter" || element.target.id == "value-input")) {
                return;
            }
            this.validated = true;
            $(".query-menu").hide();

            var $input = this.$el.find("#value-input").filter(":visible");
            var $link = this.$el.find("#value-link").filter(":hidden");

            if (!$input.length) {

                if (this.isDateTimeValue && this.datetimeIntervals) {

                    var datetimeLinkValue = this.$el.find("#value-link-datetime-intervals").filter(":visible");

                    if (!datetimeLinkValue.length) {
                        return;
                    }

                    $link = datetimeLinkValue;
                    $input = datetimeLinkValue;

                } else {
                    return;
                }
            } else {

                //Hide Interval option if regular datetime value is selected.
                if (this.isDateTimeValue && $input.val() !== "") {
                    this.$el.find("#value-link-datetime-intervals").hide();
                    this.$el.find(".datetime-separator").hide();
                } else {
                    this.$el.find("#value-link-datetime-intervals").show();
                    this.$el.find(".datetime-separator").show();
                }
            }

            var valueInput = $input.val();

            if (this.isIntValue) {
                this.validated = false;
                if (parseInt(valueInput) != NaN) {
                    this.validated = true;
                } else {
                    $input.val("");
                }
            }

            if (valueInput != "" && this.validated) {
                this.queryValue = valueInput;
                $link.html(this.queryValue);
            } else {

                if (this.queryValue != undefined && this.queryValue != "" && valueInput == "") {
                    this.queryValue = "";

                    if (this.datetimeIntervals) {
                        $link.html("enter an interval");
                    } else {
                        $link.html("enter a value");
                    }
                }
            }

            if (this.showValue == true) {
                $input.hide();
                $link.show();
            }
        },
        clearSubItems: function (rowId) {
            if (this.mainrowId == rowId) {
                this.remove();
                this.unbind();

                var oldRows = this.parent.rows;

                this.parent.rows = [];

                $.each(oldRows, function (index, row) {
                    if (row.mainrowId == undefined || row.mainrowId != this.mainrowId) {
                        this.parent.rows.push(row);
                    }
                });
            }
        },
        events: {
            "click #system-link": "onSystemLink",
            "click #operator-link": "onOperatorLink",
            "click #language-link": "onLanguageLink",
            "click #value-link": "onValueLink",

            "click #system-list div.category": "onCategoryClick",
            "click #system-list div.showCategory": "onCategoryClick",
            "click #system-list div": "onSystemLinkSelection",
            "click #operator-list li": "onOperatorLinkSelection",
            "click #language-list li": "onLanguageLinkSelection",
            "click #value-list div": "onValueLinkSelection",
            "click #value-list input": "onValueLinkSelection",

            "click .field-plus-row": "onPlusRow",
            "click .relation-plus-row": "onPlusRow",
            "click .completeness-plus-row": "onPlusRow",
            "click .specification-plus-row": "onPlusRow",
            "keyup #filter": "onFiltering",
            "click #value-link-datetime-intervals": "onExtendDatetimeWithIntervals",
            "click #data-list-datetime-intervals li": "clickDatetimeIntervalsListItem"
        },
        onExtendDatetimeWithIntervals: function (e) {

            e.stopPropagation();
            e.preventDefault();
            var self = this;

            this.$el.find("#data-list-datetime-intervals:first").toggle(50);
        },
        clickDatetimeIntervalsListItem: function (e) {
            if (e != null) {
                e.stopPropagation();

                if (e.currentTarget.id === "none-date-interval") {
                    this.$el.find("#value-link-datetime-intervals").val("enter an interval");
                    this.$el.find("#value-link").show();

                    if (this.$el.find("#value-link").html() == "") {

                        this.$el.find("#value-input").val("");
                        this.$el.find("#value-link").html("enter a value");
                    }

                    this.$el.find(".datetime-separator").show();
                    this.datetimeIntervalsValue = null;
                } else {
                    this.$el.find("#value-link").hide();
                    this.$el.find(".datetime-separator").hide();

                    this.$el.find("#value-link-datetime-intervals").val("NOW + " + e.currentTarget.innerHTML);

                    this.datetimeIntervalsValue = e.currentTarget.id;
                    this.datetimeIntervals = true;
                }

                //Needs to be after so the value is set.
                this.deselect();
            }
        },
        onFiltering: function (e) {
            e.stopPropagation();

            var filterString = this.$el.find("input#filter").val();


            if (filterString && filterString.length > 2) {
                $("div.item").hide();
                $("div.item").each(function (i, args) {
                    if (args.innerText.toLowerCase().indexOf(filterString.toLowerCase()) >= 0) $(args).show();
                });
            } else {
                $("div.item").show();
            }

        },
        onCategoryClick: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var catid = e.currentTarget.id;

            if (catid.indexOf("-show") < 0) {
                catid += "-show";
            }
            this.$el.find("#" + catid + " i").toggle();
            var fields = this.$el.find("#" + catid + " i").parent().parent().find("#fields");
            fields.toggle();

        },
        onPlusRow: function (e) {
            e.stopPropagation();
            e.preventDefault();
            //if (this.queryValue == null || this.queryValue == "") {
            //    return; 
            //}
            if (e.currentTarget.id == "RelationData") {
                var self = this;
                var relatedEntityTypeId = "";
                //Find the relationtype in in or outbound collections

                var linkedentitytype = _.find(self.inboundcollection.models, function (model) {
                    return "i" + model.attributes.Id == self.relationType;
                });
                if (linkedentitytype != null) {
                    relatedEntityTypeId = linkedentitytype.attributes.SourceEntityTypeId;
                } else {
                    linkedentitytype = _.find(self.outboundcollection.models, function (model) {
                        return "o" + model.attributes.Id == self.relationType;
                    });
                    if (linkedentitytype != null) {
                        relatedEntityTypeId = linkedentitytype.attributes.TargetEntityTypeId;
                    }
                }

                if (relatedEntityTypeId != null) {
                    this.parent.addSubCondition({ rowId: this.rowId, view: this, entityType: relatedEntityTypeId, type: "RelationData" });
                }
            } else if (e.currentTarget.id === "SpecificationData") {
                this.parent.addSubCondition({ rowId: this.rowId, view: this, entityType: this.entityType, entityId: this.querySystem, type: "SpecificationData" });
            } else {
                if (this.queryValue == null || this.queryValue == "") {
                    return;
                }

                this.parent.addSubCondition({ rowId: this.rowId, view: this, entityType: this.entityType, type: e.currentTarget.id });

                if (e.currentTarget.id == "Relation") {
                    this.$el.find("#query-plus-row #Relation").addClass("hidden");
                }
            }
        },
        onClose: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.remove();
            this.unbind();
        },
        onSystemLink: function (e) {
            e.stopPropagation();
            e.preventDefault();

            this.$el.find("#system-list:first").toggle(50);
            this.$el.find("#operator-list").hide();
            this.$el.find("#value-list").hide();
        },
        onOperatorLink: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find("#system-list").hide();
            this.$el.find("#operator-list:first").toggle(50);
            this.$el.find("#value-list").hide();
        },
        onLanguageLink: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find("#system-list").hide();
            this.$el.find("#language-list:first").toggle(50);
            this.$el.find("#value-list").hide();
        },
        onValueLink: function (e) {

            $(".query-menu").hide();

            e.stopPropagation();
            e.preventDefault();

            this.$el.find("#system-list").hide();
            this.$el.find("#operator-list").hide();

            if (this.valuesList) {
                this.$el.find("#value-list:first").toggle(50);
            } else {
                this.$el.find("#value-input").show();
                this.$el.find("#value-input").focus();
                this.$el.find("#value-link").hide();
            }
        },
        onValueLinkSelection: function (e) {
            e.stopPropagation();
            var self = this;

            if (this.isCVLValue) {
                if (e.currentTarget.className == "item") {
                    var checkbox = $(e.currentTarget).find("input")[0];
                    if (checkbox.checked) {
                        checkbox.checked = !checkbox.checked;
                    } else {
                        checkbox.checked = true;
                    }
                }

                var i = this.$el.find("#value-list input:checked");

                var keys = "";
                var values = "";
                $.each(i, function (index, value) {

                    var key = value.parentElement.getAttribute("data-cvlkey");

                    keys += key + ";";
                    values += value.value + ",";
                });
                if (keys.length > 0) {
                    keys = keys.substring(0, keys.length - 1);
                }
                if (values.length > 0) {
                    values = values.substring(0, values.length - 1);
                }

                this.queryValue = keys;

                if (keys === "") {
                    this.$el.find("#value-link").html("select value");
                } else {
                    this.$el.find("#value-link").html(values);
                }
            } else {
                this.deselect();

                if (this.cvlId != undefined) {
                    this.queryValue = e.currentTarget.getAttribute("data-cvlkey");
                } else {
                    this.queryValue = e.currentTarget.id;
                }

                if (this.querySystem == "EntityType" && this.queryOperator == "Equals" && (this.queryValue != null && this.queryValue != "")) {
                    this.entityType = this.queryValue;
                    advancedSearchUtil.currentEntityType = this.entityType;
                    window.appHelper.event_bus.trigger('clearsublist', this.rowId);
                    this.$el.find("#query-plus-row").removeClass("hidden");

                    if (this.queryValue == "Specification") {
                        if (!self.$el.find("#Specifications").hasClass("hidden")) {
                            self.$el.find("#Specifications").addClass("hidden");
                        }

                        this.$el.find("#value-link").html(e.currentTarget.innerHTML);
                        return;
                    }

                    // There can only be one "EntityType" condition in a Query, so if new EntityType condition is selected
                    // also secure that the relation icon is visible again.
                    this.$el.find("#query-plus-row #Relation").removeClass("hidden");
                    
                    $.get("/api/tools/hasSpecificationLink/" + this.entityType).done(function (specificationExists) {
                        if (specificationExists) {
                            self.$el.find("#Specifications").removeClass("hidden");
                        } else {
                            if (!self.$el.find("#Specifications").hasClass("hidden")) {
                                self.$el.find("#Specifications").addClass("hidden");
                            }
                        }
                    });
                }

                // this.$el.find("#value-link").addClass("value-bold");
                this.$el.find("#value-link").html(e.currentTarget.innerHTML);
            }
        },
        onOperatorLinkSelection: function (e) {
            if (e != null) {
                e.stopPropagation();
                this.deselect();
                this.queryOperator = e.currentTarget.id;
                this.$el.find("#operator-link").html(e.currentTarget.innerHTML);
            }

            if (this.querySystem == "EntityType" && this.queryOperator == "Equals" && this.queryValue != "") {
                this.$el.find("#query-plus-row").removeClass("hidden");
            } else {
                this.$el.find("#query-plus-row").addClass("hidden");
            }
            if (this.conditionType == "Relation" && this.queryValue != "" && this.queryOperator == "NotEmpty") {
                this.$el.find("#query-relation-plus-row").first().removeClass("hidden");
            } else {
                this.$el.find("#query-relation-plus-row").first().addClass("hidden");
            }

            if (this.queryOperator == "Empty" || this.queryOperator == "NotEmpty") {
                this.$el.find("#value-link").hide();
                if (this.showLanguage == true) {
                    this.$el.find("#language-link").show();
                }
                else {
                    this.$el.find("#language-link").hide();
                }
            } else {
                if (this.showValue == true) {
                    if (this.showValue == true) {

                        //If row isDateTimeValue. Then it can show both regular datetime control and datetime interval control
                        if (this.isDateTimeValue) {

                            var elementValueLink = this.$el.find("#value-input").val();

                            //If regular datetime control has value. Don't display interval. Only until regular datetime is cleared.
                            if (this.datetimeIntervalsValue == null && elementValueLink) {
                                this.$el.find("#value-link").show();

                            } else {
                                this.$el.find("#value-link-datetime-intervals").show();
                            }

                        } else {
                            this.$el.find("#value-link").show();
                        }
                    }
                }
                if (this.showLanguage == true) {
                    this.$el.find("#language-link").show();
                }
                else {
                    this.$el.find("#language-link").hide();
                }
            }
        },
        onLanguageLinkSelection: function (e) {
            e.stopPropagation();
            this.deselect();
            this.queryLanguage = e.currentTarget.id;
            this.localeStringLanguage = this.queryLanguage;
            this.$el.find("#language-link").html(e.currentTarget.innerHTML);
        },
        onSystemLinkSelection: function (e) {
            if (e != null) {
                if (e.currentTarget.id == "filter") {
                    return;
                }
                if ($(e.currentTarget).hasClass("category") || $(e.currentTarget).hasClass("showCategory")) {
                    return;
                }

                e.stopPropagation();
                this.deselect();
            }
            var self = this;

            // Clear and reset all selections
            this.$el.find('#operator-list').empty();
            this.$el.find('#value-list').empty();
            this.$el.find('#language-list').empty();
            this.$el.find('#language-link').hide();
            this.$el.find("#value-input").datetimepicker("destroy");

            this.$el.find('#value-link-datetime-intervals').empty();
            this.$el.find('.datetime-separator').empty();

            if (this.conditionType !== "Specifications") {
                if (e) {
                    this.$el.find("#value-input").val("");
                    this.$el.find("#value-link").html("enter a value");

                    this.$el.find("#value-link").show();

                } else {
                    if (this.datetimeIntervals) {

                        //If row type is datetime and datetimeIntervals is set to true.
                        //Then we dont want to show the regular datetime value. Only the interval.
                        this.$el.find("#value-link").hide();
                        this.$el.find("#value-input").hide();
                    } else {

                        this.$el.find("#value-input").val(this.queryValue);
                        if (this.queryValue != null) {
                            this.$el.find("#value-link").html(this.queryValue);
                        } else {
                            this.$el.find("#value-link").html("enter a value");
                        }

                        this.$el.find("#value-link").show();
                    }
                }
            }

            this.$el.find("#query-plus-row").addClass("hidden");
            this.valuesList = false;

            this.showLanguage = false;
            var operatorType;

            if (e != null && this.systemQuery == undefined) {
                this.querySystem = e.currentTarget.id;
                this.$el.find("#system-link").html(e.currentTarget.innerHTML);
            }

            if (this.subview && this.isDataConditionType()) {
                var dataTypeKey = this.conditionType === "SpecificationData" ? "SpecificationDataType" : "datatype";

                var model;
                if (this.conditionType === "SpecificationData") {
                    if (this.querySystem.substring(0, 1) == "s") {
                        model = this.collection.get(this.querySystem.substring(1));
                    } else {
                        model = this.collection.get(this.querySystem);
                    }

                } else {
                    model = this.collection.get(this.querySystem);
                }

                var cvlIdKey = this.conditionType === "SpecificationData" ? "CVLId" : "cvlid";
                this.cvlId = model.get(cvlIdKey);
                if (this.cvlId) {
                    var multivalueKey = this.conditionType === "SpecificationData" ? "Multivalue" : "multivalue";
                    this.isMultivalue = model.get(multivalueKey);
                }

                if (model.get(dataTypeKey) !== "LocaleString") {
                    this.queryLanguage = null;
                }

                operatorType = this.setValueType(model.get(dataTypeKey));
                this.setOperatorsList(advancedSearchUtil.setOperators(operatorType));
                return;
            }

            if (this.subview && this.conditionType == "Completeness") {
                this.$el.find("#value-link").hide();
                if (this.queryOperator == "Equal" || this.queryOperator == "Equals") {
                    self.queryOperator = "IsTrue";
                }

                if (this.querySystem.substring(0, 1) == "g") {
                    this.completenessGroup = parseInt(this.querySystem.substring(1));
                    this.completenessRule = undefined;
                } else if (this.querySystem.substring(0, 1) == "r") {
                    var splitter = this.querySystem.split("_");
                    this.completenessGroup = undefined;
                    this.completenessRule = parseInt(splitter[1]);
                } else {
                    this.completenessDefintition = this.querySystem;
                    this.completenessGroup = undefined;
                    this.completenessRule = undefined;
                }

                this.setOperatorsList(advancedSearchUtil.setOperators("setCompleteness"));
                return;
            }

            switch (this.conditionType) {
                case "Relation":
                    if (self.queryOperator === "Equals") {
                        self.queryOperator = "NotEmpty";
                    }
                    this.setOperatorsList(advancedSearchUtil.setOperators("setExisting"));

                    this.relationType = this.querySystem;
                    advancedSearchUtil.currentRelationEntityType = this.relationType;
                    if (e != null) {
                        window.appHelper.event_bus.trigger('clearsublist', this.rowId);
                    }

                    var linkedentitytype = _.find(self.inboundcollection.models, function (resultModel) {
                        return "i" + resultModel.attributes.Id == self.relationType;
                    });
                    if (linkedentitytype != null) {
                        this.relationEntityType = linkedentitytype.attributes.SourceEntityTypeId;
                    } else {
                        linkedentitytype = _.find(self.outboundcollection.models, function (resultModel) {
                            return "o" + resultModel.attributes.Id == self.relationType;
                        });
                        if (linkedentitytype != null) {
                            this.relationEntityType = linkedentitytype.attributes.TargetEntityTypeId;
                        }
                    }
                    if (this.queryOperator == "NotEmpty") {
                        this.$el.find("#query-relation-plus-row").first().removeClass("hidden");
                    } else {
                        this.$el.find("#query-relation-plus-row").first().addClass("hidden");
                    }
                    break;
                case "Specifications":
                    if (e != null) {
                        window.appHelper.event_bus.trigger('clearsublist', this.rowId);
                    }
                    this.$el.find("#query-specification-plus-row").first().removeClass("hidden");
                    break;
                default:
                    var setup = advancedSearchUtil.setupOperatorValuesForSystemMenu(this.querySystem);
                    setup = setup.split(',');
                    operatorType = setup[0];
                    var valueType = setup[1];
                    if (operatorType != undefined && valueType != undefined) {
                        var operators = advancedSearchUtil.setOperators(operatorType);
                        this.setOperatorsList(operators);
                        this.setValueType(valueType);
                    }
            }
        },
        setValueType: function (valueType) {
            var self = this;
            this.isIntValue = false;
            this.isBoolValue = false;
            this.isDateTimeValue = false;
            var operatorType;
            var values = {};
            var valueObjects = [];

            switch (valueType) {
                case "String":
                    operatorType = "set6";
                    return operatorType;
                case "Double":
                case "Xml":
                case "File":
                    operatorType = "set3";
                    return operatorType;
                case "LocaleString":
                    var langCollection = new languagesCollection();
                    langCollection.fetch({
                        success: function (collection) {
                            _.each(collection.toJSON(), function (model) {
                                values[model.Name] = model.DisplayName;
                            });
                            self.setLanguagesList(values);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    operatorType = "set6";
                    return operatorType;
                case "Boolean":
                    this.isBoolValue = true;
                    operatorType = "setBoolean";
                    this.$el.find("#operator-link").text("True");
                    this.$el.find("#value-link").hide();
                    this.showValue = false;
                    this.queryValue = null;

                    return operatorType;
                case "Integer":
                case "CompletenessValue":
                    this.isIntValue = true;
                    operatorType = "set5";
                    return operatorType;
                case "DateTime":
                    this.$el.find("#value-input").datetimepicker({
                        format: appHelper.momentDateTimeFormat,
                        formatTime: appHelper.momentTimeFormat,
                        formatDate: appHelper.momentDateFormat,
                        lang: 'en',
                        weeks: window.appHelper.showWeeksInDatePicker,
                        dayOfWeekStart: window.appHelper.firstDayOfWeek
                    });


                    var parentElement = this.$el.find("#value-input")[0].parentElement;
                    $(parentElement).after(_.template(datetimeIntervalsValueInputTemplate, {}));

                    if (this.datetimeIntervalsValue) {
                        //Populate value-link-datetime-intervals with the correct value and not enter an interval
                        this.$el.find("#" + this.datetimeIntervalsValue).addClass('selected');
                        this.$el.find("#value-link-datetime-intervals").show();

                        this.queryValue = "NOW + " + this.$el.find("#" + this.datetimeIntervalsValue)[0].innerHTML;
                        
                        this.$el.find("#value-link-datetime-intervals").html("");
                        this.$el.find("#value-link-datetime-intervals").html(this.queryValue);
                        this.$el.find("#value-link-datetime-intervals").val(this.queryValue);

                        //Because there is a value for interval datetime. We hide the regular datetime control.
                        this.$el.find("#value-link").hide();
                        this.$el.find("#value-input").hide();
                        this.$el.find(".datetime-separator").hide();

                    } else {
                        //If regular datetime control doesn't have value. Show interval also.
                        if (!this.queryValue && this.conditionType != "System") {
                            this.$el.find("#value-link-datetime-intervals").html("enter an interval");
                        } else {
                            this.$el.find(".datetime-separator").hide();
                        }

                        this.datetimeIntervals = false;
                    }

                    this.isDateTimeValue = true;

                    operatorType = "set5";
                    return operatorType;
                case "CVL":

                    this.isCVLValue = true;

                    var cvlValCollection = new cvlValueCollection([], { cvlId: self.cvlId });
                    cvlValCollection.fetch({
                        success: function (collection) {
                            _.each(collection.toJSON(), function (model) {
                                valueObjects.push({ key: model.Key, value: model.DisplayValue });                              
                            });
                            self.setValuesList(valueObjects);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    if (this.isMultivalue) {
                        operatorType = "multiCVL";
                        if (this.queryOperator === "Equals") {
                            this.$el.find("#operator-link").text("Contains All selected");
                            this.queryOperator = "ContainsAll";
                        }
                    } else {
                        operatorType = "set4";
                    }
                    return operatorType;
                case "Channels":
                    this.collection = new channelCollection();
                    this.collection.fetch({
                        success: function () {
                            _.each(self.collection.toJSON(), function (model) {
                                valueObjects.push({ key: model.Id, value: model.DisplayName });
                            });
                            self.setValuesList(valueObjects);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
                case "EntityTypes":
                    this.collection = new entityTypeCollection();
                    this.collection.fetch({
                        success: function () {
                            _.each(self.collection.toJSON(), function (model) {
                                valueObjects.push({ key: model.Id, value: model.DisplayName });
                            });
                            self.setValuesList(valueObjects);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
                case "Users":
                    this.collection = new usersCollection();
                    this.collection.fetch({
                        success: function () {
                            _.each(self.collection.toJSON(), function (model) {

                                var name = "";

                                if (model.FirstName != null && model.FirstName != "") {
                                    name = model.FirstName;
                                }

                                if (model.LastName != null && model.LastName) {
                                    name = name + " " + model.LastName;
                                }

                                if (name == "") {
                                    name = "(" + model.Username + ")";
                                } else {
                                    name = name + " (" + model.Username + ")";
                                }

                                valueObjects.push({ key: model.Username, value: name });
                            });
                            self.setValuesList(valueObjects);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
                case "Fieldsets":
                    this.collection = new fieldsetCollection();
                    this.collection.fetch({
                        success: function () {
                            _.each(self.collection.toJSON(), function (model) {
                                valueObjects.push({ key: model.Id, value: model.Name.stringMap[window.appHelper.userLanguage] });
                            });
                            self.setValuesList(valueObjects);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
            }

            return "";
        },
        setSystemList: function (list) {
            var self = this;
            if (list != undefined) {
                var i = 1;
                if (this.isDataConditionType() || self.conditionType === "Specifications") {

                    var filter = "<input id=\"filter\" class=\"filter\" placeholder=\"Filter\" type=\"text\" /><i class=\"fa fa-filter\" id=\"workareasearch\"></i>";

                    self.$el.find('#system-list')
                        .append($('<div>', { id: "filter" })
                            .html(filter));


                    var currentCategory = "";
                    var categorycount = 0;
                    var section = $().html("");
                    _.each(list, function (value, key) {

                        var querySystem = self.querySystem;

                        if (self.conditionType === "SpecificationData") {
                            if (key.substring(0, 1) != "s") {
                                key = "s" + key;
                            }

                            if (querySystem != null && querySystem.substring(0, 1) != "s") {
                                querySystem = "s" + querySystem;
                            }
                        }

                        if (querySystem != null) {
                            if (key == querySystem) {
                                self.$el.find("#system-link").html(value.field);
                                self.onSystemLinkSelection(null);
                            }
                        } else if (i == 1) {
                            self.$el.find("#system-link").html(value.field);
                            self.querySystem = key;
                            self.onSystemLinkSelection(null);
                        }

                        if (currentCategory != value.categoryName) {

                            if (categorycount > 0) {
                                self.$el.find('#Category' + categorycount + " #fields").append(section);
                            }
                            categorycount++;
                            var html = "<div class=\"showCategory\" id=\"" + "Category" + categorycount + "-show\" style=\"display:inline\">" +
                                "<i class=\"fa fa-minus-circle\"></i><i class=\"fa fa-plus-circle\" style=\"display:none\"></i>" + value.categoryName + "</div><div id=\"fields\"></div>";

                            self.$el.find('#system-list')
                                .append($('<div>', { id: "Category" + categorycount, "class": "category" })
                                    .html(html));

                            currentCategory = value.categoryName;
                            section = self.$el.find('#Category' + categorycount + " #fields");
                        }

                        section.append($('<div>', { id: key, "class": "item" })
                                .text(value.field));

                        i++;
                    });

                    if (i < 15) {
                        self.$el.find('div#filter').hide();
                    }
                } else {
                    _.each(list, function (value, key) {
                        if (key === "CompletenessValue" && !self.completenessExists) {
                            return;
                        }

                        if (self.querySystem != null) {

                            if (self.conditionType === "Completeness") {
                                var washedKey;

                                if (key != self.querySystem && key.substring(0, 1) == "g") {
                                    washedKey = key.substring(1);
                                }

                                if (key != self.querySystem && key.substring(0, 1) == "r") {
                                    var splitter = key.split("_");
                                    washedKey = splitter[1];
                                }

                                if (washedKey == undefined) {
                                    washedKey = key;
                                }

                                if (washedKey == self.querySystem) {
                                    self.$el.find("#system-link").html(value);
                                    self.querySystem = key;
                                    self.onSystemLinkSelection(null);
                                }

                            } else {
                                if (key == self.querySystem) {
                                    self.$el.find("#system-link").html(value);
                                    self.onSystemLinkSelection(null);
                                }
                            }

                        } else if (i == 1) {
                            self.$el.find("#system-link").html(value);
                            self.querySystem = key;
                            self.onSystemLinkSelection(null);
                        }

                        self.$el.find('#system-list')
                            .append($('<div>', { id: key, "class": "item" })
                                .html(value));

                        i++;
                    });
                }
            }
        },
        setOperatorsList: function (operators) {
            var self = this;
            this.$el.find('#operator-list').html("");
            this.$el.find('#operator-link').html("");

            if (operators != undefined && !$.isEmptyObject(operators)) {
                if (!_.has(operators, this.queryOperator)) {
                    this.queryOperator = Object.keys(operators)[0];
                }

                this.$el.find("#operator-link").html(operators[this.queryOperator]);
                this.onOperatorLinkSelection(null);

                _.each(operators, function (value, key) {
                    self.$el.find('#operator-list')
                        .append($('<li>', { id: key })
                            .text(value));
                });
            } else {
                self.$el.find('#operator-link').hide();
            }
        },
        setValuesList: function (values) {
            var self = this;
            if (values != undefined) {
                this.valuesList = true;

                if (this.predefinedEntityType == undefined) {
                    this.$el.find("#value-link").html("select value");
                } else {
                    this.$el.find("#value-link").html(this.predefinedEntityType);
                    this.queryValue = this.predefinedEntityType;
                }

                self.$el.find('#value-list').empty();

                _.each(values, function (model) {
                    var key = model.key;
                    var value = model.value;
                    var checked = "";
                    if (self.queryValue != null && _.indexOf(self.queryValue.split(";"), key) > -1) {
                        //self.$el.find("#value-link").append(value);
                        checked = " checked=\"checked\"";
                    }

                    if (self.isCVLValue) {

                        if (self.cvlId == undefined) {
                            self.$el.find('#value-list').append("<div id=\"" + key + "\" class=\"item\"><input type=\"checkbox\" name=\"" + self.querySystem + "\" id=\"" + key + "\" value=\"" + value + "\"" + checked + "/>" + value + "</div>");
                        } else {

                            element = $('<div>', { "class": "item" }).attr('data-cvlkey', key).text(value).append("<input type=\"checkbox\" name=\"" + self.querySystem + "\" value=\"" + value + "\"" + checked + "/>");
                            self.$el.find('#value-list').append(element);
                        }

                    } else {
                        var element;

                        if (self.cvlId == undefined) {
                            if (value === "") {
                                value = "[" + key + "]";
                            }
                            element = $('<div>', { id: key, "class": "item" }).text(value);
                        } else {
                            if (value === "") {
                                value = "[" + key + "]";
                            }

                            element = $('<div>', { "class": "item" }).attr('data-cvlkey', key).text(value);
                        }

                        self.$el.find('#value-list').append(element);
                    }

                    if (self.queryValue != null && self.queryValue == key) {
                        self.$el.find("#value-link").html(value);
                    }
                });

                if (this.querySystem == "EntityType" && this.queryOperator == "Equals" && (this.queryValue != "" && this.queryValue != null)) {
                    this.entityType = this.queryValue;
                    advancedSearchUtil.currentEntityType = this.entityType;
                    this.$el.find("#query-plus-row").removeClass("hidden");
                }

                if (this.queryValue != undefined && this.isCVLValue) {
                    var i = this.$el.find("#value-list input:checked");

                    var innerValues = "";
                    $.each(i, function (index, value) {
                        innerValues += value.value + ",";
                    });
                    if (innerValues.length > 0) {
                        innerValues = innerValues.substring(0, innerValues.length - 1);
                    }

                    if (innerValues != "") {
                        this.$el.find("#value-link").html(innerValues);
                    }
                }
            }
        },

        setLanguagesList: function (values) {
            var self = this;
            if (values != undefined) {
                _.each(values, function (value, key) {
                    self.$el.find('#language-list').append($('<li>', { id: key }).text(value));
                });

                if (this.localeStringLanguage) {
                    this.queryLanguage = this.localeStringLanguage;
                }
                else {
                    if (!this.queryLanguage || !_.has(values, this.queryLanguage)) {
                        this.queryLanguage = window.appHelper.userLanguage && _.has(values, window.appHelper.userLanguage) ? window.appHelper.userLanguage : _.keys(values)[0];
                        this.localeStringLanguage = this.queryLanguage
                    }
                }
                self.$el.find("#language-link").html(values[this.queryLanguage]);
                this.$el.find("#language-link").show();
                this.showLanguage = true;
            }
        },
        onSpecificationDataLoaded: function () {
            if (this.categoryCollectionLoaded && this.collectionLoaded) {
                var self = this;
                var values = {};
                _.each(_.sortBy(self.collection.toJSON(), "DisplayName"), function (field) {

                    if (field != null) {
                        values["s" + field.Id] = { field: field.DisplayName, categoryName: "General" };
                    }
                });
                self.setSystemList(values);
            }
        },
        setSubSystemList: function () {
            var self = this;
            var values = {};

            switch (this.conditionType) {
                case "Data":
                case "RelationData":
                    this.collection = new fieldTypeCollection();
                    this.collection.fetch({
                        async: false,
                        data: { id: self.entityType },
                        success: function () {
                            _.each(self.collection.toJSON(), function (model) {
                                var obj = { field: model.displayname, categoryName: model.categoryname }
                                values[model.id] = obj;
                            });

                            self.setSystemList(values);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    break;
                case "SpecificationData":
                    this.categoryCollection = new backbone.Collection(null, { url: "/api/specificationtemplate/category", model: backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("DisplayName"); } }) });
                    this.collection = new backbone.Collection(null, { url: "/api/specificationtemplate/" + this.entityId + "/field", model: backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });
                    self.categoryCollectionLoaded = false;
                    self.collectionLoaded = false;
                    this.categoryCollection.fetch({
                        async: false,
                        success: function () {
                            self.categoryCollectionLoaded = true;
                            self.onSpecificationDataLoaded();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    this.collection.fetch({
                        async: false,
                        success: function () {
                            self.collectionLoaded = true;
                            self.onSpecificationDataLoaded();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
                case "Relation":
                    var found = 0;
                    this.outboundcollection = new relationTypeCollection([], { direction: "outbound", entityTypeId: self.entityType });
                    this.outboundcollection.fetch({
                        async: false,
                        success: function () {
                            _.each(self.outboundcollection.toJSON(), function (model) {
                                values["o" + model.Id] = model.Name;
                            });
                            found++;
                            if (found == 2) {
                                self.showValue = false;
                                self.setSystemList(values);
                                self.setOperatorsList(advancedSearchUtil.setOperators("setExisting"));
                                self.$el.find("#value-link").hide();
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    this.inboundcollection = new relationTypeCollection([], { direction: "inbound", entityTypeId: self.entityType });
                    this.inboundcollection.fetch({
                        async: false,
                        success: function () {
                            _.each(self.inboundcollection.toJSON(), function (model) {
                                values["i" + model.Id] = model.Name;
                            });
                            found++;
                            if (found == 2) {
                                self.showValue = false;
                                self.setSystemList(values);
                                self.setOperatorsList(advancedSearchUtil.setOperators("setExisting"));
                                self.$el.find("#value-link").hide();
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    break;

                case "Completeness":
                    this.collection = new completenessDefinitionCollection(null, { entitytype: self.entityType });
                    this.collection.fetch({
                        url: "/api/completeness/definition/entitytype/" + self.entityType,
                        async: false,
                        success: function () {
                            if (self.collection.length > 0) {
                                var model = self.collection.models[0];
                                self.completenessDefinition = model.attributes.Id;
                                _.each(model.toJSON().Groups, function (group) {
                                    values["g" + group.Id] = "<i class='fa fa-share-alt' style='margin-right:5px;'></i>" + group.DisplayName;

                                    _.each(group.Rules, function (rule) {
                                        values["r" + group.Id + "_" + rule.Id] = "<i class='fa fa-check' style='margin-left: 5px; margin-right:5px;'></i>" + rule.DisplayName;
                                    });
                                });
                                self.showValue = false;
                                self.setSystemList(values);
                                self.setOperatorsList(advancedSearchUtil.setOperators("setCompleteness"));
                                self.$el.find("#value-link").hide();
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    break;
                case "Specifications":
                    this.collection = new entityCollection();
                    this.collection.fetch({
                        url: "/api/entity?entityType=Specification",
                        async: false,
                        success: function (model, response) {
                            _.each(response, function (specificationTemplate) {
                                values[specificationTemplate.Id] = { field: specificationTemplate.DisplayName };
                            });

                            self.showValue = false;
                            self.setSystemList(values);
                            self.$el.find("#value-link").hide();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                    break;
            }
        },
        validate: function () {
            this.$el.find("#" + this.rowId + " #row-error").html("");
            this.$el.find("#" + this.rowId).removeClass("error");


            if (this.showValue && (!(this.queryOperator == "Empty" || this.queryOperator == "NotEmpty"))) {
                var x = this.queryValue != null && this.queryValue != "";
                if (x != true) {
                    this.$el.find("#" + this.rowId + " #row-error").html("value is missing! ");
                    this.$el.find("#" + this.rowId).addClass("error");

                }

                return x;
            }


            return true;
        },
        onSave: function () {
        },
        render: function () {
            this.$el.html(_.template(queryRowTemplate, { rowId: this.rowId }));

            this.$el.find(".search-criteria-condition").hide();

            if (this.subview) {
                this.setSubSystemList();
                this.$el.find(".query-row").addClass("sub-row");
            } else {

                var list = advancedSearchUtil.getMainSystemList();
                list = this.parent.cleanSystemList(list);
                if (!Object.keys(list).length) {
                    this.$el.html("");
                    return this;
                }

                this.setSystemList(list);
                this.$el.find("#value-link").html(this.queryValue);
            }

            this.$el.find("#system-list").hide();
            this.$el.find("#operator-list").hide();
            this.$el.find("#language-list").hide();
            this.$el.find("#value-list").hide();

            this.$el.find("#value-input").hide();


            if (this.queryOperator == "Empty" || this.queryOperator == "NotEmpty") {
                this.$el.find("#value-link").hide();
                if (this.showLanguage == true) {
                    this.$el.find("#language-link").hide();
                }
            } else {
                if (this.showValue == true) {
                    this.$el.find("#value-link").show();
                }
                if (this.showLanguage == true) {
                    this.$el.find("#language-link").show();
                }
            }

            return this;
        },
        isDataConditionType: function () {
            return _.contains(["Data", "RelationData", "SpecificationData"], this.conditionType);
        }
    });

    return queryRowView;
});
