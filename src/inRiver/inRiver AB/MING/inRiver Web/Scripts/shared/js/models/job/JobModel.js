define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var JobModel = Backbone.Model.extend({
        idAttribute: "Job",
        urlRoot: '/api/job',
        initialize: function () {

        },
        schema: { // Used by backbone forms extension
            Job: {type: 'Text', editorAttrs: { disabled: true }, title: 'Id' },
            Name: { type: 'Text', validators: ['required'], editorAttrs: { disabled: true } },
            Interval: {
                type: 'Text',
                title: 'Interval',
                validators: [
                    {
                        type: 'regexp',
                        regexp: '^[1-9]\\d*$',
                        message: 'Most be a positive number'
                    }
                ]
            },
            Enabled: { type: 'Checkbox', title: 'Enabled'}
        }
    });

    return JobModel;

});
