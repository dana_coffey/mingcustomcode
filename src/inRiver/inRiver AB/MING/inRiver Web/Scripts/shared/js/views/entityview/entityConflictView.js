﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
  'sharedjs/models/field/fieldModel',
  'text!sharedtemplates/entity/entityConflictTemplate.html',
  'text!sharedtemplates/entity/fieldConflictTemplate.html',
  'text!sharedtemplates/entity/fieldLocaleStringConflictTemplate.html',
  'sharedjs/misc/inRiverUtil'

], function ($, _, Backbone, alertify, fieldModel, entityConflictTemplate, fieldConflictTemplate, fieldLocaleStringConflictTemplate, inRiverUtil) {

    var entityConflictView = Backbone.View.extend({
        initialize: function (options) {
            this.entityModel = options.model;
            this.validationResult = options.validationResult;
            this.parent = options.parent;
            this.selectedValue = false;

            this.render();
        },
        events: {
            "click #view-history-button": "onViewHistoryClick",
            "click #conflict-cancel-button": "onCancel",
            "click #conflict-save-button": "onSave",
            "click i.conflict-select": "onValueSelect",
            "click span.conflict-select": "onValueSelect"
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            $("#conflictContainer").html("");
            this.remove();
        },
        onValueSelect: function (e) {
            var i = e.currentTarget.id;

            // local eller server
            var selected = "local";
            var unselected = "server";
            if (i.indexOf("server") > -1) {
                selected = "server";
                unselected = "local";
            }

            var id = i.substring(i.indexOf(selected) + selected.length + 1);

            if (id.indexOf("ls-") > -1) {
                id = id.substring(3);
            }

            //kopiera värde till selected

            $("#selected-" + id).text($("#" + selected + "-" + id).text());
            $("#" + selected + "-" + id).addClass("selected");
            $("#" + unselected + "-" + id).removeClass("selected");

            this.selectedValue = true;

            //Om LS:
            if ($("#selected-ls" + id)) {

                $("#selected-ls-" + id).html($("#" + selected + "-ls-" + id).html());
                $("#" + selected + "-ls-" + id).addClass("selected");
                $("#" + unselected + "-ls-" + id).removeClass("selected");
            }
        },
        onSave: function () {
            var that = this;

            if (!this.selectedValue) {
                inRiverUtil.NotifyError("No value selected", "Please select either <b>local</b> or <b>server</b> value before save");
                return;
            }
            
            $("span.fieldconflict-selectedvalue").each(function (index, value) {

                var currentVal = value.innerText;
                var id = value.id.substring(value.id.indexOf("-") + 1);
                var field = _.findWhere(that.entityModel.get("Fields"), { FieldType: id });

                var localeString = $("#selected-ls-" + id);

                if (localeString.length > 0) {
                    field.Value = JSON.parse(currentVal);
                } else {
                    field.Value = currentVal;
                }

                field.Revision = 0;

            });

            this.parent.onConflictSave();
            this.done();
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(entityConflictTemplate, { data: this.entityModel.toJSON() }));

            var count = this.validationResult.ConflictedFields.length;
            var i = 0;

            var container = this.$el.find("#conflictbody");
            var local = this.validationResult.ConflictedFields;
            var server = this.validationResult.ServerFields;

            for (i = 0; i < count; i++) {

                if (local[i].FieldDataType != "LocaleString") {

                    container.append(_.template(fieldConflictTemplate,
                        {
                            fieldtype: local[i].FieldType,
                            localvalue: local[i].Value,
                            servervalue: server[i].Value,
                            fieldtypedisplayname: local[i].FieldTypeDisplayName
                        }
                    ));
                } else {
                    container.append(_.template(fieldLocaleStringConflictTemplate,
                       {
                           fieldtype: local[i].FieldType,
                           localvalue: local[i].Value,
                           servervalue: server[i].Value,
                           fieldtypedisplayname: local[i].FieldTypeDisplayName
                       }
                   ));
                }
            }

            return this; // enable chained calls
        }
    });

    return entityConflictView;
});
