define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'jquery-ui',
  'text!templates/start/startPageTemplate.html',
  'views/start/startPageConnectorListView',
  'views/start/startPageLatestEventListView'
], function ($, _, backbone, backboneforms, jqueryui, startPageTemplate, startPageConnectorListView, startPageLatestEventListView) {

    var startSupplyPage = backbone.View.extend({
        initialize: function () {
            var self = this;
            self.render();
            this.done = 0; 
        },
        //onDone: function () {

        //    this.done++;
        //    if (this.done < 2) {
        //        return; 
        //    }
        //    var mainAreaElement = $('body');
        //    var areaHeight = mainAreaElement.innerHeight() - 80;
        //    $("main-container").height(areaHeight);
        //    this.$el.find("#start-main-container").height(areaHeight);
        //},
        render: function () {
            var startPage = this.$el.html(startPageTemplate);
            var connectorList = new startPageConnectorListView();
            startPage.find("#list-of-connectors").append(connectorList.$el);
            var latestEventsList = new startPageLatestEventListView();
            startPage.find("#latest-events-from-connectors").append(latestEventsList.$el);

            //this.listenTo(window.appHelper.event_bus, 'viewdone', this.onDone);
        }
    });

    return startSupplyPage;
});

