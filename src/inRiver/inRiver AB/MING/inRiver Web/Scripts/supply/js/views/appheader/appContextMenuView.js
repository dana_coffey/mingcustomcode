﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'alertify',
  'sharedjs/misc/permissionUtil',
  'sharedjs/views/create/connectorCreateView',
  'text!templates/shared/appContextMenuTemplate.html'
], function ($, _, Backbone, modalPopup, alertify, permissionUtil,connectorCreateView, appContextMenuTemplate) {

    var appContextMenuView = Backbone.View.extend({
        initialize: function (options) {
            this.undelegateEvents();
            this.listenTo(appHelper.event_bus, 'connectoradd', this.render);
            this.render();
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
        },
        deselect: function (e) {
            if (e.target.id == "connector-name") {
                return;
            }
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle(200);
            $(document).one('click', this.deselect.bind(this));
        },
        render: function () {
            this.$el.html(appContextMenuTemplate);
            if (permissionUtil.CheckPermission("AddDeleteConnector") == true) {
                this.$el.find("#create-connector-container").html(new connectorCreateView({ inbounds: true, outbounds: false, contextmenu: this, startPage: "supply" }).$el);

                this.$el.find("#app-tools-container").addClass("add-connector"); 
            } else {
                this.$el.find(".app-tools").hide();
            }
            return this;
        }
    });

    return appContextMenuView;

});
