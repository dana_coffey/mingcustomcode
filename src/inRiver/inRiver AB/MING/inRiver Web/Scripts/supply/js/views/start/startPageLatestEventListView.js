define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'sharedjs/misc/inRiverUtil',
  'text!templates/start/startPageLatestEventListTemplate.html',
  'views/connectorevents/connectorEventView',
  'sharedjs/collections/connectorevents/latestConnectorEventCollection'
], function ($, _, backbone, jqueryui,inRiverUtil, startPageLatestEventListTemplate, connectorEventView, latestConnectorEventCollection) {

    var startPageLatestEventList = backbone.View.extend({
        initialize: function () {
            this.undelegateEvents();
            this.fetchEvents();

            this.listenTo(appHelper.event_bus, 'connectorupdated', this.connectorUpdated);
            this.listenTo(appHelper.event_bus, 'connectoradd', this.connectorUpdated);
            this.listenTo(appHelper.event_bus, 'connectordeleted', this.connectorDeleted);

        },
        connectorUpdated: function () {
            this.fetchEvents();
        },
        connectorDeleted: function () {
            this.fetchEvents();
        },
        fetchEvents: function () {
            var self = this;
            this.latestEventsCollection = new latestConnectorEventCollection();
            this.latestEventsCollection.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                data: $.param({ maxNumberOfEvents: "5", inbounds: "true", outbounds: "false" })
            });
        },
        render: function () {
            var channellist = this.$el.html(startPageLatestEventListTemplate);
            var models = this.latestEventsCollection.models;
            if (models.length == 0) {
                this.$el.find("#latest-event-list").html("<div class='no-items-text'>No events</div>");
            } else {
                _.each(models, function (model) {
                    var eventView = new connectorEventView({ model: model });
                    channellist.find('#latest-event-list').append(eventView.render().el);
                });
            }

            //window.appHelper.event_bus.trigger('viewdone');
            return this;
        }
    });

    return startPageLatestEventList;
});

