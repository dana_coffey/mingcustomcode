﻿define([
    'jquery',
    'underscore',
    'backbone',
    'fabric',
    'sharedjs/models/completeness/completenessModel',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/models/field/fieldModel',
    'sharedjs/models/resource/resourceModel',
    'modalPopup',
    'views/entityview/entityView',
    'sharedjs/views/entityview/entityDetailSettingView',
    'sharedjs/views/completeness/completenessPopupView',
    'sharedjs/views/lightboard/lightboardPreviewDetail',
    'sharedjs/misc/inRiverUtil',
    'text!templates/merchandising/merchandisingDrawingPopupTemplate.html',
    'text!sharedtemplates/entitycard/workareaCardTemplate.html',
    'text!templates/shared/resourceDownloadPopup.html'
], function ($, _, Backbone, fabric, completenessModel, relationModel, entityModel, fieldModel, resourceModel, modalPopup, entityView, entityDetailSettingView, completenessPopupView, lightboardPreviewDetail, inRiverUtil, merchandisingDrawingPopupTemplate, workareaCardTemplate, resourceDownloadPopup) {

    var merchandisingDrawingPopupView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function(options) {
            this.resourceFileId = options.resourceFileId;
            this.pendingAction = options.pendingAction;
            this.drawingState = {};

            this.render();
        },
        events: {
            "click .lightboard-card-wrap": "onCardClick",
        },
        render: function () {
            var that = this;

            var text1 = "", text2 = "";
            if (this.pendingAction == "add-spot") {
                text1 = "Create Spot";
                text2 = "Click somewhere in the image to create your new spot.";
            } else if (this.pendingAction == "add-polygon") {
                text1 = "Create Area";
                text2 = "Create an area by clicking multiple times in the image to create your shape.";
            }

            this.$el.html(_.template(merchandisingDrawingPopupTemplate, { text1: text1, text2: text2 }));

            var imgEl = this.$el.find("#merchandising-drawing-image");
            imgEl.load(function () {
                var width = $(this).width();
                var height = $(this).height();
                that.$el.find("#merchandising-drawing-fabric").attr("width", width).attr("height", height);
                that.$el.find("#merchandising-drawing-image-and-fabric-wrapper").width(width).height(height);

                fabricHandler.init(that.$el.find("#merchandising-drawing-fabric")[0], that.drawingState);
                fabricHandler.state.pendingAction = that.pendingAction;
                fabricHandler.state.width = width;
                fabricHandler.state.height = height;
            });
            imgEl.attr("src", "../picture?id=" + this.resourceFileId + "&size=Preview");
            
            return this; // enable chained calls
        }
    });
    
    var fabricHandler = {
        init: function (htmlElement, state) {
            var that = this;
            this.canvas = new fabric.Canvas(htmlElement);
            this.state = state;

            this.resetState();

            this.canvas.on('mouse:down', function (options) {
                if (that.state.pendingAction == "add-spot") {
                    that.addSpot(options.e.offsetX, options.e.offsetY);
                }
                else if (that.state.pendingAction == "add-polygon") {
                    that.addVertex(options.e.offsetX, options.e.offsetY);
                }
            });
        },
        resetState: function () {
            this.canvas.clear();
            this.state.width = null;
            this.state.height = null;
            this.state.pendingAction = null;
            this.state.pendingVertices = null;
            this.state.pendingSpot = null;
        },
        addSpot: function (x, y) {
            this.canvas.clear().renderAll();
            this.state.pendingSpot = { x: x, y: y };

            var radius = 10;
            var circle = new fabric.Circle({
                radius: radius,
                stroke: "#fff",
                strokeWidth: 2,
                fill: "#f00",
                opacity: 0.5,
                left: x - radius,
                top: y - radius,
                lockRotation: true,
                lockScalingX: true,
                lockScalingY: true,
                hasControls: false,
                selectable: false,
            });

            this.canvas.add(circle);
            this.canvas.renderAll();
        },
        addVertex: function (x, y) {
            var that = this;

            if (!this.state.pendingVertices) this.state.pendingVertices = [];
            this.state.pendingVertices.push({ x: x, y: y });

            // remove all objects and re-render
            this.canvas.clear();

            var polygon = new fabric.Polygon($.extend(true, [], this.state.pendingVertices), {
                stroke: "#000000",
                strokeWidth: 1,
                fill: "#f00",
                opacity: 0.3,
                selectable: false,
            }, false);
            this.canvas.add(polygon);

            _.each(this.state.pendingVertices, function (p) {
                that.canvas.add(new fabric.Circle({
                    selectable: false,
                    radius: 4,
                    fill: '#fff',
                    stroke: "#000",
                    strokeWidth: 1,
                    top: p.y - 4,
                    left: p.x - 4
                }));
            });

            this.canvas.renderAll();
        }
    };


    return merchandisingDrawingPopupView;
});
