﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-steps',
  'dropzone',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/languages/LanguagesCollection',
  'collections/import/fileImportConfigurationMappingCollection',
  'models/import/fileImportConfigurationModel',
  'models/import/fileImportConfigurationMappingModel',
  'models/import/fileImportValidationModel',
  'models/import/fileImportEntityModel',
  'views/appheader/validateFieldsView',
  'text!templates/import/importDataTemplate.html',
  'text!templates/import/importConfigurationTemplate.html',
  'text!templates/import/importValidationTemplate.html',
  'text!templates/import/importActionTemplate.html'
], function ($, _, backbone, jquerySteps, Dropzone, inRiverUtil, languagesCollection, fileImportConfigurationMappingCollection, fileImportConfigurationModel, fileImportConfigurationMappingModel, fileImportValidationModel, fileImportEntityModel, validateFieldsView, importDataTemplate, importConfigurationTemplate, importValidationTemplate, importActionTemplate) {

    var importDataView = backbone.View.extend({
        initialize: function (options) {
            window.appHelper.languagesCollection = new languagesCollection();
            window.appHelper.languagesCollection.fetch();

            //load all saved mappings.
            this.fileImportConfigurationMappings = new fileImportConfigurationMappingCollection();
            this.fileImportConfigurationMappings.fetch({});

            this.render();
        },
        events: {
            "click #showEmptyColumns": "onToggleShowEmptyColumns",
            "click #clearEmptyValue": "onToggleClearEmptyValues",
            "change #selectEntityType": "onSelectEntityType",
            "change .import-mapping-language": "onChangeLanguage",
            "change .import-mapping-fields": "onChangeField",
            "click #import-validate-button": "onValidateData",
            "click #import-action-button": "onImportData",
            "click #save-mapping": "showSaveMappingDialog",
            "click #do-save-mapping": "onSaveMapping",
            "click #do-cancel-mapping": "onCancelSaveMapping",
            "click #update-mapping": "onUpdateMapping",
            "click #delete-mapping": "onDeleteMapping",
            "change #mapping": "onSelectMapping"
        },
        onToggleShowEmptyColumns: function () {
            var self = this;

            this.fileImportConfigurationModel.set("ShowEmptyColumns", this.$el.find("#showEmptyColumns")[0].checked);

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onChangeLanguage: function (e) {
            var newLanguage = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;

            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id == columnId;
            });

            column.LanguageCode = newLanguage;
            this.fileImportConfigurationModel.save();
        },
        onValidateData: function (e) {
            this.$el.find("#validation-spinner").show();
            this.$el.find("#validation-content").hide();

            this.fetchEntityModel(0);
        },
        onValidateComplete: function () {
            var self = this;

            this.$el.find("#validation-spinner").hide();
            this.$el.find("#validation-content").show();

            this.fileImportValidationModel.set("IsValidated", true);

            //this.$el.find("#import-validation").html(_.template(importValidationTemplate, { data: self.fileImportValidationModel.toJSON() }));

            this.$el.find("#import-validation").html(new validateFieldsView({ parent: this, model: self.fileImportValidationModel }).el);

            //this.fileImportValidationModel.save(null, {
            //    success: function () {
            //    }
            //});
        },
        fetchEntityModel: function (position) {
            var self = this;
            var total = this.fileImportValidationModel.attributes.ColumnIds.length;

            if (position == total) {
                this.onValidateComplete();
                return;
            }
            var columnId = this.fileImportValidationModel.attributes.ColumnIds[position];

            var model = new fileImportEntityModel({ Id: columnId });

            model.fetch({
                success: function (resultModel) {
                    self.fileImportValidationModel.get("EntityModels").push(resultModel);
                    self.$el.find("#validation-status")[0].innerHTML = "<span>Processed " + (position + 1) + "/" + total + "</span>";

                    position = position + 1;
                    self.fetchEntityModel(position);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onImportData: function (e) {

            this.importedEntities = [];
            this.importResult = [];
            this.$el.find("#import-spinner").show();
            this.$el.find("#import-content").hide();

            this.saveEntityModel(0);
        },
        saveEntityModel: function (position) {
            var self = this;
            var total = this.fileImportValidationModel.attributes.ColumnIds.length;

            if (position == total) {
                this.onImportComplete();
                return;
            }
            var columnId = this.fileImportValidationModel.attributes.ColumnIds[position];

            var model = this.fileImportValidationModel.attributes.EntityModels[position];

            model.url = "/api/fileimportentity/";
            model.save({
                type: "import"
            }, {
                success: function (resultModel) {
                    //self.fileImportValidationModel.get("EntityModels").push(resultModel);
                    self.importedEntities.push(resultModel.attributes.EntityId);
                    self.importResult.push(resultModel);

                    self.$el.find("#import-status")[0].innerHTML = "<span>Processed " + (position + 1) + "/" + total + "</span>";

                    position = position + 1;
                    self.saveEntityModel(position);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onImportComplete: function () {
            var self = this;

            this.$el.find("#import-spinner").hide();
            this.$el.find("#import-content").show();

            var errors = "";

            _.each(this.importResult, function (model) {
                if (!model.attributes.Imported) {
                    errors += model.attributes.Name + "<br />";
                }
            });

            if (errors == "") {
                this.$el.find("#import-action").html("Import successful!");
            } else {
                this.$el.find("#import-action").html("Import completed with errors!<br /><br />The following entities were not imported: <br />" + errors);
            }


            //this.fileImportValidationModel.save(null, {
            //    success: function () {
            //    }
            //});
        },
        onChangeField: function (e) {
            var self = this;

            var newField = e.currentTarget.value;
            var columnId = e.currentTarget.parentNode.id;
            var column = _.find(this.fileImportConfigurationModel.attributes.Columns, function (item) {
                return item.Id == columnId;
            });

            column.FieldTypeId = newField;

            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onToggleClearEmptyValues: function () {
            this.fileImportConfigurationModel.set("ClearEmptyValues", this.$el.find("#clearEmptyValue")[0].checked);
            this.fileImportConfigurationModel.save();
        },
        onSelectEntityType: function () {
            var self = this;

            this.fileImportConfigurationModel.set("EntityTypeId", this.$el.find("#selectEntityType").val());

            this.fileImportValidationModel = null;

            this.fileImportConfigurationModel.save(null, {
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        showSaveMappingDialog: function () {
            this.$el.find("#save-mapping-dialog").show();
            this.$el.find("#select-existing-mapping").hide();
        },
        onCancelSaveMapping: function () {
            this.$el.find("#save-mapping-dialog").hide();
            this.$el.find("#select-existing-mapping").show();
            this.$el.find("#mapping-name").val("");
        },
        onSaveMapping: function () {
            var self = this;

            //Hämta namn på mappningen. 
            if (this.$el.find("#mapping-name").val() == "") {
                this.$el.find("#save-mapping-dialog").addClass("error");
                this.$el.find("#mapping-name-error").show();
                return;
            }

            this.fileImportConfigurationMappingModel = new fileImportConfigurationMappingModel();
            this.fileImportConfigurationMappingModel.attributes = this.fileImportConfigurationModel.attributes;
            //   this.fileImportConfigurationMappingModel.id = this.fileImportConfigurationModel.id; 

            this.fileImportConfigurationMappingModel.set("MappingName", this.$el.find("#mapping-name").val());
            //   this.fileImportConfigurationMappingModel.set("Action", "saveMapping");

            this.fileImportValidationModel = null;

            this.fileImportConfigurationMappingModel.save(null, {
                success: function (model) {
                    inRiverUtil.Notify("Mapping successfully saved");
                    self.fileImportConfigurationModel.attributes.Id = model.attributes.Id;
                    self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                    self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                    self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;
                    self.fileImportConfigurationMappings.fetch({
                        success: function () {
                            self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onSelectMapping: function () {
            var self = this;

            var x = new fileImportConfigurationMappingModel({ MappingId: self.$el.find("#mapping").val() });

            x.fetch({
                success: function (model) {
                    self.fileImportConfigurationModel.attributes.Id = model.attributes.Id;
                    self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                    self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                    self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;

                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });


        },
        onDeleteMapping: function () {


            var callback = function (self) {
                var x = new fileImportConfigurationMappingModel({ MappingId: self.$el.find("#mapping").val() });
                x.destroy({
                    success: function () {
                        inRiverUtil.Notify("Mapping successfully deleted");
                        self.fileImportConfigurationMappings.fetch({
                            success: function () {

                                self.fileImportConfigurationModel.attributes.MappingId = -1;
                                self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            };
            inRiverUtil.NotifyConfirm("Delete", "Do you want to delete this mapping", callback, this);
        },
        onUpdateMapping: function () {
            var self = this;

            this.fileImportConfigurationMappingModel = new fileImportConfigurationMappingModel({
                MappingId: this.fileImportConfigurationModel.attributes.MappingId
            });


            this.fileImportConfigurationMappingModel.fetch({
                success: function () {
                    self.fileImportConfigurationMappingModel.attributes = self.fileImportConfigurationModel.attributes;
                    self.fileImportConfigurationMappingModel.save(null, {
                        success: function (model) {
                            inRiverUtil.Notify("Mapping successfully updated");
                            self.fileImportConfigurationModel.attributes.Id = model.attributes.Id;
                            self.fileImportConfigurationModel.attributes.MappingId = model.attributes.MappingId;
                            self.fileImportConfigurationModel.attributes.EntityTypeId = model.attributes.EntityTypeId;
                            self.fileImportConfigurationModel.attributes.Columns = model.attributes.Columns;
                            self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        moveToConfiguration: function () {
            var self = this;

            this.fileImportConfigurationModel = new fileImportConfigurationModel({ Id: this.modelid });
            this.fileImportConfigurationModel.fetch({
                success: function () {
                    self.$el.find("#import-configuration").html(_.template(importConfigurationTemplate, { data: self.fileImportConfigurationModel.toJSON(), mappings: self.fileImportConfigurationMappings.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        moveToValidation: function () {
            if (this.fileImportValidationModel != null) {
                return;
            }

            var self = this;
            this.fileImportValidationModel = new fileImportValidationModel({ Id: this.modelid, entitytypeId : this.fileImportConfigurationModel.attributes.EntityTypeId });
            this.fileImportValidationModel.fetch(
            {
                success: function () {
                    self.$el.find("#import-validation").html(_.template(importValidationTemplate, { data: self.fileImportValidationModel.toJSON() }));
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //  this.$el.find("#import-validation").html(_.template(importValidationTemplate, { data: this.fileImportConfigurationModel.toJSON() }));
        },
        moveToImport: function () {

            this.fileImportValidationModel.attributes.EntityModels = _.select(
                this.fileImportValidationModel.attributes.EntityModels,
                function (model) {
                    return (model.get("Modified") == true || model.get("IsNew") == true) && model.get("Valid") == true;
                }
            );

            this.fileImportValidationModel.attributes.EntitiesToProcess = this.fileImportValidationModel.attributes.EntityModels.length;

            this.fileImportValidationModel.attributes.ColumnIds = _.pluck(this.fileImportValidationModel.attributes.EntityModels, 'id');

            this.$el.find("#import-action").html(_.template(importActionTemplate, { data: this.fileImportValidationModel.toJSON() }));
        },
        validateStatus: function (fileImportModel) {
            this.fileImportValidationModel.Valid = true;
        },
        finish: function () {

            if (this.importedEntities == null) {
                return;
            }
            var self = this;
            window.appHelper.event_bus.trigger('closepopup');


            var entities = this.importedEntities.toString();
            $.post("/api/tools/compress", { '': entities }).done(function (compressed) {

                self.goTo("workareasearch?title=Imported Entities&compressedEntities=" + compressed);
            });




        },
        render: function () {
            var self = this;
            this.$el.html(_.template(importDataTemplate, {}));
            this.$el.find("#import-data-wizard").steps(
            {
                transitionEffect: "slideLeft",
                transitionEffectSpeed: 400,
                enableAllSteps: false,
                autoFocus: true,
                enableCancelButton: true,
                onCanceled: function () {
                    self.onCancel();
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (currentIndex == 0 && self.modelid == null) {
                        return false;
                    }

                    if (currentIndex == 0 && newIndex == 1) {
                        self.moveToConfiguration();
                    }

                    if (currentIndex == 1 && newIndex == 2) {
                        self.moveToValidation();
                    }

                    // if (newIndex == 3 && !self.fileImportConfigurationModel.attributes.DataIsValid) {
                    if (newIndex == 3 && !self.fileImportValidationModel.Valid) {
                        return false;
                    }

                    if (newIndex == 3 && self.fileImportValidationModel.Valid) {
                        self.moveToImport();
                    }

                    return true;
                },
                onFinished: function (event, currentIndex) {
                    self.finish();
                }
            });

            this.myAwesomeDropzone = new Dropzone(this.$el.find("#import-dropzone")[0], {
                url: "/fileupload/file",
                maxFilesize: 1000,
                parallelUploads: 10,
                thumbnailWidth: 100,
                thumbnailHeight: 100,
                maxFiles: 1,
                autoProcessQueue: true,
                dictDefaultMessage: "Drop file here or Click to Browse",
                acceptedFiles: ".xls,.xlsx,.csv",
                init: function () {
                    var thisDropzone = this;

                    this.on("processing", function () {
                        thisDropzone.options.autoProcessQueue = true;
                    });

                    this.on("addedfile", function (file) {
                        var files = thisDropzone.getAcceptedFiles();

                        if (files.length == 1) {
                            thisDropzone.removeFile(files[0]);
                        }

                        $("#import-dropzone").removeClass("dropzone-button");
                        $("#import-dropzone").addClass("dropzone");
                    });

                    this.on("success", function (file, id) {
                        self.fileImportValidationModel = null;
                        self.modelid = id;
                    });
                }
            });

            this.listenTo(window.appHelper.event_bus, 'validationstatus', this.validateStatus);

            return this;
        }
    });

    return importDataView;
});
