﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/specification/specificationTemplateEditFieldTemplate.html',
    'sharedjs/collections/languages/languagesCollection',
    'sharedjs/extensions/localeStringEditor',
    'text!sharedtemplates/entity/fieldEditorTemplate.html'
], function ($, _, Backbone, inRiverUtil, specificationTemplateEditFieldTemplate, languagesCollection, localeStringEditor, fieldEditorTemplate) {

    var specificationTemplateEditFieldView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            var that = this;
            if (!options.model) {
                // Create new
                var tmpModel = new Backbone.Model({ Id: "empty" }, {
                    idAttribute: "Id",
                    urlRoot: options.categoryCollection.url + "/empty"
                });
                tmpModel.fetch({
                    success: function() {
                        that.model = tmpModel;
                        that.init();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                // Edit existing
                this.init();
            }
        },
        events: {
            //"click .lightboard-card-wrap": "onCardClick",
        },
        init: function () {
            var that = this;

            // Fetch languages (TODO: this should be bootstrapped in to the page for global access)
            this.languages = new languagesCollection();
            this.languages.fetch({
                success: function (e) {
                    window.appHelper.loadingLangages = false;
                    window.appHelper.languagesCollection = e;

                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var that = this;

            this.$el.html(_.template(specificationTemplateEditFieldTemplate, {}));
            
            this.model.schema = {
                Id: { title: "Id", type: "Text", validators: ['required'], editorAttrs: { disabled: !this.model.isNew() } },
                Name: { title: "Name", type: localeStringEditor },
                Index: { title: "Index", type: "Text", validators: ['required'] },
            };

            var formTemplate = "<div id='inriver-fields-form-area'><form>";
            _.each(Object.keys(this.model.schema), function (key) {
                var editor = _.template(fieldEditorTemplate, {
                    dataEditorKey: key, name: that.model.schema[key].title, type: "", unit: null, revision: null, description: null,
                    showSelectAllCheckbox: that.model.schema[key].type == "Checkboxes" && that.model.schema[key].editorAttrs.disabled != "disabled"
                });
                formTemplate += (editor);
            });
            formTemplate += "</div></form>";

            // Render user info form
            this.form = new Backbone.Form({
                template: _.template(formTemplate),
                model: this.model
            }).render();
            this.form.fields.Name.editor.onEdit(); // Show localestring editor in expanded mode
            this.$el.find("#specification-template-edit-field-form-wrap").html(this.form.el);

            return this; // enable chained calls
        }
    });
    

    return specificationTemplateEditFieldView;
});
