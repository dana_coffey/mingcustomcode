define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/field/fieldModel'
], function ($, _, Backbone, fieldModel) {
    var fileImportConfigurationMappingCollection = Backbone.Collection.extend({
        initialize: function () {
        },
        url: '/api/fileimportconfigurationmapping'
    });

    return fileImportConfigurationMappingCollection;
});
