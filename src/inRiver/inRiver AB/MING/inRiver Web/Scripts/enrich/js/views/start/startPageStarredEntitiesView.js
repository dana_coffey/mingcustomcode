﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'jstree',
  'sharedjs/misc/inRiverUtil',
  'views/entitycard/entitycardView',
  'sharedjs/models/workarea/workareaFolderModel',
  'sharedjs/collections/entities/starredEntityCollection',
  'text!templates/start/startpageWorkareaTemplate.html'
], function ($, _, Backbone, jqueryui, jstree, inRiverUtil, entitycardView, workareaFolderModel, starredEntityCollection, startpageTemplate) {

    var startPageStarredEntitiesView = Backbone.View.extend({
        initialize: function (data) {

            var self = this;

            self.starred = new starredEntityCollection();
            self.starred.fetch({
                success: function () {
                    self.startedfetched = true;
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {

        },
        entityUpdated: function (model) {

            if (this.starred.length == 0) {
                $("#starred-entities-content").html("<div class='no-items-text'>No starred entities</div>");
            } else {
                for (var i = 0; i < this.starred.models.length; i++) {

                    var star = this.starred.models[i];
                    if (star.attributes.Starred == "0") {
                        $("#card-" + star.id).parent().parent().remove();
                        this.starred.models.splice(i, 1);
                    }
                }
            }
        },
        render: function () {
            this.listenTo(appHelper.event_bus, 'entityupdated', this.entityUpdated);

            var self = this;

            $("#starred-entities-content .loadingspinner").remove();
            if (this.starred.length == 0) {
                $("#starred-entities-content").html("<div class='no-items-text'>No starred entities</div>");
            } else {

                var count = this.starred.models.length;
                for (var i = 0; i < count; i++) {

                    var star = this.starred.models[i];
                    var entitycard = new entitycardView({
                        model: star,
                    });

                    $("#starred-entities-content").append(entitycard.render().el);
                }

                if (count > 5) {
                    $("#starred-entities-content").append("<div id=\"showAllStars\">Show all...</div>");
                }
            }
        }
    });

    return startPageStarredEntitiesView;
});