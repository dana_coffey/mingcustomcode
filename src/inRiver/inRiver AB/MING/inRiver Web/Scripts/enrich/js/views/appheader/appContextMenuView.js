﻿define([
  'jquery',
  'underscore',
  'backbone',
    'modalPopup',
    'sharedjs/misc/permissionUtil',
  'sharedjs/views/create/entityCreateView',
  'views/appheader/uploadMediaView',
  'views/appheader/importDataView',
  'text!templates/shared/appContextMenuTemplate.html'
], function ($, _, backbone, modalPopup, permissionUtil, entityCreateView, uploadMediaView, importDataView, appContextMenuTemplate) {

    var appContextMenuView = backbone.View.extend({
        initialize: function () {
            this.render();
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #upload-media": "uploadMedia",
            "click #import-data": "importData",
            "click #close-tools-container": "deselect"
        },
        deselect: function (e) {
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        uploadMedia: function () {

            this.modal = new modalPopup();

            this.modal.popOut(new uploadMediaView({

            }), { size: "large", header: "Upload Media" });

            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        importData: function () {
            this.modal = new modalPopup();

            this.modal.popOut(new importDataView({

            }), { size: "medium", header: "Import Data" });

            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle();

            $box.position({
                my: "right top",
                at: "right top+25",
                of: $("#tools-show")
            });

            $(document).one('click', this.deselect.bind(this));
        },
        createEntity: function (e) {
            this.onToggleTools(e);

            this.listenTo(appHelper.event_bus, 'entitycreated', this.entityCreated);

            if (this._modalView) {
                this._modalView.remove();
            }

            this._modalView = new entityCreateView();

            window.scrollTo(0, 0);
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        entityCreated: function (model) {
            if (model.attributes.EntityType != "Task") {
                this.goTo("entity/" + model.id);
            }
        },
        render: function () {
            this.$el.html(appContextMenuTemplate);
            permissionUtil.CheckPermissionForElement("AddFile", this.$el.find("#upload-media"));
            permissionUtil.CheckPermissionForElement("AddEntity", this.$el.find("#import-data"));
            if (!permissionUtil.CheckPermission("AddFile") && !permissionUtil.CheckPermission("AddEntity")) {
                this.$el.find(".app-tools").hide();
            }
            
            if (permissionUtil.CheckPermission("AddEntity") == true) {
                this.$el.find("#create-entity-types").html(new entityCreateView({ contextmenu: this }).el);
            }
            return this;
        }
    });

    return appContextMenuView;

});
