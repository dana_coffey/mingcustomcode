﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fileImportConfigurationMappingModel = Backbone.Model.extend({
        idAttribute: "MappingId",
        initialize: function () {
        },
        urlRoot: '/api/fileimportconfigurationmapping/',
    });

    return fileImportConfigurationMappingModel;

});
