// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'sharedjs/views/topheader/topHeaderView',
  'views/appheader/appHeaderView',
  'views/start/startPageView',
  'views/entityview/entityView',
  'sharedjs/views/workarea/workAreaHandler',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/languages/LanguagesCollection'

], function ($, _, backbone, jqueryui, topHeaderView, appHeaderView, startPageView, entityView, workAreaHandler, inRiverUtil, LanguagesCollection) {

    var appRouter = backbone.Router.extend({
        routes: {
            // Define some URL routes
            "home": "home",
            "workarea?*queryString": "workarea",
            "workareasearch?*queryString": "workareasearch",
            "entity/:id(/:tab)": "entity",
            // Default
            '*actions': 'home',
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);
            
            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(newView.el);
        },
        closeView: function (specView) {
            if (specView) {
                if (specView._currentTopTabView && specView._currentTopTabView.detailfields) {
                    for (index = 0; index < specView._currentTopTabView.detailfields.length; index++) {
                        specView._currentTopTabView.detailfields[index].stopListening(window.appHelper.event_bus);
                        specView._currentTopTabView.detailfields[index].remove();
                        specView._currentTopTabView.detailfields[index].unbind();
                    }
                }

                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function (queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function (el) {
                        var key = el.substr(0, el.indexOf('='));
                        var value = el.substr(el.indexOf('=') + 1);
                        var o = {};
                        o[key] = value;
                        return o;
                    }),
                    function (o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function () {
        console.log("init router");
        window.appHelper.event_bus = _({}).extend(backbone.Events);

        window.appHelper.currentApp = "Enrich";
        var headerView = new topHeaderView({ app: window.appHelper.currentApp });
        var appheaderView = new appHeaderView();

        var app_Router = new appRouter;
        window.appHelper.languagesCollection = new LanguagesCollection(window.appData.languages);

        app_Router.workAreaHandler = new workAreaHandler({ app: "enrich" });
        window.appHelper.workAreaHandler = app_Router.workAreaHandler; 
        app_Router.currentViewType = ""; 
        app_Router.on('route:entity', function (id, tab) {
            var workareaCollection = undefined;
            var workareaEntities = undefined;

            
            if (tab) {
                appSession.entityViewTabId = "tab-" + tab;
            } else {
                if (!appSession.entityViewTabId || appSession.entityViewTabId == "") {
                    appSession.entityViewTabId = "tab-overview";
                }
            }

            var hasEmptyWorkArea;
             if (!app_Router.workAreaHandler.hasWorkareas()) {
                app_Router.workAreaHandler.reopenOrAddWorkAreas({ title: "Empty" });
                hasEmptyWorkArea = app_Router.workAreaHandler.hasEmptyWorkareas();
            } else {
                hasEmptyWorkArea = false;
            }
             app_Router.workAreaHandler.setSmallWorkareas();

            this.switchView(new entityView({ id: id, queryview: this.workAreaView, hasEmptyWorkArea: hasEmptyWorkArea }));
            
            app_Router.workAreaHandler.bindEvents();
            app_Router.currentViewType = "entity"; 

        });

        app_Router.on('route:workareasearch', function (queryString) {
            app_Router.navigate("", { trigger: false, replace: true }); // reset to "no route" to make sure other routes work properly (fix for bug 8154)
            if (app_Router.currentViewType == "start" ){ //}|| app_Router.currentViewType =="entity") {
                this.switchView(app_Router.workAreaHandler);
                if (app_Router.currentViewType == "entity") {
                    app_Router.workAreaHandler.setWideWorkareas(); 
                }

                app_Router.currentViewType = "workarea";
            }
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var workarea = app_Router.workAreaHandler.getActiveWorkarea();
            
            if (params.advsearch != undefined) {
                app_Router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
            } else if (params.searchPhrase != undefined) {
                app_Router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
            } else if (params.compressedEntities != undefined) {
                app_Router.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities});
            }

        });

        app_Router.on('route:workarea', function (queryString) {
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var rendered = false; 

            if (params.stopAdvQuery != undefined)
                if (params.stopAdvQuery == "true")
                    advsearchPhrase = undefined;

            if (!app_Router.workAreaHandler.hasWorkareas()) {
                var title = params.title;
                if (title == undefined || title == null) {
                    title = "Empty"; 
                }
                if (params.workareaId != undefined && (params.isQuery != undefined && params.isQuery.toString() == "true")) {
                    app_Router.workAreaHandler.openSavedQuery({ title: title, workareaId: params.workareaId });
                   rendered=true; 
                } else if (params.workareaId != undefined) {
                    app_Router.workAreaHandler.openSavedWorkarea({ title: title, workareaId: params.workareaId });
                    rendered = true;
                } else {
                    app_Router.workAreaHandler.reopenOrAddWorkAreas({ title: title, compressedEntities: params.compressedEntities, workareaId: params.workareaId, searchPhrase: params.searchPhrase, advsearchPhrase: advsearchPhrase, hasworkareaSearch: true, isQuery: params.isQuery });
                    rendered = true;
                }

            }
            if (!rendered) {
                if (params.advsearch != undefined) {
                    app_Router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
                } else if (params.searchPhrase != undefined) {
                    app_Router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
                } else if (params.compressedEntities != undefined) {
                    app_Router.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities });
                } else if (params.workareaId != undefined) {
                    if (params.isQuery != undefined && params.isQuery.toString() == "true") {
                        app_Router.workAreaHandler.openSavedQuery({ title: params.title, workareaId: params.workareaId });
                    } else {
                        app_Router.workAreaHandler.openSavedWorkarea({ title: params.title, workareaId: params.workareaId });
                    }
                } else {
                    app_Router.workAreaHandler.renderResults();
                }
            }
            app_Router.workAreaHandler.setWideWorkareas();
            app_Router.workAreaHandler.bindEvents();
            this.switchView(app_Router.workAreaHandler);
            app_Router.currentViewType = "workarea";
        });

        app_Router.on('route:home', function (id) {
            app_Router.workAreaHandler.closeWorkAreas();
            app_Router.workAreaHandler.hideWorkareas();
            this.switchView(new startPageView());
            app_Router.currentViewType = "start";
        });

        // var footerView = new FooterView();

        // Extend the View class to include a navigation method goTo
        backbone.View.prototype.goTo = function (loc, changeurl) {
            if (changeurl == undefined) {
                changeurl = { trigger: true };
            }

            // Check for unsaved changes in current view before navigating.
            // When trigger is false it means that the view is actually switched elsewhere
            // and therefore the check for unsaved changes must be handled there instead (only the
            // browser url will be affected when trigger is set to false).
            if (changeurl.trigger) {
                inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(app_Router._currentView, function() {
                    app_Router.navigate(loc, changeurl);
                });
            } else {
                app_Router.navigate(loc, changeurl);
            }
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        backbone.View.prototype.close = function () {
            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };

        backbone.history.start();
    };

    return {
        initialize: initialize
    };

});
