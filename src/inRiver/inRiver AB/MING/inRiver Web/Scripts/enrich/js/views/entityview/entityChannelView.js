﻿define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'jstree',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/channel/channelEntityCollection',
  'sharedjs/views/channels/channelEntityView',
  'sharedjs/models/entity/channelModel',
  'text!sharedtemplates/entity/channelTemplate.html'

], function ($, _, Backbone, jqueryui, jstree,inRiverUtil, channelEntityCollection, channelEntityView, channelModel, channelTemplate) {

    var entityChannelView = Backbone.View.extend({
        initialize: function (options) {
            var self = this;

            self.id = options.id;
            this.undelegateEvents();

            this.channels = new channelEntityCollection(([], { id: options.id }));
            this.channels.fetch({
                success: function () {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click .showInStructure": "onToggleStructure"
        },
        onToggleStructure: function (e) {
            e.stopPropagation();

            var id = e.currentTarget.id;
            id = id.replace("show-", "");

            this.$el.find("#Struct-" + id).toggle(200);

            if (this.$el.find("#show-" + id).html().indexOf("caret-down") > 0) {
                this.$el.find("#show-" + id).html("<i class=\"fa fa-caret-up\"></i> Hide Channel structure");

            } else {
                this.$el.find("#show-" + id).html("<i class=\"fa fa-caret-down\"></i> Show in Channel structure");
            }

        },
        addChannel: function (struct, channelId) {
            var self = this;
            this.$el.find("#channels-list").append("<div id=\"Channel-" + channelId + "\" class=\"channel-wrap\"></div><div id=\"Struct-" + channelId + "\" ></div><div class=\"showInStructure\" id=\"show-" + channelId + "\"><i class=\"fa fa-caret-down\"></i> Show in Channel structure</div>");

            var entityModel = new channelModel({ Id: channelId });
            entityModel.fetch({
                success: function (entity) {
                    var entityView = new channelEntityView({ model: entity });
                    self.$el.find('#Channel-' + channelId).append(entityView.render().el);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.$el.find("#Struct-" + channelId).jstree({
                core: {
                    data: struct[0].children,
                    animation: "100",
                    themes: { dots: false },
                    'check_callback': function (operation, node, node_parent, node_position, more) {
                        // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                        // in case of 'rename_node' node_position is filled with the new node name
                        return operation === 'move_node' ? true : true;
                    }
                },
                "plugins": [
                    "types", "wholerow", "dnd", "search", "state"
                ],
                "types": {
                    "#": {
                        "icon": "/Icon?id=Channel",
                        "max_children": 1,
                        "max_depth": 10,
                        "valid_children": ["topnode", "childnode"]
                    },
                    "node": {
                        "icon": "/Icon?id=ChannelNode",
                        "valid_children": ["childnode"]
                    }
                }
            });
            this.$el.find("#Struct-" + channelId).hide();
        },
        render: function () {
            var self = this;
            var list = "";

            var breadcrumb = "";
            this.$el.html(_.template(channelTemplate, { data: self.channels.models }));
            var struct = [];
            var objarr = [];
            var channelId = "";
            _.each(self.channels.models, function (channel) {
                i++;
                if (channel.attributes.parent == "#") {
                    //Create Card
                    if (struct.length > 0) {
                        self.addChannel(struct, channelId);
                    }

                    channelId = channel.attributes.entityId;

                    var obj = { id: channel.attributes.entityId, text: channel.attributes.text, type: "#", state: { opened: true }, children: [], structid: channel.attributes.id };
                    struct = [obj];
                    objarr = [obj];

                    breadcrumb += "<ul class='breadcrumbs'>";
                } else {

                    if (channel.attributes.entityId != self.id) {

                        var obj = { id: channel.attributes.entityId, text: channel.attributes.text, type: "node", state: { opened: true }, children: [], structid: channel.attributes.id };
                        objarr.push(obj);

                        var parent = _.findWhere(objarr, { structid: channel.attributes.parent });

                        if (parent != null) {
                            parent.children.push(obj);
                        }
                        
                    } else {

                        var i = 0;
                    }
                }
                list += "<li><a href='enrich#entity/" + channel.attributes.entityId + "'>" + channel.attributes.text + "</a></li>";
                i++;
            });
            if (struct.length > 0) {
                this.addChannel(struct, channelId);
            }
            breadcrumb += list + "</ul>";

        }
    });

    return entityChannelView;
});

