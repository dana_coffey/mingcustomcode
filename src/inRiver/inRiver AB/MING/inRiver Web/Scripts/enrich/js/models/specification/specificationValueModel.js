﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var specificationModel = Backbone.DeepModel.extend({
        idAttribute: "EntityId",
        initialize: function (options) {
            this.entityId = options.entityId;
            this.entityTypeId = options.type;
        },
        //urlRoot: '/api/specificationValue',
        url: function () {
            return '/api/specification?entityId=' + this.entityId + "&entityTypeId=" + this.entityTypeId;
        }
    });

    return specificationModel;

});
