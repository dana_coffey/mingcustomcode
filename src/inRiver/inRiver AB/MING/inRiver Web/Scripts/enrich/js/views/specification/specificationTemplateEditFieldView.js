﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/specification/specificationTemplateEditFieldTemplate.html',
    'sharedjs/collections/languages/languagesCollection',
    'sharedjs/extensions/localeStringEditor',
    'sharedjs/collections/cvl/CvlCollection',
    'sharedjs/collections/cvl/CvlValueCollection',
    'text!sharedtemplates/entity/fieldEditorTemplate.html'
], function ($, _, Backbone, inRiverUtil, specificationTemplateEditFieldTemplate, languagesCollection, localeStringEditor, CvlCollection, CvlValueCollection, fieldEditorTemplate) {

    var specificationTemplateEditFieldView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            var that = this;
            if (!options.model) {
                // Create new
                var tmpModel = new Backbone.Model({ Id: "empty" }, {
                    idAttribute: "Id",
                    urlRoot: options.fieldCollection.url + "/empty"
                });
                tmpModel.fetch({
                    success: function() {
                        that.model = tmpModel;
                        that.maybeInitAdditionalDataCvl();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                // Edit existing
                this.maybeInitAdditionalDataCvl();
            }
        },
        events: {
            
        },
        maybeInitAdditionalDataCvl: function () {
            if (this.model.get("AdditionalDataCVLId")) {
                this.additionalDataCvlValuesCollection = new CvlValueCollection(null, { cvlId: this.model.get("AdditionalDataCVLId") });
                this.listenTo(this.additionalDataCvlValuesCollection, "reset", this.init);
                this.additionalDataCvlValuesCollection.fetch({ reset: true });
            } else {
                this.init();
            }
        },
        init: function () {
            var that = this;

            // Fetch languages (TODO: this should be bootstrapped in to the page for global access)
            this.languages = new languagesCollection();
            this.languages.fetch({
                success: function (e) {
                    window.appHelper.loadingLangages = false;
                    window.appHelper.languagesCollection = e;

                    that.cvlCollection = new CvlCollection();
                    that.listenTo(that.cvlCollection, "reset", that.render);
                    that.cvlCollection.fetch({ reset: true });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        updateUi: function () {
            if (this.form.fields.SpecificationDataType.getValue() == "CVL") {
                this.$el.find("[name=CVLId]").parent().parent().parent().show();
            } else {
                this.$el.find("[name=CVLId]").parent().parent().parent().hide();
            }
        },
        render: function () {
            var that = this;

            this.$el.html(_.template(specificationTemplateEditFieldTemplate, {}));

            // Create option list with available units
            var unitArray = [];
            unitArray.push({ val: null, label: "(not set)" });
            this.options.unitCollection.each(function(model) {
                unitArray.push({ val: model.get("Key"), label: model.get("DisplayValue") });
            });
            
            this.model.schema = {
                Id: { title: "Id", type: "Text", validators: ['required'], editorAttrs: { disabled: !this.model.isNew() } },
                Name: { title: "Name", type: localeStringEditor },
                "CategoryId": { title: "Category", type: "Select", options: this.options.categoryCollection },
                "SpecificationDataType": { title: "Data type", type: "Select", validators: ['required'], editorAttrs: { disabled: !this.model.isNew() },  options: ["String", "LocaleString", "Double", "Integer", "Boolean", "DateTime", "CVL"] },
                "CVLId": { title: "CVL", type: "Select", options: this.cvlCollection.pluck("Id") },
                "Unit": { title: "Unit",  type: "Select", options: unitArray },
                "Mandatory": { title: "Mandatory",  type: "Checkbox" },
                "Multivalue": { title: "Multivalue", type: "Checkbox" },
                "Enabled": { title: "Enabled", type: "Checkbox" },
                "Index": { title: "Index", type: "Text", validators: ['required'] },
                "Format": { title: "Format", type: "Text" },
            };

            if (this.additionalDataCvlValuesCollection) {
                // Create option list with available units
                var additionalDataCvlValuesArray = [];
                if (!this.model.get("AdditionalDataIsArray")) {
                    additionalDataCvlValuesArray.push({ val: null, label: "(not set)" });
                }
                this.additionalDataCvlValuesCollection.each(function(model) {
                    additionalDataCvlValuesArray.push({ val: model.get("Key"), label: model.get("DisplayValue") });
                });

                // Create forms schema
                if (this.model.get("AdditionalDataIsArray")) {
                    this.model.schema.AdditionalDataValueArray = { title: this.model.get("AdditionalDataCVLId"), type: "Checkboxes", options: additionalDataCvlValuesArray, editorAttrs: { disabled: this.model.isNew() } };
                } else {
                    this.model.schema.AdditionalDataValue = { title: this.model.get("AdditionalDataCVLId"), type: "Select", options: additionalDataCvlValuesArray, editorAttrs: { disabled: this.model.isNew() } };
                }
            }

            var formTemplate = "<div id='inriver-fields-form-area'><form>";
            _.each(Object.keys(this.model.schema), function (key) {
                var editor = _.template(fieldEditorTemplate, { dataEditorKey: key, name: that.model.schema[key].title, type: key, unit: null, revision: null, description: null, showSelectAllCheckbox: that.model.schema[key].type == "Checkboxes" && that.model.schema[key].editorAttrs.disabled != "disabled" });
                formTemplate += (editor);
            });
            formTemplate += "</div></form>";

            // Render user info form
            this.form = new Backbone.Form({
                template: _.template(formTemplate),
                model: this.model
            }).render();
            this.form.on('SpecificationDataType:change', function (a, b) {
                that.updateUi();
            });
            this.form.fields.Name.editor.onEdit(); // Show localestring editor in expanded mode
            this.$el.find("#specification-template-edit-field-form-wrap").html(this.form.el);

            this.updateUi();

            return this; // enable chained calls
        }
    });
    

    return specificationTemplateEditFieldView;
});
