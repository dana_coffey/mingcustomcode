﻿define([
    'jquery',
    'underscore',
    'backbone',
    'alertify',
    'modalPopup',
     'sharedjs/misc/inRiverUtil',
    'views/specification/specificationCopyFromPopupView',
    'views/specification/specificationCopyToPopupView',
    'views/specification/specificationSelectTemplateView',
    'models/specification/specificationValueModel',
    'text!templates/specification/specificationDataTemplate.html',
    'text!templates/specification/specificationCategoryTemplate.html',
    'sharedjs/views/entityview/entityDetailSettingView',
    'sharedjs/misc/permissionUtil',
], function ($, _, backbone, alertify, modalPopup, inRiverUtil, specificationCopyFromPopupView, specificationCopyToPopupView, specificationSelectTemplateView, specificationValueModel, specificationDataTemplate, specificationCategoryTemplate, entityDetailSettingView, permissionUtil) {

    var specificationView = backbone.View.extend({
        initialize: function (options) {
            this.undelegateEvents();
            this.detailfields = [];
            var that = this;
            this.type = options.type;
            this.model = new specificationValueModel({ entityId: options.id, type: options.type });
            this.editCount = 0;
            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, "specificationupdated", this.onReload);
            this.model.fetch({
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #open-template-button": "onOpenTemplateClick",
            "click #select-template-button": "onSelectTemplateClick",
            "click #copy-from-button": "onCopyFromClick",
            "click #copy-to-button": "onCopyToClick",
            "click #entity-detail-settings-save-all-button": "onSaveAll",
            "click #entity-detail-settings-undo-all-button": "onUndoAll",
            "click #expand": "onExpandAll",
            "click #collapse": "onCollapseAll"
        },
        onSaveAll: function () {
            window.appHelper.event_bus.trigger('entity-details-save');
            //this.entityDetailSettingSubView.save
        },
        onReload: function (e) {
            var that = this;
            this.model.fetch({
                success: function () {
                    that.entityDetailSettingSubView.close();
                    that.entityDetailSettingSubView = null;
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onUndoAll: function () {
            window.appHelper.event_bus.trigger('entity-details-undo');
            //this.entityDetailSettingSubView.undo
        },
        onExpandAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").show();
            categories.find("i.fa-minus-circle").show();
            categories.find("i.fa-plus-circle").hide();
        },
        onCollapseAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").hide();
            categories.find("i.fa-minus-circle").hide();
            categories.find("i.fa-plus-circle").show();
        },
        onOpenTemplateClick: function (e) {
            e.stopPropagation();

            this.goTo("entity/" + this.model.get("SpecificationTemplateEntityId") + "/specification");

        },
        onSelectTemplateClick: function (e) {
            e.stopPropagation();

            var modal = new modalPopup();
            modal.popOut(new specificationSelectTemplateView({
                template: this.model.get("SpecificationTemplateEntityId"),
                currentEntityId: this.model.get("EntityId"),
                currentEntityTypeId: this.type
            }), { size: "small", header: "Select Template", usesavebutton: true });
        },
        onClose: function () {
            if (this.entityDetailSettingSubView) {
                this.entityDetailSettingSubView.close();
            }
        },
        onCopyFromClick: function (e) {
            e.stopPropagation();

            var modal = new modalPopup();

            modal.popOut(new specificationCopyFromPopupView({
                template: this.model.get("SpecificationTemplateEntityId"),
                currentEntityId: this.model.get("EntityId"),
                currentEntityTypeId: this.type
            }), { size: "small", header: "Copy From", usesavebutton: true });
        },
        onCopyToClick: function (e) {
            e.stopPropagation();

            var modal = new modalPopup();

            modal.popOut(new specificationCopyToPopupView({
                template: this.model.get("SpecificationTemplateEntityId"),
                currentEntityId: this.model.get("EntityId"),
                currentEntityTypeId: this.type
            }), { size: "small", header: "Copy To", usesavebutton: true });
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            if (this.entityDetailSettingSubView) { // might not yet be rendered 
                return this.entityDetailSettingSubView.isUnsaved;
            } else {
                return false;
            }
        },
        onIsUnsavedChanged: function () {
            var isUnsaved = this.hasUnsavedChanges();
            if (isUnsaved) {
                this.$el.find("#entity-detail-settings-save-all-button").show();
                this.$el.find("#entity-detail-settings-undo-all-button").show();
            } else {
                this.$el.find("#entity-detail-settings-save-all-button").hide();
                this.$el.find("#entity-detail-settings-undo-all-button").hide();
            }
        },
        onSaveSuccessful: function (o) {
            this.$el.find("#entity-detail-settings-save-all-button").hide();
            this.$el.find("#entity-detail-settings-undo-all-button").hide();

            inRiverUtil.Notify("Specification data has been updated");
            //this.entityUpdated(this.model.id);
        },
        render: function () {

            this.$el.html(_.template(specificationDataTemplate, { Name: this.model.get("SpecificationTemplateName"), HasTemplate: this.model.get("HasTemplate") }));

            if (!this.entityDetailSettingSubView) {

                this.model.attributes["EntityTypeDisplayName"] = "Specification Template";

                this.entityDetailSettingSubView = new entityDetailSettingView({ model: this.model, onIsUnsavedChange: this.onIsUnsavedChanged });
                this.listenTo(this.entityDetailSettingSubView, "entityDetailsIsUnsavedChange", this.onIsUnsavedChanged);
                this.listenTo(this.entityDetailSettingSubView, "saveSuccessful", this.onSaveSuccessful);
            }

            this.$el.find("#details-view-wrap").html(this.entityDetailSettingSubView.el);

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#select-template-button"));
            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#copy-from-button"));
            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#copy-to-button"));

            return this;
        }
    });

    return specificationView;
});