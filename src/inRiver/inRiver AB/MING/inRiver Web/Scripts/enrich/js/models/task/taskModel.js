﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var taskModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/taskShallow',
        
    });

    return taskModel;

});
