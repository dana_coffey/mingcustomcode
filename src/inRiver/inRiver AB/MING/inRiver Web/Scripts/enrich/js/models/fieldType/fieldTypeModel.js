﻿define([
  'underscore',
  'backbone'
], function (_, backbone) {

    var fieldTypeModel = backbone.Model.extend({
        idAttribute: "id",
        initialize: function (options) {
            this.id = options.id;
            this.fieldTypeId = options.fieldTypeId;
        },
        urlRoot: '/api/fieldtype/?fieldTypeId=' + this.fieldTypeId,
    });

    return fieldTypeModel;
});
