﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'models/cvl/CvlModel',
    'text!templates/specification/specificationTemplateEditFieldTemplate.html',
    'sharedjs/collections/languages/languagesCollection',
    'sharedjs/extensions/localeStringEditor',
    'text!sharedtemplates/entity/fieldEditorTemplate.html'
], function ($, _, Backbone, inRiverUtil, CvlModel, specificationTemplateEditFieldTemplate, languagesCollection, localeStringEditor, fieldEditorTemplate) {

    var specificationTemplateEditFieldView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            var that = this;
            this.model = new CvlModel({ CVLId: "Unit" }, {});
            
            // Fetch the unit cvl to determine the datatype
            this.unitCvlModel = new CvlModel({ Id: "Unit" });
            this.listenTo(this.unitCvlModel, "sync", this.init);
            this.unitCvlModel.fetch();
        },
        events: {
        },
        init: function () {
            var that = this;

            // Fetch languages (TODO: this should be bootstrapped in to the page for global access)
            this.languages = new languagesCollection();
            this.languages.fetch({
                success: function (e) {
                    window.appHelper.loadingLangages = false;
                    window.appHelper.languagesCollection = e;

                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            var that = this;

            this.$el.html(_.template(specificationTemplateEditFieldTemplate, {}));
            
            // Create forms schema based on CVL datayype
            this.model.schema = {
                Key: { title: "Key", type: "Text", validators: ['required'] },
            };
            if (this.unitCvlModel.get("SpecificationDataType") == "LocaleString") {
                this.model.schema.LSValue = { title: "Value", type: localeStringEditor };
            } else {
                this.model.schema.Value = { title: "Value", type: "Text", validators: ['required'] };
            }

            // Generate form template
            var formTemplate = "<div id='inriver-fields-form-area'><form>";
            _.each(Object.keys(this.model.schema), function (key) {
                var editor = _.template(fieldEditorTemplate, { dataEditorKey: key, name: that.model.schema[key].title, type: "", unit: null, revision: null, description: null, showSelectAllCheckbox: null });
                formTemplate += (editor);
            });
            formTemplate += "</div></form>";

            // Render form
            this.form = new Backbone.Form({
                template: _.template(formTemplate),
                model: this.model
            }).render();
            if (this.model.schema.LSValue) {
                this.form.fields.LSValue.editor.onEdit(); // Show localestring editor in expanded mode
            }

            this.$el.find("#specification-template-edit-field-form-wrap").html(this.form.el);

            return this; // enable chained calls
        }
    });
    

    return specificationTemplateEditFieldView;
});
