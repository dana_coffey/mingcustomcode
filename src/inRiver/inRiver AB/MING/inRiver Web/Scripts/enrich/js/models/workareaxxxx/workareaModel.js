﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var workareaModel = Backbone.DeepModel.extend({
        idAttribute: "id",
        initialize: function () {
        },
    });

    return workareaModel;

});
