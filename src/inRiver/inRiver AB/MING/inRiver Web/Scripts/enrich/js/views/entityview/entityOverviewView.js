﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/collections/resources/resourceCollection',
    'modalPopup',
    'sharedjs/models/entity/entityOverviewModel',
    'sharedjs/models/entity/entityModel',
    'text!sharedtemplates/entity/entityOverviewTemplate.html',
    'text!sharedtemplates/entity/entityOverviewFieldTemplate.html',
    'text!sharedtemplates/entity/entityOverviewChannelTemplate.html',
    'text!sharedtemplates/entity/entityOverviewMediaTemplate.html',
    'text!sharedtemplates/entity/entityOverviewRelationTemplate.html',
    'sharedjs/views/lightboard/lightboardPreview',
    'sharedjs/views/contextmenu/contextMenuView',
    'sharedjs/views/entityview/entityDetailSettingView',
    'views/relation/relationCardOverviewView'
], function ($, _, backbone, inRiverUtil, resourceCollection, modalPopup, entityOverviewModel, entityModel, entityOverviewTemplate, entityOverviewFieldTemplate, entityOverviewChannelTemplate, entityOverviewMediaTemplate, entityOverviewRelationTemplate, lightboardPreview, contextMenuView, entityDetailSettingView, relationCardView) {

    var entityOverviewView = backbone.View.extend({
        initialize: function (options) {

            this.id = options.model.id;
            this.model = new entityOverviewModel({ id: options.model.id });
            
            this.render();

            this.renderControls();

            this.listenTo(window.appHelper.event_bus, "relationremoved", this.updateLightBoard);
            this.listenTo(window.appHelper.event_bus, "entitydeleted", this.updateLightBoard);

           // this.listenTo(window.appHelper.event_bus, "entityupdated", this.renderControls);
        },
        events: {
            "click .lightboard-overview-card-wrap": "onPreview",
            "click .entity-overview-channel": "onSelectChannel",
            "click .entity-overview-publication": "onSelectPublication",
            "click .card-wrap-overview": "onSelectEntity",
            "click .completeness-small": "onShowCompletnessDetails",
            "click #showAllResources": "showAllResources"
        },
        onPreview: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var self = this;

            this.modal = new modalPopup();
            this.collection = new resourceCollection([], { Id: this.id, subEntities: false });
            this.collection.fetch({
                success: function () {
                    self.modal.popOut(new lightboardPreview({
                        id: e.currentTarget.id, collection: self.collection, parentEntityId: self.id
                    }), { size: "preview", header: "Preview" });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onSelectEntity: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var id = e.currentTarget.id;

            this.goTo("entity/" + id);
        },
        onSelectChannel: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var id = e.currentTarget.id;
            window.location.href = "/app/planrelease/index#channel/" + id;
        },
        onSelectPublication: function (e) {
            e.stopPropagation();
            e.preventDefault();
            var id = e.currentTarget.id;
            window.location.href = "/app/planrelease/index#publication/" + id;
        },
        closePopup: function () {
            this.modal.popIn();
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness($(e.currentTarget).parent()[0].id, $(e.currentTarget));
        },
        renderLightboard: function () {
            if (this.model == undefined) {
                return;
            }

            var container = this.$el.find(".thumbnails-overview");

            if (container.length > 0) {
                $(container)[0].innerHTML = "";
            }
            
            if (this.model.attributes.Resources.length == 0) {
                container.append("<li style=\"margin-top: 5px;\"><i>No media to show</i></li>");
            }

            _.each(this.model.attributes.Resources, function (model) {
                if (model.PictureUrl) {
                    model.PictureUrl = model.PictureUrl.replace("Thumbnail", "SmallThumbnail"); 
                }

                container.append(_.template(entityOverviewMediaTemplate, { Id: model.Id, DisplayName: model.DisplayName, PictureUrl: model.PictureUrl }));
            });
        },
        renderChannel: function () {

            var container = this.$el.find("#channel-area-content");

            if (container.length > 0) {
                $(container)[0].innerHTML = "";
            }

            if (this.model.attributes.Channels.length == 0) {
                container.append("<div style=\"margin-bottom: 10px;\"><i>Not used in any channels</i></div>");
            }

            _.each(this.model.attributes.Channels, function (channel) {
                container.append(_.template(entityOverviewChannelTemplate, { TypeId: channel.EntityType, Published: channel.ChannelPublished, Title: channel.DisplayName, TypeName: channel.EntityTypeDisplayName, Id: channel.Id }));
            });
        },
        renderField: function () {
            var container = this.$el.find("#field-area-content");

            if (container.length > 0) {
                $(container)[0].innerHTML = "";
            }

            if (this.model.attributes.Fields.length == 0) {
                container.append("<div><i>No data to show</i></div>");
            }

            _.each(this.model.attributes.Fields, function (f) {
                if (f.FieldDataType == "CVL" && f.DisplayValue == null) {
                    f.DisplayValue = "(not set)";
                }

                if (f.FieldDataType == "Xml") {
                    f.DisplayValue = f.Value;
                }

                // just to have spaces in the commaseparated list so the GUI will work a little bit better
                if (f.DisplayValue instanceof Array) {
                    
                    var text = "";
                    _.each(f.DisplayValue, function(val) {
                        text += val + ", "; 
                    });
                    if (text.length > 2) {
                        text = text.substring(0, text.length - 2); 
                    }
                    f.DisplayValue = text; 
                }

                if (f.DisplayValue == null) {
                    f.DisplayValue = "";
                }



                container.append(_.template(entityOverviewFieldTemplate, { key: f.FieldTypeDisplayName, value: $("<div/>").text(f.DisplayValue).html(), description: f.FieldTypeDescription }));
            });
        },
        renderOutboundRelations: function () {
            var container = this.$el.find("#outbound-area-content");

            if (container.length > 0) {
                container[0].innerHTML = "";
            }

            if (this.model.attributes.OutboundRelations.length == 0) {
                container.append("<div style=\"margin-bottom: 10px;\"><i>No relations to show</i></div>");
            }

            _.each(this.model.attributes.OutboundRelations, function (relation) {

                var relationCard = new relationCardView({ relation: relation, direction: "outbound" });

                container.append(relationCard.$el);
            });
        },
        renderInboundRelations: function () {
            var container = this.$el.find("#inbound-area-content");

            if (container.length > 0) {
                container[0].innerHTML = "";
            }

            if (this.model.attributes.InboundRelations.length == 0) {
                container.append("<div style=\"margin-bottom: 10px;\"><i>No relations to show</i></div>");
            }

            _.each(this.model.attributes.InboundRelations, function (relation) {

                var relationCard = new relationCardView({ relation: relation, direction: "inbound" });

                container.append(relationCard.$el);
            });
        },
        updateLightBoard: function(id) {
            this.$el.find("#picturearea #" + id).remove(); 

        },
        renderControls: function () {

            var that = this;

            this.model.url = "/api/entityOverview/" + this.id + "?maxResourceCount=5&maxInboundRelationCount=8&maxOutboundRelationCount=8";
            this.model.fetch(
            {
                success: function () {
                    that.renderLightboard();
                    that.renderField();
                    that.renderChannel();
                    that.renderOutboundRelations();
                    that.renderInboundRelations();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {
            this.$el.html(_.template(entityOverviewTemplate));


            return this; // enable chained calls
        }
    });

    return entityOverviewView;
});
