﻿define([
    'jquery',
    'underscore',
    'backbone',
    'modalPopup',
    'text!templates/merchandising/merchandisingSelectLinkTypePopupTemplate.html'
], function ($, _, Backbone, modalPopup, merchandisingSelectLinkTypePopupTemplate) {

    var merchandisingDrawingPopupView = Backbone.View.extend({
        initialize: function(options) {
            this.relationTypes = options.relationTypes;
            this.render();
        },
        events: {

        },
        render: function () {
            this.$el.html(_.template(merchandisingSelectLinkTypePopupTemplate, {}));

            var selectEl = this.$el.find("select");
            _.each(this.relationTypes, function(relationType) {
                selectEl.append($("<option>" + relationType.Id + "</option>", {
                    value: relationType.Id
                }));
            });

           return this; // enable chained calls
        }
    });

    return merchandisingDrawingPopupView;
});
