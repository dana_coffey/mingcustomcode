﻿define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-steps',
    'dropzone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/appheader/uploadMediaTemplate.html',
    'models/fieldtype/fieldTypeModel',
    'sharedjs/models/fieldtype/FieldTypeSettingModel'
], function (
    $,
    _,
    backbone,
    jquerySteps,
    Dropzone,
    inRiverUtil,
    uploadMediaTemplate,
    fieldTypeModel,
    fieldTypeSettingModel) {

    var uploadMediaView = backbone.View.extend({
        initialize: function (options) {
            this.fieldTypeSetting = new fieldTypeSettingModel("ResourceFilename");
            this.fieldTypeSetting.fetch({
                success: function (model, response) {
                    // We only want to fill this.attributes for the Model-object.
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.render();
        },
        onCancel: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {
            var self = this;
            this.$el.html(uploadMediaTemplate);

            this.myAwesomeDropzone = new Dropzone(this.$el.find("#media-dropzone")[0], {
                url: "/imageupload/file",
                maxFilesize: 1000,
                parallelUploads: 10,
                thumbnailWidth: 100,
                thumbnailHeight: 100,
                autoProcessQueue: false,
                dictDefaultMessage: "Drop files here or Click to Browse",
                init: function () {
                    var thisDropzone = this;
                    var entityResourceArray = new Array();

                    this.on("processing", function () {
                        thisDropzone.options.autoProcessQueue = true;
                    });

                    this.on("drop", function (file) {
                        // handle drop ?
                    });

                    this.on("addedfile", function (file) {

                        self.$el.find("#media-dropzone").removeClass("dropzone-button");
                        self.$el.find("#media-dropzone").addClass("dropzone");
                        //$("#drop-zone").addClass("bbm-modal");
                        self.$el.find("#dropzone-buttons").show();
                        self.$el.find("#drop-zone-message").hide();
                    });

                    this.on("success", function (file, res) {
                        console.log("successfully uploaded file");

                        if (res > 0) {
                            entityResourceArray.push(res);
                            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                                setTimeout(function () {
                                    thisDropzone.removeAllFiles();
                                    var entities = entityResourceArray.join(',');

                                    $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                                        self.onCancel();
                                        self.goTo("workarea?title=My Uploaded Resources&compressedEntities=" + compressed);
                                    });
                                }, 500);
                            }
                        } else {
                            file.previewElement.classList.add("dz-error");
                            file.previewElement.classList.remove("dz-success");

                            var ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                            var node;
                            var results = [];
                            for (var i = 0, len = ref.length; i < len; i++) {
                                node = ref[i];
                                results.push(node.textContent = "Filename already exists");
                            }
                            return results;
                        }
                    });

                    this.on("error", function (file, reason) {
                        var text = reason;

                        // Check if error is from server-side (else internal from Dropzone-component)
                        if (file.xhr != undefined) {
                            text = file.xhr.statusText;
                        }

                        inRiverUtil.NotifyError("Error when selecting file", text);
                    });

                    var submitButton = self.$el.find("#start-upload");
                    submitButton.on("click", function (e) {
                        thisDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    });

                    var closeButton = self.$el.find("#close-upload");
                    closeButton.on("click", function (e) {
                        thisDropzone.removeAllFiles();
                        $("#media-dropzone").removeClass("dropzone");
                        $("#media-dropzone").addClass("dropzone-button");
                        $("#dropzone-container").removeClass("bbm-wrapper");
                        $("#drop-zone2").removeClass("bbm-modal");
                        $("#ddrop-zone2 #dropzone-buttons").hide();
                    });
                },
                accept: function (file, done) {

                    if (!self.fieldTypeSetting.ValidateRegExp(file.name))
                    {
                        done("Filename '" + file.name + "' has illegal characters");
                        return;
                    }
                    
                    done();
                }
            });

            return this;
        }
    });

    return uploadMediaView;
});
