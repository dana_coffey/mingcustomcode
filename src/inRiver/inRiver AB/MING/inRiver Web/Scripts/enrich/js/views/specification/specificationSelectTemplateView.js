﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
   'sharedjs/misc/inRiverUtil',

   'collections/entities/entityCollection',
  'text!templates/specification/specificationSelectTemplateTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, entityCollection, specificationSelectTemplateTemplate) {

    var specificationSelectTemplateView = backbone.View.extend({
        initialize: function (options) {
            var that = this;

            this.templateId = options.template;
            this.currentEntityId = options.currentEntityId;
            this.currentEntityTypeId = options.currentEntityTypeId;

            this.collection = new entityCollection();
            this.collection.url = '/api/specification/gettemplates?templateId=' + this.templateId;

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.collection.fetch({
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //when selection made => highlight row, enable save button
        },
        events: {
            "click #save-form-button": "onSave",
            "click #cancel-form-button": "onCancel",
            "click .copy-from-content-row": "onRowSelected",
            "keyup #nameFilterInput": "onContentFiltering",
            "keyup #descriptionFilterInput": "onContentFiltering"
        },
        onRowSelected: function (e) {
            e.stopPropagation();
            this.id = e.currentTarget.id;
            $(".copy-from-content-row").each(function (i, x) {
                $(x).removeClass("copy-from-selected");
            });

            $("#" + this.id).addClass("copy-from-selected");
            $("#save-form-button").removeAttr("disabled");
        },
        onSave: function () {
            var that = this;
            if (this.id && this.id != 0) {
                $.get("/api/specification/settemplate?entityId=" + this.currentEntityId + '&entityTypeId=' + this.currentEntityTypeId + "&templateId=" + this.id).done(function () {
                    appHelper.event_bus.trigger('entityupdated', that.currentEntityId);
                });
                that.done();
            }
        },
        onCancel: function () {
            this.done();
        },
        onContentFiltering: function (e) {

            var nameFilterString = this.$el.find("#nameFilterInput").val().toLowerCase();
            var descriptionFilterString = this.$el.find("#descriptionFilterInput").val().toLowerCase();

            this.$el.find(".copy-from-content-row").show();

            this.$el.find(".copy-from-content-row").each(function (i, x) {

                if (nameFilterString && nameFilterString.length > 0) {
                    if($(x).attr("filter-str-name").indexOf(nameFilterString) < 0) $(x).hide();
                }

                if (descriptionFilterString && descriptionFilterString.length > 0) {
                    if ($(x).attr("filter-str-desc").indexOf(descriptionFilterString) < 0) $(x).hide();
                }
            });
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            backbone.View.prototype.remove.call(this);
        },
        render: function () {

            var that = this;

            this.$el.html(_.template(specificationSelectTemplateTemplate, { templateId: that.templateId, entities: that.collection.toJSON() }));

            return that; // enable chained calls
        },
    });

    return specificationSelectTemplateView;
});

