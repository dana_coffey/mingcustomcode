﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var entityOverviewModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/entityOverview'
    });

    return entityOverviewModel;

});
