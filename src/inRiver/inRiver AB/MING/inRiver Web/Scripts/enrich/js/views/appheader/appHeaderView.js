﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/views/workarea/quickSearchView',
  'views/appheader/appContextMenuView'
], function ($, _, Backbone, quickSearchView, appContextMenuView) {

    var appHeaderView = Backbone.View.extend({
        initialize: function (options) {
            this.render();
        },
        render: function () {
            this.listenTo(appHelper.event_bus, 'userupdated', this.render);
            $("#context-menu-container").html(new appContextMenuView().el);
            $("#search-container").html(new quickSearchView().el);
            return this;
        }
    });

    return appHeaderView;

});
