﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/setting/SettingModel',
  'sharedjs/models/comment/commentModel',
  'modalPopup',
  'jquery-ui-touch-punch',
  'sharedjs/views/panelsplitter/panelBorders',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/entityview/entityCustomEditorWrapperView',
  'views/entityview/entityOverviewView',
  'views/entityview/entityInfoView',
  'sharedjs/views/lightboard/lightboardView',
  'sharedjs/views/comment/commentsView',
  'views/relation/relationView',
  'views/merchandising/merchandisingView',
  'views/entityview/entityChannelView',
  'views/specification/specificationView',
  'views/specification/specificationTemplateView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/copyEntity/copyEntityWizzardView',
  'sharedjs/views/templateprinting/TemplatePrintingSetupView',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'models/fieldtype/fieldTypeModel',
  'text!templates/entity/entityTemplate.html'
], function ($, _, backbone, relationTypeCollection, entityModel, completenessModel, relationModel, settingModel, commentModel, modalPopup, jqtp, panelBorders, entityDetailSettingWrapperView, entityCustomEditorWrapperView,
    entityOverviewView, entityInfoView, lightboardView, commentsView, relationView, merchandisingView, entityChannelView, specificationView, specificationTemplateView, completenessPopupView,
    copyEntityWizzardView, templatePrintingSetupView, inRiverUtil, permissionUtil, fieldTypeModel, entityTemplate) {

    var entityView = backbone.View.extend({
        initialize: function (options) {
            var that = this;

            var comment = new commentModel();
            comment.url = "/api/comment/exists/" + options.id;

            comment.fetch({
                async: false,
                success: function (model, response) {
                    if (response) {
                        that.commentsExists = true;
                    } else {
                        that.commentsExists = false;
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            //Ladda om entitetet, för vi vet ju för lite!
            var onDataHandler = function () {
                that.render();
            };

            if (this.model) {
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                var id = options.id;
                this.model = new entityModel(id);
                this.model.url = "/api/entity/" + id;
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            this.hasEmptyWorkArea = options.hasEmptyWorkArea;

            this.listenTo(window.appHelper.event_bus, 'entityupdated', this.entityUpdated);

            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.onEntityDeleted);
        },
        events: {
            //"click #card-completeness-text": "onShowCompletnessDetails",
            //"click #progress-rect-percentage": "onShowCompletnessDetails",
            "click .completeness": "onShowCompletnessDetails",
            "click #tab-overview": "onChangeTab",
            "click #tab-specification": "onChangeTab",
            "click #tab-details": "onChangeTab",
            "click .custom-editor": "onChangeTab",
            "click #tab-media": "onChangeTab",
            "click #tab-includes": "onChangeTab",
            "click #tab-includedin": "onChangeTab",
            "click #tab-merchandising": "onChangeTab",
            "click #tab-channels": "onChangeTab",
            "click #system-button": "onShowSystemInfo",
            "click #edit-button": "onEditDetail",
            "click #lock-button": "onEntityLock",
            "click #unlock-button": "onEntityUnlock",
            "click #star-button": "onEntityStar",
            "click #delete-button": "onEntityDelete",
            "click #closeentity": "onEntityClose",
            "click #add-task-button": "onAddTask",
            "click #inboundarea .showAll": "gotoIncludedInTab",
            "click #outboundarea .showAll": "gotoIncludesTab",
            "click #entity-copy-button": "onCopyEntityClick",
            "click #entity-comments-button": "gotoComments",
            "click #pdf-button": "onDownloadPDF",
            "click #preview-button": "onShowPreview",
        },
        onAddTask: function (e) {
            e.stopPropagation();

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });
        },
        taskAddedNowLinkEntity: function (model) {
            var self = this;

            this.sourceId = model.attributes.Id;
            this.targetId = this.model.id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function () {
                    inRiverUtil.Notify("Relation successfully created");
                    self.entityUpdated(self.sourceId);
                    self.entityUpdated(self.targetId);
                },
                error: function (error, response) {
                    inRiverUtil.OnErrors(error, response);
                }
            });
        },
        onEntityClose: function () {
            window.appHelper.event_bus.trigger('entityareaclose');
            this.remove();
            this.unbind();
        },
        onEntityDelete: function () {
            inRiverUtil.deleteEntity(this);
        },
        onEditDetail: function () {

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });

        },
        onEntityLock: function () {
            var self = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#lock-button").hide();
                    $("#entitylocked").show();
                    $("#entitylocked").attr("title", "Locked by " + window.appHelper.userName);
                    $("#unlock-button").show();

                    self.entityUpdated(self.model.id);
                }
            }
            xmlhttp.open("GET", "/api/tools/lockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityUnlock: function () {
            var self = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#lock-button").show();
                    $("#entitylocked").hide();
                    $("#unlock-button").hide();

                    self.entityUpdated(self.model.id);
                }
            };

            xmlhttp.open("GET", "/api/tools/unlockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityStar: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    self.entityUpdated(self.model.id);
                }

            }
            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");

                e.currentTarget.title = "Star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star");
                $(e.currentTarget).addClass("icon-entypo-fix-star-empty");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");

                e.currentTarget.title = "Un-star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star-empty");
                $(e.currentTarget).addClass("icon-entypo-fix-star");
            }

        },
        onShowSystemInfo: function () {

            inRiverUtil.ShowEntityInfo(this.model.id);

        },
        createOverviewTabView: function () {
            var contentSubView = new entityOverviewView({
                model: this.model
            });
            return contentSubView;
        },
        createDetailsTabView: function () {
            var contentSubView = new entityDetailSettingWrapperView({
                model: this.model
            });
            return contentSubView;
        },
        createCustomEditorTabView: function (templateId) {
            var that = this;
            if (!_.find(appData.editTemplates, function (t) {
                return t.Id == templateId && t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, that.model.get("EntityType"));
            })) {
                // template not valid for this entity type or doesn't exist
                return null;
            }
            var contentSubView = new entityCustomEditorWrapperView({
                model: this.model,
                templateId: templateId
            });
            return contentSubView;
        },
        createSpecificationTabView: function () {
            var contentSubView;
            if (this.model.get("EntityType") == "Specification") {
                contentSubView = new specificationTemplateView({ // Edit specification template
                    Name: "Specification",
                    id: this.model.id,
                    type: this.model.get("EntityType"),
                    displayName: this.model.get("DisplayName")
                });
            } else {
                contentSubView = new specificationView({
                    Name: "Specification",
                    id: this.model.id,
                    type: this.model.get("EntityType")
                });
            }

            return contentSubView;
        },
        createLightboardTabView: function () {
            var contentSubView = new lightboardView({
                Name: "Lightboard",
                id: this.model.id,
                type: this.model.attributes.EntityType
            });
            return contentSubView;
        },
        createIncludesTabView: function () {
            var contentSubView = new relationView({
                Name: "Includes",
                id: this.model.id,
                type: this.model.attributes.EntityType,
                direction: "outbound"
            });
            return contentSubView;
        },
        createIncludedInTabView: function () {
            var contentSubView = new relationView({
                Name: "Included In",
                id: this.model.id,
                type: this.model.attributes.EntityType,
                direction: "inbound"
            });
            return contentSubView;
        },
        createMerchandisingInTabView: function () {
            var contentSubView = new merchandisingView({
                Name: "Merchandising",
                id: this.model.id,
                type: this.model.attributes.EntityType,
            });
            return contentSubView;
        },
        createChannelsTabView: function () {
            var contentSubView = new entityChannelView({
                Name: "Channels",
                id: this.model.id
            });
            return contentSubView;
        },
        gotoLightboardTab: function () {
            window.appSession.entityViewTabId = "tab-includes";
            this.createTabView(window.appSession.entityViewTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTab();
        },
        gotoIncludesTab: function () {
            window.appSession.entityViewTabId = "tab-includes";
            this.createTabView(window.appSession.entityViewTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTab();
        },
        gotoIncludedInTab: function () {
            window.appSession.entityViewTabId = "tab-includedin";
            this.createTabView(window.appSession.entityViewTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTab();
        },
        gotoOverviewTab: function () {
            window.appSession.entityViewTabId = "tab-overview";
            this.createTabView(window.appSession.entityViewTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);
            this.indicateActiveTab();
        },
        onCopyEntityClick: function (e) {
            e.stopPropagation();
            this.modal = new modalPopup();
            this.modal.popOut(new copyEntityWizzardView({ id: this.model.id, entityTypeId: this.model.attributes.EntityType, model: this.model }), { size: "medium", header: "Copy " + this.model.attributes.DisplayName });
        },
        gotoComments: function () {
            var modal = new modalPopup();
            modal.popOut(new commentsView({ id: this.model.id }), { size: "medium", header: "Comments for " + this.model.attributes.DisplayName, usesavebutton: false });
        },
        onShowPreview: function (e) {
            e.stopPropagation();
            inRiverUtil.openEntityPreview(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPreviewTemplates);
        },
        onDownloadPDF: function (e) {
            e.stopPropagation();
            inRiverUtil.createEntityPDF(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPDFTemplates);
        },
        onChangeTab: function (e) {
            var self = this;
            if (window.appSession.entityViewToTabId == e.currentTarget.id) {
                return;
            }

            inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(this._currentTopTabView, function () {
                if (e.currentTarget.id == "tab-merchandising") {
                    var fieldType = new fieldTypeModel({ id: "ResourceImageMap", fieldTypeId: "ResourceImageMap" });
                    fieldType.url = "/api/fieldtype/?fieldTypeId=ResourceImageMap";
                    fieldType.fetch({
                        success: function (model) {
                            if (model.get("datatype") != null) {
                                window.appSession.entityViewTabId = e.currentTarget.id; // remember default tab
                                var url = window.appSession.entityViewTabId.replace("tab-", "");
                                self.goTo("entity/" + self.id + "/" + url, { trigger: false });
                                self.createTabView(window.appSession.entityViewTabId);
                                self.$el.find("#entity-top-info-container").append(self._currentTopTabView.el);
                                self.indicateActiveTab();
                            } else {
                                inRiverUtil.NotifyError("Field missing", "The current model is not ready for Merchandising.", "The field 'ResourceImageMap' on Resource are missing");
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                    return;
                }

                window.appSession.entityViewTabId = e.currentTarget.id; // remember default tab
                var urlTabName = window.appSession.entityViewTabId.replace("tab-", "");
                self.goTo("entity/" + self.id + "/" + urlTabName, { trigger: false });
                self.createTabView(window.appSession.entityViewTabId);
                self.$el.find("#entity-top-info-container").append(self._currentTopTabView.el);
                self.indicateActiveTab();
            });
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            if (this._currentTopTabView == null) {
                return false;
            }

            // Delegate decision to current subview


            if (_.isFunction(this._currentTopTabView.hasUnsavedChanges)) {
                return this._currentTopTabView.hasUnsavedChanges();
            }
            return false;
        },
        createTabView: function (idOfTab) {
            if (this._currentTopTabView) {
                this._currentTopTabView.remove();

                if (this._currentTopTabView.detailfields) {
                    for (var index = 0; index < this._currentTopTabView.detailfields.length; index++) {
                        this._currentTopTabView.detailfields[index].stopListening(window.appHelper.event_bus);
                        this._currentTopTabView.detailfields[index].remove();
                        this._currentTopTabView.detailfields[index].unbind();
                    }
                }
                this._currentTopTabView.close();
                this._currentTopTabView = null;
            }

            if (idOfTab == "tab-details") {
                this._currentTopTabView = this.createDetailsTabView();
            } else if (/^tab-custom-editor-[0-9]*$/i.test(idOfTab)) {
                this._currentTopTabView = this.createCustomEditorTabView(idOfTab.match(/-([0-9]*)$/)[1]);
            } else if (idOfTab == "tab-specification") {
                this._currentTopTabView = this.createSpecificationTabView();
            } else if (idOfTab == "tab-media") {
                this._currentTopTabView = this.createLightboardTabView();
            }
            else if (idOfTab == "tab-includes") {
                this._currentTopTabView = this.createIncludesTabView();
            }
            else if (idOfTab == "tab-includedin") {
                this._currentTopTabView = this.createIncludedInTabView();
            }
            else if (idOfTab == "tab-merchandising") {
                this._currentTopTabView = this.createMerchandisingInTabView();
            }
            else if (idOfTab == "tab-channels") {
                this._currentTopTabView = this.createChannelsTabView();
            }

            // default
            if (!this._currentTopTabView) {
                this._currentTopTabView = this.createOverviewTabView();
            }
        },
        indicateActiveTab: function () {
            this.$el.find(".control-row-tab-button").removeClass("control-row-tab-button-active");
            this.$el.find("#" + window.appSession.entityViewTabId).addClass("control-row-tab-button-active");
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        onClose: function () {
            this.stopListening(window.appHelper.event_bus);
        },
        entityUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch({
                    success: function () {
                        self.rerender = true;
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        drawCompleteness: function (percent) {
            this.$el.find('.percent').html(Math.round(percent) + '%');

            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        onEntityDeleted: function (id) {
            if (id == this.model.id) {
                this.onEntityClose();
            }
        },
        render: function () {
            var self = this;
            console.log("Render Entity View");

            if (this.model.attributes.EntityType == undefined) {
                inRiverUtil.NotifyError("No data found", "No entity found.<br/><br/>Please verify that it actually exists and try again.");
                this.$el.html();
                return this;
            }

            var icon = this.model.attributes.EntityType;
            if (icon == "Channel" && !this.model.attributes.ChannelPublished) {
                icon = "ChannelUnpublished";
            }

            var previewTemplateIsAvailable = false;
            if (_.find(appData.enrichPreviewTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, self.model.get("EntityType"));
            })) {
                previewTemplateIsAvailable = true;
            }

            var pdfTemplateIsAvailable = false;
            if (_.find(appData.enrichPDFTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, self.model.get("EntityType"));
            })) {
                pdfTemplateIsAvailable = true;
            }

            var availableEditTemplates = _.filter(appData.editTemplates, function (t) {
                var isValid = t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, self.model.get("EntityType"));
                if (isValid && t.Properties && t.Properties.Roles !== undefined) {
                    isValid = _.intersection(t.Properties.Roles, appHelper.roles).length > 0;
                }
                return isValid;
            });

            this.$el.html(_.template(entityTemplate, {
                data: this.model.toJSON(), userName: window.appHelper.userName, icon: icon, commentsExists: self.commentsExists, previewTemplateIsAvailable: previewTemplateIsAvailable, pdfTemplateIsAvailable: pdfTemplateIsAvailable, editTemplates: availableEditTemplates
            }));

            // Activate merchandising tab?
            if (_.contains(appData.merchandisingEntityTypes, this.model.get("EntityType"))) {
                this.$el.find("#tab-merchandising").show();
            } else {
                this.$el.find("#tab-merchandising").hide();
                if (window.appSession.entityViewTabId === "tab-merchandising") {
                    this.gotoOverviewTab(); // merchandising not allowed so go to overview
                }
            }

            this.createTabView(window.appSession.entityViewTabId);
            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);

            this.indicateActiveTab();

            if (window.appHelper.hasTask != undefined) {

                if (!window.appHelper.TaskRelationTypes) {
                    this.$el.find("#add-task-button").hide();
                }

            } else {
                this.relationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: "Task" });

                this.relationTypes.fetch({
                    success: function () {

                        self.TaskRelationType = _.find(self.relationTypes.models, function (model) {
                            return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                        });
                        if (!self.TaskRelationType) {
                            self.$el.find("#add-task-button").hide();
                        }
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

            // Set borders, splitters and collapse
            panelBorders.initAllEvents();

            var collapsed = sessionStorage.getItem("workarea-collapsed");

            if (collapsed && collapsed.toString() == "true") {
                if ($("#workarea-header-label").hasClass("workarea-header-label-horisontal")) {
                    $("#workarea-header-toggle-button").click();
                }
            };

            var callback = function (access, id, that) {
                if (!access) {
                    that.$el.find(id).hide();
                }
            };

            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#edit-button"));
            permissionUtil.CheckPermissionForElement("DeleteEntity", this.$el.find("#delete-button"));
            permissionUtil.CheckPermissionForElement("AddEntity", this.$el.find("#add-task-button"));
            permissionUtil.CheckPermissionForElement("LockEntity", this.$el.find("#lock-button"));
            permissionUtil.CheckPermissionForElement("LockEntity", this.$el.find("#unlock-button"));
            permissionUtil.CheckPermissionForElement("CopyEntity", this.$el.find("#entity-copy-button"));
            this.drawCompleteness(this.model.attributes.Completeness);
            return this; // enable chained calls
        }
    });

    return entityView;
});