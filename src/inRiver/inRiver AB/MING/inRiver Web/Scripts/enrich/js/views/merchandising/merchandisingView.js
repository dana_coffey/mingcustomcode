﻿define([
  'jquery',
  'underscore',
  'backbone',
  'inriver-imagemap',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/collections/resources/resourceCollection',
  'sharedjs/models/entity/entityOverviewModel',
  //'sharedjs/collections/relations/relationTypeCollection',
  'modalPopup',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/entity/entityModel',
  'views/merchandising/merchandisingDrawingPopupView',
  'views/merchandising/merchandisingSelectLinkTypePopupView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'views/relation/relationCardOverviewView',
  'text!sharedtemplates/relation/relationTemplate.html',
  'text!sharedtemplates/relation/relationTypeGroupTemplate.html',
  'text!sharedtemplates/relation/relationTypeLinkEntityGroupTemplate.html',
  'text!sharedtemplates/entity/entityOverviewRelationTemplate.html',
  'text!templates/merchandising/merchandisingTemplate.html',
  'text!templates/merchandising/merchandisingMediaTemplate.html',
  'text!templates/merchandising/merchandisingAreaItemTemplate.html',
  'text!templates/merchandising/merchandisingAddAreaItemTemplate.html'
], function ($, _, backbone, inriverImageMap, completenessModel, resourceCollection, entityOverviewModel, modalPopup, inRiverUtil, permissionUtil, relationModel, entityModel, merchandisingDrawingPopupView, merchandisingSelectLinkTypePopupView, entityDetailSettingView, completenessPopupView, contextMenuView, relationCardView, relationTemplate, relationTypeGroupTemplate, relationTypeLinkEntityGroupTemplate, entityOverviewRelationTemplate, merchandisingTemplate, merchandisingMediaTemplate, merchandisingAreaItemTemplate, merchandisingAddAreaItemTemplate) {

    var merchandisingView = backbone.View.extend({

        initialize: function (options) {
            var that = this;
            this.template = _.template(merchandisingTemplate, {});

            this.id = options.id;
            this.entityTypeId = options.type;

            // Fetch stuff we need to start the rendering
            this.overviewModel = new entityOverviewModel({ id: options.id }, { url: "/api/entityOverview/" + options.id });
            this.overviewModel.fetch({
                success: function (model) {
                    if (model.get("Resources").length > 0) {
                        $.getJSON("/api/linktype?sourceEntityTypeId=" + that.entityTypeId, function (data) {
                            // Create selector for accepted droppable entity types
                            that.acceptedTargetEntityTypes = _.pluck(data, "TargetEntityTypeId");
                            var tmpSelectors = [];
                            _.each(that.acceptedTargetEntityTypes, function (entityType) {
                                tmpSelectors.push("[entity-type-id='" + entityType + "']");
                            });
                            that.dropAcceptSelector = tmpSelectors.join(",");

                            // Render and and load upp an image in the editor
                            that.loadImageMapEditor(model.get("Resources")[0].Id);
                        });
                    }

                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.droppable = permissionUtil.CheckPermission("AddLink");
            this.sortable = permissionUtil.CheckPermission("UpdateLink");

            this.$el.html("<div id=\"loading\"><img src=\"/Images/nine-squares-32x32.gif\" />   <div><h2>Loading data</h2></div></div>");
        },
        events: {
            "click .merchandising-media-item": "onSelectResource",
            "click #add-spot": "onAddSpot",
            "click #add-polygon": "onAddPolygon",
            "click #merchandising-delete-area": "onDeleteArea"
        },
        onClose: function () {
        },
        onSelectResource: function (e) {
            this.loadImageMapEditor(e.currentTarget.id);
        },
        onAddPolygon: function () {
            this.openDrawingPopup("add-polygon");
        },
        onAddSpot: function () {
            this.openDrawingPopup("add-spot");
        },
        onDeleteArea: function (e) {
            var that = this;
            inRiverUtil.NotifyConfirm("Confirm Delete Area", "Do you want to delete this area? <br><br>(Note that the link for this entity will not be removed.)", function () {
                var areaIx = _.find(e.currentTarget.attributes, function (a) { return a.name == "data-id"; }).value;
                that.imageMapData.areas = _.reject(that.imageMapData.areas, function (a) { return a.index == areaIx; });
                that.saveImageMapData();
                that.rebindImageMapster();
            });
        },
        openDrawingPopup: function (action) {
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new merchandisingDrawingPopupView({
                resourceFileId: this.resourceFileId,
                pendingAction: action,
            }), {
                header: "Merchandising",
                size: "large",
                onSave: this.onDrawingPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onDrawingPopupSave: function (popup, childView) {
            // Convert the drawing state to the persistance structure
            if (childView.drawingState.pendingVertices) {
                var relativeCoordSpaceVertices = [];
                for (var i in childView.drawingState.pendingVertices) {
                    relativeCoordSpaceVertices.push(this.toRelativeCoordSpace(childView.drawingState.pendingVertices[i], childView.drawingState.width, childView.drawingState.height));
                }
                this.imageMapData.areas.push({
                    "type": "poly",
                    "coords": this.createImageMapCoordsString(relativeCoordSpaceVertices),
                });
            }
            if (childView.drawingState.pendingSpot) {
                this.imageMapData.areas.push({
                    "type": "spot",
                    "coords": this.createImageMapCoordsString([this.toRelativeCoordSpace(childView.drawingState.pendingSpot, childView.drawingState.width, childView.drawingState.height)]),
                });
            }

            // Store and clean up
            popup.close();
            this.saveImageMapData(); // Store to server
            appHelper.event_bus.trigger('closepopup');
            this.rebindImageMapster(); // Display the new area we just created
        },
        loadImageMapEditor: function (resourceId) {
            var that = this;

            // Hide the image element while setting things up (to avoid things jumping around)
            this.$el.find("#merchandising-image-wrap").css('visibility', 'hidden');

            // Indicate selected resource
            this.$el.find(".merchandising-media-item").removeClass("merchandising-media-item-selected");
            this.$el.find("#" + resourceId).addClass("merchandising-media-item-selected");

            // Remove image map for previous resource
            var imgEl = that.$el.find("#merchandising-image");
            InRiverImageMap.removeImageMap(imgEl);

            // Fetch data for displaying the image map editor
            var tmp = new backbone.Model();
            tmp.idAttribute = "Id";
            tmp.url = "/api/entityfields/" + resourceId;
            tmp.fetch({
                data: $.param({ includeHiddenFields: true }),
                success: function (model) {
                    that.entityFieldModel = model;
                    var tmp2 = new backbone.Model(null, { idAttribute: "Id" });
                    tmp2.url = "/api/resource/" + resourceId;
                    tmp2.fetch({
                        success: function (model2) {
                            // We got all we need! success!
                            that.resourceId = resourceId;
                            that.resourceFileId = model2.get("ResourceFileId");
                            that.imageMapFieldEntry = _.find(that.entityFieldModel.get("Fields"), function (f) { return f.FieldType == "ResourceImageMap"; });

                            // Run init routine after image is loaded
                            imgEl.one("load", function (e) {
                                that.rebindImageMapster();

                                // Register the image as an drop target
                                imgEl.droppable({
                                    accept: that.dropAcceptSelector,
                                    tolerance: "pointer", // make sure that the drop will be made at the cursor position and not in the center of the draggable element
                                    drop: function (e, ui) {
                                        // set z-index to the highest, so it can be dragged again
                                        //$(ui.draggable).css('z-index', 201);

                                        var droppedEntityTypeId = ui.draggable.attr("entity-type-id");

                                        // returns the mapKey value of the currently highlighted item
                                        var areaKey = imgEl.mapster('highlight');
                                        if (areaKey.split("-")[1] != "dummy") { // the dummy area is usually the drop target when no true area is the drop target (workaround for limitations in ImageMapster)
                                            var droppedEntityId = ui.draggable[0].id.replace("card-", "");
                                            var areaIndex = areaKey.split("-")[1];
                                            that.assignEntityToArea(areaIndex, droppedEntityId, droppedEntityTypeId);
                                        }
                                    }
                                });
                            });
                            imgEl.attr("src", "../picture?id=" + that.resourceFileId + "&size=Preview");
                        },
                        error: function (model2, response) {
                            inRiverUtil.OnErrors(model2, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        determineRelationType: function (sourceEntityTypeId, targetEntityTypeId, successFn) {
            $.getJSON("/api/linktype?sourceEntityTypeId=" + sourceEntityTypeId + "&targetEntityTypeId=" + targetEntityTypeId, function (data) {
                successFn(data);
            });
        },
        assignEntityToArea: function (areaIndex, addedEntity, addedEntityTypeId) {
            var that = this;
            this.determineRelationType(this.entityTypeId, addedEntityTypeId, function (relationTypes) {
                if (relationTypes.length == 0) {
                    // Do nothing (should never end up here anyway at this point)
                } else if (relationTypes.length == 1) {
                    that.createRelationAndAssignToImageMapArea(relationTypes[0].Id, addedEntity, areaIndex);
                } else {
                    var modal = new modalPopup({ callbackContext: this });
                    modal.popOut(new merchandisingSelectLinkTypePopupView({
                        relationTypes: relationTypes,
                    }), {
                        enableSaveButton: true,
                        size: "small",
                        header: "Select Link Type",
                        onSave: function (popup, childView) {
                            var relationType = childView.$el.find("#merchandising-link-type-select").val();
                            popup.close();
                            that.createRelationAndAssignToImageMapArea(relationType, addedEntity, areaIndex);
                        },
                        onCancel: function (popup, childView) {
                            popup.close();
                        }
                    });
                }
            });
        },
        createRelationAndAssignToImageMapArea: function (relationTypeId, addedEntity, areaIndex) {
            var that = this;

            // User feedback for indicating that we are doing some work...
            this.$el.find(".merchandising-area-item[data-id='" + areaIndex + "']").animate({ opacity: 0.1 });

            // Create relation
            var newModel = new relationModel({ LinkType: relationTypeId, SourceId: this.id, TargetId: addedEntity });
            newModel.save([], {
                success: function (model) {
                    // Assign an entity id to the area
                    var area = _.find(that.imageMapData.areas, function (a) { return a.index == areaIndex; });
                    area.entityId = addedEntity;
                    that.saveImageMapData();

                    // Reload the entire overview model so we can reflect the result of the new relation
                    that.overviewModel.fetch({
                        success: function (model) {
                            that.rebindImageMapster();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        toDisplayCoordSpace: function () {

        },
        toRelativeCoordSpace: function (point, width, height) {
            return { x: point.x / width, y: point.y / height }
        },
        createImageMapCoordsString: function (coords) {
            var s = "";
            _.each(coords, function (c) {
                s += c.x + "," + c.y + ",";
            });
            s = s.substring(0, s.length - 1); // remove last comma
            return s;
        },
        saveImageMapData: function () {
            // Set correct index on each area
            for (var i = 0; i < this.imageMapData.areas.length; i++) {
                this.imageMapData.areas[i].index = i;
            }

            this.imageMapFieldEntry.Value = JSON.stringify(this.imageMapData);

            // The API is a bit inconsistent so had to use pure AJAX call instead
            $.ajax({
                type: "POST",
                url: "/api/entityfields/" + this.resourceId + "/ResourceImageMap",
                data: this.imageMapFieldEntry,
                //success: success,
                dataType: "application/json"
            });
        },
        rebindImageMapster: function () {
            // Update image mapster (hide the whole area while settings up to avoid flicker)
            var that = this;
            this.$el.find("#merchandising-image-wrap").css('visibility', 'hidden');
            var imgEl = this.$el.find("#merchandising-image");
            imgEl.mapster("unbind");
            this.initImageMapster(imgEl, function () {
                that.$el.find("#merchandising-image-wrap").css('visibility', 'visible');
            });

            // Update the area list
            this.renderAreaList();
        },
        initImageMapster: function (imgEl, successFn) {
            var that = this;

            // Check validity of the image map data, it will be reset to empty if failed
            try {
                var tmp = JSON.parse(this.imageMapFieldEntry.Value);
                if (typeof tmp == 'object') {
                    this.imageMapData = tmp;
                } else {
                    this.imageMapData = null;
                }
            } catch (err) {
                this.imageMapData = null;
            }

            // If there is no image map data, create an empty structure to use "in memory"
            if (!this.imageMapData) {
                this.imageMapData = {};
                this.imageMapData.id = this.resourceId;
                this.imageMapData.areas = [];
            }

            var inRiverImageMapOptions = {
                imageMapData: this.imageMapData,
                disableSpotRendering: true,   // Spots will be rendered as areas
                //spotImageUrl: "/images/spot-image.png",
                //spotSize: 50, 
                keepAreasHighLighted: true,
                useDummyArea: true, // Used to determine areas when used as a drop target
                //renderHighlightForSpots: {
                //    fill: false,
                //    stroke: true,
                //    strokeColor: '777777',
                //    strokeOpacity: 0.7,
                //    strokeWidth: 2,
                //},
                onClick: function(area) {
                    //alert(JSON.stringify(area));
                },
                onMouseOver: function (area) {
                    that.$el.find("#merchandising-area-list li[data-id='" + area.index + "']").addClass("merchandising-area-item-highlighted");
                },
                onMouseOut: function (area) {
                    that.$el.find("#merchandising-area-list li[data-id='" + area.index + "']").removeClass("merchandising-area-item-highlighted");
                },
                onConfigured: function () {
                    // this will be called only after ImageMapster is done setting up
                    // Adjust z-order to support drag n drop of entities
                    imgEl.siblings().css('z-index', 0);
                    imgEl.css('z-index', 200);
                    successFn();
                }
            }

            InRiverImageMap.initImageMap(imgEl, inRiverImageMapOptions);
        },
        renderAreaList: function () {
            var that = this;
            var ulEl = this.$el.find("#merchandising-area-list");
            ulEl.empty();

            _.each(this.imageMapData.areas, function (area) {
                var relationForThisArea = _.find(that.overviewModel.get("OutboundRelations"), function (r) {
                    return r.TargetId == area.entityId;
                });

                var itemEl = $($.parseHTML(_.template(merchandisingAreaItemTemplate, { area: area })));

                // Render an entity card if we have an assigned entity
                if (relationForThisArea) {
                    var entitycard = new relationCardView({
                        direction: "outbound",
                        relation: relationForThisArea
                    });
                    itemEl.find("#merchandising-entity-card-slot").replaceWith(entitycard.render().el);
                }

                // Drop support
                itemEl.droppable({
                    accept: that.dropAcceptSelector,
                    tolerance: "pointer", // make sure that the drop will be made at the cursor position and not in the center of the draggable element
                    hoverClass: "merchandising-area-item-highlighted",
                    drop: function (e, ui) {
                        var droppedEntityTypeId = ui.draggable.attr("entity-type-id");
                        var areaIndex = itemEl.attr("data-id");
                        var droppedEntityId = ui.draggable[0].id.replace("card-", "");
                        that.assignEntityToArea(areaIndex, droppedEntityId, droppedEntityTypeId);
                    }
                });

                // Hover functionality for indicating areas in the imagemap and in the list
                itemEl.hover(function () {
                    itemEl.addClass("merchandising-area-item-highlighted");
                    InRiverImageMap.removeHighlightFromAllAreas(that.resourceId);
                    InRiverImageMap.highlightArea(that.resourceId, area.index);
                }, function () {
                    itemEl.removeClass("merchandising-area-item-highlighted");
                    InRiverImageMap.highlightAllAreas(that.resourceId);
                });

                ulEl.append(itemEl);
            });

            // Add buttons placeholer
            var addItemEl = $($.parseHTML(_.template(merchandisingAddAreaItemTemplate, {})));
            ulEl.append(addItemEl);
        },
        render: function () {
            var that = this;

            this.$el.html(this.template);

            // Media section
            var container = this.$el.find(".thumbnails-overview");
            if (container.length > 0) {
                $(container)[0].innerHTML = "";
            }
            if (this.overviewModel.get("Resources").length == 0) {
                container.append("<li style=\"margin-top: 5px;\"><i>No media to show</i></li>");
            }
            _.each(this.overviewModel.get("Resources"), function (resource) {
                container.append(_.template(merchandisingMediaTemplate, { Id: resource.Id, DisplayName: resource.DisplayName, PictureUrl: resource.PictureUrl }));
            });

            return this;
        }
    });

    return merchandisingView;
});
