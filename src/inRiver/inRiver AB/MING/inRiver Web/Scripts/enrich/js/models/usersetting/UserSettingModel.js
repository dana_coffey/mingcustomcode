define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var UserSettingModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/usersetting',
        initialize: function () {

        },
        defaults: {
        },
        schema: { // Used by backbone forms extension
        },
        toString: function () {
            return this.get("Id");
        },
    });

    return UserSettingModel;

});
