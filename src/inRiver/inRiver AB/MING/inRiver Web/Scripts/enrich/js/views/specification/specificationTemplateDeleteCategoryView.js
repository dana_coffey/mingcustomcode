﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/misc/inRiverUtil',
    'text!templates/specification/specificationTemplateDeleteCategoryTemplate.html',
    'sharedjs/collections/languages/languagesCollection',
    'sharedjs/extensions/localeStringEditor',
    'sharedjs/collections/cvl/CvlCollection'
], function ($, _, Backbone, inRiverUtil, specificationTemplateDeleteCategoryTemplate, languagesCollection, localeStringEditor, CvlCollection) {

    var specificationTemplateEditFieldView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {

            this.model = new Backbone.Model({ CategoryId: "" });

            this.render();
        },
        events: {
            "click #specification-template-delete-confirm": "onDelete",
            "click #specification-template-delete-cancel": "onCancel",
        },
        init: function () {
            var that = this;

            // Fetch languages (TODO: this should be bootstrapped in to the page for global access)
            this.languages = new languagesCollection();
            this.languages.fetch({
                success: function (e) {
                    window.appHelper.loadingLangages = false;
                    window.appHelper.languagesCollection = e;

                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onDelete: function (e) {
            var that = this;
            this.form.commit();
            var modelToRemove = this.options.categoryCollection.get(this.model.get("CategoryId"));
            modelToRemove.destroy({
                wait: true,
                success: function () {
                    window.appHelper.event_bus.trigger('closepopup');
                    //that.options.categoryCollection.fetch();
                },
                error: function (a, b, c) {
                    if (that.$el.find("#delete-failed").length == 0) {
                        that.$el.find("#form-container").append("<span id='delete-failed'>The category is still in use by a template and can't be deleted.</span>");
                    }
                }
            });
        },
        onCancel: function(e) {
            window.appHelper.event_bus.trigger('closepopup');
        },
        render: function () {
            var that = this;

            this.$el.html(_.template(specificationTemplateDeleteCategoryTemplate, {}));
            
            this.model.schema = {
                "CategoryId": { title: "Category to Delete", type: "Select", options: this.options.categoryCollection },
            };

            // Render user info form
            this.form = new Backbone.Form({
                model: this.model
            }).render();
            this.$el.find("#form-container").html(this.form.el);

            return this; // enable chained calls
        }
    });
    

    return specificationTemplateEditFieldView;
});
