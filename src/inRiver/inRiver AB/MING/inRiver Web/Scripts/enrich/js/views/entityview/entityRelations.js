﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'views/entityview/entityView',
  'sharedjs/views/fieldrevision/fieldRevisionView',
  'text!sharedtemplates/entity/entityDetailTemplate.html',
  'text!sharedtemplates/entity/entityFieldTemplate.html',
  'text!sharedtemplates/entity/entityCategoryTemplate.html'

], function ($, _, Backbone, modalPopup,fieldModel, entityView, fieldRevisionView, entityDetailTemplate, entityFieldTemplate, entityCategoryTemplate) {

    // This file is probably not in use.... remove
    var entityDetailView = Backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.entityModel = options.model;

            // load all fields
            this.model = new fieldModel({ id: this.entityModel.id });
            this.model.fetch(
            {
                success: function () {
                    that.render();
                }
            });
        },
        events: {
            "click #view-history-button": "onViewHistoryClick",
        },
        onViewHistoryClick: function (e, o) {
            e.stopPropagation();

            var fieldTypeId = $(e.currentTarget).attr("fieldtypeid");
            var fieldDataType = $(e.currentTarget).attr("fielddatatype");

            var modal = new modalPopup();
            modal.popOut(new fieldRevisionView({ entityId: this.model.id, fieldTypeId: fieldTypeId, fieldDataType: fieldDataType }), { size: "small", header: "Field History" });
        },
        onClose: function () {
        },
        render: function () {
            var that = this;
            this.$el.html(_.template(entityDetailTemplate, { data: this.entityModel.toJSON() }));

            var categories = _.groupBy(this.model.get("Fields"), "CategoryId");

            //  _.groupBy(this.collection.models, "CategoryId");
            var count = 0;
            var keys = _.keys(categories);
            var i = 0;
            if (keys.length > 1) {

                _.each(keys, function (key) {
                    i = 0;
                    count++;

                    var sectionEl = $(_.template(entityCategoryTemplate, { sectionName: categories[key][0].CategoryName, sectionId: categories[key][0].CategoryId }));
                    var fieldContainer = sectionEl.find("#category-field-container");
                    _.each(categories[key], function (f) {

                        if (!f.IsHidden && (!f.ExcludeFromDefaultView || (f.ExcludeFromDefaultView && f.FieldSets.split(',').indexOf(that.model.get("FieldSet"))))) {
                            var historyId = null;
                            if (f.Revision > 0) {
                                historyId = f.FieldType;
                            }
                            fieldContainer.append(_.template(entityFieldTemplate, { key: f.FieldTypeDisplayName, value: f.DisplayValue, type: f.FieldDataType, isOddRow: i++ % 2 == 1, excludeFromDefaultView: f.ExcludeFromDefaultView, historyId: historyId }));
                        }
                    });

                    that.$el.find("#details-row-container").append(sectionEl);
                });

            } else {

                i = 0;
                var container = this.$el.find("#details-row-container");
                _.each(this.model.get("Fields"), function (f) {
                    if (!f.IsHidden && (!f.ExcludeFromDefaultView || (f.ExcludeFromDefaultView && f.FieldSets.split(',').indexOf(that.model.get("FieldSet"))))) {
                        var historyId = null;
                        if (f.Revision > 0) {
                            historyId = f.FieldType;
                        }
                        container.append(_.template(entityFieldTemplate, { key: f.FieldTypeDisplayName, value: f.DisplayValue, type: f.FieldDataType, isOddRow: i++ % 2 == 1, excludeFromDefaultView: f.ExcludeFromDefaultView, historyId: historyId }));
                    }
                });

            }
            var ids = that.$el.find(".category-section-wrap");

            for (index = 0; index < ids.length; ++index) {
                showCategories('#' + ids[index].id);
            }

            function showCategories(id) {
                id = id.replace(" ", "-");

                var $id = that.$el.find(id);
                var $heading = that.$el.find(id + " .category-section-title");
                var $box = $id.find("#category-field-container");

                var $showTask = that.$el.find(id + '-show i');

                $heading.click(function () {
                    $box.toggle(200);

                    $showTask.toggle();

                });
            }

            return this; // enable chained calls
        }
    });

    return entityDetailView;
});
