﻿define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {

    var entityInfoView = Backbone.View.extend({
        //tagName: 'div',

        initialize: function (options) {
            this.Name = options.Name; 
        },
        events: {
            "click #card-wrap": "onCardClick",
        },
        onClose: function () {
        },
        render: function () {
            this.$el.html(this.Name);

            return this; // enable chained calls
        }
    });

    return entityInfoView;
});