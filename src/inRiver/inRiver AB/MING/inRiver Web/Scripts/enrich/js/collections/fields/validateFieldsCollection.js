define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/field/fieldModel'
], function ($, _, Backbone, fieldModel) {
    var validateFieldsCollection = Backbone.Collection.extend({
        model: fieldModel,
        url: '/api/validatefields'
    });

    return validateFieldsCollection;
});
