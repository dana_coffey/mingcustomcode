﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/views/contextmenu/contextMenuView',
  'text!sharedtemplates/entity/entityOverviewRelationTemplate.html'
], function ($, _, backbone, inRiverUtil, entityDetailSettingView, entityDetailSettingWrapperView, entityModel, completenessModel, relationModel, contextMenuView, relationTemplate) {

    var entitycardView = backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.relation = options.relation;
            this.direction = options.direction;


            if (this.direction == "outbound") {
                this.id = this.relation.TargetEntity.Id;
                this.entity = this.relation.TargetEntity;
            } else {
                this.id = this.relation.SourceEntity.Id;
                this.entity = this.relation.SourceEntity;
            }

            this.render();
        },
        events: {
            "click #relation-remove": "onRelationRemove",
            "click #relation-inactivate": "onRelationActivate",
            "click #relation-activate": "onRelationInactivate",
            "click #delete-entity": "onEntityDelete",
            "click #edit-entity": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-wrap": "onCardClick",
            "click .card-wrap-large": "onCardClick",
            "click .completeness-small": "onShowCompletnessDetails",
        },
        onCardClick: function (e) {
            e.stopPropagation();
            var id = this.entity.Id;

            this.goTo("entity/" + id);
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onConfirmDeleteEntity: function (self) {
            self.model.destroy({
                success: function () {
                    inRiverUtil.Notify(self.model.attributes.EntityTypeDisplayName + " successfully deleted");
                    window.appHelper.event_bus.trigger('entitydeleted', self.id);
                    self.remove();
                    self.unbind();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityDelete: function (e) {
            var self = this;

            e.stopPropagation();
            this.onToggleTools(e);
            var id = this.entity.Id;

            this.model = new entityModel(id);
            this.model.url = "/api/entity/" + id;
            this.model.fetch({
                success: function () {
                    inRiverUtil.NotifyConfirm("Confirm delete entity", "Do you want to delete this entity?", self.onConfirmDeleteEntity, self);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var self = this;

            this.onToggleTools(e);
            var id = this.entity.Id;

            var starred = this.entity.Starred;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    self.entityUpdated(id);
                }
            };

            if (starred == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.entity.Id, true);
                xmlhttp.send();

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.entity.Id, true);
                xmlhttp.send();
            }

        },
        entityUpdated: function (id) {

            if (id == this.entity.Id) {

                var self = this;
                                var entity = new entityModel({ Id: this.entity.Id });

                entity.fetch({
                    success: function () {
                        self.entity = entity.toJSON();
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onEditDetail: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            var id = this.entity.Id;

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: id,
            });
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.id, $(e.currentTarget));
        },
        onRelationActivate: function (e) {
            e.stopPropagation();

            var model = new relationModel(this.relation);

            model.Id = this.relation.Id;
            model.attributes["Active"] = "active";
            model.save();

            this.entityUpdated(this.entity.Id);
            this.relation.Active = "active";
        },
        onRelationInactivate: function (e) {

            e.stopPropagation();

            var model = new relationModel(this.relation);

            model.Id = this.relation.Id;
            model.attributes["Active"] = "inactive";
            model.save();
            this.relation.Active = "inactive";
            this.entityUpdated(this.entity.Id);

        },
        onRelationRemoveConfirmed: function (self) {
            var relation = new relationModel(self.relation);

            relation.destroy({
                success: function (model) {

                    appHelper.event_bus.trigger('relationremoved', model.attributes.TargetId);

                    inRiverUtil.Notify("Relation successfully removed");
                    self.stopListening(window.appHelper.event_bus);
                    self.remove();
                    self.unbind();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onRelationRemove: function (e) {

            e.stopPropagation();

            var self = this;

            inRiverUtil.NotifyConfirm("Confirm remove relation", "Do you want to remove this relation?", this.onRelationRemoveConfirmed, self);

        },
        render: function () {

            var diff = Math.round(this.entity.Completeness / 5);

            this.$el.html($(_.template(relationTemplate, { entity: this.entity, pie: String.fromCharCode(65 + diff), linkId: this.relation.Id, active: this.relation.Active  })));

            var lockedBy;

            if (this.direction == "outbound") {
                lockedBy = this.relation.TargetEntity.LockedBy;
            } else {
                lockedBy = this.relation.SourceEntity.LockedBy;
            }

            var isLocked = inRiverUtil.isLocked(lockedBy);

            var tools = new Array();

            if (!isLocked) {
                tools.push({ name: "edit-entity", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            if (this.entity.Starred == "1") {
                tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });
            } else {
                tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }

            if (!isLocked) {
                tools.push({ name: "delete-entity", title: "Delete Entity", icon: "fa-trash-o fa", text: "Delete entity", permission: "DeleteEntity" });
            }

            if (this.relation.Active == "active") {
                tools.push({ name: "relation-activate", title: "Inactivate relation", icon: "fa fa-ban relation-activate ", text: "Inactivate relation", permission: "UpdateLink" });
            } else {
                tools.push({ name: "relation-inactivate", title: "Activate relation", icon: "fa fa-ban relation-inactivate ", text: "Activate relation", permission: "UpdateLink" });
            }

            tools.push({ name: "relation-remove", title: "Remove relation", icon: "fa-close fa", text: "Remove relation", permission: "DeleteLink" });

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.relation.Id,
                tools: tools
            }).$el);

            return this; // enable chained calls
        }
    });

    return entitycardView;
});
