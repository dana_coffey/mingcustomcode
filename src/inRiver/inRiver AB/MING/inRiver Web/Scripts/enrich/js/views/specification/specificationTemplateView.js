﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'sharedjs/collections/cvl/CvlValueCollection',
 'collections/entities/entityCollection',
  'views/specification/specificationTemplateEditFieldView',
  'views/specification/specificationTemplateEditCategoryView',
  'views/specification/specificationTemplateDeleteCategoryView',
  'views/specification/specificationTemplateEditUnitView',
  'text!templates/specification/specificationTemplateViewTemplate.html',
  'text!templates/specification/specificationTemplateFieldItemTemplate.html',
  'text!sharedtemplates/entity/entityCategoryTemplate.html',
    'sharedjs/misc/permissionUtil',
], function ($, _, backbone, inRiverUtil, modalPopup, CvlValueCollection, entityCollection, specificationTemplateEditFieldView, specificationTemplateEditCategoryView, specificationTemplateDeleteCategoryView, specificationTemplateEditUnitView, specificationTemplateViewTemplate, specificationTemplateFieldItemTemplate, entityCategoryTemplate, permissionUtil) {

    var specificationTemplateView = backbone.View.extend({
        initialize: function (options) {
            this.fieldCollection = new backbone.Collection(null, { url: "/api/specificationtemplate/" + options.id + "/field", model: backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });
            this.categoryCollection = new backbone.Collection(null, { url: "/api/specificationtemplate/category", model: backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("DisplayName"); } }) });
            this.unitCollection = new CvlValueCollection(null, { cvlId: "Unit" });

            this.listenTo(this.fieldCollection, "reset", this.onInit);
            this.listenTo(this.categoryCollection, "reset", this.onInit);
            this.listenTo(this.unitCollection, "reset", this.onInit);

            this.fieldCollection.fetch({ reset: true });
            this.categoryCollection.fetch({ reset: true });
            this.unitCollection.fetch({ reset: true });
        },
        events: {
            "click #add-specification-field": "onAddField",
            "click #specification-template-edit-field": "onEditField",
            "click #specification-template-delete-field": "onDeleteField",
            "click #add-specification-category": "onAddCategory",
            "click #category-section-edit-icon": "onEditCategory",
            "click #specification-template-manage-categories": "onManageCategories",
            "click #specification-template-add-unit": "onAddUnit",
            "click .category-section-title": "onToggleCategory",
            "click #specification-template-expand": "onExpandAll",
            "click #specification-template-collapse": "onCollapseAll",
            "click #specification-template-show-all-entities": "onShowAllEntities",
        },
        onInit: function (collection) {
            collection.isLoaded = true;

            // Perform final init when all necessary collections are loaded
            if (this.fieldCollection.isLoaded && this.categoryCollection.isLoaded && this.unitCollection.isLoaded) {
                this.listenTo(this.unitCollection, "add", this.onUnitCollectionChanged);
                this.listenTo(this.fieldCollection, "sync destroy", this.render);
                this.listenTo(this.categoryCollection, "sync", this.onCategoriesCollectionChanged);
                this.render();
            }
        },
        onUnitCollectionChanged: function() {
            // Make sure to get a fresh unit collection from the server when the collection is modified
            this.unitCollection.fetch();
        },
        onCategoriesCollectionChanged: function() {
            this.fieldCollection.fetch();
        },
        onAddField: function() {
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateEditFieldView({
                categoryCollection: this.categoryCollection,
                fieldCollection: this.fieldCollection,
                unitCollection: this.unitCollection,
                model: null
            }), {
                header: "Add Specification Field",
                size: "large",
                onSave: this.onAddFieldPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onEditField: function(e) {
            var idToEdit = $(e.currentTarget).closest("li").attr("data-id");
            
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateEditFieldView({
                categoryCollection: this.categoryCollection,
                fieldCollection: this.fieldCollection,
                unitCollection: this.unitCollection,
                model: this.fieldCollection.get(idToEdit)
            }), {
                header: "Edit Specification Field",
                size: "large",
                onSave: this.onEditFieldPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onAddFieldPopupSave: function (popup, subView) {
            var that = this;
            var errors = subView.form.commit();
            if (!errors) {
                this.fieldCollection.create(subView.model.toJSON(), {
                    method: "POST",
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {
                _.each(errors, function (error, key) {
                    subView.form.$el.find(".field-editor-validation-error-message[data-id='" + key + "']").html(error.message);
                });
            }
        },
        onEditFieldPopupSave: function (popup, subView) {
            var that = this;
            var errors = subView.form.commit();
            if (!errors) {
                subView.model.save(null, {
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {
                _.each(errors, function (error, key) {
                    subView.form.$el.find(".field-editor-validation-error-message[data-id='" + key + "']").html(error.message);
                });
            }
        },
        onDeleteField: function (e) {
            var that = this;
            var idToDelete = $(e.currentTarget).closest("li").attr("data-id");
            inRiverUtil.NotifyConfirm("Confirm Delete Specification Field", "Do you want to remove this specification field?", function(e) {
                that.fieldCollection.get(idToDelete).destroy({
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }, this);
        },
        onAddCategory: function () {
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateEditCategoryView({
                categoryCollection: this.categoryCollection,
                model: null
            }), {
                header: "Add Specification Category",
                size: "large",
                onSave: this.onAddCategoryPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onEditCategory: function (e) {
            e.stopPropagation();
            var idToEdit = $(e.currentTarget).closest(".category-section-wrap")[0].id;

            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateEditCategoryView({
                categoryCollection: this.categoryCollection,
                model: this.categoryCollection.get(idToEdit)
            }), {
                header: "Edit Specification Category",
                size: "large",
                onSave: this.onEditCategoryPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onAddCategoryPopupSave: function (popup, subView) {
            var that = this;
            var errors = subView.form.commit();
            if (!errors) {
                this.categoryCollection.create(subView.model.toJSON(), {
                    method: "POST",
                    wait: true,
                    success: function () {
                        inRiverUtil.Notify("The category was successfully created");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {
                _.each(errors, function (error, key) {
                    subView.form.$el.find(".field-editor-validation-error-message[data-id='" + key + "']").html(error.message);
                });
            }
        },
        onEditCategoryPopupSave: function (popup, subView) {
            var that = this;
            var errors = subView.form.commit();
            if (!errors) {
                subView.model.save(null, {
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {
                _.each(errors, function (error, key) {
                    subView.form.$el.find(".field-editor-validation-error-message[data-id='" + key + "']").html(error.message);
                });
            }
        },
        onAddUnit: function () {
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateEditUnitView({
                unitCollection: this.unitCollection,
                model: null
            }), {
                header: "Add Specification Unit",
                size: "large",
                onSave: this.onAddUnitPopupSave,
                onCancel: function (popup) {
                    popup.close();
                }
            });
            $("#save-form-button").removeAttr("disabled");
        },
        onAddUnitPopupSave: function (popup, subView) {
            var that = this;
            var errors = subView.form.commit();
            if (!errors) {
                this.unitCollection.create(subView.model.toJSON(), {
                    method: "POST",
                    wait: true,
                    success: function () {
                        inRiverUtil.Notify("The unit was successfully created");
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                popup.close();
            } else {
                _.each(errors, function (error, key) {
                    subView.form.$el.find(".field-editor-validation-error-message[data-id='" + key + "']").html(error.message);
                });
            }
        },
        onManageCategories: function (e) {
            e.preventDefault();
            this.modal = new modalPopup({ callbackContext: this });
            this.modal.popOut(new specificationTemplateDeleteCategoryView({
                categoryCollection: this.categoryCollection,
            }), {
                header: "Manage Specification Categories",
                size: "small",
            });
        },
        onToggleCategory: function (e) {
            var el = $(e.currentTarget);
            el.next().toggle(200);
            el.find(".showCategory > i").toggle();
        },
        onExpandAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").show();
            categories.find("i.fa-minus-circle").show();
            categories.find("i.fa-plus-circle").hide();
        },
        onCollapseAll: function () {
            var categories = this.$el.find(".category-section-title");
            categories.siblings("#category-field-container").hide();
            categories.find("i.fa-minus-circle").hide();
            categories.find("i.fa-plus-circle").show();
        },
        onShowAllEntities: function () {
            var that = this;
            var tmpCollection = new entityCollection();
            tmpCollection.url = '/api/specification/get?templateId=' + this.options.id + '&entityId=' + this.options.id + '&entityTypeId=' + this.options.type;
            tmpCollection.fetch({
                success: function() {
                    var entities = _(tmpCollection.models).chain().pluck("attributes").pluck("Id").value().join(',');
                    $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                        //window.location.href = "/app/enrich/#workarea?title=" + that.options.Name + "&compressedEntities=" + compressed;
                        backbone.history.loadUrl("workareasearch?title=" + that.options.displayName + "&compressedEntities=" + compressed);
                        if ($("#workarea-header-label").hasClass("workarea-header-label-vertical")) {
                            $("#workarea-header-toggle-button").click();
                        }
                    });
                }
            });
        },
        getCollapsedCategories: function() {
            var collapsedCategories = null;
            if (this.$el.find(".category-section-wrap").length > 0) {
                collapsedCategories = [];
                this.$el.find("#category-field-container:hidden").each(function () {
                    collapsedCategories.push($(this).attr("data-id"));
                });
            }
            return collapsedCategories;
        },
        render: function () {
            var that = this;
            var collapsedCategories = this.getCollapsedCategories(); // for restoring the collapsed state after renderingf

            this.$el.html(_.template(specificationTemplateViewTemplate, { /*data: this.model.toJSON()*/ }));
            
            var fieldListEl = this.$el.find("#field-list");

            // Render all fields 
            var categoriesInUse = _.uniq(this.fieldCollection.pluck("CategoryId"));
            var sortedCategories = _.sortBy(this.categoryCollection.filter(function (model) { return _.contains(categoriesInUse, model.get("Id")); }), function (model) { return model.get("Index"); });
            _.each(sortedCategories, function (category) {
                var categoryEl = $(_.template(entityCategoryTemplate, { sectionId: category.get("Id"), sectionName: category.get("DisplayName"), showEditIcon: true }));
                fieldListEl.append(categoryEl);
                var sortedFieldArray = _.sortBy(that.fieldCollection.where({ "CategoryId": category.get("Id") }), function (model) { return model.get("Index"); });
                _.each(sortedFieldArray, function (model) {
                    var itemEl = _.template(specificationTemplateFieldItemTemplate, { data: model.toJSON(), unitDisplayValue: model.get("Unit") ? that.unitCollection.get(model.get("Unit")).get("DisplayValue") : null });
                    categoryEl.find("#category-field-container").append(itemEl);
                });
            });

            // Restore expand/collapse state
            _.each(collapsedCategories, function (v) {
                var tmpEl = that.$el.find(".category-section-title[data-id='" + v + "']");
                tmpEl.next().hide();
                tmpEl.find("i.fa-minus-circle").hide();
                tmpEl.find("i.fa-plus-circle").show();
            });

            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("#add-specification-field"));
            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("[id=specification-template-edit-field]"));
            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("[id=specification-template-delete-field]"));
            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("#add-specification-category"));
            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("[id=category-section-edit-icon]"));
            permissionUtil.CheckPermissionForElement("AdministerSpecificationTemplates", this.$el.find("#specification-template-manage-categories"));
            permissionUtil.CheckPermissionForElement("UpdateCVL", this.$el.find("#specification-template-add-unit"));


            
            return this; // enable chained calls
        }
    });

    return specificationTemplateView;
});
