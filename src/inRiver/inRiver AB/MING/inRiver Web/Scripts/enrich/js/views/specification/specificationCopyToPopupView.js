﻿define([
  'jquery',
  'underscore',
  'backbone',
  'alertify',
   'sharedjs/misc/inRiverUtil',

   'collections/entities/entityCollection',
  'text!templates/specification/specificationCopyToTemplate.html'
], function ($, _, backbone, alertify, inRiverUtil, entityCollection, specificationCopyToTemplate) {

    var specificationCopyToPopupView = backbone.View.extend({
        initialize: function (options) {
            var that = this;

            this.templateId = options.template;
            this.currentEntityId = options.currentEntityId;
            this.currentEntityTypeId = options.currentEntityTypeId;

            this.collection = new entityCollection();
            this.collection.url = '/api/specification/get?templateId=' + this.templateId + '&entityId=' + this.currentEntityId + '&entityTypeId=' + this.currentEntityTypeId;

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.collection.fetch({
                success: function () {
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            "click #save-form-button": "onSave",
            "click #cancel-form-button": "onCancel",
            "click .copy-from-content-row": "onRowSelected",
            "keyup #nameFilterInput": "onContentFiltering",
            "keyup #descriptionFilterInput": "onContentFiltering",
            "click #toggle-all": "onToggleAll",
            "click #toggle-checkbox": "onCheckboxClicked"
        },
        onToggleAll: function (e) {
            e.stopPropagation();

            // Check or uncheck all
            var isChecked = $(e.currentTarget).is(":checked");
            this.$el.find(".copy-from-checkbox input").prop('checked', isChecked);

            // Update save button
            if (this.$el.find("#toggle-checkbox:checked").length > 0) {
                $("#save-form-button").removeAttr("disabled");
            } else {
                $("#save-form-button").attr("disabled", true);
            }
        },
        onCheckboxClicked: function (e) {
            e.stopPropagation();

            // Update save button
            if (this.$el.find("#toggle-checkbox:checked").length > 0) {
                $("#save-form-button").removeAttr("disabled");
            } else {
                $("#save-form-button").attr("disabled", true);
            }
        },
        onSave: function () {
            var that = this;

            var targets = [];
            $("#toggle-checkbox:checked").each(function (i, x) {
                targets.push($(this).attr("data-id"));
            });

            $.post("/api/specification/copyto?sourceEntity=" + this.currentEntityId + "&targets=" + targets.join(',')).done(function () {
                that.done();
            });
        },
        onCancel: function () {
            this.done();
        },
        onContentFiltering: function (e) {

            var nameFilterString = this.$el.find("#nameFilterInput").val().toLowerCase();
            var descriptionFilterString = this.$el.find("#descriptionFilterInput").val().toLowerCase();

            this.$el.find(".copy-from-content-row").show();

            this.$el.find(".copy-from-content-row").each(function (i, x) {

                if (nameFilterString && nameFilterString.length > 0) {
                    if ($(x).attr("filter-str-name").indexOf(nameFilterString) < 0) $(x).hide();
                }

                if (descriptionFilterString && descriptionFilterString.length > 0) {
                    if ($(x).attr("filter-str-desc").indexOf(descriptionFilterString) < 0) $(x).hide();
                }
            });
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.close();
            //this.undelegateEvents();
            //this.$el.removeData().unbind();
            //this.remove();
            //backbone.View.prototype.remove.call(this);
        },
        render: function () {

            var that = this;

            this.$el.html(_.template(specificationCopyToTemplate, { templateId: that.templateId, entities: that.collection.toJSON() }));

            return that; // enable chained calls
        },
    });

    return specificationCopyToPopupView;
});

