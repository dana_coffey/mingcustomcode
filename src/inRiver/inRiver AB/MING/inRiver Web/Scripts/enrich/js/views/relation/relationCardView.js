﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/views/contextmenu/contextMenuView',
  'text!sharedtemplates/relation/relationTemplate.html'
], function ($, _, Backbone, inRiverUtil, entityDetailSettingView, entityDetailSettingWrapperView, entityModel, completenessModel, contextMenuView, relationTemplate) {

    var entitycardView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function (options) {
            this.relation = options.relation;
            this.direction = options.direction;

            if (this.direction == "outbound") {
                this.id = this.relation.attributes.TargetEntity.Id;
            } else {
                this.id = this.relation.attributes.SourceEntity.Id;
            }

            this.render();
        },
        events: {
            "click #relation-remove": "onRelationRemove",
            "click #relation-inactivate": "onRelationActivate",
            "click #relation-activate": "onRelationInactivate",
            "click #delete-entity": "onEntityDelete",
            "click #edit-entity": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-wrap": "onCardClick",
            "click .card-wrap-large": "onCardClick",
            "click .completeness-small": "onShowCompletnessDetails",
        },
        onCardClick: function (e) {
            e.stopPropagation();
            var id = -1;
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
            }
            this.goTo("entity/" + id);
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onConfirmDeleteEntity: function (self) {
            //var self = this;
            self.model.destroy({
                success: function () {
                    appHelper.event_bus.trigger('entitydeleted', self.model.id);
                    window.appHelper.event_bus.trigger('relationremoved', self.relation.attributes.LinkEntity ? self.relation.attributes.LinkEntity.Id : self.relation.attributes.LinkType, self.id);
                    inRiverUtil.Notify(self.model.attributes.EntityTypeDisplayName + " successfully deleted");
                    self.remove();
                    self.unbind();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityDelete: function (e) {
            var self = this;

            e.stopPropagation();
            this.onToggleTools(e);
            var id = -1;
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
            }
            this.model = new entityModel(id);
            this.model.url = "/api/entity/" + id;
            this.model.fetch({
                success: function () {
                    inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete this " + self.model.attributes.EntityTypeDisplayName + "?", self.onConfirmDeleteEntity, self);
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEntityStar: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            var id = -1;

            var starred = -1;
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
                starred = this.relation.attributes.TargetEntity.Starred;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
                starred = this.relation.attributes.SourceEntity.Starred;
            }
            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    that.entityUpdated(id);
                }
            }

            if (starred == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + id, true);
                xmlhttp.send();

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + id, true);
                xmlhttp.send();
            }

        },
        entityUpdated: function (id) {
            var thisId = -1;
            if (this.direction == "outbound") {
                thisId = this.relation.attributes.TargetEntity.Id;
            } else {
                thisId = this.relation.attributes.SourceEntity.Id;
            }
            if (id == thisId) {

                var self = this;
                this.relation.url = "/api/relation/" + this.relation.attributes.SourceEntity.Id + "/" + this.relation.attributes.TargetEntity.Id;

                this.relation.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onEditDetail: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);
            var id = -1;
            if (this.direction == "outbound") {
                id = this.relation.attributes.TargetEntity.Id;
            } else {
                id = this.relation.attributes.SourceEntity.Id;
            }

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: id
            });
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            //inRiverUtil.ShowCompleteness(this.id);
            inRiverUtil.ShowInlineCompleteness(this.id, $(e.currentTarget));
        },
        onRelationActivate: function (e) {

            e.stopPropagation();

            var self = this;

            var modelToActivate = this.relation;

            modelToActivate.set("Active", "active");
            modelToActivate.save();

        },
        onRelationInactivate: function (e) {

            e.stopPropagation();

            var self = this;

            var modelToInactivate = this.relation;

            modelToInactivate.set("Active", "inactive");
            modelToInactivate.save();
        },
        onRelationRemoveConfirmed: function (self) {

            var modelToDelete = self.relation;

            modelToDelete.destroy({
                success: function () {

                    self.stopListening(appHelper.event_bus);
                    window.appHelper.event_bus.trigger('relationremoved', modelToDelete.attributes.LinkEntity ? modelToDelete.attributes.LinkEntity.Id : modelToDelete.attributes.LinkType, self.id);
                    window.appHelper.event_bus.trigger('entityupdated', self.relation.get("SourceId"));
                    self.remove();
                    self.unbind();

                    inRiverUtil.Notify("Relation successfully removed");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onRelationRemove: function (e) {

            e.stopPropagation();

            var self = this;

            inRiverUtil.NotifyConfirm("Confirm remove relation", "Are you sure you want to remove this relation?", this.onRelationRemoveConfirmed, self);

        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        render: function () {

            var lockedBy;

            if (this.direction == "outbound") {
                lockedBy = this.relation.attributes.TargetEntity.LockedBy;
            } else {
                lockedBy = this.relation.attributes.SourceEntity.LockedBy;
            }

            var isLocked = inRiverUtil.isLocked(lockedBy);

            this.$el.html($(_.template(relationTemplate, { model: this.relation.toJSON(), direction: this.direction, active: this.relation.attributes["Active"] })));
            
            var tools = new Array();

            if (!isLocked) {
                tools.push({ name: "edit-entity", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            if ((this.direction == "outbound" && this.relation.attributes.TargetEntity.Starred == "1") ||
               (this.direction == "inbound" && this.relation.attributes.SourceEntity.Starred == "1")) {
                tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });
            } else {
                tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }

            if (!isLocked) {
                tools.push({ name: "delete-entity", title: "Delete Entity", icon: "fa-trash-o fa", text: "Delete entity", permission: "DeleteEntity" });
            }

            if (this.relation.attributes["Active"] == "active") {
                tools.push({ name: "relation-activate", title: "Inactivate relation", icon: "fa fa-ban relation-activate ", text: "Inactivate relation", permission: "UpdateLink" });
            } else {
                tools.push({ name: "relation-inactivate", title: "Activate relation", icon: "fa fa-ban relation-inactivate ", text: "Activate relation", permission: "UpdateLink" });
            }

            tools.push({ name: "relation-remove", title: "Remove relation", icon: "fa-close fa", text: "Remove relation", permission: "DeleteLink" });

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.relation.id,
                tools: tools
            }).$el);

            var cardmodel;
            if (this.direction == "outbound") {
                cardmodel = this.relation.attributes.TargetEntity;
            } else {
                cardmodel = this.relation.attributes.SourceEntity;
            }

            this.drawCompleteness(cardmodel.Completeness);
            return this; // enable chained calls
        }
    });

    return entitycardView;
});
