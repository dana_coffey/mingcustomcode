﻿define([
  'underscore',
  'backbone'
], function (_, Backbone) {

    var fileImportModel = Backbone.Model.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.id = options.Id;
            this.entitytypeId = options.entitytypeId;
        },
        //urlRoot: '/api/specificationValue',
        url: function () {
            return '/api/fileimportvalidation?id=' + this.id + '&entitytypeId=' + this.entitytypeId;
        }
        //urlRoot: '/api/fileimportvalidation/',
    });

    return fileImportModel;

});
