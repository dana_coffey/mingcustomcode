define([
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'jquery-ui',
  'dropzone',
   'sharedjs/misc/inRiverUtil',

   'jquery-ui-touch-punch',
  'sharedjs/collections/entities/starredEntityCollection',
  'sharedjs/collections/tasks/tasksCollection',
  'views/entitycard/entitycardView',
  'views/start/startPageWorkareaView',
  'views/start/startPageStarredEntitiesView',
  'sharedjs/views/start/startPageTaskListView',
    'sharedjs/views/start/startPageNotificationListView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/create/entityCreateView',
  'text!sharedtemplates/dropzone/dropzoneTemplate.html',
  'text!templates/start/startpageTemplate.html'
], function ($, _, Backbone, moment, jqueryui, Dropzone, inRiverUtil, jqutp, starredEntitiesCollection, tasksCollection, entitycardView, startPageWorkareaView, startPageStarredEntitiesView, startPageTaskListView, startPageNotificationListView, entityDetailSettingView, entityCreateView, dropzoneTemplate, startpageTemplate) {

    var startPageView = Backbone.View.extend({
        initialize: function(data) {
            var self = this;
            self.hasTasks = false;
            this.undelegateEvents();

            self.hasTaskAssignedGroupTaskField = false;

            $.get("/api/tools/hasentitytype/Task").done(function(result) {
                if (result) {
                    self.hasTasks = true;
                }

                $.get("/api/tools/hasField/TaskAssignedGroupTask").done(function (fieldExists) {
                    if (fieldExists) {
                        self.hasTaskAssignedGroupTaskField = true;
                    }

                    self.render();
                });
                
            });
        },
        events: {
            "click #showAllStars": "showAllStars",
            "click #createNewTask": "createNewTask",
            "click #createEntity": "createEntity"
        },
        close: function () {

            this.notificationsView.close();

            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            Backbone.View.prototype.remove.call(this);

        },
        showAllStars: function() {
            var self = this;

            $.post("/api/starredentity/getidlist").done(function(result) {

                var entities = result;
                $.post("/api/tools/compress", { '': entities }).done(function(compressed) {

                    self.goTo("workarea?title=My Starred Entities&stopAdvQuery=true&compressedEntities=" + compressed);
                });
            });
        },
        entityCreated: function(model) {
            if (model.attributes.EntityType != "Task") {
                this.goTo("entity/" + model.id);
            }
        },
        createNewTask: function() {
            if (this._modalView) {

                this._modalView.remove();
            }

            this._modalView = new entityDetailSettingView({ id: 0, entityTypeId: "Task" });

            window.scrollTo(0, 0);
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();
        },
        createEntity2: function() {
            this.listenTo(appHelper.event_bus, 'entitycreated', this.entityCreated);

            if (this._modalView) {
                this._modalView.remove();
            }

            window.scrollTo(0, 0);
            $("#modalWindow").html(this._modalView.el);
            $("#modalWindow").show();

        },
        render: function () {
            var self = this;
            this.$el.html(startpageTemplate);

            /* Starred Entities */
            var starred = new startPageStarredEntitiesView();


            var wa = new startPageWorkareaView();
            
            this.notificationsView = new startPageNotificationListView();
            $("#notifications .white-box").html(this.notificationsView.$el);

            /* Tasks */
            /* My Tasks */
            if (this.hasTasks) {
                
                var mytask = new startPageTaskListView({ title: 'My Tasks', tasktype: "mytask" });
                $("#mytask .white-box").append(mytask.$el);
                var createdtask = new startPageTaskListView({ title: 'My Created Tasks', tasktype: "createdtask" });
                $("#createdtask .white-box").append(createdtask.$el);
               
            } else {
                this.$el.find("#mytask").hide();
                this.$el.find("#createdtask").hide();
                this.$el.find("#createNewTask").hide();
            }

            if (this.hasTaskAssignedGroupTaskField) {
                var groupsTasks = new startPageTaskListView({ title: 'My Groups Tasks', tasktype: "groupstasks" });
                $("#groupstasks .white-box").append(groupsTasks.$el);
            }
            else {
                this.$el.find("#groupstasks").hide();
            }
        }
    });

    return startPageView;
});

