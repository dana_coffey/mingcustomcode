﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var fileImportModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: '/api/fileimportentity/',
    });

    return fileImportModel;

});
