define([
  'underscore',
  'backbone'
], function(_, Backbone) {
    
    var CvlModel = Backbone.Model.extend({
        idAttribute: 'Id',
        urlRoot: '/api/cvl',
        initialize: function () {

        },
        schema: { // Used by backbone forms extension
            Id: { type: 'Text', validators: ['required', { type: 'regexp', message: 'No spaces or invalid characters allowed', regexp: /^[a-zA-Z0-9-_]+$/ }], editorAttrs: { disabled: true } },
            DataType: { type: 'Select', title: 'Data Type', options: ["String", "LocaleString"], editorAttrs: { disabled: true } },
            ParentId: { type: 'Select', title: 'Parent CVL', options: [] },
            CustomValueList: { type: 'Checkbox', title: 'Custom CVL' }
        },
        get: function (attr) {
            // Override of get() to support "synthetic" attributes (such as RolesList below)
            if (typeof this[attr] == 'function') {
                return this[attr]();
            }
            return Backbone.Model.prototype.get.call(this, attr);
        },
        toString: function () {
            return this.get("Id");
        },
    });

    return CvlModel;

});
