define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/field/fieldModel'
], function ($, _, Backbone, fieldModel) {
    var fieldCollection = Backbone.Collection.extend({
        sort_key: 'SortOrder',
        initialize: function (options) {
            this.url = options.url;

        },
        comparator: function(a, b) {
            // Assuming that the sort_key values can be compared with '>' and '<',
            // modifying this to account for extra processing on the sort_key model
            // attributes is fairly straight forward.
            a = a.get(this.sort_key);
            b = b.get(this.sort_key);
            return a > b ?  1
                 : a < b ? -1
                 :          0;
        } ,
        model: fieldModel,
        sortByField: function (fieldName) {
            this.sort_key = fieldName;
            this.sort();
        }
        //url: '/api/field'
    });

    return fieldCollection;
});
