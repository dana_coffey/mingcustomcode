define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/resource/resourceModel',
  'modalPopup',
  'views/entityview/entityView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/contextmenu/contextMenuView',
  'sharedjs/views/templateprinting/TemplatePrintingSetupView',
  'sharedjs/misc/inRiverUtil',
  'text!templates/entitycard/entitycardTemplate.html',
  'text!sharedtemplates/entitycard/workareaCardTemplate.html',
  'text!templates/shared/resourceDownloadPopup.html'
], function ($, _, Backbone, completenessModel, relationModel, resourceModel, modalPopup, entityView,
    entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, contextMenuView, templatePrintingSetupView,
    inRiverUtil, entitycardTemplate, workareaCardTemplate, resourceDownloadPopup) {

    var entitycardView = Backbone.View.extend({
        //tagName: 'div',
        initialize: function(options) {
            this.type = options.type;
            this.taskRelations = options.taskRelations;

            this.listenTo(appHelper.event_bus, 'entitydeleted', this.entityDeleted);

            this.listenTo(window.appHelper.event_bus, 'entityupdated-entity-card-view', this.entityUpdated);
        },
        events: {
            "click #card-completeness-text": "onShowCompletnessDetails",
            "click #large-card-completeness-text": "onShowCompletnessDetails",
            "click .completeness-small": "onShowCompletnessDetails",
            "click .card-wrap": "onCardClick",
            "click .card-wrap-large": "onCardClick",
            "click #edit-button": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "click #delete-button": "onEntityDelete",
            "click #task-button": "onAddTask",
            "click #download-button": "onDownloadResouce",
            "click #upload-button": "onUploadResource",
            "click #pdf-button": "onDownloadPDF",
            "click #preview-button": "onShowPreview",
        },
        onEntityDelete: function(e) {
            e.stopPropagation();
            this.onToggleTools(e);

            inRiverUtil.deleteEntity(this);
        },
        onAddTask: function(e) {
            e.stopPropagation();
            this.onToggleTools(e);

            this.stopListening(window.appHelper.event_bus);
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });
        },
        taskAddedNowLinkEntity: function(model) {
            this.targetIdToCreate = model.id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function() {
                    inRiverUtil.Notify("Relation successfully added");
                },
                error: function(result, response) {
                    inRiverUtil.OnErrors(result, response);
                }
            });
        },
        onDownloadResouce: function(e) {
            var self = this;
            e.stopPropagation();
            e.preventDefault();

            this.resource = new resourceModel({ id: this.model.id, urlRoot: '/api/resource' });
            this.resource.fetch({
                url: '/api/resource/' + this.model.id,
                success: function() {

                    //self.$el.append(_.template(resourceDownloadPopup, { resource: self.resource.toJSON() }));


                    var downloadElement = _.template(resourceDownloadPopup, { resource: self.resource.toJSON() });
                   // self.$el.append(downloadElement);

                    if (this.$el.find(".download-menu").length === 0) {
                        self.$el.append(downloadElement);
                    } else {
                        this.$el.find(".download-menu")[0] = downloadElement;
                    }

                    self.$el.find(".download-menu").menu();


                    var menu = self.$el.find(".download-menu").position({
                        my: "left top",
                        at: "right bottom",
                        of: e.currentTarget
                    });

                    $(document).one("click", function() {
                        // self.onToggleTools(e);
                        menu.hide();
                    });
                },
                error: function(model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });


        },
        onUploadResource: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.onToggleTools(e);
        },
        onToggleTools: function(e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },


        onEditDetail: function(e) {
            e.stopPropagation();
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id,
            });
        },
        onEntityStar: function(e) {
            e.stopPropagation();

            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    that.entityUpdated(that.model.id);
                }
            }

            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");
            }

        },
        onShowPreview: function (e) {
            e.stopPropagation();
            inRiverUtil.openEntityPreview(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPreviewTemplates);
        },
        onDownloadPDF: function (e) {
            e.stopPropagation();
            inRiverUtil.createEntityPDF(templatePrintingSetupView, this.model.get("EntityType"), this.model.get("Id"), this.model.get("DisplayName"), appData.enrichPDFTemplates);
        },
        onCardClick: function (e) {
            e.stopPropagation();
            this.goTo("entity/" + this.model.id);
        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        onClose: function () {
        },
        entityUpdated: function (id) {

            if (id == this.model.id) {

                var self = this;
                this.model.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });

                this.stopListening(window.appHelper.event_bus, ["entityupdated-entity-card-view"]);
            }
        },
        entityDeleted: function (id) {
            if (id == this.model.id) {
                this.stopListening(appHelper.event_bus);
                this.remove();
                this.unbind();
            }

        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },

        render: function () {

            var self = this;

            if (this.model == null) {
                return this;
            }

            if (this.taskRelations != null) {
                this.TaskRelationType = _.find(this.taskRelations.models, function (model) {
                    return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                });
            }
            var icon = this.model.attributes.EntityType;
            if (icon == "Channel" && !this.model.attributes.ChannelPublished) {
                icon = "ChannelUnpublished";
            }



            if (this.type == "large") {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: appHelper.userName, enableAddTask: this.TaskRelationType != undefined, icon: icon }));
            } else {
                this.$el.html(_.template(workareaCardTemplate, { data: this.model.toJSON(), userName: appHelper.userName, enableAddTask: this.TaskRelationType != undefined, icon: icon }));
            }

            //add context menu

            var isLocked = inRiverUtil.isLocked(this.model.attributes.LockedBy);

            var tools = new Array();

            if (!isLocked) {
                tools.push({ name: "edit-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
            }

            if (this.model.attributes.Starred == "1") {
                tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });

            } else {
                tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }

            if (!isLocked) {
                tools.push({ name: "delete-button", title: "Delete Entity", icon: "fa-trash-o fa", text: "Delete", permission: "DeleteEntity" });
            }

            if (this.TaskRelationType != undefined) {
                tools.push({ name: "task-button", title: "Create Task", icon: "icon-entypo-fix-clipboard std-entypo-fix-icon", text: "Create Task", permission: "AddEntity" });
            }

            if (this.model.attributes.EntityType == "Resource") {
                tools.push({ name: "download-button", title: "Download resource", icon: "fa-download fa", text: "Download" });
                tools.push({ name: "upload-button", title: "Replace file", icon: "fa-upload fa", text: "Replace file", permission: "AddFile" });
            }


            if(_.find(appData.enrichPreviewTemplates, function(t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, self.model.get("EntityType"));
            })) {
                tools.push({ name: "preview-button", title: "Preview", icon: "fa-eye fa", text: "Preview" });
            }

            if (_.find(appData.enrichPDFTemplates, function (t) {
                return t.Properties && t.Properties.EntityTypes && _.contains(t.Properties.EntityTypes, self.model.get("EntityType"));
            })) {
                tools.push({ name: "pdf-button", title: "PDF", icon: "fa-file-pdf-o fa", text: "PDF" });
            }

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.model.id,
                tools: tools
            }).$el);

            this.drawCompleteness(this.model.attributes.Completeness);
            return this; // enable chained calls
        }
    });

    return entitycardView;
});
