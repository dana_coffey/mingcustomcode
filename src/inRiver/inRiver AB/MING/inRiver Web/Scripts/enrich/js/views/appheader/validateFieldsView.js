﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'collections/fields/validateFieldsCollection',
  'sharedjs/models/field/fieldModel',
  'sharedjs/misc/inRiverUtil',
  'text!templates/import/entityOverviewEntityTemplate.html',
  'text!templates/import/entityOverviewNewEntityTemplate.html',
  'text!templates/import/entityOverviewFieldTemplate.html',
  'text!templates/import/entityOverviewNewFieldTemplate.html',
  'text!templates/import/validateFieldsTemplate.html'

], function ($, _, Backbone, backboneforms, validateFieldsCollection, fieldModel, inRiverUtil, entityOverviewEntityTemplate, entityOverviewNewEntityTemplate, entityOverviewFieldTemplate, entityOverviewNewFieldTemplate, validateFieldsTemplate) {

    var validateFieldsView = Backbone.View.extend({
        initialize: function (options) {
            this.fileImportModel = this.model;
            var that = this;

            if (this.importCollection == undefined) {
                this.importCollection = new validateFieldsCollection();
                this.invalidModels = new validateFieldsCollection();
                this.entitiesProcessed = this.fileImportModel.get("EntitiesToProcess");
                this.importCollection.reset(this.fileImportModel.get("EntityModels"));
            }

            that.invalidModels.reset(_.select(
            this.importCollection.models,
            function (model) {
                return model.get("Valid") == false;
            }
            ));

            that.modifiedModels = _.select(
            this.importCollection.models,
            function (model) {
                return model.get("Modified") == true && model.get("Valid") == true;
            }
            );

            that.newModels = _.select(
            this.importCollection.models,
            function (model) {
                return model.get("IsNew") == true && model.get("Valid") == true;
            }
            );

            that.nochangesModels = _.select(
            this.importCollection.models,
            function (model) {
                return model.get("IsNew") == false && model.get("Modified") == false && model.get("Valid") == true;
            }
            );

            this.render();
        },
        events: {
            "click .entity-review": "entityChange",
            "click #cancel-button": "onCancel",
            "click #save-button": "onSave"
        },
        entityChange: function (e, res) {
            e.stopPropagation();
            e.preventDefault();
            this.$el.find(".entity-review").removeClass("active");
            var linktype = e.currentTarget.className.replace("entity-review ", "");
            this.model = this.importCollection.get(e.currentTarget.id);

            // Active link in review area
            this.$el.find("#review-" + linktype + "-area a[id='" + e.currentTarget.id + "']").addClass("active");

            if (linktype == "error") {
                this.renderForm(e.currentTarget.id);
            }

            if (linktype == "add") {
                this.renderAdd(e.currentTarget.id);
            }

            if (linktype == "update") {
                this.renderUpdate(e.currentTarget.id);
            }

            //this.loadModel(e.currentTarget.id);
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            this.remove();
            this.goTo("home");
        },
        onSave: function () {
            var that = this;
            if (this.form.validate() == null) {
                this.form.commit();

                this.model.save({}, {
                    success: function (model) {
                        that.initialize();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        renderAdd: function(id) {
            var container = this.$el.find("#inriver-fields-form-area");
            container.empty();
            container.append("<h2>" + this.model.get("Name") + "</h2>");

            if (this.model.attributes.Fields.length == 0) {
                container.append("<div><i>No data to show</i></div>");
            }

            container.append(_.template(entityOverviewNewEntityTemplate, {}));
            var fieldContainer = this.$el.find("#import-row-body");

            _.each(this.model.attributes.Fields, function (f) {
                fieldContainer.append(_.template(entityOverviewNewFieldTemplate, { key: f.FieldTypeImportDisplayValue, persistedvalue: f.ErrorMessage, value: f.DisplayValue }));
            });

        },
        renderUpdate: function (id) {
            var container = this.$el.find("#inriver-fields-form-area");
            container.empty();
            container.append("<h2>" + this.model.get("Name") + "</h2>");

            if (this.model.attributes.Fields.length == 0) {
                container.append("<div><i>No data to show</i></div>");
                return;
            }

            container.append(_.template(entityOverviewEntityTemplate, {}));
            var fieldContainer = this.$el.find("#import-row-body");

            _.each(this.model.attributes.Fields, function (f) {
                fieldContainer.append(_.template(entityOverviewFieldTemplate, { key: f.FieldTypeImportDisplayValue, persistedvalue: f.PersistedValue, value: f.DisplayValue }));
            });

        },
        renderForm: function (id) {
            var that = this;

            // Create form using dynamic schema
            var container = this.$el.find("#inriver-fields-form-area");
            container.empty();

            container.append("<h2>" + this.model.get("Name") + "</h2>");

            if (this.model.get("Fields").length == 0) {
                container.append("<i class='fa fa-warning'></i> " + this.model.get("ErrorMessage"));
                return this;
            }

            var schema = inRiverUtil.createFormsSchemaFromRestfulFields(this.model);

            this.schema = schema;
            this.form = new Backbone.Form({
                model: this.model,
                schema: schema
            }).render();
            inRiverUtil.applyCenteredLayoutToForm(this.form.$el);

            container.append(this.form.el);

            that.$el.find(".width-limited-container").parent().hide();

            var resultModel = _.find(that.importCollection.models,
                function (model) {
                    return model.id == id;
                });

            _.each(resultModel.get("Fields"), function (field) {
                if (field.Valid == false) {
                    // $(".width-limited-container").find("label:contains('" + field.FieldTypeDisplayName + "')").first().css("color", "darkred");
                    var element = that.$el.find(".width-limited-container").find("label:contains('" + field.FieldTypeDisplayName + "')");

                    if (element.length > 0) {
                        element.parent().parent().show();
                        element.append("&nbsp;&nbsp;<i class='fa fa-warning' title='" + field.ErrorMessage + "'/>");
                    }
                }
            });

            container.append("<p></p><button id='save-button' title='Review and correct all errors'>Update</button>");

            return this;
        },
        render: function (id) {
            var that = this;

            this.$el.html(_.template(validateFieldsTemplate, { total: this.entitiesProcessed, added: this.newModels.length, updated: this.modifiedModels.length, nochanges: this.nochangesModels.length, error: this.invalidModels.length }));

            if (this.newModels.length > 0) {
                var reviewContainer = this.$el.find("#review-add-area");
                _.each(this.newModels, function(model) {
                    reviewContainer.append("<li><a href='#' class='entity-review add' id='" + model.id + "'> " + model.attributes.Name + "</li>");
                });
                showMore("add");
            } else {
                hideLinkType("add");
            }

            if (this.modifiedModels.length > 0) {
                reviewContainer = this.$el.find("#review-update-area");
                _.each(this.modifiedModels, function(model) {
                    reviewContainer.append("<li><a href='#' class='entity-review update' id='" + model.id + "'> " + model.attributes.Name + "</li>");
                });
                showMore("update");
            } else {
                hideLinkType("update");
            }

            if (this.nochangesModels.length > 0) {
                reviewContainer = this.$el.find("#review-nochanges-area");
                _.each(this.nochangesModels, function(model) {
                    reviewContainer.append("<li>" + model.attributes.Name + "</li>");
                });
                showMore("nochanges");
            } else {
                hideLinkType("nochanges");
            }

            if (this.invalidModels.length > 0) {
                reviewContainer = this.$el.find("#review-error-area");
                _.each(this.invalidModels.models, function(model) {
                    //reviewContainer.append("<li><a href='#' class='entity-review' id='" + model.attributes.EntityId + "'><i class='fa fa-times fa-lg'></i> " + model.attributes.Name + "</li>");
                    reviewContainer.append("<li><a href='#' class='entity-review error' id='" + model.id + "'> " + model.attributes.Name + "</li>");
                });
                showMore("error");
            } else {
                hideLinkType("error");
            }

            function showMore(id) {
                var $area = that.$el.find("#review-" + id + "-area");
                var $heading = that.$el.find("#" + id + "-heading");
                var $showIcons = that.$el.find("#" + id + "-heading" + " i");

                $heading.click(function () {
                    $area.slideToggle(200);
                    $showIcons.toggle();
                });
            }

            function hideLinkType(id) {
                var $area = that.$el.find("#" + id + "-container");
                $area.hide();
            }

            var container = this.$el.find("#inriver-fields-form-area");
            var statusContainer = this.$el.find("#validation-status");

            if (that.invalidModels.length > 0) {
                statusContainer.html("<i class='fa fa-times error'></i> Validation completed (but with errors)");
                //container.append("Review and correct all errors and click Update for each entity with error.");
                that.model = _.first(that.invalidModels.models);
                id = that.model.id;
                // Active link in review area
                this.$el.find("#review-error-area a[id='" + id + "']").addClass("active");
            } else {
                window.appHelper.event_bus.trigger('validationstatus', this.fileImportModel);
                statusContainer.html("<i class='fa fa-check success'></i> Validation completed");
                //container.append("Validation successful! Review changes and click next for Import.");
             }

            if (id != undefined) {
                this.renderForm(id);
            }
        }
    });

    return validateFieldsView;
});
