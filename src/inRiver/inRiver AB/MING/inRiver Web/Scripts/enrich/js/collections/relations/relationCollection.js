define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/relation/relationModel'
], function ($, _, backbone, relationModel) {
    var relationCollection = backbone.Collection.extend({
        model: relationModel,
        initialize: function (models, options) {
            this.entityId = options.entityId;
            this.direction = options.direction;
        },
        url: function () {
            return '/api/relation?id=' + this.entityId + '&direction=' + this.direction;
        }
    });

    return relationCollection;
});
