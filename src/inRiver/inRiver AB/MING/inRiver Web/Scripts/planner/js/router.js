﻿
// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'deep-model',
  'misc/plannerGlobals',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/topheader/topHeaderView',
  'sharedjs/views/workarea/workAreaHandler',
  'views/appheader/appHeaderView',
  'views/planner/plannerGanttView',
  'views/planner/plannerEntityView',
  'views/task/taskView',
  'views/start/startPageView',
  'views/planner/plannerConfigurationView',
  'text!templates/planner/plannerNavigationTemplate.html',
    'sharedjs/models/setting/SettingModel'

], function ($, _, Backbone, jqueryui, DeepModel, plannerGlobals, inRiverUtil,
topHeaderView, workAreaHandler, appHeaderView, plannerGanttView,
plannerEntityView, taskView, startPageView, plannerConfigurationView, plannerNavigationTemplate,
settingModel) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            "home": "index",
            "workarea?*queryString": "workarea",
            "workareasearch?*queryString": "workareasearch",
            "dashboard(/:id)": "dashboard",
            "gantt(/:id)": "gantt",
            "configuration(/:id)(/:tab)": "configuration",
            "entity/:id(/:tab)": "entity",
        },
        index: function(id) {
            this.navigate("dashboard", { trigger: true, replace: true });
        },
        dashboard: function (id) {
            this.switchView(new startPageView());
        },
        gantt: function (id) {
            this.switchView(new plannerGanttView());
        },
        configuration: function (id, tab) {
            if (tab) {
                appSession.plannerConfigurationTabId = "tab-" + tab;
            } else {
                appSession.plannerConfigurationTabId = "tab-quickedit";
            }

            this.switchView(new plannerConfigurationView());
        },
        entity: function (id, tab) {
            if (tab) {
                appSession.plannerEntityViewTabId = "tab-" + tab;
            } else {
                appSession.plannerEntityViewTabId = "tab-overview";
            }

            var hasEmptyWorkArea;
            if (!app_router.workAreaHandler.hasWorkareas()) {
                app_router.workAreaHandler.reopenOrAddWorkAreas({ title: "Empty" });
                hasEmptyWorkArea = app_router.workAreaHandler.hasEmptyWorkareas();
            } else {
                hasEmptyWorkArea = false;
            }
            app_router.workAreaHandler.setSmallWorkareas();

            this.switchView(new plannerEntityView({ modelToEdit: id, queryview: this.workAreaView, hasEmptyWorkArea: hasEmptyWorkArea }));

            app_router.workAreaHandler.bindEvents();
            app_router.currentViewType = "planner";
        },
        task: function (id, linkId) {
            this.switchView(new taskView({ id: id, linkId: linkId }));
        },
        switchView: function (newView, container) {
            this.closeView(this._currentView);
            window.scrollTo(0, 0);

            // If not container specified use main default
            if (container == undefined) {
                container = $("#main-container");
                this._currentView = newView;
            }

            container.html(this._currentView.el);
            if (this._currentView.onInsertedInDOM) {
                this._currentView.onInsertedInDOM();
            }
        },
        closeView: function (specView) {
            if (specView) {
                specView.close();
                specView.remove();
            }
        },
        parseQueryString: function (queryString) {
            var params = {};
            if (queryString) {
                _.each(
                    _.map(decodeURI(queryString).split(/&/g), function (el, i) {

                        var key = el.substr(0, el.indexOf('='));
                        var value = el.substr(el.indexOf('=') + 1);
                        var o = {};
                        o[key] = value;
                        // var aux = el.split(/=(.+)?/), o = {};
                        //if (aux.length >= 1) {
                        //    var val = undefined;
                        //    if (aux.length == 2)
                        //        val = aux[1];
                        //o[aux[0]] = val;
                        //}
                        return o;
                    }),
                    function (o) {
                        _.extend(params, o);
                    }
                );
            }
            return params;
        }
    });

    var initialize = function () {
        console.log("init router");
        appHelper.event_bus = _({}).extend(Backbone.Events);

        // Render top header
        var headerView = new topHeaderView({ app: "Planner" });

        // Render top header navigation (kind of ugly to do it here... fix later)
        $("#context-menu-container-left").html(_.template(plannerNavigationTemplate)());

        plannerGlobals.init(); // set up all data

        window.app_router = new AppRouter;
        app_router.workAreaHandler = new workAreaHandler({ app: "planner" });
        window.appHelper.workAreaHandler = app_router.workAreaHandler;

        app_router.on('route', function (id, tab) {
            if (id != "entity" && id != "workareasearch") {
                app_router.workAreaHandler.closeWorkAreas();
                app_router.workAreaHandler.hideWorkareas();
            }

            // Show current route in the top header navigation area
            $("#header-navigation-links-wrap a").removeClass("current");
            $("#header-navigation-links-wrap a[href='#" + id + "']").addClass("current");
            $("#header-navigation-links-wrap a[href='#entity']").css('visibility', id == "entity" ? "visible" : "hidden");
        });

        app_router.on('route:workareasearch', function (queryString) {
            if (app_router.currentViewType == "start") { //}|| app_Router.currentViewType =="entity") {
                this.switchView(app_router.workAreaHandler);
                if (app_router.currentViewType == "entity") {
                    app_router.workAreaHandler.setWideWorkareas();
                }

                app_router.currentViewType = "workarea";
            }
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var workarea = app_router.workAreaHandler.getActiveWorkarea();

            if (params.advsearch != undefined) {
                app_router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
            } else if (params.searchPhrase != undefined) {
                app_router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
            } else if (params.compressedEntities != undefined) {
                app_router.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities });
            }

        });

        app_router.on('route:workarea', function (queryString) {
            var params = this.parseQueryString(queryString);
            var advsearchPhrase = window.appHelper.advQuery;
            var rendered = false;


            if (!app_router.workAreaHandler.hasWorkareas()) {
                var title = params.title;
                if (title == undefined || title == null) {
                    title = "Empty";
                }
                if (params.workareaId != undefined && (params.isQuery != undefined && params.isQuery.toString() == "true")) {
                    app_router.workAreaHandler.openSavedQuery({ title: title, workareaId: params.workareaId });
                    rendered = true;
                } else if (params.workareaId != undefined) {
                    app_router.workAreaHandler.openSavedWorkarea({ title: title, workareaId: params.workareaId });
                    rendered = true;
                } else {
                    app_router.workAreaHandler.reopenOrAddWorkAreas({ title: title, compressedEntities: params.compressedEntities, workareaId: params.workareaId, searchPhrase: params.searchPhrase, advsearchPhrase: advsearchPhrase, hasworkareaSearch: true, isQuery: params.isQuery });
                    rendered = true;
            }

            }
            if (!rendered) {
                if (params.advsearch != undefined) {
                    app_router.workAreaHandler.querysearch({ advsearchPhrase: advsearchPhrase });
                } else if (params.searchPhrase != undefined) {
                    app_router.workAreaHandler.search({ searchPhrase: params.searchPhrase });
                } else if (params.compressedEntities != undefined) {
                    app_router.workAreaHandler.openWorkareaWithEntites({ title: params.title, compressedEntities: params.compressedEntities });
                } else if (params.workareaIds != undefined) {
                    if (params.isQuery != undefined && params.isQuery.toString() == "true") {
                        app_router.workAreaHandler.openSavedQuery({ title: params.title, workareaId: params.workareaId });
                    } else {
                        app_router.workAreaHandler.openSavedWorkarea({ title: params.title, workareaId: params.workareaId });
                    }
                } else {
                    app_router.workAreaHandler.renderResults();
                }
            }
            app_router.workAreaHandler.setWideWorkareas();
            app_router.workAreaHandler.bindEvents();
            this.switchView(app_router.workAreaHandler);
            app_router.currentViewType = "workarea";
        });
        
        if (!Backbone.history.start()) {
            app_router.navigate("dashboard", { trigger: true, replace: true });
        }
    };


        // Extend the View class to include a navigation method goTo
        Backbone.View.prototype.goTo = function (loc, changeurl) {
            if (changeurl == undefined) {
                changeurl = { trigger: true };
            }

            // Check for unsaved changes in current view before navigating.
            // When trigger is false it means that the view is actually switched elsewhere
            // and therefore the check for unsaved changes must be handled there instead (only the
            // browser url will be affected when trigger is set to false).
            if (changeurl.trigger) {
                inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(app_router._currentView, function () {
                    app_router.navigate(loc, changeurl);
                });
            } else {
            app_router.navigate(loc, changeurl);
            }
        };

        // Extend View with a close method to prevent zombie views (memory leaks)
        Backbone.View.prototype.close = function () {

            this.stopListening();
            this.$el.empty();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };
        
        Backbone.View.prototype.renderLoadingSpinner = function () {
            this.$el.html(Backbone.LoadingSpinnerEl);
        };

    return {
        initialize: initialize
    };

});




