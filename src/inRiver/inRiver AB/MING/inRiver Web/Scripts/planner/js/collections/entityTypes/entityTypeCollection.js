define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/entityType/entityTypeModel'
], function ($, _, Backbone, entityTypeModel) {
    var entityTypeCollection = Backbone.Collection.extend({
        model: entityTypeModel,
        url: '/api/entitytype/plannerentitytypes'
    });

    return entityTypeCollection;
});
