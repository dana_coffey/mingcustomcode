define([
  'jquery',
  'underscore',
  'backbone',
  'colpick',
  'misc/plannerGlobals',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'sharedjs/models/setting/settingModel',
  'sharedjs/models/entityType/entityTypeModel',
  'sharedjs/models/field/fieldModel',
  'sharedjs/models/cvl/CvlValueModel',
  'sharedjs/collections/fieldtypes/FieldTypesCollection',
  'sharedjs/collections/cvl/CvlValueCollection',
  'collections/entityTypes/entityTypeCollection',
  'modalPopup',
  'text!templates/configuration/configurationColorViewTemplate.html',
  'text!templates/configuration/configurationColorFieldTypesTemplate.html',
  'text!templates/configuration/configurationColorCvlValuesTemplate.html',
  'text!templates/configuration/configurationColorColorpickerTemplate.html',
  'views/configuration/cvlValueColorSelectionRow'
], function ($, _, Backbone, colpick, plannerGlobals, inRiverUtil, permissionUtil, settingModel, entityTypeModel, fieldModel, cvlValueModel, fieldTypesCollection, cvlValueCollection, entityTypeCollection, modalPopup, configurationColorViewTemplate, configurationColorFieldTypesTemplate, configurationColorCvlValuesTemplate, configurationColorColorpickerTemplate, cvlValueColorSelectionRow) {

    var configurationColorView = Backbone.View.extend({
        initialize: function () {
            window.appHelper.event_bus.off('planner-configuration-save');
            window.appHelper.event_bus.off('planner-configuration-undo');
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-save', this.onSaveAll);
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-undo', this.onUndoAll);

            this.model = plannerGlobals.colorConfigModel.clone();
            
            this.fieldTypesCollection = new fieldTypesCollection();
            this.listenTo(this.fieldTypesCollection, "reset", this.onFieldTypesCollectionReset);

            this.cvlValueCollection = new cvlValueCollection(null, { cvlId: null });
            this.listenTo(this.cvlValueCollection, "reset", this.onCvlValueCollectionReset);

            this.currentEntityTypeId = _.keys(window.plannerGlobals.entityTypes.attributes)[0];

            this.render();

            this.reloadDataForCurrentEntityType();

            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
        },
        events: {
            "change #configuration-color-entitytypeids": "onChangeEntityType",
            "change #configuration-color-cvlfieldtypes": "onChangeFieldType",
        },
        reloadDataForCurrentEntityType: function () {
            this.fieldTypesCollection.fetch({ reset: true, data: { id: this.currentEntityTypeId } });
        },
        onChangeEntityType: function () {
            this.currentEntityTypeId = this.$el.find("#configuration-color-entitytypeids select").val();
            this.reloadDataForCurrentEntityType();
        },
        onChangeFieldType: function () {
            var selectedFieldType = this.$el.find("#configuration-color-cvlfieldtypes select").val();
            this.model.set(this.currentEntityTypeId + ".FieldTypeId", selectedFieldType);
            this.model.set(this.currentEntityTypeId + ".CvlValueColors", {}); // clear all color data
            this.loadCvlValuesForCurrentFieldType();
            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
        },
        onFieldTypesCollectionReset: function () {
            this.loadCvlValuesForCurrentFieldType();
        },
        loadCvlValuesForCurrentFieldType: function () {
            if (this.model.get(this.currentEntityTypeId + ".FieldTypeId")) {
                var currentFieldType = this.fieldTypesCollection.get(this.model.get(this.currentEntityTypeId + ".FieldTypeId"));
                this.cvlValueCollection.cvlId = currentFieldType.get("cvlid");
                this.cvlValueCollection.fetch({ reset: true });
            } else {
                this.cvlValueCollection.reset();
            }
        },
        onCvlValueCollectionReset: function () {
            var that = this;
            this.updateUi();

            // Build colors table
            this.$el.closest("body").find(".colpick").remove();
            this.$el.find(".cvlvalue-color-selector-row").remove();
            this.cvlValueCollection.each(function (cvlValue) {
                var row = new cvlValueColorSelectionRow({
                    parent: that,
                    cvlValue: cvlValue,
                    entityTypeId: that.currentEntityTypeId,
                    tabColor: that.model.get(that.currentEntityTypeId + ".CvlValueColors." + cvlValue.get("Key") + ".BarColor"),
                    textColor: that.model.get(that.currentEntityTypeId + ".CvlValueColors." + cvlValue.get("Key") + ".TextColor")
                });
                that.$el.find("table").append(row.$el.children(0));
            });
        },
        onColorsChanged: function (cvlValueKey, barColor, textColor) { // called from a "row"
            this.model.set(this.currentEntityTypeId + ".CvlValueColors." + cvlValueKey, { BarColor: barColor, TextColor: textColor });
            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
        },
        onSaveAll: function () {
            // Update "real" model and save to server
            plannerGlobals.colorConfigModel.clear({ silent: true });
            plannerGlobals.colorConfigModel.save(this.model.toJSON()); // Copy attributes to the "real" model
            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
            window.plannerGlobals.ganttDataCollection.reset(); // force reload of gantt data
            inRiverUtil.Notify("Configurations have been saved");
        },
        onUndoAll: function () {
            this.model = plannerGlobals.colorConfigModel.clone();
            this.reloadDataForCurrentEntityType(); // reload data for current view state
            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
        },
        reloadFieldTypesCollection: function() {
            this.fieldTypes.fetch({ reset: true, data: { id: this.getEntityType() } });
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            return !_.isEqual(plannerGlobals.colorConfigModel.toJSON(), this.model.toJSON());
        },
        updateUi: function() {
            this.$el.find("#configuration-color-entitytypeids").val(this.currentEntityTypeId);

            // Populate field type select
            var fieldTypeSelectEl = this.$el.find("#configuration-color-cvlfieldtypes select");
            fieldTypeSelectEl.empty();
            var noneOptionEl = $("<option></option>")
                .attr("value", "")
                .text("(none)");
            fieldTypeSelectEl.append(noneOptionEl);

            this.fieldTypesCollection.each(function (ftModel) {
                if (ftModel.get("datatype") === "CVL" && ftModel.get("cvlid") && !ftModel.get("multivalue")) {
                    var optionEl = $("<option></option>")
                       .attr("value", ftModel.get("id"))
                       .text(ftModel.get("displayname"));
                    fieldTypeSelectEl.append(optionEl);
                }
            });
            fieldTypeSelectEl.val(this.model.get(this.currentEntityTypeId + ".FieldTypeId")); // set selected
        },
        render: function () {
            this.$el.html(_.template(configurationColorViewTemplate, {}));

            // Populate this select just once, it doesn't change
            var entityTypeSelectEl = this.$el.find("#configuration-color-entitytypeids select");
            _.each(window.plannerGlobals.entityTypes.attributes, function (entityType, value) {
                var optionEl = $("<option></option>")
                       .attr("value", value)
                       .text(entityType.Name);
                entityTypeSelectEl.append(optionEl);
            });

            return this; // enable chained calls
        }
    });

    return configurationColorView;
});

