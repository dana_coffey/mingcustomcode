define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
    'alertify',
  'misc/plannerGlobals',
  'jquery-ui-touch-punch',
  'sharedjs/views/panelsplitter/panelBorders',
  'sharedjs/misc/permissionUtil',
  'sharedjs/collections/relations/relationTypeCollection',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/models/setting/SettingModel',
  'views/campaign/campaignViewContentSubView',
  'views/relation/relationView',
  'views/entityview/entityOverviewView',
  'views/relation/relationCampaignSubView',
  'sharedjs/views/lightboard/lightboardView',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/copyEntity/copyEntityWizzardView',
  'text!templates/entityView/entityViewTemplate.html'

], function ($, _, Backbone, inRiverUtil, modalPopup, alertify, plannerUtil, jqtp, panelBorders, permissionUtil, relationTypeCollection, entityModel, relationModel, completenessModel, settingModel, campaignViewContentSubView, relationView, entityOverviewView, relationCampaignSubView, lightboardView, completenessPopupView, entityDetailSettingWrapperView, copyEntityWizzardView, entityViewTemplate) {

    var campaignView = Backbone.View.extend({
        initialize: function (options) {

            var self = this;

            this.campaignOutboundLinks = null;
            this.plannerEntityTypesAsTabs = null;
            this.filteredEntityTypeTabs = [];

            var onDataHandler = function () {
                if (self.model.get("EntityType") == appHelper.topLevelPlannerEntityType) {
                    self.getAllCampaignOutboundRelations();
                } else {
                    self.render();
                }
            };

            if (this.model) {
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            } else {
                var id = options.modelToEdit;
                this.model = new entityModel(id);
                this.model.url = "/api/entity/" + id;
                this.model.fetch({
                    success: onDataHandler,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        events: {
            "click #clone-button": "onClone",
            "click #edit-button": "onEdit",
            "click #control-row-star-button": "onStar",
            "click #add-task-button": "onNewTask",
            "click #tab-overview": "onChangeTab",
            "click #tab-content": "onChangeTab",
            "click #tab-details": "onChangeTab",
            "click #tab-inbound": "onChangeTab",
            "click .completeness": "onShowCompletenessDetails",
            "click #delete-button": "onEntityDelete",
            "click .control-row-tab-button": "onChangeTab",
            "click #system-button": "onShowSystemInfo",
            "click #lock-button": "onEntityLock",
            "click #unlock-button": "onEntityUnlock",
            "click #star-button": "onEntityStar",
            "click #closeentity": "onEntityClose",
            "click #entity-copy-button": "onCopyEntityClick"
        },
        onClone: function () {
            // is this function actually in use?! If not, then remove!
            var that = this;
            alertify.confirm("Do you want to create a copy of this item?", function (e) {
                if (e) {

                    var newModel = that.model.clone();
                    newModel.id = null;
                    newModel.attributes['Id'] = 0;

                    newModel.attributes.Fields.forEach(function (f) {
                        if (f.IsUnique == true) {
                            f.Value = "Copy of " + f.Value;
                        }

                    });
                    window.plannerGlobals.ganttDataCollection.add(newModel);
                    newModel = newModel.save({}, {
                        success: function (nmodel, response) {


                            //that.activityCollection.models.forEach(function (m) {

                            //    var newModel = m.clone();
                            //    newModel.id = null;
                            //    newModel.attributes.CampaignId = nmodel.id;
                            //    newModel.attributes.Id = 0;

                            //    newModel.urlRoot = "/api/activity/" + nmodel.id;
                            //    newModel.save();
                            //});

                            //setTimeout(function () {
                            //    that.goTo("entity/" + nmodel.id);
                            //}, 200);


                        },
                        error: function (model, response) {
                            alert("Error!");

                            window.plannerGlobals.ganttDataCollection.remove(model);
                            that.done();
                        },
                        wait: true
                    });

                }
            });
        },
        onEntityDelete: function () {
            inRiverUtil.deleteEntity(this);
            window.plannerGlobals.ganttDataCollection.remove(this.model);
            this.listenTo(appHelper.event_bus, 'entitydeleted', this.afterEntityDelete);
        },
        afterEntityDelete: function (id) {
            this.goTo("dashboard");
        },
        //activityAddedNowLinkEntity: function (model) {

        //    var self = this;

        //    this.campaignRelationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: appHelper.topLevelPlannerEntityType });

        //    this.campaignRelationTypes.fetch({
        //        success: function () {


        //            self.CampaignActivityRelationType = _.find(self.campaignRelationTypes.models, function (model) {
        //                return model.attributes.TargetEntityTypeId == "Activity";

        //            });
        //            if (!self.CampaignActivityRelationType) {
        //                return;
        //            }

        //            var linktype = self.CampaignActivityRelationType.attributes.Id;

        //            var newModel = new relationModel({ LinkType: linktype, TargetId: model.attributes.Id, SourceId: self.model.id, LinkEntityId: null });
        //            newModel.save([], {
        //                success: function (model) {
        //                    appHelper.event_bus.trigger('entityupdated', self.model.id);
        //                    inRiverUtil.Notify("Relation successfully created");
        //                },
        //                error: function (model, response) {
        //                    inRiverUtil.OnErrors(model, response);
        //                }
        //            });
        //        },
        //        error: function (model, response) {
        //            inRiverUtil.OnErrors(model, response);
        //        }
        //    });


        //},
        onNewTask: function (e) {
            e.stopPropagation();


            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });


        },
        taskAddedNowLinkEntity: function (model) {
            var self = this;

            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function (model) {
                    appHelper.event_bus.trigger('entityupdated', self.model.id);
                    inRiverUtil.Notify("Relation successfully created");
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onEdit: function (e) {
            e.stopPropagation();
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
            window.plannerGlobals.plannerCollectionIsDirty = true;
        },
        onStar: function (e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();

            if (this.model.attributes["IsStarred"]) {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("IsStarred", false);

                e.currentTarget.title = "Star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star");
                $(e.currentTarget).addClass("icon-entypo-fix-star-empty");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("IsStarred", true);

                e.currentTarget.title = "Un-star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star-empty");
                $(e.currentTarget).addClass("icon-entypo-fix-star");
            }
            window.plannerGlobals.plannerCollectionIsDirty = true;
        },
        onShowSystemInfo: function () {

            inRiverUtil.ShowEntityInfo(this.model.id);

        },
        onEntityLock: function () {
            var self = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#lock-button").hide();
                    $("#entitylocked").show();
                    $("#entitylocked").attr("title", "Locked by " + window.appHelper.userName);
                    $("#unlock-button").show();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                }
            }
            xmlhttp.open("GET", "/api/tools/lockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityUnlock: function () {
            var self = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#lock-button").show();
                    $("#entitylocked").hide();
                    $("#unlock-button").hide();
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                }
            };

            xmlhttp.open("GET", "/api/tools/unlockentity/" + this.model.id, true);
            xmlhttp.send();
        },
        onEntityStar: function (e) {
            e.stopPropagation();
            var self = this;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    window.appHelper.event_bus.trigger('entityupdated', self.model.id);
                }

            }
            if (this.model.attributes["Starred"] == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "0");

                e.currentTarget.title = "Star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star");
                $(e.currentTarget).addClass("icon-entypo-fix-star-empty");
            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.model.id, true);
                xmlhttp.send();
                this.model.set("Starred", "1");

                e.currentTarget.title = "Un-star Entity";
                $(e.currentTarget).removeClass("icon-entypo-fix-star-empty");
                $(e.currentTarget).addClass("icon-entypo-fix-star");
            }

        },
        onEntityClose: function () {
            window.appHelper.event_bus.trigger('entityareaclose');
            this.remove();
            this.unbind();
        },
        onCopyEntityClick: function (e) {
            //Open popup with wizzard.
            e.stopPropagation();
            this.modal = new modalPopup();
            this.modal.popOut(new copyEntityWizzardView({ id: this.model.id, entityTypeId: this.model.attributes.EntityType, model: this.model }), { size: "medium", header: "Copy " + this.model.attributes.EntityType });
        },
        onChangeTab: function (e) {
            var self = this;

            if (window.appSession.campaignViewTabId == e.currentTarget.id) {
                return;
            }

            inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(this._currentTopTabView, function () {
                window.appSession.campaignViewTabId = e.currentTarget.id; // remember default tab
                var urlTabName = window.appSession.campaignViewTabId.replace("tab-", "");

                self.goTo("entity/" + self.model.get("Id") + "/" + urlTabName, { trigger: false });

                self.createTabView(window.appSession.campaignViewTabId);
                self.$el.find("#entity-top-info-container").append(self._currentTopTabView.el);
                self.indicateActiveTab();
            });
        },
        onClose: function () {
            if (this.entityDetailSettingWrapperView) {
                this.entityDetailSettingWrapperView.close();
            }
        },
        onShowCompletenessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },
        createDetailsTabView: function () {

            var contentSubView = new entityDetailSettingWrapperView({
                model: this.model
            });

            return contentSubView;
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            if (this._currentTopTabView == null) {
                return false;
            }

            if (_.isFunction(this._currentTopTabView.hasUnsavedChanges)) {
                return this._currentTopTabView.hasUnsavedChanges();
            }
            return false;
        },
        createTabView: function (idOfTab) {

            var self = this;

            if (this._currentTopTabView) {
                this._currentTopTabView.remove();

                if (this._currentTopTabView.detailfields) {
                    for (var index = 0; index < this._currentTopTabView.detailfields.length; index++) {
                        this._currentTopTabView.detailfields[index].stopListening(window.appHelper.event_bus);
                        this._currentTopTabView.detailfields[index].remove();
                        this._currentTopTabView.detailfields[index].unbind();
                    }
                }

                this._currentTopTabView.close();
            }

            if (idOfTab == "tab-overview") {
                this._currentTopTabView = this.createOverviewTabView();
            }
            else if (idOfTab == "tab-details") {
                this._currentTopTabView = this.createDetailsTabView();
            }
            else if (idOfTab == "tab-media") {
                this._currentTopTabView = this.createLightboardTabView();
            }
            else if (idOfTab == "tab-content") {
                this._currentTopTabView = this.createContentTabView("outbound");
            }
            else if (idOfTab == "tab-inbound") {
                this._currentTopTabView = this.createContentTabView("inbound");
            }
            else {

                var relations = [];

                _.each(self.plannerEntityTypesAsTabs, function (entityType) {

                    if (idOfTab == "tab-" + entityType.targetEntityTypeIdToLower) {
                        relations.push(entityType);
                    }
                });

                if (relations.length > 0) {
                    self._currentTopTabView = self.createSubTabView(relations);
                }
            }
        },
        createOverviewTabView: function () {
            var contentSubView = new entityOverviewView({
                model: this.model
            });
            return contentSubView;
        },
        createSubTabView: function (relations) {

            var contentSubView = new relationCampaignSubView({
                model: this.model,
                relationTypes: relations
            });

            return contentSubView;

        },
        createLightboardTabView: function () {
            var contentSubView = new lightboardView({
                Name: "Lightboard",
                id: this.model.id,
                type: this.model.attributes.EntityType
            });
            return contentSubView;
        },
        createContentTabView: function (linkDirection) {
            var self = this;

            var contentSubView = new relationView({
                Name: "Content",
                id: this.model.id,
                type: this.model.attributes.EntityType,
                direction: linkDirection,
                excludeLinks: self.plannerEntityTypesAsTabs
            });
            return contentSubView;
        },
        indicateActiveTab: function () {
            var self = this;

            this.$el.find("#tab-overview").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-details").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-media").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-content").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-inbound").removeClass("control-row-tab-button-active");

            if (this.plannerEntityTypesAsTabs != "") {
                _.each(this.plannerEntityTypesAsTabs, function (entityType) {
                    self.$el.find("#tab-" + entityType.targetEntityTypeIdToLower).removeClass("control-row-tab-button-active");
                });
            }

            this.$el.find("#" + appSession.campaignViewTabId).addClass("control-row-tab-button-active");
        },
        entityUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch({
                    success: function () {
                        self.rerender = true;
                        self.render();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
            window.plannerGlobals.ganttDataCollection.reset(); // force reload of gantt data
        },
        drawCompleteness: function (percent) {
            this.$el.find('.percent').html(Math.round(percent) + '%');

            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        getAllCampaignOutboundRelations: function () {
            var self = this;

            var campaignRelationTypes = new relationTypeCollection([], { direction: "outbound", entityTypeId: appHelper.topLevelPlannerEntityType });

            campaignRelationTypes.fetch({
                success: function (relations) {

                    var relationTypeModels = [];

                    _.each(relations.models, function (relation) {
                        relationTypeModels.push(relation);
                    });

                    self.campaignOutboundLinks = relationTypeModels;

                    self.getAllOutboundRelationsAsTabs();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);

                    self.getAllOutboundRelationsAsTabs();
                }
            });

        },
        getAllOutboundRelationsAsTabs: function () {
            var self = this;

            var setModel = new settingModel();
            setModel.url = "/api/setting/PLANNER_ENTITYTYPES";
            setModel.fetch({
                success: function (model, response) {
                    var entityTypeIds = response.split(',');

                    var relationModelAsTabs = [];

                    _.each(self.campaignOutboundLinks, function (outboundLink) {

                        if (_.contains(entityTypeIds, outboundLink.get("TargetEntityTypeId"))) {

                            if (containsId(relationModelAsTabs, outboundLink.get("Id")).length == 0) {
                                var linkObject = outboundLink.toJSON();
                                linkObject.targetEntityTypeIdToLower = outboundLink.get("TargetEntityTypeId").toLowerCase();

                                relationModelAsTabs.push(linkObject);

                                if (containsTargetEntityTypeId(self.filteredEntityTypeTabs, linkObject.TargetEntityTypeId).length == 0) {

                                    self.filteredEntityTypeTabs.push(linkObject);
                                }
                            }
                        }
                    });

                    self.plannerEntityTypesAsTabs = relationModelAsTabs;

                    self.render();

                    function containsTargetEntityTypeId(arr, id) {

                        var matches = _.filter(arr, function (value) {
                            if (value["TargetEntityTypeId"] == id) {
                                return value;
                            }
                        });

                        return matches;
                    }

                    function containsId(arr, id) {

                        var matches = _.filter(arr, function (value) {
                            if (value.Id == id) {
                                return value;
                            }
                        });

                        return matches;
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        render: function () {

            if (this.model.get("EntityType") == null) {
                //If model does not exist in system. Show tab with some error text.
                return this;
            }

            if (this.model.get("EntityType") == appHelper.topLevelPlannerEntityType && this.plannerEntityTypesAsTabs == null) {
                return this;
            }

            this.$el.html(_.template(entityViewTemplate, {
                data: this.model.toJSON(),
                userName: window.appHelper.userName,
                icon: this.model.get("EntityType"),
                entityType: this.model.get("EntityType"),
                outboundLinks: this.filteredEntityTypeTabs
            }));
            
            this.createTabView(appSession.campaignViewTabId);

            this.$el.find("#entity-top-info-container").append(this._currentTopTabView.el);

            // Set borders, splitters and collapse
            panelBorders.initAllEvents();

            this.indicateActiveTab();

            this.stopListening(window.appHelper.event_bus);
            window.appHelper.event_bus.off('entityupdated');
            this.listenTo(window.appHelper.event_bus, 'entityupdated', this.entityUpdated);
            this.listenTo(window.appHelper.event_bus, 'relationremoved', this.entityUpdated);
            
            var collapsed = sessionStorage.getItem("workarea-collapsed");

            if (collapsed && collapsed.toString() == "true") {
                if ($("#workarea-header-label").hasClass("workarea-header-label-horisontal")) {
                    $("#workarea-header-toggle-button").click();
                }
            };

            var callback = function (access, id, that) {
                if (!access) {
                    that.$el.find(id).hide();
                }
            };

            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "UpdateEntity", this.$el.find("#edit-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "DeleteEntity", this.$el.find("#delete-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "AddEntity", this.$el.find("#add-task-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "LockEntity", this.$el.find("#lock-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "LockEntity", this.$el.find("#unlock-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "CopyEntity", this.$el.find("#entity-copy-button"));
            permissionUtil.CheckPermissionForElement(window.appHelper.userName, "AddEntity", this.$el.find("#entity-copy-button"));
            this.drawCompleteness(this.model.attributes.Completeness);

            return this; // enable chained calls
        },
    });

    return campaignView;
});

