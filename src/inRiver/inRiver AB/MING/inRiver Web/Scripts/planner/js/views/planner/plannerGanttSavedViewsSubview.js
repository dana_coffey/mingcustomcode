define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
    'text!templates/planner/plannerGanttSavedViewsSubviewTemplate.html',
    'text!templates/planner/plannerGanttSavedViewsListItemTemplate.html'

], function ($, _, Backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    plannerGanttSavedViewsSubViewTemplate, plannerGanttSavedViewsListItemTemplate) {

    var plannerGanttSavedViewsSubView = Backbone.View.extend({
        initialize: function (options) {
            this.parent = options.parent;
            this.template = _.template(plannerGanttSavedViewsSubViewTemplate);
            this.listItemTemplate = _.template(plannerGanttSavedViewsListItemTemplate);

            this.listenTo(appHelper.event_bus, 'popupcancel', this.onCancel);
            this.listenTo(appHelper.event_bus, 'popupsave', this.onSave);

            this.listenTo(plannerGlobals.viewStateCollection, "add remove", this.onViewStateCollectionUpdated);
            plannerGlobals.viewStateCollection.fetch();

            if (!plannerGlobals.currentSavedViewsTab) plannerGlobals.currentSavedViewsTab = "#sharedwithme";
            if (!plannerGlobals.currentSavedViewsFilter) plannerGlobals.currentSavedViewsFilter = "";

            this.render();
            this.updateUi();
        },
        events: {
            "click #saved-views-wrap a": "onTabClicked",
            "keyup #filter-string": "onFilterChanged",
            "change #filter-string": "onFilterChanged",
            "click #filter-string-wrap i": "onClearFilterClicked",
            "click #view-name": "onViewClicked",
            "click #share": "onShareClicked",
            "click #remove": "onRemoveClicked",
            "click #ical-link": "onIcalLinkClicked"
        },
        onViewStateCollectionUpdated: function() {
            this.render();
        },
        onTabClicked: function (e) {
            e.preventDefault();
            plannerGlobals.currentSavedViewsTab = $(e.currentTarget).attr("href");
            this.renderList();
            this.updateUi();
        },
        onFilterChanged: function(e) {
            plannerGlobals.currentSavedViewsFilter = this.$el.find("#filter-string").val();
            this.updateClearSearchButtonVisibility();
            this.renderList();
        },
        onClearFilterClicked: function(e) {
            plannerGlobals.currentSavedViewsFilter = "";
            this.updateUi();
            this.renderList();
        },
        onViewClicked: function(e) {
            var viewToOpen = $(e.currentTarget).closest("li").attr("data-id");
            var tmp = plannerGlobals.viewStateCollection.get(viewToOpen);
            plannerGlobals.currentViewStateModel.clear({ silent: true });
            plannerGlobals.currentViewStateModel.save(tmp.get("ViewState")); // Make a copy (we don't want the saved viewstate to be affected by changes to the current viewstate)

            this.done();
        },
        //onShareClicked: function (e) {
        //    e.stopPropagation();
        //    var clickedLi = $(e.currentTarget).closest("li");
        //    clickedLi.find("#sharing-settings-wrap").html("");
            
        //    // Roles select
        //    //var selEl = $("<select style='width: 240px; height: 10px; z-index: 10000000; visibility: hidden;' multiple='multiple'></select>");
        //    var selEl = clickedLi.find("#shared-with");
        //    _.each(window.plannerGlobals.roleDefs, function (name, id) {
        //        var optionEl = $("<option></option>")
        //            .attr("value", id)
        //            .text(name);
        //        selEl.append(optionEl);
        //    });
        //    //clickedLi.find("#sharing-settings-wrap").html(selEl);
        //    clickedLi.find("#sharing-settings-wrap select").multipleSelect({
        //        placeholder: "Not shared",
        //        selectAll: false,
        //        onClick: function () {
        //            //that.updateExcludedEntityTypesFromDropdown();
        //        }
        //    });
        //    //if (this.isValidConnectorSetting("EXCLUDED_ENTITY_TYPES")) {
        //    //    this.excludedEntityTypesWidget.multipleSelect("setSelects", JSON.parse(this.connectorSettingsCollection.get("EXCLUDED_ENTITY_TYPES").get("Value")));
        //    //}


        //    //var s = "";
        //    //_.each(window.plannerGlobals.roleDefs, function (name, id) {
        //    //    s += name + " ";
        //    //});

        //},
        onIcalLinkClicked: function (e) {
            var self = this;
            var id = $(e.currentTarget).closest("li").attr("data-id");
            self.name = $(e.currentTarget).closest("li")[0].children['saved-view-list-item-wrap'].children['view-name'].textContent;

            var currentViewForiCal = new Backbone.DeepModel();
            currentViewForiCal.urlRoot = "/api/planner/ical/" + id;

            currentViewForiCal.fetch({
                success: function (model, response) {

                    if (response == "") {
                        currentViewForiCal = new Backbone.DeepModel();
                        currentViewForiCal.urlRoot = "/api/planner/createicalurl/" + id;

                        currentViewForiCal.save(null, {
                            success: function(model, response) {
                                inRiverUtil.NotifyConfirm(self.name, response, function (){}, null);
                            },
                            error: function (model, errorMessage) {
                                inRiverUtil.OnErrors(model, errorMessage);
                            }
                        });

                        } else {
                        inRiverUtil.NotifyConfirm(self.name, response, function () { }, null);
                    }
                },
                error: function (model, errorMessage) {
                    inRiverUtil.OnErrors(model, errorMessage);
                }
            });

        },
        onRemoveClicked: function (e) {
            var that = this;
            e.stopPropagation();
            var idToRemove = parseInt($(e.currentTarget).closest("li").attr("data-id"));
            inRiverUtil.NotifyConfirm("Confirm delete", "Do you want to delete this view?", function () {
                plannerGlobals.viewStateCollection.get(idToRemove).destroy();
                that.render();
                that.updateUi();
                if (plannerGlobals.currentViewStateModel.get("Id") === idToRemove) { // Just for user feedback
                    plannerGlobals.markViewStateAsUnsaved();
                    plannerGlobals.currentViewStateModel.save();
                }
            });
        },
        onCancel: function () {
            this.done();
        },
        done: function () {
            appHelper.event_bus.trigger('closepopup');
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            Backbone.View.prototype.remove.call(this);
        },
        updateDataFromSharedDropdown: function(id, selection) {
            window.plannerGlobals.viewStateCollection.get(id).save({ "SharedWithRoles": selection });
        },
        updateClearSearchButtonVisibility: function() {
            this.$el.find("#filter-string-wrap i").css("visibility", this.$el.find("#filter-string").val().length == 0 ? "hidden" : "visible");
        },
        updateUi: function() {
            this.$el.find("#saved-views-tools-wrap a").removeClass("current");
            this.$el.find("#saved-views-tools-wrap a[href='" + plannerGlobals.currentSavedViewsTab + "']").addClass("current");
            this.$el.find("#filter-string").val(plannerGlobals.currentSavedViewsFilter);
            this.updateClearSearchButtonVisibility();
        },
        renderList: function () {
            var that = this;
            var modelsToShow;
            if (plannerGlobals.currentSavedViewsTab == "#my") {
                modelsToShow = window.plannerGlobals.viewStateCollection.where({ "OwnerUsername": window.appHelper.userName });
            } else if (plannerGlobals.currentSavedViewsTab == "#sharedwithme") {
                modelsToShow = window.plannerGlobals.viewStateCollection.filter(function (m) { return m.get("OwnerUsername") != window.appHelper.userName; });
            } else {
                modelsToShow = window.plannerGlobals.viewStateCollection.models;
            }

            if (plannerGlobals.currentSavedViewsFilter.length > 0) {
                var rx = new RegExp(plannerGlobals.currentSavedViewsFilter, "i");
                modelsToShow = _.filter(modelsToShow, function (m) {
                    if (rx.test(m.get("Name"))) return true;
                    return false;
                });
            }

            var ulEl = this.$el.find("#view-list");
            ulEl.empty();
            _.each(modelsToShow, function (m) {
                var liEl = $(that.listItemTemplate({ dataId: m.get("Id"), name: m.get("Name") }));

                if (m.get("CanModify") && appHelper.userCanSharePlannerViews) {
                    // Sharing dropdown
                    var selEl = liEl.find("#shared-with");
                    _.each(window.plannerGlobals.roleDefs, function (name, id) {
                        var optionEl = $("<option></option>")
                            .attr("value", id)
                            .text(name);
                        selEl.append(optionEl);
                    });
                    selEl.multipleSelect({
                        placeholder: "Not shared",
                        selectAll: true,
                        filter: true,
                        onClick: function (a, b, c) {
                            that.updateDataFromSharedDropdown(liEl.attr("data-id"), liEl.find("#shared-with").multipleSelect("getSelects"));
                        },
                        onCheckAll: function () {
                            that.updateDataFromSharedDropdown(liEl.attr("data-id"), liEl.find("#shared-with").multipleSelect("getSelects"));
                        },
                        onUncheckAll: function () {
                            that.updateDataFromSharedDropdown(liEl.attr("data-id"), liEl.find("#shared-with").multipleSelect("getSelects"));
                        }
                    });
                    if (m.get("SharedWithRoles")) {
                        selEl.multipleSelect("setSelects", m.get("SharedWithRoles"));
                    }
                    //liEl.find("#shared-with-wrap").css("display", "inline-block!important"); // hack...
                } else {
                    //liEl.find("#shared-with-wrap").css("display", "none!important"); // hack...
                    liEl.find("#shared-with").hide();
                    liEl.find("#shared-with-label").hide();
                }

                if (!m.get("CanModify")) {
                    liEl.find("#remove").hide();
                }

                ulEl.append(liEl);
            });
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());

            this.renderList();
        }
    });

    return plannerGanttSavedViewsSubView;
});

