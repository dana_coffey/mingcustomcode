define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'views/configuration/entityTypeFieldSelectionRow',
  'text!templates/configuration/entityTypeFieldSelectionTableTemplate.html'

], function ($, _, backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    entityTypeFieldSelectionRow, entityTypeFieldSelectionTableTemplate) {

    var entityTypeFieldSelectionTable = backbone.View.extend({
        template: _.template(entityTypeFieldSelectionTableTemplate),
        initialize: function() {
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {

        },
        hasUnsavedChanges: function() { // called from the router to prevent the user from losing changes
            var result = false;
            _.each(this.rows, function(row) {
                if (row.hasUnsavedChanges()) {
                    result = true;
                }
            });
            return result;
        },
        getSelections: function() {
            var selections = "";
            _.each(this.rows, function(row) {
                selections += row.getSelection();
            });
            return selections;
        },
        updateUi: function () {
            var self = this;
            _.each(this.rows, function (row) {
                var selectedFieldTypes = self.options.model.get(row.id);
                if (selectedFieldTypes || !self.options.defaultSelectAll) {
                    row.setSelection(selectedFieldTypes);
                } else {
                    row.selectAll(); // If no data was supplied for this entity type, then select all
                }
            });
        },
        selectAll: function() {
            _.each(this.rows, function (row) {
                row.selectAll();
            });
        },
        render: function () {
            this.$el.html(this.template());
            var root = this.$el.children(0);
            var rows = new Array();
            this.rows = rows;
            _.each(window.plannerGlobals.entityTypes.attributes, function (entityType, key) {
                var row = new entityTypeFieldSelectionRow({
                    entityType: entityType,
                    id: key
                });
                rows.push(row);
                root.append(row.$el.children(0));
            });
            this.updateUi();
            return this; // enable chained calls
        }
    });

    return entityTypeFieldSelectionTable;
});

