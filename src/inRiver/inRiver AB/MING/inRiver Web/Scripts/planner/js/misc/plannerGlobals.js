﻿define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'backgrid',
  'alertify',
  'datejs',
  'moment'

], function ($, _, Backbone, forms, Backgrid, alertify, datejs, moment) {

    window.plannerGlobals = {
        init: function() {
            // Load bootstrapped data
            window.plannerGlobals.currentViewStateModel = new Backbone.DeepModel(window.appData.currentViewState);
            window.plannerGlobals.currentViewStateModel.urlRoot = "/api/planner/currentviewstate";

            window.plannerGlobals.quickEditFieldTypesModel = new Backbone.DeepModel(window.appData.quickEditFieldTypes);
            window.plannerGlobals.quickEditFieldTypesModel.urlRoot = "/api/planner/config/quickeditfieldtypes";

            window.plannerGlobals.tooltipFieldTypesModel = new Backbone.DeepModel(window.appData.tooltipFieldTypes);
            window.plannerGlobals.tooltipFieldTypesModel.urlRoot = "/api/planner/config/tooltipfieldtypes";

            window.plannerGlobals.colorConfigModel = new Backbone.DeepModel(window.appData.colorConfig);
            window.plannerGlobals.colorConfigModel.urlRoot = "/api/planner/config/colors";

            window.plannerGlobals.entityTypes = new Backbone.DeepModel(window.appData.entityTypes);
            window.plannerGlobals.filterDefs = new Backbone.DeepModel(window.appData.filterDefs);
            window.plannerGlobals.barTitleDefs = new Backbone.DeepModel(window.appData.barTitleDefs);
            window.plannerGlobals.roleDefs = window.appData.roleDefs;

            window.plannerGlobals.viewStateCollection = new Backbone.Collection(window.appData.savedPlannerViewStates, { url: "/api/planner/viewstate", model: Backbone.DeepModel.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });

            window.plannerGlobals.ganttDataCollection = new Backbone.Collection(null, { url: "/api/planner/entity", model: Backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });
        },
        plannerCollectionIsDirty: false,
        fetchCollection: function (options) {
            var that = this;

            var params = {
                filterData: window.plannerGlobals.createJsonFilterData(),
                groupData: window.plannerGlobals.createJsonGroupData(),
                dimensions: JSON.stringify(plannerGlobals.currentViewStateModel.get("Dimensions")),
                barTitleFieldType: plannerGlobals.currentViewStateModel.get("BarTitleFieldType"),
            };
            
            if (moment(plannerGlobals.currentViewStateModel.get("FromDate")).isValid()) {
                params.FromDate = plannerGlobals.currentViewStateModel.get("FromDate");
            }
            if (moment(plannerGlobals.currentViewStateModel.get("ToDate")).isValid()) {
                params.ToDate = plannerGlobals.currentViewStateModel.get("ToDate");
            }

            var opts = { // defaults
                reset: true,
                data: $.param(params),
                success: function () {
                    $("#loading-spinner").hide();
                    $("#print").removeClass("print-disabled");
                    $("#apply-filters").hide();
                }
            }
            _.extend(opts, options);
            window.plannerGlobals.plannerCollectionIsDirty = false;

            // Delay the call a bit if more changes were made to the request...
            $("#loading-spinner").show();
            $("#print").addClass("print-disabled");
            if (this._timerId) {
                clearInterval(this._timerId);
            }
            this._timerId = setTimeout(function () {
                that._timerId = null;
                window.plannerGlobals.ganttDataCollection.fetch(opts);
            }, 500);
        },
        createJsonFilterData: function() {
            // Send filter settings as JSON in the request
            var filterData = {};
            _.each(this.currentViewStateModel.get("FilterValues"), function (v, k) {
                if (Object.keys(v).length > 0 /* && (_.contains(plannerGlobals.currentViewStateModel.get("Dimensions"), k) || k == appHelper.topLevelPlannerEntityType)*/) {
                    filterData[k] = {};
                    _.each(v, function (fieldTypeValues, fieldTypeId) {
                        if (fieldTypeValues.length > 0) {
                            filterData[k][fieldTypeId] = fieldTypeValues;
                        }
                    });
                }
            });
            return JSON.stringify(filterData);
        },
        areEntityTypesRelated: function(entityType1, entityType2) {
            return _.contains(plannerGlobals.entityTypes.get(entityType1).RelatedEntityTypes, entityType2);
        },
        getRelatedEntityTypeIds: function(entityType) {
            return Object.keys(plannerGlobals.entityTypes.get(entityType).RelatedEntityTypes);
        },
        createJsonGroupData: function () {
            // Send group settings as JSON in the request
            var groupData = [];
            if (this.currentViewStateModel.get("Groups." + plannerGlobals.currentViewStateModel.get("Dimensions")[0])) {
                groupData = this.currentViewStateModel.get("Groups." + plannerGlobals.currentViewStateModel.get("Dimensions")[0]);
            }
            return JSON.stringify(groupData);

            //var groupData = [];
            //if (this.currentViewStateModel.get("Groups.Campaign")) {
            //    //groupData = this.currentViewStateModel.get("Groups." + plannerGlobals.currentViewStateModel.get("Dimensions")[0]);
            //    groupData = this.currentViewStateModel.get("Groups.Campaign");
            //}
            //return JSON.stringify(groupData);
        },
        //weekScaleTemplate: function (date) {
        //    var dateToStr = gantt.date.date_to_str("%d %M");
        //    var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
        //    var dateToWeekStr = gantt.date.date_to_str("%W");
        //    return dateToStr(date) + " - " + dateToStr(endDate) + " (Week " + dateToWeekStr(date) + ")";
        //},
        setGanttTitle: function (title) {
            if (title && title.length > 0) {
                $("#gantt-view-name").html(title);
            } else {
                $("#gantt-view-name").html("(Unsaved View)");
            }
        },
        getGanttSortOrder: function () {
            var currentSortOrder = [];
            $(".gantt_grid_scale .gantt_grid_head_cell").each(function (ix, el) {
                var $el = $(el);
                if ($el.find(".gantt_asc").length > 0) {
                    currentSortOrder.push($el.attr("column_id") + ";asc");
                } else if ($el.find(".gantt_desc").length > 0) {
                    currentSortOrder.push($el.attr("column_id") + ";desc");
                }
            });

            return currentSortOrder.length > 0 ? currentSortOrder[0] : "";

            //if (!plannerGlobals.currentViewStateModel.get("SortSettings").byValueEquals(currentSortSettings)) {
            //    plannerGlobals.currentViewStateModel.save({ "SortSettings": currentSortSettings }, { silent: true });
            //}
        },
        ganttSortFn: function(v, a, b) {
            a = a[v];
            b = b[v];

            return a > b ? 1 : (a < b ? -1 : 0);
        },
        markViewStateAsUnsaved: function() {
            plannerGlobals.setGanttTitle("");
            plannerGlobals.currentViewStateModel.set({ "Name": null, "Id": null }, { silent: true });
        },
        //setSortSettings: function (sortSettings) {
        //    if (sortSettings && sortSettings.length > 0) {
        //        var tmp = sortSettings[0].split(";");
        //        gantt.sort(tmp[0], true);// tmp[1] == "desc");//, null, true);
        //    }
        //},
        setScaleConfig: function (value) {
            switch (value) {
                case "day":
                    gantt.config.min_column_width = 50;
                    gantt.config.scale_unit = "week";
                    gantt.config.date_scale = "Week %W";
                    gantt.config.step = 1;
                    gantt.config.subscales = [
                        { unit: "day", step: 1, date: "%j %M" }
                    ];
                    gantt.config.scale_height = 50;
                    gantt.config.round_dnd_dates = true;
                    gantt.templates.date_scale = null;
                    break;
                case "week":
                    gantt.config.min_column_width = 40;
                    gantt.config.scale_unit = "month";
                    gantt.config.date_scale = "%F, %Y";
                    gantt.config.step = 1;
                    gantt.config.subscales = [
                        { unit: "week", step: 1, date: "%W" }
                    ];
                    gantt.config.scale_height = 50;
                    gantt.templates.date_scale = null;
                    gantt.config.round_dnd_dates = false;
                    break;
                case "month":
                    gantt.config.min_column_width = 22;
                    gantt.config.scale_unit = "month";
                    gantt.config.date_scale = "%F, %Y";
                    gantt.config.subscales = [
                        { unit: "day", step: 1, date: "%j" }
                    ];
                    gantt.config.scale_height = 50;
                    gantt.templates.date_scale = null;
                    gantt.config.round_dnd_dates = false;
                    break;
                case "year":
                    gantt.config.min_column_width = 90;
                    gantt.config.scale_unit = "year";
                    gantt.config.step = 1;
                    gantt.config.date_scale = "%Y";

                    gantt.config.scale_height = 50;
                    gantt.templates.date_scale = null;

                    gantt.config.subscales = [
                        { unit: "month", step: 1, date: "%M" }
                    ];

                    gantt.config.round_dnd_dates = false;
                    break;
            }
        },
        ganttDateToUtcString: function (d) {
            return moment(d).format("YYYY-MM-DDTHH:mm"); // hack
        },
        utcStringToGanttDate: function (s) {
            return moment(s).toDate(); // Will assure we get the same result in all browsers
        },
        setIsStarred: function(model) {


            var id = model.id;

            var starred = model.get("IsStarred") ? "0":"1";
          
            var that = this;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    window.appHelper.event_bus.trigger('entityupdated', id);
                }
            }

            if (model.get("IsStarred")==true) {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + id, true);
                xmlhttp.send();
                model.set("IsStarred", false);

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + id, true);
                xmlhttp.send();
                model.set("IsStarred", true);
            }

        }

    };
    return plannerGlobals;
});

