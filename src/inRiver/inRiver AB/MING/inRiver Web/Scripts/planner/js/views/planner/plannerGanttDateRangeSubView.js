﻿define([
  'jquery',
  'underscore',
  'backbone',
  'datetimepicker',
  'moment',
  'text!templates/planner/plannerGanttDateRangeSubViewTemplate.html',
  
  ], function ($, _, Backbone, datetimepicker, moment, plannerGanttDateRangeSubViewTemplate) {

    var plannerGanttDateRangeSubView = Backbone.View.extend({
        template: _.template(plannerGanttDateRangeSubViewTemplate),
        initialize: function(options) {

            this.render();
        },
        events: {
            "change #gantt-date-range-picker-from": "onChangeDateRange",
            "change #gantt-date-range-picker-to": "onChangeDateRange",
            "change #fixed-dates-checkbox": "onClickFixedDates"
        },
        onChangeDateRange: function() {
            var from = this.$el.find("#gantt-date-range-picker-from").val();
            var to = this.$el.find("#gantt-date-range-picker-to").val();
            window.plannerGlobals.markViewStateAsUnsaved();
            window.plannerGlobals.currentViewStateModel.set({ "FromDate": from, "ToDate": to }, { silent: true });
            window.plannerGlobals.fetchCollection();
            window.plannerGlobals.currentViewStateModel.save();
            this.updateUi();            
        },
        onClickFixedDates: function() {
            if (this.$el.find("#fixed-dates-checkbox").prop("checked")) {
                this.$el.find("#gantt-date-range-picker-from").focus();
            } else {
                window.plannerGlobals.markViewStateAsUnsaved();
                window.plannerGlobals.currentViewStateModel.set({ "FromDate": null, "ToDate": null }, { silent: true });
                window.plannerGlobals.fetchCollection();
                window.plannerGlobals.currentViewStateModel.save();
                this.updateUi();
            }
        },
        onClose: function() {
            //var that = this;
            //_.each(_activeFilters, function(ctl) {
            //    that.destroyFilterCtl(ctl);
            //});
            //_.each(_activeGroups, function (ctl) {
            //    that.destroyGroupCtl(ctl);
            //});
        },
        updateUi: function() {
            var dateOnlyFormat = appHelper.momentDateFormat;
            var fromDate = window.plannerGlobals.currentViewStateModel.get("FromDate");
            if (fromDate) {
                this.$el.find("#gantt-date-range-picker-from").val(moment(fromDate).format(dateOnlyFormat));
                this.$el.find("#gantt-date-range-picker-to").datetimepicker({ defaultDate: moment(fromDate).format(dateOnlyFormat) });
            } else {
                this.$el.find("#gantt-date-range-picker-from").val("");
            }
            var toDate = window.plannerGlobals.currentViewStateModel.get("ToDate");
            if (toDate) {
                this.$el.find("#gantt-date-range-picker-to").val(moment(toDate).format(dateOnlyFormat));
                this.$el.find("#gantt-date-range-picker-from").datetimepicker({ defaultDate: moment(toDate).format(dateOnlyFormat) });
            } else {
                this.$el.find("#gantt-date-range-picker-to").val("");
            }
                                                                         
            if (this.$el.find("#gantt-date-range-picker-from").val() || this.$el.find("#gantt-date-range-picker-to").val()) {
                this.$el.find("#fixed-dates-checkbox").prop("checked", true);
            } else {
                this.$el.find("#fixed-dates-checkbox").prop("checked", false);
            }
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());

            this.$el.find("#gantt-date-range-picker-from").datetimepicker({
                format: appHelper.momentDateFormat,
                formatTime: appHelper.momentTimeFormat,
                formatDate: appHelper.momentDateFormat,
                lang: 'en',
                timepicker: false,
                weeks: appHelper.showWeeksInDatePicker,
                dayOfWeekStart: appHelper.firstDayOfWeek
            });

            this.$el.find("#gantt-date-range-picker-to").datetimepicker({
                format: appHelper.momentDateFormat,
                formatTime: appHelper.momentTimeFormat,
                formatDate: appHelper.momentDateFormat,
                lang: 'en',
                timepicker: false,
                weeks: appHelper.showWeeksInDatePicker,
                dayOfWeekStart: appHelper.firstDayOfWeek
            });

            this.updateUi();

            return this; // enable chained calls
        },
    });
    
    return plannerGanttDateRangeSubView;
});

