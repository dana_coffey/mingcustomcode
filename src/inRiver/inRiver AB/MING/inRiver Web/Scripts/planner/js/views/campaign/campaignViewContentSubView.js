﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'collections/entities/entityCollection',
  'views/entitycard/cardItemView',
  'text!templates/entityView/entityViewContentSubviewTemplate.html',
  'text!templates/entityCard/entityCardSectionViewTemplate.html'

], function ($, _, Backbone, inRiverUtil, modalPopup, entityCollection, cardItemView, entityViewContentSubviewTemplate, entityCardSectionViewTemplate) {

    var campaignViewContentSubView = Backbone.View.extend({
        //tagName: 'div',
        template: _.template(entityViewContentSubviewTemplate),
        cardSectionTemplate: _.template(entityCardSectionViewTemplate),
        initialize: function (options) {
            var that = this;
            this.isInitializing = true;
            this.renderLoadingSpinner();
            this.collection = new entityCollection();
            this.listenTo(this.collection, 'reset', this.render);
            this.collection.fetch({
                reset: true,
                url: '../../api/relation/'+options.campaignId, 
                data: $.param({ direction: "outbound" }),
                success: function () {
                    that.isInitializing = false;
                    that.render();
                },
                error: function () {
                    that.isInitializing = false;
                    that.render();
                }
            });
        },
        events: {
        },
        onClose: function () {
        },
        getIcon: function (entityType) {

            if (entityType.match(/product/i)) return "bag";
            if (entityType.match(/item/i)) return "tag";
            if (entityType.match(/look/i)) return "palette";
            if (entityType.match(/resource/i)) return "attach"; // docs

            return "archive";
        },
        render: function () {
            if (this.isInitializing) {
                return this;
            }
            if (this.collection.length == 0) {
                this.$el.html("<div class='no-campaigns-found'><span><i>There is no content for this item</i></span></div>");
                return this;
            }

            var that = this;
            this.$el.html(this.template({}));

//            var groups = this.collection.groupBy("EntityType");

            var groups = _.groupBy(this.collection.models, function (model) {

                return model.attributes["TargetEntity"].EntityType; 
            });


            var count = 0;
            var keys = _.keys(groups);
            _.each(keys, function (key) {
                if (key.match(/Activity/i)) return; // Don't show activities here

                count++;
                var sectionName = groups[key][0].attributes["TargetEntity"].EntityTypeDisplayName; 

                var sectionEl = $(that.cardSectionTemplate({ sectionName: sectionName, icon: that.getIcon(key) }));
                _.each(groups[key], function (model) {
                    var cardItemSubView = new cardItemView({
                        model: model,

                    });

                    sectionEl.find(".card-section-container").append(cardItemSubView.render().el);
                });

                that.$el.find("#cards-container").append(sectionEl);
            });
            if (count == 0) {
                this.$el.html("<div class='no-campaigns-found'><span><i>There is no content for this item</i></span></div>");
                return this;
            }
            return this; // enable chained calls
        },
    });


    return campaignViewContentSubView;
});

