﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'collections/entities/entityCollection',
  'text!templates/entityView/entityViewContentSubviewTemplate.html',
  'text!templates/entitycard/entityCardSectionViewTemplate.html',
   'views/entitycard/cardItemView'
], function ($, _, Backbone, inRiverUtil, entityCollection, entityViewContentSubviewTemplate, entityCardSectionViewTemplate, cardItemView) {

    var taskContentSubView = Backbone.View.extend({
        //tagName: 'div',
        template: _.template(entityViewContentSubviewTemplate),
        cardSectionTemplate: _.template(entityCardSectionViewTemplate),
        initialize: function (options) {
            this.collection = new entityCollection();
            this.listenTo(this.collection, 'reset', this.render);
            this.collection.fetch({
                reset: true,
                url: '../../api/relation/' + options.taskId,
                data: $.param({ direction: "outbound" }),
            });
        },
        events: {
        },
        onClose: function () {
        },
        getIcon: function (entityType) {

            if (entityType.match(/product/i)) return "bag";
            if (entityType.match(/item/i)) return "tag";
            if (entityType.match(/look/i)) return "palette";
            if (entityType.match(/resource/i)) return "attach"; // docs

            return "archive";
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({}));

            var groups = this.collection.groupBy("LinkTypeTargetDisplayName");
            var keys = _.keys(groups);
            _.each(keys, function (key) {

                var sectionEl = $(that.cardSectionTemplate({ sectionName: groups[key][0].attributes.TargetEntity.EntityTypeDisplayName, icon: that.getIcon(groups[key][0].attributes.TargetEntity.EntityType), data: groups[key][0].attributes.TargetEntity }));
                _.each(groups[key], function (model) {
                    var cardItemSubView = new cardItemView({
                        model: model 
                    });
                    sectionEl.find(".card-section-container").append(cardItemSubView.render().el);
                });

                that.$el.find("#cards-container").append(sectionEl);
            });

            return this; // enable chained calls
        },
    });

    return taskContentSubView;
});

