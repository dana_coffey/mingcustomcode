﻿define([
  'jquery',
  'underscore',
  'backbone',
    'multiple-select',
  'text!templates/planner/plannerGanttFiltersAndGroupsSubviewTemplate.html',
  'text!templates/planner/plannerGanttFilterCtlTemplate.html',
    'text!templates/planner/plannerGanttGroupCtlTemplate.html',
    'text!templates/planner/plannerGanttEntityFilterCtlTemplate.html'

], function ($, _, Backbone, multipleSelect, plannerGanttFiltersAndGroupsSubviewTemplate, 
    plannerGanttFilterCtlTemplate, plannerGanttGroupCtlTemplate, plannerGanttEntityFilterCtlTemplate) {

    var plannerGanttFiltersAndGroupsSubview = Backbone.View.extend({
        template: _.template(plannerGanttFiltersAndGroupsSubviewTemplate),
        initialize: function (options) {
            //this._activeFilters = [];
            //this._activeGroups = [];
            this.render();
        },
        events: {
            "click #add-filter": "onAddFilter",
            "click #add-group": "onAddGroup",
            "click #apply-filters": "onApplyFilters"
        },
        onFiltersChanged: function (m) {
            if (this.pendingFieldFilterChange === true ||
                this.pendingEntityFilterChange === true) {
                if (!this.$el.find("#apply-filters").is(":visible")) {
                    this.$el.find("#apply-filters").show();
                    this.$el.find("#apply-filters").css("opacity", 0);
                    this.$el.find("#apply-filters").fadeTo(500, 1);
                }
            }
        },
        onAddFilter: function() {
            this.createFilterCtl();
            this.options.parent.updateLayout(this.options.parent);
        },
        onAddGroup: function() {
            this.createGroupCtl();
        },
        onApplyFilters: function () {
            var that = this;
            window.plannerGlobals.markViewStateAsUnsaved();
            if (this.pendingFieldFilterChange === true) {
                // pendingEntityFilterChange will be handled here as well
                this.pendingFieldFilterChange = false;
                this.pendingEntityFilterChange = false;
                window.plannerGlobals.fetchCollection();
            } else if (this.pendingEntityFilterChange === true) {
                appHelper.event_bus.trigger('entityFilterChanged');
                this.pendingEntityFilterChange = false;
            }

            window.plannerGlobals.currentViewStateModel.save();
            that.$el.find("#apply-filters").fadeTo(500, 0.5);
            setTimeout(function() {
                that.$el.find("#apply-filters").hide();
            }, 1000);
        },
        createFilterCtl: function (activeField) {
            var options = { parent: this };
            if (!activeField) {
                // init the ctl with a list of fields to chose from
                var availableFilterEntityTypes = _.reject(plannerGlobals.currentViewStateModel.get("Dimensions").slice(), _.isNull);
                _.each(availableFilterEntityTypes, function(entityType) {
                    _.each(plannerGlobals.entityTypes.get(entityType).RelatedEntityTypes, function(x, relatedEntityTypeId) {
                        if (!_.contains(availableFilterEntityTypes, relatedEntityTypeId)) {
                            availableFilterEntityTypes.push(relatedEntityTypeId);
                        }
                    });
                });
                var availableFiltersForEntityTypes = {};
                _.each(availableFilterEntityTypes, function (entityType) {
                    if (entityType) { // might be null
                        availableFiltersForEntityTypes[entityType] = {};
                        _.each(plannerGlobals.filterDefs.get(entityType), function(v, fieldType) {
                            //if (v.Value == null) { // TODO: FIX!
                            availableFiltersForEntityTypes[entityType][fieldType] = v;
                            //}
                        });
                    }
                });
                options.data = availableFiltersForEntityTypes; //plannerGlobals.filterDefs.toJSON();
            } else {
                // active filter was supplied so init ctl with it right away
                options.activeField = activeField;
            }
            var tmp = new plannerFilterCtlSubView(options);
            //this._activeFilters.push(tmp);
            tmp.$el.insertBefore(this.$el.find("#add-filter"));
            this.updateUi();
        },
        createGroupCtl: function (activeField) {
            var options = { parent: this };
            if (!activeField) {
                // init the ctl with a list of fields to chose from
                /*var availableFilterEntityTypes = _.reject(plannerGlobals.currentViewStateModel.get("Dimensions").slice(), _.isNull);
                _.each(availableFilterEntityTypes, function (entityType) {
                    _.each(plannerGlobals.entityTypes.get(entityType).RelatedEntityTypes, function (x, relatedEntityTypeId) {
                        if (!_.contains(availableFilterEntityTypes, relatedEntityTypeId)) {
                            availableFilterEntityTypes.push(relatedEntityTypeId);
                        }
                    });
                });
                var availableFiltersForEntityTypes = {};
                _.each(availableFilterEntityTypes, function (entityType) {
                    if (entityType) { // might be null
                        availableFiltersForEntityTypes[entityType] = {};
                        _.each(plannerGlobals.filterDefs.get(entityType), function (v, fieldType) {
                            //if (v.Value == null) { // TODO: FIX!
                            availableFiltersForEntityTypes[entityType][fieldType] = v;
                            //}
                        });
                    }
                });
                options.data = availableFiltersForEntityTypes; //plannerGlobals.filterDefs.toJSON();*/

                
                // init the ctl with a list of fields to chose from
                var availableGroupingsForEntityTypes = {};
                //_.each(plannerGlobals.currentViewStateModel.get("Dimensions"), function (entityType) {
                var entityType = plannerGlobals.currentViewStateModel.get("Dimensions")[0];
                    availableGroupingsForEntityTypes[entityType] = {};
                    _.each(plannerGlobals.filterDefs.get(entityType), function (v, fieldType) {
                        //if (v.Value == null) { // TODO: FIX!
                        availableGroupingsForEntityTypes[entityType][fieldType] = v;
                        //}
                    });
                //});
                    options.data = availableGroupingsForEntityTypes;
            } else {
                // active filter was supplied so init ctl with it right away
                options.activeField = activeField;
            }
            var tmp = new plannerGroupCtlSubView(options);
            //this._activeGroups.push(tmp);
            tmp.$el.insertBefore(this.$el.find("#add-group"));
            this.updateUi();
        },
        createEntityFilterCtl: function() {
            var options = { parent: this };
            if (this._entityFilterCtl) {
                this._entityFilterCtl.close();
            }
            this._entityFilterCtl = new plannerEntityFilterCtlSubView(options);
            this._entityFilterCtl.$el.insertBefore(this.$el.find("#add-filter"));
        },
        destroyFilterCtl: function(ctl) {
            ctl.close();
            //this._activeFilters.pop(ctl);
            this.options.parent.updateLayout(this.options.parent);
            this.updateUi();
        },
        destroyGroupCtl: function (ctl) {
            ctl.close();
            //this._activeGroups.pop(ctl);
            this.options.parent.updateLayout(this.options.parent);
            this.updateUi();
        },
        onClose: function () {
            this._entityFilterCtl.close();
            //var that = this;
            //_.each(_activeFilters, function(ctl) {
            //    that.destroyFilterCtl(ctl);
            //});
            //_.each(_activeGroups, function (ctl) {
            //    that.destroyGroupCtl(ctl);
            //});
        },
        updateUi: function() {
            if (this.$el.find("#active-groups-wrap li").length >= 3) {
                this.$el.find("#add-group").hide();
            } else {
                this.$el.find("#add-group").show();
            }
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());
            //this._activeFilters = [];
            //this._activeGroups = [];

            this.$el.find("#active-filters-wrap").append("<button id='add-filter'>+Add Filter</button>");
            this.$el.find("#active-filters-wrap").append("<button id='apply-filters' style='display: none;'>Apply Filters</button>");
            this.$el.find("#active-groups-wrap").append("<button id='add-group'>+Add Group</button>");

            this.createEntityFilterCtl();

            // Only show filters that apply to current dimensions
            var tmp = window.plannerGlobals.currentViewStateModel.get("Dimensions").slice();
            _.each(tmp, function(entityType) { if (entityType) tmp = tmp.concat(Object.keys(plannerGlobals.entityTypes.get(entityType).RelatedEntityTypes)); });
            var filterableEntityTypes = _.uniq(tmp);
            _.each(filterableEntityTypes, function (entityTypeId) {
                _.each(window.plannerGlobals.currentViewStateModel.get("FilterValues." + entityTypeId), function (fieldType, fieldTypeId) {
                    that.createFilterCtl(entityTypeId + "." + fieldTypeId);
                });
            });

            // Only show groups that apply to current dimension
            var entityTypeId = window.plannerGlobals.currentViewStateModel.get("Dimensions")[0];
            _.each(window.plannerGlobals.currentViewStateModel.get("Groups." + entityTypeId), function (fieldType) {
                that.createGroupCtl(entityTypeId + "." + fieldType);
            });

            return this; // enable chained calls
        },
    });

    var plannerFilterCtlSubView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(plannerGanttFilterCtlTemplate),
        initialize: function (options) {
            if (options.activeField) {
                this.activeField = options.activeField;
            }
            this.render();
        },
        events: {
            "change #possible-filter-fields": "onFieldSelect",
            "click #remove-filter-button": "onRemoveClick"
        },
        onChange: function() {
        },
        onFieldSelect: function () {
            var that = this;

            this.activeField = this.$el.find("#possible-filter-fields").val();
            window.plannerGlobals.markViewStateAsUnsaved();
            window.plannerGlobals.currentViewStateModel.set("FilterValues." + that.activeField, [], { silent: true }); // indicates we have an active field with no selection
            //window.plannerGlobals.currentViewStateModel.save();

            // Update ui
            this.$el.find("#possible-filter-fields").fadeOut(200, function () {
                that.$el.find("#active-filter-field").fadeIn(function() {
                    that.$el.find("#active-filter-value").show();
                    that.switchToActiveMode();
                });
            });
        },
        selectionChanged: function (that) {
            var selection = that.$el.find("#active-filter-value").multipleSelect('getSelects');
            // Change the state silently to not mess up the filters UI
            window.plannerGlobals.currentViewStateModel.set("FilterValues." + that.activeField, selection, { silent: true });
            this.options.parent.pendingFieldFilterChange = true;
            this.options.parent.onFiltersChanged();
        },
        switchToActiveMode: function () {
            var that = this;

            if (this.activeField) {
                var activeFilterDefEntityName = window.plannerGlobals.entityTypes.get(this.activeField.split(".")[0]).Name;
                var activeFilterDef = window.plannerGlobals.filterDefs.get(this.activeField);
                this.$el.find("#active-filter-field").html(activeFilterDefEntityName + ": " + activeFilterDef.DisplayName);

                // Create multi select dropdown
                var valueDropdownEl = this.$el.find("#active-filter-value");
                _.each(activeFilterDef.PossibleValues, function(text, value) {
                    var optionEl = $("<option></option>")
                        .attr("value", value)
                        .text(text);
                    valueDropdownEl.append(optionEl);
                });
                valueDropdownEl.multipleSelect({
                    placeholder: "No selection",
                    selectAll: true,
                    filter: that.$el.find("#active-filter-value option").length > 10 ? true : false,
                    onClick: function() {
                        that.selectionChanged(that);
                    },
                    onCheckAll: function () {
                        that.selectionChanged(that);
                    },
                    onUncheckAll: function () {
                        that.selectionChanged(that);
                    },
                });
                valueDropdownEl.multipleSelect('setSelects', window.plannerGlobals.currentViewStateModel.get("FilterValues." + this.activeField));
            } 
        },
        onRemoveClick: function () {
            if (this.activeField) {
                window.plannerGlobals.currentViewStateModel.unset("FilterValues." + this.activeField, { silent: true });
                this.options.parent.pendingFieldFilterChange = true;
                this.options.parent.onFiltersChanged();
            }
            this.options.parent.destroyFilterCtl(this);
        },
        onClose: function () {

        },
        render: function () {
            this.$el.html(this.template());

            if (!this.activeField) {
                this.$el.find("#active-filter-value").hide();
                this.$el.find("#active-filter-field").hide();

                var fieldDropdownEl = this.$el.find("#possible-filter-fields");
                fieldDropdownEl.append($("<option></option>").attr("value", "-1").text("Select Filter Type"));
                _.each(this.options.data, function (entityType, entityTypeId) {
                    if (Object.keys(entityType).length > 0) {
                        fieldDropdownEl.append("<optgroup label='--- " + plannerGlobals.entityTypes.get(entityTypeId + ".Name") + " ---'>");
                        _.each(entityType, function (v, fieldTypeId) {
                            if (plannerGlobals.currentViewStateModel.get("FilterValues." + entityTypeId + "." + fieldTypeId)) {
                                // Filter is already active so exclude from list
                                return;
                            }
                            var optionEl = $("<option></option>")
                                .attr("value", entityTypeId + "." + fieldTypeId)
                                .text(v.DisplayName);
                            fieldDropdownEl.append(optionEl);
                        });
                    }
                });
            } else {
                this.$el.find("#possible-filter-fields").hide();
                this.switchToActiveMode();
            }

            return this; // enable chained calls
        },
    });

    var plannerGroupCtlSubView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(plannerGanttGroupCtlTemplate),
        initialize: function (options) {
            if (options.activeField) {
                this.activeField = options.activeField;
            }
            this.render();
        },
        events: {
            "change #possible-group-fields": "onFieldSelect",
            "click #remove-group-button": "onRemoveClick"
        },
        onChange: function () {
        },
        onFieldSelect: function () {
            var that = this;

            this.activeField = this.$el.find("#possible-group-fields").val();
            var activeEntityType = this.activeField.split(".")[0];
            if (!window.plannerGlobals.currentViewStateModel.get("Groups." + activeEntityType)) {
                window.plannerGlobals.currentViewStateModel.set("Groups." + activeEntityType, [], { silent: true });
            }
            var groupsArr = window.plannerGlobals.currentViewStateModel.get("Groups." + activeEntityType).slice(); // Create copy of array, otherwiser set() won't detect a change in the array
            groupsArr.push(this.activeField.split(".")[1]);
            window.plannerGlobals.markViewStateAsUnsaved();
            window.plannerGlobals.currentViewStateModel.set("Groups." + activeEntityType, groupsArr);
            window.plannerGlobals.currentViewStateModel.save({ wait: true });
          
            // Update ui
            this.$el.find("#possible-group-fields").fadeOut(200, function () {
                that.$el.find("#active-group-field").show();
                that.switchToActiveMode();
            });
        },
        switchToActiveMode: function () {
            var that = this;
            var activeFilterDef = window.plannerGlobals.filterDefs.get(this.activeField);
            this.$el.find("#active-group-field").html(activeFilterDef.DisplayName);
        },
        onRemoveClick: function () {
            if (this.activeField) {
                var fieldToRemove = this.activeField.split(".")[1];
                var tmp = window.plannerGlobals.currentViewStateModel.get("Groups." + this.activeField.split(".")[0]);
                tmp = _.reject(tmp, function (v) { return v == fieldToRemove; });
                window.plannerGlobals.markViewStateAsUnsaved();
                window.plannerGlobals.currentViewStateModel.set("Groups." + this.activeField.split(".")[0], tmp);
                window.plannerGlobals.currentViewStateModel.save();
            }
            this.options.parent.destroyGroupCtl(this);
        },
        onClose: function () {

        },
        render: function () {
            this.$el.html(this.template());

            if (!this.activeField) {
                //this.$el.find("#active-filter-value").hide();
                this.$el.find("#active-group-field").hide();

                var fieldDropdownEl = this.$el.find("#possible-group-fields");
                fieldDropdownEl.append($("<option></option>").attr("value", "-1").text("Select Group Type"));
                _.each(this.options.data, function (entityType, entityTypeId) {
                    //var optionEl = $("<option></option>")
                    //    .attr("value", fieldTypeId)
                    //    .text(fieldType);
                    //fieldDropdownEl.append(optionEl);
                    if (Object.keys(entityType).length > 0) {
                        //fieldDropdownEl.append("<optgroup label='--- " + plannerGlobals.entityTypes.get(entityTypeId + ".Name") + " ---'>");
                        _.each(entityType, function (v, fieldTypeId) {
                            if (_.contains(plannerGlobals.currentViewStateModel.get("Groups." + entityTypeId), fieldTypeId)) {
                                // Field is already active so exclude from list
                                return;
                            }
                            var optionEl = $("<option></option>")
                                .attr("value", entityTypeId + "." + fieldTypeId)
                                .text(v.DisplayName);
                            fieldDropdownEl.append(optionEl);
                        });
                    }
                });
            } else {
                this.$el.find("#possible-group-fields").hide();
                this.switchToActiveMode();
            }

            return this; // enable chained calls
        },
    });

    
    var plannerEntityFilterCtlSubView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(plannerGanttEntityFilterCtlTemplate),
        initialize: function (options) {
            this.render();
            this.listenTo(plannerGlobals.ganttDataCollection, "sync", this.populateSelect);         
        },
        events: {

        },
        onClose: function () {

        },
        populateSelect: function() {
            var valueDropdownEl = this.$el.find("#entity-filter-select");
            valueDropdownEl.empty();
            plannerGlobals.ganttDataCollection.each(function (model) {
                if (model.get("EntityId") && model.get("EntityType") == plannerGlobals.currentViewStateModel.get("Dimensions.0")) {
                    var optionEl = $("<option></option>")
                        .attr("value", model.get("EntityId"))
                        .text(model.get("DisplayName"));
                    valueDropdownEl.append(optionEl);
                }
            });
            valueDropdownEl.multipleSelect("refresh");
            if (!window.plannerGlobals.currentViewStateModel.get("EntityFilter")) {
                valueDropdownEl.multipleSelect("setSelects", plannerGlobals.ganttDataCollection.pluck("EntityId"));
            } else {
                valueDropdownEl.multipleSelect("setSelects", window.plannerGlobals.currentViewStateModel.get("EntityFilter"));
            }
        },
        selectionChanged: function (that) {
            window.plannerGlobals.markViewStateAsUnsaved();
            var selection = that.$el.find("#entity-filter-select").multipleSelect('getSelects');
            if (selection.length == that.$el.find("#entity-filter-select option").length) {
                window.plannerGlobals.currentViewStateModel.set("EntityFilter", null, { silent: true });
            } else {
                window.plannerGlobals.currentViewStateModel.set("EntityFilter", _.map(selection, function (x) { return parseInt(x); }), { silent: true });
            }
            that.options.parent.pendingEntityFilterChange = true;
            that.options.parent.onFiltersChanged();
            //window.plannerGlobals.currentViewStateModel.save();
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());

            // Create multi select dropdown
            var valueDropdownEl = this.$el.find("#entity-filter-select");
            valueDropdownEl.multipleSelect({
                placeholder: "0",
                selectAll: true,
                allSelected: "All",
                minimumCountSelected: 0,
                countSelected: '# of %',
                width: 200,
                filter: true,
                onClick: function (e) {
                    that.selectionChanged(that);
                },
                onCheckAll: function () {
                    that.selectionChanged(that);
                },
                onUncheckAll: function () {
                    that.selectionChanged(that);
                }
            });
            this.populateSelect();
            //valueDropdownEl.multipleSelect('setSelects', window.plannerGlobals.currentViewStateModel.get("EntityFilter"));
      
   
            return this; // enable chained calls
        },
    });

    return plannerGanttFiltersAndGroupsSubview;
});

