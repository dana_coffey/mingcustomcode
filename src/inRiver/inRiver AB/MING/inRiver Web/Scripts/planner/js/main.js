// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    jquery: '../../libs/jquery/jquery-2.0.3.min',
    underscore: '../../libs/underscore/underscore',
    backbone: '../../libs/backbone/backbone-min',
    templates: '../templates',
    sharedtemplates: '../../shared/templates',
    sharedjs: '../../shared/js',
    'backbone-forms': '../../libs/backbone-forms/backbone-forms.amd',
    'backbone-modal': '../../libs/backbone-modal/backbone.modal',
    'jquery-ui': '../../libs/jquery-ui/jquery-ui.min',
    'jquery-ui-touch-punch': '../../libs/jquery.ui.touch-punch/jquery.ui.touch-punch',
    'inriver-imagemap': '../../libs/inriver.imagemap/inriver.imagemap',
    'alertify': '../../libs/alertify/alertify',
    'colpick': '../../libs/colpick/colpick',
    'backgrid': '../../libs/backgrid/backgrid-mod',
    'text': '../../libs/text/text',
    'jstree': '../../libs/jstree/jstree.min',
    'deep-model': '../../libs/deep-model/deep-model',
    'moment': '../../libs/moment/moment.min',
    'datetimepicker': '../../libs/datetimepicker/jquery.datetimepicker',
    'dropzone': '../../libs/dropzone/dropzone-amd-module',
    'modalPopup': '../../shared/js/views/modalpopup/modalPopup',
    'multiple-select': '../../libs/multiple-select/multiple-select',
    'globalize': '../../libs/devextreme/globalize.min',
    'devextreme': '../../libs/devextreme/dx.all',
    'datejs': '../../libs/date/date',
    'dhtmlxgantt': 'dhtmlxgantt/dhtmlxgantt',
    'dhtmlxgantt_marker': 'dhtmlxgantt/ext/dhtmlxgantt_marker',
    'dhtmlxgantt_tooltip': 'dhtmlxgantt/ext/dhtmlxgantt_tooltip',
    'dhtmlxgantt_smart_rendering': 'dhtmlxgantt/ext/dhtmlxgantt_smart_rendering',
    'jquery-steps': '../../libs/jquery.steps/jquery.steps.min',
    'jquery-filedownload': '../../libs/filedownload/jquery.fileDownload',
    'jquery-mousewheel': '../../libs/jquery.mousewheel/jquery.mousewheel',
  },
  shim: {
      underscore: { exports: '_' },
      backbone: { deps: ['underscore', 'jquery'], exports: 'Backbone' },
      'backbone-modal': { deps: ['jquery', 'backbone'], exports: 'Modal' },
      'backbone-forms': { deps: ['jquery', 'backbone'], exports: 'backbone-forms' },
      'backgrid': { deps: ['jquery', 'backbone'], exports: 'Backgrid' },
      'deep-model': { deps: ['underscore', 'jquery'], exports: 'deep-model' },
      'jstree': { deps: ['jquery'], exports: 'jstree' },
      'datetimepicker': { deps: ['underscore', 'jquery'], exports: 'datetimepicker' },
      'dateTimeEditor': { deps: ['backbone-forms'], exports: 'dateTimeEditor' },
      'localeStringEditor': { deps: ['backbone-forms'], exports: 'localeStringEditor' },
      'devextreme': { deps: ['globalize'], exports: 'devextreme' },
      'dhtmlxgantt_marker': { deps: ['dhtmlxgantt'] },
      'dhtmlxgantt_tooltip': { deps: ['dhtmlxgantt'] },
      'dhtmlxgantt_smart_rendering': { deps: ['dhtmlxgantt'] },
      'jquery-filedownload': { deps: ['jquery'] },
      'jquery-ui': { deps: ['jquery'] },
      'jquery-ui-touch-punch': { deps: ['jquery-ui'] },
  }
});

require([
  // Load our app module and pass it to our definition function
  'app', 'backbone-forms'

], function (App) {
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    App.initialize();
});
