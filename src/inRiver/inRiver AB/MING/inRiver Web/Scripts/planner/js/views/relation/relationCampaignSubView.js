﻿define([
    'jquery',
    'underscore',
    'backbone',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/misc/permissionUtil',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/collections/relations/relationCollection',
    'sharedjs/collections/relations/relationTypeCollection',
    'views/relation/relationCardView',
    'sharedjs/views/contextmenu/contextMenuView',
    'sharedjs/views/entityview/entityDetailSettingWrapperView',
    'text!templates/campaignSubEntity/campaignSubContentTemplate.html',
    'text!sharedtemplates/relation/relationTypeLinkEntityGroupTemplate.html'
], function ($, _, backbone, modalPopup, inRiverUtil, permissionUtil, relationModel, entityModel, relationCollection, relationTypeCollection, relationCardView, contextMenuView, entityDetailSettingWrapperView, campaignSubContentTemplate, relationTypeLinkEntityGroupTemplate) {
    var relationCampaignSubView = backbone.View.extend({
        template: _.template(campaignSubContentTemplate),
        initialize: function (options) {

            var self = this;

            this.model = options.model;
            this.relationTypes = options.relationTypes;

            this.relations = new relationCollection([], { entityId: this.model.id, direction: "outbound" });

            self.relations.fetch({
                success: function () {
                    self.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.droppable = permissionUtil.CheckPermission("AddLink");
            this.sortable = permissionUtil.CheckPermission("UpdateLink");

            this.$el.html("<div id=\"loading\"><img src=\"/Images/nine-squares-32x32.gif\" />   <div><h2>Loading data</h2></div></div>");
            this.listenTo(window.appHelper.event_bus, 'entityupdated', this.onEntityUpdated);
            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.onEntityDeleted);
            this.listenTo(window.appHelper.event_bus, 'relationremoved', this.onRelationRemoved);
        },
        events: {
            "click .relation-wrap": "onCardClick",
            "click #edit-link-button": "onEditLink",
            "click #addAndLink": "onAddAndLinkEntity",
            "click .add-entity-withentity-button": "onAddAndLinkEntityWithLinkEntity",
            "click #relation-completeness-text": "onShowCompletnessDetails",
            "click #open-in-workarea": "onOpenWorkarea",
            "click #card-view": "onToggleView",
            "click #table-view": "onToggleView",
            "click #include-fields": "onIncludeFields"
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = e.currentTarget.find("#card-tools-container");
            $box.toggle(200);
        },
        onOpenWorkarea: function (e) {
            e.stopPropagation();
            var entities = $(e.currentTarget).closest(".relation-section-wrap").find(".card-wrap").map(function () {
                return this.id;
            }).get().join(",");
            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;
            $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                backbone.history.loadUrl("workareasearch?title=" + relationType + "&compressedEntities=" + compressed);
                if ($("#workarea-header-label").hasClass("workarea-header-label-vertical")) {
                    $("#workarea-header-toggle-button").click();
                }
            });
        },
        onAddAndLinkEntity: function (e) {
            e.stopPropagation();

            var entitytype = $(e.currentTarget).closest(".relation-section-wrap")[0].attributes["entitytypeid"];
            if (entitytype == null) {
                entitytype = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].attributes["entitytypeid"];
            }
            if (entitytype == null) {
                return;
            }
            var entitytypeid = entitytype.value;

            var relationType = $(e.target).closest(".relation-section-wrap")[0].id;

            var linkentitytypeid = "";
            var linkentitytypeidObj = $(e.target).closest(".relation-section-wrap")[0].attributes["linkentitytypeid"];
            if (linkentitytypeidObj) {

                linkentitytypeid = linkentitytypeidObj.value;
            }

            var linkentityid = "";

            if ($(event.target).closest(".relation-sub-section-wrap").length != 0) {
                linkentityid = $(event.target).closest(".relation-sub-section-wrap")[0].id;
            }


            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = this.model.id;
            this.targetIdToCreate = -1;

            this.entityTypeIdToCreate = entitytypeid;
            this.linkentitytypeidToCreate = linkentitytypeid;
            this.linkentityidToCreate = linkentityid;

            this.stopListening(window.appHelper.event_bus, 'entitycreated');
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeAddAndLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: entitytypeid,
                showInWorkareaOnSave: false
            });
        },
        completeAddAndLinkEntity: function (model) {
            var self = this;

            this.stopListening(window.appHelper.event_bus, 'entitycreated');
            this.targetIdToCreate = model.attributes["Id"];

            if (this.linkentitytypeidToCreate && this.linkentityidToCreate == "") {
                this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeLinkCreatation);

                entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                    id: 0,
                    entityTypeId: this.linkentitytypeidToCreate,
                    showInWorkareaOnSave: false
                });
            } else {

                var newModel = new relationModel({ LinkType: this.relationTypeToCreate, SourceId: this.sourceIdToCreate, TargetId: this.targetIdToCreate, LinkEntityId: this.linkentityidToCreate });
                newModel.save([], {
                    success: function (result) {
                        self.relations.add(result);

                        var relationCard = new relationCardView({ relation: result, direction: "outbound" });
                        var findCriteria = "#" + (self.linkentityidToCreate ? self.linkentityidToCreate + " #relation-sub-section-container" : self.relationTypeToCreate + " #relation-section-container");
                        self.$el.find(findCriteria).append(relationCard.$el);

                        window.plannerGlobals.ganttDataCollection.reset();
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onAddAndLinkEntityWithLinkEntity: function (e) {
            e.stopPropagation();

            var entitytypeid = e.currentTarget.id.replace("EntityType-", "");

            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;

            var linkentityid = $(event.target).closest(".relation-sub-section-wrap")[0].id;

            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = this.id;
            this.targetIdToCreate = -1;
            this.entityTypeIdToCreate = entitytypeid;
            this.linkentityidToCreate = linkentityid;

            this.stopListening(window.appHelper.event_bus, 'entitycreated');
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeAddAndLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: entitytypeid,
                showInWorkareaOnSave: false
            });
        },
        onEditLink: function (e) {
            e.stopPropagation();
            var linkEntityId = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].id;
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: linkEntityId
            });
        },
        onCardClick: function (e) {
            var id = e.currentTarget.id;
            this.goTo("campaign/" + id);
        },
        onClose: function () {
        },
        onChangeSortOrder: function (event, ui) {
            var id = $(ui.item[0]).find(".card-wrap")[0].id;
            var position = 0;
            var temp = $(ui.item[0]).find("#contextmenu > div");
            var relationId = temp[0].id;

            var parent = $(this).closest("div#relation-section-container");

            parent.find(".card-wrap").each(function (i) {
                if ($(this).attr('id') == id) {
                    position = i;
                    if (position < 0) {
                        position = 0;
                    }
                }
            });

            // API and save order
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "/api/tools/updaterelationsortorder/" + relationId + "/" + position, true);
            xmlhttp.send();

            //window.appHelper.event_bus.trigger('entityupdated', parent.data("entityId"));
            window.plannerGlobals.ganttDataCollection.reset(); // force reload of gantt data
        },
        completeLinkCreatation: function (model) {
            var that = this;

            var linkentityid = model.attributes.Id;
            var linktype = this.relationTypeToCreate;
            var direction = "outbound";

            var newModel = new relationModel({ LinkType: this.relationTypeToCreate, SourceId: this.sourceIdToCreate, TargetId: this.targetIdToCreate, LinkEntityId: linkentityid });
            newModel.save([], {
                success: function (xmodel) {
                    that.relations.add(xmodel);
                    var entitytype = xmodel.attributes.TargetEntity.EntityType;
                    if (direction == "inbound") {
                        entitytype = xmodel.attributes.SourceEntity.EntityType;
                    }

                    var subsectionEl = $(_.template(relationTypeLinkEntityGroupTemplate, { model: xmodel.toJSON(), entitytypeid: entitytype }));
                    var tools = new Array();
                    tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                    that.pushAddAndLinkTool(tools);

                    subsectionEl.find("#contextmenu").html(new contextMenuView({
                        id: xmodel.id,
                        tools: tools
                    }).$el);


                    $("#" + linktype + " #relation-section-container").append(subsectionEl);

                    var id = "#" + linkentityid.toString().replace(" ", "-");
                    var $heading = $(id + " .relation-sub-section-title");
                    var $box = $(id).find("#relation-sub-section-container");

                    var $showTask = $(id + '-show i');

                    $heading.click(function () {
                        $box.toggle(200);
                        $showTask.toggle();
                    });

                    var relationCard = new relationCardView({ relation: xmodel, direction: direction });
                    $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                },
                error: function () {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        completeMultipleLinkCreatation: function (model) {
            var that = this;

            var direction = "outbound";

            if (this.LinksCollection.length > 0) {
                _.each(this.LinksCollection.models, function (link) {

                    var linkentityid = model.attributes.Id;
                    var linktype = link.relationTypeToCreate;

                    var newModel = new relationModel({ LinkType: link.relationTypeToCreate, SourceId: link.sourceIdToCreate, TargetId: link.targetIdToCreate, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (xmodel) {

                            var entitytype = xmodel.attributes.TargetEntity.EntityType;
                            if (direction == "inbound") {
                                entitytype = xmodel.attributes.SourceEntity.EntityType;
                            }

                            var subsectionEl = $(_.template(relationTypeLinkEntityGroupTemplate, { model: xmodel.toJSON(), entitytypeid: entitytype }));

                            if (that.$el.find("#" + linkentityid + " #relation-sub-section-container").length == 0) {
                                var tools = new Array();
                                tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                                that.pushAddAndLinkTool(tools);

                                subsectionEl.find("#contextmenu").html(new contextMenuView({
                                    id: xmodel.id,
                                    tools: tools
                                }).$el);

                                that.$el.find("#" + linktype + " #relation-section-container").append(subsectionEl);

                                var idx = "#" + linkentityid.toString().replace(" ", "-");
                                var $heading = $(idx + " .relation-sub-section-title");
                                var $box = $(idx).find("#relation-sub-section-container");

                                var $showTask = $(idx + '-show i');

                                $heading.click(function () {
                                    $box.toggle(200);
                                    $showTask.toggle();
                                });
                                var id;

                                if (direction == "outbound") {
                                    id = xmodel.attributes["TargetEntityTypeId"];
                                } else {
                                    id = xmodel.attributes["SourceEntityTypeId"];
                                }
                                if (that.droppable) {
                                    subsectionEl.find(idx).droppable({
                                        greedy: true,
                                        accept: function (draggable) {

                                            var typeObj = $(draggable).closest(".card-section-wrap");
                                            var linktype = "";
                                            if (typeObj[0]) {
                                                linktype = typeObj[0].id;
                                            }
                                            if (linktype != id) {
                                                return false;
                                            }

                                            var drager = draggable[0].id.replace("card-", "");
                                            //kolla om dragger finns.
                                            if (drager == null || drager == "") {
                                                return false;
                                            }

                                            var x = $(this).find("#" + drager);
                                            if (x.length > 0) {
                                                return false;
                                            }
                                            return true;
                                        },
                                        activeClass: "relation-sub-section-container-active",
                                        hoverClass: "relation-dropable-hover",
                                        drop:
                                            function (event, ui) {
                                                that.addLinkedRelation(event, ui, that, linktype);
                                            }
                                    });
                                }
                            }

                            var relationCard = new relationCardView({ relation: xmodel, direction: direction });
                            that.$el.find("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                        },
                        error: function () {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });

                this.LinksCollection.reset();
            }
        },
        createLineEntityAndAddRelation: function (relationType, sourceId, targetId, entitytypeid, that) {
            that.relationTypeToCreate = relationType;
            that.sourceIdToCreate = sourceId;
            that.targetIdToCreate = targetId;

            that.entityTypeIdToCreate = entitytypeid;
            that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeLinkCreatation);


            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: entitytypeid,
                showInWorkareaOnSave: false
            });

        },
        addRelation: function (event, ui, self, entitytype) {
            event.stopPropagation();

            var linkentitytypeid = "";
            if (event.target.attributes["linkentitytypeid"]) {
                linkentitytypeid = event.target.attributes["linkentitytypeid"];
            };

            var that = this;
            var addedToEntity = that.model.id;
            var relationType = event.target.id;
            var tableView = entitytype == null;

            var entitys = [];

            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {

                var workareaView = window.appHelper.workAreaHandler.getActiveWorkarea();

                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                if (!workareaView) {
                    return;
                }

                var cards = workareaView.getSelectedEntityIds();

                if (cards.length > 0) {
                    _.each(cards, function (card) {
                        var cardView = _.find(workareaView.cards, function (c) {
                            return c.card.model.attributes.Id == card;
                        });

                        entitys.push({ id: card, entitytype: cardView.card.model.attributes.EntityType });

                    });
                } else {
                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var entities = grid.getSelectedRowKeys();
                        _.each(entities, function (entity) {
                            entitys.push({ id: entity.Id, entitytype: entity.EntityType });
                        });
                    }
                }
            } else {
                if (ui.draggable[0].id != null && ui.draggable[0].id != "") {
                    var addedEntity = { id: ui.draggable[0].id.replace("card-", ""), entitytype: ui.draggable[0].attributes["entity-type-id"].value };
                } else {

                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var selection = grid.getSelectedRowKeys();
                        addedEntity = { id: selection[0].Id, entitytype: selection[0].EntityType }
                    }
                }
                entitys.push(addedEntity);
            }


            this.LinksCollection = new Backbone.Collection();

            if (this.LinksCollection.length > 0) {
                this.LinksCollection.reset();
            }

            _.each(entitys, function (entity) {

                var addedEntity = entity.id;

                if (entitytype != null && entity.entitytype != entitytype) {
                    return;
                }

                if (tableView) {
                    var relationTypeObj = _.find(that.relationTypes.models, function (relationType) {
                        return relationType.attributes.TargetEntityTypeId == entity.entitytype;
                    });
                    if (relationTypeObj) {
                        relationType = relationTypeObj.attributes.Id;
                    }
                }

                var relationSectionElement = that.$el.find("#" + relationType + " #relation-section-container #" + entity.id);
                var relationSubSectionElement = that.$el.find("#" + relationType + " #relation-sub-section-container #" + entity.id);
                if (relationSectionElement.length > relationSubSectionElement.length) {
                    return;
                }

                var sourceId = addedToEntity;
                var targetId = addedEntity;

                if (linkentitytypeid && linkentitytypeid.value != "") {
                    var linkItemRelation = new Backbone.Model();

                    linkItemRelation.relationTypeToCreate = relationType;
                    linkItemRelation.sourceIdToCreate = sourceId;
                    linkItemRelation.targetIdToCreate = targetId;
                    linkItemRelation.entityTypeIdToCreate = linkentitytypeid.value;

                    that.LinksCollection.add(linkItemRelation);

                } else {
                    var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId });
                    newModel.save([], {
                        success: function (model) {
                            that.relations.add(model);

                            window.appHelper.event_bus.trigger('entityupdated', model.id);

                            if (that.size == "small") {
                                if ($("#nodelist #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodelist #" + model.attributes["LinkType"]).removeClass("droppable-empty");
                                    $.each($("#nodelist .card-wrap").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }
                            else if (that.size == "table") {
                                that.renderTableList();
                            }
                            else {
                                if ($("#nodebox #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodebox #" + model.attributes["LinkType"]).removeClass("droppable-empty");

                                    $.each($("#nodebox .card-wrap-large").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }
                            var checkIfCardExists = self.$el.find("#" + relationType + " #relation-section-container #card-" + model.get("TargetId"));

                            if (checkIfCardExists.length === 0) {
                                var relationCardSub = new relationCardView({ relation: model, direction: "outbound" });

                                $("#" + model.attributes["LinkType"] + " #relation-section-container").append(relationCardSub.$el);
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
            });

            if (linkentitytypeid && linkentitytypeid.value != "" && that.LinksCollection.length > 0) {

                that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeMultipleLinkCreatation);

                entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                    id: 0,
                    entityTypeId: linkentitytypeid.value,
                    showInWorkareaOnSave: false
                });
            }
        },
        addLinkedRelation: function (event, ui, self, relationType) {
            event.stopPropagation();
            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {
                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                var workareaView = window.appHelper.workAreaHandler.getActiveWorkarea();

                if (!workareaView) {
                    return;
                }

                var cards = workareaView.getSelectedEntityIds();

                _.each(cards, function (card) {

                    var addedToEntity = self.id;
                    var addedEntity = card;
                    var linkentityid = event.target.id;
                    var cardView = _.find(workareaView.cards, function (c) {
                        return c.card.model.attributes.Id == card;
                    });

                    var relationSectionElement = self.$el.find("#" + relationType + " #relation-section-container #" + addedEntity);

                    if (relationSectionElement.length > 0) {
                        return;
                    }

                    var typeObj = $(ui.draggable).closest(".card-section-wrap");
                    var linktype = "";
                    if (typeObj[0]) {
                        linktype = typeObj[0].id;
                    }

                    if (cardView.card.model.attributes.EntityType != linktype) {
                        return;
                    }

                    var sourceId = addedToEntity;
                    var targetId = addedEntity;

                    var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (model) {
                            self.relations.add(model);

                            var relationCard = new relationCardView({ relation: model, direction: "outbound" });

                            $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                });

            } else {

                var addedToEntity = self.model.id;
                var addedEntity = ui.draggable[0].id.replace("card-", "");
                var linkentityid = event.target.id;

                var sourceId = addedToEntity;
                var targetId = addedEntity;

                var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId, LinkEntityId: linkentityid });
                newModel.save([], {
                    success: function (model) {
                        self.relations.add(model);

                        var relationCard = new relationCardView({ relation: model, direction: "outbound" });

                        self.$el.find("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onToggleView: function (e) {
            e.stopPropagation();

            if (this.mode == "card") {
                this.mode = "table";
            } else {
                this.mode = "card";
            }

            window.appSession.relationViewMode = this.mode;
            this.render();
        },
        onIncludeFields: function (e) {
            e.stopPropagation();
            if (this.includeFields) {
                this.includeFields = false;
            } else {
                this.includeFields = true;
            }

            this.render();
        },
        setVisualMode: function () {
            this.$el.find("#card-view").removeClass("content-active-view");
            this.$el.find("#table-view").removeClass("content-active-view");
            this.$el.find("#include-fields").hide();

            if (this.mode == "table") {
                this.$el.find("#table-view").addClass("content-active-view");

                var groups = _.groupBy(this.relations.toJSON(), function (model) {
                    return model.LinkType;
                });
                var keys = _.keys(groups);
                if (keys.length == 1) {
                    this.$el.find("#include-fields").show();
                }
            } else {
                this.$el.find("#card-view").addClass("content-active-view");
            }
        },
        onEntityUpdated: function (model) {
            if (this.$el.find("#" + model).length > 0) {
                var self = this;
                var linkEntity = new entityModel({
                    id: model
                });

                linkEntity.fetch({
                    url: "/api/entity/" + model,
                    success: function () {
                        self.$el.find("#" + linkEntity.id + " .relation-sub-section-title-text").html("<i class=\"fa fa-chain\"></i>" + linkEntity.attributes.DisplayName);
                        if (linkEntity.attributes.DisplayDescription != "") {
                            self.$el.find("#" + linkEntity.id + " .relation-sub-section-title-text").append("-" + linkEntity.attributes.DisplayDescription);
                        }
                    }


                });

            }
        },
        onEntityDeleted: function (model) {
            var relationCards = this.$el.find("#" + model);
            _.each(relationCards, function (relationCard) {
                var subSectionContainer = relationCard.closest("#relation-sub-section-container");
                if (subSectionContainer && subSectionContainer.childElementCount == 1) {
                    var elementToRemove = subSectionContainer.parentElement.parentElement;
                    elementToRemove.parentElement.removeChild(elementToRemove);
                } else {
                    relationCard.remove();
                }
            });
        },
        onRelationRemoved: function (linkEntityId, entityId) {
            var subSectionContainer = this.$el.find("#" + linkEntityId + " #relation-sub-section-container");
            if (subSectionContainer.length > 0 && subSectionContainer[0].childElementCount == 1) {
                subSectionContainer.parent().parent().remove();
            }
        },
        pushAddAndLinkTool: function (tools) {
            tools.push({ name: "addAndLink", title: "Add and link new entity", icon: "icon-entypo-fix-plus std-entypo-fix-icon", text: "Add new entity", permission: "AddEntity,AddLink" });
        },
        render: function () {

            var self = this;

            this.$el.empty();

            _.each(this.relationTypes, function (model) {

                var subContentView = $(_.template(campaignSubContentTemplate, { model: model, entitytypeid: model.TargetEntityTypeId }));
                var tools = new Array();
                self.pushAddAndLinkTool(tools);

                subContentView.find("#contextmenu").html(new contextMenuView({
                    id: model.id,
                    tools: tools
                }).$el);

                var lastLinkEntityId = -1;

                _.each(self.relations.models, function (relation) {
                    if (relation.get("LinkType") == model.Id) {

                        if (relation.attributes["LinkEntity"]) {

                            var linkentity = relation.attributes["LinkEntity"];

                            if (lastLinkEntityId != linkentity.Id) {
                                lastLinkEntityId = linkentity.Id;

                                var subsectionLinkEntity = $(_.template(relationTypeLinkEntityGroupTemplate, { model: relation.toJSON(), entitytypeid: model.TargetEntityTypeId }));
                                tools = new Array();
                                tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                                self.pushAddAndLinkTool(tools);

                                subsectionLinkEntity.find("#contextmenu").html(new contextMenuView({
                                    id: relation.id,
                                    tools: tools
                                }).$el);

                                subContentView.find("#relation-section-container").append(subsectionLinkEntity);
                            }

                            var relationCardSub = new relationCardView({ relation: relation, direction: "outbound" });
                            subContentView.find("#" + lastLinkEntityId + " #relation-sub-section-container").append(relationCardSub.$el);

                            if (self.droppable) {
                                subContentView.find("#" + lastLinkEntityId).droppable({
                                    greedy: true,
                                    accept: function (draggable) {

                                        var typeObj = $(draggable).closest(".card-section-wrap");
                                        var linktype = "";
                                        if (typeObj[0]) {
                                            linktype = typeObj[0].id;
                                        }
                                        if (linktype != id) {
                                            return false;
                                        }

                                        var drager = draggable[0].id.replace("card-", "");
                                        //kolla om dragger finns.
                                        if (drager == null || drager == "") {
                                            return false;
                                        }

                                        var x = $(this).find("#" + drager);
                                        if (x.length > 0) {
                                            return false;
                                        }
                                        return true;
                                    },
                                    activeClass: "relation-sub-section-container-active",
                                    hoverClass: "relation-dropable-hover",
                                    drop:
                                        function (event, ui) {
                                            self.addLinkedRelation(event, ui, self, model.Id);
                                        }
                                });
                            }

                            if (self.sortable) {
                                subContentView.find("#relation-section-container").sortable({
                                    handle: ".card-picture-area, .card-wrap",
                                    placeholder: "card-placeholder",
                                    update: self.onChangeSortOrder,
    
                                });

                                subContentView.find("#relation-sub-section-container").sortable({
                                    handle: ".relation-picture-area, .card-wrap",
                                    placeholder: "card-placeholder",
                                    update: self.onChangeSortOrder,
                                });
                            }

                        } else {
                            var relationCard = new relationCardView({ relation: relation, direction: "outbound" });

                            subContentView.find("#relation-section-container").append(relationCard.$el);

                            if (self.sortable) {
                                subContentView.find("#relation-section-container").sortable({
                                    // connectWith: "#relation-section-container",
                                    handle: ".card-picture-area, .card-wrap",
                                    placeholder: "card-placeholder",
                                    update: self.onChangeSortOrder
                                });
                            }

                        }
                    }
                });

                var id = model.TargetEntityTypeId;

                self.$el.append(subContentView);

                if (self.droppable) {
                    $("#" + model["Id"]).droppable({
                        accept: function (draggable) {

                            var drager = draggable[0].id;

                            if (drager == "") {
                                //kolla om det är från en tabell.

                                var gridDiv = $(draggable[0]).closest("#entity-table-container");
                                if (gridDiv.length > 0) {
                                    var grid = gridDiv.dxDataGrid('instance');
                                    var entities = grid.getSelectedRowKeys();
                                    if (entities && entities.length > 0) {
                                        var ok = false;
                                        var i = 0;
                                        while (!ok && i < entities.length) {
                                            var x = $(this).find("#" + entities[i].Id);
                                            ok = !x || x.length == 0;

                                            ok = ok && (entities[i].EntityType == id);
                                            i++;
                                        }

                                        return ok;
                                    }

                                }
                            }

                            var typeObj = $(draggable).closest(".card-section-wrap");
                            var linktype = "";
                            if (typeObj[0]) {
                                linktype = typeObj[0].id;
                            }
                            if (linktype != id) {
                                return false;
                            }

                            //kolla om dragger finns.
                            if (drager == null || drager == "") {
                                return false;
                            }

                            var x = $(this).find("#" + drager);
                            if (x.length > 0) {
                                return false;
                            }
                            return true;
                        },
                        activeClass: "relation-section-container-active",
                        hoverClass: "relation-dropable-hover",
                        drop:
                            function (event, ui) {
                                self.addRelation(event, ui, self, id);
                            }
                    });
                }
            });

            var ids = $(".relation-section-wrap");

            for (var index = 0; index < ids.length; ++index) {
                showLinkTypes('#' + ids[index].id);
            }

            function showLinkTypes(id) {
                id = id.replace(" ", "-");
                var $heading = $(id + " .relation-section-title .showLinkTypes");
                var $heading2 = $(id + " .relation-section-title .relation-section-title-text");
                var $box = $(id).find("#relation-section-container");

                var $showTask = $(id + '-show i');

                $heading.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
                $heading2.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            ids = $(".relation-sub-section-wrap");

            for (index = 0; index < ids.length; ++index) {
                showSubLinkTypes('#' + ids[index].id);
            }

            function showSubLinkTypes(id) {
                id = id.replace(" ", "-");
                var $heading = $(id + " .relation-sub-section-title .showLinkTypes");
                var $heading2 = $(id + " .relation-sub-section-title .relation-section-title-text");

                var $box = $(id).find("#relation-sub-section-container");
                var $showTask = self.$el.find(id + '-show i');

                $heading.click(function (e) {
                    $box.toggle(200);
                    $showTask.toggle();
                });
                $heading2.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });

                return this;
            }
        }
    });

    return relationCampaignSubView;
});