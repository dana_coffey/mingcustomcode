﻿define([
  'jquery',
  'underscore',
  'backbone',
  'modalPopup',
  'sharedjs/misc/permissionUtil',
  'text!templates/contextmenu/appContextMenuTemplate.html'
], function ($, _, backbone, modalPopup, permissionUtil, appContextMenuTemplate) {

    var appContextMenuView = backbone.View.extend({
        initialize: function () {
            this.render();
        },
        events: {
            "click .app-tools-header": "onToggleTools",
            "click #close-tools-container": "deselect",
            "click #gant-view": "gotoGantView"
        },
        deselect: function (e) {
            var $box = this.$el.find("#app-tools-container");
            $box.hide();
            $(document).unbind('click');
        },
        onToggleTools: function (e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
            var $box = this.$el.find("#app-tools-container");
            $box.toggle();

            $box.position({
                my: "right top",
                at: "right top+25",
                of: $("#tools-show")
            });

            $(document).one('click', this.deselect.bind(this));
        },
        gotoGantView: function (e) {
            this.goTo("campaigns");
        },

        render: function () {
            this.$el.html(appContextMenuTemplate);

            return this;
        }
    });

    return appContextMenuView;

});
