﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'text!templates/entityView/entityViewDetailsSubviewTemplate.html',
  'text!templates/entityView/entityViewDetailsSubviewRowTemplate.html'

], function ($, _, Backbone, inRiverUtil, entityViewDetailsSubviewTemplate, entityViewDetailsSubviewRowTemplate) {

    var taskDetailsSubView = Backbone.View.extend({
        template: _.template(entityViewDetailsSubviewTemplate),
        rowTemplate: _.template(entityViewDetailsSubviewRowTemplate),
        initialize: function () {
        },
        events: {
        },
        onClose: function () {
        },
        taskUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch(
                {
                    success: function () { self.render(); },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

        },

        render: function () {
            var that = this;
            this.$el.html(this.template({ data: this.model.toJSON() }));

            var i = 0;
            var container = this.$el.find("#details-row-container");
            _.each(this.model.get("Fields"), function (f) {
                if (!f.IsHidden) {
                    container.append(that.rowTemplate({ key: f.FieldTypeDisplayName, value: f.DisplayValue, isOddRow: i++ % 2 == 1 }));
                }
            });
            this.listenTo(appHelper.event_bus, 'entityupdated', this.taskUpdated);
            return this; // enable chained calls
        },
    });

    return taskDetailsSubView;
});

