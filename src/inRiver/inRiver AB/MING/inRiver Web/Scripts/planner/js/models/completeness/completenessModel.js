﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var completenessModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function (options) {
            this.urlRoot = "../../api/tools/completenessdetails" + options.entityId;
            this.fetch();
        }
    });

    return completenessModel;

});

