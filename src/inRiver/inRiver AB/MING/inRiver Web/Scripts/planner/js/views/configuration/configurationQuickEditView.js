define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'sharedjs/models/setting/SettingModel',
  'views/configuration/entityTypeFieldSelectionTable',
  'text!templates/configuration/configurationQuickEditViewTemplate.html'

], function ($, _, backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    settingModel, entityTypeFieldSelectionTable, configurationQuickEditViewTemplate) {

    var configurationQuickEditView = backbone.View.extend({
        template: _.template(configurationQuickEditViewTemplate),
        initialize: function () {
            window.appHelper.event_bus.off('planner-configuration-save');
            window.appHelper.event_bus.off('planner-configuration-undo');

            this.listenTo(window.appHelper.event_bus, 'planner-configuration-save', this.onSaveAll);
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-undo', this.onUndoAll);
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {
        },
        onSaveAll: function () {
            // Update model with selection and save to server
            plannerGlobals.quickEditFieldTypesModel.clear({ silent: true });
            var data = {};
            _.each(this.selectionTable.rows, function (row) {
                data[row.id] = row.getSelection();
            });
            plannerGlobals.quickEditFieldTypesModel.save(data);
            this.selectionTable.updateUi();
            inRiverUtil.Notify("Configurations have been saved");
        },
        onUndoAll: function () {
            this.selectionTable.updateUi();
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            return this.selectionTable ? this.selectionTable.hasUnsavedChanges() : false;
        },
        render: function () {
            this.$el.html(this.template());
            this.selectionTable = new entityTypeFieldSelectionTable({
                model: plannerGlobals.quickEditFieldTypesModel,
                defaultSelectAll: true
            });
            this.$el.children(0).append(this.selectionTable.$el.children(0));
            this.onUndoAll();
            return this; // enable chained calls
        }
    });

    return configurationQuickEditView;
});

