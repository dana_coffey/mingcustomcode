define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'views/planner/plannerGanttFiltersAndGroupsSubview',
  'views/planner/plannerGanttDateRangeSubview',
  'views/planner/plannerGanttSavedViewsSubview',
  'views/completeness/completenessPopupView',
  'views/start/startPageView',
  'views/planner/plannerGanttComponentSubview',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/excelexport/excelExportView',
  'text!templates/planner/plannerGanttViewTemplate.html',
  'jquery-filedownload'
], function ($, _, Backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    plannerGanttFiltersAndGroupsSubview, plannerGanttDateRangeSubview, plannerGanttSavedViewsSubview,
    completenessPopupView, startPageView,
    plannerGanttComponentSubview, entityDetailSettingView,
    entityDetailSettingWrapperView, excelExportView, plannerGanttViewTemplate,
    jqueryFiledownload) {

    var plannerGanttView = Backbone.View.extend({
        template: _.template(plannerGanttViewTemplate),
        initialize: function () {
            if (window.location.toString().toLowerCase().indexOf("isprinting=true") >= 0 || (typeof wnvPdfConverter !== 'undefined')) {
                console.log("Gantt is in printing mode");
                plannerGlobals.isPrinting = true;
                plannerGlobals.printSortOrder = inRiverUtil.getUrlVar("sortorder");
            }

            this.collection = window.plannerGlobals.ganttDataCollection;

            this.listenTo(plannerGlobals.currentViewStateModel, "change", this.onViewStateModelChanged);

            this.render();

            if (!this.collection.length > 0) {
                this.$el.find("#print").addClass("print-disabled");
                window.plannerGlobals.fetchCollection();
            } else {
                this.$el.find("#loading-spinner").hide();
                $("#print").removeClass("print-disabled");
            }
        },
        events: {
            //"change input[name=listmode]": "onChangeListMode",
            "change #gantt-scale-mode": "onChangeGanttScaleMode",
            "change #gantt-dimension-1": "onChangeGanttDimension",
            "change #gantt-dimension-2": "onChangeGanttDimension",
            "change #gantt-bar-title-field-type": "onChangeGanttBarTitle",
            "click #toggle-filters-groups": "onToggleFiltersGroups",
            "click #save-current-view-state": "onSaveCurrentViewState",
            //"change #saved-view-states": "onChangeSavedViewState"
            "click #open-saved-view": "onOpenSavedView",
            "click #print": "onPrint",
            "click #export": "onExport"
        },
        onViewStateModelChanged: function () {

            // Could be improved...
            if (plannerGlobals.currentViewStateModel.hasChanged("Dimensions")) {
                window.plannerGlobals.fetchCollection();
                this._ganttFiltersAndGroupsSubView.render();
                this.updateDimensionDropdowns();
            } else if (plannerGlobals.currentViewStateModel.hasChanged("BarTitleFieldType")) {
                window.plannerGlobals.fetchCollection();
                this._ganttFiltersAndGroupsSubView.render();
                this.updateDimensionDropdowns();
            } else if (plannerGlobals.currentViewStateModel.hasChanged("FilterValues")) {
                window.plannerGlobals.fetchCollection();
                this._ganttFiltersAndGroupsSubView.render();
            } else if (plannerGlobals.currentViewStateModel.hasChanged("Groups")) {
                window.plannerGlobals.fetchCollection();
                this._ganttFiltersAndGroupsSubView.render();
            } else if (plannerGlobals.currentViewStateModel.hasChanged("FromDate") || plannerGlobals.currentViewStateModel.hasChanged("ToDate")) {
                window.plannerGlobals.fetchCollection();
                this._ganttDateRangeSubView.updateUi();
            }

            var needRender = false;
            var scaleMode = window.plannerGlobals.currentViewStateModel.get("ScaleMode");
            if (scaleMode) {
                if (!this.isValidScaleMode(scaleMode)) {
                    // Default to first mode if in unknown state
                    scaleMode = "year";
                    window.plannerGlobals.currentViewStateModel.set("ScaleMode", scaleMode, { silent: true });
                }
                this.$el.find("#gantt-scale-mode").val(scaleMode);
                if (plannerGlobals.currentViewStateModel.hasChanged("ScaleMode")) {
                    window.plannerGlobals.setScaleConfig(scaleMode);
                    needRender = true;
                }
            }

            if (plannerGlobals.currentViewStateModel.hasChanged("FromDate") || plannerGlobals.currentViewStateModel.hasChanged("ToDate")) {
                window.plannerGlobals.fetchCollection();
                this._ganttDateRangeSubView.updateUi();
                needRender = true;
            }

            //var sortSettings = window.plannerGlobals.currentViewStateModel.get("SortSettings");
            //if (sortSettings) {
            //    if (window.plannerGlobals.currentViewStateModel.hasChanged("SortSettings")) {
            //        window.plannerGlobals.setSortSettings(sortSettings);
            //        needRender = true;
            //    }
            //}

            if (needRender) {
                gantt.render();
            }

            this.updateLayout(this);
        },
        onChangeGanttScaleMode: function () {
            var val = this.$el.find("#gantt-scale-mode").val();
            window.plannerGlobals.currentViewStateModel.save({ "ScaleMode": val}) ;
        },
        onChangeGanttDimension: function () {
            plannerGlobals.markViewStateAsUnsaved();

            var attributesToSet = {
                "Dimensions.0": this.$el.find("#gantt-dimension-1").val(),
                "Dimensions.1": this.$el.find("#gantt-dimension-2").val(),
                "BarTitleFieldType": this.$el.find("#gantt-bar-title-field-type").val(),
                "EntityFilter": null
            };

            // Check data integrity (make sure the second dimension is a valid entity type by checking the related entity types info)
            if (!_.contains(Object.keys(window.plannerGlobals.entityTypes.get(attributesToSet["Dimensions.0"] + ".RelatedEntityTypes")), attributesToSet["Dimensions.1"])) {
                attributesToSet["Dimensions.1"] = null; // remove "invalid" element 
            }

            //  Check data integrity (make sure the bar title entity type is a valid entity type by checking the related entity types info)
            if (attributesToSet["BarTitleFieldType"]) {
                if (!_.contains(Object.keys(window.plannerGlobals.entityTypes.get(attributesToSet["Dimensions.0"] + ".RelatedEntityTypes")), attributesToSet["BarTitleFieldType"].split(".")[0])) {
                    attributesToSet["BarTitleFieldType"] = null; // remove "invalid" setting 
                }
            }
            //console.log(JSON.stringify(attributesToSet));

            window.plannerGlobals.currentViewStateModel.save(attributesToSet); // will trigger a fetchCollection

            this.updateDimensionDropdowns();
        },
        onChangeGanttBarTitle: function() {
            plannerGlobals.markViewStateAsUnsaved();

            var attributesToSet = {
                "BarTitleFieldType": this.$el.find("#gantt-bar-title-field-type").val(),
            };

            window.plannerGlobals.currentViewStateModel.save(attributesToSet); // will trigger a fetchCollection

            this.updateDimensionDropdowns();
        },
        onToggleFiltersGroups: function () {
            var that = this;
            this.$el.find("#gantt-config-area").toggle(0, function() {
                that.updateLayout(that);
            });
        },
        onSaveCurrentViewState: function () {
            var self = this;

            inRiverUtil.Prompt("Enter a name for your new view", function(e, name) {
                if (e) {
                    // add current viewstate to collection (will be saved to server)
                    window.plannerGlobals.currentViewStateModel.set("Id", null, { silent: true });
                    window.plannerGlobals.viewStateCollection.create({ Name: name, ViewState: window.plannerGlobals.currentViewStateModel.toJSON() }, { 
                         success: function(model, response) {
                             self.successCallbackIcal(model, response);
                             plannerGlobals.currentViewStateModel.save(_.clone(model.get("ViewState")), { silent: true }); // To make sure Id from the server is set
                             window.plannerGlobals.setGanttTitle(plannerGlobals.currentViewStateModel.get("Name"));
                         }
                    }); 
                 }
            });
        },
        successCallbackIcal: function(m, response) {

            var viewStateiCal = new Backbone.DeepModel();
            viewStateiCal.urlRoot = "/api/planner/createicalurl/" + response.id;

            viewStateiCal.save(null, {
                error: function (model, errorMessage) {
                    inRiverUtil.OnErrors(model, errorMessage);
                }
            });
        },
        onOpenSavedView: function() {
            var pop = new modalPopup();
            pop.popOut(new plannerGanttSavedViewsSubview({
                parent: this
            }), { size: "medium", header: "Open View", usesavebutton: false });
            $("#save-form-button").removeAttr("disabled");
        },
        onPrint: function (e) {
            var sortOrder = plannerGlobals.getGanttSortOrder();


            if ($(e.currentTarget).hasClass("print-disabled")) {
                return true; // view is not ready for print yet
            }
            $("#over-div-content").html("<div class='pdf-in-progress'><img src='/images/ajax-loader.gif'><span>We are preparing your pdf</span></div>");
            $("#over-div-wrap").show();
            setTimeout(function () {
                var filename = plannerGlobals.currentViewStateModel.get("Name") ? plannerGlobals.currentViewStateModel.get("Name") : plannerGlobals.entityTypes.get(plannerGlobals.currentViewStateModel.get("Dimensions.0")).Name;
                var width = $(".gantt_grid").width() + $(".gantt_task_bg").width();
                var extrasHeight = $(".gantt_task_scale").height(); // + $(".gantt_task_bg").height() + 1;
                extrasHeight += 30; // Header area
                var rows = gantt._order.length;
                var fileUrl = "/api/planner/pdf?" + $.param({
                        width: Math.ceil(width),
                        extrasHeight: Math.ceil(extrasHeight),
                        rows: rows,
                        filename: filename,
                        sortOrder: sortOrder,
                        viewStateForPrinting: JSON.stringify(plannerGlobals.currentViewStateModel.toJSON())
                    });
                $.fileDownload(fileUrl, {
                    successCallback: function (url) {
                        $("#over-div-wrap").hide();
                    },
                    failCallback: function (html, url) {
                        $("#over-div-wrap").hide();
                    }
                });
                //window.location = "/api/planner/pdf?" + $.param({
                //    width: Math.ceil(width),
                //    extrasHeight: Math.ceil(extrasHeight),
                //    rows: rows,
                //});
            }, 1000);
            return false;
        },
        onExport: function () {
            this.modal = new modalPopup();
            this.modal.popOut(new excelExportView({ entityIds: _.uniq(this.collection.pluck("EntityId")), entitytypeIds: Object.keys(window.plannerGlobals.entityTypes.attributes) }), { size: "medium", usesavebutton: false });
        },
        onClose: function () {
            if (this._ganttSubView) {
                this._ganttSubView.close();
            }
        },
        updateLayout: function (that) {
            var configAreaIsVisible = that.$el.find("#gantt-config-area").is(":visible");
            if (configAreaIsVisible) {
                // Super hack to force width of the filter and groups area so it wraps properly in a small window
                var containerWidth = that.$el.find("#gantt-config-area").width();
                var dimensionsAreaWidth = that.$el.find("#gantt-dimensions-area").width();
                that.$el.find("#filters-and-groups-area").css("width", (containerWidth - dimensionsAreaWidth - 30) + "px");
            }

            var height = configAreaIsVisible ? that.$el.find("#filters-and-groups-area").height() : 0;
            that.$el.find("#campaign-overview-dataview").css("margin-top", height + "px");
            gantt.setSizes();
        },
        updateDimensionDropdowns: function() {
            this.$el.find("#gantt-dimension-1").val(window.plannerGlobals.currentViewStateModel.get("Dimensions.0"));

            var dim2El = this.$el.find("#gantt-dimension-2");
            dim2El.html($("<option></option>").attr("value", "").text("(not set)"));
            _.each(window.plannerGlobals.entityTypes.get(window.plannerGlobals.currentViewStateModel.get("Dimensions.0")).RelatedEntityTypes, function (text, value) {
                var optionEl = $("<option></option>")
                       .attr("value", value)
                       .text(text);
                dim2El.append(optionEl);
            });
            if (window.plannerGlobals.currentViewStateModel.get("Dimensions").length > 0) {
                dim2El.val(window.plannerGlobals.currentViewStateModel.get("Dimensions.1"));
            }

            // Bar title dropdown
            var barTitleDropdownEl = this.$el.find("#gantt-bar-title-field-type");
            barTitleDropdownEl.html($("<option></option>").attr("value", "").text("(default)"));
            var dim1EntityType = plannerGlobals.currentViewStateModel.get("Dimensions.0");
            var compatibleEntityTypes = [dim1EntityType];
            compatibleEntityTypes = compatibleEntityTypes.concat(_.keys(plannerGlobals.entityTypes.get(dim1EntityType + ".RelatedEntityTypes")));
            //if (!_.contains(compatibleEntityTypes, appHelper.topLevelPlannerEntityType)) {
            //    compatibleEntityTypes.push(appHelper.topLevelPlannerEntityType);
            //}
            _.each(compatibleEntityTypes, function (entityTypeId) {
                if (entityTypeId && Object.keys(plannerGlobals.barTitleDefs.get(entityTypeId)).length > 0) {
                    barTitleDropdownEl.append("<optgroup label='--- " + plannerGlobals.entityTypes.get(entityTypeId + ".Name") + " ---'>");
                    _.each(plannerGlobals.barTitleDefs.get(entityTypeId), function (text, fieldTypeId) {
                        var optionEl = $("<option></option>")
                            .attr("value", entityTypeId + "." + fieldTypeId)
                            .text(text);
                        barTitleDropdownEl.append(optionEl);
                    });
                }
            });
            //if (window.plannerGlobals.currentViewStateModel.get("Dimensions").length > 0) {
            barTitleDropdownEl.val(window.plannerGlobals.currentViewStateModel.get("BarTitleFieldType"));
            //}
        },
        onInsertedInDOM: function () {
            if (this._ganttSubView instanceof plannerGanttComponentSubview) {
                this._ganttSubView.onInsertedInDOM();
            }
        },
        plannerEntityUpdated: function (entityId) {
            if (this.collection.findWhere({ EntityId: entityId })) {
                window.plannerGlobals.fetchCollection();
            }
        },
        plannerEntityCreated: function (model) {
            if (plannerGlobals.currentViewStateModel.get("EntityFilter")) {
                // If entity filter is applied then add the new entity to it to give the user feedback of the creation
                plannerGlobals.currentViewStateModel.get("EntityFilter").push(model.get("Id"));
            }
            window.plannerGlobals.fetchCollection();
        },
        isValidScaleMode: function(s) {
            if (s == "day" || s == "week" || s == "month" || s == "year") {
                return true;
            } else {
                return false;
            }
        },
        renderGanttSubView: function () {
            this._ganttSubView = new plannerGanttComponentSubview(this.collection);
            this._ganttSubView._parent = this;
            this.$el.find("#campaign-overview-dataview").html(this._ganttSubView.el);
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());

            this._ganttFiltersAndGroupsSubView = new plannerGanttFiltersAndGroupsSubview({ parent: this });
            this.$el.find("#filters-and-groups-area").html(this._ganttFiltersAndGroupsSubView.el);

            // Dimension 1 always has fixed content so render just once...
            var dimEl = this.$el.find("#gantt-dimension-1");
            _.each(window.plannerGlobals.entityTypes.attributes, function (entityType, key) {
                var optionEl = $("<option></option>")
                       .attr("value", key)
                       .text(entityType.Name);
                dimEl.append(optionEl);
            });
            this.updateDimensionDropdowns();

            // Gantt date range sub view
            this._ganttDateRangeSubView = new plannerGanttDateRangeSubview({ parent: this });
            this.$el.find("#gantt-date-range-area").html(this._ganttDateRangeSubView.el);

            // Gantt scale dropdown
            var ganttScaleMode = window.plannerGlobals.currentViewStateModel.get("ScaleMode");
            if (!this.isValidScaleMode(ganttScaleMode)) {
                // Default to first mode if in unknown state
                window.plannerGlobals.currentViewStateModel.set("ScaleMode", "year", { silent: true });
            }
            this.$el.find("#gantt-scale-mode").val(window.plannerGlobals.currentViewStateModel.get("ScaleMode"));

            //this.$el.find("label img").on("click", function () {
            //    $("#" + $(this).parents("label").attr("for")).click();
            //});

            this.listenTo(appHelper.event_bus, 'entityupdated', this.plannerEntityUpdated);
            this.listenTo(appHelper.event_bus, 'entitycreated', this.plannerEntityCreated);

            // TODO: FIX!!!!
            permissionUtil.CheckPermissionForElement("AddEntity", this.$el.find("#new-button"));

            this.renderGanttSubView();

            return this; // enable chained calls
        },
    });
    
    return plannerGanttView;
});

