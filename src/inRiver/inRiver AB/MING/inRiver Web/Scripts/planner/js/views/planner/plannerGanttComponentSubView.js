﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/planner/plannerGanttComponentSubviewTemplate.html',
  'dhtmlxgantt',
  'dhtmlxgantt_marker',
  'dhtmlxgantt_tooltip',
  'dhtmlxgantt_smart_rendering',
  'datejs',
  'misc/plannerGlobals',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'moment'

], function ($, _, Backbone, plannerGanttComponentSubviewTemplate, dhtmlxgantt, dhtmlxgantt_marker, dhtmlxgantt_tooltip, dhtmlxgantt_smart_rendering, datejs, plannerGlobals, inRiverUtil, entityDetailSettingWrapperView, moment) {

    var plannerGanttComponentSubview = Backbone.View.extend({
        template: _.template(plannerGanttComponentSubviewTemplate),
        initialize: function (collection) {
            this.collection = collection;

            this.listenTo(this.collection, 'change', this.onTaskModelChange);
            this.listenTo(this.collection, 'reset', this.onCollectionReset);
            this.listenTo(plannerGlobals.currentViewStateModel, 'change:EntityFilter', this.updateAllTasksInGantt);
            this.listenTo(appHelper.event_bus, 'entityFilterChanged', this.updateAllTasksInGantt);

            this.render();
        },
        events: {
            "click .gantt-edit-task-button": "onEditTask",
        },
        onCollectionReset: function () {
            if (plannerGlobals.isPrinting) {
                plannerGlobals.dataForPrintingIsFetched = true;
            }
            this.updateAllTasksInGantt();
        },
        onCreateTask: function (parentEntityId) {
            var dim1EntityType = plannerGlobals.currentViewStateModel.get("Dimensions")[0];
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: dim1EntityType,
                showInWorkareaOnSave: false
            });
        },
        onEditTask: function (entityId, entityType) {
            var fieldTypesFilter = plannerGlobals.quickEditFieldTypesModel.get(entityType);
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: entityId,
                fieldTypesFilter: fieldTypesFilter
            });
        },
        onTaskOpenedOrClosed: function (task) {
            var branches = Object.keys(gantt._branches);
            branches.shift(); // removes the first (root) element with id 0
            var collapsedItems = [];
            _.each(branches, function (b) {
                if (!gantt.getTask(b).$open) {
                    collapsedItems.push(b);
                }
            });
            plannerGlobals.currentViewStateModel.save("CollapsedItems", collapsedItems, { silent: true });
        },
        onClose: function () {
        },
        ganttTaskNameTemplate: function (task) {
            if (task.entityId) {
                var clickAction = 'window.app_router.navigate("entity/' + task.entityId + '", { trigger: true });';
                return "<span class='gantt-entity-name' onClick='" + clickAction + "'>" + task.text + "</span>";
            } else {
                return "<span>" + task.text + "</span>";
            }
        },
        ganttTaskProgressTemplate: function (task) {
            if (task.entityId && task.completenessDisplayText) {
                return "<span>" + task.completenessDisplayText + "</span>";
            }
            return "";
        },
        ganttTaskButtonsTemplate: function (task) {
            if (task.entityType != null) {
                return '<i class="fa fa-pencil row-icon-button gantt-task-button" onClick="event.stopPropagation(); gantt.onEditTask(' + task.entityId + ', &quot;' + task.entityType + '&quot;);"></i>';
            }
            return "";
        },
        createInlinePreviewTaskBox: function (sizes, className) { // Used to show child tasks when parent is collapsed
            var box = document.createElement('div');
            box.style.cssText = [
                "height:" + sizes.height + "px",
                "line-height:" + sizes.height + "px",
                "width:" + sizes.width + "px",
                "top:" + sizes.top + 'px',
                "left:" + sizes.left + "px",
                "position:absolute"
            ].join(";");
            box.className = className;
            return box;
        },
        createInlinePreviewMilestone: function (sizes, className) { // Used to show child tasks when parent is collapsed
            var box = document.createElement('div');
            box.style.cssText = [
                "height:" + sizes.height + "px",
                "line-height:" + sizes.height + "px",
                "width:" + sizes.width + "px",
                "top:" + sizes.top + 'px',
                "left:" + sizes.left + "px",
                "position:absolute",
                "transform: rotate(45deg)"
            ].join(";");
            box.className = className;
            return box;
        },
        getTaskFitValue: function (task, text) {
            var taskStartPos = gantt.posFromDate(task.start_date),
                taskEndPos = gantt.posFromDate(task.end_date);

            var width = taskEndPos - taskStartPos;
            var textWidth = (text || "").length * gantt.config.font_width_ratio;

            if (width < textWidth) {
                var ganttLastDate = gantt.getState().max_date;
                var ganttEndPos = gantt.posFromDate(ganttLastDate);
                if (ganttEndPos - taskEndPos < textWidth) {
                    return "left";
                }
                else {
                    return "right";
                }
            }
            else {
                return "center";
            }
        },
        onInsertedInDOM: function () {
            var that = this;

            if (plannerGlobals.ganttEvents && plannerGlobals.ganttEvents.length > 0) {
                _.each(plannerGlobals.ganttEvents, function (eventId) {
                    gantt.detachEvent(eventId);
                });
            }

            plannerGlobals.ganttEvents = [];

            // Create the gantt component AFTER this view is inserted on the page (otherwise it won't render properly)
            //gantt._parentView = this;
            //gantt.config.auto_scheduling = true;
            gantt.config.show_errors = false;
            //gantt.config.fit_tasks = true;
            //gantt.config.round_dnd_dates = false;
            gantt.config.sort = true;
            //gantt.config.order_branch = true;
            gantt.config.font_width_ratio = 7;
            gantt.config.drag_move = appHelper.isInteractiveEditingEnabled;
            gantt.config.drag_resize = appHelper.isInteractiveEditingEnabled;
            gantt.config.drag_links = false;
            gantt.config.drag_progress = false;
            gantt.config.server_utc = true;
            gantt.config.grid_width = 500;
            gantt.config.row_height = 23;
            gantt.config.task_height = 18;
            gantt.config.columns = [
                { name: "text", label: "Name", align: "left", tree: true, width: '*', template: this.ganttTaskNameTemplate, sort: function(a, b) { return plannerGlobals.ganttSortFn("nameSortValue", a, b); } },
                { name: "start_date", width: 70, label: "Start time", align: "left", sort: function(a, b) { return plannerGlobals.ganttSortFn("startDateSortValue", a, b); } },
                { name: "completeness", label: "", width: 45, align: "right", template: this.ganttTaskProgressTemplate, sort: function(a, b) { return plannerGlobals.ganttSortFn("progress", a, b); } },
                { name: "add", label: "", width: 25, sort: false },
		        { name: "buttons", label: "", width: 30, template: this.ganttTaskButtonsTemplate, sort: false }
            ];
            gantt.templates.task_class = function (s, e, task) {
                var css = [];
                css.push("task-color-" + task.id.replace("|", "-"));
                if (gantt.hasChild(task.id)) {
                    css.push("task-parent");
                }
                if (!task.$open && gantt.hasChild(task.id)) {
                    css.push("task-collapsed");
                }
                return css.join(" ");
            };
            gantt.templates.grid_row_class = function (s, e, task) {
                var css = [];
                css.push("gantt-no-add-button");
                if (task.inRiverItemType == "group") {
                    css.push("gantt-group-item");
                }
                if (gantt.hasChild(task.id)) {
                    css.push("task-parent");
                }
                if (!task.$open && gantt.hasChild(task.id)) {
                    css.push("task-collapsed");
                }
                return css.join(" ");
            };
            //gantt.templates.task_row_class = function (start, end, task) {
            //    if (task.inRiverItemType == "group") {
            //        return "gantt-group-item";
            //    }
            //    return "";
            //};
            gantt.templates.grid_folder = function (item) {
                if (item.entityType) {
                    return "<div class='gantt_tree_icon' style='background-image: url(../icon?id=" + item.entityType + ");'></div>";
                } else {
                    return "<div class='gantt_tree_icon'></div>";
                }
            };
            gantt.templates.grid_file = function (item) {
                return "<div class='gantt_tree_icon' style='background-image: url(../icon?id=" + item.entityType + ");'></div>";
            };
            //gantt.templates.grid_blank = function (item) {
            //    return "";
            //};
            //gantt.templates.task_text = function (start, end, task) {
            //    return task.barText ? task.barText : task.text;
            //};
            //gantt.templates.rightside_text = function (start, end, task) {
            //    if (task.type == gantt.config.types.milestone) {
            //        return task.barText ? task.barText : task.text;
            //    }
            //    return null;
            //};
            gantt.templates.leftside_text = function leftSideTextTemplate(start, end, task) {
                var text = task.barText ? task.barText : task.text;
                if (that.getTaskFitValue(task, text) === "left") {
                    return text;
                }
                return "";
            };
            gantt.templates.rightside_text = function rightSideTextTemplate(start, end, task) {
                //if (task.type == gantt.config.types.milestone) {
                //    return task.barText ? task.barText : task.text;
                //} else {
                var text = task.barText ? task.barText : task.text;
                if (that.getTaskFitValue(task, text) === "right") {
                    return text;
                }
                //}
                return "";
            };
            gantt.templates.task_text = function taskTextTemplate(start, end, task) {
                var text = task.barText ? task.barText : task.text;
                if (that.getTaskFitValue(task, text) === "center") {
                    return text;
                }
                return "";
            };
            gantt.templates.tooltip_text = function (start, end, task) {
                var s = "";
                if (task.tooltipInfo) {
                    _.each(task.tooltipInfo, function (v) {
                        s += "<p><b>" + v.Name + ":</b> " + v.Value + "</p>";
                    });
                }
                if (task.pictureUrl) {
                    s += "<img src='" + task.pictureUrl + "'><br>";
                }
                return s;
            };
            gantt.onEditTask = function (entityId, entityType) {
                that.onEditTask(entityId, entityType);
            }
            //gantt.attachEvent("onBeforeDataRender", function () {
            //    //gantt.sort("text", false, gantt.config.root_id, true);
            //});
            //gantt.attachEvent("onBeforeTaskChanged", function (id, e) {
            //    console.log("changed: " + id);
            //    return true;
            //});
            //gantt.attachEvent("onTaskSelected", function (id, e) {
            //    var entityId = gantt.getTask(id).entityId;
            //    if (entityId) {
            //        //this._parentView.$el.hide();
            //        window.app_router.navigate("campaign/" + entityId, { trigger: true });
            //    }
            //});
            //gantt.attachEvent("onAfterTaskAdd", function (id, task) {
            //    //console.log(JSON.stringify(task));
            //    return true;
            //});
            plannerGlobals.ganttEvents.push(gantt.attachEvent("onTaskDblClick", function (id, e) {
                if (!id) return false;

                e.stopPropagation();

                if (e.type == "dblclick") {
                var task = gantt.getTask(id);
                if (task.relatedEntityId) {
                    that.onEditTask(task.relatedEntityId, task.relatedEntityType);
                }
                else if (task.entityId) {
                    that.onEditTask(task.entityId, task.entityType);
                }
                }                
            }));
            plannerGlobals.ganttEvents.push(gantt.attachEvent("onAfterTaskUpdate", function (id, task) {
                that.collection.get(id).save({
                    StartDate: plannerGlobals.ganttDateToUtcString(task.start_date),
                    EndDate: plannerGlobals.ganttDateToUtcString(task.end_date),
                    
                }, {
                    wait: true,
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }));
            gantt.attachEvent("onTaskCreated", function (task) {
                var parentEntityId = null;
                if (task.parent) {
                    parentEntityId = gantt.getTask(task.parent).entityId;
                }
                that.onCreateTask(parentEntityId);
                return false;
            });
            gantt.attachEvent("onTaskOpened", function (task) {
                that.onTaskOpenedOrClosed(task);
            });
            gantt.attachEvent("onTaskClosed", function (task) {
                that.onTaskOpenedOrClosed(task);
            });
            //gantt.attachEvent("onDataRender", function (task) {
            //    console.log("onDataRender");
            //});
            gantt.attachEvent("onGanttRender", function (id, task) {
                //console.log("onGanttRender");
                // Super hack... update or add the today-marker (sometimes the marker is removed and sometimes not)
                // var marker = gantt.getMarker(window.plannerGlobals.todayMarker);
                //if (!marker)
                {
                    if (window.plannerGlobals.todayMarker) {
                        gantt.deleteMarker(window.plannerGlobals.todayMarker);
                    }

                    var today = new Date();
                    today.setHours(0);
                    today.setMinutes(0);
                    today.setSeconds(0);
                    today.setMilliseconds(0);
                    window.plannerGlobals.todayMarker = gantt.addMarker({
                        start_date: today,
                        css: "gantt-today-marker-line",
                        //text: "Today",
                        //title: "Today"
                    });
                }
                // } else {
                    //gantt.updateMarker(marker);
                //}

                plannerGlobals.setGanttTitle(plannerGlobals.currentViewStateModel.get("Name"));
                //plannerGlobals.getGanttSorting();

                if (plannerGlobals.ganttIsReadyForPrint) {
                    // Modify DOM to optimize for printing
                    $("#campaign-overview-dataview").addClass("campaign-overview-dataview-printing");
                    $("body > :not(#campaign-overview-dataview)").hide(); //hide all nodes directly under the body
                    $("#campaign-overview-dataview").appendTo("body");
                    $("#campaign-overview-dataview").addClass("campaign-overview-dataview-printing");

                    // Create a header area for title and date etc
                    $("#campaign-overview-dataview").attr("style", "margin-top: 30px !important; margin-bottom: 30px !imporant");
                    $("body").append("<div class='print-header'></div>");
                    $(".print-header").append("<h2 class='print-title'>&nbsp;</h2>");
                    if (plannerGlobals.currentViewStateModel.get("Name")) {
                        $(".print-title").text(plannerGlobals.currentViewStateModel.get("Name"));
                    }
                    $(".print-header").append("<h2 class='print-date'>" + moment().format('MMMM Do YYYY') + "</h2>");

                    // Misc print adjustments in layout
                    $(".row-icon-button").hide();
                    $(".gantt_grid_head_add").hide();
                    $(".gantt_ver_scroll").remove();
                    $(".gantt_hor_scroll").remove();

                    // Footer stuff...
                    //$("#campaign-overview-view-gantt-container").css("overflow", "hidden");
                    //$("#campaign-overview-view-gantt-container").css("height", $(".gantt_task_bg").height() + $(".gantt_task_scale").height() + 1);
                    //$(".print-date").css("margin-top", ($("#campaign-overview-view-gantt-container").height() + 30) + "px");

                    // Run init routine after image is loaded, use a trick to load an image in order to know when the rendering is finished
                    var imgEl = $("<img src=''>");
                    if ($(".gantt_last_cell").last().length > 0) {
                        $(".gantt_last_cell").last().append(imgEl);
                    } else {
                        $("body").append(imgEl);
                    }                    
                    imgEl.one("load", function (e) {
                        //console.log("trans img loaded");
                        //console.log(imgEl.width());
                        if (typeof wnvPdfConverter !== 'undefined') {
                            setTimeout(function () {
                                // give the browser some time to render everything
                                wnvPdfConverter.startConversion();
                            }, 5000);
                        }
                    });
                    imgEl.attr("src", "/images/transparent.gif?" + Math.round(Math.random()*10) + 1);
                }

                return true;
            });

            gantt.config.buttons_right = ["task-settings"];
            gantt.locale.labels["task-settings"] = "Edit";
            
            window.plannerGlobals.setScaleConfig(plannerGlobals.currentViewStateModel.get("ScaleMode"));

           // var date_to_str = gantt.date.date_to_str(gantt.config.task_date);
            //var today = new Date(2016, 11, 11);
            //gantt.addMarker({
            //    start_date: today,
            //    css: "today",
            //    text: "Today",
            //    //title: "Today: " + date_to_str(today)
            //});

            gantt.config.smart_rendering = !plannerGlobals.isPrinting; // Smart rendering will mess up pdf printing
            gantt.config.touch_tooltip = true; // Fix for tooltips in IE 11
            gantt.init(this.$el.find("#campaign-overview-view-gantt-container").get(0));

            gantt.addTaskLayer(function showHidden(task) {
                if (!task.$open && gantt.hasChild(task.id)) {
                    var subHeight = gantt.config.row_height - 5;
                    var el = document.createElement('div');
                    var sizes = gantt.getTaskPosition(task);

                    // Get all children descendant from this item (recursively)
                    var allChildren = [];
                    var getChildrenRecursively = function (taskId) {
                        var subTasks = gantt.getChildren(taskId);
                        allChildren = allChildren.concat(subTasks);
                        _.each(subTasks, function (t) {
                            getChildrenRecursively(t);
                        });
                    }
                    getChildrenRecursively(task.id);

                    for (var i = 0; i < allChildren.length; i++) {
                        var childTask = gantt.getTask(allChildren[i]);
                        if (childTask.unscheduled) continue;

                        var childSizes = gantt.getTaskPosition(childTask);
                        var childEl;
                        if (childTask.type == gantt.config.types.milestone) {
                            childEl = that.createInlinePreviewMilestone({
                                height: subHeight,
                                top: sizes.top,
                                left: childSizes.left - (subHeight / 2),
                                width: subHeight
                            }, "child-milestone-preview"); //child_el.innerHTML = childTask.barText ? childTask.barText : childTask.text;
                            el.appendChild(childEl);
                        } else {
                            childEl = that.createInlinePreviewTaskBox({
                                height: subHeight,
                                top: sizes.top,
                                left: childSizes.left,
                                width: childSizes.width
                            }, "child-task-preview gantt_task_line");
                            childEl.innerHTML = childTask.barText ? childTask.barText : childTask.text;
                            el.appendChild(childEl);
                        }

                    }
                    return el;
                }
                return false;
            });

            this.updateAllTasksInGantt();
        },
        taskModelToGanttTask: function (taskModel) {
            var task = {
                id: taskModel.get("Id"),
                text: taskModel.get("DisplayName"),
                entityId: taskModel.get("EntityId"), // for internal use
                entityType: taskModel.get("EntityType"), // for internal use
                relatedEntityId: taskModel.get("RelatedEntityId"), // for internal use
                relatedEntityType: taskModel.get("RelatedEntityType"), // for internal use
                barText: taskModel.get("BarText"),
                inRiverItemType: taskModel.get("ItemType")
            }
            if (!taskModel.get("StartDate") || !taskModel.get("EndDate")) {
                task.unscheduled = true;
                task.start_date = null;
                task.end_date = null;
                task.duration = null;
            }
            if (task.inRiverItemType == "group") {
                //task.groupSortValue = taskModel.get("SortValue");
                task.readonly = true;
            }
            else {
                task.nameSortValue = task.text;
                task.startDateSortValue = taskModel.get("SortValue"); // Must be set even though there is not start date set
                if (taskModel.get("StartDate")) {
                    task.startDateSortValue = taskModel.get("SortValue");
                    task.start_date = plannerGlobals.utcStringToGanttDate(taskModel.get("StartDate"));
                    if (task.inRiverItemType == "milestone") {
                        task.type = gantt.config.types.milestone;
                        task.unscheduled = false; // milestones only need a start date
                    } else if (taskModel.get("EndDate")) {
                        task.type = gantt.config.types.task;
                        task.duration = gantt.calculateDuration(plannerGlobals.utcStringToGanttDate(taskModel.get("StartDate")), plannerGlobals.utcStringToGanttDate(taskModel.get("EndDate")));
                        task.end_date = plannerGlobals.utcStringToGanttDate(taskModel.get("EndDate"));
                    } else {
                        task.type = gantt.config.types.task;
                        task.duration = 1;
                    }
                }
            }
            if (taskModel.get("Completeness")) {
                task.progress = taskModel.get("Completeness") / 100;
                task.completenessDisplayText = taskModel.get("Completeness") + "%";
            }
            var pictureUrl = taskModel.get("PictureUrl");
            if (pictureUrl && !/.*nopicture.*/i.test(pictureUrl)) { // ugly...
                task.pictureUrl = pictureUrl;
            }
            if (taskModel.get("TooltipInfo")) {
                task.tooltipInfo = taskModel.get("TooltipInfo");
            }

            task.open = true;
            task.parent = taskModel.get("ParentId") ? taskModel.get("ParentId") : 0;

            return task;
        },
        onTaskModelChange: function (taskModel, a, b) {
            window.plannerGlobals.fetchCollection(); // For now, always update the whole gantt collection to maintain data integrity (can be optimized)
            return;
            /*var that = this;
            if (this.collection.where({ EntityId: taskModel.get("EntityId") }).length > 1) {
                // This entity exists several times in the gantt so we have to do a full update to make sure all of them get updated
                window.plannerGlobals.fetchCollection();
                return;
            }

            if (taskModel.hasChanged("ParentId")) {
                // not sure this is rock solid...
                window.plannerGlobals.fetchCollection(); // the tree structure was affected by the change, so render everything
                return;
            }

            if (taskModel.get("ParentId") && !gantt.isTaskExists(taskModel.get("ParentId"))) {
                alert("error..."); // should never happen, in theory
            }
            var ganttTask = gantt.getTask(taskModel.get("Id"));
            var newGanttTask = this.taskModelToGanttTask(taskModel);
            _.extend(ganttTask, newGanttTask);

            this.updateGanttCss();
            gantt.refreshTask(ganttTask.id);*/
        },
        //onTaskModelRemove: function (taskModel) {
        //    if (gantt.isTaskExists(taskModel.get("Id"))) {
        //        gantt.deleteTask(taskModel.get("Id"));
        //        this.updateGanttCss();
        //    }
        //},
        updateAllTasksInGantt: function () {
            var that = this;
            gantt.clearAll();
            var collapsedItems = plannerGlobals.currentViewStateModel.get("CollapsedItems");
            var entityFilter = plannerGlobals.currentViewStateModel.get("EntityFilter");

            var tasks = { data: [] };
            this.collection.each(function (taskModel) {
                if (entityFilter && taskModel.get("EntityId") > 0 && taskModel.get("EntityType") == plannerGlobals.currentViewStateModel.get("Dimensions.0") && !_.contains(entityFilter, taskModel.get("EntityId"))) {
                    return;
                }
                var ganttTask = that.taskModelToGanttTask(taskModel);
                ganttTask.open = !_.contains(collapsedItems, taskModel.get("Id"));
                tasks.data.push(ganttTask);
            });

            // Remove empty groups as a result of the entity filter
            for (var i = 0; i < tasks.data.length;) {
                if (tasks.data[i].inRiverItemType == "group" && _.where(tasks.data, { parent: tasks.data[i].id }).length == 0) {
                    tasks.data.splice(i, 1);
                    i = 0;
                } else {
                    i++;
                }
            }

            gantt.parse(tasks);

            if (plannerGlobals.printSortOrder) {
                var tmp = plannerGlobals.printSortOrder.split(";");
                var columnName = tmp[0];
                var isDesc = tmp[1] === "asc" ? false : true;
                var ganttColumn = _.findWhere(gantt.config.columns, { name: columnName });
                gantt.sort(ganttColumn.sort, isDesc, null, true); // delegate to the columns own sort function
            }

            //gantt.addTask({
            //    id: 10,
            //    text: "Task #5",
            //    start_date: "02-09-2015",
            //    duration: 4
            //}, null, 1);

            //gantt.addTask({ "id": 23343434, "text": "Mediate milestone", "start_date": "14-04-2015", type: gantt.config.types.milestone, "progress": 0, "open": true });

            this.updateGanttCss();

            if (plannerGlobals.isPrinting && plannerGlobals.dataForPrintingIsFetched) {
                plannerGlobals.ganttIsReadyForPrint = true;
            }

            gantt.render();
        },
        updateGanttCss: function () {
            var taskCssRules = "";
            this.collection.each(function (taskModel) {
                if (taskModel.get("BarColor")) {
                    if (taskModel.get("ItemType") == "milestone") {
                        taskCssRules += ".task-color-" + taskModel.get("Id").replace("|", "-") + "{ background: " + taskModel.get("BarColor") + "!important; border: 1px solid " + taskModel.get("BarBorderColor") + "!important }\n";
                    } else {
                        taskCssRules += ".task-color-" + taskModel.get("Id").replace("|", "-") + " { background: " + taskModel.get("BarColor") + "; border: 1px solid " + taskModel.get("BarBorderColor") + " }\n";
                        if (taskModel.get("Completeness") >= 0) {
                            taskCssRules += ".task-color-" + taskModel.get("Id").replace("|", "-") + " .gantt_task_progress { background: " + taskModel.get("ProgressBarColor") + "; }\n";
                        }
                    }
                }
                if (taskModel.get("TextColor")) {
                    taskCssRules += ".task-color-" + taskModel.get("Id").replace("|", "-") + " .gantt_task_content { color: " + taskModel.get("TextColor") + "; }\n";
                }
            });
            if ($('head #gantt-task-styles').length == 0) {
                $("head").append("<style id='gantt-task-styles'></style>");
            }
            $("#gantt-task-styles").html(taskCssRules);
        },
        render: function () {
            var that = this;
            this.$el.html(this.template());

            /*
                        var tasks = {
                            data: [
                                {
                                    id: 1, text: "Project #2", start_date: "01-04-2015", duration: 18, order: 10,
                                    progress: 0.4, open: true
                                },
                                {
                                    id: 2, text: "Task #1", start_date: "02-04-2015", duration: 8, order: 10,
                                    progress: 0.6, parent: 1
                                },
                                {
                                    id: 3, text: "Task #2", start_date: "11-04-2015", duration: 8, order: 20,
                                    progress: 0.6, parent: 1
                                }
                            ],
                            links: [
                                { id: 1, source: 1, target: 2, type: "1" },
                                { id: 2, source: 2, target: 3, type: "0" },
                                { id: 3, source: 3, target: 4, type: "0" },
                                { id: 4, source: 2, target: 5, type: "2" },
                            ]
                        };
            
                        var demo_tasks = {
                            data: [
                                { "id": 1, "text": "Office itinerancy", open: false,  },
                                { "id": 2, "text": "Office facing", "start_date": "21-07-2014", "duration": "20", "parent": "1" },
                                { "id": 3, "text": "Furniture installation", "start_date": "21-07-2014", "duration": "5", "parent": "1" },
                                { "id": 4, "text": "The employee relocation", "start_date": "28-07-2014", "duration": "15", "parent": "1" },
                                { "id": 5, "text": "Interior office", "start_date": "28-07-2014", "duration": "15", "parent": "1" },
                                { "id": 6, "text": "Air conditioners installation", "start_date": "18-08-2014", "duration": "2", "parent": "1" },
                                { "id": 7, "text": "Workplaces preparation", "start_date": "20-08-2014", "duration": "2", "parent": "1" },
                                { "id": 8, "text": "Preparing workplaces for us", "start_date": "21-07-2014", "duration": "10", "parent": "1" },
                                { "id": 9, "text": "Workplaces importation", "start_date": "22-08-2014", "duration": "1", "parent": "1" },
                                { "id": 10, "text": "Analysis", open: false, },
                                { "id": 11, "text": "Documentation creation", "start_date": "25-08-2014", "duration": "14", "parent": "10" },
                                { "id": 12, "text": "Software design", "start_date": "25-08-2014", "duration": "10", "parent": "10" },
                                { "id": 13, "text": "Interface setup", "start_date": "12-09-2014", "duration": "1", "parent": "10" },
                                { "id": 14, "text": "Development", open: false, },
                                { "id": 15, "text": "Develop System", "start_date": "15-09-2014", "duration": "5", "parent": "14" },
                                { "id": 16, "text": "Integrate System", "start_date": "15-09-2014", "duration": "15", "parent": "14" },
                                { "id": 17, "text": "Test", "start_date": "06-10-2014", "duration": "1", "parent": "14" }
            
            
                            ],
                            links: [
                                { id: "1", source: "3", target: "4", type: "0" },
                                { id: "2", source: "3", target: "5", type: "0" },
                                { id: "3", source: "2", target: "6", type: "0" },
                                { id: "4", source: "4", target: "6", type: "0" },
                                { id: "5", source: "5", target: "6", type: "0" },
                                { id: "6", source: "6", target: "7", type: "0" },
                                { id: "7", source: "7", target: "9", type: "0" },
                                { id: "8", source: "8", target: "9", type: "0" },
                                { id: "9", source: "9", target: "10", type: "0" },
                                { id: "10", source: "9", target: "11", type: "0" },
                                { id: "11", source: "9", target: "12", type: "0" },
                                { id: "12", source: "11", target: "13", type: "0" },
                                { id: "13", source: "12", target: "13", type: "0" },
                                { id: "14", source: "13", target: "14", type: "0" },
                                { id: "15", source: "13", target: "15", type: "0" },
                                { id: "16", source: "15", target: "17", type: "0" },
                                { id: "17", source: "16", target: "17", type: "0" }
                            ]
                        };
                        */




            return this; // enable chained calls
        },
    });


    return plannerGanttComponentSubview;
});

