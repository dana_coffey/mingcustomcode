define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'views/configuration/entityTypeFieldSelectionTable',
  'text!templates/configuration/configurationTooltipsViewTemplate.html'

], function ($, _, backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    entityTypeFieldSelectionTable, configurationTooltipsViewTemplate) {

    var configurationTooltipsView = backbone.View.extend({
        template: _.template(configurationTooltipsViewTemplate),
        initialize: function () {
            window.appHelper.event_bus.off('planner-configuration-save');
            window.appHelper.event_bus.off('planner-configuration-undo');
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-save', this.onSaveAll);
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-undo', this.onUndoAll);
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {
        },
        onSaveAll: function () {
            // Update model with selection and save to server
            plannerGlobals.tooltipFieldTypesModel.clear({ silent: true });
            var data = {};
            _.each(this.selectionTable.rows, function (row) {
                data[row.id] = row.getSelection();
            });
            plannerGlobals.tooltipFieldTypesModel.save(data);
            this.selectionTable.updateUi();
            window.plannerGlobals.ganttDataCollection.reset(); // force reload of gantt data
            inRiverUtil.Notify("Configurations have been saved");
        },
        onUndoAll: function () {
            this.selectionTable.updateUi();
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            return this.selectionTable ? this.selectionTable.hasUnsavedChanges() : false;
        },
        render: function () {
            this.$el.html(this.template());
            this.selectionTable = new entityTypeFieldSelectionTable({
                model: plannerGlobals.tooltipFieldTypesModel
            });
            this.$el.children(0).append(this.selectionTable.$el.children(0));
            this.onUndoAll();
            return this; // enable chained calls
        }
    });

    return configurationTooltipsView;
});

