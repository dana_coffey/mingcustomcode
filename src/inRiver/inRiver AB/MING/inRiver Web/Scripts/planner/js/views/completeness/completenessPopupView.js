﻿define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/completeness/completenessDetailsPopupviewTemplate.html'

], function ($, _, Backbone, completenessDetailsPopupviewTemplate) {

    var completenessDetailsPopupView = Backbone.View.extend({
        template: _.template(completenessDetailsPopupviewTemplate),
        initialize: function (options) {
            this.model = options.model;

            this.render(); 
        },
        events: {

        },
        render: function () {
            this.$el.html(this.template({ data: this.model.toJSON() }));

            return this; // enable chained calls
        },
    });


    return completenessDetailsPopupView;
});

