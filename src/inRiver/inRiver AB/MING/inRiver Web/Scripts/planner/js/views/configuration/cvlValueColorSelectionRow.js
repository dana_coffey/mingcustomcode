define([
  'jquery',
  'underscore',
  'backbone',
  'colpick',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'text!templates/configuration/cvlValueColorSelectionRowTemplate.html'

], function ($, _, backbone, colpick, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    fieldModel, cvlValueColorSelectionRowTemplate) {

    var entityTypeFieldSelectionRow = backbone.View.extend({
        template: _.template(cvlValueColorSelectionRowTemplate),
        initialize: function () {
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {
        },
        onColorInputFocused: function (e) {
            // Bug in colpickShow(). Using click as a workaround.
            this.click();
        },
        onColorInputBlured: function (e) {
            $(this).parent().colpickHide();
        },
        getTabColor: function () {
            var color = this.tabColorPicker.find(".configuration-input-tab-color").val();
            return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color) ? color : null;
        },
        getTextColor: function () {
            var color = this.textColorPicker.find(".configuration-input-text-color").val();
            return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color) ? color : null;
        },
        onTextChanged: function () {
            var text = this.value.toLowerCase();
            if (text) {
                $(this).parent().parent().find(".set-default-color-button").show();
                text = "#" + text.split("").filter(function (char) { return /[0-9a-f]/.test(char); }).join("").substring(0, 6);
                if (this.value !== text) {
                    var selectionStart = this.selectionStart;
                    var selectionEnd = this.selectionEnd;
                    if (this.value.charAt(0) !== "#") {
                        selectionStart++;
                        selectionEnd++;
                    }
                    this.value = text;
                    this.selectionStart = selectionStart;
                    this.selectionEnd = selectionEnd;
                }
                if (text.length === 4 || text.length === 7) {
                    if (text.length === 4) {
                        text = "#" + text.charAt(1) + text.charAt(1) + text.charAt(2) + text.charAt(2) + text.charAt(3) + text.charAt(3);
                    }
                    $(this).parent().colpickSetColor(text);
                }
            } else {
                $(this).parent().parent().find(".set-default-color-button").hide();
                $(this).parent().colpickSetColor($(this).parent()[0].defaultColor);
            }
        },
        onColorInputKeyPress: function (e) {
            var text = this.value.substring(0, this.selectionStart) + String.fromCharCode(e.which) + this.value.substring(this.selectionEnd);
            return /^#?[0-9A-F]*$/i.test(text) && text.length < 8;
        },
        getDefaultColor: function (attributeName) {
            var isMilestone = $.inArray(this.options.entityTypeId, appHelper.milestoneEntityTypes) > -1;
            return attributeName === "tabColor" ? isMilestone ? "#d33daf" : "#3db9d3" : "#ffffff";
        },
        createColorCtl: function (el, attributeName, attributeValue) {
            var that = this;
            var isTabColor = attributeName === "tabColor";
            var color = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(attributeValue) ? attributeValue : this.getDefaultColor(attributeName);
            if (color.length === 4) {
                color = "#" + color.charAt(1) + color.charAt(1) + color.charAt(2) + color.charAt(2) + color.charAt(3) + color.charAt(3);
            }
            el[0].defaultColor = this.getDefaultColor(attributeName);
            el.colpick({
                color: color,
                layout: "hex",
                submit: 0,
                onChange: function (hsb, hex, rgb, elem, bySetColor) {
                    var input = el.children(0);
                    input.css("border-color", "#" + hex);
                    $(el).closest(".cvlvalue-color-selector-row").find(".color-example").css(isTabColor ? "background-color" : "color", "#" + hex);
                    if (!bySetColor) {
                        input.val("#" + hex);
                        $(el).parent().find(".set-default-color-button").show();
                    }
                    that.options.parent.onColorsChanged.call(that.options.parent, that.options.cvlValue.get("Key"), that.getTabColor(), that.getTextColor());
                }
            });

            el.children(0).val(attributeValue).css("border-color", color);
            $(el).closest(".cvlvalue-color-selector-row").find(".color-example").css(isTabColor ? "background-color" : "color", color);
            if (el.children(0).val()) {
                el.parent().find(".set-default-color-button").show();
            } else {
                el.parent().find(".set-default-color-button").hide();
            }
        },
        onClickDefaultColorButton: function() {
            $(this).parent().find(".color-input").val("");
            $(this).parent().find(".configuration-color-pick-color").colpickSetColor($(this).parent().find(".configuration-color-pick-color")[0].defaultColor);
            $(this).hide();
        },
        render: function () {
            this.$el.html(this.template());
            this.$el.find(".cvlValue").text(this.options.cvlValue.get("DisplayValue"));
            this.tabColorPicker = this.$el.find(".configuration-color-pick-tab-color");
            this.createColorCtl(this.tabColorPicker, "tabColor", this.options.tabColor);
            this.textColorPicker = this.$el.find(".configuration-color-pick-text-color");
            this.createColorCtl(this.textColorPicker, "textColor", this.options.textColor);
            this.$el.find(".color-input").keypress(this.onColorInputKeyPress).on("change paste keyup", this.onTextChanged).on("focus", this.onColorInputFocused).on("blur", this.onColorInputBlured);
            this.$el.find(".set-default-color-button").click(this.onClickDefaultColorButton);
            this.example = this.$el.find(".color-example");
            return this; // enable chained calls
        }
    });

    return entityTypeFieldSelectionRow;
});

