﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'sharedjs/misc/permissionUtil',
  'models/task/taskModel',
  'views/task/taskViewDetailsSubView',
  'views/task/taskViewContentSubView',
  'views/completeness/completenessPopupView',
  'sharedjs/views/entityview/entityDetailSettingView',
     'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/entityView/entityViewTemplate.html'

], function ($, _, Backbone, inRiverUtil, modalPopup, permissionUtil, taskModel, taskViewDetailsSubView, taskViewContentSubView, completenessPopupView, entityDetailSettingView, entityDetailSettingWrapperView, entityViewTemplate) {

    var taskView = Backbone.View.extend({
        // This view operates without a collection
        template: _.template(entityViewTemplate),
        initialize: function (options) {
            this._belongsToLinkId = options.linkId;
            if (this._belongsToLinkId == null) {
                this._belongsToLinkId = 0;
            }
            var isNewModel = options.id == "0";

            var tmpModel = new taskModel({ Id: isNewModel ? 0 : options.id }); // id 0 means new object 
            tmpModel.urlRoot = "../../api/task/" + options.id;

            var that = this;
            tmpModel.fetch({
                success: function () {
                    if (isNewModel) tmpModel.id = null; // this is a trick to force backbone to think it's a new object even though it's created on the server, and therefor use POST when saving it for the first time
                    that.model = tmpModel;
                    that.render();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        events: {
            //"change input": "onContentChanged"
            "click #edit-button": "onEdit",
            "click #progress-rect-percentage": "onShowCompletnessDetails",
            "click #tab-details": "onChangeTab",
            "click #tab-content": "onChangeTab",
            "click #delete-button": "onEntityDelete"
        },
        onEntityDelete: function () {

            inRiverUtil.deleteEntity(this);
            this.listenTo(appHelper.event_bus, 'entitydeleted', this.afterEntityDelete);

            //var deleteModel = new taskModel({ Id: this.model.id }); // id 0 means new object
            //deleteModel.urlRoot = "../../api/task/" + this.model.id;
            //deleteModel.fetch({
            //    success: function () {
                    
            //    }
            //});
        },
        afterEntityDelete: function (id) {
            history.back();
        },
        onEdit: function (e) {
            var modal = new modalPopup();

            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });

        },
        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.model.id, $(e.currentTarget));
        },

        createContentTabView: function () {
            var contentSubView = new taskViewContentSubView({
                taskId: this.model.get("Id")
            });
            return contentSubView;
        },
        createDetailsTabView: function () {
            var contentSubView = new taskViewDetailsSubView({
                model: this.model
            });
            return contentSubView;
        },
        onChangeTab: function (e) {
            appSession.plannerEntityViewTabId = e.currentTarget.id; // remember default tab
            this.switchTabView(appSession.plannerEntityViewTabId);
            this.render();
        },
        switchTabView: function (idOfTab) {
            if (this._currentTabView) {
                this._currentTabView.close();
            }

            if (idOfTab == "tab-details") {
                this._currentTabView = this.createDetailsTabView();
            }
            else if (idOfTab == "tab-content") {
                this._currentTabView = this.createContentTabView();
            }
            else if (idOfTab == "tab-activities") {
                this._currentTabView = this.createActivitiesTabView();
            } else {
                this._currentTabView = this.createDetailsTabView();
            }
        },
        indicateActiveTab: function () {
            this.$el.find("#tab-details").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-content").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-activities").removeClass("control-row-tab-button-active");

            this.$el.find("#" + appSession.plannerEntityViewTabId).addClass("control-row-tab-button-active");
        },
        taskUpdated: function (id) {
            if (id == this.model.id) {
                var self = this;
                this.model.fetch(
                {
                    success: function () { self.render(); },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }

        },
        drawCompleteness: function (percent) {
            this.$el.find('.percent').html(Math.round(percent) + '%');

            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },
        render: function () {
            var that = this;
            this.$el.html(this.template({ data: this.model.toJSON(), userName: window.appHelper.userName, icon: this.model.get("EntityType") }));
            if (!this._currentTabView) {
                this.switchTabView(appSession.plannerEntityViewTabId);
            }

            this.$el.find("#tab-activities").hide();
            this.$el.find("#entity-view-tabview-container").append(this._currentTabView.render().el);
            this.indicateActiveTab();

            this.$el.find("#entity-view-back-button").hide();
            this.$el.find("#add-task-button").hide();

            this.listenTo(appHelper.event_bus, 'entityupdated', this.taskUpdated);
            this.drawCompleteness(this.model.attributes.Completeness);
            permissionUtil.CheckPermissionForElement("UpdateEntity", this.$el.find("#edit-button"));
            permissionUtil.CheckPermissionForElement("DeleteEntity", this.$el.find("#delete-button"));


            this.$el.find("#clone-button").hide();

            return this; // enable chained calls
        },
    });

    return taskView;
});

