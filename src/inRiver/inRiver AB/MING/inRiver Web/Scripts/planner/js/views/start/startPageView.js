define([
  'jquery',
  'underscore',
  'backbone',
  'jquery-ui',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/collections/entities/starredEntityCollection',
  'sharedjs/collections/tasks/tasksCollection',
  'sharedjs/views/start/startPageTaskListView',
  'sharedjs/views/entityview/entityDetailSettingView',
  'text!templates/start/startpageTemplate.html',
  'text!templates/start/startpageCampaignCardTemplate.html',
  'text!templates/start/startpageActivityCardTemplate.html'
], function ($, _, Backbone, jqueryui,inRiverUtil,  starredEntitiesCollection, tasksCollection, startPageTaskListView, entityDetailSettingView, startpageTemplate, startpageCampaignCardTemplate, startpageActivityCardTemplate) {

    var startPageView = Backbone.View.extend({
        campaignTemplate: _.template(startpageCampaignCardTemplate),
        activityTemplate: _.template(startpageActivityCardTemplate),
        initialize: function (data) {
            var self = this;

            this.ongoingCollection = new Backbone.Collection(null, { url: "/api/planner/ongoing", model: Backbone.Model.extend({ idAttribute: "Id", toString: function () { return this.get("Id"); } }) });

            self.hasTasks = false;
            self.hasTaskAssignedGroupTaskField = false;

            this.undelegateEvents();
            $.get("/api/tools/hasentitytype/Task").done(function (result) {
                if (result) {
                    self.hasTasks = true;
                }

                $.get("/api/tools/hasField/TaskAssignedGroupTask").done(function (fieldExists) {
                    if (fieldExists) {
                        self.hasTaskAssignedGroupTaskField = true;
                    }

                    self.ongoingCollection.fetch({
                        data: $.param({ filterData: null, selectionType: "ongoing" }),
                    success: function () {
                        self.starred = new starredEntitiesCollection();
                        self.starred.fetch({
                            data: $.param({ entitytype: appHelper.plannerEntityTypes }),
                            success: function () {
                                self.render();
                            },
                            error: function (model, response) {
                                inRiverUtil.OnErrors(model, response);
                            }
                        });
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
                });
                
            });
        },
        events: {
            "click #showAllStars": "showAllStars",
            "click #createNewTask": "createNewTask",
            "click #createEntity": "createEntity"
        },
        showAllStars: function () {
            var self = this;

            $.post("/api/starredentity/getidlist").done(function (result) {

                var entities = result;
                $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                    self.goTo("workarea?title=My Starred Entities&compressedEntities=" + compressed);
                });
            });
        },
       
        render: function () {
            var self = this;
            this.$el.html(startpageTemplate);

            /* Ongoing Campaigns */

            if (this.ongoingCollection.length == 0) {
                this.$el.find("#ongoing").hide();
                this.$el.find("#noongoing").show();
            } else {
                this.$el.find("#noongoing").hide();
                this.$el.find("#ongoing").show();

                this.ongoingCollection.each(function(m) {
                    self.$el.find("#ongoing-list").append(self.campaignTemplate({ model: m.toJSON() }));
                });
            }


            /* Starred Campaigns */
            if (this.starred.length == 0) {
                this.$el.find("#starred").hide();
                this.$el.find("#nostarred").show();
            } else {
                this.$el.find("#starred").show();
                this.$el.find("#nostarred").hide();

                this.starred.each(function(m) {
                    self.$el.find("#starred-list").append(self.campaignTemplate({ model: m.toJSON() }));
                });
            }

            /* Tasks */
            /* My Tasks */
            if (this.hasTasks) {
                
                var mytask = new startPageTaskListView({ title: 'My Tasks', tasktype: "mytask" });
                $("#mytask .white-box").append(mytask.$el);
                var createdtask = new startPageTaskListView({ title: 'My Created Tasks', tasktype: "createdtask" });
                $("#createdtask .white-box").append(createdtask.$el);
               
            } else {
                this.$el.find("#mytask").hide();
                this.$el.find("#createdtask").hide();
                this.$el.find("#createNewTask").hide();
            }

            if (this.hasTaskAssignedGroupTaskField) {
                var groupsTasks = new startPageTaskListView({ title: 'My Groups Tasks', tasktype: "groupstasks" });
                $("#groupstasks .white-box").append(groupsTasks.$el);
            }
            else {
                this.$el.find("#groupstasks").hide();
            }

            showMoreLinks('#ongoing-campaigns-list', 3);
            showMoreLinks('#starred-campaigns-list', 3);
            showMoreLinks('#week-activities-list', 2);

            function showMoreLinks(id, startVisibleNumber) {
                var moreVisibleNumber = 100;
                var $more = $('<div id="showMoreLink"><i class="fa fa-caret-down"></i> Show more</div>');
                var $less = $('<div id="showMoreLink"><i class="fa fa-caret-up"></i> Show less</div>');
                if ($(id + ' li').length > startVisibleNumber) {
                    $(id + ' li').slice(startVisibleNumber).hide();
                } else {
                    $more.hide();
                }

                $more.click(function () {
                    $(id + ' li:hidden').slice(0, moreVisibleNumber).slideToggle();
                    if ($(id + ' li:hidden').length == 0) {
                        $more.hide();
                        $less.show();
                        $(id).after($less);
                    }
                });

                $less.click(function () {
                    $(id + ' li').slice(startVisibleNumber).slideToggle();
                    $less.hide();
                    $more.show();
                });

                $(id).after($more);
            }
        }
    });

    return startPageView;
});

