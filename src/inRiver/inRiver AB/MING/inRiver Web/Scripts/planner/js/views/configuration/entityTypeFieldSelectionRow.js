define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'sharedjs/models/field/fieldModel',
  'text!templates/configuration/entityTypeFieldSelectionRowTemplate.html'

], function ($, _, backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    fieldModel, entityTypeFieldSelectionRowTemplate) {

    var entityTypeFieldSelectionRow = backbone.View.extend({
        template: _.template(entityTypeFieldSelectionRowTemplate),
        initialize: function (options) {
            this.entityType = options.entityType;
            this.selection = [];
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {
        },
        onSelectionChanged: function () {
            window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            var result = false;
            if (this.optionsLoaded) {
                var currentSelection = this.getSelection();
                if (currentSelection.length === this.selection.length) {
                    _.each(this.selection, function (selectedField) {
                        if (!_.contains(currentSelection, selectedField)) {
                            result = true;
                        }
                    });
                } else {
                    result = true;
                }
            }

            return result;
        },
        getSelection: function () {
            var result = this.select.val();
            return result ? result : [];
        },
        setSelection: function (selection) {
            this.shouldSelectAll = false;
            this.selection = selection ? selection : [];
            this.updateSelection();
        },
        selectAll: function() {
            this.shouldSelectAll = true;
            this.updateSelection();
        },
        updateSelection: function () {
            if (this.optionsLoaded) {
                if (this.shouldSelectAll) {
                    this.select.find("option").prop("selected", true);
                } else {
                    this.select.val(this.selection);
                }
                this.selection = this.getSelection();
                this.select.multipleSelect("refresh");
                window.appHelper.event_bus.trigger('planner-configuration-unsaved-changed');
            }
        },
        render: function () {
            var self = this;
            this.$el.html(this.template());
            this.$el.find("#entity-type").text(this.options.entityType.Name);
            this.select = this.$el.find("#field-types-select");
            var fieldTypesCollection = new backbone.Collection(null, { url: "/api/fieldtype/" + this.options.id, model: backbone.Model.extend({ idAttribute: "Id" }) });
            fieldTypesCollection.fetch({
                reset: true,
                success: function (c) {
                    fieldTypesCollection.each(function (m) {
                        if (m.get("hidden") === true) {
                            return; // skip hidden fields
                        } 
                        var id = m.get("id");
                        var displayname = m.get("displayname");
                        var categoryname = m.get("categoryname");
                        var optgroup = self.select.find("optgroup[label='" + categoryname + "']");
                        if (optgroup.length === 0) {
                            optgroup = $("<optgroup></optgroup>")
                                .attr("label", categoryname);
                            self.select.append(optgroup);
                        }
                        var option = $("<option></option>")
                            .attr("value", id)
                            .text(displayname);
                        optgroup.append(option);
                    });
                    self.select.change(self.onSelectionChanged);
                    self.select.multipleSelect({
                        styler: function(value) {
                            return "margin-left:15px;";
                        }
                    });
                    self.optionsLoaded = true;
                    self.updateSelection();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
            return this; // enable chained calls
        }
    });

    return entityTypeFieldSelectionRow;
});

