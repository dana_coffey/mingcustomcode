﻿define([
    'jquery',
    'underscore',
    'backbone',
    'sharedjs/models/completeness/completenessModel',
    'sharedjs/collections/relations/relationCollection',
    'sharedjs/collections/relations/relationTypeCollection',
    'sharedjs/collections/entities/entityTableCollection',
    'modalPopup',
    'sharedjs/misc/inRiverUtil',
    'sharedjs/misc/permissionUtil',
    'sharedjs/models/relation/relationModel',
    'sharedjs/models/entity/entityModel',
    'sharedjs/models/linkrule/linkRuleModel',
    'sharedjs/views/entityview/entityDetailSettingView',
    'sharedjs/views/entityview/entityDetailSettingWrapperView',
    'sharedjs/views/completeness/completenessPopupView',
    'sharedjs/views/contextmenu/contextMenuView',
    'sharedjs/views/configureLinkRule/configureLinkRuleView',
    'sharedjs/views/entityview/entityTableView',
    'views/relation/relationCardView',
    'text!sharedtemplates/relation/relationTemplate.html',
    'text!sharedtemplates/relation/relationTypeGroupTemplate.html',
    'text!sharedtemplates/relation/relationTypeLinkEntityGroupTemplate.html',
    'text!sharedtemplates/relation/relationTopMenuTemplate.html'
], function ($, _, backbone, completenessModel, relationCollection, relationTypeCollection, entityTableCollection, modalPopup, inRiverUtil, permissionUtil, relationModel, entityModel, linkRuleModel, entityDetailSettingView, entityDetailSettingWrapperView, completenessPopupView, contextMenuView, configureLinkRuleView, entityTableView, relationCardView, relationTemplate, relationTypeGroupTemplate, relationTypeLinkEntityGroupTemplate, relationTopMenuTemplate) {

    var relationView = backbone.View.extend({
        initialize: function (options) {
            var that = this;
            this.id = options.id;
            this.direction = options.direction;
            this.entityTypeId = options.type;
            this.excludeLinks = options.excludeLinks;

            this.mode = "card";
            this.includeFields = false;

            if (window.appSession.relationViewMode != null) {
                this.mode = window.appSession.relationViewMode;
            }

            if (this.direction == "outbound") {
                var restfulEntity = new entityModel({
                    Id: this.id
                });

                restfulEntity.fetch({
                    success: function (model, response) {
                        that.model = model;
                    },
                    error: function (errorModel, errorMessage) {
                    }
                });
            }

            this.relationTypes = new relationTypeCollection([], { direction: options.direction, entityTypeId: options.type });
            this.relations = new relationCollection([], { entityId: options.id, direction: options.direction });

            this.relationTypes.fetch(
            {
                success: function () {
                    that.relations.fetch({
                        success: function () {
                            that.render();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    }
                    );
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });

            this.droppable = permissionUtil.CheckPermission("AddLink");
            this.sortable = permissionUtil.CheckPermission("UpdateLink");

            this.relations.on('change', this.render, this);

            this.$el.html("<div id=\"loading\"><img src=\"/Images/nine-squares-32x32.gif\" />   <div><h2>Loading data</h2></div></div>");

            if (!inRiverUtil.isEventListenedTo('entityupdated-relation-view')) {
                //This should be triggered if the linkEntity is updated in relationView. Because then we will update the displayname and description of the linkEntity
                this.listenTo(window.appHelper.event_bus, 'entityupdated-relation-view', this.onEntityUpdated);
            }

            this.listenTo(window.appHelper.event_bus, 'entitydeleted', this.onEntityDeleted);
            this.listenTo(window.appHelper.event_bus, 'relationremoved', this.onRelationRemoved);

        },
        events: {
            "click .relation-wrap": "onCardClick",
            "click #edit-link-button": "onEditLink",
            "click #addAndLink": "onAddAndLinkEntity",
            "click .add-entity-withentity-button": "onAddAndLinkEntityWithLinkEntity",
            "click #relation-completeness-text": "onShowCompletnessDetails",
            "click #open-in-workarea": "onOpenWorkarea",
            "click #card-view": "onToggleView",
            "click #table-view": "onToggleView",
            "click #include-fields": "onIncludeFields",
            "click #configure-rule": "onConfigureAutomaticRule",    
        },
        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = e.currentTarget.find("#card-tools-container");
            $box.toggle(200);
        },
        onOpenWorkarea: function (e) {
            e.stopPropagation();
            var entities = $(e.currentTarget).closest(".relation-section-wrap").find(".card-wrap").map(function () {
                return this.id;
            }).get().join(",");
            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;
            $.post("/api/tools/compress", { '': entities }).done(function (compressed) {
                backbone.history.loadUrl("workareasearch?title=" + relationType + "&compressedEntities=" + compressed);
                if ($("#workarea-header-label").hasClass("workarea-header-label-vertical")) {
                    $("#workarea-header-toggle-button").click();
                }
            });
        },
        onAddAndLinkEntity: function (e) {
            e.stopPropagation();

            var entitytype = $(e.currentTarget).closest(".relation-section-wrap")[0].attributes["entitytypeid"];
            if (entitytype == null) {
                entitytype = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].attributes["entitytypeid"];
            }
            if (entitytype == null) {
                return;
            }
            var entitytypeid = entitytype.value;

            var relationType = $(e.target).closest(".relation-section-wrap")[0].id;

            var linkentitytypeid = "";
            var linkentitytypeidObj = $(e.target).closest(".relation-section-wrap")[0].attributes["linkentitytypeid"];
            if (linkentitytypeidObj) {

                linkentitytypeid = linkentitytypeidObj.value;
            }

            var linkentityid = "";

            if ($(event.target).closest(".relation-sub-section-wrap").length != 0) {
                linkentityid = $(event.target).closest(".relation-sub-section-wrap")[0].id;
            }

            var direction = this.direction;
            var sourceId;
            var targetId;
            if (direction == "outbound") {
                sourceId = this.id;
                targetId = -1;
            } else {
                sourceId = -1;
                targetId = this.id;
            }

            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = sourceId;
            this.targetIdToCreate = targetId;

            this.entityTypeIdToCreate = entitytypeid;
            this.linkentitytypeidToCreate = linkentitytypeid;
            this.linkentityidToCreate = linkentityid;

            this.checkLinkRuleOrOpenAddLinkDialog(direction);
        },
        completeAddAndLinkEntity: function (model) {
            var that = this;

            this.stopListening(window.appHelper.event_bus, 'entitycreated');

            if (this.direction == "outbound") {
                this.targetIdToCreate = model.attributes["Id"];
            } else {
                this.sourceIdToCreate = model.attributes["Id"];
            }

            if (this.linkentitytypeidToCreate && this.linkentityidToCreate == "") {
                this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeLinkCreatation);

                entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                    id: 0,
                    entityTypeId: this.linkentitytypeidToCreate,
                    showInWorkareaOnSave: false
                });
            } else {

                var newModel = new relationModel({ LinkType: this.relationTypeToCreate, SourceId: this.sourceIdToCreate, TargetId: this.targetIdToCreate, LinkEntityId: this.linkentityidToCreate });
                newModel.save([], {
                    success: function (result) {
                        that.relations.add(result);

                        var relationCard = new relationCardView({ relation: result, direction: that.direction });
                        var findCriteria = "#" + (that.linkentityidToCreate ? that.linkentityidToCreate + " #relation-sub-section-container" : that.relationTypeToCreate + " #relation-section-container");
                        that.$el.find(findCriteria).append(relationCard.$el);
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onAddAndLinkEntityWithLinkEntity: function (e) {
            e.stopPropagation();

            var entitytypeid = e.currentTarget.id.replace("EntityType-", "");

            var relationType = $(event.target).closest(".relation-section-wrap")[0].id;

            var linkentityid = $(event.target).closest(".relation-sub-section-wrap")[0].id;

            var direction = this.direction;
            var sourceId;
            var targetId;
            if (direction == "outbound") {
                sourceId = this.id;
                targetId = -1;
            } else {
                sourceId = -1;
                targetId = this.id;
            }

            this.relationTypeToCreate = relationType;
            this.sourceIdToCreate = sourceId;
            this.targetIdToCreate = targetId;
            this.entityTypeIdToCreate = entitytypeid;
            this.linkentityidToCreate = linkentityid;

            this.checkLinkRuleOrOpenAddLinkDialog(direction);
        },
        checkLinkRuleOrOpenAddLinkDialog: function (direction) {
            var self = this;
            if (direction == "outbound") {
                var linkRule = new linkRuleModel({ entityid: this.id });
                linkRule.fetch({
                    success: function (model, response) {
                        if (response == null) {
                            self.stopListening(window.appHelper.event_bus, 'entitycreated');
                            self.listenTo(window.appHelper.event_bus, 'entitycreated', self.completeAddAndLinkEntity);

                            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                                id: 0,
                                entityTypeId: self.entityTypeIdToCreate,
                                showInWorkareaOnSave: false
                            });
                        } else {
                            inRiverUtil.NotifyError("Add link where \"self configure linkrule\" exists", "You are not able to add link for: " + self.relationTypeToCreate + ". Because there exists \"self configure linkrule\". Please remove the linkrule and try again.");
                        }
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    },
                    url: '/api/linkrule/' + this.id + '/' + this.relationTypeToCreate
                });
            } else {

            this.stopListening(window.appHelper.event_bus, 'entitycreated');
            this.listenTo(window.appHelper.event_bus, 'entitycreated', this.completeAddAndLinkEntity);

            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                    entityTypeId: this.entityTypeIdToCreate,
                showInWorkareaOnSave: false
            });
            }
        },
        onEditLink: function (e) {
            e.stopPropagation();
            var linkEntityId = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].id;
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: linkEntityId,
                isLinkEntityEdit: true
            });
        },
        onCardClick: function (e) {
            var id = e.currentTarget.id;
            this.goTo("campaign/" + id);
        },

        onClose: function () {
        },
        onChangeSortOrder: function (event, ui) {
            var id = $(ui.item[0]).find(".card-wrap")[0].id;
            var position = 0;
            var temp = $(ui.item[0]).find("#contextmenu > div");
            var relationId = temp[0].id;

            var parent = $(this).closest("div#relation-section-container");

            parent.find(".card-wrap").each(function (i) {
                if ($(this).attr('id') == id) {
                    position = i;
                    if (position < 0) {
                        position = 0;
                    }
                }
            });

            // API and save order
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "/api/tools/updaterelationsortorder/" + relationId + "/" + position, true);
            xmlhttp.send();
        },
        completeLinkCreatation: function (model) {
            var that = this;

            var linkentityid = model.attributes.Id;
            var linktype = this.relationTypeToCreate;

            var newModel = new relationModel({ LinkType: this.relationTypeToCreate, SourceId: this.sourceIdToCreate, TargetId: this.targetIdToCreate, LinkEntityId: linkentityid });
            newModel.save([], {
                success: function (xmodel) {
                    that.relations.add(xmodel);
                    var entitytype = xmodel.attributes.TargetEntity.EntityType;
                    if (that.direction == "inbound") {
                        entitytype = xmodel.attributes.SourceEntity.EntityType;
                    }

                    var subsectionEl = $(_.template(relationTypeLinkEntityGroupTemplate, { model: xmodel.toJSON(), entitytypeid: entitytype }));
                    var tools = new Array();
                    tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                    that.pushAddAndLinkTool(tools);
                    tools.push({ name: "open-in-workarea", title: "Open in workarea", icon: "fa-folder-open fa ", text: "Open in Workarea" });

                    subsectionEl.find("#contextmenu").html(new contextMenuView({
                        id: xmodel.id,
                        tools: tools
                    }).$el);


                    $("#" + linktype + " #relation-section-container").append(subsectionEl);

                    var id = "#" + linkentityid.toString().replace(" ", "-");
                    var $heading = $(id + " .relation-sub-section-title");
                    var $box = $(id).find("#relation-sub-section-container");

                    var $showTask = $(id + '-show i');

                    $heading.click(function () {
                        $box.toggle(200);
                        $showTask.toggle();
                    });

                    var relationCard = new relationCardView({ relation: xmodel, direction: that.direction });
                    $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                },
                error: function () {
                    inRiverUtil.OnErrors(model, response);
                }
            });

        },
        completeMultipleLinkCreatation: function (model) {
            var that = this;

            if (this.LinksCollection.length > 0) {
                _.each(this.LinksCollection.models, function (link) {

                    var linkentityid = model.attributes.Id;
                    var linktype = link.relationTypeToCreate;

                    var newModel = new relationModel({ LinkType: link.relationTypeToCreate, SourceId: link.sourceIdToCreate, TargetId: link.targetIdToCreate, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (xmodel) {

                            var entitytype = xmodel.attributes.TargetEntity.EntityType;
                            if (that.direction == "inbound") {
                                entitytype = xmodel.attributes.SourceEntity.EntityType;
                            }

                            var subsectionEl = $(_.template(relationTypeLinkEntityGroupTemplate, { model: xmodel.toJSON(), entitytypeid: entitytype }));

                            if (that.$el.find("#" + linkentityid + " #relation-sub-section-container").length == 0) {
                                var tools = new Array();
                                tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                                that.pushAddAndLinkTool(tools);
                                tools.push({ name: "open-in-workarea", title: "Open in workarea", icon: "fa-folder-open fa ", text: "Open in Workarea" });

                                subsectionEl.find("#contextmenu").html(new contextMenuView({
                                    id: xmodel.id,
                                    tools: tools
                                }).$el);

                                that.$el.find("#" + linktype + " #relation-section-container").append(subsectionEl);

                                var idx = "#" + linkentityid.toString().replace(" ", "-");
                                var $heading = $(idx + " .relation-sub-section-title");
                                var $box = $(idx).find("#relation-sub-section-container");

                                var $showTask = $(idx + '-show i');

                                $heading.click(function () {
                                    $box.toggle(200);
                                    $showTask.toggle();
                                });
                                var id;

                                if (that.direction == "outbound") {
                                    id = xmodel.attributes["TargetEntityTypeId"];
                                } else {
                                    id = xmodel.attributes["SourceEntityTypeId"];
                                }
                                if (that.droppable) {
                                    subsectionEl.find(idx).droppable({
                                        greedy: true,
                                        accept: function (draggable) {

                                            var typeObj = $(draggable).closest(".card-section-wrap");
                                            var linktype = "";
                                            if (typeObj[0]) {
                                                linktype = typeObj[0].id;
                                            }
                                            if (linktype != id) {
                                                return false;
                                            }

                                            var drager = draggable[0].id.replace("card-", "");
                                            //kolla om dragger finns.
                                            if (drager == null || drager == "") {
                                                return false;
                                            }

                                            var x = $(this).find("#" + drager);
                                            if (x.length > 0) {
                                                return false;
                                            }
                                            return true;
                                        },
                                        activeClass: "relation-sub-section-container-active",
                                        hoverClass: "relation-dropable-hover",
                                        drop:
                                            function (event, ui) {
                                                that.addLinkedRelation(event, ui, that, linktype);
                                            }
                                    });
                                }
                            }

                            var relationCard = new relationCardView({ relation: xmodel, direction: that.direction });
                            that.$el.find("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                        },
                        error: function () {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                });

                this.LinksCollection.reset();
            }
        },
        createLineEntityAndAddRelation: function (relationType, sourceId, targetId, entitytypeid, that) {
            that.relationTypeToCreate = relationType;
            that.sourceIdToCreate = sourceId;
            that.targetIdToCreate = targetId;

            that.entityTypeIdToCreate = entitytypeid;
            that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeLinkCreatation);


            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: entitytypeid,
                showInWorkareaOnSave: false
            });

        },
        addRelation: function (event, ui, self, entitytype) {
            event.stopPropagation();


            var linkentitytypeid = "";
            if (event.target.attributes["linkentitytypeid"]) {
                linkentitytypeid = event.target.attributes["linkentitytypeid"];
            };

            var that = this;
            var addedToEntity = that.id;
            var relationType = event.target.id;
            var tableView = entitytype == null;

            var entitys = [];

            //1. Vi tar reda på idn på de som ska länkas.
            //Även typ?
            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {


                var workareaView = window.appHelper.workAreaHandler.getActiveWorkarea();

                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                //var cards = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");
                if (!workareaView) {
                    return;
                }


                //var cards = $(workarea).find(".selected-card .fa-check-square-o").closest("div.card-wrap-large");
                var cards = workareaView.getSelectedEntityIds();

                if (cards.length > 0) {
                    _.each(cards, function (card) {
                        var cardView = _.find(workareaView.cards, function (c) {
                            return c.card.model.attributes.Id == card;
                        });
                        //entitys.push({ id: card.id.replace("card-", ""), entitytype: card.attributes["entity-type-id"].value });
                        entitys.push({ id: card, entitytype: cardView.card.model.attributes.EntityType });

                    });
                } else {
                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var entities = grid.getSelectedRowKeys();
                        _.each(entities, function (entity) {
                            entitys.push({ id: entity.Id, entitytype: entity.EntityType });
                        });
                    }
                }
            } else {
                if (ui.draggable[0].id != null && ui.draggable[0].id != "") {
                    var addedEntity = { id: ui.draggable[0].id.replace("card-", ""), entitytype: ui.draggable[0].attributes["entity-type-id"].value };
                } else {

                    var gridDiv = $(ui.draggable[0]).closest("#entity-table-container");
                    if (gridDiv.length > 0) {
                        var grid = gridDiv.dxDataGrid('instance');
                        var selection = grid.getSelectedRowKeys();
                        addedEntity = { id: selection[0].Id, entitytype: selection[0].EntityType }
                    }
                }
                entitys.push(addedEntity);
            }


            this.LinksCollection = new Backbone.Collection();

            if (this.LinksCollection.length > 0) {
                this.LinksCollection.reset();
            }

            _.each(entitys, function (entity) {

                var addedEntity = entity.id;

                if (entitytype != null && entity.entitytype != entitytype) {
                    return;
                }

                if (tableView) {
                    var relationTypeObj = _.find(that.relationTypes.models, function (relationType) {
                        return relationType.attributes.TargetEntityTypeId == entity.entitytype;
                    });
                    if (relationTypeObj) {
                        relationType = relationTypeObj.attributes.Id;
                    }
                }

                var relationSectionElement = that.$el.find("#" + relationType + " #relation-section-container #" + entity.id);
                var relationSubSectionElement = that.$el.find("#" + relationType + " #relation-sub-section-container #" + entity.id);
                if (relationSectionElement.length > relationSubSectionElement.length) {
                    return;
                }


                var direction = that.direction;
                var sourceId;
                var targetId;
                if (direction == "outbound") {
                    sourceId = addedToEntity;
                    targetId = addedEntity;
                } else {
                    sourceId = addedEntity;
                    targetId = addedToEntity;
                }

                if (linkentitytypeid && linkentitytypeid.value != "") {
                    var linkItemRelation = new Backbone.Model();

                    linkItemRelation.relationTypeToCreate = relationType;
                    linkItemRelation.sourceIdToCreate = sourceId;
                    linkItemRelation.targetIdToCreate = targetId;
                    linkItemRelation.entityTypeIdToCreate = linkentitytypeid.value;

                    that.LinksCollection.add(linkItemRelation);

                } else {
                    var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId });
                    newModel.save([], {
                        success: function (model) {
                            that.relations.add(model);
                            //var relationCard = $(_.template(nodeContentTemplate, { model: model.toJSON(), direction: that.direction, active: model.attributes["Active"], size: that.size }));

                            if (that.size == "small") {
                                if ($("#nodelist #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodelist #" + model.attributes["LinkType"]).removeClass("droppable-empty");
                                    // finns det någon mer som har entiteter men inte visar rubrik..
                                    $.each($("#nodelist .card-wrap").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }
                            else if (that.size == "table") {
                                that.renderTableList();
                            }
                            else {
                                if ($("#nodebox #" + model.attributes["LinkType"]).hasClass("droppable-empty")) {
                                    $("#nodebox #" + model.attributes["LinkType"]).removeClass("droppable-empty");

                                    // finns det någon mer som har entiteter men inte visar rubrik..
                                    $.each($("#nodebox .card-wrap-large").closest(".relation-section-wrap").find(".relation-section-title"), function () {
                                        if ($(this).hasClass("droppable-empty")) {
                                            $(this).removeClass("droppable-empty");
                                        }
                                    });
                                }
                            }
                            var checkIfCardExists = self.$el.find("#" + relationType + " #relation-section-container #card-" + model.get("TargetId"));

                            if (checkIfCardExists.length === 0) {
                                var relationCardSub = new relationCardView({ relation: model, direction: that.direction });

                                $("#" + model.attributes["LinkType"] + " #relation-section-container").append(relationCardSub.$el);
                            }
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });
                }
            });

            if (linkentitytypeid && linkentitytypeid.value != "" && that.LinksCollection.length > 0) {

                that.listenTo(window.appHelper.event_bus, 'entitycreated', that.completeMultipleLinkCreatation);

                entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                    id: 0,
                    entityTypeId: linkentitytypeid.value,
                    showInWorkareaOnSave: false
                });
            }
        },
        addLinkedRelation: function (event, ui, that, relationType) {
            event.stopPropagation();
            //kolla om det finns en LinkEntityTypeId om så måste man koppla till länkentitet.
            if (ui.helper && $(ui.helper[0]).find(".multiples").length > 0) {
                var workarea = $(ui.draggable[0]).closest("div#cards-container");
                var workareaView = window.appHelper.workAreaHandler.getActiveWorkarea();

                if (!workareaView) {
                    return;
                }

                var cards = workareaView.getSelectedEntityIds();

                _.each(cards, function (card) {

                    var addedToEntity = that.id;
                    var addedEntity = card;
                    var linkentityid = event.target.id;
                    var cardView = _.find(workareaView.cards, function (c) {
                        return c.card.model.attributes.Id == card;
                    });

                    var relationSectionElement = that.$el.find("#" + relationType + " #relation-section-container #" + addedEntity);

                    if (relationSectionElement.length > 0) {
                        return;
                    }

                    var typeObj = $(ui.draggable).closest(".card-section-wrap");
                    var linktype = "";
                    if (typeObj[0]) {
                        linktype = typeObj[0].id;
                    }

                    if (cardView.card.model.attributes.EntityType != linktype) {
                        return;
                    }

                    var direction = that.direction;
                    var sourceId;
                    var targetId;
                    if (direction == "outbound") {
                        sourceId = addedToEntity;
                        targetId = addedEntity;
                    } else {
                        sourceId = addedEntity;
                        targetId = addedToEntity;
                    }

                    var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId, LinkEntityId: linkentityid });
                    newModel.save([], {
                        success: function (model) {
                            that.relations.add(model);

                            var relationCard = new relationCardView({ relation: model, direction: that.direction });

                            $("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                        }
                    });

                });

            } else {

                var addedToEntity = that.id;
                var addedEntity = ui.draggable[0].id.replace("card-", "");
                var linkentityid = event.target.id;

                var direction = that.direction;
                var sourceId;
                var targetId;
                if (direction == "outbound") {
                    sourceId = addedToEntity;
                    targetId = addedEntity;
                } else {
                    sourceId = addedEntity;
                    targetId = addedToEntity;
                }

                var newModel = new relationModel({ LinkType: relationType, SourceId: sourceId, TargetId: targetId, LinkEntityId: linkentityid });
                newModel.save([], {
                    success: function (model) {
                        that.relations.add(model);

                        var relationCard = new relationCardView({ relation: model, direction: that.direction });

                        that.$el.find("#" + linkentityid + " #relation-sub-section-container").append(relationCard.$el);
                    },
                    error: function (model, response) {
                        inRiverUtil.OnErrors(model, response);
                    }
                });
            }
        },
        onToggleView: function (e) {
            e.stopPropagation();

            if (this.mode == "card") {
                this.mode = "table";
            } else {
                this.mode = "card";
            }

            window.appSession.relationViewMode = this.mode;
            this.render();
        },
        onIncludeFields: function (e) {
            e.stopPropagation();
            if (this.includeFields) {
                this.includeFields = false;
            } else {
                this.includeFields = true;
            }

            this.render();
        },
        setVisualMode: function () {
            this.$el.find("#card-view").removeClass("content-active-view");
            this.$el.find("#table-view").removeClass("content-active-view");
            this.$el.find("#include-fields").hide();

            if (this.mode == "table") {
                this.$el.find("#table-view").addClass("content-active-view");

                var groups = _.groupBy(this.relations.toJSON(), function (model) {
                    return model.LinkType;
                });
                var keys = _.keys(groups);
                if (keys.length == 1) {
                    this.$el.find("#include-fields").show();
                }
            } else {
                this.$el.find("#card-view").addClass("content-active-view");
            }
        },
        onEntityUpdated: function (model) {
            if (this.$el.find("#" + model).length > 0) {
                var self = this;
                var linkEntity = new entityModel({
                    id: model
                });

                linkEntity.fetch({
                    url: "/api/entity/" + model,
                    success: function () {
                        self.$el.find("#" + linkEntity.id + " .relation-sub-section-title-text").html("<i class=\"fa fa-chain\"></i>" + linkEntity.attributes.DisplayName);
                        if (linkEntity.attributes.DisplayDescription != "") {
                            self.$el.find("#" + linkEntity.id + " .relation-sub-section-title-text").append("-" + linkEntity.attributes.DisplayDescription);
                        }
                    }


                });

            }
        },
        onEntityDeleted: function (model) {
            var relationCards = this.$el.find("#" + model);
            _.each(relationCards, function (relationCard) {
                var subSectionContainer = $(relationCard).closest("#relation-sub-section-container");
                if (subSectionContainer && subSectionContainer.childElementCount == 1) {
                    var elementToRemove = subSectionContainer.parentElement.parentElement;
                    elementToRemove.parentElement.removeChild(elementToRemove);
                } else {
                    $(relationCard).remove();
                }
            });
        },
        onRelationRemoved: function (linkEntityId, entityId) {
            var subSectionContainer = this.$el.find("#" + linkEntityId + " #relation-sub-section-container");
            if (subSectionContainer.length > 0 && subSectionContainer[0].childElementCount == 1) {
                subSectionContainer.parent().parent().remove();
            }
        },
        pushAddAndLinkTool: function (tools) {
            tools.push({ name: "addAndLink", title: "Add and link new entity", icon: "icon-entypo-fix-plus std-entypo-fix-icon", text: "Add new entity", permission: "AddEntity,AddLink" });
        },
        contains: function (arrayList, targetEntityTypeId) {

            var matches = _.filter(arrayList, function (value) {
                if (value.TargetEntityTypeId == targetEntityTypeId) {
                    return value;
                }
            });

            return matches;
        },
        onConfigureAutomaticRule: function (e) {
            var self = this;

            var entitytype = $(e.currentTarget).closest(".relation-section-wrap")[0].attributes["entitytypeid"];
            if (entitytype == null) {
                entitytype = $(e.currentTarget).closest(".relation-sub-section-wrap")[0].attributes["entitytypeid"];
            }
            if (entitytype == null) {
                return;
            }
            var entitytypeid = entitytype.value;

            var relationTypeElement = $(e.target).closest(".relation-section-wrap")[0];

            var relationType = relationTypeElement.id;

            var headerLinkId = relationTypeElement.getElementsByClassName("relation-section-title-text")['0'].textContent;

            if (headerLinkId == null) {
                headerLinkId = relationType;
            }

            var modal = new modalPopup();
            modal.popOut(new configureLinkRuleView({ model: self.model, linkTypeId: relationType, direction: self.direction, targetEntityTypeId: entitytypeid }), { size: "medium", header: "Configure Link Rule for " + headerLinkId });
        },
        configureRuleExists: function (event, ui, that, type, linkedRelation) {

            var self = this;
            self.linkedRelation = linkedRelation;

            var linkRule = new linkRuleModel({ entityid: this.id });
            linkRule.fetch({
                success: function (model, response) {
                    if (response == null) {
                        if (self.linkedRelation) {
                            self.addLinkedRelation(event, ui, that, type);
                        } else {
                            self.addRelation(event, ui, that, type);
                        }
                    } else {
                        inRiverUtil.NotifyError("Add link where \"self configure linkrule\" exists", "You are not able to add link for: " + event.target.id + ". Because there exists \"self configure linkrule\". Please remove the linkrule and try again.");
                    }
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                },
                url: '/api/linkrule/' + self.id + '/' + event.target.id
            });
        },
        render: function () {
            var self = this;

            if (this.relations == null) {
                return this;
            }

            this.$el.empty();
            this.$el.append($(_.template(relationTopMenuTemplate, {})));
            this.setVisualMode();

            if (this.mode == "table") {
                var idList = [];
                if (this.includeFields) {
                    _.each(this.relations.toJSON(), function (model) {
                        if (self.direction == 'inbound') {
                            idList.push(model.TargetId);
                        } else {
                            idList.push(model.SourceId);
                        }
                    });

                    var entityTable = new entityTableCollection();
                    entityTable.fetch({
                        data: { ids: idList },
                        type: "POST",
                        success: function (models) {
                            var tableView = new entityTableView({ collection: models, includeFields: self.includeFields, placement: "relations", direction: self.direction, clickable: true });
                            self.$el.append(tableView.el);
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                            return;
                        }
                    });
                } else {
                    var entitytable = new entityTableView({ collection: this.relations, includeFields: this.includeFields, placement: "relations", direction: this.direction, clickable: true });
                    this.$el.append(entitytable.el);
                }
                var icon = this.$el.find("#include-fields i");
                if (this.includeFields) {
                    icon.removeClass("fa-square-o");
                    icon.addClass("fa-check-square-o");
                } else {
                    icon.removeClass("fa-check-square-o");
                    icon.addClass("fa-square-o");
                }

                return this;
            }

            _.each(this.relationTypes.models, function (model) {

                var entitytype = model.attributes["TargetEntityTypeId"];

                if (self.contains(self.excludeLinks, entitytype).length == 0) {

                    if (self.direction == "inbound") {
                        entitytype = model.attributes["SourceEntityTypeId"];
                    }

                    var sectionEl = $(_.template(relationTypeGroupTemplate, { model: model.toJSON(), entitytypeid: entitytype }));
                    var tools = new Array();
                    self.pushAddAndLinkTool(tools);
                    tools.push({ name: "open-in-workarea", title: "Open in workarea", icon: "fa-folder-open fa ", text: "Open in Workarea" });

                    if (self.direction == "outbound" && model.attributes.LinkEntityTypeId == null) {
                        tools.push({ name: "configure-rule", title: "Automatic Configure Rule", icon: "fa-close fa", text: "Automatic Configure Rule", permission: "AddLink" });
                    }

                    sectionEl.find("#contextmenu").html(new contextMenuView({
                        id: model.id,
                        tools: tools
                    }).$el);

                    var type = model.attributes["Id"];

                    var lastLinkEntityId = -1;

                    _.each(self.relations.models, function (relation) {

                        if (relation.attributes["LinkType"] == type) {

                            if (relation.attributes["LinkEntity"]) {

                                var linkentity = relation.attributes["LinkEntity"];
                                if (lastLinkEntityId != linkentity.Id) {
                                    lastLinkEntityId = linkentity.Id;

                                    var subsectionEl = $(_.template(relationTypeLinkEntityGroupTemplate, { model: relation.toJSON(), entitytypeid: entitytype }));
                                    tools = new Array();
                                    tools.push({ name: "edit-link-button", title: "Edit Entity", icon: "icon-entypo-fix-pencil std-entypo-fix-icon", text: "Edit", permission: "UpdateEntity" });
                                    self.pushAddAndLinkTool(tools);
                                    tools.push({ name: "open-in-workarea", title: "Open in workarea", icon: "fa-folder-open fa ", text: "Open in Workarea" });

                                    subsectionEl.find("#contextmenu").html(new contextMenuView({
                                        id: relation.id,
                                        tools: tools
                                    }).$el);

                                    sectionEl.find("#relation-section-container").append(subsectionEl);
                                }

                                var relationCardSub = new relationCardView({ relation: relation, direction: self.direction });

                                var test = sectionEl.find("#" + lastLinkEntityId + " #relation-sub-section-container");

                                test.append(relationCardSub.$el);

                                if (self.droppable) {
                                    sectionEl.find("#" + lastLinkEntityId).droppable({
                                        greedy: true,
                                        accept: function (draggable) {

                                            var typeObj = $(draggable).closest(".card-section-wrap");
                                            var linktype = "";
                                            if (typeObj[0]) {
                                                linktype = typeObj[0].id;
                                            }
                                            if (linktype != id) {
                                                return false;
                                            }

                                            var drager = draggable[0].id.replace("card-", "");
                                            //kolla om dragger finns.
                                            if (drager == null || drager == "") {
                                                return false;
                                            }

                                            var x = $(this).find("#" + drager);
                                            if (x.length > 0) {
                                                return false;
                                            }
                                            return true;
                                        },
                                        activeClass: "relation-sub-section-container-active",
                                        hoverClass: "relation-dropable-hover",
                                        drop:
                                            function (event, ui) {
                                                self.configureRuleExists(event, ui, self, type, true);
                                            }
                                    });
                                }
                                if (self.sortable) {
                                    sectionEl.find("#relation-section-container").sortable({
                                        //connectWith: "#relation-section-container",
                                        handle: ".card-picture-area, .card-wrap",
                                        placeholder: "card-placeholder",
                                        update: self.onChangeSortOrder
                                    });

                                    sectionEl.find("#relation-sub-section-container").sortable({
                                        // connectWith: "#relation-sub-section-container",
                                        handle: ".relation-picture-area, .card-wrap",
                                        placeholder: "card-placeholder",
                                        update: self.onChangeSortOrder
                                    });
                                }
                            } else {
                                var relationCard = new relationCardView({ relation: relation, direction: self.direction });


                                sectionEl.find("#relation-section-container").append(relationCard.$el);
                                if (self.sortable) {
                                    sectionEl.find("#relation-section-container").sortable({
                                        // connectWith: "#relation-section-container",
                                        handle: ".card-picture-area, .card-wrap",
                                        placeholder: "card-placeholder",
                                        update: self.onChangeSortOrder
                                    });
                                }
                            }
                        }
                    });

                    var id;

                    if (self.direction == "outbound") {
                        id = model.attributes["TargetEntityTypeId"];
                    } else {
                        id = model.attributes["SourceEntityTypeId"];
                    }

                    self.$el.append(sectionEl);

                    if (self.droppable) {
                        $("#" + model.attributes["Id"]).droppable({
                            accept: function (draggable) {

                                var drager = draggable[0].id;

                                if (drager == "") {
                                    //kolla om det är från en tabell.

                                    var gridDiv = $(draggable[0]).closest("#entity-table-container");
                                    if (gridDiv.length > 0) {
                                        var grid = gridDiv.dxDataGrid('instance');
                                        var entities = grid.getSelectedRowKeys();
                                        if (entities && entities.length > 0) {
                                            var ok = false;
                                            var i = 0;
                                            while (!ok && i < entities.length) {
                                                var x = $(this).find("#" + entities[i].Id);
                                                ok = !x || x.length == 0;

                                                ok = ok && (entities[i].EntityType == id);
                                                i++;
                                            }

                                            return ok;
                                        }

                                    }
                                }

                                var typeObj = $(draggable).closest(".card-section-wrap");
                                var linktype = "";
                                if (typeObj[0]) {
                                    linktype = typeObj[0].id;
                                }
                                if (linktype != id) {
                                    return false;
                                }

                                //kolla om dragger finns.
                                if (drager == null || drager == "") {
                                    return false;
                                }

                                var x = $(this).find("#" + drager);
                                if (x.length > 0) {
                                    return false;
                                }
                                return true;
                            },
                            activeClass: "relation-section-container-active",
                            hoverClass: "relation-dropable-hover",
                            drop:
                                function (event, ui) {
                                    self.configureRuleExists(event, ui, self, id, false);
                                }
                        });
                    }
                }
            });

            var ids = $(".relation-section-wrap");

            for (var index = 0; index < ids.length; ++index) {
                showLinkTypes('#' + ids[index].id);
            }

            function showLinkTypes(id) {
                id = id.replace(" ", "-");
                var $heading = $(id + " .relation-section-title .showLinkTypes");
                var $heading2 = $(id + " .relation-section-title .relation-section-title-text");
                var $box = $(id).find("#relation-section-container");

                var $showTask = $(id + '-show i');

                $heading.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
                $heading2.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            ids = $(".relation-sub-section-wrap");

            for (index = 0; index < ids.length; ++index) {
                showSubLinkTypes('#' + ids[index].id);
            }

            function showSubLinkTypes(id) {
                id = id.replace(" ", "-");
                //var $heading = self.$el.find(id + " .relation-sub-section-title");
                var $heading = $(id + " .relation-sub-section-title .showLinkTypes");
                var $heading2 = $(id + " .relation-sub-section-title .relation-section-title-text");

                var $box = $(id).find("#relation-sub-section-container");
                var $showTask = self.$el.find(id + '-show i');

                $heading.click(function (e) {
                    $box.toggle(200);
                    $showTask.toggle();
                });
                $heading2.click(function () {
                    $box.toggle(200);
                    $showTask.toggle();
                });
            }

            //add context menu
            return this;
        }
    });

    return relationView;
});
