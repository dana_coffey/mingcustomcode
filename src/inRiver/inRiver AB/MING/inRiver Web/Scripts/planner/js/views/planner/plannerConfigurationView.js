define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'misc/plannerGlobals',
  'sharedjs/misc/permissionUtil',
  'modalPopup',
  'views/configuration/configurationQuickEditView',
  'views/configuration/configurationTooltipsView',
  'views/configuration/configurationColorView',
  'text!templates/planner/plannerConfigurationViewTemplate.html'

], function ($, _, backbone, inRiverUtil, plannerGlobals, permissionUtil, modalPopup,
    configurationQuickEditView, configurationToolTipsView, configurationColorView, plannerConfigurationViewTemplate) {

    var plannerConfigurationView = backbone.View.extend({
        template: _.template(plannerConfigurationViewTemplate),
        initialize: function () {
            window.appHelper.event_bus.off('planner-configuration-unsaved-changed');
            this.listenTo(window.appHelper.event_bus, 'planner-configuration-unsaved-changed', this.onIsUnsavedChanged);
            this.onIsUnsavedChanged();
            this.render();
            this.$el.find("#loading-spinner").hide();
        },
        events: {
            "click #planner-configuration-save-all-button": "onSaveAll",
            "click #planner-configuration-undo-all-button": "onUndoAll",
            "click #tab-quickedit": "onChangeTab",
            "click #tab-tooltips": "onChangeTab",
            "click #tab-color": "onChangeTab"
        },
        onSaveAll: function () {
            window.appHelper.event_bus.trigger('planner-configuration-save');
        },
        onUndoAll: function () {
            window.appHelper.event_bus.trigger('planner-configuration-undo');
        },
        createQuickEditTabView: function () {
            var quickEditSubView = new configurationQuickEditView();
            return quickEditSubView;
        },
        createTooltipTabView: function () {
            var tooltipSubView = new configurationToolTipsView();
            return tooltipSubView;
        },
        createColorTabView: function () {
            var colorSubView = new configurationColorView();
            return colorSubView;
        },
        onChangeTab: function (e) {
            var self = this;
            if (window.appSession.plannerConfigurationTabId === e.currentTarget.id) {
                return;
            }

            inRiverUtil.proceedOnlyAfterUnsavedChangesCheck(this._currentTopTabView, function () {
                window.appSession.plannerConfigurationTabId = e.currentTarget.id; // remember default tab
                var urlTabName = window.appSession.plannerConfigurationTabId.replace("tab-", "");
                self.goTo("configuration/" + self.id + "/" + urlTabName, { trigger: false });
                self.createTabView(window.appSession.plannerConfigurationTabId);
                self.$el.find("#planner-configuration-main").html(self._currentTopTabView.el);
                self.indicateActiveTab();
            });
        },
        hasUnsavedChanges: function () { // called from the router to prevent the user from losing changes
            if (this._currentTopTabView == null) {
                return false;
            }

            // Delegate decision to current subview
            if (_.isFunction(this._currentTopTabView.hasUnsavedChanges)) {
                return this._currentTopTabView.hasUnsavedChanges();
            }
            return false;
        },
        onIsUnsavedChanged: function () {
            var isUnsaved = this.hasUnsavedChanges();
            if (isUnsaved) {
                this.$el.find("#planner-configuration-save-all-button").show();
                this.$el.find("#planner-configuration-undo-all-button").show();
            } else {
                this.$el.find("#planner-configuration-save-all-button").hide();
                this.$el.find("#planner-configuration-undo-all-button").hide();
            }
        },
        createTabView: function (idOfTab) {
            if (this._currentTopTabView) {
                this._currentTopTabView.remove();
                this._currentTopTabView.close();
            }

            if (idOfTab === "tab-quickedit") {
                this._currentTopTabView = this.createQuickEditTabView();
            } else if (idOfTab === "tab-tooltips") {
                this._currentTopTabView = this.createTooltipTabView();
            } else if (idOfTab === "tab-color") {
                this._currentTopTabView = this.createColorTabView();
            }
            if (this._currentTopTabView) {
                this.listenTo(this._currentTopTabView, "isUnsavedChanged", this.onIsUnsavedChanged);
            }
        },
        indicateActiveTab: function () {
            this.$el.find("#tab-quickedit").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-tooltips").removeClass("control-row-tab-button-active");
            this.$el.find("#tab-color").removeClass("control-row-tab-button-active");

            this.$el.find("#" + window.appSession.plannerConfigurationTabId).addClass("control-row-tab-button-active");
        },
        render: function () {
            this.$el.html(this.template());
            if (!window.appSession.plannerConfigurationTabId) {
                window.appSession.plannerConfigurationTabId = "tab-quickedit";
            }
            this.createTabView(window.appSession.plannerConfigurationTabId);
            this.$el.find("#planner-configuration-main").html(this._currentTopTabView.el);

            this.indicateActiveTab();

            return this; // enable chained calls
        }
    });

    return plannerConfigurationView;
});

