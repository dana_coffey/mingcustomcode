define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'sharedjs/views/completeness/completenessPopupView',
  'sharedjs/models/entity/entityModel',
  'sharedjs/models/resource/resourceModel',
  'sharedjs/models/relation/relationModel',
  'sharedjs/models/completeness/completenessModel',
  'sharedjs/views/entityview/entityDetailSettingView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'sharedjs/views/contextmenu/contextMenuView',
  'text!sharedtemplates/entitycard/workareaCardTemplate.html',
  'text!templates/entitycard/ResourceDownloadPopup.html'

], function ($, _, Backbone, inRiverUtil, modalPopup, completenessPopupView, entityModel, resourceModel, relationModel, completenessModel, entityDetailSettingView, entityDetailSettingWrapperView, contextMenuView, entityCardItemViewTemplate, resourceDownloadPopup) {

    var cardItemView = Backbone.View.extend({
        //tagName: 'div',
        template: _.template(entityCardItemViewTemplate),
        initialize: function (options) {

            this.id = this.model.attributes["TargetEntity"].Id;

            this.relationId = this.model.id;

            this.target = new entityModel({ id: this.id });

            this.target.fetch({
                url: "../../api/entity/" + this.id

            });


        },
        events: {
            "click #card-completeness-text": "onShowCompletnessDetails",
            "click #large-card-completeness-text": "onShowCompletnessDetails",
            "click .completeness-small": "onShowCompletnessDetails",
            "click #edit-button": "onEditDetail",
            "click #star-button": "onEntityStar",
            "click .card-tools-header": "onToggleTools",
            "click #close-tools-container": "onToggleTools",
            "click #delete-button": "onEntityDelete",
            "click #add-task-button": "onNewTask",
            "click #enrich-button": "onGoToEnrich",
            "click #remove-relation-button": "onRemoveRelation"
        },
        onRemoveRelationConfirm: function (self) {

            var relation = new relationModel({ Id: self.relationId });

            //relation.fetch({
            //    success: function () {

            //        self.stopListening(appHelper.event_bus);
            //        self.remove();
            //        self.unbind();
            //    }
            //});
            
            relation.destroy({
                success: function () {
                    inRiverUtil.Notify("Relation successfully removed");
                    self.stopListening(appHelper.event_bus);
                    self.remove();
                    self.unbind();
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onRemoveRelation: function (e) {
            e.stopPropagation();

            inRiverUtil.NotifyConfirm("Confirm remove relation", "Are you sure you want to remove the selected relation?", this.onRemoveRelationConfirm, this);
        },
        onEntityDelete: function (e) {
            e.stopPropagation();
            this.onToggleTools(e);


            inRiverUtil.deleteEntity(this);

        },
        onGoToEnrich: function (e) {
            e.stopPropagation();

            window.location.href = "/app/enrich/index#entity/" + this.model.attributes.TargetEntity.Id;

        },
        onNewTask: function (e) {
            e.stopPropagation();

            this.stopListening(appHelper.event_bus);
            this.listenTo(appHelper.event_bus, 'entitycreated', this.taskAddedNowLinkEntity);
            entityDetailSettingWrapperView.prototype.showCreateEntityPopup({
                id: 0,
                entityTypeId: "Task",
                showInWorkareaOnSave: false
            });


        },
        taskAddedNowLinkEntity: function (model) {
            var self = this;

            this.targetIdToCreate = model.attributes.Id;
            var linktype = this.TaskRelationType.attributes.Id;

            var newModel = new relationModel({ LinkType: linktype, SourceId: model.attributes.Id, TargetId: this.model.id, LinkEntityId: null });
            newModel.save([], {
                success: function (model) {
                    var i = 0;
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });
        },
        onDownloadResouce: function (e) {
            var self = this;
            e.stopPropagation();
            e.preventDefault();


            this.resource = new resourceModel({ id: this.id, urlRoot: '/api/resource' });
            this.resource.fetch({
                url: '/api/resource/' + this.id,
                success: function () {

                    //if (self.resource.get("DownloadConfigurations").length > 0) {
                    //    self.$el.append(_.template(resourceDownloadPopup, { resource: self.resource.toJSON() }));
                    //}
                    //self.$el.append(_.template(resourceDownloadPopup, { resource: self.resource.toJSON() }));
                    self.$el.append(_.template(resourceDownloadPopup, { resource: self.resource.toJSON() }));

                    self.$el.find(".download-menu").menu();

                    var menu = self.$el.find(".download-menu").position({
                        my: "left top",
                        at: "right bottom",
                        of: e.currentTarget
                    });
                    menu.show();

                    $(document).one("click", function () {
                      //  self.onToggleTools(e);
                        menu.remove();
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                }
            });


        },

        onToggleTools: function (e) {
            e.stopPropagation();
            var $box = this.$el.find("#card-tools-container");
            $box.toggle(200);
        },
        onEditDetail: function (e) {
            e.stopPropagation();
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: this.model.id
            });
        },
        onEntityStar: function (e) {
            e.stopPropagation();

            var xmlhttp = new XMLHttpRequest();

            if (this.model.attributes.TargetEntity.Starred == "1") {
                xmlhttp.open("GET", "/api/starredentity/removestar/" + this.id, true);
                xmlhttp.send();
                this.model.attributes.TargetEntity.Starred = 0;

            } else {
                xmlhttp.open("GET", "/api/starredentity/star/" + this.id, true);
                xmlhttp.send();
                this.model.attributes.TargetEntity.Starred = 1;
            }

            this.render();
        },

        onShowCompletnessDetails: function (e) {
            e.stopPropagation();
            inRiverUtil.ShowInlineCompleteness(this.id, $(e.currentTarget));
       
        },
        onClose: function () {
        },
        drawCompleteness: function (percent) {
            var diff = Math.round(percent / 5);

            this.$el.find('.pie').html(String.fromCharCode(65 + diff));
        },

        render: function () {

            var self = this;

            if (this.model == null) {
                return this;
            }

            if (this.taskRelations != null) {
                this.TaskRelationType = _.find(this.taskRelations.models, function (model) {
                    return model.attributes.TargetEntityTypeId == self.model.attributes.EntityType;

                });
            }

            this.$el.html(_.template(entityCardItemViewTemplate, { data: this.model.toJSON().TargetEntity, userName: appHelper.userName, enableAddTask: this.TaskRelationType != undefined, icon: this.model.attributes.TargetEntity.EntityType }));

            var tools = new Array();
            if (this.model.attributes.TargetEntity.Starred == "1") {
                tools.push({ name: "star-button", title: "Unstar Entity", icon: "icon-entypo-fix-star std-entypo-fix-icon", text: "Unstar entity" });
            } else {
                tools.push({ name: "star-button", title: "Star Entity", icon: "icon-entypo-fix-star-empty std-entypo-fix-icon", text: "Star entity" });
            }
            tools.push({ name: "enrich-button", title: "View in Enrich", icon: "fa fa-list", text: "View in Enrich" });
            tools.push({ name: "remove-relation-button", title: "Remove relation", icon: "fa-trash-o fa", text: "Remove relation", permission: "DeleteLink" });

            this.$el.find("#contextmenu").html(new contextMenuView({
                id: this.model.id,
                tools: tools
            }).$el);
            this.drawCompleteness(this.model.attributes.TargetEntity.Completeness);
            return this; // enable chained calls
        },
    });

    return cardItemView;
});

