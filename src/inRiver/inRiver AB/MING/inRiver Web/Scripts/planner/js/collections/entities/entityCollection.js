﻿define([
  'jquery',
  'underscore',
  'backbone',
  'sharedjs/models/entity/entityModel'
], function ($, _, Backbone, entityModel) {
    var entityCollection = Backbone.Collection.extend({
        model: entityModel,
        url: '../../api/entity'
    });

    return entityCollection;
});
