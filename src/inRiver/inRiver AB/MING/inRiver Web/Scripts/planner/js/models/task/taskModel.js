﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone) {

    var taskModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {

        },
        get: function (attr) {
            // Override of get() to support "synthetic" attributes
            if (typeof this[attr] == 'function') {
                return this[attr]();
            }
            return Backbone.DeepModel.prototype.get.call(this, attr);
        },
        Name: function () {
            var desc = this.get("DisplayDescription");
            var name = this.get("DisplayName");
            if (desc && name) return name + " - " + desc;
            else if (desc) return desc;
            else if (name) return name;
            return "-";
        },
    });

    return taskModel;

});
