﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var marketLinkModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: "../../api/marketLink"

    });

    return marketLinkModel;

});
