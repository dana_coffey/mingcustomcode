// Filename: app.js
define([
  'jquery',
  'underscore',
  'backbone',
  'router', // Request router.js
  'alertify',
  'sharedjs/views/settingsmenu/settingsMenu'
], function ($, _, backbone, router, alertify, settingsMenu) {
    function initApp() {
        settingsMenu.initializeSettingsMenu();
        alertify.set({ buttonReverse: true });
    }

    var initialize = function () {
        // Pass in our Router module and call it's initialize function
        router.initialize();
        initApp();
    };
    return {
        initialize: initialize
    };
});
