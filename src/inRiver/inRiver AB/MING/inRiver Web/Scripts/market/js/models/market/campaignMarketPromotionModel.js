﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var campaignMarketPromotionModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: "../../api/campaignMarketPromotion"

    });

    return campaignMarketPromotionModel;

});
