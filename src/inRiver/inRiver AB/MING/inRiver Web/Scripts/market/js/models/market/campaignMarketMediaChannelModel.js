﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var campaignMarketMediaChannelModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: "../../api/campaignMarketMediaChannel"

    });

    return campaignMarketMediaChannelModel;

});
