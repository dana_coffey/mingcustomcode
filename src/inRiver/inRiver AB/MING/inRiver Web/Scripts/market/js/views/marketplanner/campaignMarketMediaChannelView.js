define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'devextreme',
  'jquery-ui',
  'misc/marketPlannerUtil',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'sharedjs/models/relation/relationModel',
  'collections/market/campaignMarketMediaChannelCollection',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/marketplanner/campaignMarketMediaChannelTemplate.html'
], function ($, _, backbone, backboneforms, devextreme, jqueryui, marketPlannerUtil, inRiverUtil, permissionUtil, relationModel, campaignMarketMediaChannelCollection, entityDetailSettingWrapperView, campaignMarketMediaChannelTemplate) {

    var marketPlannerView = backbone.View.extend({
        initialize: function (options) {
            this.campaignId = options.campaignId;
            this.mediaChannelId = options.mediaChannelId;
            this.mediaChannelModel = options.mediaChannelModel;
            this.participatingMarkets = options.participatingMarkets;
            this.campaignMarketMediaChannelCollection = new campaignMarketMediaChannelCollection({ campaignId: options.campaignId, mediaChannelId: options.mediaChannelId });
            this.listenTo(appHelper.event_bus, "entitycreated", this.render);
            this.listenTo(appHelper.event_bus, "entityupdated", this.render);
            this.listenTo(appHelper.event_bus, "entitydeleted", this.render);
            this.render();
        },
        rowClick: function (e) {
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: e.key.id
            });
        },
        render: function () {
            var self = this;
            if (!this.$el.html()) {
                this.$el.html(campaignMarketMediaChannelTemplate);
            }

            this.campaignMarketMediaChannelCollection.fetch({
                success: function () {
                    var models = _.filter(self.campaignMarketMediaChannelCollection.models, function(model) {
                        return permissionUtil.CheckPermission(window.appHelper.userName, _.findWhere(model.attributes.Fields, { FieldType: "ActivityMarketMediaChannelMarket" }).Value);
                    });
                    var defaultFieldMapping = {
                        MediaChannelStartDate: "ActivityMarketMediaChannelStartDate",
                        MediaChannelEndDate: "ActivityMarketMediaChannelEndDate",
                        MediaChannellssueDate: "ActivityMarketMediaChannelIssueDate",
                        MediaChannelWorkflowTemplate: "ActivityMarketMediaChannelWorlflowTemplate",
                        MediaChannelTargetGroups: "ActivityMarketMediaChannelTargetGroup",
                        MediaChannelResposibleRole: "ActivityMarketMediaChannelResponsibleRole",
                        MediaChannelResponsibleName: "ActivityMarketMediaChannelResponsibleName"
                    };
                    var fieldDefaults = {};
                    _.each(self.mediaChannelModel.attributes.Fields, function (field) {
                        if (defaultFieldMapping[field.FieldType]) {
                            fieldDefaults[defaultFieldMapping[field.FieldType]] = field;
                        }
                    });
                    if (models.length) {
                        var columnTemplate = [{ dataField: "attributes.DisplayName", caption: models[0].attributes.EntityTypeDisplayName, allowEditing: false, width: "442px" }];
                        var fields = models[0].attributes.Fields;
                        for (var index = 0; index < fields.length; index++) {
                            if (fields[index].FieldType !== "ActivityMarketMediaChannelMarket") {
                                var column = marketPlannerUtil.addToColumnTemplate(models, fields, index, columnTemplate);
                                if (column) {
                                    column.defaultField = fieldDefaults[fields[index].FieldType];
                                    column.width = "150px";
                                }
                            }
                        }
                        self.grid = self.$el.find("#campaign-market-media-channel-edited-container").dxDataGrid({
                            cellHintEnabled: false,
                            dataSource: models,
                            columns: columnTemplate,
                            showColumnLines: true,
                            showRowLines: true,
                            rowClick: function (e) {
                                self.rowClick(e);
                            },
                            columnAutoWidth: true,
                            allowColumnResizing: true,
                            onCellClick: function (e) {
                                if (e.rowType === "header") {
                                    return;
                                }
                                entityDetailSettingWrapperView.prototype.showAsPopup({
                                    id: e.data.id
                                });
                                e.jQueryEvent.stopPropagation();
                            },
                            onCellHoverChanged: function (e) {
                                if (e.rowType === "header" || !e.column || !e.column.defaultField) {
                                    return;
                                }

                                var defaultValue = marketPlannerUtil.getDataGridDisplayValueFromField(e.column.defaultField);
                                if (defaultValue !== e.text) {
                                    if (e.eventType === "mouseover") {
                                        var tooltip = self.$el.find("#tooltip").dxTooltip({ target: e.cellElement, visible: true }).dxTooltip("instance");
                                        tooltip.content().html(defaultValue);
                                    } else {
                                        self.$el.find("#tooltip").dxTooltip({ target: e.cellElement, visible: false });
                                    }
                                }
                            },
                            onCellPrepared: function (e) {
                                if (e.rowType === "header") {
                                    return;
                                }

                                var defaultField = e.column.defaultField;
                                var isModified = false;
                                if (defaultField && e.text) {
                                    isModified = marketPlannerUtil.getDataGridDisplayValueFromField(defaultField).toLowerCase() !== e.text.toLowerCase();
                                }
                                e.cellElement.css("background", isModified ? "LightYellow" : null);
                            },
                            paging: false,
                            scrolling: {
                                useNative: true
                            },
                            selection: {
                                mode: "none"
                            },
                            sorting: { mode: 'multiple' },
                            loadPanel: { text: 'Loading data...', showIndicator: false },
                        }).dxDataGrid('instance');
                    }
                    if (models.length === 0) {
                        self.$el.find("#campaign-market-media-channel-edited-container").text("No participation");
                    }
                }
            });
        }
    });

    return marketPlannerView;
});

