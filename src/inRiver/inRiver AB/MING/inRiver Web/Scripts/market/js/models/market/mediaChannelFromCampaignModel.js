﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var mediaChannelFromCampaignModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: "../../api/mediaChannelFromCampaign"

    });

    return mediaChannelFromCampaignModel;

});
