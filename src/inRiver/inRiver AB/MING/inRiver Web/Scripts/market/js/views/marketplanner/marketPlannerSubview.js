define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'devextreme',
  'jquery-ui',
  'misc/marketPlannerUtil',
  'sharedjs/misc/inRiverUtil',
  'models/market/marketLinkModel',
  'collections/market/mediaChannelFromCampaignCollection',
  'collections/market/promotionFromCampaignCollection',
  'views/marketplanner/campaignMarketMediaChannelView',
  'views/marketplanner/campaignMarketPromotionView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/marketplanner/marketPlannerSubviewTemplate.html'
], function ($, _, backbone, backboneforms, devextreme, jqueryui, marketPlannerUtil, inRiverUtil, marketLinkModel, mediaChannelCollection, promotionCollection, campaignMarketMediaChannelView, campaignMarketPromotionView, entityDetailSettingWrapperView, marketPlannerSubviewTemplate) {

    var marketPlannerSubview = backbone.View.extend({
        initialize: function (options) {
            this.campaignMarketMediaChannelViews = {};
            this.campaignMarketPromotionViews = {};
            this.campaignId = options.id;
            this.campaignModel = options.campaignModel;
            this.participatingMarkets = options.participatingMarkets;
            this.render();
        },
        render: function() {
            var self = this;
            this.expandedMediaChannelRows = [];
            this.expandedPromotionRows = [];
            if (!this.$el.html()) {
                this.$el.html(marketPlannerSubviewTemplate);
            }
            if (this.participatingMarkets && this.participatingMarkets.length) {
                this.mediaChannelCollection = new mediaChannelCollection({ id: this.campaignId });
                this.mediaChannelCollection.fetch({
                    success: function() {
                        self.renderMediaChannelGrid();
                    }
                });
                this.promotionCollection = new promotionCollection({ id: this.campaignId });
                this.promotionCollection.fetch({
                    success: function() {
                        self.renderPromotionGrid();
                    }
                });
            } else {
                self.$el.find("#market-planner-channel-container").text("No participation");
            }
        },
        renderMediaChannelGrid: function () {
            var self = this;
            if (self.mediaChannelGrid) {
                _.forEach(self.mediaChannelModels, function (model) {
                    if (self.mediaChannelGrid.isRowExpanded(model)) {
                        self.expandedMediaChannelRows.push(model.id);
                    }
                });
            }
            var models = self.mediaChannelModels = _.filter(this.mediaChannelCollection.models, function (model) {
                var mediaChannelMarketField = _.findWhere(model.attributes.Fields, { FieldType: "MediaChannelMarket" });
                return _.intersection(self.participatingMarkets, mediaChannelMarketField ? mediaChannelMarketField.Value : null).length;
            });
            if (models.length) {
                var columnTemplate = [{ dataField: "attributes.NameAndDescription", caption: models[0].attributes.EntityTypeDisplayName, allowEditing: false, width: "450px" }];
                var fields = models[0].attributes.Fields;
                var visibleFields = [
                    "MediaChannelStartDate",
                    "MediaChannelEndDate"
                ];
                for (var index = 0; index < fields.length; index++) {
                    if (_.contains(["MediaChannelMarket", "MediaChannelMarketParticipation", "MediaChannelMarketNonParticipation"], fields[index].FieldType)) {
                        self[fields[index].FieldType + "Index"] = index;
                    }
                    marketPlannerUtil.addToColumnTemplate(models, fields, index, columnTemplate, visibleFields);
                }
                marketPlannerUtil.addParticipatingPropertiesToModels(models, self.MediaChannelMarketIndex, self.MediaChannelMarketParticipationIndex, self.MediaChannelMarketNonParticipationIndex, self.participatingMarkets);
                marketPlannerUtil.pushMarketParticipationColumnsToTemplate(fields[self.MediaChannelMarketIndex], columnTemplate, self.participatingMarkets);
                columnTemplate.push({ dataField: "", allowEditing: false, allowFiltering: false });
                self.mediaChannelGrid = self.$el.find("#market-planner-channel-container").dxDataGrid({
                    dataSource: models,
                    columns: columnTemplate,
                    showColumnLines: true,
                    showRowLines: true,
                    masterDetail: {
                        enabled: true,
                        template: function (container, info) {
                            if (!self.campaignMarketMediaChannelViews[info.key.id]) {
                                self.campaignMarketMediaChannelViews[info.key.id] = new campaignMarketMediaChannelView({ campaignId: self.campaignId, mediaChannelId: info.key.id, mediaChannelModel: info.key, participatingMarkets: self.participatingMarkets });
                            } else {
                                self.campaignMarketMediaChannelViews[info.key.id].render();
                            }
                            self.campaignMarketMediaChannelViews[info.key.id].$el.appendTo(container);
                        }
                    },
                    editing: {
                        editEnabled: true,
                        editMode: "cell"
                    },
                    onCellPrepared: function(info) { marketPlannerUtil.onCellPrepared(info) },
                    onEditingStart: function (info) {
                        info.cancel = !info.data.attributes[info.column.dataField.split(".")[1]];
                    },
                    onRowUpdated: function (info) {
                        var market = Object.keys(info.data.attributes)[0];
                        marketPlannerUtil.onRowUpdated(info, self.MediaChannelMarketParticipationIndex, self.MediaChannelMarketNonParticipationIndex, function () {
                            self.render();
                        });
                        var newMarketLinkModel = new marketLinkModel();
                        newMarketLinkModel.save({ Id: 0, LinkType: "ActivityMarketMediaChannels", SourceId: self.campaignId, TargetId: info.key.id, Market: market, Participate: info.data.attributes[market] === "Y" },
                        {
                            success: function (){
                                if (self.campaignMarketMediaChannelViews[info.key.id]) {
                                    self.campaignMarketMediaChannelViews[info.key.id].participatingMarkets = info.key.attributes.Fields[self.MediaChannelMarketParticipationIndex].Value;
                                    self.campaignMarketMediaChannelViews[info.key.id].render();
                                }
                            },
                            error: function (result, response) {
                                inRiverUtil.OnErrors(result, response);
                            }
                        });
                    },
                    onRowCollapsing: function (e) {
                        self.expandedMediaChannelRows = _.without(self.expandedMediaChannelRows, e.key.id);
                    },
                    onRowExpanding: function(e) {
                        self.expandedMediaChannelRows = _.union(self.expandedMediaChannelRows, e.key.id);
                    },
                    onCellClick: function (e) {
                        if (e.rowType === "header") {
                            return;
                        }
                        if (e.columnIndex === 1) {
                            entityDetailSettingWrapperView.prototype.showAsPopup({
                                id: e.data.id,
                                disableAllFields: true
                            });
                        }
                    },
                    columnAutoWidth: true,
                    allowColumnResizing: true,
                    paging: false,
                    scrolling: {
                        useNative: false,
                        scrollByThumb: true
                    },
                    selection: {
                        mode: "none"
                    },
                    sorting: { mode: 'multiple' },
                    loadPanel: { text: 'Loading data...', showIndicator: false },
                    onContentReady: function (e) {
                        _.forEach(self.expandedMediaChannelRows, function (id) {
                            self.mediaChannelGrid.expandRow(_.find(models, function (newModel) {
                                return newModel.id === id;
                            }));
                        });
                    }
                }).dxDataGrid('instance');
            } else {
                self.$el.find("#market-planner-channel-container").text("No media channels");
            }
        },
        renderPromotionGrid: function () {
            var self = this;
            if (self.promotionGrid) {
                _.forEach(self.promotionModels, function (model) {
                    if (self.promotionGrid.isRowExpanded(model)) {
                        self.expandedPromotionRows.push(model.id);
                    }
                });
            }
            var models = self.promotionModels = _.filter(this.promotionCollection.models, function (model) {
                var promotionMarketField = _.findWhere(model.attributes.Fields, { FieldType: "PromotionMarket" });
                return _.intersection(self.participatingMarkets, promotionMarketField ? promotionMarketField.Value : null).length;
            });
            if (models.length) {
                var columnTemplate = [{ dataField: "attributes.DisplayName", caption: models[0].attributes.EntityTypeDisplayName, allowEditing: false, width: "450px" }];
                var fields = models[0].attributes.Fields;
                var visibleFields = [
                    "PromotionStartDate",
                    "PromotionEndDate"
                ];
                for (var index = 0; index < fields.length; index++) {
                    if (_.contains(["PromotionMarket", "PromotionMarketParticipation", "PromotionMarketNonParticipation"], fields[index].FieldType)) {
                        self[fields[index].FieldType + "Index"] = index;
                    }
                    marketPlannerUtil.addToColumnTemplate(models, fields, index, columnTemplate, visibleFields);
                }
                marketPlannerUtil.addParticipatingPropertiesToModels(models, self.PromotionMarketIndex, self.PromotionMarketParticipationIndex, self.PromotionMarketNonParticipationIndex, self.participatingMarkets);
                marketPlannerUtil.pushMarketParticipationColumnsToTemplate(fields[self.PromotionMarketIndex], columnTemplate, self.participatingMarkets);
                columnTemplate.push({ dataField: "", allowEditing: false, allowFiltering: false });
                self.promotionGrid = self.$el.find("#market-planner-promotion-container").dxDataGrid({
                    dataSource: models,
                    columns: columnTemplate,
                    showColumnLines: true,
                    showRowLines: true,
                    masterDetail: {
                        enabled: true,
                        template: function (container, info) {
                            if (!self.campaignMarketPromotionViews[info.key.id]) {
                                self.campaignMarketPromotionViews[info.key.id] = new campaignMarketPromotionView({ campaignId: self.campaignId, promotionId: info.key.id, campaignModel: self.campaignModel, promotionModel: info.key, participatingMarkets: self.participatingMarkets });
                            } else {
                                self.campaignMarketPromotionViews[info.key.id].render();
                            }
                            self.campaignMarketPromotionViews[info.key.id].$el.appendTo(container);
                        }
                    },
                    editing: {
                        editEnabled: true,
                        editMode: "cell"
                    },
                    onRowCollapsing: function (e) {
                        self.expandedPromotionRows = _.without(self.expandedPromotionRows, e.key.id);
                    },
                    onRowExpanding: function (e) {
                        self.expandedPromotionRows = _.union(self.expandedPromotionRows, e.key.id);
                    },
                    onCellClick: function (e) {
                        if (e.columnIndex === 1) {
                            entityDetailSettingWrapperView.prototype.showAsPopup({
                                id: e.data.id,
                                disableAllFields: true
                            });
                        }
                    },
                    onCellPrepared: function (info) { marketPlannerUtil.onCellPrepared(info) },
                    onEditingStart: function (info) {
                        info.cancel = !info.data.attributes[info.column.dataField.split(".")[1]];
                    },
                    onRowUpdated: function (info) {
                        var market = Object.keys(info.data.attributes)[0];
                        marketPlannerUtil.onRowUpdated(info, self.PromotionMarketParticipationIndex, self.PromotionMarketNonParticipationIndex, function () {
                            self.render();
                        });
                        var newMarketLinkModel = new marketLinkModel();
                        newMarketLinkModel.save({ Id: 0, LinkType: "ActivityMarketPromotions", SourceId: self.campaignId, TargetId: info.key.id, Market: market, Participate: info.data.attributes[market] === "Y" },
                        {
                            success: function() {
                                if (self.campaignMarketPromotionViews[info.key.id]) {
                                    self.campaignMarketPromotionViews[info.key.id].participatingMarkets = info.key.attributes.Fields[self.PromotionMarketParticipationIndex].Value;
                                    self.campaignMarketPromotionViews[info.key.id].render();
                                }
                            },
                            error: function (result, response) {
                                inRiverUtil.OnErrors(result, response);
                            }
                        });
                    },
                    columnAutoWidth: true,
                    allowColumnResizing: true,
                    paging: false,
                    scrolling: {
                        useNative: true
                    },
                    selection: {
                        mode: "none"
                    },
                    sorting: { mode: 'multiple' },
                    loadPanel: { text: 'Loading data...', showIndicator: false },
                    onContentReady: function(e) {
                        _.forEach(self.expandedPromotionRows, function (id) {
                            self.promotionGrid.expandRow(_.find(models, function (newModel) {
                                return newModel.id === id;
                            }));
                        });
                    }
                }).dxDataGrid('instance');
            } else {
                self.$el.find("#market-planner-promotion-container").text("No promotions");
            }
        }
    });

    return marketPlannerSubview;
});

