define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'devextreme',
  'jquery-ui',
  'misc/marketPlannerUtil',
  'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil',
  'sharedjs/models/relation/relationModel',
  'collections/market/campaignMarketPromotionCollection',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/marketplanner/campaignMarketPromotionTemplate.html'
], function ($, _, backbone, backboneforms, devextreme, jqueryui, marketPlannerUtil, inRiverUtil, permissionUtil, relationModel, campaignMarketPromotionCollection, entityDetailSettingWrapperView, campaignMarketPromotionTemplate) {

    var marketPlannerView = backbone.View.extend({
        initialize: function (options) {
            this.campaignId = options.campaignId;
            this.promotionId = options.promotionId;
            this.promotionModel = options.promotionModel;
            this.campaignModel = options.campaignModel;
            this.participatingMarkets = options.participatingMarkets;
            this.campaignMarketPromotionCollection = new campaignMarketPromotionCollection({ campaignId: options.campaignId, promotionId: options.promotionId });
            this.listenTo(appHelper.event_bus, "entitycreated", this.render);
            this.listenTo(appHelper.event_bus, "entityupdated", this.render);
            this.listenTo(appHelper.event_bus, "entitydeleted", this.render);
            this.render();
        },
        rowClick: function (e) {
            entityDetailSettingWrapperView.prototype.showAsPopup({
                id: e.key.id
            });
        },
        render: function () {
            var self = this;
            if (!this.$el.html()) {
                this.$el.html(campaignMarketPromotionTemplate);
            }

            this.campaignMarketPromotionCollection.fetch({
                success: function () {
                    var models = _.filter(self.campaignMarketPromotionCollection.models, function (model) {
                        return permissionUtil.CheckPermission(window.appHelper.userName, _.findWhere(model.attributes.Fields, { FieldType: "ActivityMarketPromotionMarket" }).Value);
                    });
                    var defaultFieldMapping = {
                        "PromotionStartDate": "ActivityMarketPromotionStartDate",
                        "PromotionEndDate": "ActivityMarketPromotionEndDate",
                        "PromotionValidOnSale": "ActivityMarketPromotionValidaOnSale",
                        "PromotionStepDiscount": "ActivityMarketPromotionStepDiscount"
                    };
                    var fieldDefaults = {};
                    _.each(self.promotionModel.attributes.Fields, function (field) {
                        if (defaultFieldMapping[field.FieldType]) {
                            fieldDefaults[defaultFieldMapping[field.FieldType]] = field;
                        }
                    });
                    if (models.length) {
                        var columnTemplate = [{ dataField: "attributes.DisplayName", caption: models[0].attributes.EntityTypeDisplayName, allowEditing: false, width: "442px" }];
                        var fields = models[0].attributes.Fields;
                        for (var index = 0; index < fields.length; index++) {
                            if (fields[index].FieldType !== "ActivityMarketPromotionMarket") {
                                var column = marketPlannerUtil.addToColumnTemplate(models, fields, index, columnTemplate);
                                if (column) {
                                    column.defaultField = fieldDefaults[fields[index].FieldType];
                                    column.width = "150px";
                                }
                            }
                        }
                        self.grid = self.$el.find("#campaign-market-promotion-edited-container").dxDataGrid({
                            cellHintEnabled: false,
                            dataSource: models,
                            columns: columnTemplate,
                            showColumnLines: true,
                            showRowLines: true,
                            rowClick: function (e) {
                                self.rowClick(e);
                            },
                            columnAutoWidth: true,
                            allowColumnResizing: true,
                            onCellHoverChanged: function (hoverCell) {
                                if (hoverCell.rowType === "header" || !hoverCell.column || !hoverCell.column.defaultField) {
                                    return;
                                }

                                if (hoverCell.eventType === "mouseover") {
                                    var tooltip = self.$el.find("#tooltip").dxTooltip({ target: hoverCell.cellElement, visible: true }).dxTooltip("instance");
                                    tooltip.content().html(hoverCell.column.defaultField.DisplayValue);
                                } else {
                                    self.$el.find("#tooltip").dxTooltip({ target: hoverCell.cellElement, visible: false });
                                }
                            },
                            onCellClick: function (e) {
                                if (e.rowType === "header") {
                                    return;
                                }
                                entityDetailSettingWrapperView.prototype.showAsPopup({
                                    id: e.data.id
                                });
                                e.jQueryEvent.stopPropagation();
                            },
                            onCellPrepared: function (e) {
                                if (e.rowType === "header") {
                                    return;
                                }

                                var defaultField = e.column.defaultField;
                                var isModified = false;
                                if (defaultField && e.value) {
                                    if (defaultField.Value && e.value && defaultField.FieldDataType === "DateTime") {
                                        isModified = new Date(defaultField.BrowserFriendlyValue).toDateString() !== e.value.toDateString();
                                    } else {
                                        var defaultString = defaultField.Value ? defaultField.Value.toString() : "";
                                        var valueString = e.value ? e.value.toString() : "";
                                        isModified = defaultString.toLowerCase().trim() !== valueString.toLowerCase().trim();
                                    }
                                }
                                e.cellElement.css("background", isModified ? "LightYellow" : null);
                            },
                            paging: false,
                            scrolling: {
                                useNative: true
                            },
                            selection: {
                                mode: "none"
                            },
                            sorting: { mode: 'multiple' },
                            loadPanel: { text: 'Loading data...', showIndicator: false },
                            onContentReady: function (e) { }
                        }).dxDataGrid('instance');
                    }
                    if (models.length === 0) {
                        self.$el.find("#campaign-market-promotion-edited-container").text("No participation");
                    }
                }
            });
        }
    });

    return marketPlannerView;
});

