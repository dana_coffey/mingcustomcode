﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/market/mediaChannelFromCampaignModel'
], function ($, _, Backbone, mediaChannelFromCampaignModel) {
    var mediaChannelFromCampaignCollection = Backbone.Collection.extend({
        model: mediaChannelFromCampaignModel,
        initialize: function (options) {
            if (options && options.id) {
                this.campaignId = options.id;
            }
            this.markets = null;
            if (options && options.markets) {
                this.markets = options.markets;
            }
        },
        url: function () {
            return '../../api/mediaChannelFromCampaign/' + this.campaignId;
        }
    });

    return mediaChannelFromCampaignCollection;
});
