﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/market/campaignMarketMediaChannelModel'
], function ($, _, Backbone, campaignMarketMediaChannelModel) {
    var campaignMarketMediaChannelCollection = Backbone.Collection.extend({
        model: campaignMarketMediaChannelModel,
        initialize: function (options) {
            if (options && options.campaignId) {
                this.campaignId = options.campaignId;
            }
            if (options && options.mediaChannelId) {
                this.mediaChannelId = options.mediaChannelId;
            }
            if (options && options.markets) {
                this.markets = options.markets;
            }
        },
        url: function () {
            return '../../api/campaignMarketMediaChannel?campaignId=' + this.campaignId + '&mediaChannelId=' + this.mediaChannelId;
        }
    });

    return campaignMarketMediaChannelCollection;
});
