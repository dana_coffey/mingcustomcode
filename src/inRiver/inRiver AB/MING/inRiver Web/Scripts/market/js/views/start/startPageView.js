define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'jquery-ui',
  'views/marketplanner/marketPlannerView',
  'text!templates/start/startPageTemplate.html'
], function ($, _, backbone, backboneforms, jqueryui, marketPlannerView, startPageTemplate) {

    var startMarketPage = backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            this.$el.html(new marketPlannerView().el);
            //this.$el.html(startPageTemplate);
        }
    });

    return startMarketPage;
});

