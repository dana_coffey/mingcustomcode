﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/market/campaignMarketPromotionModel'
], function ($, _, Backbone, campaignMarketPromotionModel) {
    var campaignMarketPromotionCollection = Backbone.Collection.extend({
        model: campaignMarketPromotionModel,
        initialize: function (options) {
            if (options && options.campaignId) {
                this.campaignId = options.campaignId;
            }
            if (options && options.promotionId) {
                this.promotionId = options.promotionId;
            }
            if (options && options.markets) {
                this.markets = options.markets;
            }
        },
        url: function () {
            return '../../api/campaignMarketPromotion?campaignId=' + this.campaignId + '&promotionId=' + this.promotionId;
        }
    });

    return campaignMarketPromotionCollection;
});
