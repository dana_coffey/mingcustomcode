﻿define([
  'jquery',
  'underscore',
  'backbone',
], function ($, _, Backbone) {
    var campaignParticipantCollection = Backbone.Collection.extend({
        initialize: function () {
        },
        url: function () {
            return '/api/campaignparticipant';
        }
    });

    return campaignParticipantCollection;
});
