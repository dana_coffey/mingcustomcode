﻿define([
  'underscore',
  'backbone',
  'deep-model'
], function (_, Backbone, DeepModel) {

    var promotionFromCampaignModel = Backbone.DeepModel.extend({
        idAttribute: "Id",
        initialize: function () {
        },
        urlRoot: "../../api/promotionFromCampaign"

    });

    return promotionFromCampaignModel;

});
