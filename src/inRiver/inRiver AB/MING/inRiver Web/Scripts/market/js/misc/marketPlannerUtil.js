﻿define([
  'jquery',
  'underscore',
   'sharedjs/misc/inRiverUtil',
  'sharedjs/misc/permissionUtil'

], function ($, _, inRiverUtil, permissionUtil) {
    var participation = {
        PARTICIPATING: "Y",
        NOTPARTICIPATING: "N",
        INVITED: "P",
        NOTINVITED: ""
    };
    var marketPlannerUtil = {
        formats: {
            "ar-SA" : "dd/MM/yy",
            "bg-BG" : "dd.M.yyyy",
            "ca-ES" : "dd/MM/yyyy",
            "zh-TW" : "yyyy/M/d",
            "cs-CZ" : "d.M.yyyy",
            "da-DK" : "dd-MM-yyyy",
            "de-DE" : "dd.MM.yyyy",
            "el-GR" : "d/M/yyyy",
            "en-US" : "M/d/yyyy",
            "fi-FI" : "d.M.yyyy",
            "fr-FR" : "dd/MM/yyyy",
            "he-IL" : "dd/MM/yyyy",
            "hu-HU" : "yyyy. MM. dd.",
            "is-IS" : "d.M.yyyy",
            "it-IT" : "dd/MM/yyyy",
            "ja-JP" : "yyyy/MM/dd",
            "ko-KR" : "yyyy-MM-dd",
            "nl-NL" : "d-M-yyyy",
            "nb-NO" : "dd.MM.yyyy",
            "pl-PL" : "yyyy-MM-dd",
            "pt-BR" : "d/M/yyyy",
            "ro-RO" : "dd.MM.yyyy",
            "ru-RU" : "dd.MM.yyyy",
            "hr-HR" : "d.M.yyyy",
            "sk-SK" : "d. M. yyyy",
            "sq-AL" : "yyyy-MM-dd",
            "sv-SE" : "yyyy-MM-dd",
            "th-TH" : "d/M/yyyy",
            "tr-TR" : "dd.MM.yyyy",
            "ur-PK" : "dd/MM/yyyy",
            "id-ID" : "dd/MM/yyyy",
            "uk-UA" : "dd.MM.yyyy",
            "be-BY" : "dd.MM.yyyy",
            "sl-SI" : "d.M.yyyy",
            "et-EE" : "d.MM.yyyy",
            "lv-LV" : "yyyy.MM.dd.",
            "lt-LT" : "yyyy.MM.dd",
            "fa-IR" : "MM/dd/yyyy",
            "vi-VN" : "dd/MM/yyyy",
            "hy-AM" : "dd.MM.yyyy",
            "az-Latn-AZ" : "dd.MM.yyyy",
            "eu-ES" : "yyyy/MM/dd",
            "mk-MK" : "dd.MM.yyyy",
            "af-ZA" : "yyyy/MM/dd",
            "ka-GE" : "dd.MM.yyyy",
            "fo-FO" : "dd-MM-yyyy",
            "hi-IN" : "dd-MM-yyyy",
            "ms-MY" : "dd/MM/yyyy",
            "kk-KZ" : "dd.MM.yyyy",
            "ky-KG" : "dd.MM.yy",
            "sw-KE" : "M/d/yyyy",
            "uz-Latn-UZ" : "dd/MM yyyy",
            "tt-RU" : "dd.MM.yyyy",
            "pa-IN" : "dd-MM-yy",
            "gu-IN" : "dd-MM-yy",
            "ta-IN" : "dd-MM-yyyy",
            "te-IN" : "dd-MM-yy",
            "kn-IN" : "dd-MM-yy",
            "mr-IN" : "dd-MM-yyyy",
            "sa-IN" : "dd-MM-yyyy",
            "mn-MN" : "yy.MM.dd",
            "gl-ES" : "dd/MM/yy",
            "kok-IN" : "dd-MM-yyyy",
            "syr-SY" : "dd/MM/yyyy",
            "dv-MV" : "dd/MM/yy",
            "ar-IQ" : "dd/MM/yyyy",
            "zh-CN" : "yyyy/M/d",
            "de-CH" : "dd.MM.yyyy",
            "en-GB" : "dd/MM/yyyy",
            "es-MX" : "dd/MM/yyyy",
            "fr-BE" : "d/MM/yyyy",
            "it-CH" : "dd.MM.yyyy",
            "nl-BE" : "d/MM/yyyy",
            "nn-NO" : "dd.MM.yyyy",
            "pt-PT" : "dd-MM-yyyy",
            "sr-Latn-CS" : "d.M.yyyy",
            "sv-FI" : "d.M.yyyy",
            "az-Cyrl-AZ" : "dd.MM.yyyy",
            "ms-BN" : "dd/MM/yyyy",
            "uz-Cyrl-UZ" : "dd.MM.yyyy",
            "ar-EG" : "dd/MM/yyyy",
            "zh-HK" : "d/M/yyyy",
            "de-AT" : "dd.MM.yyyy",
            "en-AU" : "d/MM/yyyy",
            "es-ES" : "dd/MM/yyyy",
            "fr-CA" : "yyyy-MM-dd",
            "sr-Cyrl-CS" : "d.M.yyyy",
            "ar-LY" : "dd/MM/yyyy",
            "zh-SG" : "d/M/yyyy",
            "de-LU" : "dd.MM.yyyy",
            "en-CA" : "dd/MM/yyyy",
            "es-GT" : "dd/MM/yyyy",
            "fr-CH" : "dd.MM.yyyy",
            "ar-DZ" : "dd-MM-yyyy",
            "zh-MO" : "d/M/yyyy",
            "de-LI" : "dd.MM.yyyy",
            "en-NZ" : "d/MM/yyyy",
            "es-CR" : "dd/MM/yyyy",
            "fr-LU" : "dd/MM/yyyy",
            "ar-MA" : "dd-MM-yyyy",
            "en-IE" : "dd/MM/yyyy",
            "es-PA" : "MM/dd/yyyy",
            "fr-MC" : "dd/MM/yyyy",
            "ar-TN" : "dd-MM-yyyy",
            "en-ZA" : "yyyy/MM/dd",
            "es-DO" : "dd/MM/yyyy",
            "ar-OM" : "dd/MM/yyyy",
            "en-JM" : "dd/MM/yyyy",
            "es-VE" : "dd/MM/yyyy",
            "ar-YE" : "dd/MM/yyyy",
            "en-029" : "MM/dd/yyyy",
            "es-CO" : "dd/MM/yyyy",
            "ar-SY" : "dd/MM/yyyy",
            "en-BZ" : "dd/MM/yyyy",
            "es-PE" : "dd/MM/yyyy",
            "ar-JO" : "dd/MM/yyyy",
            "en-TT" : "dd/MM/yyyy",
            "es-AR" : "dd/MM/yyyy",
            "ar-LB" : "dd/MM/yyyy",
            "en-ZW" : "M/d/yyyy",
            "es-EC" : "dd/MM/yyyy",
            "ar-KW" : "dd/MM/yyyy",
            "en-PH" : "M/d/yyyy",
            "es-CL" : "dd-MM-yyyy",
            "ar-AE" : "dd/MM/yyyy",
            "es-UY" : "dd/MM/yyyy",
            "ar-BH" : "dd/MM/yyyy",
            "es-PY" : "dd/MM/yyyy",
            "ar-QA" : "dd/MM/yyyy",
            "es-BO" : "dd/MM/yyyy",
            "es-SV" : "dd/MM/yyyy",
            "es-HN" : "dd/MM/yyyy",
            "es-NI" : "dd/MM/yyyy",
            "es-PR" : "dd/MM/yyyy",
            "am-ET" : "d/M/yyyy",
            "tzm-Latn-DZ" : "dd-MM-yyyy",
            "iu-Latn-CA" : "d/MM/yyyy",
            "sma-NO" : "dd.MM.yyyy",
            "mn-Mong-CN" : "yyyy/M/d",
            "gd-GB" : "dd/MM/yyyy",
            "en-MY" : "d/M/yyyy",
            "prs-AF" : "dd/MM/yy",
            "bn-BD" : "dd-MM-yy",
            "wo-SN" : "dd/MM/yyyy",
            "rw-RW" : "M/d/yyyy",
            "qut-GT" : "dd/MM/yyyy",
            "sah-RU" : "MM.dd.yyyy",
            "gsw-FR" : "dd/MM/yyyy",
            "co-FR" : "dd/MM/yyyy",
            "oc-FR" : "dd/MM/yyyy",
            "mi-NZ" : "dd/MM/yyyy",
            "ga-IE" : "dd/MM/yyyy",
            "se-SE" : "yyyy-MM-dd",
            "br-FR" : "dd/MM/yyyy",
            "smn-FI" : "d.M.yyyy",
            "moh-CA" : "M/d/yyyy",
            "arn-CL" : "dd-MM-yyyy",
            "ii-CN" : "yyyy/M/d",
            "dsb-DE" : "d. M. yyyy",
            "ig-NG" : "d/M/yyyy",
            "kl-GL" : "dd-MM-yyyy",
            "lb-LU" : "dd/MM/yyyy",
            "ba-RU" : "dd.MM.yy",
            "nso-ZA" : "yyyy/MM/dd",
            "quz-BO" : "dd/MM/yyyy",
            "yo-NG" : "d/M/yyyy",
            "ha-Latn-NG" : "d/M/yyyy",
            "fil-PH" : "M/d/yyyy",
            "ps-AF" : "dd/MM/yy",
            "fy-NL" : "d-M-yyyy",
            "ne-NP" : "M/d/yyyy",
            "se-NO" : "dd.MM.yyyy",
            "iu-Cans-CA" : "d/M/yyyy",
            "sr-Latn-RS" : "d.M.yyyy",
            "si-LK" : "yyyy-MM-dd",
            "sr-Cyrl-RS" : "d.M.yyyy",
            "lo-LA" : "dd/MM/yyyy",
            "km-KH" : "yyyy-MM-dd",
            "cy-GB" : "dd/MM/yyyy",
            "bo-CN" : "yyyy/M/d",
            "sms-FI" : "d.M.yyyy",
            "as-IN" : "dd-MM-yyyy",
            "ml-IN" : "dd-MM-yy",
            "en-IN" : "dd-MM-yyyy",
            "or-IN" : "dd-MM-yy",
            "bn-IN" : "dd-MM-yy",
            "tk-TM" : "dd.MM.yy",
            "bs-Latn-BA" : "d.M.yyyy",
            "mt-MT" : "dd/MM/yyyy",
            "sr-Cyrl-ME" : "d.M.yyyy",
            "se-FI" : "d.M.yyyy",
            "zu-ZA" : "yyyy/MM/dd",
            "xh-ZA" : "yyyy/MM/dd",
            "tn-ZA" : "yyyy/MM/dd",
            "hsb-DE" : "d. M. yyyy",
            "bs-Cyrl-BA" : "d.M.yyyy",
            "tg-Cyrl-TJ" : "dd.MM.yy",
            "sr-Latn-BA" : "d.M.yyyy",
            "smj-NO" : "dd.MM.yyyy",
            "rm-CH" : "dd/MM/yyyy",
            "smj-SE" : "yyyy-MM-dd",
            "quz-EC" : "dd/MM/yyyy",
            "quz-PE" : "dd/MM/yyyy",
            "hr-BA" : "d.M.yyyy.",
            "sr-Latn-ME" : "d.M.yyyy",
            "sma-SE" : "yyyy-MM-dd",
            "en-SG" : "d/M/yyyy",
            "ug-CN" : "yyyy-M-d",
            "sr-Cyrl-BA" : "d.M.yyyy",
            "es-US" : "M/d/yyyy"
        },
        getLocalDateFormat: function() {
            return this.formats[_.find(Object.keys(this.formats), function (key) {
                var language = navigator.userLanguage ? navigator.userLanguage : navigator.language;
                return key.indexOf(language) === 0;
            })];
        },
        addToColumnTemplate: function (models, fields, index, columnTemplate, visibleFields) {
            var column = null;
            if (!visibleFields || visibleFields.indexOf(fields[index].FieldType) > -1) {
                column = {
                    dataField: "attributes.Fields[" + index + "].DisplayValue",
                    caption: fields[index].FieldTypeDisplayName,
                    allowEditing: false
                }
                if (fields[index].FieldDataType === "DateTime") {
                    column.dataField = "attributes.Fields[" + index + "].BrowserFriendlyValue",
                    column.sortOrder = "asc";
                    column.dataType = "date";
                    column.format = this.getLocalDateFormat();
                    column.width = "150px";
                } else if (fields[index].FieldDataType === "CVL") {
                    column.dataField= "attributes.Fields[" + index + "].Value",
                    column.lookup = {
                        dataSource: fields[index].PossibleValues,
                        displayExpr: "Value",
                        valueExpr: "Key"
                    };
                }
                columnTemplate.push(column);
            }
            return column;
        },
        participationModel: [
                { Display: "Y", Value: participation.PARTICIPATING },
                { Display: "N", Value: participation.NOTPARTICIPATING },
                { Display: "P", Value: participation.INVITED }
        ],
        addParticipatingPropertiesToModels: function (models, marketsIndex, marketParticipationIndex, marketNonParticipation, participatingMarkets) {
            _.each(models, function (model) {
                var availableMarkets = typeof participatingMarkets === "undefined"
                    ? model.attributes.Fields[marketsIndex].Value
                    : _.intersection(model.attributes.Fields[marketsIndex].Value, participatingMarkets);
                _.each(model.attributes.Fields[marketsIndex].PossibleValues, function (market) {
                    var invited = _.contains(availableMarkets, market.Key);
                    var participating = _.contains(model.attributes.Fields[marketParticipationIndex].Value, market.Key);
                    var notParticipating = _.contains(model.attributes.Fields[marketNonParticipation].Value, market.Key);
                    model.attributes[market.Key] = participating ? participation.PARTICIPATING : notParticipating ? participation.NOTPARTICIPATING : invited ? participation.INVITED : participation.NOTINVITED;
                });
                model.attributes.NameAndDescription = model.attributes.DisplayName.trim() + (model.attributes.DisplayDescription.trim() ? ", " + model.attributes.DisplayDescription.trim() : "");
            });
        },
        pushMarketParticipationColumnsToTemplate: function (marketField, columnTemplate, participatingMarkets) {
            var self = this;
            _.each(marketField.PossibleValues, function (market) {
                if (permissionUtil.CheckPermission(window.appHelper.userName, market.Key) && (typeof participatingMarkets === "undefined" || _.contains(participatingMarkets, market.Key))) {
                    columnTemplate.push({
                        dataField: "attributes." + market.Key,
                        caption: market.Key,
                        allowEditing: true,
                        lookup: {
                            dataSource: self.participationModel,
                            displayExpr: "Display",
                            valueExpr: "Value"
                        },
                        width: "60px"
                    });
                }
            });
        },
        onRowUpdated: function (info, marketParticipationIndex, marketNonParticipationIndex, onError) {
            var market = Object.keys(info.data.attributes)[0];
            var participationField = info.key.attributes.Fields[marketParticipationIndex];
            var nonParticipationField = info.key.attributes.Fields[marketNonParticipationIndex];
            var marketDisplay = _.findWhere(participationField.PossibleValues, { Key: market }).Value;
            
            switch (info.data.attributes[market]) {
                case participation.PARTICIPATING:
                    if (participationField.Value) {
                        participationField.Value.push(market);
                        participationField.DisplayValue.push(marketDisplay);
                    } else {
                        participationField.Value = [market];
                        participationField.DisplayValue = [marketDisplay];
                    }
                    nonParticipationField.Value = _.without(nonParticipationField.Value, market);
                    nonParticipationField.DisplayValue = _.without(nonParticipationField.DisplayValue, marketDisplay);
                    break;
                case participation.NOTPARTICIPATING:
                    if (nonParticipationField.Value) {
                        nonParticipationField.Value.push(market);
                        nonParticipationField.DisplayValue.push(marketDisplay);
                    } else {
                        nonParticipationField.Value = [market];
                        nonParticipationField.DisplayValue = [marketDisplay];
                    }
                    participationField.Value = _.without(participationField.Value, market);
                    participationField.DisplayValue = _.without(participationField.DisplayValue, marketDisplay);
                    break;
                default:
                    nonParticipationField.Value = _.without(nonParticipationField.Value, market);
                    nonParticipationField.DisplayValue = _.without(nonParticipationField.DisplayValue, marketDisplay);
                    participationField.Value = _.without(participationField.Value, market);
                    participationField.DisplayValue = _.without(participationField.DisplayValue, marketDisplay);
                    break;
            }
            info.key.save({ Fields: info.key.get("Fields") }, {
                wait: true,
                error: function () {
                    inRiverUtil.NotifyError(
                        "Conflicting updates",
                        "The data has been modified by an other user. Please try again once the data has been reloaded.");
                    if (onError) {
                        onError();
                    }
                }
            });
        },
        onCellPrepared: function (info, participantCollection) {
            if (info.column.lookup) {
                var color;
                switch (info.value) {
                    case participation.PARTICIPATING:
                        if (participantCollection == undefined || participantCollection.models[0].attributes[info.data.id][info.column.caption] == undefined) {
                            color = "LightGreen";
                            break;
                        } else {
                            if (participantCollection.models[0].attributes[info.data.id][info.column.caption]) {
                                color = "LightGreen";
                            } else {
                                color = "LightSalmon";
                            }
                        }

                        break;
                    case participation.NOTPARTICIPATING:
                        color = "LightGray";
                        break;
                    case participation.INVITED:
                        color = "LightPink";
                        break;
                    default:
                        color = "White";
                }
                info.cellElement.css("background", color);
            }
        },
        getFieldValueFromEntity: function (model, fieldType) {
            return _.findWhere(model.Fields, { FieldType: fieldType }).Value;
        },
        getDataGridDisplayValueFromField: function (field) {
            var result = "";
            if (field) {
                if (field.BrowserFriendlyValue && field.FieldDataType === "DateTime") {
                    result = $.datepicker.formatDate(this.getLocalDateFormat().toLowerCase().replace(/yy/g, "y"), new Date(field.BrowserFriendlyValue));
                } else if (field.DisplayValue) {
                    result = field.DisplayValue;
                }
            }
            return result;
        }
    };
    return marketPlannerUtil;
});

