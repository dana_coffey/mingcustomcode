﻿define([
  'jquery',
  'underscore',
  'backbone',
  'models/market/promotionFromCampaignModel'
], function ($, _, Backbone, promotionFromCampaignModel) {
    var promotionFromCampaignCollection = Backbone.Collection.extend({
        model: promotionFromCampaignModel,
        initialize: function (options) {
            if (options && options.id) {
                this.campaignId = options.id;
            }
            this.markets = null;
            if (options && options.markets) {
                this.markets = options.markets;
            }
        },
        url: function () {
            return '../../api/promotionFromCampaign/' + this.campaignId;
        }
    });

    return promotionFromCampaignCollection;
});
