define([
  'jquery',
  'underscore',
  'backbone',
  'backbone-forms',
  'devextreme',
  'jquery-ui',
  'sharedjs/misc/inRiverUtil',
  'modalPopup',
  'misc/marketPlannerUtil',
  'sharedjs/models/comment/commentModel',
  'sharedjs/collections/campaigns/campaignCollection',
  'collections/market/campaignParticipantCollection',
  'views/marketplanner/marketPlannerSubview',
  'sharedjs/views/comment/commentsView',
  'sharedjs/views/entityview/entityDetailSettingWrapperView',
  'text!templates/marketplanner/marketPlannerTemplate.html'
], function ($, _, backbone, backboneforms, devextreme, jqueryui, inRiverUtil, modalPopup, marketPlannerUtil, commentModel, campaignCollection, campaignParticipantCollection, marketPlannerSubview, commentsView, entityDetailSettingWrapperView, marketPlannerTemplate) {

    var marketPlannerView = backbone.View.extend({
        initialize: function () {
            this.collection = new campaignCollection();
            this.participantCollection = new campaignParticipantCollection();
            this.subviews = {};
            this.reload();
        },
        events: {
            "click #tab-notanswered": "onChangeTab",
            "click #tab-answered": "onChangeTab",
            "click #tab-completed": "onChangeTab",
            "click #tab-cancelled": "onChangeTab",
            "click #action-reload": "reload"
        },
        indicateActiveTab: function () {
            this.$el.find("#tab-notanswered").removeClass("market-control-row-tab-button-active");
            this.$el.find("#tab-answered").removeClass("market-control-row-tab-button-active");
            this.$el.find("#tab-completed").removeClass("market-control-row-tab-button-active");
            this.$el.find("#tab-cancelled").removeClass("market-control-row-tab-button-active");

            this.$el.find("#" + window.appSession.filterMenuTabId).addClass("market-control-row-tab-button-active");
        },
        reload: function() {
            var self = this;
            var marketView = window.appSession.filterMenuTabId.replace('tab-', '');
            $("#loading-spinner-container").show();
            this.collection.fetch({
                data: $.param({ marketView: marketView }),
                success: function (result) {
                    self.participantCollection.fetch({
                        success: function () {
                            if (self.ActivitymarketIndex) {
                                marketPlannerUtil.addParticipatingPropertiesToModels(result.models, self.ActivitymarketIndex, self.ActivityMarketParticipationIndex, self.ActivityMarketNonParticipationIndex);
                                if (self.grid != undefined) {
                                    self.expandedRows = [];
                                    _.forEach(result.models, function (model) {
                                        if (self.grid.isRowExpanded(model)) {
                                            self.expandedRows.push(model);
                                        }
                                    });
                                    self.render();
                                    _.forEach(self.subviews, function (subview) {
                                        subview.render();
                                    });
                                    _.forEach(self.expandedRows, function (model) {
                                        self.grid.expandRow(model);
                                    });
                                }
                            } else {
                                self.render();
                            }
                            $("#loading-spinner-container").hide();
                        },
                        error: function (model, response) {
                            inRiverUtil.OnErrors(model, response);
                            $("#loading-spinner-container").hide();
                        }
                    });
                },
                error: function (model, response) {
                    inRiverUtil.OnErrors(model, response);
                    $("#loading-spinner-container").hide();
                }
            });
        },
        onChangeTab: function (e) {
            window.appSession.filterMenuTabId = e.currentTarget.id;
            this.reload();
            this.indicateActiveTab();
        },
        render: function () {
            var self = this;
            var models = this.collection.models;
            this.$el.html(marketPlannerTemplate);
            this.indicateActiveTab();
            if (models.length) {
                var columnTemplate = [{ dataField: "attributes.DisplayName", caption: models[0].attributes.EntityTypeDisplayName, allowEditing: false, width: "400px" }];
                var fields = models[0].attributes.Fields;
                var visibleFields = [
                    "ActivityStartdate",
                    "ActivityEnddate"
                ];

                columnTemplate.push({
                    dataField: "comments",
                    caption: "Comments",
                    allowEditing: false,
                    cellTemplate: function (container, options) {
                        var htmlIdElement = "id=\"comment-" + options.data.id + "\"";

                        var model = new commentModel();
                        model.url = "/api/comment/exists/" + options.data.id;

                        model.fetch({
                            success: function (result, response) {

                                var commentsIcon;

                                if (response) {
                                    commentsIcon = "<span title=\"View and add comments\" class=\"fa fa-comments-o control-row-icon-button\"></span>";
                                } else {
                                    commentsIcon = "<span title=\"View and add comments\" class=\"fa fa-comment-o control-row-icon-button\"></span>";
                                }

                                _.each(self.collection.models, function(tempModel) {
                                    if (tempModel.id == options.data.id) {
                                        
                                        var tempMarketField = tempModel.attributes.Fields[self.ActivitymarketIndex];

                                        $('<div ' + htmlIdElement + '/>').append(commentsIcon).click({ id: options.data.id, displayName: options.data.attributes.DisplayName, marketField: tempMarketField }, function (event) {
                                            var modal = new modalPopup();
                                            modal.popOut(new commentsView({ id: event.data.id, marketField: event.data.marketField }), { size: "medium", header: "Comments for " + event.data.displayName, usesavebutton: false });

                                        }).appendTo(container);

                                        return;

                                    }
                                });
                            }
                        });
                    }
                });

                for (var index = 0; index < fields.length; index++) {
                    if (_.contains(["Activitymarket", "ActivityMarketParticipation", "ActivityMarketNonParticipation"], fields[index].FieldType)) {
                        self[fields[index].FieldType + "Index"] = index;
                    }
                    marketPlannerUtil.addToColumnTemplate(models, fields, index, columnTemplate, visibleFields);
                }

                marketPlannerUtil.addParticipatingPropertiesToModels(models, self.ActivitymarketIndex, self.ActivityMarketParticipationIndex, self.ActivityMarketNonParticipationIndex);
                marketPlannerUtil.pushMarketParticipationColumnsToTemplate(fields[self.ActivitymarketIndex], columnTemplate);

                columnTemplate.push({ dataField: "", allowEditing: false, allowFiltering: false });

                self.grid = self.$el.find("#market-planner-table-container").dxDataGrid({
                    dataSource: models,
                    columns: columnTemplate,
                    filterRow: {
                        visible: true,
                        showAllText: "*"
                    },
                    showColumnLines: true,
                    showRowLines: true,
                    onCellClick: function (e) {
                        if (e.columnIndex === 1 && e.data) {
                            entityDetailSettingWrapperView.prototype.showAsPopup({
                                id: e.data.id,
                                disableAllFields: true
                            });
                        }
                    },
                    masterDetail: {
                        enabled: true,
                        template: function (container, info) {
                            if (!self.subviews[info.key.id]) {
                                self.subviews[info.key.id] = new marketPlannerSubview({ id: info.key.id, campaignModel: info.key, participatingMarkets: info.key.attributes.Fields[self.ActivityMarketParticipationIndex].Value });
                            } else {
                                self.subviews[info.key.id].render();
                            }
                            self.subviews[info.key.id].$el.appendTo(container);
                        }
                    },
                    editing: {
                        editEnabled: true,
                        editMode: "cell"
                    },
                    onCellPrepared: function (info) { marketPlannerUtil.onCellPrepared(info, self.participantCollection); },
                    onEditingStart: function (info) {
                        info.cancel = !info.data.attributes[info.column.dataField.split(".")[1]];
                    },
                    onRowUpdated: function (info) {
                        marketPlannerUtil.onRowUpdated(info, self.ActivityMarketParticipationIndex, self.ActivityMarketNonParticipationIndex, function() {
                            self.reload();
                        });
                        if (self.subviews[info.key.id]) {
                            self.subviews[info.key.id].participatingMarkets = info.key.attributes.Fields[self.ActivityMarketParticipationIndex].Value;
                            self.subviews[info.key.id].render();
                        }
                        _.forEach(self.expandedRows, function (model) {
                                self.grid.expandRow(model);
                        });
                    },
                    onRowUpdating: function (info) {
                        self.expandedRows = [];
                        _.forEach(models, function (model) {
                            if (self.grid.isRowExpanded(model)) {
                                self.expandedRows.push(model);
                            }
                        });
                    },
                    loadPanel: {
                        enabled: false
                    },
                    scrolling: {
                        useNative: false,
                        scrollByThumb: true
                    },
                    columnAutoWidth: true,
                    allowColumnResizing: true,
                    paging: false,
                    sorting: { mode: 'multiple' },
                    selection: {
                        mode: "none"
                    }
                    
                }).dxDataGrid('instance');
            } else {
                self.$el.find("#market-planner-table-container").text("No activities");
            }
        }
    });

    return marketPlannerView;
});

