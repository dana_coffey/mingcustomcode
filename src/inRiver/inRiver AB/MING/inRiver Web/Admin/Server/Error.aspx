﻿<%@ Page Title="" Language="C#" MasterPageFile="/Admin/Site.Master" AutoEventWireup="true"
    CodeBehind="Error.aspx.cs" Inherits="inRiver.Administration.Server.Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="content">
        <div class="text">
            <h3>There was a problem with the last request</h3>        
            If this error persists please contact your local support.
            <br />
            <a href="/Admin/Print/Default.aspx">Return and try again</a></div>
     </div>
</asp:Content>
