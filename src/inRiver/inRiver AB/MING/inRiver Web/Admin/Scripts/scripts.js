﻿/*$(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "../Handlers/LoginHandler.ashx",
            data: { method: "Authentication" },
            success: function (result) {
                $(".Login").replaceWith(result);

                $("#LoginUsernameInput").keypress(function (e) {
                    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                        buttonLogin();
                    }
                });

                $("#LoginPasswordInput").keypress(function (e) {
                    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                        buttonLogin();
                    }
                });
            }
        });
});*/

function ajaxError(request, type, errorThrown) {
    window.location.href = "../Server/Error.aspx";

}

function buttonLogin() {
    var username = $("#LoginUsernameInput").val();
    var password = $("#LoginPasswordInput").val();
    var remember = $("#LoginRememberMe").is(":checked");
    var pathname = getParameterByName("ReturnUrl");

    $.ajax({
        type: "POST",
        url: "../Handlers/LoginHandler.ashx",
        data: { method: "login", username: username, password: password, remember: remember, pathname: pathname },
        success: function (result) {

            if (result == "loginFailed") {
                $("#ErrorLabel").css('visibility', 'visible');
                return;
            }
            else if (result) {
                location.href = result;
            }
            else {
                location.href = "../Default.aspx";
                return;
            }
        }
    });
}

function buttonLogout() {
    $.ajax({
        type: "POST",
        url: "../Handlers/LoginHandler.ashx",
        data: { method: "logout" },
        success: function (result) {
            location.href = "../Server/Default.aspx";
        }
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function showModal(content) {
    $.modal(content, { onOpen: function (dialog) {
        dialog.overlay.fadeIn('fast', function () {
            dialog.container.slideDown('fast', function () {
                dialog.data.fadeIn('fast');
            });
        });
    }, onClose: function (dialog) {
        dialog.data.fadeOut('fast', function () {
            dialog.container.fadeOut('fast', function () {
                dialog.overlay.fadeOut('fast', function () {
                    $.modal.close();
                });
            });
        });
    }
    });
}

function closeModal() {
    $.modal.close();
}
