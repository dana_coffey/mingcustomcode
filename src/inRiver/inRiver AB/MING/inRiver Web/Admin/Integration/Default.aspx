﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="inRiver.Administration.Integration.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">

      <script type="text/javascript">

          $(document).ready(function () {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/HeaderMenu.ashx",
                  data: { area: "integration" },
                  success: function (result) {
                      $("#headerMenu").replaceWith(result);
                  }
              });

              $.ajax({
                  type: "POST",
                  url: "../Handlers/ContextMenu.ashx",
                  data: { area: "integration", selected: "home" },
                  success: function (result) {
                      $("#contextMenu").replaceWith(result);
                  }
              });

              reloadInboundConnectors();
              reloadOutboundConnectors();
          });

          function reloadOutboundConnectors() {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "GetOutboundConnectors" },
                  success: function (result) {

                      $("#outboundConnectors").replaceWith(result);

                      $("#outboundConnectorsTable").tablesorter({ headers: { 1: { sorter: false }, 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false }, 5: { sorter: false } } });
                  }
              });
          }

          function reloadInboundConnectors() {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "GetInboundConnectors" },
                  success: function (result) {

                      $("#inboundConnectors").replaceWith(result);

                      $("#inboundConnectorsTable").tablesorter({ headers: { 1: { sorter: false }, 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false }, 5: { sorter: false } } });
                  }
              });
          }

          function stopInboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "StopConnector", id: id },
                  success: function (result) {

                      reloadInboundConnectors();
                  }
              });
          }

          function stopOutboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "StopConnector", id: id },
                  success: function (result) {

                      reloadOutboundConnectors();
                  }
              });
          }

          function startInboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "StartConnector", id: id },
                  success: function (result) {

                      reloadInboundConnectors();
                  }
              });
          }

          function startOutboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "StartConnector", id: id },
                  success: function (result) {

                      reloadOutboundConnectors();
                  }
              });
          }

          function deleteInboundConnectorConfirmation(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "GetConfirmDeleteInbound", id: id },
                  success: function (result) {

                      showModal(result);
                  }
              });
          }

          function deleteInboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "DeleteConnector", id: id },
                  success: function (result) {

                      closeModal();
                      reloadInboundConnectors();
                  }
              });
          }

          function deleteOutboundConnectorConfirmation(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "GetConfirmDeleteOutbound", id: id },
                  success: function (result) {

                      showModal(result);
                  }
              });
          }

          function deleteOutboundConnector(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "DeleteConnector", id: id },
                  success: function (result) {

                      closeModal();
                      reloadOutboundConnectors();
                  }
              });
          }

          function showAddInboundConnector() {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "ShowAddInboundConnector" },
                  success: function (result) {

                      showModal(result);
                  }
              });
          }

          function addInboundConnector() {

              var id = $("#inputId").val();
              var type = $("#selectType").val();

              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "AddInboundConnector", id: id, type: type },
                  success: function (result) {
                      closeModal();
                      reloadInboundConnectors();

                  }
              });
          }

          function showAddOutboundConnector() {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "ShowAddOutboundConnector" },
                  success: function (result) {

                      showModal(result);
                  }
              });
          }

          function addOutboundConnector() {

              var id = $("#inputId").val();
              var type = $("#selectType").val();

              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "AddOutboundConnector", id: id, type: type },
                  success: function (result) {
                      closeModal();
                      reloadOutboundConnectors();

                  }
              });
          }

          function viewConfiguration(id) {
              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "ShowConfigurationForm", id: id },
                  success: function (result) {

                      showModal(result);
                  }
              });
          }

          function viewMonitor(id) {
              window.location = "Monitor.aspx?id=" + id;
          }

          function saveConnectorConfiguration(id) {

              var result = "";
              var rows = $("#connector-Settings-Table tr");
              rows.each(function (index) {
                  var value = $("td:nth-child(2) input", this);
                  var currentId = value.attr('id');

                  if (currentId == undefined) {
                      var value1 = $("td:nth-child(1) input", this);
                      currentId = value1.val();
                  }

                  result += currentId;
                  result += "|"
                  result += value.val();
                  result += "§$$inriver$$§";

              });


              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "SaveConfigurationSettings", id: id, config: result },
                  success: function (result) {

                      closeModal();
                  }
              });
          }

          function addConnectorConfiguration(id) {

              var table = $("#connector-Settings-Table");

              var newRowContent = "<tr><td><input type=\"textbox\" style=\"width:100px\"></input></td><td><input type=\"textbox\" style=\"width:200px\" ></input></td></tr>";

              table.append(newRowContent);
              //alert(id);
          }

          function viewEvents(id) {

              $.ajax({
                  type: "POST",
                  url: "../Handlers/IntegrationHandler.ashx",
                  data: { method: "ViewEvents", id: id },
                  success: function (result) {

                      $("#eventContainer").replaceWith(result);

                      $("#eventContainerTable").tablesorter();
                  }
              });
          }

       </script>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="margin-top: 20px; margin-left: 10px;">
        <div>
            <h3>Inbound Connectors</h3>
            <div id="inboundConnectors">
            </div>
            <h3>Outbound Connectors</h3>
            <div id="outboundConnectors">
            </div>
            <div id="eventContainer">
            </div>
        </div>
    </div>
</asp:Content>
