﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true"
    CodeBehind="ExtendedImport.aspx.cs" Inherits="inRiver.Administration.Integration.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">

    <style type="text/css">
        .table-min-width {
            width: 500px;
            border: none;
        }

            .table-min-width td {
                padding: 5px 5px;
            }

        .hidden {
            display: none;
        }

        #mappings {
            width: 500px;
        }

        #mappingsettingsTable td, #mappingsTable td, #mappingAdditionsTable {
            border: none;
            padding: 5px 5px;
        }
    </style>

    <script type="text/javascript">

        var connectorId = "";

        $(document).ready(function () {
           
        });

        function reloadConnectors() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtendedImportHandler.ashx",
                data: { method: "GetConnectors" },
                success: function (result) {
                    $("#connectors").replaceWith(result);
                    $("#connectorsTable").tablesorter({ headers: { 1: { sorter: false }, 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false } } });
                    hideMappingsSettings();
                }
            });
        }

        function hideMappingsSettings() {
            $("#mappings").html("");
            $("#mappingsHeader").addClass("hidden");
        }

        function showMappingsSettings() {
            $("#mappingsHeader").removeClass("hidden");
        }

        function showConnectorSettings() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtendedImportHandler.ashx",
                data: { method: "ShowConnectorSettings", valuesExist: "false" },
                success: function (result) {
                    $("#mappings").html("");
                    $("#mappings").html(result);
                    showMappingsSettings();
                }
            });

        }

        var folder;
        var importType;
        var entityType;
        var xpath;

        function WizardNext(step, next, max) {
        var id;

            if (next == 2) {
                var update = false;
                //get id from input and send to AddConnector
                if (document.getElementById("inputId") != null)
                    id = document.getElementById("inputId").value;
                else if (document.getElementById("existingInputId") != null) {
                    id = document.getElementById("existingInputId").value;
                    update = true;
                }

                folder = document.getElementById("folderPath").value;
                importType = document.getElementById("selectImportType").value;
                entityType = document.getElementById("selectEntityType").value;
                xpath = $("option:selected", $("#selectNode")).text();

                if (isValidXML == false)
                   alert("The file is not a valid XML-file!");
                else if (fileBytes == null) 
                    alert("You haven't uploaded a file");
                else if ((importType == "XML" && fileType != 'text/xml')) 
                    alert("The file is in wrong format");
                else if (importType == "XML" && xpath == "") 
                    alert("You haven't selected a node");
                else {
                    $("#loading").css({ 'display': 'block' });

                    var formdata = new FormData();
                    if (update)
                        formdata.append("method", 'EditConnectorSettings');
                    else
                        formdata.append("method", 'AddConnector');
                    formdata.append("id", id);
                    formdata.append("folder", folder);
                    formdata.append("importType", importType);
                    formdata.append("entityType", entityType);
                    formdata.append("xpath", xpath);
                    formdata.append("fieldList", fieldList);
                    if (update)
                        formdata.append("mode", "update");

                    var f = document.getElementById('files');
                    formdata.append("file", f.files[0]);

                    $.ajax({
                        type: "POST",
                        url: "../Handlers/ExtendedImportHandler.ashx",
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function(result) {

                            $("#loading").css({ 'display': 'none' });

                            if (result == "ConnectorIsNotUnique") {
                                alert("Connector id is not unique");
                            } else {
                                $("#mappings").html(result);
                            }
                        },
                        error: function() {

                            $("#loading").css({ 'display': 'none' });
                        }

                    });
                }
            }

            if (next == 1) {
                GoToFirstWizardStep();
            }
        }

        function CancelWizard(id) {
            if (id != null && id != "") {

                $.ajax({
                    type: "POST",
                    url: "../Handlers/ExtendedImportHandler.ashx",
                    data: { method: "SaveConfigurationSettings", id: id, config: "", importType: "" },
                    success: function () {
                        editConnector(id);
                    }
                });
            }
        }


        function UpdateConfigurationSettings(inputId) {
            folder = document.getElementById("folderPath").value;
            importType = document.getElementById("selectImportType").value;
            entityType = document.getElementById("selectEntityType").value;
            xpath = document.getElementById("selectNode").value;

            saveConnectorConfiguration(inputId);
        }

      function SaveConfigurationSettings(id) {
            
            saveConnectorConfiguration(id);
        }

        function saveConnectorConfiguration(id) {

            $("#loading").css({ 'display': 'block' });

            var result = "";
            
            result += "Path" + "|" + folder + "§$$inriver$$§";
            
            result += "IncludeSubDirectories" + "|" + "false" + "§$$inriver$$§";
            
            //Save extended file import settings
            var extRows = $("#mappingsTable tr");
            if (extRows.length > 0) {
                var xmlMapping = "<mappings importtype='" + importType + "' entitytype='" + entityType + "'";
                if (importType == "XML")
                    xmlMapping += " xpath='" + xpath + "'";
                xmlMapping += ">";

                if (document.getElementById("hasSysIdCol") != null && document.getElementById("hasSysIdCol").value != "") {
                    var hasSysIdCol = document.getElementById("hasSysIdCol").value;
                    if (hasSysIdCol == "True") {
                        xmlMapping += "<mapping><nodename>sys_id</nodename><fieldtype></fieldtype>";
                        xmlMapping += "</mapping>";
                    }
                }

               extRows.each(function (index) {
                    var value = $("td:nth-child(2) select", this);
                    var currentId = value.attr('id');
                    if (currentId != null && currentId != undefined) {
                        var selectedValue = $("option:selected", value).val();
                        if (selectedValue == "None")
                            selectedValue = "";
                        var language = $("option:selected", value).attr('language');

                        xmlMapping += "<mapping><nodename>" + currentId + "</nodename><fieldtype>" + selectedValue + "</fieldtype>";
                        if (language != "" && language != undefined)
                            xmlMapping += "<language>" + language + "</language>";
                        xmlMapping += "</mapping>";
                    }
                });

                xmlMapping += "</mappings>";

                result += "XML_MAPPING";
                result += "|";
                result += xmlMapping;
                result += "§$$inriver$$§";
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/ExtendedImportHandler.ashx",
                data: { method: "SaveConfigurationSettings", id: id, config: result, importType: importType },
                success: function () {
                    $("#loading").css({ 'display': 'none' });
                    editConnector(id);
                },
                error: function () {
                $("#loading").css({ 'display': 'none' });
            }
            });
        }

        function ValidateConfigurationSettings(step, next, max) {
            validateConfigurationSettings(false);
        }

       
        function editConnector(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtendedImportHandler.ashx",
                data: { method: "EditConnector", id: id },
                success: function (result) {
                    $("#mappings").html(result);

                    showMappingsSettings();
                }
            });
        }

        var fileBytes;
        var fileType;
        var nodeList = [];
        var fieldList = [];
        var parentPath;
        var $xml;
        var originalxml;
        var isValidXML;

        function isXML(xml) {
            try {
                xmlDoc = $.parseXML(xml); 
                return true;
            } catch (err) {
                return false;
            }
        }

        function handleFileSelect(files) {
            var file = files[0];

            if (file) {
                $('#selectNode').empty();
                $('#selectNodeRow').addClass("hidden");
                nodeList = [];
                parentPath = "";

                readFile(file, function (e) {
                    fileBytes = e.target.result;
                });
                fileType = file.type;

                if (file.type.match('text/xml') && $('#selectImportType').val() == 'XML') {
                    readFile(file, function (e) {

                        if (isXML(e.target.result) === false) {
                            isValidXML = false;
                            alert("The file is not a valid XML-file!");
                            return;
                        }
                        isValidXML = true;

                        originalxml = e.target.result.toString();
                        xmlDoc = $.parseXML(e.target.result);
                       
                        $xml = $.parseXML(e.target.result);

                        var root = $(xmlDoc).find("*").eq(0)[0];
                        var rootName = root.nodeName;
                        if (root.childElementCount > 0) {
                            nodeList.push([rootName, rootName]);
                        }

                        $(root.nodeName + ' > *', $xml).each(function () {
                            var xmlnode = $(this);
                            if (xmlnode.children.length > 0) {
                                parentPath = rootName + "/" + this.tagName;
                                nodeList.push([this.tagName, parentPath]);
                                $(xmlnode.children().each(processChildren));
                            }
                        });

                        $("#selectNodeRow").removeClass("hidden");
                        $('#selectNode').append($('<option></option>').val(-1).html(''));
                        $.each(nodeList, function (val, text) {
                            $('#selectNode').append($('<option></option>').val(text[0]).html(text[1]));
                        });

                        $("#selectNode").change(function () {
                            if ($(this).val() != -1) {
                                var selected = $("option:selected", this).val();
                                fieldList = [];
                                xmlDoc = $.parseXML(originalxml);
                                $xml = $(xmlDoc);
                                var node = $xml.find(selected).eq(0);
                                node.children().each(function () {
                                    if ($(this).children().length == 0) {
                                        var nodeName = this.tagName;
                                        fieldList.push(nodeName);
                                    }
                                });

                                if (fieldList.length == 0)
                                    fieldList.push(rootName);
                            }
                        });
                    });
                }
                else {
                    //Do nothing
                }
            }
        }

        function readFile(file, onLoadCallback) {
            var reader = new FileReader();
            reader.onload = onLoadCallback;
            reader.readAsText(file);
        }

        function processChildren(obj) {
            var nodeName = this.tagName;
            $child = $(this);
            if ($child.children().length > 0) {
                parentPath += "/" + nodeName;
                nodeList.push([nodeName, parentPath]);
                $child.children().each(processChildren);
            }
        }

        function deleteInboundConnectorConfirmation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/IntegrationHandler.ashx",
                data: { method: "GetConfirmDeleteInbound", id: id },
                success: function (result) {
                    showModal(result);
                }
            });
        }

        function deleteInboundConnector(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/IntegrationHandler.ashx",
                data: { method: "DeleteConnector", id: id },
                success: function (result) {
                    closeModal();
                    reloadConnectors();
                }
            });
        }

        function viewEvents(id) {

            $.ajax({
                type: "POST",
                url: "../Handlers/IntegrationHandler.ashx",
                data: { method: "ViewEvents", id: id },
                success: function (result) {

                    $("#eventContainer").replaceWith(result);

                    $("#eventContainerTable").tablesorter();
                }
            });
        }

        function startInboundConnector(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/IntegrationHandler.ashx",
                data: { method: "StartConnector", id: id },
                success: function (result) {

                    reloadConnectors();
                }
            });
        }

        function stopInboundConnector(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/IntegrationHandler.ashx",
                data: { method: "StopConnector", id: id },
                success: function (result) {

                    reloadConnectors();
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="loading">
        <img src="/Images/nine-squares-48x48.gif" />
        <div>
            <h2>Loading</h2>
        </div>
    </div>
    <div style="margin-top: 20px; margin-left: 10px;">
        <div>
            <div id="mappingsHeader" class="hidden">
                <h3>Mappings settings</h3>
            </div>
            <div id="mappings">
            </div>
            <div id="eventContainer">
            </div>
        </div>
    </div>

</asp:Content>
