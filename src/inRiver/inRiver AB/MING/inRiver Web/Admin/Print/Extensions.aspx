﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true"
    CodeBehind="Extensions.aspx.cs" Inherits="inRiver.Administration.Print.Extensions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {
                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "extensions" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadCustomAttributes();

            reloadFormatters();
        });

        function reloadFormatters() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "GetAllFormatters" },
                success: function (result) {

                    $("#formatterContent").replaceWith(result);

                    $("#formatterTable").tablesorter({ headers: { 1: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function reloadCustomAttributes() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "GetAllCustomAttributes" },
                success: function (result) {

                    $("#customAttributeContent").replaceWith(result);

                    $("#customAttributeTable").tablesorter({ headers: { 2: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function showAddFormatter() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "AddFormatterForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function showAddCustomAttribute() {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "AddCustomAttributeForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addFormatter() {

            var type = $("#selectType").val();

            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "AddFormatter", type: type },
                success: function (result) {
                    closeModal();
                    reloadFormatters();
                },
                error: ajaxError
            });
        }

        function addCustomAttribute() {

            var type = $("#selectType").val();

            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "AddCustomAttribute", type: type },
                success: function (result) {
                    closeModal();
                    reloadCustomAttributes();
                },
                error: ajaxError
            });
        }

        function deleteCustomAttributeConfirmation(id) {

            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "DeleteCustomAttributeConfirmation", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteFormatterConfirmation(id) {

            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "DeleteFormatterConfirmation", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteFormatter(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "DeleteFormatter", id: id },
                success: function (result) {
                    closeModal();
                    reloadFormatters();
                },
                error: ajaxError
            });
        }

        function deleteCustomAttribute(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/ExtensionHandler.ashx",
                data: { method: "DeleteCustomAttribute", id: id },
                success: function (result) {
                    closeModal();
                    reloadCustomAttributes();
                },
                error: ajaxError
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="formatterContent">
    </div>
    <div id="customAttributeContent">
    </div>
</asp:Content>
