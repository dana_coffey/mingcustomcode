﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="AttributeSet.aspx.cs"
    Inherits="inRiver.Administration.Print.AttributeSet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "attributeSet" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

        });

        function reloadAttributeSet() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "GetAttributeSetPage" },
                success: function (result) {
                    $("#definitionContent").replaceWith(result);
                    $("#attributeSetTable").treeTable({});
                    $("#attributeSetTable").tablesorter({ headers: { 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false } } });
                },
                error: ajaxError
            });
        }

        function reloadAttributeSetImpl(implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAttributeSetImplPage" },
                success: function (result) {
                    $("#attributeSetImplContent").replaceWith(result);
                    $("#attributeSetImplTable").treeTable({});
                    $("#attributeSetImplTable").tablesorter({ headers: { 1: { sorter: false }, 3: { sorter: false }, 4: { sorter: false }, 5: { sorter: false }, 6: { sorter: false } } });

                    if (implementationId != undefined || implementationId != null) {
                        var checkIfClassNameSelectedRowExists = document.getElementsByClassName("selectedRow");

                        if (checkIfClassNameSelectedRowExists.length > 0) {
                            for (i = 0; i < checkIfClassNameSelectedRowExists.length; i++) {
                                $(checkIfClassNameSelectedRowExists[i]).removeClass("selectedRow");
                            }
                        }

                        var selectedRow = document.getElementById("node-" + implementationId);
                        $(selectedRow).addClass("selectedRow");
                    }
                },
                error: ajaxError
            });
        }

        function reloadAttributeSetChainTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAttributeSetChainPage" },
                success: function (result) {
                    $("#attributeSetChainContent").replaceWith(result);
                    $("#attributeSetChainTable").treeTable({});
                    $("#attributeSetChainTable").tablesorter({ headers: { 1: { sorter: false }, 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false } } });
                },
                error: ajaxError
            });
        }

        //ATTRIBUTE SET
        //**********************************************************************************//

        //ADD

        function showAddAttributeSet() {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetHandler.ashx',
                data: { method: "GetAddAttributeForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeSet() {
            var inputName = document.getElementById("inputName").value;
            var selectType = document.getElementById("selectType");
            var selectedType = "";

            for (i = 0; i < selectType.children.length; i++) {
                if (!selectType.children[i].selected) {
                    continue;
                }

                selectedType = selectType.children[i].text;
                break;
            }

            var selectAttributeSetGroup = document.getElementById("selectAttributeSetGroup").value;

            $.ajax({
                type: "POST",
                url: '../Handlers/PrintAttributeSetHandler.ashx',
                data: { method: "AddAttributeSet", name: inputName, type: selectedType, attributeSetGroup: selectAttributeSetGroup },
                success: function (result) {
                    if (result != "0") {
                        alert(result);
                        return;
                    }
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //EDIT

        function editAttributeSetForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "GetEditAttributeSetForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttributeSet(id) {
            var name = document.getElementById("inputName").value;
            var selectType = document.getElementById("selectType");

            var selectedType = "";

            for (i = 0; i < selectType.children.length; i++) {
                if (!selectType.children[i].selected) {
                    continue;
                }

                selectedType = selectType.children[i].text;
                break;
            }

            var selectAttributeSetGroup = document.getElementById("selectAttributeSetGroup").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "EditAttributeSet", id: id, name: name, type: selectedType, parentFolder: selectAttributeSetGroup },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //DELETE

        function deleteAttributeSetForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "GetDeleteAttributeSetForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeSet(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "DeleteAttributeSet", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //COPY

        function copyAttributeSetForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "GetCopyAttributeSetForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function copyAttributeSet(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "CopyAttributeSet", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }
        //**********************************************************************************//


        //ATTRIBUTE SET GROUP
        //**********************************************************************************//

        //ADD
        function showAddAttributeSetGroup() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "AddAttributeSetGroupForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeSetGroup() {
            var inputName = document.getElementById("inputName").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "AddAttributeSetGroup", name: inputName },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //EDIT
        function editAttributeSetGroupForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "EditAttributeSetGroupForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttributeSetGroup(id) {
            var inputName = document.getElementById("inputName").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "EditAttributeSetGroup", id: id, name: inputName },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //DELETE

        function deleteAttributeSetGroupForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "DeleteAttributeSetGroupForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeSetGroup(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetHandler.ashx",
                data: { method: "DeleteAttributeSetGroup", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttributeSet();
                },
                error: ajaxError
            });
        }

        //**************************************************************************************************************************

        //Implementations of AttributeSets

        function getAttributeSetImplementationsMappingTable(id, implementationId, tableColumnId) {

            var checkIfClassNameSelectedRowExists = document.getElementsByClassName("selectedRow");

            if (checkIfClassNameSelectedRowExists.length > 0) {
                for (i = 0; i < checkIfClassNameSelectedRowExists.length; i++) {
                    $(checkIfClassNameSelectedRowExists[i]).removeClass("selectedRow");
                }
            }

            var selectedRow = document.getElementById("node-" + implementationId);
            $(selectedRow).addClass("selectedRow");


            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAttributeSetImplementationsMappingTable", id: id, implementationId: implementationId },
                success: function (result) {
                    $("#editionsRelatedToImpl").html("");
                    $("#attributesRelatedToImpl").replaceWith(result);


                    //TEXT TABLE
                    $("#attributeSetImplementationsMappingTable").tableDnD({
                        onDrop: function (table, row) {
                            var tableRows = [];

                            for (i = 0; i < table.tBodies[0].rows.length; i++) {
                                var attributeId = table.tBodies[0].rows[i].children[1].children[0].className;

                                tableRows.push(attributeId);
                            }

                            $.ajax({
                                type: "POST",
                                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                                data: { method: "UpdateAttributesOrderForImpl", implementationId: implementationId, data: tableRows.join("[row]") },
                                success: function (result) {
                                    getAttributeSetImplementationsMappingTable(id, implementationId);
                                },
                                error: ajaxError
                            });
                        }
                    });

                    //TABLE TABLE
                    $("#attributeSetWithTypeTableMappingTable").tableDnD({
                        onDrop: function (table, row) {
                            var tableRows = [];

                            for (i = 0; i < table.tBodies[0].rows.length; i++) {
                                var attributeId = table.tBodies[0].rows[i].children[1].children[0].className;

                                tableRows.push(attributeId);
                            }

                            $.ajax({
                                type: "POST",
                                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                                data: { method: "UpdateAttributesOrderInTable", implementationId: implementationId, data: tableRows.join("[row]") },
                                success: function (result) {

                                },
                                error: ajaxError
                            });
                        }
                    });

                    if (tableColumnId != undefined) {
                        columnRowClickedShowAttributes(id, tableColumnId, implementationId, false);
                    }
                },
                error: ajaxError
            });
        }

        function columnRowClickedShowAttributes(attributeSetId, tableColumnId, implementationId, reloadTable) {

            if (reloadTable == undefined || reloadTable == null || reloadTable == false) {
                var checkIfAnyRowIsSelected = document.getElementsByClassName("selectedColumn");

                var selectedRow = $("#attributeSetWithTypeTableMappingTable tr:hover");
                $(selectedRow).addClass("selectedColumn");

                if (checkIfAnyRowIsSelected.length != 0) {
                    for (i = 0; i < checkIfAnyRowIsSelected.length; i++) {
                        if (checkIfAnyRowIsSelected[i].uniqueNumber != selectedRow[0].uniqueNumber) {
                            $(checkIfAnyRowIsSelected[i]).removeClass("selectedColumn");
                        }
                    }
                }
            }
            else {
                var columnRow = document.getElementById(tableColumnId);

                $(columnRow).addClass("selectedColumn");

            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAttributesForSelectedColumn", id: attributeSetId, tableColumnId: tableColumnId },
                success: function (result) {
                    $("#attributesInSelectedColumn").replaceWith(result);

                    $("#attributesInSelectedColumn").tableDnD({
                        onDrop: function (table, row) {
                            var tableRows = [];

                            for (i = 0; i < table.tBodies[0].rows.length; i++) {
                                var attributeId = table.tBodies[0].rows[i].className;

                                tableRows.push(attributeId);
                            }

                            $.ajax({
                                type: "POST",
                                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                                data: { method: "UpdateLinkedAttributesToTableColumn", implementationId: implementationId, tableColumnId: tableColumnId, data: tableRows.join("[row]") },
                                success: function (result) {

                                },
                                error: ajaxError
                            });
                        }
                    });

                },
                error: ajaxError
            });
        }

        function addSelectedAttributeToColumn(attributeSetId, implementationId, attributeDefId) {

            var selectedColumnToAddAttribute = document.getElementsByClassName("selectedColumn");
            if (selectedColumnToAddAttribute.length == 0) {
                alert("To add attribute, please select a row!");
                return;
            }

            var tableColumnId = selectedColumnToAddAttribute[0].id;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "AddSelectedAttributeToTableColumn", tableColumnId: tableColumnId, attributeDefId: attributeDefId, id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    columnRowClickedShowAttributes(attributeSetId, tableColumnId, implementationId, true);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAllAttributesAvailable", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    $("#availableAttributes").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeFromTableColumn(attributeSetId, attributeId, tableColumnId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteAttributeFromTableColumn", tableColumnId: tableColumnId, attributeMappingId: attributeId, implementationId: implementationId },
                success: function (result) {
                    $.ajax({
                        type: "POST",
                        url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                        data: { method: "GetAttributesForSelectedColumn", tableColumnId: tableColumnId },
                        success: function (result) {
                            $("#attributesInSelectedColumn").replaceWith(result);
                        },
                        error: ajaxError
                    });

                    $.ajax({
                        type: "POST",
                        url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                        data: { method: "GetAllAttributesAvailable", id: attributeSetId, implementationId: implementationId },
                        success: function (result) {
                            $("#availableAttributes").replaceWith(result);
                            columnRowClickedShowAttributes(attributeSetId, tableColumnId, implementationId, true);
                        },
                        error: ajaxError
                    });
                },
                error: ajaxError
            });
        }

        //CHANGE PRIORITY IN TABLE FORM
        function getChangePriorityForAttribute(attributeSetId, implId, attributeMappingId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetChangePriorityForAttributeForm", id: attributeSetId, implementationId: implId, attributeMappingId: attributeMappingId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //CHANGE PRIORITY IN TABLE
        function changePriorityForAttribute(AttributeSetId, implId, attributeMappingId) {
            var inputPriority = document.getElementById("inputPriority").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "ChangePriorityForAttribute", id: AttributeSetId, implementationId: implId, attributeMappingId: attributeMappingId, inputPriority: inputPriority },
                success: function (result) {
                    closeModal();
                    getAttributeSetImplementationsMappingTable(AttributeSetId, implId);
                },
                error: ajaxError
            });
        }

        //GET ADD ATTRIBUTESET IN IMPLEMENTATION FORM
        function showAddAttributeSetImplementation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetShowAddImplementationsForAttributeSetForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //ADD ATTRIBUTESET IN IMPLEMENTATION
        function addImplementationsForAttributeSet(id) {
            var inputName = document.getElementById("inputName").value;

            var removeRowsIfEmpty;
            var checkIfInputRemoveRowsIfEmptyIsNUll = document.getElementById("inputRemoveRowIfEmpty");

            if (checkIfInputRemoveRowsIfEmptyIsNUll != null) {
                removeRowsIfEmpty = checkIfInputRemoveRowsIfEmptyIsNUll.checked;
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "AddImplementationsForAttributeSet", id: id, name: inputName, removeRowsIfEmpty: removeRowsIfEmpty },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                },
                error: ajaxError
            });
        }

        //GET EDIT EDITION IN IMPLEMENTATION FORM
        function getEditionsForImplementationsTable(AttributeSetId, ImplementationId) {
            var checkIfClassNameSelectedRowExists = document.getElementsByClassName("selectedRow");

            if (checkIfClassNameSelectedRowExists.length > 0) {
                for (i = 0; i < checkIfClassNameSelectedRowExists.length; i++) {
                    $(checkIfClassNameSelectedRowExists[i]).removeClass("selectedRow");
                }
            }

            var selectedRow = document.getElementById("node-" + ImplementationId);
            $(selectedRow).addClass("selectedRow");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetEditionsForImplementationTable", id: AttributeSetId, implementationId: ImplementationId },
                success: function (result) {
                    $("#attributesRelatedToImpl").html("");
                    $("#attributeSetRelatedToImplWithTypeTable").html("");
                    $("#editionsRelatedToImpl").replaceWith(result);
                },
                error: ajaxError
            });
        }

        //GET EDIT EDITION IN IMPLEMENTATION FOR ATTRIBUTE SET FORM
        function getEditImplementationForAttributeSet(attributeSetId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetEditImplementationsForAttributeSetForm", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //EDIT IMPLEMENTATION FOR ATTRIBUTESET
        function editImplementationsForAttributeSet(id, implementationid) {
            var inputName = document.getElementById("inputName").value;
            var removeRowsIfEmpty;
            var checkIfInputRemoveRowsIfEmptyIsNUll = document.getElementById("inputRemoveRowIfEmpty");

            if (checkIfInputRemoveRowsIfEmptyIsNUll != null) {
                removeRowsIfEmpty = checkIfInputRemoveRowsIfEmptyIsNUll.checked;
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "EditImplementationsForAttributeSet", id: id, implementationId: implementationid, name: inputName, removeRowsIfEmpty: removeRowsIfEmpty },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                    //getEditionsForImplementationsTable(id, implementationid);
                },
                error: ajaxError
            });
        }

        //GET DELETE IMPLEMENTATION FOR ATTRIBUTESET
        function getDeleteImplementationForAttributeSet(attributeSetId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetDeleteImplementationsForAttributeSetForm", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //DELETE IMPLEMENTATION FOR ATTRIBUTESET
        function deleteImplementationForAttributeSet(id, implementationid) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteImplementationsForAttributeSet", id: id, implementationId: implementationid },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                },
                error: ajaxError
            });
        }

        //ADD ATTRIBUTES FOR IMPLEMENTATION
        function addAttributesForImplementation(id, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "getAddAttributesForImplementationForm", id: id, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                    $("#attributesMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                    $(".multiSelectOptions").css("height", "200px");
                    $(".multiSelectOptions").css("width", "300px");
                },
                error: ajaxError
            });
        }

        //ADD CHOSEN ATTRIBUTE FOR IMPLEMENTATION
        function addChosenAttributesForImplementation(id, implementationId) {
            var attributes = [];
            $.each($("input[name='attributesMultivalue[]']:checked"), function () {
                attributes.push($(this).val());
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "AddAttributesForImplementation", implementationId: implementationId, data: attributes.toString() },
                success: function (result) {
                    closeModal();
                    getAttributeSetImplementationsMappingTable(id, implementationId);
                },
                error: ajaxError
            });
        }


        //GET ADD EDITION FOR IMPLEMENTATION FORM
        function getAddEditionsForImplementation(attributeId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAddEditionsForImplementationForm", id: attributeId, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                    $("#editionsMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                },
                error: ajaxError
            });
        }

        //ADD CHOSEN EDITION FOR IMPLEMENTATION
        function addChosenEditionsForImplementation(attributeSetId, implementationId) {
            var attributes = [];
            $.each($("input[name='editionsMultivalue[]']:checked"), function () {
                attributes.push($(this).val());
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "AddEditionsForImplementationForm", id: attributeSetId, implementationId: implementationId, data: attributes.join('[stop]') },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl(implementationId);
                    getEditionsForImplementationsTable(attributeSetId, implementationId);
                },
                error: ajaxError
            });
        }

        //GET DELETE EDITION FOR IMPLEMENTATION FROM
        function getRemoveEditionForAttributeSetImpl(attributeSetId, implId, editionId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetDeleteEditionForImplementationForm", id: attributeSetId, implementationId: implId, editionId: editionId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //DELETE EDITION FOR IMPLEMENTATION
        function deleteEditionForImplementation(attributeSetId, implId, editionId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteEditionForImplementation", id: attributeSetId, implementationId: implId, editionId: editionId },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl(implId);
                    getEditionsForImplementationsTable(attributeSetId, implId);
                },
                error: ajaxError
            });
        }

        //GET DELETE ATTRIBUTES FOR IMPLEMENTATION FORM
        function getDeleteAttributesForImplementationForm(attributeSetId, implId, attributeMappingId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetDeleteAttributesForImplementationForm", id: attributeSetId, implementationId: implId, attributeMappingId: attributeMappingId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //DELETE ATTRIBUTES FOR IMPLEMENTATION
        function deleteAttributesForImplementation(attributeSetId, implId, attributeMappingId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteAttributeForImplementation", id: attributeSetId, implementationId: implId, attributeMappingId: attributeMappingId },
                success: function (result) {
                    closeModal();
                    getAttributeSetImplementationsMappingTable(attributeSetId, implId);

                },
                error: ajaxError
            });
        }

        //GET ADD ATTRIBUTESET TO IMPLEMENTATION, TYPE: TABLE
        function getAddAttributeSetWithTypeTableToImplementationForm(AttributeSetId, ImplementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAddAttributeForColumnInImplementationForm", id: AttributeSetId, implementationId: ImplementationId },
                success: function (result) {
                    showModal(result);
                    $("#attributesMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                    $(".multiSelectOptions").css("height", "200px");
                    $(".multiSelectOptions").css("width", "300px");
                },
                error: ajaxError
            });
        }

        //ADD ATTRIBUTESET TO IMPLEMENTATION, TYPE: TABLE
        function addAttributeSetWithTypeTableToImplementation(attributeSetId, implementationId) {
            var selectedTitle = document.getElementById("chooseTitleForm").checked;

            var inputIdentifier = document.getElementById("inputIdentifier").value;
            var inputRemoveIfEmpty = document.getElementById("inputRemoveIfEmpty").checked;
            var selectWidth = document.getElementById("selectWidth");
            var selectedWidthInMM = document.getElementById("inputWidthInMM").value;

            var selectedWidth = "";

            for (i = 0; i < selectWidth.children.length; i++) {
                if (!selectWidth.children[i].selected) {
                    continue;
                }

                selectedWidth = selectWidth.children[i].text;
                break;
            }

            var selectedAttributes = [];
            $.each($("input[name='attributesMultivalue[]']:checked"), function () {
                selectedAttributes.push($(this).val());
            });

            if (selectedTitle) {
                var selectTextVariables = document.getElementById("selectTextVariables");

                var selectedTextVariables = "";

                for (i = 0; i < selectTextVariables.children.length; i++) {
                    if (!selectTextVariables.children[i].selected) {
                        continue;
                    }

                    selectedTextVariables = selectTextVariables.children[i].text;
                    break;
                }

                $.ajax({
                    type: "POST",
                    url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                    data: { method: "AddAttributeForColumnInImplementation", id: attributeSetId, implementationId: implementationId, title: "", identifier: inputIdentifier, removeIfEmpty: inputRemoveIfEmpty, width: selectedWidth, textVariables: selectedTextVariables, attributes: selectedAttributes.join('[stop]'), selectedWidthInMM: selectedWidthInMM },
                    success: function (result) {
                        if (result == "WidthInMMFailed") {
                            alert("WidthInMM needs to be numeric")
                            return;
                        }

                        closeModal();
                        getAttributeSetImplementationsMappingTable(attributeSetId, implementationId);
                    },
                    error: ajaxError
                });
            }
            else {
                var inputTitle = document.getElementById("inputTitle").value;

                $.ajax({
                    type: "POST",
                    url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                    data: { method: "AddAttributeForColumnInImplementation", id: attributeSetId, implementationId: implementationId, title: inputTitle, identifier: inputIdentifier, removeIfEmpty: inputRemoveIfEmpty, width: selectedWidth, textVariables: "", attributes: selectedAttributes.join('[stop]'), selectedWidthInMM: selectedWidthInMM },
                    success: function (result) {
                        if (result == "WidthInMMFailed") {
                            alert("WidthInMM needs to be numeric");
                            return;
                        }
                        closeModal();
                        getAttributeSetImplementationsMappingTable(attributeSetId, implementationId);
                    },
                    error: ajaxError
                });
            }
        }

        //GET EDIT ATTRIBUTESET TO IMPLEMENTATION, TYPE:TABLE
        function getEditAttributeSetWithTypeTableToImplementationForm(AttributeSetId, ImplementationId, tableColumnId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetEditAttributeForColumnInImplementationForm", id: AttributeSetId, implementationId: ImplementationId, tableColumnId: tableColumnId },
                success: function (result) {
                    showModal(result);
                    $("#attributesMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                    $(".multiSelectOptions").css("height", "200px");
                    $(".multiSelectOptions").css("width", "300px");
                },
                error: ajaxError
            });
        }

        //EDIT ATTRIBUTESET TO IMPLEMENTATION, TYPE:TABLE
        function editAttributeSetWithTypeTableToImplementation(attributeSetId, implementationId, tableColumnId) {
            var selectedTitle = document.getElementById("chooseTitleForm").checked;

            if (selectedTitle) {
                var inputIdentifier = document.getElementById("inputIdentifier").value;
                var inputRemoveIfEmpty = document.getElementById("inputRemoveIfEmpty").checked;
                var selectWidth = document.getElementById("selectWidth");
                var selectedWidthInMM = document.getElementById("inputWidthInMM").value;


                var selectedWidth = "";

                for (i = 0; i < selectWidth.children.length; i++) {
                    if (!selectWidth.children[i].selected) {
                        continue;
                    }

                    selectedWidth = selectWidth.children[i].text;
                    break;
                }

                var selectTextVariables = document.getElementById("selectTextVariables");

                var selectedTextVariables = "";

                for (i = 0; i < selectTextVariables.children.length; i++) {
                    if (!selectTextVariables.children[i].selected) {
                        continue;
                    }

                    selectedTextVariables = selectTextVariables.children[i].text;
                    break;
                }

                var selectedAttributes = [];
                $.each($("input[name='attributesMultivalue[]']:checked"), function () {
                    selectedAttributes.push($(this).val());
                });

                $.ajax({
                    type: "POST",
                    url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                    data: { method: "EditAttributeForColumnInImplementation", id: attributeSetId, implementationId: implementationId, title: "", identifier: inputIdentifier, removeIfEmpty: inputRemoveIfEmpty, width: selectedWidth, textVariables: selectedTextVariables, attributes: selectedAttributes.join('[stop]'), tableColumnId: tableColumnId, selectedWidthInMM: selectedWidthInMM },
                    success: function (result) {
                        if (result == "WidthInMMFailed") {
                            alert("WidthInMM needs to be numeric")
                            return;
                        }

                        closeModal();
                        getAttributeSetImplementationsMappingTable(attributeSetId, implementationId);
                    },
                    error: ajaxError
                });
            }
            else {
                var inputTitle = document.getElementById("inputTitle").value;
                var inputIdentifier = document.getElementById("inputIdentifier").value;
                var inputRemoveIfEmpty = document.getElementById("inputRemoveIfEmpty").checked;
                var selectWidth = document.getElementById("selectWidth");
                var selectedWidthInMM = document.getElementById("inputWidthInMM").value;

                var selectedWidth = "";

                for (i = 0; i < selectWidth.children.length; i++) {
                    if (!selectWidth.children[i].selected) {
                        continue;
                    }

                    selectedWidth = selectWidth.children[i].text;
                    break;
                }

                var selectedAttributes = [];
                $.each($("input[name='attributesMultivalue[]']:checked"), function () {
                    selectedAttributes.push($(this).val());
                });

                $.ajax({
                    type: "POST",
                    url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                    data: { method: "EditAttributeForColumnInImplementation", id: attributeSetId, implementationId: implementationId, title: inputTitle, identifier: inputIdentifier, removeIfEmpty: inputRemoveIfEmpty, width: selectedWidth, textVariables: "", attributes: selectedAttributes.join('[stop]'), tableColumnId: tableColumnId, selectedWidthInMM: selectedWidthInMM },
                    success: function (result) {
                        if (result == "WidthInMMFailed") {
                            alert("WidthInMM needs to be numeric")
                            return;
                        }

                        closeModal();
                        getAttributeSetImplementationsMappingTable(attributeSetId, implementationId);
                    },
                    error: ajaxError
                });

            }
        }

        //CHANGE TITLE CONTROLL
        function inAddColumnFormChangeTitle(attributeSetId, implementationid, tableColumnid) {
            var chosen = document.getElementById("chooseTitleForm").checked;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetTitleControll", chosen: chosen, id: attributeSetId, implementationId: implementationid, tableColumnId: tableColumnid },
                success: function (result) {

                    $("#selectionOfTitleTR").replaceWith(result);
                },
                error: ajaxError
            });
        }

        //GET DELETE ATTRIBUTESET TO IMPLEMENTATION, TYPE:TABLE
        function getDeleteAttributeSetWithTypeTableToImplementationForm(AttributeSetId, ImplementationId, tableColumnId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetDeleteAttributeForColumnInImplementationForm", id: AttributeSetId, implementationId: ImplementationId, tableColumnId: tableColumnId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //DELETE ATTRIBUTESET TO IMPLEMENTATION, TYPE:TABLE
        function deleteAttributeForColumnInImplementation(attributeSetId, implementationId, tableColumnId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteAttributeForColumnInImplementation", id: attributeSetId, implementationId: implementationId, tableColumnId: tableColumnId },
                success: function (result) {
                    closeModal();
                    getAttributeSetImplementationsMappingTable(attributeSetId, implementationId);
                },
                error: ajaxError
            });
        }

        //*************************************************************************************************//
        //ATTRIBUTE SET CHAIN

        function showAddAttributeSetChain() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAddAttributeSetChainForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeSetChain() {
            var name = document.getElementById("inputName").value

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "AddAttributeSetChain", name: name },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //EDIT

        function editAttributeSetChainForm(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "GetEditAttributeSetChainForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttributeSetChain(id) {
            var name = document.getElementById("inputName").value

            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "EditAttributeSetChain", id: id, name: name },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //DELETE

        function deleteAttributeSetChainForm(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "GetDeleteAttributeSetChainForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeSetChain(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "DeleteAttributeSetChain", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //**********************************************************************************//

        //OTHERS

        //**********************************************************************************//

        function showAttributeSetsInChain(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAllAttributeSetsForChain", id: id },
                success: function (result) {
                    $("#attributeSetsChainTable").replaceWith(result);
                    $("#attributeSetsChainTable").tableDnD();
                },
                error: ajaxError
            });
        }

        function saveAttributeSetsOrder(id) {
            var tableRows = [];
            var rowsElements = document.getElementsByClassName(id);

            for (i = 0; i < rowsElements.length; i++) {
                var attributeSetId = rowsElements[i].children[1].children[0].id;

                tableRows.push(attributeSetId);
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "UpdateAttributeSetsForChain", id: id, data: tableRows.join("[row]") },
                success: function (result) {
                    
                },
                error: ajaxError
            });

        }

        function addAttributeSetsToChain(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAddAttributeSetsForChainForm", id: id },
                success: function (result) {
                    showModal(result)
                    $("#attributeSetsChanMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                    $(".multiSelectOptions").css("height", "200px");
                    $(".multiSelectOptions").css("width", "300px");

                },
                error: ajaxError
            });
        }

        function addChosenAttributeSets(id) {
            var attributeSets = $("#attributeSetsChanMultivalue")[0].getAttribute('title').split(", ");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "AddAttributeSetsForChain", id: id, data: attributeSets.toString() },
                success: function (result) {
                    closeModal();
                    showAttributeSetsInChain(id);
                },
                error: ajaxError
            });

        }

        function getDeleteAttributeSetForChainForm(AttributeSetChainId, AttributeSetId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetDeleteAttributeSetsForChainForm", id: AttributeSetChainId, attributeSetId: AttributeSetId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeSetFromChain(id, attributeSetId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "DeleteAttributeSetsForChain", id: id, attributeSetId: attributeSetId },
                success: function (result) {
                    closeModal();
                    showAttributeSetsInChain(id);
                },
                error: ajaxError
            });
        }      

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="tabsWrapper">
        <ul class="idTabs">
            <li><a class="selected" onclick="javascript:reloadAttributeSet();" href="#definitionContent">
                Definition</a></li>
            <li><a onclick="javascript:reloadAttributeSetImpl();" href="#attributeSetImplContent">
                Implementations</a></li>
            <li><a onclick="javascript:reloadAttributeSetChainTable();" href="#attributeSetChainContent">
                Attribute Set Chain</a></li>
        </ul>
    </div>
    <div id="definitionContent">
    </div>
    <div id="attributeSetImplContent">
    </div>
    <div id="attributeSetChainContent">
    </div>
</asp:Content>
