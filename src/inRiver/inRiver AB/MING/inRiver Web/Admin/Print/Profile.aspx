﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" MasterPageFile="~/Admin/Site.Master"
    Inherits="inRiver.Administration.Print.Profile" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">

        //DOCUMENT
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {
                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "edition" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            //reloadEditionsTable();
        });

        function customNameCheck() {
//            var customNameChecked = document.getElementById("inputCustomName").checked;

//            if (!customNameChecked) {
//                CreateEditionName();
//                return;
//            }

//            var inputName = document.getElementById("inputName");
//            var emptyString = "";
//            $(inputName).val(emptyString)
        }

        //EDITION
        //**********************************************************//
        function reloadEditionsTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "GetAllEditions" },
                success: function (result) {

                    $("#profileContent").replaceWith(result);

                    $("#profileTable").tablesorter({ headers: { 7: { sorter: false }, 8: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function editEditionForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditEditionForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function showAddEdition() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddEditionForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteEditionConfirmation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteEditionForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteEdition(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteEdition", id: id },
                success: function (result) {
                    closeModal();
                    reloadEditionsTable();
                },
                error: ajaxError
            });
        }

        function addEdition() {
            var name = document.getElementById("inputName").value;
            var country = document.getElementById("selectedCountry").value;
            var language = document.getElementById("selectLanguage").value;
            var commonLanguage = document.getElementById("inputCommonLanguage").checked;
            var profileCode = document.getElementById("inputProfileCode").value;
            var businessUnit = document.getElementById("selectBusinessUnit").value;
            var unitType = document.getElementById("selectUnitType").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddEdition", name: name, country: country, language: language, commonLanguage: commonLanguage, profileCode: profileCode, businessUnit: businessUnit, unitType: unitType },
                success: function (result) {
                    if (result == "1") {
                        alert("Please insert data in all existing dropdowns/fields!");
                        return;
                    }
                    closeModal();
                    reloadEditionsTable();
                },
                error: ajaxError
            });
        }

        function editEdition(id) {
            var name = document.getElementById("inputName").value;
            var country = document.getElementById("selectedCountry").value;
            var language = document.getElementById("selectLanguage").value;
            var commonLanguage = document.getElementById("inputCommonLanguage").checked;
            var profileCode = document.getElementById("inputProfileCode").value;
            var businessUnit = document.getElementById("selectBusinessUnit").value;
            var unitType = document.getElementById("selectUnitType").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditEdition", id: id, name: name, country: country, language: language, commonLanguage: commonLanguage, profileCode: profileCode, businessUnit: businessUnit, unitType: unitType },
                success: function (result) {
                    if (result == "1") {
                        alert("please insert data in all existing dropdown/fields!");
                        return;
                    }
                    closeModal();
                    reloadEditionsTable();
                },
                error: ajaxError
            });
        }

        function CreateEditionName() {
            var customNameChecked = document.getElementById("inputCustomName").checked;

            if (customNameChecked) {
                return;
            }

            var language = document.getElementById("selectLanguage");
            var selectedLanguage = "";

            for (i = 0; i < language.children.length; i++) {
                if (!language.children[i].selected) {
                    continue;
                }

                if (language.children[i].value == "-1") {
                    break;
                }

                selectedLanguage = language.children[i].text;
                break;

            }

            var profileCode = document.getElementById("inputProfileCode").value;

            var businessUnit = document.getElementById("selectBusinessUnit");
            var selectedBusinessUnit = "";

            for (j = 0; j < businessUnit.children.length; j++) {
                if (!businessUnit.children[j].selected) {
                    continue;
                }

                if (businessUnit.children[j].value == "-1") {
                    break;
                }

                selectedBusinessUnit = businessUnit.children[j].text;
                break;
            }

            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintProfileHandler.ashx',
                data: { method: "CreateEditionName", language: selectedLanguage, profileCode: profileCode, businessUnit: selectedBusinessUnit },
                success: function (result) {
                    $("#inputName").val(result);
                },
                error: ajaxError
            });

        }

        //COUNTRIES
        //**********************************************************//
        function reloadCountriesTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "GetAllCountries" },
                success: function (result) {

                    $("#countryContent").replaceWith(result);

                    $("#countryTable").tablesorter({ headers: { 7: { sorter: false }, 8: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function showAddCountry() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddCountryForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addCountry() {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddCountry", name: name, identifier: identifier },
                success: function (result) {
                    closeModal();
                    reloadCountriesTable();
                },
                error: ajaxError
            });

        }

        function editCountryForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditCountryForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editCountry(id) {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditCountry", id: id, name: name, identifier: identifier },
                success: function (result) {
                    closeModal();
                    reloadCountriesTable();
                },
                error: ajaxError
            });
        }

        function deleteCountryConfirmation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteCountryForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteCountry(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteCountry", id: id },
                success: function (result) {
                    closeModal();
                    reloadCountriesTable();
                },
                error: ajaxError
            });
        }

        //BUSINESSUNIT
        //**********************************************************//
        function reloadBusinessUnitTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "GetAllBusinessUnit" },
                success: function (result) {

                    $("#businessUnitContent").replaceWith(result);

                    $("#businessUnitTable").tablesorter({ headers: { 7: { sorter: false }, 8: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function showAddBusinessUnit() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddBusinessUnitForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addBusinessUnit() {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddBusinessUnit", name: name, identifier: identifier },
                success: function (result) {
                    closeModal();
                    reloadBusinessUnitTable();
                },
                error: ajaxError
            });
        }

        function editBusinessUnitForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditBusinessUnitForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editBusinessUnit(id) {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditBusinessUnit", name: name, identifier: identifier, id: id },
                success: function (result) {
                    closeModal();
                    reloadBusinessUnitTable();
                },
                error: ajaxError
            });
        }

        function deleteBusinessUnitConfirmation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteBusinessUnitForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteBusinessUnit(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteBusinessUnit", id: id },
                success: function (result) {
                    closeModal();
                    reloadBusinessUnitTable();
                },
                error: ajaxError
            });
        }

        //UNITTYPE
        //**********************************************************//
        function reloadUnitTypeTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "GetAllUnitTypes" },
                success: function (result) {

                    $("#unitTypeContent").replaceWith(result);

                    $("#unitTypeTable").tablesorter({ headers: { 7: { sorter: false }, 8: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function showAddUnitType() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddUnitTypeForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addUnitType() {
            var name = document.getElementById("inputName").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "AddUnitType", name: name },
                success: function (result) {
                    closeModal();
                    reloadUnitTypeTable();
                },
                error: ajaxError
            });

        }

        function editUnitTypeForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditUnitTypeForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editUnitType(id) {
            var name = document.getElementById("inputName").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "EditUnitType", id: id, name: name },
                success: function (result) {
                    closeModal();
                    reloadUnitTypeTable();
                },
                error: ajaxError
            });
        }

        function deleteUnitTypeConfirmation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteUnitTypeForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteUnitType(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintProfileHandler.ashx",
                data: { method: "DeleteUnitType", id: id },
                success: function (result) {
                    closeModal();
                    reloadUnitTypeTable();
                },
                error: ajaxError
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="tabsWrapper">
        <ul class="idTabs">
            <li><a class="selected" onclick="javascript:reloadEditionsTable();" href="#profileContent">
                Edition</a></li>
            <li><a onclick="javascript:reloadCountriesTable();" href="#countryContent">Country</a></li>
            <li><a onclick="javascript:reloadBusinessUnitTable();" href="#businessUnitContent">Business
                Unit</a></li>
            <li><a onclick="javascript:reloadUnitTypeTable();" href="#unitTypeContent">Unit Type</a></li>
        </ul>
    </div>
    <div id="profileContent">
    </div>
    <div id="countryContent">
    </div>
    <div id="businessUnitContent">
    </div>
    <div id="unitTypeContent">
    </div>
</asp:Content>
