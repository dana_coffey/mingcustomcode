﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="Attribute.aspx.cs"
    Inherits="inRiver.Administration.Print.Attribute" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "attribute" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadAttribute();
        });

        function reloadAttribute() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "GetAttributePage" },
                success: function (result) {
                    $("#attributeContent").replaceWith(result);
                    $("#attributeTable").treeTable({});
                    $("#attributeTable").tablesorter({ headers: { 2: { sorter: false }, 3: { sorter: false } } });
                },
                error: ajaxError
            });
        }

        function showAddAttribute() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "GetAddAttributeForm" },
                success: function (result) {
                    showModal(result);
                    $("#attributeAllFieldsTable").treeTable({
                        expand: true
                    });
                },
                error: ajaxError
            });
        }

        function addAttribute() {
            var fieldTypes = [];

            var treeTableRows = $("#attributeAllFieldsTableBody tr");

            for (i = 0; i < treeTableRows.length; i++) {
                if ($(treeTableRows[i]).hasClass("parent")) {
                    continue;
                }

                if (treeTableRows[i].children[0] == null) {
                    continue;
                }

                var inputElement = treeTableRows[i].children[0].children[0];

                if (inputElement.checked != true) {
                    continue;
                }

                fieldTypes.push(inputElement.id);
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "AddAttribute", data: fieldTypes.join('[stop]') },
                success: function (result) {
                    if (result == "-1") {
                        return;
                    }

                    closeModal();
                    reloadAttribute();
                },
                error: ajaxError
            });
        }

        function editAttributeForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "GetEditAttributeForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttribute(id) {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;
            var metaAttribute = document.getElementById("inputMetaAttribute").checked;
            var defaultFetchStrategy = document.getElementById("inputDefaultFetchStrategy").checked;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "EditAttribute", id: id, name: name, identifier: identifier, metaAttribute: metaAttribute, defaultFetchStrategy: defaultFetchStrategy },
                success: function (result) {

                    if (result == "1") {
                        alert("Use alphanumeric charaters only");

                    }
                    else {
                        closeModal();
                        reloadAttribute();
                    }
                },
                error: ajaxError
            });
        }

        function deleteAttributeForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "GetDeleteAttributeForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttribute(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeHandler.ashx",
                data: { method: "DeleteAttribute", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttribute();
                },
                error: ajaxError
            });
        }   

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="attributeContent">
    </div>
</asp:Content>
