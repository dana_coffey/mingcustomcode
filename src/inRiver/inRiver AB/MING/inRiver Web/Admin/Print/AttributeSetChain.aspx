﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Site.Master" CodeBehind="AttributeSetChain.aspx.cs"
    Inherits="inRiver.Administration.Print.AttributeSetChain" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "attributeSetChain" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadAttributeSetChainTable();
        });

        function reloadAttributeSetChainTable() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAttributeSetChainPage" },
                success: function (result) {
                    $("#attributeSetChainContent").replaceWith(result);
                    $("#attributeSetChainTable").treeTable({});
                },
                error: ajaxError
            });
        }

        function showAddAttributeSetChain() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAddAttributeSetChainForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeSetChain() {
            var name = document.getElementById("inputName").value

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "AddAttributeSetChain", name: name },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //EDIT

        function editAttributeSetChainForm(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "GetEditAttributeSetChainForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttributeSetChain(id) {
            var name = document.getElementById("inputName").value

            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "EditAttributeSetChain", id: id, name: name },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //DELETE

        function deleteAttributeSetChainForm(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "GetDeleteAttributeSetChainForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeSetChain(id) {
            $.ajax({
                type: 'POST',
                url: '../Handlers/PrintAttributeSetChainHandler.ashx',
                data: { method: "DeleteAttributeSetChain", id: id },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetChainTable();
                },
                error: ajaxError
            });
        }

        //**********************************************************************************//

        //OTHERS

        //**********************************************************************************//

        function showAttributeSetsInChain(id) {           

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAllAttributeSetsForChain", id: id },
                success: function (result) {
                    $("#attributeSetsChainTable").replaceWith(result);
                    $("#attributeSetsChainTable").tableDnD(); 
                },
                error: ajaxError
            });
        }

        function saveAttributeSetsOrder(id) {
            var tableRows = [];
            var rowsElements = document.getElementsByClassName(id);

            for (i = 0; i < rowsElements.length; i++) {
                var attributeSetId = rowsElements[i].children[1].children[0].id;

                tableRows.push(attributeSetId);
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "UpdateAttributeSetsForChain", id: id, data: tableRows.toString() },
                success: function (result) {

                },
                error: ajaxError
            });
                
        }

        function addAttributeSetsToChain(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetAddAttributeSetsForChainForm", id: id },
                success: function (result) {
                    showModal(result);
                    $("#attributeSetsChanMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });
                    $(".multiSelectOptions").css("height", "200px");
                    $(".multiSelectOptions").css("width", "250px");
                },
                error: ajaxError
            });
        }

        function addChosenAttributeSets(id) {
            var attributeSets = [];
            $.each($("input[name='attributeSetsChanMultivalue[]']:checked"), function () {
                attributeSets.push($(this).val());
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "AddAttributeSetsForChain", id: id, data: attributeSets.toString() },
                success: function (result) {
                    closeModal();
                    showAttributeSetsInChain(id);
                },
                error: ajaxError
            });

        }

        function getDeleteAttributeSetForChainForm(AttributeSetChainId, AttributeSetId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "GetDeleteAttributeSetsForChainForm", id: AttributeSetChainId, attributeSetId: AttributeSetId },
                success: function (result) {
                    showModal(result)
                },
                error: ajaxError
            });
        }

        function deleteAttributeSetFromChain(id, attributeSetId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetChainHandler.ashx",
                data: { method: "DeleteAttributeSetsForChain", id: id, attributeSetId: attributeSetId },
                success: function (result) {
                    closeModal();
                    showAttributeSetsInChain(id);
                },
                error: ajaxError
            });    
        }

        //**********************************************************************************//


    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="attributeSetChainContent">
    </div>
</asp:Content>
