﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="ObjectTemplate.aspx.cs"
    Inherits="inRiver.Administration.Print.ObjectTemplate" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "objectTemplate" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadObjectTemplate();
        });

        function reloadObjectTemplate() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "GetAllObjectTemplates" },
                success: function (result) {

                    $("#objectTemplateContent").replaceWith(result);

                    $("#objectTemplateTable").tablesorter({ headers: { 2: { sorter: false }, 3: { sorter: false }, 4: { sorter: false}} });
                },
                error: ajaxError
            });
        }

        function showAddObjectTemplate() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "AddObjectTemplateForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addObjectTemplate() {

            var name = $("#inputName").val();
            var folder = $("#selectAttributeSetGroup").val();
            var snippet = $("#inputSnippet").val();

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "AddObjectTemplate", name: name, folder: folder, snippet: snippet },
                success: function (result) {

                    closeModal();
                    reloadObjectTemplate();
                },
                error: ajaxError
            });
        }

        function editObjectTemplateForm(id) {

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "EditObjectTemplateForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function updateObjectTemplate(id) {

            var name = $("#inputName").val();
            var folder = $("#selectAttributeSetGroup").val();

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "UpdateObjectTemplate", id: id, name: name, folder: folder },
                success: function (result) {

                    closeModal();
                    reloadObjectTemplate();
                },
                error: ajaxError
            });
        }

        function deleteObjectTemplateConfirmation(id, name) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "DeleteObjectTemplateForm", id: id, name: name },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteObjectTemplate(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "DeleteObjectTemplate", id: id },
                success: function (result) {
                    closeModal();
                    reloadObjectTemplate();
                },
                error: ajaxError
            });
        }

        function objectTemplateDetails(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintObjectTemplateHandler.ashx",
                data: { method: "GetObjectTemplateDetailForm", id: id},
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="objectTemplateContent">
    </div>
</asp:Content>
