﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="Formatting.aspx.cs"
    Inherits="inRiver.Administration.Print.Formatting" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "formatting" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadFormatting();
        });

        function reloadFormatting() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetFormattingPage" },
                success: function (result) {
                    $("#formattingContent").replaceWith(result);
                    $("#formattingTable").tablesorter();
                },
                error: ajaxError
            });
        }

        //SELECT ATTRIBUTE
        function attributeSelected(attributeId) {
            $("#formatterEditForm").html("<div id='formatterEditForm'></div>");
            $("#formatterAttributeMapping").html("<div id='formatterAttributeMapping'></div>");

            var selectedAttribute = document.getElementsByClassName("selectedRow");

            if (selectedAttribute.length > 0) {
                for (i = 0; i < selectedAttribute.length; i++) {
                    $(selectedAttribute[i]).removeClass("selectedRow");
                }
            }

            var formatter = document.getElementsByClassName(attributeId);

            $(formatter[0]).addClass("selectedRow");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetFormatterLinkedToSelectedAttributeTable", attributeId: attributeId },
                success: function (result) {
                    $("#formattersLinkedToAttribute").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function reloadAttributeSelected(attributeId) {
            var selectedAttribute = document.getElementsByClassName("selectedRow");

            if (selectedAttribute.length > 0) {
                for (i = 0; i < selectedAttribute.length; i++) {
                    $(selectedAttribute[i]).removeClass("selectedRow");
                }
            }

            var formatter = document.getElementsByClassName(attributeId);

            $(formatter[0]).addClass("selectedRow");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "ReloadFormatterLinkedToSelectedAttributeTable", attributeId: attributeId },
                success: function (result) {
                    $("#formattersLinkedToAttributeTable").replaceWith(result);
                },
                error: ajaxError
            });
        }

        //GET FORM FOR ADD FORMATTER FOR ATTRIBUTE
        function getAddFormatterForAttributeForm(attributeId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetAddFormatterForAttributeForm", attributeId: attributeId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        //ADD FORMATTER FOR ATTRIBUTE
        function addFormatterForAttribute(attributeId) {
            var inputName = document.getElementById("inputName").value;

            var selectFormatter = document.getElementById("selectFormatter");
            var selectedFormatter = "";

            for (i = 0; i < selectFormatter.children.length; i++) {
                if (!selectFormatter.children[i].selected) {
                    continue;
                }

                selectedFormatter = selectFormatter.children[i].value;
                break;
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "AddFormatterForAttribute", attributeId: attributeId, name: inputName, formatter: selectedFormatter },
                success: function (result) {
                    closeModal();
                    attributeSelected(attributeId);
                },
                error: ajaxError
            });
        }

        //GET FORM FOR DELETE FORMATTER FOR ATTRIBUTE
        function getDeleteFormatterForAttributeForm(attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetDeleteFormatterForAttributeForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteFormatterForAttribute(attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "DeleteFormatterForAttribute", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    closeModal();
                    attributeSelected(attributeId);
                },
                error: ajaxError
            });
        }

        function getFormatterForm(attributeId, formatterId) {
            var checkIfClassNameSelectedRowExists = document.getElementsByClassName("selectedRowFormatter");

            if (checkIfClassNameSelectedRowExists.length > 0) {
                for (i = 0; i < checkIfClassNameSelectedRowExists.length; i++) {
                    $(checkIfClassNameSelectedRowExists[i]).removeClass("selectedRowFormatter");
                }
            }

            var selectedRow = document.getElementById(formatterId);
            $(selectedRow).addClass("selectedRowFormatter");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetFormatterEditForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#formatterAttributeMapping").replaceWith("<div id='formatterAttributeMapping'></div>");
                    $("#formatterEditForm").replaceWith(result);

                    reloadPredefinedPatterns(attributeId, formatterId);
                },
                error: ajaxError
            });

        }

        function getFormatterAttributeMappingForm(attributeId, formatterId) {
            var checkIfClassNameSelectedRowExists = document.getElementsByClassName("selectedRowFormatter");

            if (checkIfClassNameSelectedRowExists.length > 0) {
                for (i = 0; i < checkIfClassNameSelectedRowExists.length; i++) {
                    $(checkIfClassNameSelectedRowExists[i]).removeClass("selectedRowFormatter");
                }
            }

            var selectedRow = document.getElementById(formatterId);
            $(selectedRow).addClass("selectedRowFormatter");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetFormatterAttributeMappingForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#formatterEditForm").replaceWith("<div id='formatterEditForm'></div>");
                    $("#formatterAttributeMapping").replaceWith(result);
                },
                error: ajaxError
            });

        }

        function getAddAttributeFormatMappingForm(attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetAddAttributeFormatMappingForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeFormatMapping(attributeId, formatterId) {
            var selectProfile = document.getElementById("selectProfile");
            var selectedProfile = "";

            for (i = 0; i < selectProfile.children.length; i++) {
                if (!selectProfile.children[i].selected) {
                    continue;
                }

                selectedProfile = selectProfile.children[i].value;
                break;
            }

            var selectAttributeset = document.getElementById("selectAttributeset");
            var selectedAttributeset = "";

            for (i = 0; i < selectAttributeset.children.length; i++) {
                if (!selectAttributeset.children[i].selected) {
                    continue;
                }

                selectedAttributeset = selectAttributeset.children[i].value;
                break;
            }


            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "AddAttributeFormatMapping", attributeId: attributeId, formatterId: formatterId, attributeSetId: selectedAttributeset, editionId: selectedProfile },
                success: function (result) {
                    closeModal();
                    getFormatterAttributeMappingForm(attributeId, formatterId);
                },
                error: ajaxError
            });
        }

        function getEditAttributeFormatForm(attributeFormatId, attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetEditAttributeFormatForm", attributeFormatId: attributeFormatId, attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editAttributeFormat(attributeFormatId, attributeId, formatterId) {
            var selectProfile = document.getElementById("selectProfile");
            var selectedProfile = "";

            for (i = 0; i < selectProfile.children.length; i++) {
                if (!selectProfile.children[i].selected) {
                    continue;
                }

                selectedProfile = selectProfile.children[i].value;
                break;
            }

            var selectAttributeset = document.getElementById("selectAttributeset");
            var selectedAttributeset = "";

            for (i = 0; i < selectAttributeset.children.length; i++) {
                if (!selectAttributeset.children[i].selected) {
                    continue;
                }

                selectedAttributeset = selectAttributeset.children[i].value;
                break;
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "EditAttributeFormat", attributeFormatId: attributeFormatId, attributeSetId: selectedAttributeset, editionId: selectedProfile },
                success: function (result) {
                    closeModal();
                    getFormatterAttributeMappingForm(attributeId, formatterId);
                },
                error: ajaxError
            });
        }

        function getDeleteAttributeFormatForm(attributeFormatId, attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetDeleteAttributeFormatForm", attributeFormatId: attributeFormatId, attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteAttributeFormat(attributeFormatId, attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "DeleteAttributeFormat", attributeFormatId: attributeFormatId },
                success: function (result) {
                    closeModal();
                    getFormatterAttributeMappingForm(attributeId, formatterId);
                },
                error: ajaxError
            });
        }

        function savePatternToFormatter(attributeId, formatterId) {
            var inputDescription = document.getElementById("inputDescription").value;
            var selectFormatterClass = document.getElementById("selectFormatterClass");
            var selectedFormatterClass = "";

            for (i = 0; i < selectFormatterClass.children.length; i++) {
                if (!selectFormatterClass.children[i].selected) {
                    continue;
                }

                selectedFormatterClass = selectFormatterClass.children[i].value;
                break;
            }

            var textareaFormatterDescription = document.getElementById("textareaFormatterDescription").value;
            var textareaPattern = document.getElementById("textareaPattern").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "SavePatternToSelectedFormatterAttribute", attributeId: attributeId, formatterId: formatterId, description: inputDescription, formatterClass: selectedFormatterClass, formatterDescription: textareaFormatterDescription, pattern: textareaPattern },
                success: function (result) {
                    //RELOAD ALL DETAILS AND TABS
                    //$("#formatterEditForm").replaceWith("<div id='formatterEditForm'></div>");
                    //attributeSelected(attributeId);
                    getFormatterForm(attributeId, formatterId);
                    reloadAttributeSelected(attributeId);

                },
                error: ajaxError
            });
        }

        function cancelFormatterEdit(attributeId, formatterId) {
            getFormatterForm(attributeId, formatterId);
        }

        function formatterClassChanged(attributeId, formatterId) {
            var selectFormatterClass = document.getElementById("selectFormatterClass");
            var selectedFormatterClass = "";

            for (i = 0; i < selectFormatterClass.children.length; i++) {
                if (!selectFormatterClass.children[i].selected) {
                    continue;
                }

                selectedFormatterClass = selectFormatterClass.children[i].value;
                break;
            }

            var selectedTab = $(".selected");

            var selectedTabId = selectedTab[0].innerHTML;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetFormatterDetailsFormOnFormatterClassChange", attributeId: attributeId, formatterId: formatterId, formatterClass: selectedFormatterClass, selectedTabId: selectedTabId },
                success: function (result) {
                    $("#formatterAttributeMapping").replaceWith("<div id='formatterAttributeMapping'></div>");
                    $("#formatterEditForm").replaceWith(result);

                    //Se vilken som har selected den tabben ska refreshas.

                    reloadPredefinedPatterns(attributeId, formatterId);

//                    if (selectedTabId == "Predefined Pattern") {
//                        reloadPredefinedPatterns(attributeId, formatterId);
//                    }

//                    if (selectedTabId == "KeyWords") {
//                        reloadKeyWords(attributeId, formatterId);
//                    }

//                    if (selectedTabId == "Text Variables") {
//                        reloadTextVariables(attributeId, formatterId);
//                    }

//                    if (selectedTabId == "Test Pattern") {
//                        reloadTestPatterns(attributeId, formatterId);
//                    }
                },
                error: ajaxError
            });
        }

        function reloadPredefinedPatterns(attributeId, formatterId) {
            var selectedTab = document.getElementsByClassName("selected");

            if (selectedTab.length > 0) {
                for (i = 0; i < selectedTab.length; i++) {
                    $(selectedTab[i]).removeClass("selected");
                }
            }

            $(".predefinedPatternsContent").addClass("selected");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetPredefinedPatternsTableForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#predefinedPatternsContent").replaceWith(result);
                    $("#testPatternContent").html("");
                    $("#textVariablesContent").html("");
                    $("#keyWordsContent").html("");
                },
                error: ajaxError
            });
        }

        function reloadKeyWords(attributeId, formatterId) {

            var selectedTab = document.getElementsByClassName("selected");

            if (selectedTab.length > 0) {
                for (i = 0; i < selectedTab.length; i++) {
                    $(selectedTab[i]).removeClass("selected");
                }
            }

            $(".keyWordsContent").addClass("selected");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetKeyWordsTableForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#keyWordsContent").replaceWith(result);
                    $("#testPatternContent").html("");
                    $("#textVariablesContent").html("");
                    $("#predefinedPatternsContent").html("");
                },
                error: ajaxError
            });
        }


        function reloadTextVariables(attributeId, formatterId) {
            var selectedTab = document.getElementsByClassName("selected");

            if (selectedTab.length > 0) {
                for (i = 0; i < selectedTab.length; i++) {
                    $(selectedTab[i]).removeClass("selected");
                }
            }

            $(".textVariablesContent").addClass("selected");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetTextVariablesTableForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#textVariablesContent").replaceWith(result);
                    $("#testPatternContent").html("");
                    $("#keyWordsContent").html("");
                    $("#predefinedPatternsContent").html("");
                },
                error: ajaxError
            });
        }

        function insertSelectedTextVariable(textVariableId, attributeId, formatterId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "InsertSelectedTextVariable", attributeId: attributeId, formatterId: formatterId, textVariableId: textVariableId },
                success: function (result) {
                    $("#textareaPattern").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function reloadTestPatterns(attributeId, formatterId) {
            var selectedTab = document.getElementsByClassName("selected");

            if (selectedTab.length > 0) {
                for (i = 0; i < selectedTab.length; i++) {
                    $(selectedTab[i]).removeClass("selected");
                }
            }

            $(".testPatternContent").addClass("selected");

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "GetTestPatternsTableForm", attributeId: attributeId, formatterId: formatterId },
                success: function (result) {
                    $("#testPatternContent").replaceWith(result);
                    $("#textVariablesContent").html("");
                    $("#keyWordsContent").html("");
                    $("#predefinedPatternsContent").html("");
                },
                error: ajaxError
            });
        }

        function insertSelectedKeyWords(attributeId, formatterId, keyWordsValue) {
            var textAreaPattern = document.getElementById("textareaPattern").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "InsertSelectedKeyWord", attributeId: attributeId, formatterId: formatterId, keyWordsValue: keyWordsValue, textAreaPattern: textAreaPattern },
                success: function (result) {
                    $("#textareaPattern").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function insertSelectedPredefinedPattern(attributeId, formatterId, predefinedPatternValue) {
            var textAreaPattern = document.getElementById("textareaPattern").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "InsertSelectedPredefinedPattern", attributeId: attributeId, formatterId: formatterId, predefinedPatternValue: predefinedPatternValue, textAreaPattern: textAreaPattern },
                success: function (result) {
                    $("#textareaPattern").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function addPredefinedValueToTextInputValue(attributeId, formatterId) {
            var patternOutput = document.getElementById("inputPatternOutPut").value;

            var selectPredefinedValue = document.getElementById("selectPredefinedValue");
            var selectedPredefinedValue = "";

            for (i = 0; i < selectPredefinedValue.children.length; i++) {
                if (!selectPredefinedValue.children[i].selected) {
                    continue;
                }

                selectedPredefinedValue = selectPredefinedValue.children[i].value;
                break;
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "AddPredefinedValueToTextInputValue", attributeId: attributeId, formatterId: formatterId, predefinedPatternValue: selectedPredefinedValue, patternOutput: patternOutput },
                success: function (result) {
                    //$("#textareaPattern").html("");
                    //$("#textareaPattern").replaceWith(result);
                    $("#inputTextInputValue").replaceWith(result);
                    //Add to input ----> inputPatternOutPut
                },
                error: ajaxError
            });
        }

        function predefinedPatternSelectedShowPattern(attributeId, formatterId, predefinedPatternValue) {

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "ShowSelectedPatternInPredefinedPatternRightSection", attributeId: attributeId, formatterId: formatterId, predefinedPatternValue: predefinedPatternValue },
                success: function (result) {
                    $("#keyWordsDescriptionId").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function addTextInputValueToPatternOutput(attributeId, formatterId) {
            var inputTextInputValue = document.getElementById("inputTextInputValue").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "AddTextInputValueToPatternOutput", attributeId: attributeId, formatterId: formatterId, inputTextInputValue: inputTextInputValue },
                success: function (result) {
                    $("#inputPatternOutPut").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function selectedKeywordsShowDescription(attributeId, formatterId, keywordValue) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintFormattingHandler.ashx",
                data: { method: "ShowKeyWordDescription", attributeId: attributeId, formatterId: formatterId, keyWordsValue: keywordValue },
                success: function (result) {
                    $("#keyWordsDescriptionId").replaceWith(result);   
                },
                error: ajaxError
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="formattingContent">
    </div>
</asp:Content>
