﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Site.Master" CodeBehind="AttributeSetImpl.aspx.cs"
    Inherits="inRiver.Administration.Print.AttributeSetImpl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {

                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "attributeSetImpl" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadAttribute();
        });

        function reloadAttributeSetImpl() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAttributeSetImplPage" },
                success: function (result) {
                    $("#attributeSetImplContent").replaceWith(result);
                    $("#attributeSetImplTable").treeTable({});
                },
                error: ajaxError
            });
        }

        //Implementations of AttributeSets

        function getAttributeSetImplementationsMappingTable(id, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAttributeSetImplementationsMappingTable", id: id, implementationId: implementationId },
                success: function (result) {
                    $("#attributeSetImplementationsMappingTable").replaceWith(result);
                },
                error: ajaxError
            });
        }

        function showAddAttributeSetImplementation(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetShowAddImplementationsForAttributeSetForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addImplementationsForAttributeSet(id) {
            var inputName = document.getElementById("inputName").value;

//            var selectAttributeSet = document.getElementById("selectAttributeSet");
//            var attributeSetId;
//            for (i = 0; i < selectAttributeSet.length; i++) {
//                if (selectAttributeSet[i].selected) {
//                    attributeSetId = selectAttributeSet[i].value;
//                    break;
//                }
//            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "AddImplementationsForAttributeSet", id: id, name: inputName },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                },
                error: ajaxError
            });
        }

        function getEditImplementationForAttributeSet(attributeSetId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetEditImplementationsForAttributeSetForm", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editImplementationsForAttributeSet(id, implementationid) {
            var inputName = document.getElementById("inputName").value;
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "EditImplementationsForAttributeSet", id: id, implementationId: implementationid, name: inputName },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                },
                error: ajaxError
            });
        }

        function getDeleteImplementationForAttributeSet(attributeSetId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetDeleteImplementationsForAttributeSetForm", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteImplementationForAttributeSet(id, implementationid) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "DeleteImplementationsForAttributeSet", id: id, implementationId: implementationid },
                success: function (result) {
                    closeModal();
                    reloadAttributeSetImpl();
                },
                error: ajaxError
            });
        }

        function getAddAttributeSetWithTypeTableToImplementationForm(attributeSetId, implementationId) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAddAttributeForColumnInImplementationForm", id: attributeSetId, implementationId: implementationId },
                success: function (result) {
                    $("#attributesMultivalue").multiSelect({ selectAll: false, oneOrMoreSelected: '*' });

                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addAttributeSetWithTypeTableToImplementation(attributeSetId, implementationId) {
            var inputTitle = document.getElementById("inputTitle").value;
            var inputIdentifier = document.getElementById("inputIdentifier").value;
            var inputRemoveIfEmpty = document.getElementById("inputRemoveIfEmpty").checked;
            var selectWidth = document.getElementById("selectWidth");

            var selectedWidth = "";

            for (i = 0; i < selectWidth.children.length; i++) {
                if (!selectWidth.children[i].selected) {
                    continue;
                }

                selectedWidth = selectWidth.children[i].text;
                break;
            }

            var selectTextVariables = document.getElementById("selectTextVariables");

            var selectedTextVariables = "";

            for (i = 0; i < selectTextVariables.children.length; i++) {
                if (!selectTextVariables.children[i].selected) {
                    continue;
                }

                selectedTextVariables = selectTextVariables.children[i].text;
                break;
            }

            //attributesMultivalue

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintAttributeSetImplHandler.ashx",
                data: { method: "GetAddAttributeForColumnInImplementationForm", id: attributeSetId, implementationId: implementationId, title: inputTitle, identifier: inputIdentifier, removeIfEmpty: inputRemoveIfEmpty, width: selectedWidth, textVariables: selectedTextVariables },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });   
        }

        function getEditAttributeSetWithTypeTableToImplementationForm(AttributeSetId, ImplementationId, tableColumnId) {

        }

        function getDeleteAttributeSetWithTypeTableToImplementationForm(AttributeSetId, ImplementationId, tableColumnId) {

        }



    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="attributeSetImplContent">
    </div>
</asp:Content>
