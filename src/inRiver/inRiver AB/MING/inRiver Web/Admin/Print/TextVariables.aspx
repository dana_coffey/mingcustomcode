﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="TextVariables.aspx.cs" Inherits="inRiver.Administration.Print.TextVariables" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "../Handlers/HeaderMenu.ashx",
                data: { area: "print" },
                success: function (result) {
                    $("#headerMenu").replaceWith(result);
                },
                error: ajaxError
            });

            $.ajax({
                type: "POST",
                url: "../Handlers/ContextMenu.ashx",
                data: { area: "print", selected: "textVariables" },
                success: function (result) {
                    $("#contextMenu").replaceWith(result);
                },
                error: ajaxError
            });

            reloadtextVariables();
        });

        function reloadtextVariables() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "GetTextVariablesPage" },
                success: function (result) {
                    $("#textVariablesContent").replaceWith(result);
                    $("#textVariablesTable").treeTable({});

                    $("#textVariablesTable").tablesorter({ headers: { 3: { sorter: false }, 4: { sorter: false }, 5: { sorter: false } } });
                },
                error: ajaxError
            });
        }

        function showAddTextVariables() {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "GetAddTextVariablesForm" },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function addTextVariables() {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;
            var description = document.getElementById("inputDescription").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "AddTextVariables", name: name, identifier: identifier, descrption: description },
                success: function (result) {
                    closeModal();
                    reloadtextVariables();
                },
                error: ajaxError
            });
        }

        function editTextVariablesForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "GetEditTextVariablesForm", id:id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function editTextVariable(id) {
            var name = document.getElementById("inputName").value;
            var identifier = document.getElementById("inputIdentifier").value;
            var description = document.getElementById("inputDescription").value;

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "EditTextVariables", id: id, name: name, identifier: identifier, descrption: description },
                success: function (result) {
                    closeModal();
                    reloadtextVariables();
                },
                error: ajaxError
            });
        }

        function deleteTextVariablesForm(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "GetDeleteTextVariablesForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function deleteTextVariable(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "DeleteTextVariables", id: id },
                success: function (result) {
                    closeModal();
                    reloadtextVariables();
                },
                error: ajaxError
            });
        }

        function getTextVariableTranslations(id) {
            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "GetTextVariablesTranslationForm", id: id },
                success: function (result) {
                    showModal(result);
                },
                error: ajaxError
            });
        }

        function updateTranslationToTextVariable(textVariableId) {          

            var allElementsInTable = document.getElementsByClassName(textVariableId);
            var allRows = [];

            for (i = 0; i < allElementsInTable.length; i++) {
                var editionId = allElementsInTable[i].id;
                var inputValue = allElementsInTable[i].children[1].children[0].value;

                for (j = 0; j < allElementsInTable[i].children[2].children[0].children.length; j++) {
                    if (allElementsInTable[i].children[2].children[0].children[j].selected) {
                        var selectEmpty = allElementsInTable[i].children[2].children[0].children[j].value;
                        allRows.push(editionId + "[stop]" + inputValue + "[stop]" + selectEmpty);
                        break;
                    }
                }
            }

            $.ajax({
                type: "POST",
                url: "../Handlers/PrintTextVariablesHandler.ashx",
                data: { method: "UpdateTranslationForTextVariable", id: textVariableId, data: allRows.join('[row]') },
                success: function (result) {
                    closeModal();
                },
                error: ajaxError
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="textVariablesContent">
    </div>
</asp:Content>
