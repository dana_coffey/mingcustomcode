﻿using System.Collections.Generic;

namespace TheCollective.Infor.Models
{
    public class Product : IEqualityComparer<Product>
    {
        public string Sku { get; set; }
        public string VendorSku { get; set; }
        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string ProductLine { get; set; }
        public string Description { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Length { get; set; }
        public string Status { get; set; }
        public string ProductCategory { get; set; }
        public string SastaCodeValue { get; set; }
        public string SastaCodeIdentifier { get; set; }
        public string ProductCategoryDescription { get; set; }
        public string TosCode { get; set; }
        public int RoundBy { get; set; }
        public string StockingUnit { get; set; }
        public bool IsCatalogProduct { get; set; }

        /// <summary>Determines whether the specified objects are equal.</summary>
        /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        public bool Equals(Product x, Product y)
        {
            return x?.GetHashCode() == y?.GetHashCode();
        }

        /// <summary>Returns a hash code for the specified object.</summary>
        /// <param name="obj">The <see cref="T:System.Object" /> for which a hash code is to be returned.</param>
        /// <returns>A hash code for the specified object.</returns>
        /// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj" /> is a reference type and <paramref name="obj" /> is null.</exception>
        public int GetHashCode(Product obj)
        {
            return string.Format(Sku, VendorSku, VendorNumber).GetHashCode();
        }
    }
}
