﻿namespace TheCollective.Infor.Models
{
    public class BarcodeInformation
    {
        public string ProductSku { get; set; }
        public string Barcode { get; set; }
    }
}
