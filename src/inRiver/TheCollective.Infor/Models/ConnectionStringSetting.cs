﻿namespace TheCollective.InRiver.InboundConnector.Infor.Models
{
    public class ConnectionStringSetting
    {
        public ConnectionStringSetting(string driver, string host, string port, string database, string username, string password)
        {
            Driver = driver;
            Host = host;
            Port = port;
            Database = database;
            Username = username;
            Password = password;
        }

        public string Driver { get; private set; }
        public string Host { get; private set; }
        public string Port { get; private set; }
        public string Database { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
    }

}