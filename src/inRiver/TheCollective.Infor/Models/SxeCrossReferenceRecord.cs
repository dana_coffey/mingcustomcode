﻿namespace TheCollective.Infor.Models
{
    public class SxeCrossReferenceRecord
    {
        public string ProductSku { get; set; }
        public string AlternateProductSku { get; set; }
    }
}
