SELECT icsec.prod as ProductSku,
       icsec.altprod as AlternateProductSku
FROM pub.icsec icsec
WHERE icsec.cono = {CompanyNumber}
      AND icsec.rectype in ('B', 'C', 'H', 'I', 'O', 'S', 'T', 'U', 'V')