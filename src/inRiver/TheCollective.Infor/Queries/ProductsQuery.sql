SELECT icsw.prod as Sku,
       icsw.vendprod as VendorSku, 
       icsw.arpvendno as VendorNumber,
       icsw.prodline as ProductLine,
       icsp.descrip as Description,
       icsp.weight as Weight,
       icsp.height as Height,
       icsp.width as Width,
       icsp.length as Length,
       icsp.statustype as Status,
	   icsp.prodcat as ProductCategory,
	   sasta.codeval as SastaCodeValue,
	   sasta.codeiden as SastaCodeIdentifier,
	   sasta.descrip as ProductCategoryDescription,
	   icsp.user2 as TosCode,
	   apsv.name as VendorName,
	   icsp.sellmult as RoundBy,
	   icsp.unitsell as StockingUnit,
       0 as IsCatalogProduct
FROM pub.icsp icsp
	LEFT JOIN pub.icsw icsw ON (icsp.prod = icsw.prod AND icsw.cono = icsp.cono)
	LEFT JOIN pub.sasta sasta ON icsp.prodcat = RIGHT(sasta.codeval, 4) AND sasta.cono = icsp.cono
	LEFT JOIN pub.apsv apsv ON icsw.arpvendno = apsv.vendno AND apsv.cono = icsp.cono
WHERE icsp.cono = {CompanyNumber}
      AND (sasta.exclecomm != 'y')
      AND icsw.whse = '{GhostWarehouse}'
      {WhereClause}