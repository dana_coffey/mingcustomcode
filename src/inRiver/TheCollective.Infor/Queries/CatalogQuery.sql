SELECT icsc.catalog as Sku,
       icsc.vendprod as VendorSku, 
       icsc.vendno as VendorNumber,
       icsc.descrip as Description,
       icsc.weight as Weight,
       icsc.height as Height,
       icsc.width as Width,
       icsc.length as Length,
       icsc.statustype as Status,
       icsc.prodcat as ProductCategory,
	   icsc.prodline as ProductLine,
	   sasta.codeval as SastaCodeValue,
	   sasta.codeiden as SastaCodeIdentifier,
       sasta.descrip as ProductCategoryDescription,
	   apsv.name as VendorName,
	   '' as TosCode,
	   0 as RoundBy,
	   icsc.unitstock as StockingUnit,
       1 as IsCatalogProduct
FROM pub.icsc icsc
LEFT JOIN pub.sasta sasta ON icsc.prodcat = RIGHT(sasta.codeval, 4)
LEFT JOIN pub.apsv apsv ON icsc.vendno = apsv.vendno AND apsv.cono = sasta.cono
WHERE sasta.cono = {CompanyNumber}
        AND sasta.exclecomm != 'y'
        {WhereClause}