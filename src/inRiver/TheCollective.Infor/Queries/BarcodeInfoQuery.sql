SELECT icsec.altprod as ProductSku,
	icsec.prod as Barcode
FROM pub.icsec icsec
WHERE icsec.cono = {CompanyNumber}
      AND icsec.rectype = 'B' 
      {WhereClause}