﻿using System;
using System.Collections.Generic;
using TheCollective.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models;

namespace TheCollective.Infor.Repositories
{
    public interface ICatalogRepository
    {
        IEnumerable<Product> GetProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, string ghostWarehouse, params string[] skus);
        IEnumerable<Product> GetUpdatedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, DateTime updatedSince, string ghostWarehouse);
	    IEnumerable<Product> GetCatalogs(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, params string[] skus);
		IEnumerable<Product> GetUpdatedCatalog(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, DateTime updatedSince);
        IEnumerable<SupersedProduct> GetSupersedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string productSku);
        IEnumerable<BarcodeInformation> GetBarcodes(ConnectionStringSetting connectionStringSetting, string companyNumber, string productSku);
        IEnumerable<RelatedProduct> GetRelatedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber);
    }
}
