﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheCollective.Infor.Helpers;
using TheCollective.Infor.Models;
using TheCollective.InRiver.InboundConnector.Infor.Models;

namespace TheCollective.Infor.Repositories
{
    public class CatalogRepository : ICatalogRepository
    {
        public IEnumerable<Product> GetProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, string ghostWarehouse, params string[] skus)
        {
            if (!skus.Any())
            {
                return new List<Product>();
            }

            var sqlSkus = QueryHelper.ToSqlInFormat(skus);

            var whereClause = $" AND icsp.prod IN ({sqlSkus})";

            FilterWithCodeIdentifier(supportedCodeIden, ref whereClause);

            var query = ResourceHelper.GetQueryFile("ProductsQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause },
                { "{GhostWarehouse}", ghostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetUpdatedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, DateTime updatedSince, string ghostWarehouse)
        {
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);

            var whereClause = string.Empty;

            FilterWithCodeIdentifier(supportedCodeIden, ref whereClause);

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsp.slchgdt >= '{formattedDate}' OR icsw.slchgdt >= '{formattedDate}' OR icsp.transdt >= '{formattedDate}' OR icsw.transdt >= '{formattedDate}')");
            }

            var query = ResourceHelper.GetQueryFile("ProductsQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause },
                { "{GhostWarehouse}", ghostWarehouse }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetCatalogs(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, params string[] skus)
        {
            if (!skus.Any())
            {
                return new List<Product>();
            }

            var sqlSkus = QueryHelper.ToSqlInFormat(skus);

            var whereClause = $" AND icsc.catalog IN ({sqlSkus})";

            FilterWithCodeIdentifier(supportedCodeIden, ref whereClause);

            var query = ResourceHelper.GetQueryFile("CatalogQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<Product> GetUpdatedCatalog(ConnectionStringSetting connectionStringSetting, string companyNumber, string[] supportedCodeIden, DateTime updatedSince)
        {
            var formattedDate = QueryHelper.ToSqlFormat(updatedSince);

            var whereClause = string.Empty;

            FilterWithCodeIdentifier(supportedCodeIden, ref whereClause);

            if (!string.IsNullOrEmpty(formattedDate))
            {
                whereClause = string.Concat(whereClause, $" AND (icsc.slchgdt >= '{formattedDate}' OR icsc.transdt >= '{formattedDate}')");
            }

            var query = ResourceHelper.GetQueryFile("CatalogQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<Product>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<SupersedProduct> GetSupersedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber, string productSku)
        {
            var whereClause = $" AND icsec.prod = '{productSku}'";

            var query = ResourceHelper.GetQueryFile("SupersededInfoQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<SupersedProduct>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<BarcodeInformation> GetBarcodes(ConnectionStringSetting connectionStringSetting, string companyNumber, string productSku)
        {
            var whereClause = $" AND icsec.altprod = '{productSku}'";

            var query = ResourceHelper.GetQueryFile("BarcodeInfoQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber },
                { "{WhereClause}", whereClause }
            });

            return QueryHelper.ExecuteOdbc<BarcodeInformation>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        public IEnumerable<RelatedProduct> GetRelatedProducts(ConnectionStringSetting connectionStringSetting, string companyNumber)
        {
            var query = ResourceHelper.GetQueryFile("RelatedProductQuery.sql");

            query = QueryHelper.ReplaceTags(query, new Dictionary<string, string>
            {
                { "{CompanyNumber}", companyNumber }
            });

            return QueryHelper.ExecuteOdbc<RelatedProduct>(query, ConnectionHelper.GetInforSxeConnectionString(connectionStringSetting));
        }

        private void FilterWithCodeIdentifier(string[] supportedCodeIden, ref string whereClause)
        {
            if (supportedCodeIden.Any())
            {
                var sqlCodeIden = QueryHelper.ToSqlInFormat(supportedCodeIden);
                whereClause = string.Concat(whereClause, $" AND sasta.codeiden IN ({sqlCodeIden})");
            }
            else
            {
                whereClause = string.Concat(whereClause, " AND sasta.codeiden = 'c'");
            }
        }
    }
}
