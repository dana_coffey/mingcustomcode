﻿using System.IO;
using System.Reflection;

namespace TheCollective.Infor.Helpers
{
    public static class ResourceHelper
    {
        public static string GetQueryFile(string resourceName)
        {
            var assembly = Assembly.GetCallingAssembly();
            var fullResourceName = string.Concat(assembly.GetName().Name, ".Queries." + resourceName);

            using (var stream = assembly.GetManifestResourceStream(fullResourceName))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
