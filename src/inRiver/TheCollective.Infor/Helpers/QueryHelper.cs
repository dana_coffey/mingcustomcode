﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;

namespace TheCollective.Infor.Helpers
{
    public static class QueryHelper
    {
        public static List<TModel> ExecuteOdbc<TModel>(string sql, string connectionString)
           where TModel : new()
        {
            using (var connection = new OdbcConnection(connectionString))
            {
                using (var odbcCommand = new OdbcCommand(sql, connection))
                {
                    return Execute<TModel, OdbcConnection, OdbcCommand>(connection, odbcCommand);
                }
            }
        }

        public static string ReplaceTags(string query, Dictionary<string, string> tags)
        {
            foreach (var tag in tags)
            {
                query = query.Replace(tag.Key, tag.Value);
            }

            return query;
        }

        public static string ToSqlFormat(DateTime? datetime)
        {
            if (!datetime.HasValue)
            {
                return string.Empty;
            }

            return datetime.Value.ToString("yyyy-MM-dd");
        }

        public static DateTime? Parse(string value)
        {
            DateTime dt;
            if (DateTime.TryParse(value, out dt))
            {
                return dt;
            }

            return null;
        }

        public static string ToSqlInFormat(params string[] values)
        {
            var sqlvalues = values.Select(v => string.Concat("'", v, "'")).ToArray();

            return sqlvalues.Length > 1 ? string.Join(",", sqlvalues) : sqlvalues.First();
        }

        private static List<TModel> Execute<TModel, TConnection, TCommand>(TConnection connection, TCommand command)
            where TModel : new()
            where TConnection : DbConnection
            where TCommand : DbCommand
        {
            try
            {
                var results = new List<TModel>();

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var item = ToModel<TModel>(reader);
                        if (item != null)
                        {
                            results.Add(item);
                        }
                    }
                }

                return results;
            }
            finally
            {
                command.Dispose();

                connection.Close();
                connection.Dispose();
            }
        }

        private static T ToModel<T>(DbDataReader reader) where T : new()
        {
            var model = new T();
            foreach (var property in typeof(T).GetProperties())
            {
                var value = reader.IsDBNull(reader.GetOrdinal(property.Name)) ? GetDefault(property.PropertyType) : Convert.ChangeType(reader[property.Name], GetNullableUnderlyingTypeIfNeeded(property.PropertyType));
                property.SetMethod.Invoke(model, new[] { value });
            }

            return model;
        }

        private static object GetDefault(Type type)
        {
            if (type == typeof(string))
            {
                return string.Empty;
            }

            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        private static Type GetNullableUnderlyingTypeIfNeeded(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return Nullable.GetUnderlyingType(type);
            }

            return type;
        }
    }
}
