﻿using System.Globalization;

namespace TheCollective.InRiver.Core
{
    public static class Constants
    {
        public static class Cultures
        {
            public static readonly CultureInfo English = new CultureInfo("en");
            public static readonly CultureInfo French = new CultureInfo("fr");
        }

        public static class Fields
        {
            public static class Product
            {
                public static class Validations
                {
                    public const int MaxMetaDescriptionFieldSize = 2048;
                }
            }
        }

        public static class FieldTypes
        {
            public static class ImportItem
            {
                public const string Done = "Done";
                public const string Ready = "Ready";
            }

            public static class Settings
            {
                public const string CarrierId = "CarrierId";
            }
        }
    }
}
