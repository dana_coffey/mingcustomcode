﻿using System;
using AMI = Absolunet.InRiver.Core.Models.Entities;

namespace TheCollective.InRiver.Core.Entities
{
    public class Item : AMI.Item
    {
        public string AutoEnrichmentStatus { get; set; }
        public string ItemERPCategory { get; set; }
        public string ItemERPCategoryDescription { get; set; }
        public string ItemERPDescription { get; set; }
        public string ItemERPPackageDescription { get; set; }
        public string ItemERPRoundBy { get; set; }
        public string ItemERPStatus { get; set; }
        public string ItemERPStockingUnit { get; set; }
        public string ItemERPVendorNo { get; set; }
        public DateTime ItemLastModifiedTime { get; set; }
        public string ItemTosCode { get; set; }
        public string ItemUpc { get; set; }
        public string ItemUrn { get; set; }
    }
}
