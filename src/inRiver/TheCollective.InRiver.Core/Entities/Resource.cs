﻿using System;
using inRiver.Remoting.Objects;
using AMI = Absolunet.InRiver.Core.Models.Entities;

namespace TheCollective.InRiver.Core.Entities
{
    public class Resource : AMI.Resource
    {
        public string ResourceDocumentType { get; set; }
        public string ResourceLocale { get; set; }
        public DateTime ResourcePublishDate { get; set; }
        public LocaleString ResourceTitle { get; set; }
    }
}
