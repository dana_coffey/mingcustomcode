﻿using inRiver.Remoting.Objects;
using AMI = Absolunet.InRiver.Core.Models.Entities;

namespace TheCollective.InRiver.Core.Entities
{
    public class Product : AMI.Product
    {
        public string ProductCarrierCategories { get; set; }
        public string ProductCarrierFeaturesTexts { get; set; }
        public string ProductCarrierName { get; set; }
        public string ProductCarrierOverviewTexts { get; set; }
        public string ProductERPCategoryCode { get; set; }
        public string ProductERPCategoryDescription { get; set; }
        public string ProductERPMajorCode { get; set; }
        public string ProductERPMajorDescription { get; set; }
        public string ProductERPMajorMinorCategoryDescription { get; set; }
        public string ProductERPMinorCode { get; set; }
        public string ProductFamilyProductsUrn { get; set; }
        public LocaleString ProductNameOverride { get; set; }
    }
}
