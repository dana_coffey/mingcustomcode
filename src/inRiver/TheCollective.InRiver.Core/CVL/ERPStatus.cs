﻿namespace TheCollective.InRiver.Core.CVL
{
    public static class ErpStatus
    {
        public const string Active = "A";
        public const string Inactive = "I";
        public const string Labor = "L";
        public const string Superseded = "S";
    }
}
