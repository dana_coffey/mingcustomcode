﻿using System.Globalization;
using inRiver.Remoting.Objects;

namespace TheCollective.InRiver.Core.Helpers
{
    public static class StringExtensions
    {
        public static string RemoveHtmlTags(this string source)
        {
            var array = new char[source.Length];
            var arrayIndex = 0;
            var inside = false;

            foreach (var c in source)
            {
                if (c == '<')
                {
                    inside = true;
                    continue;
                }

                if (c == '>')
                {
                    inside = false;
                    continue;
                }

                if (!inside)
                {
                    array[arrayIndex] = c;
                    arrayIndex++;
                }
            }

            return new string(array, 0, arrayIndex);
        }

        public static LocaleString ToLocaleString(this string value, CultureInfo cultureInfo = null)
        {
            return new LocaleString { [cultureInfo ?? Constants.Cultures.English] = value };
        }
    }
}
