﻿using System.Collections.Generic;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models;
using TheCollective.InRiver.Core.Models;

namespace TheCollective.InRiver.Core.Services
{
    public interface ICarrierSupersededService
    {
        CreateSupersededLinkResult CreateSupersededLinks(List<PartHistory> supersededParts, Entity itemEntity);
    }
}
