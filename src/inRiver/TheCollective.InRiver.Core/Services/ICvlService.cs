﻿namespace TheCollective.InRiver.Core.Services
{
    public interface ICvlService
    {
        void CreateCvlAndValueWhenMissing(string cvlId, string key, string value, string dataType);

        void CreateCvlValueWhenMissing(string cvlId, string key, string value);
    }
}
