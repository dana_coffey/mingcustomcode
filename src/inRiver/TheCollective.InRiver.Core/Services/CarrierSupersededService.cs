﻿using System.Collections.Generic;
using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;
using TheCollective.Common.Carrier.Models;
using TheCollective.InRiver.Core.CVL;
using TheCollective.InRiver.Core.Entities;
using TheCollective.InRiver.Core.LinkType;
using TheCollective.InRiver.Core.Models;

namespace TheCollective.InRiver.Core.Services
{
    public class CarrierSupersededService : ICarrierSupersededService
    {
        private readonly ICoreService _coreService;

        public CarrierSupersededService(ICoreService coreService)
        {
            _coreService = coreService;
        }

        public CreateSupersededLinkResult CreateSupersededLinks(List<PartHistory> supersededParts, Entity itemEntity)
        {
            var itemItemSupersededLinkType = _coreService.ModelRepository.GetLinkType(nameof(ItemItemSuperseded));
            var createSupersededLinkResult = new CreateSupersededLinkResult();

            foreach (var supersededPart in supersededParts)
            {
                var supersededItemEntity = _coreService.DataRepository.GetEntityByUniqueValue(nameof(Item.ItemSku), supersededPart.PartNumber, LoadLevel.DataOnly);

                if (supersededItemEntity != null && supersededItemEntity.Id != itemEntity.Id)
                {
                    _coreService.DataRepository.CreateLinkIfNecessary(supersededItemEntity, itemEntity, itemItemSupersededLinkType);

                    // Update Status Superseded = "S"
                    _coreService.SetFieldValue(supersededItemEntity, nameof(Item.ItemERPStatus), ErpStatus.Superseded);
                    _coreService.DataRepository.UpdateEntity(supersededItemEntity);

                    createSupersededLinkResult.Success.Add(string.Concat("'", supersededPart.PartNumber, "'"));
                }
                else
                {
                    createSupersededLinkResult.Errors.Add(string.Concat("'", supersededPart.PartNumber, "'"));
                }
            }

            return createSupersededLinkResult;
        }
    }
}
