﻿using Absolunet.InRiver.Core.Services;
using inRiver.Remoting.Objects;

namespace TheCollective.InRiver.Core.Services
{
    public class CvlService : ICvlService
    {
        private readonly ICoreService _coreService;

        public CvlService(ICoreService coreService)
        {
            _coreService = coreService;
        }

        public void CreateCvlAndValueWhenMissing(string cvlId, string key, string value, string dataType)
        {
            var cvlValue = _coreService.ModelRepository.GetCVLValueByKey(key, cvlId);

            if (cvlValue != null)
            {
                return;
            }

            var cvl = _coreService.ModelRepository.GetCVL(cvlId);
            if (cvl == null)
            {
                cvl = _coreService.ModelRepository.AddCvl(new inRiver.Remoting.Objects.CVL { Id = cvlId, DataType = dataType });
            }

            object cvlValueValue = value;

            if (cvl.DataType == DataType.LocaleString)
            {
                var languages = _coreService.Languages;
                var localeString = new LocaleString(languages);

                foreach (var language in languages)
                {
                    localeString[language] = value;
                }

                cvlValueValue = localeString;
            }

            _coreService.ModelRepository.AddCVLValue(new CVLValue { CVLId = cvlId, Key = key, Value = cvlValueValue });
        }

        public void CreateCvlValueWhenMissing(string cvlId, string key, string value)
        {
            var cvlValue = _coreService.ModelRepository.GetCVLValueByKey(key, cvlId);

            if (cvlValue != null)
            {
                return;
            }

            var cvl = _coreService.ModelRepository.GetCVL(cvlId);
            object cvlValueValue = value;

            if (cvl.DataType == DataType.LocaleString)
            {
                var languages = _coreService.Languages;
                var localeString = new LocaleString(languages);

                foreach (var language in languages)
                {
                    localeString[language] = value;
                }

                cvlValueValue = localeString;
            }

            _coreService.ModelRepository.AddCVLValue(new CVLValue { CVLId = cvlId, Key = key, Value = cvlValueValue });
        }
    }
}
