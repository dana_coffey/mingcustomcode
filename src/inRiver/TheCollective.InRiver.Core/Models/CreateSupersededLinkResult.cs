﻿using System.Collections.Generic;

namespace TheCollective.InRiver.Core.Models
{
    public class CreateSupersededLinkResult
    {
        public List<string> Errors { get; } = new List<string>();

        public List<string> Success { get; } = new List<string>();
    }
}
