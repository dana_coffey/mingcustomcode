Param(
    [string]$sourceDirectory,
    [string]$destinationDirectory,
    [string]$clientName
)

$scriptPath = Split-Path $MyInvocation.MyCommand.Path
Import-Module $scriptPath\bamboo.build.functions.psm1 -Force

$subDestinationDirectory = "$destinationDirectory\Collective"

CreateOrCleanDirectory $subDestinationDirectory

robocopy "$sourceDirectory\Collective.Core\bin\" $destinationDirectory "Collective.Core.dll"
robocopy "$sourceDirectory\Collective.InforSxe.Core\bin\" $destinationDirectory "Collective.InforSxe.Core.dll"
robocopy "$sourceDirectory\Collective.Integration.Core\bin\" $destinationDirectory "Collective.Integration.Core.dll"
robocopy "$sourceDirectory\Collective.Integration\bin\" $destinationDirectory "Absolunet.InRiver.Dequeue.Insite.Models.dll"

robocopy "$sourceDirectory\Collective.Integration\bin\" $subDestinationDirectory "Collective.Integration.dll" 
robocopy "$sourceDirectory\Collective.InforSxe.$clientName\bin\" $subDestinationDirectory "Collective.InforSxe.$clientName.dll"
robocopy "$sourceDirectory\Collective.Integration.$clientName\bin\" $subDestinationDirectory "Collective.Integration.$clientName.dll"

robocopy "$sourceDirectory\wis\" $destinationDirectory "Insite.WindowsIntegrationService.exe.config" "collectiveAppSettings.config" "SiteConnections.config" /s