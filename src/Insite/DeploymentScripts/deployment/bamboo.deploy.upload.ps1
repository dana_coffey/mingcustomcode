param(
	[string] $artifactDirectory,
	[string] $ftpUri,
	[string] $ftpUser,
	[string] $ftpPassword
)

$scriptPath = Split-Path $MyInvocation.MyCommand.Path
Import-Module $scriptPath\bamboo.deploy.functions.psm1 -Force

$readyFileName = "z_ready.txt"

if(!(Test-Path "$artifactDirectory/$readyFileName"))
{
    New-Item -Name $readyFileName -Path $artifactDirectory -ItemType File
}

UploadToFtp -artifact $artifactDirectory -ftp_uri $ftpUri -user $ftpUser -pass $ftpPassword