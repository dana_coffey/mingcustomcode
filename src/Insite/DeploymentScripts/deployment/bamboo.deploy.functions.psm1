$typeDefinition = @"
using System;
using System.Net;
public class FtpClient : WebClient
{
    protected override WebRequest GetWebRequest(Uri address)
    {
        FtpWebRequest ftpWebRequest = base.GetWebRequest(address) as FtpWebRequest;
        ftpWebRequest.EnableSsl = false;
        return ftpWebRequest;
    }
}
"@

Add-Type -TypeDefinition $typeDefinition

function UploadToFtp($artifacts, $ftp_uri, $user, $pass)
{
    $webclient = New-Object FtpClient
    $webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass)  

    "Uploading $artifacts"    

    $files = Get-ChildItem -recurse $artifacts

    foreach($item in $files){ 

        $relpath = [system.io.path]::GetFullPath($item.FullName).SubString([system.io.path]::GetFullPath($artifacts).Length + 1)

        "Uploading $item..."
        $uri = New-Object System.Uri($ftp_uri+$relpath) 
        "Uploading $uri..."

        Try
        {
            $webclient.UploadFile($uri, $item.FullName)
        }
        Catch
        {
            $ErrorMessage = $_.Exception.Message
            "Error $ErrorMessage"
            Break
        }
    }
}