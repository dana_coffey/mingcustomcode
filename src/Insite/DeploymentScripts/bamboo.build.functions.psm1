function RemoveDirectory([string] $path)
{
    if (Test-Path $path) {
        Remove-Item $path -Recurse 
    }
}

function CreateOrCleanDirectory([string] $path)
{
    if (!(Test-path $path)) {
        New-Item -ItemType directory -Path $path
    } else {
        Get-ChildItem $path | Remove-Item -Recurse
    }
}