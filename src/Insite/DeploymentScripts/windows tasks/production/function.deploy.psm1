Import-Module $PSScriptRoot\function.wis.psm1 -Force
Add-Type -assembly "system.io.compression.filesystem"

function ExtractPackage([string] $temporaryPath, [string] $sourcePackagePath)
{
    CreateDirectoryIfNotExist $temporaryPath $true
    
    write-host "Extracting pakage $sourcePackagePath"
    [io.compression.zipfile]::ExtractToDirectory($sourcePackagePath, $temporaryPath)
}

function CreateDirectoryIfNotExist([string] $sourceDirectory, [boolean] $cleanDirectory)
{
    if (!(Test-path $sourceDirectory)) {
        New-Item -ItemType directory -Path $sourceDirectory
    }
    else {
        if ($cleanDirectory){
            Get-ChildItem $sourceDirectory | Remove-Item -Recurse
        }
    }
}

function DeployPackage([string] $sourceDirectory, [string] $packageDirectory, [string] $name, [ScriptBlock]$deployScript)
{
	$readyFilePath = "$packageDirectory\z_ready.txt"
	
	if(Test-Path $readyFilePath)
	{
		Remove-Item $readyFilePath
	
		$packageFilename = Get-ChildItem $packageDirectory -Filter *.zip | Select-Object -First 1

		if ($packageFilename)
		{
			$archivePath = "$sourceDirectory\Archive\$name"
			$temporaryPath = "$sourceDirectory\temp\$name"
			$packagePath = "$packageDirectory\$packageFilename"

			ExtractPackage $temporaryPath $packagePath

			if ($deployScript)
			{
				$deployScript.Invoke($temporaryPath)
			}

			Get-ChildItem $temporaryPath | Remove-Item -Recurse

			CreateDirectoryIfNotExist $archivePath $false

			Move-Item $packagePath "$archivePath\$packageFilename" -force
			Get-ChildItem $archivePath | Where-Object { -not $_.PsIsContainer } | Sort-Object LastWriteTime -desc | Select-Object -Skip 10 | Remove-Item -Force
			
			Write-Host "Deploy done." -foregroundcolor "Green"
		}
		else 
		{
			write-host "No package to deploy"
		}
	}
}