Import-Module $PSScriptRoot\function.wis.psm1 -Force
Import-Module $PSScriptRoot\function.deploy.psm1 -Force

function DeployWis([string] $sourceDirectory)
{
    $destinationDirectory = "C:\Program Files\Insite Software\Commerce Integration Service V4.3"
    $wisServiceName = "Commerce Integration Service V4.3 (x64)"

    StopService $wisServiceName
    
    ROBOCOPY $sourceDirectory $destinationDirectory /s
    
	StartService $wisServiceName
}

RunAsAdministrator $myInvocation.MyCommand.Definition $PSScriptRoot

$packageDirectory = "\\MSAZ-DEV-INST\InRiver_WIS\deploy.packages.ming.staging"
$name = "ming.staging"

DeployPackage $PSScriptRoot $packageDirectory $name $function:DeployWis