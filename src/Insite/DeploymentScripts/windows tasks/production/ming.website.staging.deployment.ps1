Import-Module $PSScriptRoot\function.wis.psm1 -Force
Import-Module $PSScriptRoot\function.deploy.psm1 -Force

function DeployWebsite([string] $sourceDirectory)
{
    $destinationDirectory = "C:\WebRoot\insitedev.mingledorffs.com"

	ROBOCOPY "$sourceDirectory\bin\" "$destinationDirectory\bin\" /MIR
	ROBOCOPY "$sourceDirectory\config\" "$destinationDirectory\config\" "appSettings.config" "collectiveAppSettings.config" "connectionStrings.config" /s
	ROBOCOPY "$sourceDirectory\nwayo-build\" "$destinationDirectory\nwayo-build\" /MIR
	ROBOCOPY "$sourceDirectory\themes\" "$destinationDirectory\themes\" /MIR
	ROBOCOPY "$sourceDirectory\SystemResources\" "$destinationDirectory\SystemResources\" /MIR
}

RunAsAdministrator $myInvocation.MyCommand.Definition $PSScriptRoot

$packageDirectory = "D:\FtpRoot\deploy.packages.ming.staging"
$name = "ming.staging"

DeployPackage $PSScriptRoot $packageDirectory $name $function:DeployWebsite