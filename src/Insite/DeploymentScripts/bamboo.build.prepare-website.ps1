Param(
    [string]$sourceDirectory,
    [string]$destinationDirectory,
    [string]$themeName
)

robocopy "$sourceDirectory\bin\" "$destinationDirectory\bin\" /mir
robocopy "$sourceDirectory\app_config\" "$destinationDirectory\app_config\" /mir
robocopy "$sourceDirectory\config\" "$destinationDirectory\config\" "appSettings.config" "collectiveAppSettings.config" "connectionStrings.config" /s

robocopy "$sourceDirectory\nwayo-build\store\" "$destinationDirectory\nwayo-build\store\" /mir
robocopy "$sourceDirectory\themes\store\views\" "$destinationDirectory\themes\store\views\" /mir
robocopy "$sourceDirectory\themes\store\scripts\" "$destinationDirectory\themes\store\scripts\" *.js *.ts /mir
robocopy "$sourceDirectory\SystemResources\" "$destinationDirectory\SystemResources\" *.js /mir

robocopy "$sourceDirectory\nwayo-build\$themeName\" "$destinationDirectory\nwayo-build\$themeName\" /mir
robocopy "$sourceDirectory\themes\$themeName\views\" "$destinationDirectory\themes\$themeName\views\" /mir
robocopy "$sourceDirectory\themes\$themeName\scripts\" "$destinationDirectory\themes\$themeName\scripts\" *.js *.ts /mir