Param(
    [string]$sourceDirectory,
    [string]$destinationDirectory,
    [string]$buildConfiguration
)

$scriptPath = Split-Path $MyInvocation.MyCommand.Path
Import-Module $scriptPath\bamboo.build.functions.psm1 -Force

$destination = "$destinationDirectory\Deploy_" + $buildConfiguration + "_" + (Get-Date -format "yyyy-MM-dd HH.mm") + ".zip"

CreateOrCleanDirectory $destinationDirectory

Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($sourceDirectory, $destination)