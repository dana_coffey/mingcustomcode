Param(
    [string]$workingDirectory
)

$scriptPath = Split-Path $MyInvocation.MyCommand.Path
Import-Module $scriptPath\bamboo.build.functions.psm1 -Force

RemoveDirectory "$workingDirectory\builds"
RemoveDirectory "$workingDirectory\packages"