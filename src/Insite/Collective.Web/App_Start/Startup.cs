﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="Insite Software">
//   Copyright © 2017. Insite Software. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Owin;

namespace Collective.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            new CustomSiteStartup().Run(app);
        }
    }
}
