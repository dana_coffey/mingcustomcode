﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Collective.Web.App_Start;
using Collective.Web.Attributes;
using Collective.Web.Extension.Core.DotLiquid.Tags;
using Collective.Web.Extension.Core.PostStartupScripts;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using DotLiquid;
using FluentScheduler;
using IdentityServer3.Core.Models;
using IdentityServer3.EntityFramework;
using IdentityServer3.EntityFramework.Entities;
using Insite.Core.Interfaces.Data;
using Insite.IdentityServer.Options;
using Insite.SystemResources;
using Insite.WebFramework.Mvc;
using Insite.WebFramework.Routing;

namespace Collective.Web
{
    public class CustomSiteStartup : SiteStartup
    {
        public override string[] GetAdditionalAdminAuthenticationPaths()
        {
            // if there are additional paths that should use the admin bearer token authentication, return them here
            // return new[] { "/customAdminRoute" }; - ensures that requests to any urls starting with /customAdminRoute/ will use the admin bearer token authentication
            // note that the requests will not work with cookies, the bearer token will have to be sent in the query string or as a request header
            return new string[0];
        }

        protected override void PostStartup(IUnitOfWork unitOfWork)
        {
            BundleConfig.RegisterBundles();

            // load any custom dot liquid tags here
            // Template.RegisterTag<MyCustomTag>("myCustomTag");
            JobManager.Initialize(new TaskRegistry());
            PostStartupScriptsRunner.RunAllScripts<BaseStandardPostStartupScript>();

            Template.RegisterTag<ThemeNameTag>("themeName");
            Template.RegisterTag<WebsiteHostNameTag>("websiteHostName");
            Template.RegisterTag<WebsiteNameTag>("websiteName");

            ConfigureMingleIdentityAccess();
#if DEBUG
            // This setting is overriden here only for v4.3.x of Insite here, in v4.4.5+ it's moved into an admin setting
            SecurityOptions.CookieOptions.ExpireTimeSpan = TimeSpan.FromHours(24);
#endif
        }

        protected override void PreStartup()
        {
            // this happens before any other startup code, but will happen after Application_Start
            // The IOC container has not been initialized, the bootstrapper has not been run, not IStartupTasks have been run.
            GlobalConfiguration.Configuration.Filters.Add(new InterceptionAttribute());
        }

        protected override void RegisterCustomRoutes(RouteCollection routes, IRouteProvider routeProvider)
        {
            // Add additional routes with this syntax
            // routeProvider.MapRoute(routes, null, "Test", new { Controller = "Test", Action = "Index" }, true);
            routes.IgnoreRoute("SystemResources/Scripts/Admin/Common/ckeditor.directive.js");
            routes.IgnoreRoute("SystemResources/Scripts/ContentAdminShell/insite.contentadmin.js");

            routeProvider.MapRoute(routes, "Jobs", "Jobs/{action}", new { Controller = "Jobs" });
            routeProvider.MapRoute(routes, "ScanBarcode", "ScanBarcode/{barcode}", new { Controller = "ScanBarcode", Action = "ScanBarcode" });

            routeProvider.MapRoute(routes, "CustomContactUs", "CustomContactUs/{action}", new { Controller = "CustomContactUs" });
        }

        private void ConfigureMingleIdentityAccess()
        {
            const string clientName = "Mingle";

            var efConfig = new EntityFrameworkServiceOptions
            {
                ConnectionString = "InSite.Commerce"
            };

            using (var db = new ClientConfigurationDbContext(efConfig.ConnectionString, efConfig.Schema))
            {
                if (!db.Clients.Any(c => c.ClientId.Equals(clientName.ToLower())))
                {
                    db.Clients.Add(new IdentityServer3.EntityFramework.Entities.Client
                    {
                        ClientName = clientName,
                        ClientId = clientName.ToLower(),
                        Enabled = true,
                        RefreshTokenUsage = TokenUsage.OneTimeOnly,
                        EnableLocalLogin = true,
                        PrefixClientClaims = true,
                        IdentityTokenLifetime = 360,
                        AccessTokenLifetime = 3600,
                        AuthorizationCodeLifetime = 300,
                        AbsoluteRefreshTokenLifetime = 86400,
                        SlidingRefreshTokenLifetime = 43200,
                        AccessTokenType = AccessTokenType.Jwt,
                        ClientSecrets = new List<ClientSecret>
                        {
                            new ClientSecret
                            {
                                Value = "m1ngl3d0rff".Sha256(),
                                Type = "SharedSecret"
                            }
                        },
                        Flow = Flows.ResourceOwner,
                        AllowedScopes = new List<ClientScope>
                        {
                            new ClientScope
                            {
                                Scope = SecurityOptions.Scope
                            }
                        }
                    });

                    db.SaveChanges();
                }
            }
        }
    }
}
