﻿using Collective.Web.Extension.Core.Jobs;
using FluentScheduler;

namespace Collective.Web.App_Start
{
    public class TaskRegistry : Registry
    {
        public TaskRegistry()
        {
            Schedule<SchedulerJob>().NonReentrant().ToRunNow().AndEvery(10).Seconds();
        }
    }
}
