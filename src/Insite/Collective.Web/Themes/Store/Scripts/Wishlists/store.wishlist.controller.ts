﻿module insite.wishlist {
    "use strict";

    export class StoreWishListController extends WishListController {
        static $inject = ["$scope", "coreService", "wishListService", "productService", "cartService", "paginationService", "settingsService", "queryString", "spinnerService"];

        constructor(
            protected $scope: ng.IScope,
            protected coreService: core.ICoreService,
            protected wishListService: IWishListService,
            protected productService: catalog.IProductService,
            protected cartService: cart.ICartService,
            protected paginationService: core.IPaginationService,
            protected settingsService: core.ISettingsService,
            protected queryString: common.IQueryStringService,
            protected spinnerService: core.ISpinnerService) {
            super($scope, coreService, wishListService, productService, cartService, paginationService, settingsService, queryString);
        }

        getSelectedWishListDetails(): void {
            this.selectedWishList.pagination = this.paginationService.getDefaultPagination(this.paginationStorageKey, this.selectedWishList.pagination);

            this.spinnerService.show("wishlist");
            this.wishListService.getWishList(this.selectedWishList)
                .then(
                    (wishList: WishListModel) => { this.getWishListCompleted(wishList); },
                    (error: any) => { this.getWishListFailed(error); }
                ).finally(() => {
                    this.spinnerService.hide("wishlist");
                });
        }

        deleteWishList(): void {
            this.spinnerService.show("wishlist");
            this.wishListService.deleteWishList(this.selectedWishList).then(
                (wishList: WishListModel) => { this.deleteWishListCompleted(wishList); },
                (error: any) => { this.deleteWishListFailed(error); });
        }

        deleteLine(line: WishListLineModel): void {
            if (this.inProgress) {
                return;
            }

            this.spinnerService.show("wishlist");
            this.inProgress = true;
            this.wishListService.deleteLine(line).then(
                (wishListLine: WishListLineModel) => { this.deleteLineCompleted(wishListLine); },
                (error: any) => { this.deleteLineFailed(error); });
        }

        addLineToCart(line: any): void {
            this.spinnerService.show("wishlist");
            this.cartService.addLine(line, true, true)
                .then(
                    (cartLine: CartLineModel) => { this.addLineCompleted(cartLine); },
                    (error: any) => { this.addLineFailed(error); }
                ).finally(() => {
                    this.spinnerService.hide("wishlist");
                });
        }

        addAllToCart(): void {
            this.spinnerService.show("wishlist");
            this.inProgress = true;
            this.cartService.addLineCollection(this.selectedWishList.wishListLineCollection, true)
                .then(
                    (cartLineCollection: CartLineCollectionModel) => { this.addLineCollectionCompleted(cartLineCollection); },
                    (error: any) => { this.addLineCollectionFailed(error); }
                ).finally(() => {
                    this.spinnerService.hide("wishlist");
                });
        }
    }

    angular
        .module("insite")
        .controller("WishListController", StoreWishListController);
}