﻿module insite.wishlist {
    "use strict";

    export class StoreAddToWishlistPopupController extends AddToWishlistPopupController {
        static $inject = ["wishListService", "coreService", "settingsService", "addToWishlistPopupService", "accessToken", "sessionService", "spinnerService"];

        constructor(
            protected wishListService: IWishListService,
            protected coreService: core.ICoreService,
            protected settingsService: core.ISettingsService,
            protected addToWishlistPopupService: AddToWishlistPopupService,
            protected accessToken: common.IAccessTokenService,
            protected sessionService: account.ISessionService,
            protected spinnerService: core.ISpinnerService) {
            super(wishListService, coreService, settingsService, addToWishlistPopupService, accessToken, sessionService);
        }

        addWishList(wishListName: string): void {
            this.spinnerService.show("popup-add-wishlist");
            this.wishListService.addWishList(wishListName)
                .then(
                    (newWishList: WishListModel) => { this.addWishListCompleted(newWishList); },
                    (error: any) => {
                        this.addWishListFailed(error);
                        this.spinnerService.hide("popup-add-wishlist");
                    }
                );
        }

        protected addLineToWishList(wishList: WishListModel): void {
            this.spinnerService.show("popup-add-wishlist");
            this.wishListService.addWishListLine(wishList, this.productsToAdd[0])
                .then(
                    (wishListLine: WishListLineModel) => { this.addWishListLineCompleted(wishListLine); },
                    (error: any) => { this.addWishListLineFailed(error); }
                ).finally(() => {
                    this.spinnerService.hide("popup-add-wishlist");
                });
        }

        protected addLineCollectionToWishList(wishList: WishListModel): void {
            this.spinnerService.show("popup-add-wishlist");
            this.wishListService.addWishListLines(wishList, this.productsToAdd)
                .then(
                    (wishListLineCollection: WishListLineCollectionModel) => { this.addWishListLineCollectionCompleted(wishListLineCollection); },
                    (error: any) => { this.addWishListLineCollectionFailed(error); }
                ).finally(() => {
                    this.spinnerService.hide("popup-add-wishlist");
                });
        }
    }

    angular
        .module("insite")
        .controller("AddToWishlistPopupController", StoreAddToWishlistPopupController);
}