﻿import WarehouseCollectionModel = Store.Warehouse.WebApi.V1.ApiModels.WarehouseCollectionModel;
import WarehouseModel = Store.Warehouse.WebApi.V1.ApiModels.WarehouseModel;

module store.warehouse {
    "use strict";

    export interface IStoreWarehouseService {
        getAvailableWarehouses(customerSpecificWarehouse: string);
        getPickUpWarehouses();
        getShipFromWarehouses();
        getWarehouse(warehouseId: string);
    }

    export class StoreWarehouseService implements IStoreWarehouseService {
        serviceUri = "/api/v1/warehouses";

        static $inject = ["$http", "$rootScope", "$q", "httpWrapperService"];

        constructor(
            protected $http: ng.IHttpService,
            protected $rootScope: ng.IRootScopeService,
            protected $q: ng.IQService,
            protected httpWrapperService: insite.core.HttpWrapperService)
        {
        }

        public getAvailableWarehouses(customerSpecificWarehouse: string): ng.IPromise<WarehouseCollectionModel> {
            if (!customerSpecificWarehouse) {
                customerSpecificWarehouse = 'null';
            }
            const uri = `${this.serviceUri}/available/${customerSpecificWarehouse}`;
            
            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "GET", url: uri }),
                () => {},
                () => {}
            );
        }

        public getPickUpWarehouses(): ng.IPromise<WarehouseCollectionModel> {
            const uri = `${this.serviceUri}/pickup`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "GET", url: uri }),
                (response: ng.IHttpPromiseCallbackArg<WarehouseCollectionModel>) => {
                },
                (error: ng.IHttpPromiseCallbackArg<any>) => {
                }
            );
        }

        public getShipFromWarehouses(): ng.IPromise<WarehouseCollectionModel> {
            const uri = `${this.serviceUri}/shipfrom`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "GET", url: uri }),
                (response: ng.IHttpPromiseCallbackArg<WarehouseCollectionModel>) => {
                },
                (error: ng.IHttpPromiseCallbackArg<any>) => {
                }
            );
        }

        public getWarehouse(warehouseName: string): ng.IPromise<WarehouseModel> {
            const uri = `${this.serviceUri}/${warehouseName}`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "GET", url: uri }),
                (response: ng.IHttpPromiseCallbackArg<WarehouseModel>) => {
                },
                (error: ng.IHttpPromiseCallbackArg<any>) => {
                }
            );
        }
    }

    angular
        .module("insite")
        .service("warehouseService", StoreWarehouseService);
}
