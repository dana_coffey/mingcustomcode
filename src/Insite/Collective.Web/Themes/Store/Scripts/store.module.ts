﻿declare let Foundation: any;
declare let PubSub: any;

module insite {
    "use strict";

    export class StoreAppRunService extends AppRunService {
        static $inject = ["coreService", "$localStorage", "$window", "$rootScope", "$urlRouter", "spinnerService", "$location"];

        constructor(
            protected coreService: core.ICoreService,
            protected $localStorage: common.IWindowStorage,
            protected $window: ng.IWindowService,
            protected $rootScope: IAppRootScope,
            protected $urlRouter: angular.ui.IUrlRouterService,
            protected spinnerService: core.ISpinnerService,
            protected $location: ng.ILocationService) {

            super(coreService, $localStorage, $window, $rootScope, $urlRouter, spinnerService, $location);
        }

        protected onViewContentLoaded(): void {
            PubSub.publish("nwayo.foundation-reflow");
            if (!this.$rootScope.firstPage) {
                this.sendGoogleAnalytics();
            }
            this.sendVirtualPageView();
        }
    }

    angular
        .module("insite")
        .service("appRunService", StoreAppRunService);
}