﻿module insite.order {
    "use strict";

    export class StoreOrderDocumentService {
        serviceUri = "/api/v1/orders";
        isAuthenticated: string;

        static $inject = ["accessToken"];

        constructor(
            protected accessToken: common.IAccessTokenService) {

            this.isAuthenticated = this.accessToken.exists().toString();
        }

        getInvoiceUrl(orderId): string {
            return this.getDocumentUrl(orderId, "inv");
        }

        getProofOfDeliveryUrl(orderId): string {
            return this.getDocumentUrl(orderId, "pod");
        }

        getDocumentUrl(orderId, type) {
            let url = `${this.serviceUri}/document/download/${type}/${orderId}`;

            if (this.isAuthenticated) {
                url += `?access_token=${this.accessToken.get()}`;
            }

            return url;
        }
    }

    angular
        .module("insite")
        .service("storeOrderDocumentService", StoreOrderDocumentService);
}