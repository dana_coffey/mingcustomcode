﻿module insite.order {
    "use strict";

    interface IInvoiceDownloadModel {
        invoiceUrl: string;
        proofOfDeliveryUrl: string;
        statusCode: string;
        stageCode: string;
        canDownloadInvoice: Boolean;
        canDownloadProofOfDelivery: Boolean;
    }

    export class StoreOrderDetailController extends OrderDetailController {
        serviceUri = "/api/v1/orders";
        isPickup: boolean;
        warehouse: Store.Warehouse.WebApi.V1.ApiModels.WarehouseModel;
        location: OrderLocationModel;
        subAccount: Store.Catalog.WebApi.V1.ApiModels.OrderSubAccountModel;
        shipComplete: Boolean;
        ready: Boolean;
        hasInvoice: Boolean;
        hasProofOfDelivery: Boolean;
        invoiceInfo: IInvoiceDownloadModel;

        static $inject = ["orderService", "settingsService", "queryString", "coreService", "sessionService", "cartService", "spinnerService", "$window", "ipCookie", "httpWrapperService", "$http", "storeOrderDocumentService"];

        constructor(
            protected orderService: order.IOrderService,
            protected settingsService: core.ISettingsService,
            protected queryString: common.IQueryStringService,
            protected coreService: core.ICoreService,
            protected sessionService: account.ISessionService,
            protected cartService: cart.ICartService,
            protected spinnerService: core.ISpinnerService,
            protected $window: ng.IWindowService,
            protected ipCookie: any,
            protected httpWrapperService: core.HttpWrapperService,
            protected $http: ng.IHttpService,
            protected documentService: StoreOrderDocumentService) {
            super(orderService, settingsService, queryString, coreService, sessionService, cartService);
        }

        storeReorder($event): void {
            $event.preventDefault();
            this.canReorderItems = false;
            const cartLines = this.orderService.convertToCartLines(this.order.orderLines);
            if (cartLines.length > 0) {
                this.spinnerService.show();
                this.cartService
                    .addLineCollection(cartLines, true, false)
                    .finally(() => { this.spinnerService.hide(); })
                    .then((cartLineCollection: CartLineCollectionModel) => {
                        this.$window.location.href = $event.target.getAttribute("href");
                    });
            }
        }

        getOrder(orderNumber: string): void {
           
            if (this.ready) {
                this.spinnerService.show();
            } else {
                setTimeout(() => {
                    this.spinnerService.show();
                }, 0);
            }
        
            this.orderService.getOrder(orderNumber, "orderlines,shipments,documents")
                .then(
                    (order: OrderModel) => { this.getOrderCompleted(order); },
                    (error: any) => { this.getOrderFailed(error); }
                )
                .finally(() => {
                    this.ready = true;
                    this.spinnerService.hide();
                });
        }

        downloadInvoice(): void {
            if (this.hasInvoice) {
                this.$window.open(this.documentService.getInvoiceUrl(this.order.id));
            }
        }

        downloadPod(): void {
            if (this.hasProofOfDelivery) {
                this.$window.open(this.documentService.getProofOfDeliveryUrl(this.order.id));
            }
        }

        protected getOrderCompleted(order: OrderModel): void {
            super.getOrderCompleted(order);

            this.isPickup = order.properties["shipViaIsPickup"] !== undefined;
            if (order.properties["warehouse"]) {
                this.warehouse = JSON.parse(order.properties["warehouse"]);
            }
            if (order.properties["location"]) {
                this.location = JSON.parse(order.properties["location"]);
            }
            if (order.properties["subAccount"]) {
                this.subAccount = JSON.parse(order.properties["subAccount"]);
            }
            if (order.properties["shipComplete"]) {
                this.shipComplete = JSON.parse(order.properties["shipComplete"]);
            }
            if (order.properties["hasInvoice"]) {
                this.hasInvoice = order.properties["hasInvoice"] !== undefined;
            }
            if (order.properties["hasProofOfDelivery"]) {
                this.hasProofOfDelivery = order.properties["hasProofOfDelivery"] !== undefined;
            }
        }
    }

    angular
        .module("insite")
        .controller("OrderDetailController", StoreOrderDetailController);
}