﻿module store.checkout {
    "use strict";

    angular.module("insite")
        .directive("storeOrderDetailLines", () => ({
            restrict: "E",
            replace: true,
            scope: {
                order: "="
            },
            templateUrl: "/PartialViews/OrderDetail-Lines"
        }));
}