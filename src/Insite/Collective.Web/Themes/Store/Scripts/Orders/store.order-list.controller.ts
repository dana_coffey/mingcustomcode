﻿module insite.order {
    "use strict";

    export class StoreOrderSearchFilter extends OrderSearchFilter {
        reference: string;
        serialNumber: string;
    }

    export class StoreOrderListController extends OrderListController {
        static $inject = ["orderService", "customerService", "coreService", "paginationService", "settingsService", "spinnerService"];

        referenceSearchFilter: string;
        productSerialNumberFilter: string;
        storeAppliedSearchFilter = new StoreOrderSearchFilter();

        constructor(
            protected orderService: order.IOrderService,
            protected customerService: customers.ICustomerService,
            protected coreService: core.ICoreService,
            protected paginationService: core.IPaginationService,
            protected settingsService: core.ISettingsService,
            protected spinnerService: core.ISpinnerService) {
            super(orderService, customerService, coreService, paginationService, settingsService);
        }

        getOrders(): void {
            this.spinnerService.show();
            this.storePrepareSearchFilter();
            this.appliedSearchFilter.sort = this.searchFilter.sort;
            this.coreService.replaceState({ filter: this.appliedSearchFilter, pagination: this.pagination });

            delete this.storeAppliedSearchFilter.statusDisplay;

            this.orderService
                .getOrders(this.storeAppliedSearchFilter, this.pagination)
                .then(
                    (orderCollection: OrderCollectionModel) => { this.getOrdersCompleted(orderCollection); },
                    (error: any) => { this.getOrdersFailed(error); }
                )
                .finally(() => { this.spinnerService.hide(); });
        }

        protected getOrdersCompleted(orderCollection: OrderCollectionModel): void {
            super.getOrdersCompleted(orderCollection);

            if (!this.searchFilter.ordertotaloperator) {
                this.searchFilter.ordertotaloperator = "";
            }
        }

        storePrepareSearchFilter(): void {
            for (let property in this.searchFilter) {
                if (this.searchFilter.hasOwnProperty(property)) {
                    if (this.searchFilter[property] === "") {
                        this.storeAppliedSearchFilter[property] = null;
                    } else {
                        this.storeAppliedSearchFilter[property] = this.searchFilter[property];
                    }
                }
            }

            this.storeAppliedSearchFilter.reference = this.referenceSearchFilter;
            this.storeAppliedSearchFilter.serialNumber = this.productSerialNumberFilter;

            if (this.storeAppliedSearchFilter.statusDisplay && this.orderStatusMappings && this.orderStatusMappings[this.storeAppliedSearchFilter.statusDisplay]) {
                this.storeAppliedSearchFilter.status = this.orderStatusMappings[this.storeAppliedSearchFilter.statusDisplay];
            }
        }

        clear(): void {
            super.clear();

            this.productSerialNumberFilter = "";
            this.referenceSearchFilter = "";

            this.getOrders();
        }
    }

    angular
        .module("insite")
        .controller("OrderListController", StoreOrderListController);
}