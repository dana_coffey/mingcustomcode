﻿module insite.dealers {
    "use strict";

    declare let Foundation: any;
    declare function RichMarker(options: google.maps.MarkerOptions): void;

    export class StoreDealerCollectionController {
        protected infoWindow: google.maps.InfoWindow;
        protected markers = [];

        addressSearchField: string;
        center: google.maps.LatLng;
        dealers: DealerModel[];
        distanceUnitOfMeasure: number;
        locationKnown = true;
        pagination: PaginationModel;
        storeName: string;
        $infoWindowDropdowns: any;
        location: LocationModel;
        setDealerError: boolean;

        static $inject = ["$scope", "$q", "dealerService", "$compile", "$element", "locationService", "warehouseService", "$rootScope"];

        constructor(
            protected $scope: IDealerCollectionScope,
            protected $q: ng.IQService,
            protected dealerService: IDealerService,
            protected $compile: ng.ICompileService,
            protected $element: ng.IRootElementService,
            protected locationService: locations.ILocationService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected $rootScope: ng.IRootScopeService) {
            this.init();
        }

        init(): void {
            this.setDealerError = false;
            this.locationService.get().then(
                (currentLocation: LocationModel) => {
                    this.location = currentLocation;
                });

            this.$scope.$on("mapInitialized",
                () => {
                    this.onMapInitialized();
                });
        }

        protected onMapInitialized(): void {
            this.searchDealers();
        }

        getDealers(): void {
            if (this.pagination) {
                this.pagination.page = 1;
            }

            if (this.addressSearchField && this.addressSearchField.trim()) {
                // resolve an address
                this.dealerService.getGeoCodeFromAddress(this.addressSearchField).then(
                    (geocoderResults: google.maps.GeocoderResult[]) => { this.getGeoCodeFromAddressCompleted(geocoderResults); },
                    (error: any) => { this.getGeoCodeFromAddressFailed(error); });
            } else {
                // get from the browser
                this.searchDealers();
            }
        }

        selectDealer(dealer: DealerModel) {
            this.setDealerError = false;
            this.locationService.get().then(
                (currentLocation: LocationModel) => {
                    this.location = currentLocation;
                    this.warehouseService.getWarehouse(dealer.properties["associatedWarehouse"]).then(
                        (warehouse: WarehouseModel) => {
                            this.location.warehouse = warehouse;
                            this.locationService.update(this.location);
                            this.$rootScope.$broadcast("updateWarehouse", this.location.warehouse);
                        },
                        () => { this.selectDealerError(); });
                },
                () => { this.selectDealerError(); });
        }

        selectDealerError() {
            this.setDealerError = true;
        }

        protected getGeoCodeFromAddressCompleted(geocoderResults: google.maps.GeocoderResult[]): void {
            this.locationKnown = true;

            const geocoderResult = geocoderResults[0];
            if (typeof geocoderResult.formatted_address !== "undefined") {
                this.addressSearchField = geocoderResult.formatted_address;
            }

            const coords = new google.maps.LatLng(geocoderResult.geometry.location.lat(), geocoderResult.geometry.location.lng());
            this.getDealerCollection(coords);
        }

        protected getGeoCodeFromAddressFailed(error: any): void {
            this.locationKnown = false;
        }

        protected getDealerCollection(coords: google.maps.LatLng): void {
            const filter = this.getFilter(coords);
            this.dealerService.getDealers(filter).then(
                (dealerCollection: DealerCollectionModel) => { this.getDealerCollectionCompleted(dealerCollection); },
                (error: any) => { this.getDealerCollectionFailed(error); });
        }

        searchDealers(): void {
            if (!this.center) {
                const defaultCoords = new google.maps.LatLng(0, 0);
                if (navigator.geolocation) {
                    const timeout = setTimeout(() => { this.getDealerCollection(defaultCoords); }, 500);
                    const allowRefresh = this.center == undefined;
                    navigator.geolocation.getCurrentPosition(
                        (position: Position) => {
                            clearTimeout(timeout);
                            this.getDealerCollection(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                        },
                        (error: any) => {
                            clearTimeout(timeout);
                            this.getDealerCollection(defaultCoords);
                        });
                } else {
                    this.getDealerCollection(defaultCoords);
                }
            } else {
                this.getDealerCollection(this.center);
            }
            this.locationKnown = true;
        }

        protected getDealerCollectionCompleted(dealerCollection: DealerCollectionModel): void {
            this.dealers = dealerCollection.dealers;
            this.pagination = dealerCollection.pagination;
            this.addressSearchField = dealerCollection.formattedAddress;
            this.distanceUnitOfMeasure = dealerCollection.distanceUnitOfMeasure === "Metric" ? 1 : 0;

            if (!this.center || this.center.lat() === 0 && this.center.lng() === 0) {
                this.center = new google.maps.LatLng(dealerCollection.defaultLatitude, dealerCollection.defaultLongitude);
            }

            this.setMap();
        }

        protected getDealerCollectionFailed(error: any): void {
        }

        protected getFilter(coords: google.maps.LatLng): IDealerFilter {
            this.center = coords;

            const filter: IDealerFilter = {
                name: this.storeName,
                latitude: coords.lat(),
                longitude: coords.lng()
            };

            if (this.pagination) {
                filter.pageSize = this.pagination.pageSize;
                filter.page = this.pagination.page;
            }

            return filter;
        }

        protected getDealerMarkerPopupHtml(dealer: DealerModel): string {
            const markerPopupScope = this.$scope.$new();
            (markerPopupScope as any).dealer = dealer;
            (markerPopupScope as any).dealer.distanceUnitOfMeasure = this.distanceUnitOfMeasure.toString();
            const markerPopupRawHtml = angular.element("#dealerMarkerPopup").html();
            const markerPopup = this.$compile(markerPopupRawHtml)(markerPopupScope);
            markerPopupScope.$digest();

            return markerPopup[0].outerHTML;
        }

        protected setHomeMarker(): void {
            const marker = this.createMarker(this.center.lat(), this.center.lng(), "<span class='marker home'><span></span></span>", false);

            google.maps.event.addListener(marker, "click", () => {
                this.onHomeMarkerClick(marker);
            });
        }

        protected onHomeMarkerClick(marker: any): void {
            this.openMarker(marker, `<div class="info-window"><div class="user-info small caps"><div class="user-info-title">Your searched location</div><span>${this.addressSearchField}</span></div></div>`);
        }

        protected setMap(): void {
            this.$scope.map.panTo(this.center);
            this.removeAllMarkers();
            this.setDealersMarkers();
            this.setHomeMarker();
            this.fitBounds();
        }

        protected toMarker(index: number): void {
            const marker = this.markers[index];
            this.$scope.map.panTo(marker.getPosition());
            this.$scope.map.setZoom(15);
            PubSub.publish("nwayo.scroll-to", { element: "#map" });
        }

        protected removeAllMarkers(): void {
            for (let m = 0; m < this.markers.length; m++) {
                this.markers[m].setMap(null);
            }
            this.markers = [];
        }

        protected setDealersMarkers(): void {
            this.dealers.forEach((dealer, i) => {
                const marker = this.createMarker(dealer.latitude, dealer.longitude, `<span class='marker'><span>${this.getDealerNumber(i)}</span></span>`);

                google.maps.event.addListener(marker, "click", () => {
                    if (event) { event.stopPropagation(); }
                    this.onDealerMarkerClick(marker, dealer);
                });
            });
        }

        protected createMarker(lat: number, lng: number, content: string, isDealerMarker = true): any {
            const markerOptions = {
                position: new google.maps.LatLng(lat, lng),
                map: this.$scope.map,
                flat: true,
                draggable: false,
                content: content,
                isDealerMarker: isDealerMarker
            };
            const marker = new RichMarker(markerOptions);
            this.markers.push(marker);

            return marker;
        }

        protected onDealerMarkerClick(marker: any, dealer: DealerModel): void {
            this.openMarker(marker, this.getDealerMarkerPopupHtml(dealer));
        }

        protected openMarker(marker: any, content: string): void {
            if (this.infoWindow) {
                this.infoWindow.close();
                this.clearInfoWindowDropdowns();
            } else {
                this.infoWindow = new google.maps.InfoWindow();
                google.maps.event.addListener(this.infoWindow, 'domready', () => {
                    this.$infoWindowDropdowns = this.$element.find("#info-window [data-dropdown]").appendTo(this.$element);
                    PubSub.publish("nwayo.foundation-reflow", { element: this.$infoWindowDropdowns });
                });
                google.maps.event.addListener(this.infoWindow, 'closeclick', this.clearInfoWindowDropdowns);
            }

            this.infoWindow.setContent(content);
            this.infoWindow.open(this.$scope.map, marker);
        }

        protected clearInfoWindowDropdowns(): void {
            if (this.$infoWindowDropdowns) {
                this.$infoWindowDropdowns.remove();
            }
        }

        getDealerNumber(index: number): number {
            return index + 1 + (this.pagination.pageSize * (this.pagination.page - 1));
        }

        protected fitBounds(): void {
            if (this.$scope.map != null) {
                const bounds = new google.maps.LatLngBounds();
                for (let i = 0, markersLength = this.markers.length; i < markersLength; i++) {
                    bounds.extend(this.markers[i].position);
                }

                // Extends the bounds when we have only one marker to prevent zooming in too far.
                if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                    const extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.03, bounds.getNorthEast().lng() + 0.03);
                    const extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.03, bounds.getNorthEast().lng() - 0.03);
                    bounds.extend(extendPoint1);
                    bounds.extend(extendPoint2);
                }

                if (bounds.getCenter().lat() === 0 && bounds.getCenter().lng() === -180) {
                    return;
                }

                this.$scope.map.setCenter(bounds.getCenter());
                this.$scope.map.fitBounds(bounds);
            }
        }

        onOpenHoursClick($event): void {
            $event.preventDefault();
        }
    }

    angular
        .module("insite")
        .controller("DealerCollectionController", StoreDealerCollectionController);
}