﻿module store.contactus {
    "use strict";

    export class StoreContactUsController extends insite.contactus.ContactUsController{

        static $inject = ["$element", "$scope", "spinnerService"];

        constructor(
            protected $element: ng.IRootElementService,
            protected $scope: ng.IScope,
            protected spinnerService: insite.core.ISpinnerService) {
            super($element, $scope);
        }

        submit($event): boolean {
            $event.preventDefault();
            if (!this.$form.valid()) {
                return false;
            }

            this.spinnerService.show();
            (this.$form as any).ajaxPost(() => {
                this.spinnerService.hide();
                this.submitted = true;
                this.$scope.$apply();
            });

            return false;
        }
    }

    angular
        .module("insite")
        .controller("ContactUsController", StoreContactUsController);
}