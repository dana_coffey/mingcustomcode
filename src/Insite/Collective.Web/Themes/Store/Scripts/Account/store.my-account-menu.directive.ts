﻿module store.account {
    "use strict";

    angular
        .module("insite")
        .directive("storeMyAccountMenu", () => ({
            restrict: "E",
            replace: true,
            scope: {
                currentPage: "="
            },
            templateUrl: "/PartialViews/Account-Menu",
            controller: "StoreMyAccountMenuController",
            controllerAs: "vm",
            bindToController: true
        }));
}