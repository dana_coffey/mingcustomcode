﻿module insite.account {
    import AddressFieldDisplayCollectionModel = Insite.Websites.WebApi.V1.ApiModels.AddressFieldDisplayCollectionModel;
    import Guid = System.Guid;
    "use strict";

    export class StoreSelectCustomerController {
        dashboardUrl: string;
        errorMessage = "";
        returnUrl: string;
        homePageUrl: string;
        checkoutAddressUrl: string;
        reviewAndPayUrl: string;
        cartUrl: string;
        addressesUrl: string;
        cart: CartModel;
        useDefaultCustomer: boolean;
        defaultCustomerIsShipTo: boolean;
        defaultCustomerIsBillTo: boolean;
        showIsDefaultCheckbox: boolean;
        noShipToAndCantCreate = false;
        pagination: PaginationModel;
        hasDefaultCustomer: boolean;

        billTo: BillToModel;
        billToTable: any;
        billToTableApi: any;
        hasBillTos: boolean;
        

        shipTo: ShipToModel;
        shipToTable: any;
        shipToTableApi: any;
        hasShipTos: boolean;

        location: LocationModel;
        locationEdit: LocationModel;
        originalLocation: LocationModel;
        initializeLocation: boolean = true;
        countries: CountryModel[];

        locationAddressFields: AddressFieldDisplayCollectionModel;

        shipToIsBillingAddress: boolean;

        warehouses: WarehouseModel[];
        warehouseSorted: boolean;

        spinnerQueue = [];

        static $inject = [
            "$scope",
            "$window",
            "$element",
            "$timeout",
            "accountService",
            "sessionService",
            "customerService",
            "$attrs",
            "settingsService",
            "cartService",
            "queryString",
            "coreService",
            "spinnerService",
            "locationService",
            "websiteService",
            "warehouseService",
            "geolocationService",
            "dealerService"
        ];

        constructor(
            protected $scope: ng.IScope,
            protected $window: ng.IWindowService,
            protected $element: ng.IRootElementService,
            protected $timeout: ng.ITimeoutService,
            protected accountService: IAccountService,
            protected sessionService: ISessionService,
            protected customerService: customers.ICustomerService,
            protected $attrs: ISelectCustomerControllerAttributes,
            protected settingsService: core.ISettingsService,
            protected cartService: cart.ICartService,
            protected queryString: common.IQueryStringService,
            protected coreService: core.ICoreService,
            protected spinnerService: core.ISpinnerService,
            protected locationService: locations.ILocationService,
            protected websiteService: websites.IWebsiteService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected geolocationService: store.core.StoreGeolocationService,
            protected dealerService: dealers.IDealerService) {
            this.init();
        }

        init(): void {
            this.homePageUrl = this.$attrs.homePageUrl;
            this.dashboardUrl = this.$attrs.dashboardUrl;
            this.addressesUrl = this.$attrs.addressesUrl;
            this.checkoutAddressUrl = this.$attrs.checkoutAddressUrl;
            this.reviewAndPayUrl = this.$attrs.reviewAndPayUrl;
            this.cartUrl = this.$attrs.cartUrl;

            this.returnUrl = this.queryString.get("returnUrl");
            if (!this.returnUrl) {
                this.returnUrl = this.homePageUrl;
            }

            this.cart = this.cartService.getLoadedCurrentCart();
            if (!this.cart) {
                this.$scope.$on("cartLoaded",
                    (event: ng.IAngularEvent, cart: CartModel) => {
                        this.onCartLoaded(cart);
                    });
            }

            this.settingsService.getSettings().then(
                (settingsCollection: core.SettingsCollection) => { this.getSettingsCompleted(settingsCollection); },
                (error: any) => { this.getSettingsFailed(error); });
        }

        protected onCartLoaded(cart: CartModel): void {
            this.cart = cart;
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            this.showSpinner('session');
            const requireSelectCustomerOnSignIn = settingsCollection.accountSettings.requireSelectCustomerOnSignIn;
            this.sessionService.getSession().then(
                (session: SessionModel) => { this.getSessionCompleted(requireSelectCustomerOnSignIn, session); },
                (error: any) => { this.getSessionFailed(error); })
                .finally(() => { this.hideSpinner('session'); });
            this.initLocation();
            this.initDataTables(settingsCollection.customerSettings);
        }

        protected getSettingsFailed(error: any): void {
        }

        protected getSessionCompleted(requireSelectCustomerOnSignIn: boolean, session: SessionModel) {
            this.showIsDefaultCheckbox = !requireSelectCustomerOnSignIn && !session.hasDefaultCustomer;
            this.defaultCustomerIsBillTo = session.properties["defaultCustomerIsBillTo"] === "True";
            this.defaultCustomerIsShipTo = session.properties["defaultCustomerIsShipTo"] === "True";
            this.hasDefaultCustomer = (this.defaultCustomerIsBillTo || this.defaultCustomerIsShipTo);
        }

        protected getSessionFailed(error: any): void {
        }

        initDataTables(customerSettings: any): void {
            const self = this;
            const _sharedOptions = {
                info: false,
                ordering: false,
                paging: true,
                pageLength: 5,
                lengthChange: false,
                serverSide: true,
                searchDelay: 250,
                orderClasses: false,
                stripeClasses: [],
                autoWidth: false,
                select: {
                    style: 'api',
                    items: 'row',
                    className: 'on'
                }
            };

            const _cursorColumn = {
                data: null,
                searchable: false,
                className: 'select-cursor',
                defaultContent: ''
            }

            const _checkboxColumn = {
                data: null,
                searchable: false,
                defaultContent: '<span class="radio"></span>'
            }

            let $billToTable = (self.$element.find('[data-table="bill-to"]') as any);
            if ($billToTable.length) {
                self.billToTable = $billToTable.dataTable($.extend({},
                    _sharedOptions,
                    {
                        columns: [
                            _cursorColumn,
                            _checkboxColumn,
                            { data: 'customerNumber' },
                            { data: 'customerName' },
                            { data: 'address1' },
                            { data: 'address2' },
                            { data: 'city' }
                        ],
                        rowCallback: (row, data) => {
                            $(row).addClass('selectable');
                            if (self.billTo && self.billTo.id === data.id) {
                                self.billToTableApi.row(row).select();
                            }
                        },
                        ajax: (data, callback) => {
                            if (!self.hasBillTos) {
                                this.showSpinner('bill-to');
                            }
                            self.customerService
                                .getBillTos("state,validation",
                                    data.search.value,
                                    $.extend({},
                                        self.pagination,
                                        {
                                            page: (data.start / data.length) + 1,
                                            pageSize: data.length
                                        }))
                                .then(result => {
                                    let billTos = result.billTos;
                                    $(self.billToTableApi.table().container()).find('.dataTables_paginate').toggleClass('empty', result.pagination.numberOfPages <= 1);

                                    self.$timeout(() => {
                                        self.noShipToAndCantCreate = false;
                                        if (data.start === 0 && billTos.length > 0 && !data.search.value) {
                                            self.selectBillTo(billTos[0]);
                                        } else if (self.billTo) {
                                            self.selectBillTo(null);
                                        }

                                        callback({
                                            draw: data.draw,
                                            recordsTotal: result.pagination.totalItemCount,
                                            recordsFiltered: result.pagination.totalItemCount,
                                            data: billTos
                                        });

                                        self.hasBillTos = true;
                                        this.hideSpinner('bill-to');
                                    });
                                });
                        }
                    }));

                self.billToTableApi = self.billToTable.api();
                self.billToTable
                    .on('click',
                        'tbody tr',
                        function() {
                            self.selectRow(this, self.billToTableApi, self.selectBillTo);
                        });
            }

            let $shipToTable = (self.$element.find('[data-table="ship-to"]') as any);
            if ($shipToTable.length) {
                self.shipToTable = $shipToTable.dataTable($.extend({},
                    _sharedOptions,
                    {
                        columns: [
                            _cursorColumn,
                            _checkboxColumn,
                            { data: 'customerSequence' },
                            { data: 'customerName' },
                            { data: 'address1' },
                            { data: 'address2'},
                            { data: 'city' }
                        ],
                        rowCallback: (row, data) => {
                            $(row).addClass('selectable');
                            if (self.shipTo && self.shipTo.id === data.id) {
                                self.shipToTableApi.row(row).select();
                            }
                            if (self.billTo.id === data.id || data.isNew) {
                                let $row = $(row);
                                $row.children(':gt(2)').remove();
                                $row.append('<td colspan="3">' + data.label + '</td>');
                            }
                        },
                        ajax: (data, callback) => {
                            if (self.billTo) {
                                if (!self.hasShipTos) {
                                    this.showSpinner('ship-to');
                                }
                                self.customerService
                                    .getShipTos("excludeshowall,validation",
                                        data.search.value,
                                        $.extend({},
                                            self.pagination,
                                            {
                                                page: (data.start / data.length) + 1,
                                                pageSize: data.length
                                            }),
                                        self.billTo.id)
                                    .then(result => {
                                        let shipTos = result.shipTos.filter(x => {
                                            return x.properties["customShippingAddressCartId"] == undefined;
                                        });
                                        $(self.shipToTableApi.table().container()).find('.dataTables_paginate').toggleClass('empty', result.pagination.numberOfPages <= 1);

                                        self.$timeout(() => {
                                            self.noShipToAndCantCreate = false;
                                            
                                            if (shipTos.length > 0 && !data.search.value && this.initializeLocation && (!this.hasDefaultCustomer || this.defaultCustomerIsShipTo)) {
                                                self.applyCurrentLocationToShipTo(shipTos[0], this.originalLocation);
                                                self.shipToIsBillingAddress = self.billTo.id === shipTos[0].id;
                                                this.initializeLocation = false;
                                            } else {
                                                self.selectShipTo(null);
                                                self.shipToIsBillingAddress = false;
                                            }

                                            if (!customerSettings.allowCreateNewShipToAddress && !data.search.value && shipTos.length === 0) {
                                                self.noShipToAndCantCreate = true;
                                            }

                                            callback({
                                                draw: data.draw,
                                                recordsTotal: result.pagination.totalItemCount,
                                                recordsFiltered: result.pagination.totalItemCount,
                                                data: shipTos
                                            });

                                            self.hasShipTos = result.properties['hasShipTos'] != '0';
                                            this.hideSpinner('ship-to');
                                        });
                                    });
                            } else {
                                callback({
                                    draw: data.draw,
                                    recordsTotal: 0,
                                    recordsFiltered: 0,
                                    data: {}
                                });
                            }
                        }
                    }));

                self.shipToTableApi = self.shipToTable.api();
                self.shipToTable
                    .on('click',
                        'tbody tr',
                        function() {
                            self.selectRow(this, self.shipToTableApi, self.selectShipTo);
                        });
            }
        }

        initLocation(): void {
            this.showSpinner('location');
            this.locationService.get().then(
                (location: LocationModel) => { this.getLocationCompleted(location); },
                (error: any) => { this.getLocationFailed(error); });

            this.showSpinner('address-fields');
            this.websiteService.getAddressFields().then(
                (addressFieldCollection: AddressFieldCollectionModel) => { this.locationAddressFields = this.filterLocationAddressFields(addressFieldCollection.shipToAddressFields); },
                (error: any) => { })
                .finally(() => { this.hideSpinner('address-fields'); });
        }

        protected getLocationCompleted(location: LocationModel): void {
            this.originalLocation = location;
            this.setLocation(location, false);
            this.updateLocationEdit();

            this.showSpinner('countries');
            this.websiteService.getCountries("states").then(
                (countryCollection: CountryCollectionModel) => { this.getCountriesCompleted(countryCollection); },
                (error: any) => { this.getCountriesFailed(error); })
                .finally(() => { this.hideSpinner('countries'); });

            this.hideSpinner('location');
        }

        protected getLocationFailed(error: any): void {
            this.hideSpinner('location');
        }

        protected getCountriesCompleted(countryCollection: CountryCollectionModel): void {
            this.countries = countryCollection.countries;

            this.setObjectToReference(this.countries, this.location, "country");
            if (this.location.country) {
                this.setObjectToReference(this.location.country.states, this.location, "state");
            }

            this.updateLocationEdit();
        }

        protected getCountriesFailed(error: any): void {
        }

        initWarehouses(): void {
            if (this.billTo != null) {
                this.showSpinner('warehouses');
                this.warehouseService.getAvailableWarehouses(this.billTo.properties['customerSpecificWarehouse']).then((warehouseCollection: WarehouseCollectionModel) => {
                    this.getAvailableWarehousesCompleted(warehouseCollection);
                },
                (error: any) => {
                    this.getAvailableWarehousesFailed(error);
                });
            }
        }

        protected getAvailableWarehousesCompleted(warehouseCollection: Store.Warehouse.WebApi.V1.ApiModels.WarehouseCollectionModel): void {
            this.warehouses = warehouseCollection.warehouses;
        }

        protected getAvailableWarehousesFailed(error: any): void {
        }

        selectRow(row: any, tableApi: any, selectFn: any): void {
            this.$timeout(() => {
                let _row = tableApi.row(row);
                let _selectedRows = tableApi.rows({ selected: true });
                if (tableApi.row(row, { selected: true }).any()) {
                    selectFn.call(this, null);
                } else {
                    _row.select();
                    selectFn.call(this, _row.data());
                }
                _selectedRows.deselect();
            });
        }

        selectBillTo(billTo: BillToModel): void {
            if (!billTo) {
                this.hasShipTos = false;
                this.shipToIsBillingAddress = false;
            }
            this.billTo = billTo;
            this.shipTo = null;
            this.shipToTableApi.search('');
            this.shipToTableApi.ajax.reload();
            this.initWarehouses();
        }

        selectShipTo(shipTo: ShipToModel): void {
            this.shipTo = shipTo;
            
            if (this.shipTo) {
                this.setLocation(this.mapShippingAddressToLocation(this.shipTo), false);
                this.updateLocationEdit();
            }
        }

        private applyCurrentLocationToShipTo(shipTo: ShipToModel, location: LocationModel): void {
            this.shipTo = shipTo;

            this.setObjectToReference(this.countries, shipTo, "country");

            var locationToUse = {
                address1: location.address1,
                address2: location.address2,
                address3: location.address3,
                address4: location.address4,
                postalCode: location.postalCode,
                state: location.state,
                city: location.city,
                country: location.country,
                validation: shipTo.validation,
                warehouse: location.warehouse
            } as LocationModel;

            if (this.shipTo) {
                this.setLocation(locationToUse, true);
                this.updateLocationEdit();
            }
        }

        cancel() {
            this.$window.location.href = this.returnUrl;
        }

        setLocation(location: LocationModel, updateGeocoding: boolean) {
            this.location = location;
            this.warehouseSorted = false;

            if (updateGeocoding) {
                let address = location.address1 + ", " + location.city + ", " + location.state.abbreviation + " " + location.postalCode + ", " + location.country.abbreviation;

                this.dealerService.getGeoCodeFromAddress(address).then(
                    (geocoderResults: google.maps.GeocoderResult[]) => { this.getGeoCodeFromLocationCompleted(geocoderResults); },
                    (error: any) => { this.getGeoCodeFromLocationFailed(error); });
            }
        }

        protected getGeoCodeFromLocationCompleted(geocoderResults: google.maps.GeocoderResult[]): void {
            const geocoderResult = geocoderResults[0];
            let locationCoordinates = new google.maps.LatLng(geocoderResult.geometry.location.lat(), geocoderResult.geometry.location.lng());
            this.location.latitude = locationCoordinates.lat();
            this.location.longitude = locationCoordinates.lng();
        }

        protected getGeoCodeFromLocationFailed(error: any): void {
            this.location.latitude = null;
            this.location.longitude = null;
        }

        setCustomer(): void {
            if (!this.billTo || !this.shipTo) {
                return;
            }

            this.showSpinner('submit');
            this.sessionService.setCustomer(this.billTo.id, this.shipTo.id, this.useDefaultCustomer).then(
                (session: SessionModel) => { this.setCustomerCompleted(session); },
                (error: any) => { this.setCustomerFailed(error); });
        }

        protected setCustomerCompleted(session: SessionModel): void {
            session.shipTo = this.shipTo;
            this.locationService.update(this.location);
            
            this.cart.properties["warehouse"] = "";
            this.cartService.updateCart(this.cart);

            this.sessionService.redirectAfterSelectCustomer(
                session,
                this.cart.canBypassCheckoutAddress,
                this.dashboardUrl,
                this.returnUrl,
                this.checkoutAddressUrl,
                this.reviewAndPayUrl,
                this.addressesUrl,
                this.cartUrl,
                this.cart.canCheckOut);
        }

        protected setCustomerFailed(error: any): void {
            this.errorMessage = error.message;
            this.hideSpinner('submit');
        }

        setAsDefaultCustomer(): void {
            if (this.useDefaultCustomer) {
                this.coreService.displayModal(angular.element("#defaultCustomerChangedMessage"));
            }
        }

        setObjectToReference(references, object, objectPropertyName): void {
            if (references !== undefined) {
                references.forEach(reference => {
                    if (object[objectPropertyName] && (reference.abbreviation === object[objectPropertyName].abbreviation)) {
                        object[objectPropertyName] = reference;
                    }
                });
            }
        }

        mapShippingAddressToLocation(shipTo: ShipToModel): LocationModel {
            this.setObjectToReference(this.countries, shipTo, "country");
            
            return {
                address1: shipTo.address1,
                address2: shipTo.address2,
                address3: shipTo.address3,
                address4: shipTo.address4,
                postalCode: shipTo.postalCode,
                state: shipTo.state,
                city: shipTo.city,
                country: shipTo.country,
                validation: shipTo.validation,
                warehouse: JSON.parse(shipTo.properties["defaultWarehouse"])
            } as LocationModel;
        }

        openLocationEdit(): void {
            this.coreService.displayModal("#location-edit", () => { this.onCloseLocationEdit(); });
        }

        onCloseLocationEdit(): void {
            $("#location-edit-addressForm").validate().resetForm();
            this.updateLocationEdit();
        }

        saveLocationEdit(): void {
            const valid = $("#location-edit-addressForm").validate().form();
            if (!valid) { return; }

            this.setLocation(angular.copy(this.locationEdit), true);
            this.coreService.closeModal("#location-edit");
        }

        updateLocationEdit(): void {
            this.locationEdit = angular.copy(this.location);
        }

        filterLocationAddressFields(addressFields: AddressFieldDisplayCollectionModel): AddressFieldDisplayCollectionModel {
            const excludedFields = ["attention", "companyName", "contactFullName", "contactFullName", "email", "fax", "firstName", "lastName", "phone"];
            excludedFields.forEach((x) => { addressFields[x] = null; });

            return addressFields;
        }

        getCurrentLocationSuccess(position: Position) {
            this.warehouses.sort((itemA, itemB) => {
                let latitudeA = parseFloat(itemA.properties["latitude"]);
                let longitudeA = parseFloat(itemA.properties["longitude"]);
                let latitudeB = parseFloat(itemB.properties["latitude"]);
                let longitudeB = parseFloat(itemB.properties["longitude"]);

                const distanceA = this.geolocationService.getDistanceFromLocationInKm(position.coords.latitude, position.coords.longitude, latitudeA, longitudeA);
                const distanceB = this.geolocationService.getDistanceFromLocationInKm(position.coords.latitude, position.coords.longitude, latitudeB, longitudeB);

                return distanceA > distanceB ? 1 : -1;
            });
        }

        openWarehouseSelect() {
            if (!this.warehouseSorted && this.location.latitude && this.location.longitude) {

                this.warehouses.sort((itemA, itemB) => {
                    let latitudeA = parseFloat(itemA.properties["latitude"]);
                    if (isNaN(latitudeA)) {
                        latitudeA = 0;
                    }

                    let longitudeA = parseFloat(itemA.properties["longitude"]);
                    if (isNaN(longitudeA)) {
                        longitudeA = 0;
                    }

                    let latitudeB = parseFloat(itemB.properties["latitude"]);
                    if (isNaN(latitudeB)) {
                        latitudeB = 0;
                    }

                    let longitudeB = parseFloat(itemB.properties["longitude"]);
                    if (isNaN(longitudeB)) {
                        longitudeB = 0;
                    }

                    const distanceA = this.geolocationService.getDistanceFromLocationInKm(this.location.latitude, this.location.longitude, latitudeA, longitudeA);
                    const distanceB = this.geolocationService.getDistanceFromLocationInKm(this.location.latitude, this.location.longitude, latitudeB, longitudeB);

                    return distanceA > distanceB ? 1 : -1;
                });

                this.warehouseSorted = true;
            }
            this.coreService.displayModal("#warehouse-select", () => { this.onCloseWarehouseSelect(); });
        }

        selectWarehouse(warehouse: WarehouseModel) {
            this.location.warehouse = warehouse;
            this.updateLocationEdit();
            $('.modal-close').click();
        }

        onCloseWarehouseSelect(): void {
        }

        protected showSpinner(name: string): void {
            this.spinnerQueue.push(name);
            this.spinnerService.show();
        }

        protected hideSpinner(name: string): void {
            this.spinnerQueue = this.spinnerQueue.filter((x) => { return x !== name });
            if (!this.spinnerQueue.length) {
                this.spinnerService.hide();
            }
        }
    }

    angular
        .module("insite")
        .controller("SelectCustomerController", StoreSelectCustomerController);
}