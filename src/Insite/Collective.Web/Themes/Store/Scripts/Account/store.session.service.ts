﻿module insite.account {
    "use strict";

    export class StoreSessionService extends SessionService {

        static $inject = ["$http", "$rootScope", "$q", "$localStorage", "$window", "ipCookie", "accessToken", "$location", "httpWrapperService", "coreService", "settingsService"];

        constructor(
            protected $http: ng.IHttpService,
            protected $rootScope: ng.IRootScopeService,
            protected $q: ng.IQService,
            protected $localStorage: common.IWindowStorage,
            protected $window: ng.IWindowService,
            protected ipCookie: any,
            protected accessToken: common.IAccessTokenService,
            protected $location: ng.ILocationService,
            protected httpWrapperService: core.HttpWrapperService,
            protected coreService: core.ICoreService,
            protected settingsService: core.ISettingsService) {
            super($http, $rootScope, $q, $localStorage, $window, ipCookie, accessToken, $location, httpWrapperService, coreService, settingsService);
        }

        signIn(accessToken: string, username: string, password: string, rememberMe: boolean = false): ng.IPromise<SessionModel> {
            this.accessToken.set(accessToken);

            this.$http.post('https://InfluxInsiteConnector.mingledorffs.com/api/Connect/SaveAccessToken', JSON.stringify({ 'userID': username, 'accessToken': accessToken }));

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.post(this.serviceUri, this.signInParams(username, password, rememberMe), { bypassErrorInterceptor: true }),
                this.signInCompleted,
                this.signInFailed
            );
        }

        signOut(): ng.IPromise<string> {
            this.$http.post('https://InfluxInsiteConnector.mingledorffs.com/api/Connect/RemoveAccessToken', JSON.stringify({ 'accessToken': this.accessToken.get() }));

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.delete(`${this.serviceUri}/current`),
                this.signOutCompleted,
                this.signOutFailed
            );
        }
    }

    angular
        .module("insite")
        .service("sessionService", StoreSessionService);
}