﻿module insite.account {
    "use strict";

    export class StoreSignInController extends SignInController {
        static $inject = ["$scope",
            "$window",
            "accountService",
            "sessionService",
            "customerService",
            "coreService",
            "spinnerService",
            "$attrs",
            "settingsService",
            "cartService",
            "queryString",
            "accessToken",
            "$timeout",
            "$localStorage",
            "locationService"
        ];

        constructor(
            protected $scope: ng.IScope,
            protected $window: ng.IWindowService,
            protected accountService: IAccountService,
            protected sessionService: ISessionService,
            protected customerService: customers.ICustomerService,
            protected coreService: core.ICoreService,
            protected spinnerService: core.ISpinnerService,
            protected $attrs: ISignInControllerAttributes,
            protected settingsService: core.ISettingsService,
            protected cartService: cart.ICartService,
            protected queryString: common.IQueryStringService,
            protected accessToken: common.IAccessTokenService,
            protected $timeout: ng.ITimeoutService,
            protected $http: ng.IHttpService,
            protected locationService: locations.ILocationService) {
            super($scope, $window, accountService, sessionService, customerService, coreService, spinnerService, $attrs, settingsService, cartService, queryString, accessToken, $timeout, $http);
        }

        protected signInCompleted(session: SessionModel): void {
            this.sessionService.setContextFromSession(session);
            if (session.isRestrictedProductRemovedFromCart) {
                this.coreService.displayModal(angular.element("#removedProductsFromCart"),
                    () => {
                        this.selectCustomer(session);
                    });
                this.$timeout(() => {
                    this.coreService.closeModal("#removedProductsFromCart");
                }, 5000);
            } else {
                this.selectCustomer(session);
            }
            this.locationService.reset();
        }
    }

    angular
        .module("insite")
        .controller("SignInController", StoreSignInController);
}