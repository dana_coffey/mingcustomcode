﻿module insite.account {
    "use strict";

    export class StoreResetPasswordController extends ResetPasswordController {
        showTermsAndConditions: boolean;

        static $inject = ["$scope", "sessionService", "coreService", "settingsService", "queryString"];

        constructor(
            protected $scope: IResetPasswordControllerScope,
            protected sessionService: account.ISessionService,
            protected coreService: core.ICoreService,
            protected settingsService: core.ISettingsService,
            protected queryString: common.IQueryStringService) {
            super($scope, sessionService, coreService, settingsService, queryString);
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            this.showTermsAndConditions = (settingsCollection as any).collectiveSettingsSettings.showTermsAndConditions;

            this.settings = settingsCollection.accountSettings;
            this.hasAnyRule = this.settings.passwordMinimumRequiredLength > 0 ||
                this.settings.passwordRequiresSpecialCharacter ||
                this.settings.passwordRequiresUppercase ||
                this.settings.passwordRequiresLowercase ||
                this.settings.passwordRequiresDigit;
        }
    }

    angular
        .module("insite")
        .controller("ResetPasswordController", StoreResetPasswordController);
}