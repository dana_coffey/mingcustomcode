﻿//import BaseAddressModel = Insite.Customers.WebApi.V1.ApiModels.BaseAddressModel;
//import CountryModel = Insite.Websites.WebApi.V1.ApiModels.CountryModel;
//import FieldValidationDto = Insite.Customers.Services.Dtos.FieldValidationDto;

module store.account {
    "use strict";

    export interface IStoreMyAccountMenuAttributes  extends ng.IAttributes {
        currentPage: string;
    }

    export class StoreMyAccountMenuController {
        collectiveSettings: Store.Settings.Models.ICollectiveSettingsModel;
        currentPage: string;
        isAdministrator: boolean;
        isAuthenticated: boolean;

        static $inject = ["$window", "$attrs", "sessionService", "$scope", "settingsService"];

        constructor(
            protected $window: ng.IWindowService,
            protected $attrs: IStoreMyAccountMenuAttributes,
            protected sessionService: insite.account.ISessionService,
            protected $scope: ng.IScope,
            protected settingsService: insite.core.ISettingsService) {
            this.init();
        }

        init(): void {
            this.currentPage = this.$attrs.currentPage;

            this.$scope.$on("sessionLoaded", (event, session) => {
                if (session) {
                    this.isAuthenticated = session.isAuthenticated;
                    this.isAdministrator = session.userRoles && session.userRoles.includes("Administrator");
                }
            });

            this.settingsService.getSettings().then((settingsCollection: insite.core.SettingsCollection) => {
                this.collectiveSettings = (settingsCollection as any).collectiveSettingsSettings;
            });
        }

        signOut(returnUrl: string): void {
            this.sessionService.signOut().then(
                (signOutResult: string) => { this.signOutCompleted(signOutResult, returnUrl); },
                (error: any) => { this.signOutFailed(error); });
        }

        protected signOutCompleted(signOutResult: string, returnUrl: string): void {
            this.$window.location.href = returnUrl;
        }

        protected signOutFailed(error: any): void {
        }
    }

    angular
        .module("insite")
        .controller("StoreMyAccountMenuController", StoreMyAccountMenuController);
}