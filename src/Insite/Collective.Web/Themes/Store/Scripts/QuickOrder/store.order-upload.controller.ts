﻿module store.quickorder {
    "use strict";

    enum StoreUploadError {
        None,
        NotEnough,
        ConfigurableProduct,
        StyledProduct,
        Unavailable,
        InvalidUnit,
        NotFound,
        OutOfStock,
        Superseded,
        Obsolete
    }

    export class StoreOrderUploadController extends insite.quickorder.OrderUploadController {
        static $inject = ["$scope", "productService", "cartService", "coreService"];

        constructor(
            protected $scope: ng.IScope,
            protected productService: insite.catalog.IProductService,
            protected cartService: insite.cart.ICartService,
            protected coreService: insite.core.ICoreService) {
            super($scope, productService, cartService, coreService);

            this.firstRowHeading = true;
        }

        setFile(arg): void {
            if (arg.files.length > 0) {
                this.file = arg.files[0];
                this.fileName = this.file.name;
                const fileExtension = this.getFileExtension(this.file.name);
                this.badFile = ["xls", "xlsx", "csv"].indexOf(fileExtension) === -1;
                this.uploadLimitExceeded = false;
            } else {
                this.fileName = null;
            }

            setTimeout(() => {
                this.$scope.$apply();
            });
        }

        cancelUpload(): void {
            this.uploadCancelled = true;
            this.coreService.closeModal("#orderUploadingPopup");
            this.cleanupUploadData();

            if (this.productRequests) {
                for (let i = 0; i < this.productRequests.length; i++) {
                    (this.productRequests[i] as any).cancel();
                }
            }
        }

        closeIssuesPopup(): void {
            this.uploadCancelled = true;
            this.coreService.closeModal("#orderUploadingIssuesPopup");
            this.cleanupUploadData();
        }

        continueToCart(popupSelector?: string): void {
            if (popupSelector) {
                this.coreService.closeModal(popupSelector);
            }

            this.allowCancel = false;

            this.coreService.displayModal(angular.element("#orderUploadingPopup"));

            this.cartService.addLineCollectionFromProducts(this.products, true, false).then(
                (cartLineCollection: CartLineCollectionModel) => { this.addLineCollectionFromProductsCompleted(cartLineCollection); },
                (error: any) => { this.addLineCollectionFromProductsFailed(error); });
        }


        protected cleanupUploadData(): void {
            this.productRequests = null;
            this.errorProducts = null;
            this.products = null;
            this.fileName = null;
            this.file = null;
            $("#hiddenFileUpload").val(null);
            setTimeout(() => { this.$scope.$apply(); });
        }

        protected getProductsCompleted(productCollection: ProductCollectionModel, item: any, index: any): void {
            if (this.uploadCancelled) {
                return;
            }

            const products = productCollection.products;

            if (products && products.length === 1) {
                const product = products[0];
                const error = this.storeValidateProduct(product);

                product.qtyOrdered = !item.Qty ? 1 : item.Qty;

                if (error === StoreUploadError.None) {
                    const isErrorProduct = this.setProductUnitOfMeasure(product, item, index);

                    if (!isErrorProduct) {
                        this.addProductToList(product, item, index);
                    }
                } else {
                    this.errorProducts.push(this.storeMapProductErrorInfo(index, error, item.Name, product));
                }
            } else {
                this.errorProducts.push(this.storeMapProductErrorInfo(index, StoreUploadError.NotFound, item.Name, <ProductDto>{
                    qtyOrdered: !item.Qty ? 1 : item.Qty,
                    unitOfMeasureDisplay: item.UM
                }));
            }

            this.checkCompletion();
        }

        protected storeMapProductErrorInfo(index: number, error: StoreUploadError, name: string, product: ProductDto): any {
            return {
                index: index,
                error: StoreUploadError[error],
                supersededBy: JSON.parse(product.properties["supersededBy"]),
                name: name,
                qtyRequested: product.qtyOrdered,
                umRequested: product.unitOfMeasureDisplay,
                qtyOnHands: product.qtyOnHand
            };
        }

        protected storeValidateProduct(product: ProductDto): StoreUploadError {
            if (product.properties["isSuperseded"] === "true") {
                return StoreUploadError.Superseded;
            }

            if (product.properties["isObsolete"] === "true") {
                return StoreUploadError.Obsolete;
            }

            if (product.qtyOnHand === 0 && product.trackInventory && !product.canBackOrder) {
                return StoreUploadError.OutOfStock;
            }

            if (product.canConfigure || (product.isConfigured && !product.isFixedConfiguration)) {
                return StoreUploadError.ConfigurableProduct;
            }

            if (product.isStyleProductParent) {
                return StoreUploadError.StyledProduct;
            }

            if (!product.canAddToCart) {
                return StoreUploadError.Unavailable;
            }

            return StoreUploadError.None;
        }

        protected processWb(wb): void {
            this.bulkSearch = [];
            wb.SheetNames.forEach((sheetName) => {
                const opts = { header: 1 };
                let roa = this.XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName], opts);
                if (roa.length > 0) {
                    if (this.firstRowHeading) {
                        roa = roa.slice(1, roa.length);
                    }
                    roa = roa.filter(r => { return r[0] != null && r[0].length > 0; });
                    if (this.limitExceeded(roa.length)) {
                        return;
                    }
                    this.bulkSearch = roa.map(r => {
                        const obj = { Name: r[0], Qty: r[1], UM: "" };
                        return obj;
                    });
                }
            });
            this.bulkSearchProducts();
        }

        protected processCsv(data: string): void {
            this.bulkSearch = [];
            const newLineIndex = data.lastIndexOf("\r\n");
            if (newLineIndex + 2 === data.length) {
                data = data.substr(0, newLineIndex);
            }
            const results = Papa.parse(data);
            if (results.errors.length > 0) {
                this.badFile = true;
                return;
            }
            let rows = results.data;
            if (this.firstRowHeading) {
                rows = rows.slice(1, rows.length);
            }
            if (this.limitExceeded(rows.length)) {
                return;
            }
            rows.forEach((s) => {
                if (s[0] == null || s[0].length === 0) {
                    return;
                }
                const objectToAdd: any = {};
                objectToAdd.Name = s[0];
                if (s[1]) {
                    objectToAdd.Qty = s[1];
                }
                objectToAdd.UM = "";
                this.bulkSearch.push(objectToAdd);
            });
            this.bulkSearchProducts();
        }
    }

    angular
        .module("insite")
        .controller("OrderUploadController", StoreOrderUploadController);
}