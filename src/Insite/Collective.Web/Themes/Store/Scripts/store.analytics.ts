﻿module store.analytics {
    import AnalyticsTrackingEventModel = Store.Analytics.Models.AnalyticsTrackingEventModel;
    "use strict";

    export interface IStoreAnalyticsService {
        pushTrackingEvent(event: AnalyticsTrackingEventModel): void;
    }

    export class StoreAnalyticsService implements IStoreAnalyticsService {

        static $inject = ["$http", "$window"];

        constructor(
            protected $http: ng.IHttpService,
            protected $window: ng.IWindowService) { }

        pushTrackingEvent(event: AnalyticsTrackingEventModel): void {
            if (this.$window.hasOwnProperty("dataLayer")) {
                this.$window.dataLayer.push(event);
            }
        }

    }

    angular
        .module("insite")
        .service("storeAnalyticsService", StoreAnalyticsService);
}
