﻿module insite.layout {
    "use strict";

    export class StoreTopNavController extends TopNavController {
        cartTotalCountDisplay: string;
        collectiveSettings: Store.Settings.Models.ICollectiveSettingsModel;
        isAdministrator: boolean;
        location: LocationModel;

        static $inject = ["$window", "$attrs", "sessionService", "websiteService", "coreService", "$scope", "$timeout", "cartService", "settingsService", "locationService"];

        constructor(
            protected $window: ng.IWindowService,
            protected $attrs: ITopNavControllerAttributes,
            protected sessionService: account.ISessionService,
            protected websiteService: websites.IWebsiteService,
            protected coreService: core.ICoreService,
            protected $scope: ng.IScope,
            protected $timeout: ng.ITimeoutService,
            protected cartService: cart.ICartService,
            protected settingsService: core.ISettingsService,
            protected locationService: locations.ILocationService) {

            super($window, $attrs, sessionService, websiteService, coreService);

            this.storeInit();
        }

        changeCustomer(url: string): void {
            this.coreService.redirectToPath(`${url}?returnUrl=${this.coreService.getCurrentPath()}`);
        }

        storeInit(): void {

            this.$scope.$on('updateWarehouse', (event, warehouse) => {
                this.location.warehouse = warehouse;
            });

            this.$scope.$on("cartLoaded", (event, cart) => {
                this.onCartLoaded(cart);
            });

            this.$scope.$on("sessionLoaded", (event, session) => {
                if (session) {
                    this.isAdministrator = session.userRoles && session.userRoles.includes("Administrator");
                }
            });

            this.$scope.$on("sessionChanged", () => {
                this.getSession();
            });

            // use a short timeout to wait for anything else on the page to call to load the cart
            this.$timeout(() => {
                if (!this.cartService.cartLoadCalled) {
                    this.getCart();
                }
            }, 20);

            this.settingsService.getSettings().then((settingsCollection: core.SettingsCollection) => {
                this.collectiveSettings = (settingsCollection as any).collectiveSettingsSettings;
            });

            this.initLocation();
        }

        initLocation(): void {
            this.locationService.get().then(
                (location: LocationModel) => { this.getLocationCompleted(location); },
                (error: any) => { this.getLocationFailed(error); });
        }

        protected getLocationCompleted(location: LocationModel): void {
            this.location = location;
        }

        protected getLocationFailed(error: any): void {
        }

        protected onCartLoaded(cart: CartModel): void {
            this.cartTotalCountDisplay = cart.totalCountDisplay > 99 ? "99+" : cart.totalCountDisplay.toString();
        }

        protected getCart(): void {
            this.cartService.getCart();
        }
    }

    angular
        .module("insite")
        .controller("TopNavController", StoreTopNavController);
}