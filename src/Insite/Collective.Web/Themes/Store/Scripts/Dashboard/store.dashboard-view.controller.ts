﻿module store.dashboard {
    "use strict";

    export class StoreDashboardViewController extends insite.dashboard.DashboardViewController {
        sessionReady: boolean;
        addressFields: AddressFieldCollectionModel;
        billTo: Insite.Customers.WebApi.V1.ApiModels.BillToModel;
        shipTo: Insite.Customers.WebApi.V1.ApiModels.ShipToModel;
        userLabel: string;
        userEmail: string;
        userRoles: string;
        showBillToErrorMessage: boolean;
        showShipToErrorMessage: boolean;

        billToEditFormId = "dashboard-bill-to-edit-form";
        shipToEditFormId = "dashboard-ship-to-edit-form";

        static $inject = ["$scope", "wishListService", "sessionService", "dashboardService", "$attrs", "cartService", "customerService", "websiteService"];

        constructor(
            protected $scope: ng.IScope,
            protected wishListService: insite.wishlist.WishListService,
            protected sessionService: insite.account.ISessionService,
            protected dashboardService: insite.dashboard.IDashboardService,
            protected $attrs: insite.dashboard.IDashboardViewAttributes,
            protected cartService: cart.IStoreCartService,
            protected customerService: insite.customers.ICustomerService,
            protected websiteService: insite.websites.IWebsiteService) {
            super($scope, wishListService, sessionService, dashboardService, $attrs, cartService);
            this.storeInit();
        }

        storeInit() {
            this.websiteService.getAddressFields().then((model: AddressFieldCollectionModel) => { this.addressFields = model; });
            this.getBillTo();
            this.getShipTo();
        }

        protected getBillTo(): void {
            this.customerService.getBillTo("country,state,validation").then(result => { this.billTo = result });
        }

        protected getEditForm(formId: string): any {
            return angular.element(`#${formId}`);
        }

        protected getSessionCompleted(session: SessionModel): void {
            super.getSessionCompleted(session);

            this.userLabel = session.userLabel;
            this.userEmail = session.email;
            this.userRoles = session.properties["userRolesDisplay"];

            this.validateBillToShipTo();
            this.sessionReady = true;
        }

        protected getShipTo(): void {
            this.customerService.getShipTo("country,state,validation").then(result => { this.shipTo = result });
        }

        protected validateBillToShipTo(): boolean {
            const billToIsValid = $("#btaddressForm").validate({ ignore: "" }).form();
            const shipToIsValid = $("#staddressForm").validate({ ignore: "" }).form();

            this.showBillToErrorMessage = !billToIsValid;
            this.showShipToErrorMessage = !shipToIsValid;

            return billToIsValid && shipToIsValid;
        }
    }

    angular
        .module("insite")
        .controller("DashboardViewController", StoreDashboardViewController);
}