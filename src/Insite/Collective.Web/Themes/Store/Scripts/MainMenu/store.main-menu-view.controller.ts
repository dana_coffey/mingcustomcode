﻿module insite.menu {
    "use strict";

    export class MainMenuViewController {
        categories: Insite.Catalog.WebApi.V1.ApiModels.CategoryModel[];
        location: Store.Catalog.WebApi.V1.ApiModels.LocationModel;

        static $inject = ["$scope", "coreService", "productService", "locationService"];

        constructor(
            protected $scope: ng.IScope,
            protected coreService: insite.core.ICoreService,
            protected productService: insite.catalog.IProductService,
            protected locationService: locations.ILocationService) {

            productService.getCategoryTree("", 1).then(result => { this.categories = result.categories; });
            this.locationService.get().then((location: LocationModel) => { this.location = location; });
        }
    }

    angular
        .module("insite")
        .controller("MainMenuViewController", MainMenuViewController)
        .directive("storeMainMenuView", () => ({
            restrict: "E",
            replace: true,
            scope: {
            },
            templateUrl: "/PartialViews/MainMenu-MainMenuView",
            controller: "MainMenuViewController",
            controllerAs: "vm",
            bindToController: true
        }));

}