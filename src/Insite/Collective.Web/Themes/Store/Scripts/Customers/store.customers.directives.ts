﻿module store.customers {
    "use strict";

    angular.module("insite")
        .directive("storeCustomersAddressEdit",
            () => ({
                restrict: "E",
                replace: true,
                scope: {
                    prefix: "@",
                    address: "=",
                    addressFields: "=",
                    countries: "=",
                    isReadOnly: "=",
                    save: "&",
                    cancel: "&"
                },
                templateUrl: "/PartialViews/Customers-AddressEdit"
            }))
        .directive("storeCustomersAddressSummary",
            () => ({
                restrict: "E",
                replace: true,
                templateUrl: "/PartialViews/Customers-AddressSummary",
                scope: {
                    address: "="
                }
            }));
}