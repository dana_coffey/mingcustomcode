﻿module insite.core {
    "use strict";

    export class StoreCoreService extends CoreService {
        static $inject = ["$rootScope", "$http", "$filter", "$window", "$location", "$sessionStorage", "$timeout"];
        modalTransitionDuration: Number;

        constructor(
            protected $rootScope: ng.IRootScopeService,
            protected $http: ng.IHttpService,
            protected $filter: ng.IFilterService,
            protected $window: ng.IWindowService,
            protected $location: ng.ILocationService,
            protected $sessionStorage: common.IWindowStorage,
            protected $timeout: ng.ITimeoutService) {
            super($rootScope, $http, $filter, $window, $location, $sessionStorage, $timeout);
        }
        
        closeModal(selector: string): void {
            const modal = angular.element(`${selector}`);
            if (typeof (modal) !== "undefined" && modal !== null && modal.length > 0) {
                (modal as any).foundation("close");
            }
        }

        displayModal(html: any, onClose: any): void {
            const $html = $(html);
            if ($html.parents("body").length === 0) {
                $html.appendTo($("body"));
            }

            if ($html.data('reveal') == undefined) {
                new Foundation.Reveal($html).open();
            } else {
                ($html as any).foundation('open').off("closed.zf.reveal");
            }

            if (typeof onClose === "function") {
                $html.on("closed.zf.reveal", onClose);
            }
        }

        refreshUiBindings(): void {
            PubSub.publish("nwayo.foundation-reflow");
        }

        protected closeAllModals(): void {
            // Modals
            const $modals = ($('[data-reveal]') as any);
            if ($modals.length) {
                $modals.foundation('close');
            }

            // Datepickers (Pick A Date)
            $('body > .picker').remove();
        }
    }

    angular.module("insite")
        .service("coreService", StoreCoreService);
}