﻿module store.core {
    "use strict";

    angular.module("insite")
        .config(["$provide", ($provide: any) => {
            $provide.decorator("iscPopupTemplateDirective", ["$delegate", ($delegate) => {
                const directive = $delegate[0];

                directive.template = undefined;
                directive.templateUrl = "/PartialViews/Core-PopupTemplate";
                directive.$$isolateBindings['editable'] = { attrName: 'editable', mode: '=', optional: true };

                directive.compile = () => {
                    return (scope, elem, attr) => {
                        // Init new modal
                        PubSub.publish("nwayo.foundation-reflow", { element: elem });
                        // Clear old modals with same id
                        attr.$observe('id', function (val) {
                            const $existingPopups = $(`[id="${val}"]`).not(elem.get(0));
                            $existingPopups.parent(".reveal-overlay").remove();
                            $existingPopups.remove();
                        });
                    };
                }

                return $delegate;
            }]);
        }]);
}