﻿module store.core {
    "use strict";

    export class StoreGeolocationService {
        static $inject = ["$q", "$window"];
        currentPosition: any;

        constructor(protected $q: ng.IQService, protected $window: ng.IWindowService) {
        }

        getCurrentPosition() {
            var deferred = this.$q.defer();

            if (!this.$window.navigator.geolocation) {
                deferred.reject("Geolocation not supported.");
            } else {
                this.$window.navigator.geolocation.getCurrentPosition(
                    position => {
                        this.currentPosition = position;
                        deferred.resolve(position);
                    },
                    err => {
                        if (err.code != 1 && this.currentPosition) {
                            deferred.resolve(this.currentPosition);
                        } else {
                            deferred.reject(err);
                        }
                    });
            }

            return deferred.promise;
        }

        getDistanceFromLocationInKm(lat1, lon1, lat2, lon2) {
            const radius = 6371; // Radius of the earth in km
            const p = Math.PI / 180;
            const latitudeDegrees = Math.sin(((lat2 - lat1) * p) / 2);
            const longitudeDegrees = Math.sin(((lon2 - lon1) * p) / 2);

            const a = latitudeDegrees * latitudeDegrees + Math.cos(lat1 * p) * Math.cos(lat2 * p) * longitudeDegrees * longitudeDegrees;
            return radius * (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
        }
    }

    angular.module("insite")
        .service("geolocationService", StoreGeolocationService);
}