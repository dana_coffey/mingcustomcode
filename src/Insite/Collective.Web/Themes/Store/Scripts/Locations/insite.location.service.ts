﻿import LocationModel = Store.Catalog.WebApi.V1.ApiModels.LocationModel;
import OrderLocationModel = Store.Catalog.WebApi.V1.ApiModels.OrderLocationModel;

module insite.locations {
    "use strict";

    export interface ILocationService {
        get(): ng.IPromise<LocationModel>;
        mapOrderLocationModelFromLocation(location: LocationModel): OrderLocationModel;
        update(location: LocationModel): void;
        reset(): void;
    }

    export class LocationService implements ILocationService {
        serviceUri = "/api/v1/locations";

        static $inject = ["$http", "httpWrapperService"];

        constructor(
            protected $http: ng.IHttpService,
            protected httpWrapperService: core.HttpWrapperService) {
        }

        get(): ng.IPromise<LocationModel> {
            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ url: this.serviceUri + "/current", method: "GET" }),
                () => {},
                () => {}
            );
        }

        mapOrderLocationModelFromLocation(location: LocationModel): OrderLocationModel {
            return {
                address1: location.address1,
                address2: location.address2,
                address3: location.address3,
                address4: location.address4,
                city: location.city,
                postalCode: location.postalCode,
                stateId: location.state.id,
                state: location.state.abbreviation,
                countryId: location.country.id,
                country: location.country.abbreviation
            } as OrderLocationModel;
        }
        
        update(location: LocationModel): void {
            this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ url: this.serviceUri + "/update", method: "POST", data: location }),
                () => {},
                () => {}
            );
        }

        reset(): void {
            this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ url: this.serviceUri + "/reset", method: "POST" }),
                () => {},
                () => {}
            );
        }
    }

    angular
        .module("insite")
        .service("locationService", LocationService);
}