﻿module insite.cart {
    "use strict";

    export class StoreCartLinesController extends CartLinesController {
        static $inject = ["$scope", "cartService", "productSubscriptionPopupService", "spinnerService"];

        constructor(
            protected $scope: ICartScope,
            protected cartService: ICartService,
            protected productSubscriptionPopupService: catalog.ProductSubscriptionPopupService,
            protected spinnerService: core.ISpinnerService) {
            super($scope, cartService, productSubscriptionPopupService, spinnerService);
            this.init();
        }

        updateLine(cartLine: CartLineModel, refresh: boolean): void {
            if (refresh) {
                this.isUpdateInProgress = true;
            }
            if (!cartLine.qtyOrdered || parseFloat(cartLine.qtyOrdered.toString()) <= 0) {
                this.removeLine(cartLine);
            } else {
                this.spinnerService.show();
                this.cartService.updateLine(cartLine, refresh).then(
                    (cartLineModel: CartLineModel) => { this.updateLineCompleted(cartLineModel); },
                    (error: any) => { this.updateLineFailed(error); });
            }
        }
    }

    angular
        .module("insite")
        .controller("CartLinesController", StoreCartLinesController);
}