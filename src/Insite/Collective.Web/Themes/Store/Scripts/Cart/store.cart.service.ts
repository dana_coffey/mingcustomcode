﻿module store.cart {
    import CountryCollectionModel = Insite.Websites.WebApi.V1.ApiModels.CountryCollectionModel;
    "use strict";

    export interface IStoreCartService extends insite.cart.ICartService {
        getCartWithParams(cartId?: string, params?: any): ng.IPromise<CartModel>;
        updateCartWithParams(cart: CartModel, params?: any, suppressApiErrors?: boolean): ng.IPromise<CartModel>;
    }

    export class StoreCartService extends insite.cart.CartService implements IStoreCartService {
        location: LocationModel;

        static $inject = ["$http", "$rootScope", "$q", "addressErrorPopupService", "addToCartPopupService", "apiErrorPopupService", "httpWrapperService"];

        constructor(
            protected $http: ng.IHttpService,
            protected $rootScope: ng.IRootScopeService,
            protected $q: ng.IQService,
            protected addressErrorPopupService: insite.cart.IAddressErrorPopupService,
            protected addToCartPopupService: insite.cart.IAddToCartPopupService,
            protected apiErrorPopupService: insite.core.IApiErrorPopupService,
            protected httpWrapperService: insite.core.HttpWrapperService) {

            super($http, $rootScope, $q, addressErrorPopupService, addToCartPopupService, apiErrorPopupService, httpWrapperService);
        }

        protected addLineCompleted(response: ng.IHttpPromiseCallbackArg<CartLineModel>, showAddToCartPopup = false): void {
            const cartLine = response.data;
            this.addToCartPopupService.display({ isQtyAdjusted: cartLine.isQtyAdjusted, showAddToCartPopup: showAddToCartPopup });
            cartLine.availability = cartLine.availability;
            this.getCart();
            this.$rootScope.$broadcast("cartChanged");
        }

        protected getCartParams(customParams: any = {}): any {
            const params: any = super.getCartParams();

            Object.keys(customParams).forEach((key) => {
                params[key] = customParams[key];
            });

            return params;
        }

        getCartWithParams(cartId?: string, params?: any): ng.IPromise<CartModel> {
            if (!cartId) {
                cartId = "current";
            }

            if (cartId === "current") {
                this.cartLoadCalled = true;
            }

            const uri = `${this.serviceUri}/${cartId}`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "GET", url: uri, params: this.getCartParams(params), bypassErrorInterceptor: true }),
                (response: ng.IHttpPromiseCallbackArg<CartModel>) => { this.getCartCompleted(response, cartId); },
                this.getCartFailed);
        }

        updateCartWithParams(cart: CartModel, params: any = {}, suppressApiErrors = false): ng.IPromise<CartModel> {
            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http({ method: "PATCH", url: cart.uri, data: cart, params: this.getCartParams(params) }),
                this.updateCartCompleted,
                suppressApiErrors ? this.updateCartFailedSuppressErrors : this.updateCartFailed);
        }
    }

    angular
        .module("insite")
        .service("cartService", StoreCartService);
}
