﻿(insite as any).core = (($) => {
    "use strict";

    const that: any = {};

    that.signInUrl = "/RedirectTo/SignInPage";
    that.dateTimeFormat = "";
    that.generalErrorText = "";

    that.displayModal = (html, onClose) => {
        const $html = $(html);
        if ($html.parents("body").length === 0) {
            $html.appendTo($("body"));
        }

        if ($html.data('reveal') == undefined) {
            new Foundation.Reveal($html).open();
        } else {
            ($html as any).foundation('open').off("closed.zf.reveal");
        }

        if (typeof onClose === "function") {
            $html.on("closed.zf.reveal", onClose);
        }
    };

    that.closeModal = $modal => {
        $modal.foundation('close');
    };

    that.setupPunchoutKeepAlive = () => {
        setInterval(() => { $.post("/Punchout/punchoutsessionstatus.isch"); }, 900000);
    };

    that.checkForIFrame = () => { // used to bust out of iframes for pdass compliance
        try {
            if (window.parent != null && window.location !== window.parent.location && window.parent.location.pathname.toLowerCase().indexOf("/contentadmin") < 0) {
                (top as any).location = self.location.href;
            }
        } catch (e) {
            console.log("Problem trying to check for iframe busting, this is usually a problem with http vs https or cross domain origin");
            console.log(e);
        }
    };

    that.isCustomErrorEnabled = () => ($("html").attr("data-isCustomErrorEnabled").toLowerCase() === "true");

    that.datepicker = (selector, onCloseCallback, onSetCallback) => {
        if (typeof (selector) === "string") {
            selector = $(selector);
        }

        selector.each(function () {
            if (that.dateTimeFormat === "") {
                console.log("insite.core.dateTimeFormat has not been initialized to the format expected by the server");
                return;
            }
            const $this = $(this);
            $this.pickadate({
                format: that.dateTimeFormat.toLowerCase(),
                formatSubmit: that.dateTimeFormat.toLowerCase(),
                selectYears: true,
                onOpen() {
                    $this.blur();
                },
                onClose() {
                    $this.blur();
                    if (typeof (onCloseCallback) === "function") {
                        onCloseCallback();
                    }
                },
                onSet() {
                    if (typeof (onSetCallback) === "function") {
                        onSetCallback();
                    }
                },
                min: that.pickadateMinMax($this.attr("data-minDate")),
                max: that.pickadateMinMax($this.attr("data-maxDate"))
            });
        });
    };

    that.pickadateMinMax = data => {
        // pickadate allows min/max values of undefined, int (ie. 30 days), or a date which should be passed in according to javascript "new Date()" documentation
        if (typeof data === "undefined") {
            return data;
        }
        return isNaN(data) ? new Date(data) : Number(data);
    };

    that.timepicker = selector => {
        if (typeof (selector) === "string") {
            selector = $(selector);
        }

        selector.pickatime();
    };

    that.setupAjaxError = (onClose, on401) => {
        $(document).ajaxError((e, jqxhr, settings) => {
            if (settings.error) { // for ajax calls that already have an error function defined, we don't want to show the error below
                return;
            }
            if (jqxhr.status === 401) { // for ajax requests after the login has expired, this will redirect to the login page with the proper return url
                if (typeof (on401) === "function") {
                    on401();
                } else {
                    window.location.href = window.location.href;
                }
            } else {
                if (jqxhr.status >= 500 && jqxhr.responseText !== "") {
                    if (!that.isCustomErrorEnabled()) {
                        const width = $(window).width() - 120;
                        if (typeof (($ as any).modal) !== "undefined") {
                            ($ as any).modal.close();
                        }
                        that.displayModal(`<div class='reveal-modal cms-modal' style='width: ${width}px'><button class='simplemodal-close button' style='float: right;'>X</button><div class='modal-wrap'><iframe id='errorFrame' style='width: 100%; height: 800px;'></iframe><a class='close-reveal-modal'>&#215;</a></div></div>`, onClose);
                        const iframeDocument = (document.getElementById("errorFrame") as any).contentWindow.document;
                        iframeDocument.open();
                        iframeDocument.write(jqxhr.responseText);
                        iframeDocument.close();
                    } else {
                        alert((insite as any).core.generalErrorText);
                    }
                }
            }
        });
    };

    that.setup = () => {
        $.ajaxSetup({
            type: "POST",
            cache: false
        });

        that.setupAjaxError();
    };

    // From the Foundation 4 to 5, the section functionality was removed
    // this code emulates the functionality.
    $("body").on("click", ".section-container .title", function () {
        const $this = $(this);
        const $thisParent = $this.closest("section");

        if ($thisParent.hasClass("active")) {
            $thisParent.removeClass("active");
        } else {
            $thisParent.addClass("active");
        }
    });

    return that;
})(jQuery);