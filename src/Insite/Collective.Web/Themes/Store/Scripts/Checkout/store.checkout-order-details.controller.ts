﻿module store.checkout {
    "use strict";

    export class StoreCheckoutOrderDetailsController {
        addressFields: AddressFieldCollectionModel;
        billToEditFormId = "bill-to-edit-form";
        canEditShipTo = false;
        canChangeBillTo = false;
        canChangeShipFrom = false;
        cart: CartModel;
        cartHasRestrictedProducts = false;
        countries: CountryModel[];
        defaultWarehouse: WarehouseModel;
        pickFromWarehouses: WarehouseModel[];
        shipFromWarehouses: WarehouseModel[];
        shipToEditFormId = "ship-to-edit-form";
        shipTos: ShipToModel[];
        showBillToErrorMessage: boolean;
        showPickupInfo: boolean;
        showShippingInfo: boolean;
        showShipToErrorMessage: boolean;
        warehouse: WarehouseModel;
        location: LocationModel;
        allRestricted = false;
        cartSettings: CartSettingsModel;

        ready = false;

        static $inject = [
            "$scope", "cartService", "websiteService", "warehouseService", "customerService", "coreService", "$timeout",
            "spinnerService", "locationService", "settingsService", "$rootScope"
        ];

        constructor(protected $scope: ng.IScope,
            protected cartService: store.cart.IStoreCartService,
            protected websiteService: insite.websites.IWebsiteService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected customerService: insite.customers.ICustomerService,
            protected coreService: insite.core.ICoreService,
            protected $timeout: ng.ITimeoutService,
            protected spinnerService: insite.core.ISpinnerService,
            protected locationService: insite.locations.ILocationService,
            protected settingsService: insite.core.ISettingsService,
            protected $rootScope: ng.IRootScopeService) {
            this.init();
        }

        init(): void {
            this.getCart(true);
            this.websiteService.getAddressFields().then((model: AddressFieldCollectionModel) => {
                this.getAddressFieldsCompleted(model);
            });
            this.websiteService.getCountries("states").then((countryCollection: CountryCollectionModel) => {
                this.getCountriesCompleted(countryCollection);
            });
            this.warehouseService.getPickUpWarehouses().then((warehouseCollection: WarehouseCollectionModel) => {
                this.getPickUpWarehousesCompleted(warehouseCollection);
            });
            this.warehouseService.getShipFromWarehouses().then((warehouseCollection: WarehouseCollectionModel) => {
                this.getShipFromWarehouseCompleted(warehouseCollection);
            });
            this.locationService.get().then((location: LocationModel) => {
                this.getLocationCompleted(location);
            });
            this.settingsService.getSettings().then(
                (settings: insite.core.SettingsCollection) => { this.getSettingsCompleted(settings); },
                (error: any) => { this.getSettingsFailed(error); });
            this.$scope.$on("sessionLoaded", (event, session) => {
                if (session) {
                    if (session.properties.warehouse) {
                        this.defaultWarehouse = JSON.parse(session.properties.warehouse);
                        if (this.defaultWarehouse !== undefined) {
                            this.setWarehouse();
                        }
                    }
                }
            });
        }

        protected getLocationCompleted(location: LocationModel): void {
            this.location = location;
        }

        protected getSettingsCompleted(settingsCollection: insite.core.SettingsCollection): void {
            this.cartSettings = settingsCollection.cartSettings;
        }

        protected getSettingsFailed(error: any): void {
        }

        cancelEditBillTo(): void {
            this.getCart(false);
        }

        cancelEditShipTo(): void {
            this.getCart(false);
        }

        changeCustomer(url: string): void {
            this.coreService.redirectToPath(`${url}?returnUrl=${this.coreService.getCurrentPath()}`);
        }

        changeShipTo(): void {
            if (this.cart.shipTo.isNew === false) {
                this.cartService.updateCartWithParams(this.cart, { forceCartRepricing: true }).then((cart: CartModel) => {
                    this.changeShipToCompleted();
                });
            } else {
                this.cart.shipTo.properties["customShippingAddressCartId"] = this.cart.properties["cartId"];
                this.cart.shipTo.country = this.countries.find(x => x.abbreviation === this.cart.shipTo.country.abbreviation);
                this.canEditShipTo = true;
                this.showShipToErrorMessage = false;
                this.coreService.displayModal(`#${this.shipToEditFormId}`);
            }
        }

        continueCheckout(continueUrl: string): void {
            const mainFormIsValid = $("#orderDetailsForm").validate().form();
            const billToShipToIsValid = this.validateBillToShipTo();

            if (mainFormIsValid && billToShipToIsValid) {
                this.cartService.updateCartWithParams(this.cart, { removeRestrictedItems: true })
                    .then((cart: CartModel) => { this.updateCartCompleted(cart, continueUrl); });
            }
        }

        saveBillTo(): void {
            const valid = $("#btaddressForm").validate().form();
            if (!valid) {
                return;
            }

            this.getEditForm(this.billToEditFormId).addClass("loading");
            this.customerService.updateBillTo(this.cart.billTo).then((model: BillToModel) => {
                this.updateBillToCompleted(model);
            });
        }

        saveShipTo(): void {
            const valid = $("#staddressForm").validate().form();
            if (!valid) {
                return;
            }
            this.getEditForm(this.shipToEditFormId).addClass("loading");
            this.customerService.addOrUpdateShipTo(this.cart.shipTo).then((model: ShipToModel) => {
                this.updateShipToCompleted(model);
            });
        }

        changeShipVia(): void {
            this.warehouse = null;
            this.cart.properties["warehouse"] = "";
            this.updateShipVia();
        }

        updateShipVia(): void {
            if (this.cart.shipVia === null) {
                this.showPickupInfo = false;
                this.showShippingInfo = false;
                this.warehouse = null;
                this.cart.properties["warehouse"] = "";
            }
            this.cartService.updateCart(this.cart).then((cart: CartModel) => { this.updateShipViaCompleted(); });
        }

        updateWarehouse(): void {
            if(this.cart !== undefined){
                var originalProperty = this.cart.properties["warehouse"];

                if (this.warehouse != null) {
                    this.cart.properties["warehouse"] = this.warehouse.name;
                    this.location.warehouse = this.warehouse;
                    this.$rootScope.$broadcast("updateWarehouse", this.warehouse);
                } else {
                    this.cart.properties["warehouse"] = "";
                }

                if (this.cart.properties["warehouse"] !== originalProperty) {
                    this.locationService.update(this.location);
                    this.cartService.updateCartWithParams(this.cart, { forceCartRepricing: true }).then((cart: CartModel) => {
                        this.updateWarehouseCompleted();
                    });
                }
            }
        }

        protected changeShipToCompleted(): void {
            this.showShipToErrorMessage = false;
            this.getCart();
        }

        protected getAddressFieldsCompleted(addressFields: Insite.Websites.WebApi.V1.ApiModels.
            AddressFieldCollectionModel): void {
            this.addressFields = addressFields;
        }

        protected getCart(isInit = false): void {
            this.$timeout(() => { this.spinnerService.show(isInit ? "mainLayout" : "order-edit", true); });
            this.cartService.expand = "shiptos,validation,carriers,cartlines,shipping,tax";
            this.cartService.getCartWithParams("", { useCartWarehouse: true })
                .then((cart: CartModel) => { this.getCartCompleted(cart, isInit); })
                .finally(() => {
                    this.$timeout(() => { this.spinnerService.hide(isInit ? "mainLayout" : "order-edit"); });
                });
        }

        protected getCartCompleted(cart: Insite.Cart.WebApi.V1.ApiModels.CartModel, isInit?: boolean): void {
            this.cart = cart;
            if (this.cart.lineCount > 0) {
                this.setUpCarrier(isInit);
                this.setUpShipVia();
                this.setShipTos();
                this.setShipTo();
                this.$timeout(() => { this.validateBillToShipTo(); });
                this.canEditShipTo = this.cart.shipTo.properties["customShippingAddressCartId"] !== undefined;
                this.customerService.getBillTos().then((billTos: BillToCollectionModel) => { this.canChangeBillTo = billTos.billTos.length > 1; });
            }

            this.allRestricted = false;
            if (this.cart.properties["cartHasRestrictedProducts"] && this.cart.cartLines && this.cart.cartLines.length > 0) {
                let isAllRestricted = true;
                for (let i = 0; i < this.cart.cartLines.length; i++) {
                    if (JSON.parse(this.cart.cartLines[i].properties["availabilityStatus"]).Status !== 5) {
                        isAllRestricted = false;
                        break;
                    }
                };
                this.allRestricted = isAllRestricted;
            }

            if (!this.ready) {
                $("#orderDetailsForm").validate();
            }

            this.ready = true;
        }

        protected getCountriesCompleted(countryCollection: Insite.Websites.WebApi.V1.ApiModels.CountryCollectionModel):
            void {
            this.countries = countryCollection.countries;
        }

        protected getEditForm(formId: string): any {
            return angular.element(`#${formId}`);
        }

        protected getPickUpWarehousesCompleted(warehouseCollection: Store.Warehouse.WebApi.V1.ApiModels.
            WarehouseCollectionModel): void {
            this.pickFromWarehouses = warehouseCollection.warehouses;
        }

        protected getShipFromWarehouseCompleted(warehouseCollection: Store.Warehouse.WebApi.V1.ApiModels.
            WarehouseCollectionModel): void {
            this.shipFromWarehouses = warehouseCollection.warehouses;
        }

        protected setShippingMethodVisibility(): void {
            if (this.cart.shipVia) {
                if (this.cart.properties["shipViaIsPickup"] === "true") {
                    this.showPickupInfo = true;
                    this.showShippingInfo = false;
                } else {
                    this.showPickupInfo = false;
                    this.showShippingInfo = true;
                }
            }
        }

        protected setShipTo(): void {
            this.shipTos.forEach(shipTo => {
                if (shipTo.id === this.cart.shipTo.id) {
                    this.cart.shipTo = shipTo;
                    if (this.cart.shipTo.country !== null) {
                        this.cart.shipTo.country =
                            this.countries.find(x => x.abbreviation === this.cart.shipTo.country.abbreviation);
                    }
                    if (this.cart.shipTo.state !== null) {
                        this.cart.shipTo.state =
                            this.cart.shipTo.country.states.find(x => x.abbreviation ===
                                this.cart.shipTo.state.abbreviation);
                    }
                }
            });
        }

        protected setShipTos() {
            this.shipTos = this.cart.billTo.shipTos;
        }

        protected setShipVia(): void {
            this.cart.carrier.shipVias.forEach(shipVia => {
                if (shipVia.id === this.cart.shipVia.id) {
                    this.cart.shipVia = shipVia;
                }
            });
        }

        protected setUpCarrier(isInit?: boolean): void {
            this.cart.carriers.forEach(carrier => {
                if (carrier.id === this.cart.carrier.id) {
                    this.cart.carrier = carrier;
                    if (isInit) {
                        this.updateCarrier();
                    }
                }
            });
        }

        protected setUpShipVia(): void {
            if (this.cart.carrier && this.cart.carrier.shipVias) {
                this.sortShipVias();
                this.setShipVia();
                this.setShippingMethodVisibility();
                this.setWarehouse();
            }
        }

        protected setWarehouse(): void {
            if (this.cart !== undefined) {
                const warehouses = this.cart.properties["shipViaIsPickup"] === "true"
                    ? this.pickFromWarehouses
                    : this.shipFromWarehouses;

                if (warehouses !== undefined)
                {
                    const websiteDefaultwarehouse = warehouses.filter(x => x.isDefault);
                    var warehouseModelFromList;
                    var customerDefaultWarehouse;

                    this.warehouse = null;

                    // Get warehouse from cart
                    if (this.cart.properties["warehouse"]) {
                        warehouseModelFromList = warehouses.filter(x => x.name === this.cart.properties["warehouse"]);
                        if (warehouseModelFromList.length > 0) {
                            this.warehouse = warehouseModelFromList[0];
                        }
                    }

                    // Get warehouse from location
                    if (this.warehouse === null && this.location.warehouse) {
                        this.warehouse = this.location.warehouse;
                    }

                    // Get from customer default warehouse
                    if (this.warehouse === null && this.defaultWarehouse !== undefined) {
                        customerDefaultWarehouse = warehouses.filter(x => x.name === this.defaultWarehouse.name);
                        if (customerDefaultWarehouse.length > 0) {
                            this.warehouse = customerDefaultWarehouse[0];
                        }
                    }

                    // Get from website
                    if (this.warehouse === null && websiteDefaultwarehouse.length > 0) {
                        this.warehouse = websiteDefaultwarehouse[0];
                    }

                    // Get first as default
                    if (this.warehouse === null && warehouses.length === 1) {
                        this.warehouse = warehouses[0];
                    }
                }

                this.updateWarehouse();
            }
        }

        protected sortShipVias(): void {
            this.cart.carrier.shipVias.sort(
                (a: Insite.Cart.Services.Dtos.ShipViaDto, b: Insite.Cart.Services.Dtos.ShipViaDto) => {
                    if (a.description > b.description) {
                        return 1;
                    }
                    if (a.description < b.description) {
                        return -1;
                    }
                    return 0;
                });
        }

        protected updateBillToCompleted(billTo: Insite.Customers.WebApi.V1.ApiModels.BillToModel): void {
            this.getEditForm(this.billToEditFormId).removeClass("loading").foundation("close");
            this.showBillToErrorMessage = false;
        }

        protected updateCarrier(): void {
            if (this.cart.carrier && this.cart.carrier.shipVias) {
                if (this.cart.carrier.shipVias.length === 1 && this.cart.carrier.shipVias[0] !== this.cart.shipVia) {
                    this.cart.shipVia = this.cart.carrier.shipVias[0];
                    this.updateShipVia();
                } else if (this.cart.carrier.shipVias.length > 1 &&
                    this.cart.carrier.shipVias.every(sv => sv.id !== this.cart.shipVia.id) &&
                    this.cart.carrier.shipVias.filter(sv => sv.isDefault).length > 0) {
                    this.cart.shipVia = this.cart.carrier.shipVias.filter(sv => sv.isDefault)[0];
                    this.updateShipVia();
                }
            }
        }

        protected updateCartCompleted(cart: Insite.Cart.WebApi.V1.ApiModels.CartModel, continueUrl: string): void {
            if (continueUrl) {
                this.coreService.redirectToPath(continueUrl);
            } else {
                this.getCart();
            }
        }

        protected updateShipToCompleted(shipTo: Insite.Customers.WebApi.V1.ApiModels.ShipToModel): void {
            this.getEditForm(this.shipToEditFormId).removeClass("loading").foundation("close");
            this.showShipToErrorMessage = false;
            if (this.cart.shipTo.isNew) {
                this.cart.shipTo = shipTo;
                this.cartService.updateCartWithParams(this.cart, { forceCartRepricing: true }).then((cart: CartModel) => {
                    this.changeShipToCompleted();
                });
            } else {
                this.getCart();
            }
        }

        protected updateShipViaCompleted(): void {
            this.getCart();
        }

        protected updateWarehouseCompleted(): void {
            this.getCart();
        }

        protected validateBillToShipTo(): boolean {
            if ($("#staddressForm").validate({ ignore: "" }) !== undefined && $("#btaddressForm").validate({ ignore: "" }) !== undefined) {
                const billToIsValid = $("#btaddressForm").validate({ ignore: "" }).form();
                const shipToIsValid = $("#staddressForm").validate({ ignore: "" }).form() || this.cart.properties["shipViaIsPickup"] === "true";

                this.showBillToErrorMessage = !billToIsValid;
                this.showShipToErrorMessage = !shipToIsValid;
                return billToIsValid && shipToIsValid;
            }
            return false;
        }
    }

    angular
        .module("insite")
        .controller("StoreCheckoutOrderDetailsController", store.checkout.StoreCheckoutOrderDetailsController);
};