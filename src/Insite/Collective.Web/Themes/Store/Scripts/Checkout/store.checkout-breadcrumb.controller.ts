﻿module store.checkout {
    "use strict";

    export class StoreCheckoutBreadcrumbController {
        orderDetailsActive: boolean;
        orderReviewActive: boolean;
        orderConfirmationActive: boolean;

        static $inject = ["$scope"];

        constructor(protected $scope: ICheckoutBreadcrumbScope) {
            this.init();
        }

        init(): void {
            this.orderDetailsActive = this.$scope.activeStep === "order-details";
            this.orderReviewActive = this.$scope.activeStep === "order-review";
            this.orderConfirmationActive = this.$scope.activeStep === "order-confirmation";
        }
    }

    angular
        .module("insite")
        .controller("StoreCheckoutBreadcrumbController", StoreCheckoutBreadcrumbController);
}