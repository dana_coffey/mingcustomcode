﻿module store.checkout {
    "use strict";

    import StoreCheckoutControllerAttributes = Store.Attributes.Models.IStoreCheckoutControllerAttributes;

    export class StoreCheckoutOrderReviewController {
        cart: CartModel;
        showTermsAndConditions: boolean;
        promotions: PromotionModel[];
        order: OrderModel;
        isPickUp: boolean;
        warehouse: WarehouseModel;
        canPlaceOrder: boolean;
        submitting: boolean;
        submitErrorMessage: string;
        checkoutOrderDetailsUrl: string;
        cartUrl: string;
        addressError: boolean;
        genericError: boolean;
        location: LocationModel;
        cartLocation: OrderLocationModel;

        static $inject = ["$scope", "cartService", "websiteService", "warehouseService", "sessionService", "spinnerService", "coreService", "$attrs", "settingsService", "locationService"];

        constructor(protected $scope: ng.IScope,
            protected cartService: store.cart.IStoreCartService,
            protected websiteService: insite.websites.IWebsiteService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected sessionService: insite.account.ISessionService,
            protected spinnerService: insite.core.ISpinnerService,
            protected coreService: insite.core.ICoreService,
            protected $attrs: StoreCheckoutControllerAttributes,
            protected settingsService: insite.core.ISettingsService,
            protected locationService: insite.locations.ILocationService) {
            this.init();
        }

        init(): void {
            this.canPlaceOrder = true;
            this.cartUrl = this.$attrs.cartUrl;
            this.getCart();

            this.settingsService.getSettings().then((settingsCollection: insite.core.SettingsCollection) => {
                this.showTermsAndConditions = (settingsCollection as any).collectiveSettingsSettings.showTermsAndConditions;
            });

            this.locationService.get().then((location: LocationModel) => {
                this.location = location;
                this.cartLocation = this.locationService.mapOrderLocationModelFromLocation(this.location);
            });
        }

        submit(submitSuccessUri: string, signInUri: string): void {
            this.submitErrorMessage = "";
            this.sessionService.getIsAuthenticated().then((isAuthenticated: boolean) => { this.getIsAuthenticatedForSubmitCompleted(isAuthenticated, submitSuccessUri, signInUri); });
        }

        protected getIsAuthenticatedForSubmitCompleted(isAuthenticated: boolean, submitSuccessUri: string, signInUri: string): void {
            if (!isAuthenticated) {
                this.coreService.redirectToPath(`${signInUri}?returnUrl=${this.coreService.getCurrentPath()}`);
                return;
            }

            if (this.cart.requiresApproval) {
                this.cart.status = "AwaitingApproval";
            } else {
                this.cart.status = "Submitted";
            }

            this.cart.properties["location"] = JSON.stringify(this.cartLocation);

            this.spinnerService.show("mainLayout", true);
            this.cartService.updateCartWithParams(this.cart, { removeRestrictedItems: true }, true).then(
                (cart: CartModel) => { this.submitCompleted(cart, submitSuccessUri); },
                (error: any) => { this.submitFailed(error); });
        }

        protected submitCompleted(cart: CartModel, submitSuccessUri: string): void {
            this.cart.id = cart.id;
            this.cartService.getCart();
            this.coreService.redirectToPath(`${submitSuccessUri}?cartid=${this.cart.id}`);
            this.spinnerService.hide();
            this.submitting = false;
        }

        protected submitFailed(error: any): void {
            this.submitting = false;
            this.submitErrorMessage = error.message;
            this.spinnerService.hide();
        }

        protected getCart(): void {
            this.cartService.expand = "shiptos,validation,carriers,cartlines,shipping,tax,paymentoptions";
            this.cartService.getCartWithParams("", { useCartWarehouse: true }).then((cart: CartModel) => { this.getCartCompleted(cart); }, (error: any) => { this.getCartFailed(error); });
        }

        protected getCartCompleted(cart: CartModel): void {
            this.cart = cart;
            if (this.cart.properties["warehouse"] && this.cart.properties["warehouse"]) {
                this.warehouseService.getWarehouse(this.cart.properties["warehouse"]).then((warehouse: WarehouseModel) => { this.getWarehouseCompleted(warehouse); });
            }

            this.isPickUp = cart.properties["shipViaIsPickup"] !== undefined;
            this.validateCartStatus();
        }

        protected getCartFailed(error: any): void {
            this.coreService.redirectToPath(this.cartUrl);
        }

        protected validateCartStatus() {
            this.sessionService.getIsAuthenticated().then(
                (isAuthenticated: boolean) => {
                    if (!isAuthenticated) {
                        this.coreService.redirectToPath(`${this.coreService.getSignInUrl()}?returnUrl=${this.coreService.getCurrentPath()}`);
                        return;
                    }
                });

            if (!this.cart.billTo.address1
                || !this.cart.billTo.postalCode
                || !this.cart.billTo.city
                || !this.cart.billTo.email
                || (this.isPickUp === false && (!this.cart.shipTo.address1
                || !this.cart.shipTo.postalCode
                || !this.cart.shipTo.city
                || !this.cart.shipTo.email))) {
                this.addressError = true;
            }

            if ((this.cart.requiresPoNumber && !this.cart.poNumber)
                || this.cart.carrier == null || !this.cart.carrier.id
                || this.cart.shipVia == null || !this.cart.shipVia.id
                || !this.cart.properties["warehouse"]
                || this.cart.requiresApproval) {
                this.genericError = true;
            }

            if (this.cart.cartLines.length <= 0 || this.addressError || this.genericError) {
                this.canPlaceOrder = false;
            }
        }

        protected getWarehouseCompleted(warehouseModel: Store.Warehouse.WebApi.V1.ApiModels.WarehouseModel) {
            this.warehouse = warehouseModel;
        }
    }

    angular
        .module("insite")
        .controller("StoreCheckoutOrderReviewController", StoreCheckoutOrderReviewController);
};