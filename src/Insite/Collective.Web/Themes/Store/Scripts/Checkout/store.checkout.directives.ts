﻿module store.checkout {
    "use strict";

    angular.module("insite")
        .directive("storeCheckoutBreadcrumb", () => ({
            controller: "StoreCheckoutBreadcrumbController",
            controllerAs: "vm",
            restrict: "E",
            replace: true,
            scope: {
                activeStep: "@"
            },
            templateUrl: "/PartialViews/Checkout-Breadcrumb"
        }))
        .directive("storeCheckoutCart", () => ({
            controller: "StoreCheckoutCartController",
            controllerAs: "vm",
            restrict: "E",
            replace: true,
            templateUrl: "/PartialViews/Checkout-Cart",
            scope: {
                cart: "="
            }
        }))
        .directive("storeCheckoutOrderDetails", () => ({
            restrict: "E",
            replace: true,
            templateUrl: "/PartialViews/Checkout-OrderDetails",
            scope: {
                cart: "=",
                cartSettings: "="
            }
        }))
        .directive("storeCheckoutOrderReviewDetails", () => ({
            restrict: "E",
            replace: true,
            templateUrl: "/PartialViews/Checkout-OrderReviewDetails",
            scope: {
                showTermsAndConditions: "=",
                cart: "=",
                location: "=",
                promotions: "=",
                order: "=",
                isPickUp: "=",
                warehouse: "=",
                navigateUrl: "@",
                navigateText: "@",
                navigateClass: "@",
                nextStep: "&",
                nextStepEnabled: "=",
                nextStepVisible: "@"
            }
        }));
}