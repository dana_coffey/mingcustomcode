﻿module store.checkout {
    "use strict";

    export class StoreCheckoutCartController {

        static $inject = ["$scope", "cartService"];

        constructor(protected $scope: ICheckoutCartScope, protected cartService: insite.cart.ICartService) {
        }
    }

    angular
        .module("insite")
        .controller("StoreCheckoutCartController", StoreCheckoutCartController);
}