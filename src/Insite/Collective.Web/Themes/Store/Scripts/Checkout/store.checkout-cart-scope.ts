﻿module store.checkout {
    "use strict";

    export interface ICheckoutCartScope extends ng.IScope {
        cart: CartModel;
    }
}