﻿module store.checkout {
    "use strict";

    import StoreCheckoutControllerAttributes = Store.Attributes.Models.IStoreCheckoutControllerAttributes;

    export class StoreCheckoutOrderConfirmationController {
        cart: CartModel;
        location: LocationModel;
        promotions: PromotionModel[];
        order: OrderModel;
        isPickUp: boolean;
        warehouse: WarehouseModel;
        cartUrl: string;

        static $inject = ["cartService", "coreService", "promotionService", "queryString", "orderService", "warehouseService", "$attrs", "$rootScope", "locationService"];

        constructor(
            protected cartService: store.cart.IStoreCartService,
            protected coreService: insite.core.ICoreService,
            protected promotionService: insite.promotions.IPromotionService,
            protected queryString: insite.common.IQueryStringService,
            protected orderService: insite.order.IOrderService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected $attrs: StoreCheckoutControllerAttributes,
            protected $rootScope: ng.IRootScopeService,
            protected locationService: insite.locations.ILocationService) {

            this.init();
        }

        init(): void {
            this.cartUrl = this.$attrs.cartUrl;
            this.cartService.expand = "cartlines,carriers";

            this.$rootScope.$broadcast("sessionChanged");

            this.cartService.getCartWithParams(this.queryString.get("cartId"), { useCartWarehouse: true }).then(
                (confirmedCart: CartModel) => { this.getConfirmedCartCompleted(confirmedCart); },
                (error: any) => { this.getConfirmedCartFailed(error); });

            this.locationService.get().then((location: LocationModel) => {
                this.location = location;
            });
        }

        protected getConfirmedCartCompleted(confirmedCart: CartModel): void {
            this.cart = confirmedCart;

            if (window.hasOwnProperty("dataLayer")) {
                const data = {
                    "event": "transactionComplete",
                    "transactionId": this.cart.orderNumber,
                    "transactionAffiliation": this.cart.billTo.companyName,
                    "transactionTotal": this.cart.orderGrandTotal,
                    "transactionTax": this.cart.totalTax,
                    "transactionShipping": this.cart.shippingAndHandling,
                    "transactionProducts": []
                };

                const cartLines = this.cart.cartLines;
                for (let key in cartLines) {
                    if (cartLines.hasOwnProperty(key)) {
                        const cartLine = cartLines[key];
                        data.transactionProducts.push({
                            "sku": cartLine.erpNumber,
                            "name": cartLine.shortDescription,
                            "price": cartLine.pricing.unitNetPrice,
                            "quantity": cartLine.qtyOrdered
                        });
                    }
                }

                (window as any).dataLayer.push(data);
            }

            this.orderService.getOrder(this.cart.orderNumber, "").then((order: OrderModel) => { this.getOrderCompleted(order); });
            this.promotionService.getCartPromotions(this.cart.id).then((promotionCollection: PromotionCollectionModel) => { this.getCartPromotionsCompleted(promotionCollection); });

            if (this.cart.properties["warehouse"]) {
                this.warehouseService.getWarehouse(this.cart.properties["warehouse"]).then((warehouse: WarehouseModel) => { this.getWarehouseCompleted(warehouse); });
            }

            this.isPickUp = this.cart.properties["shipViaIsPickup"] !== undefined;
        }

        protected getConfirmedCartFailed(error: any): void {
            this.coreService.redirectToPath(this.cartUrl);
        }

        protected getOrderCompleted(orderHistory: OrderModel): void {
            this.order = orderHistory;
        }

        protected getCartPromotionsCompleted(promotionCollection: PromotionCollectionModel): void {
            this.promotions = promotionCollection.promotions;
        }

        protected getWarehouseCompleted(warehouseModel: Store.Warehouse.WebApi.V1.ApiModels.WarehouseModel) {
            this.warehouse = warehouseModel;
        }
    }

    angular
        .module("insite")
        .controller("StoreCheckoutOrderConfirmationController", StoreCheckoutOrderConfirmationController);
};