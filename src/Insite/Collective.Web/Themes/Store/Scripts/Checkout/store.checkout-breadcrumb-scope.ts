﻿module store.checkout {
    "use strict";

    export interface ICheckoutBreadcrumbScope extends ng.IScope {
        activeStep: string;
    }
}