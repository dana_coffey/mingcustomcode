﻿module store.feedback {
    import AccountModel = Insite.Account.WebApi.V1.ApiModels.AccountModel;
    "use strict";

    export class FeedbackController {
        submitted = false;
        $form: JQuery;
        fileName: string;
        subTopicItems;

        static $inject = ["$element", "$scope", "spinnerService", "accountService", "sessionService"];

        constructor(
            protected $element: ng.IRootElementService,
            protected $scope: ng.IScope,
            protected spinnerService: insite.core.ISpinnerService,
            protected accountService: insite.account.IAccountService,
            protected sessionService: insite.account.ISessionService) {
            this.init();
        }

        init(): void {
            this.$form = this.$element.find("form");
            angular.element("#feedbackFileUpload").data("_scope", this.$scope);

            this.sessionService.getIsAuthenticated().then(
                (isAuthenticated: boolean) => {
                    if (isAuthenticated) {
                        this.accountService.getAccount().then((account: AccountModel) => {
                            this.$element[0].children[0][6].value = account.firstName;
                            this.$element[0].children[0][7].value = account.lastName;
                            this.$element[0].children[0][8].value = account.email;
                        });
                    }
                });

            this.subTopicItems = this.$element[0].children[0][2];

            for (var i = 0; i < this.subTopicItems.length; i++) {
                var subTopic = this.subTopicItems.options[i].value.split(": ");
                if (this.subTopicItems.options[i].value != "") {
                    this.subTopicItems.options[i].innerHTML = subTopic[1];
                    this.subTopicItems.options[i].innerText = subTopic[1];
                    this.subTopicItems.options[i].hidden = true;
                }
            }

            if ($("#feedbackEmailAddr").val()) {
                $("#feedbackView").show();
            }
        }

        reset(): void {
            (this.$form[0] as any).reset();
            this.fileName = null;
            this.submitted = false;
            setTimeout(() => { this.$scope.$apply(); });
        }

        setFile(arg): void {
            this.fileName = arg.files.length > 0 ? arg.files[0].name : null;

            setTimeout(() => {
                this.$scope.$apply();
            });
        }

        removeFile(): void {
            this.fileName = null;
            angular.element("#feedbackFileUpload").val("");

            setTimeout(() => {
                this.$scope.$apply();
            });
        }

        topicChanged(e): void {
            var selected = e.Model;

            for (var i = 0; i < this.subTopicItems.length; i++) {
                var subTopic = this.subTopicItems.options[i].value.split(": ");

                if (subTopic[0] == selected) {
                    this.subTopicItems.options[i].hidden = false;
                } else {
                    if (this.subTopicItems.options[i].value != "")
                        this.subTopicItems.options[i].hidden = true;
                }
            }
        }

        submit(): boolean {
            if (!this.$form.valid()) {
                return false;
            }

            this.spinnerService.show('feedbackForm');
            var formData = new FormData(this.$form[0] as any);

            $.ajax({
                url: this.$form.attr("action"),
                type: "POST",
                data: formData,
                success: () => {
                    this.submitted = true;
                },
                error: () => {
                    this.submitted = false;
                },
                complete: () => {
                    this.spinnerService.hide('feedbackForm');
                    this.$scope.$apply();
                },
                cache: false,
                contentType: false,
                mimeType: 'multipart/form-data',
                processData: false
            });

            return false;
        }
    }

    angular
        .module("insite")
        .controller("FeedbackController", FeedbackController);
}