﻿declare module Store.Attributes.Models {
    export interface IStoreCheckoutControllerAttributes extends ng.IAttributes {
        cartUrl: string;
    }
}

declare module Store.Catalog.WebApi.V1.ApiModels {

    interface AvailabilityStatusModel {
        LocalQuantity: number;
        TotalQuantity: number;
        Status: number;
        Message: string;
        Warehouses: AvailabilityStatusWarehouseModel[];
        ShowWarehouses: boolean;
        LocalWarehouse: WarehouseModel;
        HasLocalWarehouse: boolean;
    }

    interface AvailabilityStatusWarehouseModel {
        CanPickup: boolean;
        CanShipFrom: boolean;
        Latitude: number;
        Longitude: number;
        Name: string;
        Quantity: number;
    }

    interface GroupModel {
        name: string;
        parts: PartModel[];
    }

    interface LocationModel {
        address1: string;
        address2: string;
        address3: string;
        address4: string;
        city: string;
        postalCode: string;
        state: Insite.Websites.WebApi.V1.ApiModels.StateModel;
        country: Insite.Websites.WebApi.V1.ApiModels.CountryModel;
        validation: Insite.Customers.Services.Dtos.CustomerValidationDto;
        latitude: number;
        longitude: number;
        warehouse: WarehouseModel;
    }

    interface OrderLocationModel {
        address1: string;
        address2: string;
        address3: string;
        address4: string;
        city: string;
        postalCode: string;
        stateId: string;
        countryId: string;
        state: string;
        country: string;
    }

    interface OrderSubAccountModel {
        companyName: string;
        address1: string;
        address2: string;
        address3: string;
        address4: string;
        city: string;
        postalCode: string;
        state: string;
        country: string;
    }

    interface PackageModel {
        number: string;
        groups: GroupModel[];
    }

    interface PartModel {
        name: string;
        number: string;
        quantity: number;
        hasHistory: boolean;
        url: string;
    }

    interface PartsListModel {
        packages: PackageModel[];
    }

    interface ProductCustomFieldModel {
        name: string;
        value: string;
    }

    interface ProductReplacementModel {
        erpNumber: string;
        productName: string;
        url: string;
    }

    interface ProductSupersedeModel {
        erpNumber: string;
        productName: string;
        url: string;
    }

    interface RelatedProductCollectionModel extends  Insite.Core.WebApi.BaseModel {
        products: Insite.Catalog.Services.Dtos.ProductDto[];
    }
}

declare module Store.Settings.Models {
    export interface ICollectiveSettingsModel {
        paymentGatewayUrl: string;
        showAvailabilityByWarehouse: boolean;
        usePostProcessorBatchSaving: boolean;
        showTermsAndConditions: boolean;
    }
}

declare module Store.Warehouse.WebApi.V1.ApiModels {

    interface WarehouseCollectionModel extends Insite.Core.WebApi.BaseModel {
        warehouses: WarehouseModel[];
    }

    interface WarehouseModel extends Insite.Core.WebApi.BaseModel {
        address1: string;
        address2: string;
        city: string;
        country: string;
        description: string;
        displayName: string;
        id: string;
        isDefault: boolean,
        name: string;
        phone: string;
        postalCode: string;
        state: string;
    }
}

declare module Store.Warranty.WebApi.V1.ApiModels {

    interface WarrantyData extends Insite.Core.WebApi.BaseModel {
        sections: WarrantySection[];
    }

    interface WarrantyDetail extends Insite.Core.WebApi.BaseModel {
        name: string;
        value: string;
    }

    interface WarrantyResult extends Insite.Core.WebApi.BaseModel {
        data: WarrantyData;
        modelNumbers: string[];
        type: number;
    }

    interface WarrantySection extends Insite.Core.WebApi.BaseModel {
        details: WarrantySectionDetails[];
        key: string;
        value: string;
    }

    interface WarrantySectionDetails extends Insite.Core.WebApi.BaseModel {
        detail: { [key: string]: WarrantyDetail; }
    }
}

declare module Store.Analytics.Models {

    interface AnalyticsTrackingEventModel {
        event: string;
        eventCategory: string;
        eventAction: string;
        eventLabel: string;
    }
}