﻿module Store.Catalog {
    import AnalyticsTrackingEventModel = Store.Analytics.Models.AnalyticsTrackingEventModel;
    import WarrantyData = Warranty.WebApi.V1.ApiModels.WarrantyData;
    import WarrantyResult = Warranty.WebApi.V1.ApiModels.WarrantyResult;
    "use strict";

    export class ProductWarrantyLookupController {
        searchQuery = "";
        selectedModel: string;
        noWarrantyResult: boolean;
        noWarrantyData: boolean;
        warrantyData: WarrantyData;
        warrantyModels: string[];
        currentSearchQuery = this.searchQuery;
        isApiError: boolean;
        searching: boolean;
        searchingData: boolean;

        static $inject = ["$scope", "coreService", "storeProductService", "storeAnalyticsService", "spinnerService"];

        constructor(
            protected $scope: ng.IScope,
            protected coreService: insite.core.ICoreService,
            protected storeProductService: store.catalog.IStoreProductService,
            protected storeAnalyticsService: store.analytics.IStoreAnalyticsService,
            protected spinnerService: insite.core.ISpinnerService) {
        }

        protected findWarranty() {
            if (this.searchQuery !== this.currentSearchQuery) {
                this.currentSearchQuery = this.searchQuery;
                if (this.currentSearchQuery !== '') {
                    this.searching = true;
                    this.storeProductService.getWarrantyLookupBySerialNumber(this.searchQuery).then(
                            (warrantyResult: WarrantyResult) => {
                                this.isApiError = false;
                                this.resetSearch();

                                switch (warrantyResult.type) {
                                case 0:
                                    this.handleWarrantyModels(warrantyResult);
                                    break;
                                case 1:
                                    this.handleWarrantyData(warrantyResult);
                                    break;
                                case 2:
                                default:
                                    this.noWarrantyResult = true;
                                }

                                this.trackEvent(this.currentSearchQuery);
                            },
                            () => {
                                this.isApiError = true;
                                this.currentSearchQuery = undefined;
                                this.resetSearch();
                            })
                        .finally(() => { this.searching = false; });
                } else {
                    this.isApiError = false;
                    this.resetSearch();
                }
            }
        }

        protected handleWarrantyModels(warrantyResult: WarrantyResult): void {
            this.warrantyModels = warrantyResult.modelNumbers;

            if (this.warrantyModels.length === 1) {
                this.selectedModel = this.warrantyModels[0];
                this.onModelChange();
            } else {
                this.selectedModel = undefined;
            }
        }

        protected handleWarrantyData(warrantyResult: WarrantyResult): void {
            this.warrantyData = warrantyResult.data;
        }

        protected onModelChange(): void {
            if (this.selectedModel) {
                this.getWarrantyLookupBySerialNumberAndModel(this.currentSearchQuery, this.selectedModel);
            }
        }

        protected getWarrantyLookupBySerialNumberAndModel(serialNumber: string, modelNumber: string) {
            this.searchingData = true;
            this.storeProductService.getWarrantyLookupBySerialNumberAndModelNumber(serialNumber, modelNumber.replace(/\./g, "-")).then(
                    (warrantyResult: WarrantyResult) => {
                        this.isApiError = false;
                        this.noWarrantyData = false;

                        if (warrantyResult.type === 1) {
                            this.handleWarrantyData(warrantyResult);
                        } else {
                            this.noWarrantyData = true;
                        }
                    },
                    () => {
                        this.isApiError = true;
                    })
                .finally(() => { this.searchingData = false; });
        }

        private resetSearch() {
            this.warrantyData = null;
            this.warrantyModels = null;
            this.selectedModel = undefined;
            this.noWarrantyResult = false;
            this.noWarrantyData = false;
        }

        trackEvent(currentSearchQuery: string) {
            const event: AnalyticsTrackingEventModel = {
                event: "gaEvent",
                eventCategory: "Search",
                eventAction: "WarrantyLookup",
                eventLabel: currentSearchQuery
            };

            this.storeAnalyticsService.pushTrackingEvent(event);
        }
    }

    angular
        .module("insite")
        .controller("ProductWarrantyLookupController", ProductWarrantyLookupController);
}