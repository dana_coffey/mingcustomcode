﻿module insite.catalog {
    "use strict";

    export class StoreProductDetailController extends ProductDetailController {
        customFields: Store.Catalog.WebApi.V1.ApiModels.ProductCustomFieldModel[];
        replacementFor: Store.Catalog.WebApi.V1.ApiModels.ProductReplacementModel[];
        supersededBy: Store.Catalog.WebApi.V1.ApiModels.ProductSupersedeModel[];
        partsList: Store.Catalog.WebApi.V1.ApiModels.PartsListModel;
        hasDescription = false;
        hasCustomFields = false;
        partsListLoaded = false;
        showInfoTab = false;
        showPartsTab = false;
        canShowDocuments = false;
        currentUrl: string;

        static $inject = ["$scope", "coreService", "cartService", "productService", "addToWishlistPopupService", "productSubscriptionPopupService", "settingsService", "$stateParams", "sessionService", "storeProductService", "spinnerService"];

        constructor(
            protected $scope: ng.IScope,
            protected coreService: core.ICoreService,
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected productSubscriptionPopupService: catalog.ProductSubscriptionPopupService,
            protected settingsService: core.ISettingsService,
            protected $stateParams: IContentPageStateParams,
            protected sessionService: account.ISessionService,
            protected storeProductService: store.catalog.IStoreProductService,
            protected spinnerService: core.ISpinnerService) {
            super($scope, coreService, cartService, productService, addToWishlistPopupService, productSubscriptionPopupService, settingsService, $stateParams, sessionService);
            this.storeInit();
        }

        storeInit(): void {
            this.$scope.$watch("vm.product", (newValue: Insite.Catalog.Services.Dtos.ProductDto) => {
                if (newValue != undefined && newValue.properties["customFields"] !== "" && newValue.properties["customFields"] !== "null") {
                    this.customFields = JSON.parse(newValue.properties["customFields"]);
                    this.hasCustomFields = this.customFields.length > 0;
                } else {
                    this.customFields = null;
                    this.hasCustomFields = false;
                }

                if (newValue != undefined && newValue.properties["replacementFor"] !== undefined) {
                    this.replacementFor = JSON.parse(newValue.properties["replacementFor"]);
                } else {
                    this.replacementFor = null;
                }

                if (newValue != undefined && newValue.properties["supersededBy"] !== undefined) {
                    this.supersededBy = JSON.parse(newValue.properties["supersededBy"]);
                } else {
                    this.supersededBy = null;
                }

                this.hasDescription = this.product != null && this.product.htmlContent != null && this.product.htmlContent.trim().length > 0;
                this.showInfoTab = this.replacementFor != null || this.hasDescription || (this.product != null && this.product.documents != null && this.product.documents.length > 0);
                this.showPartsTab = this.product.properties["showPartsTab"] === "True";
                this.sessionService.getIsAuthenticated().then((isAuthenticated: boolean) => { this.canShowDocuments = isAuthenticated; }, () => { this.canShowDocuments = false; });
                this.currentUrl = this.coreService.getCurrentPath();
            });
        }

        storeAddToCart(product: ProductDto, event: any): void {
            this.addingToCart = true;

            let sectionOptions: ConfigSectionOptionDto[] = null;
            if (this.configurationCompleted && product.configurationDto && product.configurationDto.sections) {
                sectionOptions = this.configurationSelection;
            }

            PubSub.publish("nwayo.button.load.start", event.currentTarget);
            this.cartService.addLineFromProduct(product, sectionOptions, this.productSubscription, true).then(
                (cartLine: CartLineModel) => { this.storeAddToCartCompleted(cartLine, event); },
                (error: any) => { this.storeAddToCartFailed(error, event); }
            );
        }

        protected getProductCompleted(productModel: ProductModel): void {
            this.product = productModel.product;
            this.product.qtyOrdered = this.product.multipleSaleQty || 1;

            if (this.product.isConfigured && this.product.configurationDto && this.product.configurationDto.sections) {
                this.initConfigurationSelection(this.product.configurationDto.sections);
            }

            if (this.product.styleTraits.length > 0) {
                this.initialStyledProducts = this.product.styledProducts.slice();
                this.styleTraitFiltered = this.product.styleTraits.slice();
                this.initialStyleTraits = this.product.styleTraits.slice();
                if (this.product.isStyleProductParent) {
                    this.parentProduct = angular.copy(this.product);
                }
                this.initStyleSelection(this.product.styleTraits);
            }

            this.getRealTimePrices();
        }

        getPartsList() {
            if (this.partsList === undefined) {
                this.spinnerService.show("parts-list");
                this.storeProductService.getPartsList(this.product.id.toString())
                    .then((partsList: Store.Catalog.WebApi.V1.ApiModels.PartsListModel) => {
                        this.partsList = partsList;
                    }).finally(() => {
                        this.spinnerService.hide("parts-list");
                        this.partsListLoaded = true;
                    });
            }
        }

        protected storeAddToCartCompleted(cartLine: CartLineModel, event: any): void {
            this.addingToCart = false;
            PubSub.publish("nwayo.button.load.done", event.currentTarget);
        }

        protected storeAddToCartFailed(error: any, event: any): void {
            this.addingToCart = false;
            PubSub.publish("nwayo.button.load.fail", event.currentTarget);
        }
    }

    angular
        .module("insite")
        .controller("ProductDetailController", StoreProductDetailController);
}