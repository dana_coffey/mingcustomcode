﻿module store.catalog {
    "use strict";

    export interface IStoreProductService {
        getPartsList(productId: string): ng.IPromise<Store.Catalog.WebApi.V1.ApiModels.PartsListModel>;
        getRelatedProducts(productId: string, type: string): ng.IPromise<Store.Catalog.WebApi.V1.ApiModels.RelatedProductCollectionModel>;
        getWarrantyLookupBySerialNumber(serialNumber: string): ng.IPromise<Store.Warranty.WebApi.V1.ApiModels.WarrantyResult>;
        getWarrantyLookupBySerialNumberAndModelNumber(serialNumber: string, modelNumber: string): ng.IPromise<Store.Warranty.WebApi.V1.ApiModels.WarrantyResult>;
    }

    export class StoreProductService implements IStoreProductService {
        productServiceUri = "/api/v1/products/";

        static $inject = ["$http", "httpWrapperService"];

        constructor(
            protected $http: ng.IHttpService,
            protected httpWrapperService: insite.core.HttpWrapperService) {
        }

        getPartsList(productId: string): ng.IPromise<Store.Catalog.WebApi.V1.ApiModels.PartsListModel> {
            const url = `${this.productServiceUri}${productId}/parts`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.get(url),
                () => {},
                () => {});
        }

        getRelatedProducts(productId: string, type: string): ng.IPromise<Store.Catalog.WebApi.V1.ApiModels.RelatedProductCollectionModel> {
            const url = `${this.productServiceUri}${productId}/related-product/${type}`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.get(url),
                () => { },
                () => { });
        }

        getWarrantyLookupBySerialNumber(serialNumber: string): ng.IPromise<Store.Warranty.WebApi.V1.ApiModels.WarrantyResult> {
            const url = `${this.productServiceUri}/warranty/${serialNumber}`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.get(url),
                () => { },
                () => { });
        }

        getWarrantyLookupBySerialNumberAndModelNumber(serialNumber: string, modelNumber: string): ng.IPromise<Store.Warranty.WebApi.V1.ApiModels.WarrantyResult> {
            const url = `${this.productServiceUri}/warranty/${serialNumber}/${modelNumber}`;

            return this.httpWrapperService.executeHttpRequest(
                this,
                this.$http.get(url),
                () => { },
                () => { });
        }
    }

    angular
        .module("insite")
        .service("storeProductService", StoreProductService);
}