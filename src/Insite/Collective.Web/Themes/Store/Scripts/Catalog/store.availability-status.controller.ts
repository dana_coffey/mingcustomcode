﻿module insite.catalog {
    "use strict";

    export class StoreAvailabilityStatusController {
        availabilityStatus: Store.Catalog.WebApi.V1.ApiModels.AvailabilityStatusModel;
        collectiveSettings: Store.Settings.Models.ICollectiveSettingsModel;
        product: Insite.Catalog.Services.Dtos.ProductDto;
        ready: boolean;
        showWarehouseAvailability: boolean;
        warehouseAvailabilityId: string;
        warehouseSorted: boolean;
        location: LocationModel;

        static $inject = ["$scope", "geolocationService", "$q", "settingsService", "spinnerService", "$timeout", "locationService", "warehouseService", "$rootScope"];

        constructor(protected $scope: ng.IScope,
            protected geolocationService: store.core.StoreGeolocationService,
            protected $q: ng.IQService,
            protected settingsService: core.ISettingsService,
            protected spinnerService: core.ISpinnerService,
            protected $timeout: ng.ITimeoutService,
            protected locationService: locations.ILocationService,
            protected warehouseService: store.warehouse.IStoreWarehouseService,
            protected $rootScope: ng.IRootScopeService) {

            const availabilityStatusDeferred = $q.defer();
            const settingsDeferred = $q.defer();

            this.$scope.$watch("vm.product", (newValue: Insite.Catalog.Services.Dtos.ProductDto) => {
                if (newValue != undefined && newValue.properties["availabilityStatus"] !== "" && newValue.properties["availabilityStatus"] !== "null") {
                    this.availabilityStatus = JSON.parse(newValue.properties["availabilityStatus"]);
                    this.warehouseAvailabilityId = `availability-detail-${newValue.id}`;
                    availabilityStatusDeferred.resolve();
                }
            });

            this.settingsService.getSettings().then((settingsCollection: core.SettingsCollection) => {
                this.collectiveSettings = (settingsCollection as any).collectiveSettingsSettings;
            }).finally(() => settingsDeferred.resolve());

            $q.all([availabilityStatusDeferred.promise, settingsDeferred.promise]).then(() => {
                this.showWarehouseAvailability = (this.collectiveSettings.showAvailabilityByWarehouse && this.availabilityStatus.Status !== 5);
                this.ready = this.availabilityStatus.Status != undefined;
            });

        }

        getWarehouseAvailabilities() {
            if (!this.warehouseSorted) {
                this.warehouseSorted = true;
                this.geolocationService.getCurrentPosition().then(
                    (position: Position) => {
                        this.spinnerService.show(this.warehouseAvailabilityId);
                        this.$timeout(() => { this.getCurrentLocationSuccess(position); }, 150);
                        this.$timeout(() => { this.spinnerService.hide(this.warehouseAvailabilityId); }, 300);
                    }
                );
            }
        }

        getCurrentLocationSuccess(position: Position) {
            this.availabilityStatus.Warehouses.sort((itemA, itemB) => {
                const distanceA = this.geolocationService.getDistanceFromLocationInKm(position.coords.latitude, position.coords.longitude, itemA.Latitude, itemA.Longitude);
                const distanceB = this.geolocationService.getDistanceFromLocationInKm(position.coords.latitude, position.coords.longitude, itemB.Latitude, itemB.Longitude);
                return distanceA > distanceB ? 1 : -1;
            });
        }

        selectWarehouse(warehouse: WarehouseModel) {
            this.locationService.get().then(
                (currentLocation: LocationModel) => {
                    this.availabilityStatus.LocalWarehouse = warehouse;
                    this.location = currentLocation;
                    this.location.warehouse = this.toCamel(warehouse);
                    this.locationService.update(this.location);
                    this.$rootScope.$broadcast("updateWarehouse", this.location.warehouse);
                },
                () => { });
        }

        toCamel(o): any {
            let newO;
            let newKey;
            let value;
            const self = this;

            if (o instanceof Array) {
                return o.map(value => {
                    if (typeof value === "object") {
                        value = this.toCamel(value);
                    }
                    return value;
                });
            } else {
                newO = {}
                let origKey;
                for (origKey in o) {
                    if (o.hasOwnProperty(origKey)) {
                        newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
                        value = o[origKey];
                        if (value instanceof Array || (value !== null && value.constructor === Object)) {
                            value = self.toCamel(value);
                        }
                        newO[newKey] = value;
                    }
                }
            }
            return newO;
        }
    }

    angular.module("insite")
        .controller("StoreAvailabilityStatusController", StoreAvailabilityStatusController);
}