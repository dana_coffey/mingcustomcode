﻿module insite.catalog {
    "use strict";

    export class StoreProductImagesController {
        product: any;
        selectedImage: ProductImageDto;
        showCarouselOnZoomModal: boolean;
        mainImageId = "main-product-image";
        zoomImageId = "zoom-product-image";
        zoomModalId = "product-image-zoom";

        static $inject = ["$scope", "$element"];

        constructor(protected $scope: ng.IScope, protected $element: JQuery) {
            this.init();
        }

        init(): void {
            if (this.product.productImages.length > 0) {
                this.selectedImage = this.product.productImages[0];
            } else {
                this.selectedImage = {
                    smallImagePath: this.product.smallImagePath,
                    mediumImagePath: this.product.mediumImagePath,
                    largeImagePath: this.product.largeImagePath,
                    altText: this.product.altText
                } as ProductImageDto;
            }

            angular.element(document).on("closed.zf.reveal", `#${this.zoomModalId}:visible`, () => { this.onImgZoomClose(); });
            angular.element(document).on("open.zf.reveal", `#${this.zoomModalId}`, () => { this.onImgZoomOpened(); });
        }

        protected onImgZoomClose(): void {
            this.$scope.$apply(() => {
                this.showCarouselOnZoomModal = false;
            });
        }

        protected onImgZoomOpened(): void {
            this.$scope.$apply(() => {
                this.showCarouselOnZoomModal = true;
            });
        }
    }

    angular
        .module("insite")
        .controller("ProductImagesController", StoreProductImagesController);
} 