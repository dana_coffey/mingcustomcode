﻿module Store.Catalog {
    "use strict";

    export interface IPartsListScope extends ng.IScope {
        partsList: Store.Catalog.WebApi.V1.ApiModels.PartsListModel;
        apiCalled: boolean;
    }
}