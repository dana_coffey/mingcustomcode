﻿module store.catalog {
    "use strict";

    export class StoreProductSliderController {
        product: ProductDto;
        relationType: string;
        title: string;
        maxTries: number;
        relatedProducts: ProductDto[];
        imagesLoaded: number;
        carousel: any;
        productSettings: ProductSettingsModel;
        addingToCart = true;
        $slider: any;
        minimumItemWidth = 305;

        static $inject = ["cartService", "productService", "storeProductService", "$timeout", "addToWishlistPopupService", "settingsService", "$scope", "$element"];

        constructor(
            protected cartService: insite.cart.ICartService,
            protected productService: insite.catalog.IProductService,
            protected storeProductService: IStoreProductService,
            protected $timeout: ng.ITimeoutService,
            protected addToWishlistPopupService: insite.wishlist.AddToWishlistPopupService,
            protected settingsService: insite.core.ISettingsService,
            protected $scope: ng.IScope,
            protected $element: JQuery) {
            this.init();
        }

        init(): void {
            this.settingsService.getSettings().then(
                (settingsCollection: insite.core.SettingsCollection) => { this.getSettingsCompleted(settingsCollection); });

            this.relatedProducts = [];
            this.imagesLoaded = 0;

            const cart = this.cartService.getLoadedCurrentCart();
            if (!cart) {
                this.$scope.$on("cartLoaded", () => {
                    this.addingToCart = false;
                });
            } else {
                this.addingToCart = false;
            }
        }

        addToCart(product: ProductDto, event: any): void {
            this.addingToCart = true;
            PubSub.publish("nwayo.button.load.start", event.currentTarget);

            this.cartService.addLineFromProduct(product, null, null, true)
                .then((cartLine: CartLineModel) => { this.addToCartCompleted(cartLine, event); },
                    (error: any) => { this.addToCartFailed(error, event); })
                .finally(() => { this.addingToCart = false; });
        }

        protected addToCartCompleted(cartLine: CartLineModel, event: any): void {
            PubSub.publish("nwayo.button.load.done", event.currentTarget);
        }

        protected addToCartFailed(error: any, event: any): void {
            PubSub.publish("nwayo.button.load.fail", event.currentTarget);
        }

        protected getSettingsCompleted(settingsCollection: insite.core.SettingsCollection): void {
            this.productSettings = settingsCollection.productSettings;
            this.waitForProduct(this.maxTries);
        }

        protected getRelatedProducts() {
            this.storeProductService.getRelatedProducts(this.product.id.toString(), this.relationType)
                .then((relatedProductCollection: Store.Catalog.WebApi.V1.ApiModels.RelatedProductCollectionModel) => { this.getRelatedProductsCompleted(relatedProductCollection); });
        }

        protected getRelatedProductsCompleted(relatedProductCollection: Store.Catalog.WebApi.V1.ApiModels.RelatedProductCollectionModel): void {
            if (relatedProductCollection) {
                this.relatedProducts = relatedProductCollection.products;
                this.imagesLoaded = 0;

                if (this.relatedProducts.length > 0) {
                    this.waitForDom(this.maxTries);
                }
            }
        }

        changeUnitOfMeasure(product: ProductDto): void {
            this.productService.changeUnitOfMeasure(product);
        }

        openWishListPopup(product: ProductDto): void {
            this.addToWishlistPopupService.display([product]);
        }

        showRelatedProductCarousel(): boolean {
            return !!this.relatedProducts && this.relatedProducts.length > 0;
        }

        showQuantityBreakPricing(product: ProductDto): boolean {
            return product.canShowPrice
                && product.pricing
                && !!product.pricing.unitRegularBreakPrices
                && product.pricing.unitRegularBreakPrices.length > 1
                && !product.quoteRequired;
        }

        showUnitOfMeasure(product: ProductDto): boolean {
            return product.canShowUnitOfMeasure
                && !!product.unitOfMeasureDisplay
                && !!product.productUnitOfMeasures
                && product.productUnitOfMeasures.length > 1
                && this.productSettings.alternateUnitsOfMeasure;
        }

        showUnitOfMeasureLabel(product: ProductDto): boolean {
            return product.canShowUnitOfMeasure
                && !!product.unitOfMeasureDisplay
                && !product.quoteRequired;
        }

        protected waitForProduct(tries: number): void {
            if (isNaN(+tries)) {
                tries = this.maxTries || 1000; // Max 20000ms
            }

            if (tries > 0) {
                this.$timeout(() => {
                    if (this.isProductLoaded()) {
                        this.getRelatedProducts();
                    } else {
                        this.waitForProduct(tries - 1);
                    }
                }, 20, false);
            }
        }

        protected waitForDom(tries: number): void {
            if (isNaN(+tries)) {
                tries = this.maxTries || 1000; // Max 20000ms
            }

            // If DOM isn't ready after max number of tries then stop
            if (tries > 0) {
                this.$timeout(() => {
                    if (this.isCarouselDomReadyAndImagesLoaded()) {
                        this.initializeCarousel();
                        this.$scope.$apply();
                    } else {
                        this.waitForDom(tries - 1);
                    }
                }, 20, false);
            }
        }

        protected isCarouselDomReadyAndImagesLoaded(): boolean {
            return this.$element.find('.slider').length > 0 && this.imagesLoaded >= this.relatedProducts.length;
        }

        protected isProductLoaded(): boolean {
            return this.product && typeof this.product === "object";
        }

        protected initializeCarousel(): void {
            this.$slider = this.$element.find('.slider');
            const nbItems = this.getItemsNumber();

            this.$slider.flexslider({
                animation: "slide",
                controlNav: true,
                animationLoop: true,
                slideshow: false,
                itemWidth: this.getItemSize(),
                minItems: nbItems,
                maxItems: nbItems,
                move: nbItems,
                customDirectionNav: this.$slider.find('.slider-nav'),
                start: (slider: any) => { this.onStart(slider); }
            });
        }

        protected onStart(slider: any): void {
            this.carousel = slider;
            this.toggleSliderNav();
            this.equalize();
            PubSub.subscribe('nwayo.resize', () => { this.resize(); });
        }

        protected getItemSize(): number {
            const nbItems = this.getItemsNumber();
            const width = this.$slider.innerWidth();
            return nbItems > 1 ? width / nbItems : width;
        }

        protected getItemsNumber(): number {
            return Math.floor(this.$slider.innerWidth() / this.minimumItemWidth);
        }

        protected resize(): void {
            let nbItems = this.getItemsNumber();
            let itemWidth = this.getItemSize();
            this.carousel.vars.itemWidth = itemWidth;
            this.carousel.vars.minItems = nbItems;
            this.carousel.vars.maxItems = nbItems;
            this.carousel.vars.move = nbItems;
            this.toggleSliderNav();
            this.carousel.setup();
            this.equalize();
        }

        protected equalize(): void {
            PubSub.publish('nwayo.equalize', { element: this.$slider, parameters: '.product-name, .product-price, .product-box', resize: false });
        }

        protected toggleSliderNav(): void {
            this.$slider.toggleClass('show-nav', this.relatedProducts.length >= this.getItemsNumber());
        }

    }

    angular
        .module("insite")
        .controller("StoreProductSliderController", StoreProductSliderController);
}