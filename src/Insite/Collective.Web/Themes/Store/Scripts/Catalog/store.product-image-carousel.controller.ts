﻿module store.catalog {
    "use strict";

    export class StoreProductImageCarouselController {
        maxTries: number;
        productImages: ProductImageDto[];
        selectedImage: ProductImageDto;
        imagesLoaded: number;
        carousel: any;
        $slider: any;
        minimumItemWidth = 40;

        static $inject = ["$timeout", "$scope", "$element"];

        constructor(
            protected $timeout: ng.ITimeoutService,
            protected $scope: ng.IScope,
            protected $element: JQuery) {
            this.init();
        }

        init(): void {
            this.imagesLoaded = 0;
            this.waitForDom(this.maxTries);
        }

        protected waitForDom(tries: number): void {
            if (isNaN(+tries)) {
                tries = this.maxTries || 1000; // Max 20000ms
            }

            // If DOM isn't ready after max number of tries then stop
            if (tries > 0) {
                this.$timeout(() => {
                    if (this.isCarouselDomReadyAndImagesLoaded()) {
                        this.initializeCarousel();
                        this.$scope.$apply();
                    } else {
                        this.waitForDom(tries - 1);
                    }
                }, 20, false);
            }
        }

        protected isCarouselDomReadyAndImagesLoaded(): boolean {
            return this.$element.find('.slider').length > 0 && this.productImages && this.imagesLoaded >= this.productImages.length;
        }

        protected initializeCarousel(): void {
            this.$slider = this.$element.find('.slider');
            const itemWidth = this.getItemWidth();
            const nbItems = this.getItemsNumber(itemWidth);
            this.$slider.width(nbItems * itemWidth);

            this.$slider.flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: true,
                slideshow: false,
                itemWidth: itemWidth,
                minItems: nbItems,
                maxItems: nbItems,
                move: nbItems,
                customDirectionNav: this.$slider.find('.slider-nav'),
                start: (slider: any) => { this.onStart(slider); }
            });
        }

        protected onStart(slider: any): void {
            this.carousel = slider;
            this.toggleSliderNav();
            PubSub.subscribe('nwayo.resize', () => { this.resize(); });
        }

        protected getItemWidth(): number {
            return this.$slider.find('.image').outerWidth();
        }

        protected getItemsNumber(itemWidth = this.getItemWidth()): number {
            const nbItems = Math.floor((this.$element.width() - (this.$slider.outerWidth() - this.$slider.width())) / itemWidth);
            return this.productImages.length < nbItems ? this.productImages.length : nbItems;
        }

        protected resize(): void {
            let itemWidth = this.getItemWidth();
            let nbItems = this.getItemsNumber(itemWidth);
            this.$slider.width(nbItems * itemWidth);

            this.carousel.vars.itemWidth = itemWidth;
            this.carousel.vars.minItems = nbItems;
            this.carousel.vars.maxItems = nbItems;
            this.carousel.vars.move = nbItems;
            this.toggleSliderNav(nbItems);
            this.carousel.setup();
        }

        protected toggleSliderNav(nbItems = this.getItemsNumber()): void {
            this.$slider.toggleClass('show-nav', this.productImages.length > nbItems);
        }

        selectImage(image: ProductImageDto): void {
            this.selectedImage = image;
        }
    }

    angular
        .module("insite")
        .controller("ProductImageCarouselController", StoreProductImageCarouselController);
}