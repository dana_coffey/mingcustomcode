﻿module Store.Catalog {
    "use strict";

    export class StorePartsListController {
        currentPackage: Store.Catalog.WebApi.V1.ApiModels.PackageModel;
        filteredPackage: Store.Catalog.WebApi.V1.ApiModels.PackageModel;
        query: string;

        static $inject = ["$scope", "spinnerService", "$timeout"];

        constructor(protected $scope: IPartsListScope, protected spinnerService: insite.core.ISpinnerService, protected $timeout: ng.ITimeoutService) {
            this.init();
        }

        init(): void {
            this.$scope.$watch("vm.partsList", (newValue: Store.Catalog.WebApi.V1.ApiModels.PartsListModel) => {
                if (newValue) {
                    if (this.currentPackage === undefined && newValue.packages.length > 0) {
                        this.currentPackage = newValue.packages[0];
                        this.filteredPackage = this.currentPackage;
                    }

                    this.$scope.partsList = newValue;
                } else {
                    this.$scope.partsList = undefined;
                }
            });
        }

        changePackage(): void {
            this.query = "";
            this.filteredPackage = this.currentPackage;
            this.spinnerService.show("parts-list-package");

            this.$timeout(() => {
                this.spinnerService.hide("parts-list-package");
            }, 300);
        }

        searchParts(): void {
            const _query = angular.lowercase(this.query.trim());
            this.filteredPackage = this.currentPackage;
            if (_query != "") {
                this.filteredPackage = $.extend({}, this.currentPackage, { groups: [] });
                this.currentPackage.groups.forEach((group, i) => {
                    // Filter parts
                    let filteredParts = $.grep(group.parts, p => this.partMatchQuery(p, _query));
                    // Remove duplicates
                    let uniqueParts = [];
                    filteredParts.forEach((filteredPart) => {
                        let isDuplicate = false;

                        uniqueParts.forEach((uniquePart, i) => {
                            if (uniquePart.number == filteredPart.number) {
                                uniquePart.quantity += filteredPart.quantity;
                                isDuplicate = true;
                            }
                        });

                        if (!isDuplicate) { uniqueParts.push($.extend({}, filteredPart)); }
                    });
                    // Set group
                    if (uniqueParts.length > 0) {
                        this.filteredPackage.groups.push($.extend({}, group, { parts: uniqueParts }));
                    }
                });
            } else {
                this.filteredPackage = this.currentPackage;
            }
        }

        protected partMatchQuery(part: Store.Catalog.WebApi.V1.ApiModels.PartModel, query: string): boolean {
            return angular.lowercase(part.number).indexOf(query || "") !== -1 || angular.lowercase($(`<p>${part.name}</p>`).text()).indexOf(query || "") !== -1;
        }
    }

    angular.module("insite")
        .controller("StorePartsListController", StorePartsListController);
}