﻿//import ProductAutocompleteItemModel = Insite.Catalog.WebApi.V1.ApiModels.ProductAutocompleteItemModel;

module insite.catalog {
    "use strict";

    /*export class AutocompleteTypes {
        static searchHistory = "searchhistory";
        static product = "product";
        static category = "category";
        static content = "content";
    }*/

    export class StoreProductSearchController {
        criteria: string;
        listenForData: boolean;
        products: ProductAutocompleteItemModel[] = [];
        autocomplete: any;
        autocompleteOptions: AutoCompleteOptions;
        autocompleteType: string;
        translations: Array<any>;
        preventActions: boolean;
        autocompleteEnabled: boolean;
        autocompleteCanceled: boolean;
        searchHistoryEnabled: boolean;
        searchData: Array<any> = [];
        autocompleteAllowClose: boolean;
        $input: any;

        static $inject = ["$element", "$filter", "coreService", "searchService", "settingsService", "$state", "queryString"];

        constructor(
            protected $element: ng.IRootElementService,
            protected $filter: ng.IFilterService,
            protected coreService: core.ICoreService,
            protected searchService: ISearchService,
            protected settingsService: core.ISettingsService,
            protected $state: angular.ui.IStateService,
            protected queryString: common.IQueryStringService) {
            this.init();
        }

        init(): void {
            this.autocompleteAllowClose = false;
            this.criteria = this.queryString.get("criteria");
            this.$input = this.$element.find("input.isc-searchAutoComplete");

            this.initializeAutocomplete();

            this.settingsService.getSettings().then(
                (settingsCollection: core.SettingsCollection) => { this.getSettingsCompleted(settingsCollection); },
                (error: any) => { this.getSettingsFailed(error); });
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            this.autocompleteEnabled = settingsCollection.searchSettings.autocompleteEnabled;
            this.searchHistoryEnabled = settingsCollection.searchSettings.searchHistoryEnabled;
        }

        protected getSettingsFailed(error: any): void {
        }

        initializeAutocomplete(): void {
            let appliedOnce = false;
            this.autocompleteOptions = {
                height: 600,
                filtering: (event: kendo.ui.AutoCompleteFilteringEvent) => {
                    this.onAutocompleteFiltering(event, appliedOnce);
                    appliedOnce = true;
                },
                dataTextField: "title",
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: (options: kendo.data.DataSourceTransportReadOptions) => { this.onAutocompleteRead(options); }
                    },
                    requestStart: () => { this.getAutocomplete().wrapper.addClass('loading'); },
                    requestEnd: () => { this.getAutocomplete().wrapper.removeClass('loading'); }
                },
                popup: {
                    position: "top right",
                    origin: "bottom right",
                    appendTo: this.$input.parent()
                },
                animation: (false as any),
                template: (suggestion: any) => { return this.getAutocompleteTemplate(suggestion); },
                select: (e: kendo.ui.AutoCompleteSelectEvent) => { this.onAutocompleteSelect(e); },
                dataBound: (e: kendo.ui.AutoCompleteDataBoundEvent) => { this.onAutocompleteDataBound(e); },
                close: (e: kendo.ui.AutoCompleteCloseEvent) => {
                    if (this.autocompleteAllowClose || !this.$input.is(':focus')) {
                        this.getAutocomplete().popup.element.removeClass("search-autocomplete-list--large");
                    } else {
                        e.preventDefault();
                    }
                }
            };

            this.$input.on('blur', () => { this.closeAutocomplete(); });
        }

        protected onAutocompleteFiltering(event: kendo.ui.AutoCompleteFilteringEvent, appliedOnce: boolean): void {
            if (!appliedOnce) {
                const list = this.getAutocomplete().list;
                list.addClass("search-autocomplete-list");

                list.prepend(this.$element.find(".search-history-label"));
                list.append(this.$element.find(".clear-search-history"));
            }

            this.disableSearch();

            if (this.autocompleteCanceled) {
                this.autocompleteCanceled = false;
                event.preventDefault();
                this.closeAutocomplete();
                return;
            }

            if (!event.filter.value) {
                this.autocompleteType = AutocompleteTypes.searchHistory;
            } else if (event.filter.value.length >= 3) {
                this.autocompleteType = AutocompleteTypes.product;
            } else {
                event.preventDefault();
                this.closeAutocomplete();
                return;
            }
        }

        protected onAutocompleteRead(options: kendo.data.DataSourceTransportReadOptions): void {
            let data = new Array();
            if (this.autocompleteType === AutocompleteTypes.searchHistory) {
                if (this.searchHistoryEnabled) {
                    data = this.searchService.getSearchHistory();
                    data.forEach((p: any) => p.type = "");
                }
                options.success(data);
            } else {
                if (this.autocompleteEnabled) {
                    this.searchService.autocompleteSearch(this.criteria).then(
                        (autocompleteModel: AutocompleteModel) => { this.autocompleteSearchCompleted(autocompleteModel, options, data); },
                        (error: any) => { this.autocompleteSearchFailed(error, options); });
                } else {
                    options.success(data);
                }
            }
        }

        protected autocompleteSearchCompleted(autocompleteModel: AutocompleteModel, options: kendo.data.DataSourceTransportReadOptions, data: Array<any>): void {
            this.products = autocompleteModel.products;
            this.products.forEach((p: any) => p.type = AutocompleteTypes.product);

            const categories = autocompleteModel.categories;
            categories.forEach((p: any) => p.type = AutocompleteTypes.category);

            const content = autocompleteModel.content;
            content.forEach((p: any) => p.type = AutocompleteTypes.content);

            this.searchData = data.concat(categories, content, this.products);
            options.success(this.searchData);
        }

        protected autocompleteSearchFailed(error: any, options: kendo.data.DataSourceTransportReadOptions): void {
            options.error(error);
        }

        protected onAutocompleteSelect(event: kendo.ui.AutoCompleteSelectEvent): boolean {
            this.enableSearch();

            const dataItem = this.getAutocomplete().dataItem(event.item.index(".k-item"));
            if (!dataItem) {
                event.preventDefault();
                return false;
            }

            if (this.autocompleteType === AutocompleteTypes.searchHistory) {
                this.search(dataItem.q, dataItem.includeSuggestions);
            } else {
                this.coreService.redirectToPath(dataItem.url);
            }

            setTimeout(() => {
                this.$input.val('').blur();
                $('#site-search').prop('checked', false).trigger('click');
            }, 0);
        }

        protected onAutocompleteDataBound(event: kendo.ui.AutoCompleteDataBoundEvent): void {
            this.getAutocomplete().list.toggleClass(`autocomplete-type-${AutocompleteTypes.searchHistory}`, this.autocompleteType === AutocompleteTypes.searchHistory);
            this.getAutocomplete().list.toggleClass(`autocomplete-type-${AutocompleteTypes.product}`, this.autocompleteType === AutocompleteTypes.product);

            if (this.autocompleteType === AutocompleteTypes.searchHistory) {
                return;
            }

            if (!this.searchData.length || !this.$input.is(':focus')) {
                this.closeAutocomplete();
                return;
            }

            const list = this.getAutocomplete().list;
            let groupKeys = this.searchData.map(item => { return item.type; });

            const leftColumn = $("<li>");
            const leftColumnContainer = $("<ul>");
            leftColumn.append(leftColumnContainer);

            const rightColumn = $("<li class='products'>");
            const rightColumnContainer = $("<ul>");
            rightColumn.append(rightColumnContainer);

            this.getAutocomplete().ul.append(leftColumn);
            this.getAutocomplete().ul.append(rightColumn);

            groupKeys = this.$filter("unique")(groupKeys);
            groupKeys.forEach(groupKey => {
                switch (groupKey) {
                    case AutocompleteTypes.category:
                    case AutocompleteTypes.content:
                        list.find(`.group-${groupKey}`).parent().each((index, item) => leftColumnContainer.append(item));
                        break;
                    case AutocompleteTypes.product:
                        list.find(`.group-${groupKey}`).parent().each((index, item) => rightColumnContainer.append(item));
                        break;
                }

                const translation = this.getTranslation(groupKey);
                if (translation) {
                    list.find(`.group-${groupKey}`).eq(0).closest("li").before(`<li class='type ${groupKey}'>${translation}</li>`);
                }
            });

            const leftColumnChildrenCount = leftColumnContainer.find("li").length;
            const rightColumnChildrenCount = rightColumnContainer.find("li").length;

            if (leftColumnChildrenCount === 0) { leftColumn.remove(); }
            if (rightColumnChildrenCount === 0) { rightColumn.remove(); }

            this.getAutocomplete().popup.element.toggleClass(`search-autocomplete-list--large`, leftColumnChildrenCount > 0 && rightColumnChildrenCount > 0);

            list.find(".type").on("click", () => {
                return false;
            });
        }

        protected getAutocompleteTemplate(suggestion: any): string {
            if (this.autocompleteType === AutocompleteTypes.searchHistory) {
                return this.getAutocompleteSearchHistoryTemplate(suggestion);
            }

            const pattern = `(${this.criteria.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")})`;

            switch (suggestion.type) {
                case AutocompleteTypes.category:
                    return this.getAutocompleteCategoryTemplate(suggestion, pattern);
                case AutocompleteTypes.content:
                    return this.getAutocompleteContentTemplate(suggestion, pattern);
                default:
                    return this.getAutocompleteProductTemplate(suggestion, pattern);
            }
        }

        protected getAutocompleteSearchHistoryTemplate(suggestion: any): string {
            return `<div class="group-${suggestion.type}">${suggestion.q}</div>`;
        }

        protected getAutocompleteCategoryTemplate(suggestion: any, pattern: string): string {
            const parent = suggestion.subtitle ? `<span class='parent'>in ${suggestion.subtitle}</span>` : "";
            const title = suggestion.title.replace(new RegExp(pattern, "gi"), "<strong>$1<\/strong>");
            return `<div class="group-${suggestion.type}"><span class="title">${title}</span>${parent}</div>`;
        }

        protected getAutocompleteContentTemplate(suggestion: any, pattern: string): string {
            return `<div class="group-${suggestion.type}">${suggestion.title}</div>`;
        }

        protected getAutocompleteProductTemplate(suggestion: any, pattern: string): string {
            const shortDescription = suggestion.title.replace(new RegExp(pattern, "gi"), "<strong>$1<\/strong>");

            let additionalInfo = "";

            if (suggestion.title) {
                let partNumberLabel: string;
                let partNumber: string;
                if (suggestion.isNameCustomerOverride) {
                    partNumberLabel = this.getTranslation("partNumber") || "";
                    partNumber = suggestion.name || "";
                } else {
                    partNumberLabel = this.getTranslation("customerPartNumber") || "";
                    partNumber = suggestion.erpNumber || "";
                }

                partNumber = partNumber.replace(new RegExp(pattern, "gi"), "<strong>$1<\/strong>");

                additionalInfo += `<span class='sku'><span class='label'>${partNumberLabel}</span><span class='value tst_autocomplete_product_${suggestion.id}_number'>${partNumber}</span></span>`;
            }

            if (suggestion.manufacturerItemNumber) {
                const manufacturerItemNumber = suggestion.manufacturerItemNumber.replace(new RegExp(pattern, "gi"), "<strong>$1<\/strong>");
                const manufacturerItemNumberLabel = this.getTranslation("manufacturerItemNumber") || "";
                additionalInfo += ` <span class='mfg-number'><span class='label'>${manufacturerItemNumberLabel}</span><span class='value'>${manufacturerItemNumber}</span></span>`;
            }

            return `<div class="group-${suggestion.type} tst_autocomplete_product_${suggestion.id}"><div class="visual"><div class="image"><img src='${suggestion.image}' /></div></div><div class="content"><div class='name'>${shortDescription}</div><div class="product-sku">${additionalInfo}</div></div></div>`;
        }

        onEnter(): void {
            if (this.getAutocomplete()._last === kendo.keys.ENTER && this.isSearchEnabled()) {
                this.search();
                $('#site-search').prop('checked', false).trigger('click');
            }
        }

        getAutocomplete(): any {
            if (!this.autocomplete) {
                this.autocomplete = this.$input.data("kendoAutoComplete");
            }

            return this.autocomplete;
        }

        clearSearchHistory(): void {
            this.searchService.clearSearchHistory();
            this.closeAutocomplete();
        }

        search(query?: string, includeSuggestions?: boolean): void {
            this.disableSearch();

            const searchTerm = this.getSearchTerm(query);

            if (this.isSearchTermEmpty(searchTerm)) {
                this.enableSearch();
                return;
            }

            // prevent filtering results so popup isnt visible when next page loads
            this.$input.trigger('blur');

            if (this.onlyOneProductInAutocomplete()) {
                this.navigateToFirstProductInAutocomplete();
                return;
            }

            this.redirectToSearchPage(searchTerm, includeSuggestions);
        }

        protected disableSearch(): void {
            this.preventActions = true;
        }

        protected enableSearch(): void {
            this.preventActions = false;
        }

        protected isSearchEnabled(): boolean {
            return this.preventActions;
        }

        private getSearchTerm(query?: string): string {
            return query || this.criteria.trim();
        }

        private isSearchTermEmpty(searchTerm: string): boolean {
            return !searchTerm;
        }

        private stopAutocomplete(): void {
            this.autocompleteCanceled = true;
        }

        private onlyOneProductInAutocomplete(): boolean {
            return this.products && this.products.length === 1;
        }

        private navigateToFirstProductInAutocomplete(): void {
            this.coreService.redirectToPath(this.products[0].url);
        }

        private closeAutocomplete(): void {
            this.autocompleteAllowClose = true;
            this.getAutocomplete().close();
            this.autocompleteAllowClose = false;
        }

        protected redirectToSearchPage(searchTerm: string, includeSuggestions?: boolean): void {
            // search states are needed for when the /search page is redirecting to itself
            if (insiteMicrositeUriPrefix) {
                const stateParams = {
                    microsite: insiteMicrositeUriPrefix.substring(1),
                    criteria: searchTerm,
                    includeSuggestions: includeSuggestions === false ? false : null
                };
                this.$state.go("search_microsite", stateParams);
            } else {
                this.$state.go("search", { criteria: searchTerm });
            }
        }

        getTranslation(key: string): string {
            const translationMatches = this.translations.filter(item => item.key === key);
            if (translationMatches.length > 0) {
                return translationMatches[0].text;
            }

            return null;
        }
    }

    angular
        .module("insite")
        .controller("ProductSearchController", StoreProductSearchController);
}

// Overriding kendo's autocomplete keydown event to allow support for our two column autocomplete
/*(kendo.ui.AutoComplete.prototype as any)._keydown = function (e) {
    let that = this;
    let keys = kendo.keys;
    let itemSelector = "li.k-item";
    let ul = $(that.ul[0]);
    let key = e.keyCode;
    let focusClass = "k-state-focused";
    let items = ul.find(itemSelector) as any;

    let currentIndex = -1;
    items.each((idx, i) => {
        if ($(i).hasClass(focusClass)) {
            currentIndex = idx;
        }
    });

    let current = currentIndex >= 0 && currentIndex < items.length ? $(items[currentIndex]) : null;
    let visible = that.popup.visible();

    that._last = key;

    if (key === keys.DOWN) {
        if (visible) {
            if (current) {
                current.removeClass(focusClass);
            }

            if (currentIndex < 0) {
                current = items.first();
                current.addClass(focusClass);
            } else if (currentIndex < (items.length - 1)) {
                current = $(items[currentIndex + 1]);
                current.addClass(focusClass);
            }
        }
        e.preventDefault();
        return false;
    } else if (key === keys.UP) {
        if (visible) {
            if (current) {
                current.removeClass(focusClass);
            }

            if (currentIndex < 0) {
                current = items.last();
                current.addClass(focusClass);
            } else if (currentIndex > 0) {
                current = $(items[currentIndex - 1]);
                current.addClass(focusClass);
            }
        }
        e.preventDefault();
        return false;
    } else if (key === keys.ENTER || key === keys.TAB) {
        if (key === keys.ENTER && visible) {
            e.preventDefault();
        }

        if (visible && current) {
            if (that.trigger("select", { item: current })) {
                return;
            }

            this._select(current);
        }

        this._blur();
    } else if (key === keys.ESC) {
        if (that.popup.visible()) {
            e.preventDefault();
        }
        that.close();
    } else {
        that._search();
    }
};*/