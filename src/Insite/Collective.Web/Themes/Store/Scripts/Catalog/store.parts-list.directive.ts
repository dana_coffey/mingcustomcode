﻿module store.catalog {
    "use strict";

    angular
        .module("insite")
        .directive("storePartsList", () => ({
            restrict: "E",
            replace: true,
            scope: {
                partsList: "=",
                apiCalled: "="
            },
            templateUrl: "/PartialViews/Catalog-PartsList",
            controller: "StorePartsListController",
            controllerAs: "vm",
            bindToController: true
        }));
}