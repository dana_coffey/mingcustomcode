﻿module insite.catalog {
    "use strict";

    export class StoreCrossSellCarouselController {
        product: ProductDto;
        productCrossSell: boolean;
        maxTries: number;
        crossSellProducts: ProductDto[];
        imagesLoaded: number;
        carousel: any;
        productSettings: ProductSettingsModel;
        failedToGetRealTimePrices = false;
        addingToCart = true;
        $slider: any;
        minimumItemWidth = 305; 

        static $inject = ["cartService", "productService", "$timeout", "addToWishlistPopupService", "settingsService", "$scope", "$element"];

        constructor(
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected $timeout: ng.ITimeoutService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected settingsService: core.ISettingsService,
            protected $scope: ng.IScope,
            protected $element: JQuery) {
            this.init();
        }

        init(): void {
            this.settingsService.getSettings().then(
                (settingsCollection: core.SettingsCollection) => { this.getSettingsCompleted(settingsCollection); },
                (error: any) => { this.getSettingsFailed(error); });

            this.crossSellProducts = [];
            this.imagesLoaded = 0;

            const cart = this.cartService.getLoadedCurrentCart();
            if (!cart) {
                this.$scope.$on("cartLoaded", () => {
                    this.addingToCart = false;
                });
            } else {
                this.addingToCart = false;
            }
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            this.productSettings = settingsCollection.productSettings;
            this.getCrossSells();
        }

        protected getSettingsFailed(error: any): void {
        }

        protected getCrossSells(): void {
            if (!this.productCrossSell) {
                this.productService.getCrossSells(null).then(
                    (crossSellCollection: CrossSellCollectionModel) => { this.getCrossSellsCompleted(crossSellCollection); },
                    (error: any) => { this.getCrossSellsFailed(error); });
            } else {
                this.waitForProduct(this.maxTries);
            }
        }

        protected getCrossSellsCompleted(crossSellCollection: CrossSellCollectionModel): void {
            this.crossSellProducts = crossSellCollection.products;
            this.imagesLoaded = 0;

            if (this.crossSellProducts.length > 0) {
                this.waitForDom(this.maxTries);

                if (this.productSettings.realTimePricing && this.crossSellProducts) {
                    this.productService.getProductRealTimePrices(this.crossSellProducts).then(
                        (realTimePricing: RealTimePricingModel) => this.getProductRealTimePricesCompleted(realTimePricing),
                        (error: any) => this.getProductRealTimePricesFailed(error));
                }
            }
        }

        protected getCrossSellsFailed(error: any) {
        }

        protected getProductRealTimePricesCompleted(realTimePricing: RealTimePricingModel): void {
        }

        protected getProductRealTimePricesFailed(error: any): void {
            this.failedToGetRealTimePrices = true;
        }

        addToCart(product: ProductDto): void {
            this.addingToCart = true;

            this.cartService.addLineFromProduct(product, null, null, true).then(
                (cartLine: CartLineModel) => { this.addToCartCompleted(cartLine); },
                (error: any) => { this.addToCartFailed(error); });
        }

        protected addToCartCompleted(cartLine: CartLineModel): void {
            this.addingToCart = false;
        }

        protected addToCartFailed(error: any): void {
            this.addingToCart = false;
        }

        changeUnitOfMeasure(product: ProductDto): void {
            this.productService.changeUnitOfMeasure(product).then(
                (productDto: ProductDto) => { this.changeUnitOfMeasureCompleted(productDto); },
                (error: any) => { this.changeUnitOfMeasureFailed(error); });
        }

        protected changeUnitOfMeasureCompleted(product: ProductDto): void {
        }

        protected changeUnitOfMeasureFailed(error: any): void {
        }

        openWishListPopup(product: ProductDto): void {
            this.addToWishlistPopupService.display([product]);
        }

        showCrossSellCarousel(): boolean {
            return !!this.crossSellProducts
                && this.crossSellProducts.length > 0
                && (!this.productCrossSell || !!this.productSettings);
        }

        showQuantityBreakPricing(product: ProductDto): boolean {
            return product.canShowPrice
                && product.pricing
                && !!product.pricing.unitRegularBreakPrices
                && product.pricing.unitRegularBreakPrices.length > 1
                && !product.quoteRequired;
        }

        showUnitOfMeasure(product: ProductDto): boolean {
            return product.canShowUnitOfMeasure
                && !!product.unitOfMeasureDisplay
                && !!product.productUnitOfMeasures
                && product.productUnitOfMeasures.length > 1
                && this.productSettings.alternateUnitsOfMeasure;
        }

        showUnitOfMeasureLabel(product: ProductDto): boolean {
            return product.canShowUnitOfMeasure
                && !!product.unitOfMeasureDisplay
                && !product.quoteRequired;
        }

        protected waitForProduct(tries: number): void {
            if (isNaN(+tries)) {
                tries = this.maxTries || 1000; // Max 20000ms
            }

            if (tries > 0) {
                this.$timeout(() => {
                    if (this.isProductLoaded()) {
                        this.crossSellProducts = this.product.crossSells;
                        this.imagesLoaded = 0;
                        this.$scope.$apply();
                        this.waitForDom(this.maxTries);
                    } else {
                        this.waitForProduct(tries - 1);
                    }
                }, 20, false);
            }
        }

        protected waitForDom(tries: number): void {
            if (isNaN(+tries)) {
                tries = this.maxTries || 1000; // Max 20000ms
            }

            // If DOM isn't ready after max number of tries then stop
            if (tries > 0) {
                this.$timeout(() => {
                    if (this.isCarouselDomReadyAndImagesLoaded()) {
                        this.initializeCarousel();
                        this.$scope.$apply();
                    } else {
                        this.waitForDom(tries - 1);
                    }
                }, 20, false);
            }
        }

        protected isCarouselDomReadyAndImagesLoaded(): boolean {
            return this.$element.find('.slider').length > 0 && this.imagesLoaded >= this.crossSellProducts.length;
        }

        protected isProductLoaded(): boolean {
            return this.product && typeof this.product === "object";
        }

        protected initializeCarousel(): void {
            this.$slider = this.$element.find('.slider');
            const nbItems = this.getItemsNumber();

            this.$slider.flexslider({
                animation: "slide",
                controlNav: true,
                animationLoop: true,
                slideshow: false,
                itemWidth: this.getItemSize(),
                minItems: nbItems,
                maxItems: nbItems,
                move: nbItems,
                customDirectionNav: this.$slider.find('.slider-nav'),
                start: (slider: any) => { this.onStart(slider); }
            });
        }

        protected onStart(slider: any): void {
            this.carousel = slider;
            this.toggleSliderNav();
            this.equalize();
            PubSub.subscribe('nwayo.resize', () => { this.resize(); });
        }

        protected getItemSize(): number {
            const nbItems = this.getItemsNumber();
            const width = this.$slider.innerWidth();
            return nbItems > 1 ? width / nbItems : width;
        }

        protected getItemsNumber(): number {
            return Math.floor(this.$slider.innerWidth() / this.minimumItemWidth);
        }

        protected resize(): void {
            let nbItems = this.getItemsNumber();
            let itemWidth = this.getItemSize();
            this.carousel.vars.itemWidth = itemWidth;
            this.carousel.vars.minItems = nbItems;
            this.carousel.vars.maxItems = nbItems;
            this.carousel.vars.move = nbItems;
            this.toggleSliderNav();
            this.carousel.setup();
            this.equalize();
        }

        protected equalize(): void {
            PubSub.publish('nwayo.equalize', { element: this.$slider, parameters: '.product-name, .product-price, .product-box', resize: false });
        }

        protected toggleSliderNav(): void {
            this.$slider.toggleClass('show-nav', this.crossSellProducts.length >= this.getItemsNumber()); 
        }
    }

    angular
        .module("insite")
        .controller("CrossSellCarouselController", StoreCrossSellCarouselController);
}