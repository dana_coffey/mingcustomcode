﻿module insite.catalog {
    "use strict";

    angular.module("insite")
        .directive("storeAvailabilityStatus", () => ({
            restrict: "E",
            replace: true,
            scope: {
                product: "=",
                isDetail: "="
            },
            templateUrl: "/PartialViews/Catalog-AvailabilityStatus",
            controller: "StoreAvailabilityStatusController",
            controllerAs: "vm",
            bindToController: true
        }));

}