﻿module store.catalog {
    "use strict";

    angular
        .module("insite")
        .directive("storeProductSlider", () => ({
            restrict: "E",
            replace: true,
            scope: {
                product: "=",
                relationType: "@",
                title: "@",
                maxTries: "@"
            },
            templateUrl: "/PartialViews/Catalog-ProductSlider",
            controller: "StoreProductSliderController",
            controllerAs: "vm",
            bindToController: true
        }));
}