﻿module insite.catalog {
    "use strict";

    export class StoreProductListController extends ProductListController {
        location: LocationModel;

        static $inject = ["$scope", "coreService", "cartService", "productService", "compareProductsService", "$rootScope", "$window", "$localStorage", "paginationService", "searchService", "spinnerService", "addToWishlistPopupService", "settingsService", "$stateParams", "queryString", "$location", "$timeout", "locationService"];

        constructor(
            protected $scope: ng.IScope,
            protected coreService: core.ICoreService,
            protected cartService: cart.ICartService,
            protected productService: IProductService,
            protected compareProductsService: ICompareProductsService,
            protected $rootScope: ng.IRootScopeService,
            protected $window: ng.IWindowService,
            protected $localStorage: common.IWindowStorage,
            protected paginationService: core.IPaginationService,
            protected searchService: ISearchService,
            protected spinnerService: core.ISpinnerService,
            protected addToWishlistPopupService: wishlist.AddToWishlistPopupService,
            protected settingsService: core.ISettingsService,
            protected $stateParams: IProductListStateParams,
            protected queryString: common.IQueryStringService,
            protected $location: ng.ILocationService,
            protected $timeout: ng.ITimeoutService,
            protected locationService: locations.ILocationService) {

            super($scope, coreService, cartService, productService, compareProductsService, $rootScope, $window, $localStorage, paginationService, searchService, spinnerService, addToWishlistPopupService, settingsService, $stateParams, queryString, $location);
        }

        protected showExceedCompareLimitPopup(): void {
            (angular.element("#AddToCompareExceedsSixProducts") as any).foundation("open");
        }

        storeAddToCart(product: ProductDto, event: any): void {
            this.addingToCart = true;

            PubSub.publish("nwayo.button.load.start", event.currentTarget);
            this.cartService.addLineFromProduct(product, null, null, true).then(
                (cartLine: CartLineModel) => { this.storeAddToCartCompleted(cartLine, event); },
                (error: any) => { this.storeAddToCartFailed(error, event); }
            );
        }

        protected resolvePage(): void {
            const path = this.$stateParams.path || window.location.pathname;

            this.$timeout(() => { this.spinnerService.show("mainLayout", true); });
            this.productService.getCatalogPage(path).then(
                (catalogPage: CatalogPageModel) => { this.getCatalogPageCompleted(catalogPage); },
                (error: any) => { this.getCatalogPageFailed(error); }
            ).finally(() => {
                if (this.isCategoryPage()) {
                    this.$timeout(() => { this.spinnerService.hide("mainLayout"); });
                }
            });
        }

        protected getCatalogPageCompleted(catalogPage: CatalogPageModel): void {
            if (catalogPage.productId) {
                return;
            }

            this.category = catalogPage.category;
            this.breadCrumbs = catalogPage.breadCrumbs;

            if (!this.isCategoryPage()) {
                this.getProductData({
                    categoryId: this.category.id,
                    pageSize: this.pageSize || (this.products.pagination ? this.products.pagination.pageSize : null),
                    sort: this.sort || this.$localStorage.get("productListSortType", ""),
                    page: this.page,
                    attributeValueIds: this.attributeValueIds,
                    priceFilters: this.priceFilterMinimums,
                    searchWithin: this.searchWithinTerms.join(" "),
                    includeSuggestions: this.includeSuggestions,
                    getAllAttributeFacets: false
                });
            }
        }

        protected getProductData(params: IProductCollectionParameters, expand?: string[]): void {
            this.$timeout(() => { this.spinnerService.show("mainLayout", true); });

            expand = expand != null ? expand : ["pricing", "attributes", "facets"];
            this.productService.getProducts(params, expand).then(
                (productCollection: ProductCollectionModel) => { this.getProductsCompleted(productCollection, params, expand); },
                (error: any) => { this.getProductsFailed(error); })
                .finally(() => { this.$timeout(() => { this.spinnerService.hide("mainLayout"); }); });
        }

        protected getProductsCompleted(productCollection: ProductCollectionModel, params: IProductCollectionParameters, expand?: string[]): void {
            if (productCollection.searchTermRedirectUrl) {
                // use replace to prevent back button from returning to this page
                if (productCollection.searchTermRedirectUrl.lastIndexOf("http", 0) === 0) {
                    this.$window.location.replace(productCollection.searchTermRedirectUrl);
                } else {
                    this.$location.replace();
                    this.coreService.redirectToPath(productCollection.searchTermRedirectUrl);
                }
                return;
            }

            // got product data
            if (productCollection.exactMatch) {
                this.searchService.addSearchHistory(this.query, this.searchHistoryLimit, this.includeSuggestions.toLowerCase() === "true");
                this.coreService.redirectToPath(`${productCollection.products[0].productDetailUrl}?criteria=${encodeURIComponent(params.query)}`);
                return;
            }

            this.loadProductFilter(productCollection, expand);

            this.products = productCollection;
            this.products.products.forEach(product => {
                product.qtyOrdered = product.multipleSaleQty || 1;
            });

            this.reloadCompareProducts();

            //// allow the page to show
            this.ready = true;
            this.noResults = productCollection.products.length === 0;

            if (this.includeSuggestions === "true") {
                if (productCollection.originalQuery) {
                    this.query = productCollection.correctedQuery || productCollection.originalQuery;
                    this.originalQuery = productCollection.originalQuery;
                    this.autoCorrectedQuery = productCollection.correctedQuery != null && productCollection.correctedQuery !== productCollection.originalQuery;
                } else {
                    this.autoCorrectedQuery = false;
                }
            }

            this.searchService.addSearchHistory(this.query, this.searchHistoryLimit, this.includeSuggestions.toLowerCase() === "true");

            if (this.settings.realTimePricing) {
                this.productService.getProductRealTimePrices(this.products.products).then(
                    (pricingResult: RealTimePricingModel) => this.getProductRealTimePricesCompleted(pricingResult),
                    (error: any) => this.getProductRealTimePricesFailed(error));
            }

            this.imagesLoaded = 0;
            if (this.view === "grid") {
                this.waitForDom();
            }
        }

        protected storeAddToCartCompleted(cartLine: CartLineModel, event: any): void {
            this.addingToCart = false;
            PubSub.publish("nwayo.button.load.done", event.currentTarget);
        }

        protected storeAddToCartFailed(error: any, event: any): void {
            this.addingToCart = false;
            PubSub.publish("nwayo.button.load.fail", event.currentTarget);
        }
    }

    angular
        .module("insite")
        .controller("ProductListController", StoreProductListController);
}