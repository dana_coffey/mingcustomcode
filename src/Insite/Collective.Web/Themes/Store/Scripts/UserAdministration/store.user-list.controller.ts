﻿module insite.useradministration {
    "use strict";

    export class StoreUserListController extends UserListController {
        availableRoles: any;

        static $inject = ["accountService", "paginationService", "coreService", "spinnerService"];

        constructor(
            protected accountService: account.IAccountService,
            protected paginationService: core.IPaginationService,
            protected coreService: core.ICoreService,
            protected spinnerService: core.ISpinnerService) {
            super(accountService, paginationService, coreService);
        }

        storeSearch(sort: string = "UserName", newSearch: boolean = false, storeHistory: boolean = true): void {
            this.sort = sort;

            if (newSearch) {
                this.pagination.page = 1;
            }

            if (storeHistory) {
                this.updateHistory();
            }

            this.spinnerService.show();
            this.accountService.expand = "administration";
            this.accountService.getAccounts(this.searchText, this.pagination, this.sort)
                .finally(() => { this.spinnerService.hide(); })
                .then((accountCollection: AccountCollectionModel) => { this.getAccountsCompleted(accountCollection); },
                      (error: any) => { this.getAccountsFailed(error); });
        }

        getUserRoleDisplay(userRole: string): string {
            if (this.availableRoles) {
                const roles = this.availableRoles.filter(p => p.id === userRole);

                if (roles.length >= 1) {
                    return roles[0].name;
                }

                return "";
            }
        }

        protected getAccountsCompleted(accountCollection: AccountCollectionModel): void {
            super.getAccountsCompleted(accountCollection);
            this.availableRoles = JSON.parse(accountCollection.properties["availableRoles"]);
        }
    }

    angular
        .module("insite")
        .controller("UserListController", StoreUserListController);
}