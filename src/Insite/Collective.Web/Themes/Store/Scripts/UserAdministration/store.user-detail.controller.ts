﻿module insite.useradministration {
    "use strict";

    export class StoreUserDetailController extends UserDetailController {
        session: SessionModel;
        availableRoles: any;
        currentRole: any;
        ready: Boolean;
        userActivated: Boolean;
        userDeactivated: Boolean;
        isDeactivated: Boolean;

        static $inject = ["$scope", "accountService", "coreService", "sessionService", "queryString", "spinnerService", "$rootScope", "$location"];

        constructor(
            protected $scope: IUserDetailControllerScope,
            protected accountService: account.IAccountService,
            protected coreService: core.ICoreService,
            protected sessionService: account.ISessionService,
            protected queryString: common.IQueryStringService,
            protected spinnerService: core.ISpinnerService,
            protected $rootScope: IAppRootScope,
            protected $location: ng.ILocationService) {
            super($scope, accountService, coreService, sessionService, queryString, spinnerService, $rootScope, $location);
            
            this.$scope.$on("sessionLoaded", (event, session) => {
                this.session = session;
            });
        }

        protected getAccount(): void {
            this.accountService.expand = "approvers,roles";
            this.accountService.getAccount(this.userId)
                .then(
                    (account: AccountModel) => { this.getAccountCompleted(account); },
                    (error: any) => { this.getAccountFailed(error); }
                ).finally(() => {
                    this.ready = true;
                });
        }

        activateUser() : void {
            this.changeUserActivationStatus(false);
        }

        createUser(): void {
            if (this.currentRole) {
                this.user.role = this.currentRole.id;
            }

            super.createUser();
        }

        deactivateUser(): void {
            this.changeUserActivationStatus(true);
        }

        updateUser(): void {
            if (this.currentRole) {
                this.user.role = this.currentRole.id;
            }

            this.isDeactivated = false;
            super.updateUser();
        }

        protected getAccountCompleted(account: AccountModel) {
            super.getAccountCompleted(account);
            this.availableRoles = JSON.parse(account.properties["availableRoles"]);
            const roles = this.availableRoles.filter(p => p.id === account.role);
            this.isDeactivated = account.properties["isDeactivated"] === "true";

            if (roles.length >= 1) {
                this.currentRole = roles[0];
            } else {
                this.currentRole = undefined;
            }
            
        }

        protected updateAccountCompleted(account: AccountModel, showSuccess = true): void {
            this.changesSaved = showSuccess;
            this.user.activationStatus = account.activationStatus;
            this.isDeactivated = account.properties["isDeactivated"] === "true";
        }

        private changeUserActivationStatus(isDeactivated: boolean): void {
            this.isDeactivated = isDeactivated;
            this.user.properties["isDeactivated"] = this.isDeactivated.toString();
            this.spinnerService.show();
            this.accountService.updateAccount(this.user, this.userId)
                .then(
                    (account: AccountModel) => {
                        this.userActivated = !isDeactivated;
                        this.userDeactivated = isDeactivated;
                        this.updateAccountCompleted(account, false);
                    },
                    (error: any) => { this.updateAccountFailed(error); }
                )
                .finally(() => { this.spinnerService.hide(); });
        }

        protected resetNotification(): void {
            super.resetNotification();
            this.userActivated = false;
            this.userDeactivated = false;
        }
    }

    angular
        .module("insite")
        .controller("UserDetailController", StoreUserDetailController);
}