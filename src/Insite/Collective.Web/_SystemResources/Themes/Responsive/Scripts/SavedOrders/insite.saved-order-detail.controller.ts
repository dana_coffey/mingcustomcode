﻿module insite.savedorders {
    "use strict";

    export class SavedOrderDetailController {
        cart: CartModel = null;
        canAddToCart = false;
        canAddAllToCart = false;
        showInventoryAvailability = false;
        validationMessage: string;
        listPageUri: string;

        static $inject = ["cartService", "coreService", "spinnerService", "settingsService", "queryString"];

        constructor(
            protected cartService: cart.ICartService,
            protected coreService: core.ICoreService,
            protected spinnerService: core.ISpinnerService,
            protected settingsService: core.ISettingsService,
            protected queryString: common.IQueryStringService) {
            this.init();
        }

        init(): void {
            this.settingsService.getSettings().then(
                (settingsCollection: core.SettingsCollection) => { this.getSettingsCompleted(settingsCollection); },
                (error: any) => { this.getSettingsFailed(error); });

            this.cartService.expand = "cartlines,costcodes";
            this.cartService.getSavedOrder(this.queryString.get("cartid")).then(
                (cart: CartModel) => { this.getCartCompleted(cart); },
                (error: any) => { this.getCartFailed(error); });
        }

        protected getSettingsCompleted(settingsCollection: core.SettingsCollection): void {
            this.showInventoryAvailability = settingsCollection.productSettings.showInventoryAvailability;
        }

        protected getSettingsFailed(error: any): void {
        }

        protected getCartCompleted(cart: CartModel): void {
            this.cartService.expand = "";
            this.cart = cart;
            this.canAddToCart = this.cart.cartLines.some(l => l.canAddToCart);
            this.canAddAllToCart = this.cart.cartLines.every(l => l.canAddToCart);
        }

        protected getCartFailed(error: any): void {
            this.cartService.expand = "";
            this.coreService.redirectToPath(this.listPageUri);
        }

        placeSavedOrder(cartUri: string): void {
            const availableLines = this.cart.cartLines.filter(l => l.canAddToCart);
            if (availableLines.length <= 0) {
                return;
            }

            this.spinnerService.show();
            this.cartService.addLineCollection(availableLines, true, false).then(
                (cartLineCollection: CartLineCollectionModel) => { this.addLineCollectionCompleted(cartLineCollection, cartUri); },
                (error: any) => { this.addLineCollectionFailed(error); });
        }

        protected addLineCollectionCompleted(cartLineCollection: CartLineCollectionModel, cartUri: string): void {
            const currentCart = this.cartService.getLoadedCurrentCart();
            if (!currentCart.notes) {
                currentCart.notes = this.cart.notes;
            }
            if (!currentCart.requestedDeliveryDate) {
                currentCart.requestedDeliveryDate = this.cart.requestedDeliveryDate;
            }
            if (!currentCart.poNumber) {
                currentCart.poNumber = this.cart.poNumber;
            }

            this.cartService.updateCart(currentCart).then(
                (cart: CartModel) => { this.placeSavedOrderCompleted(cart, cartUri); },
                (error: any) => { this.placeSavedOrderFailed(error); });
        }

        protected addLineCollectionFailed(error: any): void {
        }

        protected placeSavedOrderCompleted(cart: CartModel, cartUri: string): void {
            this.deleteSavedOrder(cartUri);
        }

        protected placeSavedOrderFailed(error: any): void {
        }

        deleteSavedOrder(redirectUri: string): void {
            this.cartService.removeCart(this.cart).then(
                (cart: CartModel) => { this.deleteSavedOrderCompleted(cart, redirectUri); },
                (error: any) => { this.deleteSavedOrderFailed(error); });
        }

        protected deleteSavedOrderCompleted(cart: CartModel, redirectUri: string): void {
            this.coreService.redirectToPath(redirectUri);
        }

        protected deleteSavedOrderFailed(error: any): void {
        }
    }

    angular
        .module("insite")
        .controller("SavedOrderDetailController", SavedOrderDetailController);
}