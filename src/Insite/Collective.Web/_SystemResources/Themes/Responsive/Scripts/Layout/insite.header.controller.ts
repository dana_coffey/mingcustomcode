﻿module insite.layout {
    "use strict";

    export class HeaderController {
        cart: CartModel;

        static $inject = ["$scope", "$timeout", "cartService"];

        constructor(
            protected $scope: ng.IScope,
            protected $timeout: ng.ITimeoutService,
            protected cartService: cart.ICartService) {
            this.init();
        }

        init(): void {
            this.$scope.$on("cartLoaded", (event, cart) => {
                this.onCartLoaded(cart);
            });

            // use a short timeout to wait for anything else on the page to call to load the cart
            this.$timeout(() => {
                if (!this.cartService.cartLoadCalled) {
                    this.getCart();
                }
            }, 20);
        }

        protected onCartLoaded(cart: CartModel): void {
            this.cart = cart;
        }

        protected getCart(): void {
            this.cartService.getCart().then(
                (cart: CartModel) => { this.getCartCompleted(cart); },
                (error: any) => { this.getCartFailed(error); });
        }

        protected getCartCompleted(cart: CartModel): void {
        }

        protected getCartFailed(error: any): void {
        }
    }

    angular
        .module("insite")
        .controller("HeaderController", HeaderController);
}