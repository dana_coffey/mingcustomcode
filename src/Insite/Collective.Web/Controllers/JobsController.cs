﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Collective.Web.Extension.Core.Models.Scheduler;
using Collective.Web.Extension.Core.Services;

namespace Collective.Web.Controllers
{
    public class JobsController : Controller
    {
        public ActionResult ExecuteJob(string name)
        {
            return ScheduleJobs(new List<SimpleJobModel>
            {
                new SimpleJobModel
                {
                    CancelQueueOnError = false,
                    CanScheduleNewJobDuringExecution = true,
                    IntegrationJobName = name
                }
            });
        }

        public ActionResult ProcessInRiverQueue()
        {
            return ScheduleJobs(new List<SimpleJobModel>
            {
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Product Field Refresh"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Attribute Refresh"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Category Refresh"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Product Refresh"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Resource Refresh"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = true,
                    CanScheduleNewJobDuringExecution = false,
                    IntegrationJobName = "Collective - Archive inRiver Queue"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = false,
                    CanScheduleNewJobDuringExecution = true,
                    IntegrationJobName = "Rebuild Search Index"
                },
                new SimpleJobModel
                {
                    CancelQueueOnError = false,
                    CanScheduleNewJobDuringExecution = true,
                    IntegrationJobName = "Rebuild Sitemap"
                }
            });
        }

        private ActionResult ScheduleJobs(List<SimpleJobModel> jobs)
        {
            return JobSchedulerService.ScheduleJobs(jobs) ? new HttpStatusCodeResult(HttpStatusCode.OK) : new HttpStatusCodeResult(HttpStatusCode.Conflict);
        }
    }
}
