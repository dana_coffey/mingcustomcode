﻿using System.Linq;
using System.Web.Mvc;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Product;

namespace Collective.Web.Controllers
{
    public class ScanBarcodeController : Controller
    {
        public ActionResult ScanBarcode(string barcode)
        {
            var productService = CollectiveInjector.GetInstance<ICollectiveProductService>();
            var product = productService.GetProductsByBarcode(barcode).FirstOrDefault() ?? new ProductScanBarcodeModel { Url = Extension.Core.Helpers.UrlHelper.GetSearchUrl(barcode) };

            return Json(product, JsonRequestBehavior.AllowGet);
        }
    }
}
