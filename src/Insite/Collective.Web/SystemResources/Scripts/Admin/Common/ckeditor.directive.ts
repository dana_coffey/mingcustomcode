﻿module insite_admin {
    "use strict";

    angular
        .module("insite-admin")
        .directive("ckEditor", [
            () => {
                return {
                    require: "?ngModel",
                    restrict: "A",
                    link: (scope, elm, attr, model) => {
                        const ck = CKEDITOR.replace(elm[0], <any>{
                            autoParagraph: false,
                            allowedContent: true,
                            fillEmptyBlocks: false,
                            extraPlugins: 'imagemaps',
                            fullPage: false,
                            contentsCss: '/nwayo-build/store/styles/editor.css',
                            toolbar: [
                                ['SwitchBar', '-', 'Source'],
                                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll'],
                                '/',
                                ['Bold', 'Italic', '-', 'Subscript', 'Superscript'],
                                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                ['Link', 'Unlink', 'Anchor'],
                                ['Image', 'Table', 'HorizontalRule'],
                                '/',
                                ['RemoveFormat', 'Styles', 'Format', 'ShowBlocks']
                            ],
                            stylesSet: [
                                { name: 'Button #1', element: 'a', attributes: { 'class': 'button' } },
                                { name: 'Button #2', element: 'a', attributes: { 'class': 'button alt' } },
                                { name: 'Button #3', element: 'a', attributes: { 'class': 'button other' } },
                                { name: 'List Title', element: 'p', attributes: { 'class': 'list-title' } },
                                { name: 'Small text', element: ['p', 'span'], attributes: { 'class': 'small-text' } },
                                { name: 'Large text', element: ['p', 'span'], attributes: { 'class': 'large-text' } },
                                { name: 'Table', element: 'table', attributes: { 'class': 'table' } },
                                { name: 'Table Narrow', element: 'table', attributes: { 'class': 'table-narrow' } }
                            ],
                            format_tags: 'p;h2;h3;h4;h5;h6'
                        });

                        (<any>CKFinder).setupCKEditor(ck, "/SystemResources/Scripts/Libraries/ckfinder/3.4.1/", "UserFiles");

                        const onContentChange = () => {
                            model.$setViewValue(ck.getData());
                        };

                        ck.on("mode", () => {
                            if (ck.mode === "source") {
                                const editable = ck.editable();
                                editable.attachListener(editable, "input", () => {
                                    onContentChange();
                                });
                            }
                        });

                        ck.on("change", () => {
                            onContentChange();
                        });

                        model.$render = () => {
                            ck.setData(model.$modelValue);
                        };
                    }
                };
            }]);
}