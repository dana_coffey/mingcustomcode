﻿declare module CKFinder {
    let config: config;

    function popup(options: any): void;
    /* tslint:disable */
    interface config {
        /* tslint:enable */
        startupPath: string;
        resourceType: string;
    }
}