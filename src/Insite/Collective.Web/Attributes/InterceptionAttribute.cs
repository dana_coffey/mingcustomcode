﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.OData;

namespace Collective.Web.Attributes
{
    public class InterceptionAttribute : ActionFilterAttribute
    {
        private const string Parameters = "parameters";
        private const string WebsiteId = "websiteId";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionDescriptor.ControllerDescriptor.ControllerType == typeof(Insite.Admin.Controllers.OData.SystemSettingsController)
                && actionContext.ActionDescriptor.ActionName == nameof(Insite.Admin.Controllers.OData.SystemSettingsController.SetSettings))
            {
                if (actionContext.ActionArguments.ContainsKey(Parameters))
                {
                    var parameters = (ODataActionParameters)actionContext.ActionArguments[Parameters];

                    if (parameters.ContainsKey(WebsiteId))
                    {
                        // Workaround to fix a problem with Insite that was saving automatically
                        // with a websiteId instead of the global settings
                        parameters[WebsiteId] = null;
                    }
                }
            }

            base.OnActionExecuting(actionContext);
        }
    }
}