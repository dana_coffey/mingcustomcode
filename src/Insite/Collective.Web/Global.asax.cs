﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Insite Software">
//   Copyright © 2017. Insite Software. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Web;
using System.Web.Mvc;

namespace Collective.Web
{
#pragma warning disable SA1649 // File name must match first type name
    public class MvcApplication : Insite.WebFramework.Mvc.MvcApplication
#pragma warning restore SA1649 // File name must match first type name
    {
        protected override void Application_Start()
        {
            base.Application_Start();
            MvcHandler.DisableMvcResponseHeader = true;
        }

        protected override void Application_BeginRequest()
        {
            base.Application_BeginRequest();

            // Removes the response header "x-servertime"
            HttpContext.Current.Items["RequestTimer"] = null;
        }
    }
}
