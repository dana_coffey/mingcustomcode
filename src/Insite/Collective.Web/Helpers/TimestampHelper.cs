﻿using System.IO;
using System.Web;
using System.Web.Hosting;

namespace Collective.Web.Helpers
{
    public static class TimestampHelper
    {
        public static string GetTimestampedUrl(string virtualPath)
        {
            var realPath = HostingEnvironment.MapPath(virtualPath);
            if (realPath != null)
            {
                var file = new FileInfo(realPath);
                return $"{VirtualPathUtility.ToAbsolute(virtualPath)}?{file.LastWriteTime.ToFileTime()}";
            }

            return virtualPath;
        }
    }
}
