﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultDebugModeProvider.cs" company="Insite Software">
//   Copyright © 2017. Insite Software. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Insite.Core.Providers;

namespace Collective.Web.Helpers
{
    public class DefaultDebugModeProvider : IDebugModeProvider
    {
        public bool IsDebugEnabled => this.IsPreprocessorDebugEnabled;

        private bool IsPreprocessorDebugEnabled { get; }

        public DefaultDebugModeProvider()
        {
#if DEBUG
            this.IsPreprocessorDebugEnabled = true;
#endif
        }
    }
}
