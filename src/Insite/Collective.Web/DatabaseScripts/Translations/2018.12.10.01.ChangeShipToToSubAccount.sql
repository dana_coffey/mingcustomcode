﻿exec Translate 'Store_Checkout_IncompleteBillTo', 'Your billing address is incomplete.'
exec Translate 'Store_Checkout_IncompleteShipTo', 'Your sub account is incomplete.'
exec Translate 'Store_Checkout_Popup_IncompleteShipTo', 'Your sub account is incomplete.'
exec Translate 'Store_Checkout_ContactManager', 'Please contact your sell manager to complete required informations.'