﻿exec Translate 'Store_Checkout_Step_Edit', 'Billing & Shipping'
exec Translate 'Store_Checkout_Step_Review', 'Order Review'
exec Translate 'Store_Checkout_Step_Confirmation', 'Confirmation'
exec Translate 'Store_Checkout_Edit_ShippingMethodLabel', 'Shipping / Pickup*'
exec Translate 'Store_Checkout_Edit_ShippingMethodPlaceholder', 'Choose a shipping method'
exec Translate 'Store_Checkout_Edit_ShippingMethodNote', '*The shipping method will affect the total price of your order'
exec Translate 'Store_Checkout_Edit_ShippingLabel', 'Ship from*'
exec Translate 'Store_Checkout_Edit_ShippingPlaceholder', 'Select a warehouse'
exec Translate 'Store_Checkout_Edit_ShippingNote', '*Changing warehouse might affect the items availability & pricing'
exec Translate 'Store_Checkout_Edit_PickupLabel', 'Pickup location*'
exec Translate 'Store_Checkout_Edit_PickupPlaceholder', 'Select pickup location'
exec Translate 'Store_Checkout_Edit_PickupNote', '*Changing pickup location might affect the items availability & pricing'
exec Translate 'Store_Checkout_CartDetails', 'Order details'
exec Translate 'Store_Checkout_Edit_Misc', 'Additionnal informations'
exec Translate 'Store_Checkout_EditCart', 'Edit cart'
exec Translate 'Store_Order_OrderTotal', 'Order total'