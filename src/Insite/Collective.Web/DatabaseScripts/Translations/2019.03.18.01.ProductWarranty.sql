﻿exec Translate 'Store_productWarrantyUrl', '/product-warranty-lookup'
exec Translate 'Store_productWarranty', 'Warranty'
exec Translate 'Store_ProductWarranty_SearchLabel', 'Search by serial number'
exec Translate 'Store_productWarranty_Error', 'An error has occured. Please try again later.'
exec Translate 'Store_productWarranty_SearchPlaceholder', 'Enter a product serial #'
exec Translate 'Store_productWarranty_SearchMinLengthError', 'A minimum of 3 characters is required'
exec Translate 'Store_productWarranty_SearchNoResults', 'We could not find any serial number matching your search criterias'
exec Translate 'Store_productWarranty_NoResults', 'We could not find any warranty for the selected product'
exec Translate 'Store_productWarranty_ModelLabel', 'Show warranty for'
exec Translate 'Store_productWarranty_ModelPlaceholder', 'Select a model'
exec Translate 'Store_ProductsWarranty_Error', 'An error has occured. Please try again later.'
exec Translate 'Store_ProductWarranty_SearchNoResults', 'We could not find the warranty information related to this serial number.'
exec Translate 'Store_ProductWarranty_DataUnavailable', 'The warranty information for the selected model is currently unavailable.'

exec Translate 'Warranty_productName', 'Name'
exec Translate 'Warranty_modelNumber', 'Model #'
exec Translate 'Warranty_discreteModelNumber', 'Discrete Model #'
exec Translate 'Warranty_serialNumber', 'Serial # '
exec Translate 'Warranty_owner', 'Owner'
exec Translate 'Warranty_dateInstalled', 'Date Installed'
exec Translate 'Warranty_dateTransferred', 'Date Transfered'
exec Translate 'Warranty_transferDate', 'Transfer Date'
exec Translate 'Warranty_warrantyPolicyCode', 'Warranty Policy Code'
exec Translate 'Warranty_warrantyPolicyDescription', 'Warranty Policy Description'
exec Translate 'Warranty_standardLaborWarrantyExpirationDate', 'Standard Labor Warranty Expiration Date'
exec Translate 'Warranty_standardPartWarrantyExpirationDate', 'Standard Part Warranty Expiration Date	'
exec Translate 'Warranty_markedAs', 'Marked As'
exec Translate 'Warranty_shippedDate', 'Shipped Date'
exec Translate 'Warranty_registeredOnTime', 'Regitered on Time'
exec Translate 'Warranty_replacementOfModel', 'Replacement Of Model #'
exec Translate 'Warranty_replacementOfSerialNumber', 'Replacement Of Serial #'
exec Translate 'Warranty_brand', 'Brand'
exec Translate 'Warranty_enhancedWarranty', 'Enhanced Warranty'
exec Translate 'Warranty_equipmentInstallationAddress', 'Equipment Installation Address'
exec Translate 'Warranty_applicationType', 'Application Type'
exec Translate 'Warranty_componentCode', 'Component Code'
exec Translate 'Warranty_warrantyLength', 'Warranty Length'
exec Translate 'Warranty_installedAfter', 'Installed After'
exec Translate 'Warranty_warrantyStart', 'Warranty Start'
exec Translate 'Warranty_warrantyStop', 'Warranty Stop'
exec Translate 'Warranty_Entitlement Overview', 'Entitlement Overview'
exec Translate 'Warranty_Warranty Info (Original)', 'Warranty Info (Original)'
exec Translate 'Warranty_Warranty Info (Subsequent)', 'Warranty Info (Subsequent)'
exec Translate 'Warranty_Entitlement Warranty Info (ALL)', 'Warranty Info (ALL)'
exec Translate 'Warranty_Entitlement Warranty Info (Original)', 'Warranty Info (Original)'
exec Translate 'Warranty_Entitlement Warranty Info (Subsequent)', 'Warranty Info (Subsequent)'
exec Translate 'Warranty_Entitlement Warranty Info', 'Entitlement Warranty Info'
exec Translate 'Warranty_Entitlement Overview', 'Entitlement Policy Statement'
exec Translate 'Warranty_UTC Property Id', 'UTC Property Id'


exec Translate 'Warranty_claim', 'Service History'
exec Translate 'Warranty_claimNumber', 'Claim Number'
exec Translate 'Warranty_serviceExplanation', 'Claim Explanation'
exec Translate 'Warranty_serviceDate', 'Claim Date'
exec Translate 'Warranty_partNumber', 'Part Code'
exec Translate 'Warranty_partName', 'Description'
exec Translate 'Warranty_partQuantity', 'Quantity'

exec Translate 'Warranty_serviceInfo', 'Service Information'

exec Translate 'Warranty_replacedBySerialNumber', 'Replaced By Serial #'
exec Translate 'Warranty_replacedByModel', 'Replaced By Model #'

exec Translate 'Warranty_serviceInfo', 'Service Information'
exec Translate 'Warranty_thirdPartyContractNumber', 'Third Party Contract Number'
exec Translate 'Warranty_planNumber', 'Plan Number'
exec Translate 'Warranty_planType', 'Plan Type'
exec Translate 'Warranty_planName', 'Plan Name'
exec Translate 'Warranty_coverageStart', 'Coverage Start'
exec Translate 'Warranty_coverageEnd', 'Coverage End'
exec Translate 'Warranty_serviceProviderName', 'Service Provider Name'
exec Translate 'Warranty_serviceProviderId', 'Service Provider ID'
exec Translate 'Warranty_contractNumber', 'Contract Number'
exec Translate 'Warranty_cancelReason', 'Cancel Reason'
exec Translate 'Warranty_address', 'Address'
exec Translate 'Warranty_phone', 'Phone'
exec Translate 'Warranty_dateOfPurchase', 'Date of Purchase'
exec Translate 'Warranty_productInstallDate', 'Product Install Date'
exec Translate 'Warranty_modelName', 'Model Name'
exec Translate 'Warranty_Service Contract Info', 'Service Contract Info'
exec Translate 'Warranty_UTCPropertyId', 'UTC Property Id'
