﻿exec Translate 'Store_ProductDetail_ReplacementFor', 'Replacement for'
exec Translate 'Store_ProductDetail_ReplacementFor_Warning_Tab', 'Replacement Part List'
exec Translate 'Store_ProductDetail_ReplacementFor_Warning_Title', 'This product is a replacement.'
exec Translate 'Store_ProductDetail_ReplacementFor_Warning_Note', 'For more detail, verify the parts list on product info section.'