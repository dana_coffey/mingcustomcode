﻿exec Translate 'Store_Checkout_OrderReview_ValidationFailed', 'There is a problem with the setup of your account which prevents us from completing this order. Please contact our customer support.'
exec Translate 'Store_Checkout_OrderReview_AddressError', 'Your billing & shipping information is incomplete. Go back to the previous step to edit your information.'
