﻿exec Translate 'Store_Checkout_ShipDate', 'Requested Ship Date'
exec Translate 'Store_Checkout_RequestedDeliverDateInformation', 'This date is only a request and may not be fulfilled.'
exec Translate 'Store_Checkout_RequestedDeliverDate_Required', 'The requestted ship date field is mandatory.'