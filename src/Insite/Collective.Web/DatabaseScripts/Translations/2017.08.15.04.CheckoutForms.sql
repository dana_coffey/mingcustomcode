﻿exec Translate 'Store_Checkout_OrderReview_ValidationFailed', 'Some information in your order is incomplete. Go back to the order details to edit your current order.'
exec Translate 'Store_Checkout_SubmitOrder', 'Submit Order'
exec Translate 'Store_Checkout_PickupAddress', 'Pickup Address'