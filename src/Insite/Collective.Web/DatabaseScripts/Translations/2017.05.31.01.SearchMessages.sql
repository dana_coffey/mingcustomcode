﻿exec Translate 'Store_NoResults', 'We''re sorry, there were no products found.'
exec Translate 'Store_NoSearchResults', 'We''re sorry, your search returned no results.'
exec Translate 'Store_NoFilterResults', 'We''re sorry, your filters returned no results.'