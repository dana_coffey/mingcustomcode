﻿exec Translate 'Store_ProductAvailability_LeftInWarehouse', 'left in'
exec Translate 'Store_ProductAvailability_CheckAvailability', 'Check other stores for availability'
exec Translate 'Store_ProductAvailability_OutOfStockMessage', 'Out of stock item - Call store for lead time'