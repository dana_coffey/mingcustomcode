﻿declare @englishLanguageId uniqueidentifier = 'A26095EF-C714-E311-BA31-D43D7E4E88B2';
declare @fieldType nvarchar(max) = 'Contextual'
declare @fieldName nvarchar(max) = 'RoleInformation'
declare @pageContentItemClass nvarchar(max) = 'UserSetupPage'
declare @pageContentItemName nvarchar(max) = 'User Setup'
declare @widgetContentItemClass nvarchar(max) = 'UserSetupView'

exec TranslateWidgetContentItemField @pageContentItemClass = @pageContentItemClass,
    @pageContentItemName = @pageContentItemName,
    @widgetContentItemClass = @widgetContentItemClass,
    @languageId = @englishLanguageId,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @stringValue = '<h2>User Roles</h2><div><p><h3>Administrator:</h3><ul><li>Full access to the account.</li></ul></p></div><div><p><h3>Buyer:</h3><ul><li>Can order without approval.</li><li>Can see orders and invoices.</li><li>Can access to cart and wishlists.</li><li>Can see product documentations.</li></ul></p></div><div><p><h3>Technician:</h3><ul><li>Can <strong>not</strong> order products.</li><li>Can access to cart and wishlists.</li><li>Can see product documentations.</li></ul></p></div>'