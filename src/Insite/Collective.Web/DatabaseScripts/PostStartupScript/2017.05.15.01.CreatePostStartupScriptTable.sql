﻿CREATE TABLE [dbo].[PostStartupScript](
    [Id] [uniqueidentifier] NOT NULL,
    [ScriptName] [nvarchar](255) NOT NULL,
    [AppliedOn] [datetimeoffset](7) NULL,
    [CreatedOn] [datetimeoffset](7) NULL,
    [CreatedBy] [nvarchar](100) NOT NULL,
    [ModifiedOn] [datetimeoffset](7) NULL,
    [ModifiedBy] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_PostStartupScript] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_Table_1_Id1]  DEFAULT (newsequentialid()) FOR [Id]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_ScriptName]  DEFAULT ('') FOR [ScriptName]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_AppliedOn]  DEFAULT (getutcdate()) FOR [AppliedOn]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[PostStartupScript] ADD  CONSTRAINT [DF_PostStartupScript_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO