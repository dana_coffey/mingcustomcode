﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @isPickupPropertyConfigurationId uniqueidentifier = '390E32DA-7F76-458B-96B2-7425DF00C717';
declare @isPickupPropertyAttributeConfiguration uniqueidentifier = '377E0788-0627-4D65-9668-A427A8B3614F';

-- Create IsPickup
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@isPickupPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'shipVia'), 'IsPickup', 'Is pickup', 'Insite.Admin.ControlTypes.ToggleSwitchControl', @false, @false, @user, @user, 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@isPickupPropertyAttributeConfiguration, @isPickupPropertyConfigurationId, 'ToggleType', 'YesNo', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;