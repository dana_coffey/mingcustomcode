﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @locationPropertyConfigurationId uniqueidentifier = '6BFE365F-B7C3-4058-B214-D8531F3748E0';

-- Create Location
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@locationPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'Dealer'), 'AssociatedWarehouse', 'Associated Warehouse Name', 'Insite.Admin.ControlTypes.TextAreaControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;