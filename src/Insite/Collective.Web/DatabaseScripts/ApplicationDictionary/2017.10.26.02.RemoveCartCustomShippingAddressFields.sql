﻿declare @customShippingAddressPropertyConfigurationId uniqueidentifier = '8A28BB5B-79E5-49DF-9E2F-B914BAEBB262';
declare @useCustomShippingAddressPropertyConfigurationId uniqueidentifier = '21EC9C83-85CF-4EDC-8040-B326A3F1ADB1';
declare @useCustomShippingAddressPropertyAttributeConfigurationId uniqueidentifier = '0523D363-7CD6-44F6-8D54-FBD675AB49AC';

DELETE FROM [AppDict].[PropertyConfiguration] where Id = @customShippingAddressPropertyConfigurationId;
DELETE FROM [AppDict].[PropertyConfiguration] where Id = @useCustomShippingAddressPropertyConfigurationId;
DELETE FROM [AppDict].[PropertyAttributeConfiguration] where Id = @useCustomShippingAddressPropertyAttributeConfigurationId;