﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @propertyConfigurationId uniqueidentifier = '2EBC19B4-612A-4CF9-857D-A7D8012B7C13';
declare @propertyAttributeConfiguration uniqueidentifier = '751E36F1-1F4A-4F74-B966-A7D8012D05C9';

MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@propertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'product'), 'Status', 'Status', 'Insite.Admin.ControlTypes.DropDownControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@propertyAttributeConfiguration, @propertyConfigurationId, 'MultiValueList', 'None,Superseded,Superseded With Inventory,Obsolete', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;