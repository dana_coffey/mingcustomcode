﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @isSouthCarolinaSolidWasteTaxableTaxPropertyConfigurationId uniqueidentifier = 'FD11EB3D-7C91-4C57-8D82-83ABA2482FC5';
declare @isSouthCarolinaSolidWasteTaxablePropertyAttributeConfigurationId uniqueidentifier = '295E2A8D-0C9F-4705-A0EF-2883F4FB4DCB';

-- Create IsSouthCarolinaSolidWasteTaxable
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@isSouthCarolinaSolidWasteTaxableTaxPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'product'), 'IsSouthCarolinaSolidWasteTaxable', 'Is South Carolina Solid Waste Taxable', 'Insite.Admin.ControlTypes.ToggleSwitchControl', @false, @false, @user, @user, 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@isSouthCarolinaSolidWasteTaxablePropertyAttributeConfigurationId, @isSouthCarolinaSolidWasteTaxableTaxPropertyConfigurationId, 'ToggleType', 'YesNo', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;