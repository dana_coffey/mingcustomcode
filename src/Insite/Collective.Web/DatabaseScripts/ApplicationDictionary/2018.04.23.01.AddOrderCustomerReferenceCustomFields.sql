﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @customerOrderCustomerReferencePropertyConfigurationId uniqueidentifier = '68314D9A-D949-45E9-8270-57D39DAFC1E5';
declare @orderHistoryCustomerReferencePropertyConfigurationId uniqueidentifier = '194DF035-538D-4626-A6C1-63D5BF035E4A';

-- Create CustomerReference
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@customerOrderCustomerReferencePropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'customerOrder'), 'CustomerReference', 'Customer Reference', 'Insite.Admin.ControlTypes.TextFieldControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true),
             (@orderHistoryCustomerReferencePropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'orderHistory'), 'CustomerReference', 'Customer Reference', 'Insite.Admin.ControlTypes.TextFieldControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true))
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;
