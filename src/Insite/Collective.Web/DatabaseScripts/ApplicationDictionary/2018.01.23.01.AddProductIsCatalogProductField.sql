﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @isCatalogProductPropertyConfigurationId uniqueidentifier = '89ED7C1B-676F-4679-A20F-F436B46E0DF6';
declare @isCatalogProductPropertyAttributeConfigurationId uniqueidentifier = '44272E88-E331-4067-A53B-CEB87ECA2893';

-- Create IsCatalogProduct
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@isCatalogProductPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'product'), 'IsCatalogProduct', 'Is Catalog Product', 'Insite.Admin.ControlTypes.ToggleSwitchControl', @false, @false, @user, @user, 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@isCatalogProductPropertyAttributeConfigurationId, @isCatalogProductPropertyConfigurationId, 'ToggleType', 'YesNo', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;