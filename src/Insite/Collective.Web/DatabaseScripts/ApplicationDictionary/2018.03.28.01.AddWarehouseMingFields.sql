﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @warehouseCustomerPropertyConfigurationId uniqueidentifier = 'FB390EDC-39D9-4C95-B70D-D93881545D19';
declare @warehouseEmailPropertyConfigurationId uniqueidentifier = 'B41DF5A8-46E5-4F4A-B821-9D7E030F5623';
declare @warehouseTypePropertyConfigurationId uniqueidentifier = '09D27370-7BAC-4C6A-89FD-DAB74B8D7DDE';

-- Create Warehouse Customer
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@warehouseCustomerPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'warehouse'), 'WarehouseCustomer', 'Warehouse Customer', 'Insite.Admin.ControlTypes.TextFieldControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

-- Create Warehouse Email
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@warehouseEmailPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'warehouse'), 'Email', 'Email', 'Insite.Admin.ControlTypes.TextFieldControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

-- Create Warehouse Type
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@warehouseTypePropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'warehouse'), 'WarehouseType', 'Warehouse Type', 'Insite.Admin.ControlTypes.TextFieldControl', @false, @false, @user, @user, 'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;
