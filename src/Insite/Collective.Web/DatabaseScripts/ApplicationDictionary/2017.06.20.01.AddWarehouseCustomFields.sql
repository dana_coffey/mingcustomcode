﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @canPickupPropertyConfigurationId uniqueidentifier = '062CAF14-2F33-4293-846F-057DC5084D7B';
declare @canPickUpPropertyAttributeConfiguration uniqueidentifier = 'C92A2566-6A58-4B83-AF3A-9E51A22FA98F';
declare @canShipPropertyConfigurationId uniqueidentifier = '4873ECC6-49FF-4630-8510-5A658D81B09C';
declare @canShipPropertyAttributeConfiguration uniqueidentifier = '006B02CE-562B-4921-B880-A6EAEEC91D31';


-- Create CanPickUp
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@canPickupPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'warehouse'), 'CanPickUp', 'Pickup Available', 'Insite.Admin.ControlTypes.ToggleSwitchControl', @false, @false, @user, @user, 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@canPickUpPropertyAttributeConfiguration, @canPickupPropertyConfigurationId, 'ToggleType', 'YesNo', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;

-- Create CanShip
MERGE [AppDict].[PropertyConfiguration] AS target
USING (VALUES(@canShipPropertyConfigurationId, (select Id from [AppDict].[EntityConfiguration] where name = 'warehouse'), 'CanShip', 'Shipping Available', 'Insite.Admin.ControlTypes.ToggleSwitchControl', @false, @false, @user, @user, 'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', @true, @true, @true)) 
    AS source (Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
ON
    target.Name = source.Name
    AND target.EntityConfigurationId = source.EntityConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        EntityConfigurationId = source.EntityConfigurationId,
        Name = source.Name,
        Label = source.Label,
        ControlType = source.ControlType,
        IsRequired= source.IsRequired,
        IsTranslatable = source.IsTranslatable,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        PropertyType = source.PropertyType,
        IsCustomProperty = source.IsCustomProperty,
        CanView = source.CanView,
        CanEdit = source.CanEdit
WHEN NOT MATCHED THEN 
    INSERT(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
    VALUES(Id, EntityConfigurationId, Name, Label, ControlType, IsRequired, IsTranslatable, CreatedBy, ModifiedBy, PropertyType, IsCustomProperty, CanView, CanEdit)
;

MERGE
    [AppDict].[PropertyAttributeConfiguration] AS target
USING (VALUES(@canShipPropertyAttributeConfiguration, @canShipPropertyConfigurationId, 'ToggleType', 'YesNo', @user, @user)) 
    AS source (Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
ON
    target.Name = source.Name
    AND target.PropertyConfigurationId = source.PropertyConfigurationId
WHEN MATCHED THEN 
    UPDATE SET 
        PropertyConfigurationId = source.PropertyConfigurationId,
        Name = source.Name,
        [Value] = source.[Value],
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy
WHEN NOT MATCHED THEN 
    INSERT(Id, PropertyConfigurationId, Name, [Value], CreatedBy, ModifiedBy)
    VALUES(Id, PropertyConfigurationId, Name, [Value],  CreatedBy, ModifiedBy)
;