﻿ALTER TABLE [dbo].[ProductField]
DROP COLUMN [FieldVCLId];

ALTER TABLE [dbo].[ProductField]
ADD [FieldCVLId] [varchar](128) NULL;
