﻿CREATE TABLE [dbo].[ProductBarcode](
    [Id] [uniqueidentifier] NOT NULL,
    [ProductId] [uniqueidentifier] NOT NULL,
    [Barcode] [varchar](100) NOT NULL,
    [CreatedOn] [datetimeoffset](7) NOT NULL,
    [CreatedBy] [nvarchar](100) NOT NULL,
    [ModifiedOn] [datetimeoffset](7) NOT NULL,
    [ModifiedBy] [nvarchar](100) NOT NULL,
CONSTRAINT [PK_ProductBarcode] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProductBarcode] ADD  CONSTRAINT [DF_ProductBarcode_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO

ALTER TABLE [dbo].[ProductBarcode] ADD  CONSTRAINT [DF_ProductBarcode_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[ProductBarcode] ADD  CONSTRAINT [DF_ProductBarcode_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[ProductBarcode] ADD  CONSTRAINT [DF_ProductBarcode_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[ProductBarcode] ADD  CONSTRAINT [DF_ProductBarcode_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO

ALTER TABLE [dbo].[ProductBarcode]
WITH CHECK
ADD CONSTRAINT [FK_ProductBarcode_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
ON DELETE CASCADE
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_ProductBarcode_NaturalKey] ON [dbo].[ProductBarcode]
(
    [Barcode] ASC,
    [ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
