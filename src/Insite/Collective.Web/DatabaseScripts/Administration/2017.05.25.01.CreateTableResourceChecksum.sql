﻿CREATE TABLE [dbo].[ResourceChecksum](
    [Id] [uniqueidentifier] NOT NULL,
    [Name] [nvarchar](512) NOT NULL,
    [Sha1Sum] [char](40) NOT NULL,
    [CreatedOn] [datetimeoffset](7) NOT NULL,
    [CreatedBy] [nvarchar](100) NOT NULL,
    [ModifiedOn] [datetimeoffset](7) NOT NULL,
    [ModifiedBy] [nvarchar](100) NOT NULL,
CONSTRAINT [PK_ResourceChecksum] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
