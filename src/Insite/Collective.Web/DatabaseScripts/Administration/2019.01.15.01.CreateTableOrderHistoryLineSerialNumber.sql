﻿CREATE TABLE [dbo].[OrderHistoryLineSerialNumber](
	[OrderHistoryLineId] [uniqueidentifier] NOT NULL,
	[SerialNo] [nvarchar](50) NOT NULL,
	[SeqNo] [int] NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[CreatedBy] [nvarchar](3200) NULL,
	[ModifiedOn] [datetimeoffset](7) NULL,
	[ModifiedBy] [nvarchar](3200) NULL,
 CONSTRAINT [PK_OrderHistoryLineSerialNumber] PRIMARY KEY CLUSTERED 
(
	[OrderHistoryLineId] ASC,
	[SerialNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_SeqNo]  DEFAULT ((0)) FOR [SeqNo]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO