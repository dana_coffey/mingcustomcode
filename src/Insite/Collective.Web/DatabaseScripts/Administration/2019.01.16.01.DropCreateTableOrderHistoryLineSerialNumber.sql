﻿
ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] DROP CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedBy]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] DROP CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] DROP CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedBy]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] DROP CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] DROP CONSTRAINT [DF_OrderHistoryLineSerialNumber_SeqNo]
GO

DROP TABLE [dbo].[OrderHistoryLineSerialNumber]
GO

CREATE TABLE [dbo].[OrderHistoryLineSerialNumber](
	[Id] [uniqueidentifier] NOT NULL,
	[OrderHistoryLineId] [uniqueidentifier] NOT NULL,
	[SerialNo] [nvarchar](50) NOT NULL,
	[SeqNo] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[CreatedBy] [nvarchar](3200) NULL,
	[ModifiedOn] [datetimeoffset](7) NULL,
	[ModifiedBy] [nvarchar](3200) NULL,
 CONSTRAINT [PK_OrderHistoryLineSerialNumber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_SeqNo]  DEFAULT ((0)) FOR [SeqNo]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[OrderHistoryLineSerialNumber] ADD  CONSTRAINT [DF_OrderHistoryLineSerialNumber_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderHistoryLineSerialNumber_OrderHistoryLineId_SerialNo] ON [dbo].[OrderHistoryLineSerialNumber]
(
	[OrderHistoryLineId] ASC,
	[SerialNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO