﻿CREATE TABLE [dbo].[ProductLink](
    [Id] [uniqueidentifier] NOT NULL,
    [ErpNumber] [nvarchar](50) NOT NULL,
    [LinkTo] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
    [CreatedOn] [datetimeoffset](7) NOT NULL,
    [CreatedBy] [nvarchar](100) NOT NULL,
    [ModifiedOn] [datetimeoffset](7) NOT NULL,
    [ModifiedBy] [nvarchar](100) NOT NULL,
CONSTRAINT [PK_ProductLink] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProductLink] ADD  CONSTRAINT [DF_ProductLink_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO

ALTER TABLE [dbo].[ProductLink] ADD  CONSTRAINT [DF_ProductLink_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[ProductLink] ADD  CONSTRAINT [DF_ProductLink_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[ProductLink] ADD  CONSTRAINT [DF_ProductLink_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[ProductLink] ADD  CONSTRAINT [DF_ProductLink_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_ProductLink_NaturalKey] ON [dbo].[ProductLink]
(
    [ErpNumber] ASC,
    [LinkTo] ASC, 
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
