﻿CREATE TABLE [dbo].[ProductField](
    [Id] [uniqueidentifier] NOT NULL,
	[CategoryId] [varchar](128) NOT NULL,
	[CategoryName] [varchar](max) NULL,
	[FieldId] [varchar](128) NOT NULL,
	[FieldName] [varchar](max) NULL,
	[DataType] [varchar](128) NOT NULL,
	[UnitOfMeasure] [varchar](128) NULL,
    [CreatedOn] [datetimeoffset](7) NOT NULL,
    [CreatedBy] [nvarchar](100) NOT NULL,
    [ModifiedOn] [datetimeoffset](7) NOT NULL,
    [ModifiedBy] [nvarchar](100) NOT NULL,
CONSTRAINT [PK_ProductField] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
