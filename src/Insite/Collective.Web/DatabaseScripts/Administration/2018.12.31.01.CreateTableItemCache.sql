﻿CREATE TABLE [dbo].[ItemCache](
	[Id] [uniqueidentifier] NOT NULL,
	[Key] [nvarchar](1600) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[CreatedBy] [nvarchar](3200) NULL,
	[ModifiedOn] [datetimeoffset](7) NULL,
	[ModifiedBy] [nvarchar](3200) NULL,
 CONSTRAINT [PK_ItemCache] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_ItemCacheId]  DEFAULT (newsequentialid()) FOR [Id]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_Key]  DEFAULT ('') FOR [Key]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_Value]  DEFAULT ('') FOR [Value]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_CreatedOn]  DEFAULT (getutcdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_CreatedBy]  DEFAULT ('') FOR [CreatedBy]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_ModifiedOn]  DEFAULT (getutcdate()) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[ItemCache] ADD  CONSTRAINT [DF_ItemCache_ModifiedBy]  DEFAULT ('') FOR [ModifiedBy]
GO

ALTER TABLE [dbo].[ItemCache]  WITH NOCHECK ADD  CONSTRAINT [CK_ItemCache_Key] CHECK  ((len([Key])>(0)))
GO

ALTER TABLE [dbo].[ItemCache] CHECK CONSTRAINT [CK_ItemCache_Key]
GO