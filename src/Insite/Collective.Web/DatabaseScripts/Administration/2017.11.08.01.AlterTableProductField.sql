﻿ALTER TABLE [dbo].[ProductField]
ADD [ExcludedFromSpecifications] [bit] NOT NULL  DEFAULT 0;
