﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;

declare @submittingStatus nvarchar(max) = 'Submitting';
declare @quotedStatus nvarchar(max) = 'Quoted';
declare @submittedStatus nvarchar(max) = 'Submitted';
declare @processingStatus nvarchar(max) = 'Processing';
declare @completeStatus nvarchar(max) = 'Complete';
declare @canceledStatus nvarchar(max) = 'Canceled';

MERGE [OrderStatusMapping] AS target
USING (VALUES(@submittingStatus, 'Submitting', @true, @false, @false),
             (@quotedStatus, 'Quoted', @false, @false, @false),
             (@submittedStatus, 'Submitted', @false, @false, @false),
             (@processingStatus, 'Processing', @false, @false, @false),
             (@completeStatus, 'Complete', @false, @false, @false),
             (@canceledStatus, 'Canceled', @false, @false, @false))
    AS source (ErpOrderStatus, DisplayName, IsDefault, AllowRma, AllowCancellation)
ON
    target.ErpOrderStatus = source.ErpOrderStatus
WHEN MATCHED THEN
    UPDATE SET
        DisplayName = source.DisplayName,
        IsDefault = source.IsDefault,
        AllowRma = source.AllowRma,
        AllowCancellation = source.AllowCancellation,
        ModifiedOn = sysutcdatetime(),
        ModifiedBy = @user
WHEN NOT MATCHED THEN
    INSERT(Id, ErpOrderStatus, DisplayName, IsDefault, AllowRma, AllowCancellation, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
    VALUES(NewId(), source.ErpOrderStatus, source.DisplayName, source.IsDefault, source.AllowRma, source.AllowCancellation, sysutcdatetime(), @user, sysutcdatetime(), @user)
;