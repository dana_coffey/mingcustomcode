﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;

declare @processing4Status nvarchar(max) = 'Invoiced';

MERGE [OrderStatusMapping] AS target
USING (VALUES(@processing4Status, 'Processing', @false, @false, @false))
    AS source (ErpOrderStatus, DisplayName, IsDefault, AllowRma, AllowCancellation)
ON
    target.ErpOrderStatus = source.ErpOrderStatus
WHEN MATCHED THEN
    UPDATE SET
        DisplayName = source.DisplayName,
        IsDefault = source.IsDefault,
        AllowRma = source.AllowRma,
        AllowCancellation = source.AllowCancellation,
        ModifiedOn = sysutcdatetime(),
        ModifiedBy = @user
WHEN NOT MATCHED THEN
    INSERT(Id, ErpOrderStatus, DisplayName, IsDefault, AllowRma, AllowCancellation, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
    VALUES(NewId(), source.ErpOrderStatus, source.DisplayName, source.IsDefault, source.AllowRma, source.AllowCancellation, sysutcdatetime(), @user, sysutcdatetime(), @user)
;