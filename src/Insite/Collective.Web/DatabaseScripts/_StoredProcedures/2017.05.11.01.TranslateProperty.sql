﻿if object_id('TranslateProperty') is null
    exec('create procedure TranslateProperty as set nocount on;')
go

alter procedure TranslateProperty @parentTable nvarchar(max), @parentId uniqueidentifier, @name nvarchar(max), @englishTranslation nvarchar(max)
as begin
    declare @user nvarchar(max) = 'migration';
    declare @englishLanguageId uniqueidentifier = 'A26095EF-C714-E311-BA31-D43D7E4E88B2';

    MERGE [dbo].[TranslationProperty] AS target
    USING (VALUES(@englishTranslation, @englishLanguageId))
        AS sourceTable (Translation, LanguageId)
    ON
        target.parenttable = @parentTable
        and target.parentId = @parentId
        and target.Name = @name
        and target.LanguageId = sourceTable.LanguageId
    WHEN MATCHED THEN 
        UPDATE SET 
            TranslatedValue = sourceTable.Translation,
            ModifiedOn = SYSUTCDATETIME(),
            ModifiedBy = @user
    WHEN NOT MATCHED THEN 
        INSERT(Id, ParentTable, ParentId, Name, TranslatedValue, LanguageId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        VALUES(NEWID(), @parentTable, @parentId, @name, Translation, LanguageId, SYSUTCDATETIME(), @user, SYSUTCDATETIME(), @user);
end;
go
