﻿if object_id('UpdateBillToAddressField') is null
    exec('create procedure UpdateBillToAddressField as set nocount on;')
go

alter procedure UpdateBillToAddressField @fieldName nvarchar(max), @isVisible bit, @isRequired bit, @maxFieldLength int = null, @websiteId uniqueidentifier = 'C2C53320-98DC-4ECA-8022-9EFC00DEA0DC'
as begin
    update dbo.BillToAddressField
    set MaxFieldLength = IsNull(@maxFieldLength, MaxFieldLength),
        IsRequired = @isRequired,
        IsVisible = @isVisible
    where FieldName = @fieldName and WebsiteId = @websiteId
end;
go
