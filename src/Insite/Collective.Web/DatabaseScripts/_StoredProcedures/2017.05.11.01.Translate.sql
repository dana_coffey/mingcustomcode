﻿if object_id('Translate') is null
    exec('create procedure Translate as set nocount on;')
go

alter procedure Translate @keyword nvarchar(max), @englishTranslation nvarchar(max), @translationSource varchar(max) = 'Label'
as begin
    declare @user nvarchar(max) = 'migration';
    declare @englishLanguageId uniqueidentifier = 'A26095EF-C714-E311-BA31-D43D7E4E88B2';

    MERGE [dbo].[TranslationDictionary] AS target
    USING (VALUES(@englishTranslation, @englishLanguageId))
        AS sourceTable (Translation, LanguageId)
    ON
        target.Source = @translationSource
        and target.Keyword = @keyword
        and target.LanguageId = sourceTable.LanguageId
    WHEN MATCHED THEN 
        UPDATE SET 
            Translation = sourceTable.Translation,
            ModifiedOn = SYSUTCDATETIME(),
            ModifiedBy = @user
    WHEN NOT MATCHED THEN 
        INSERT(Id, Source, Keyword, Translation, LanguageId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        VALUES(NEWID(), @translationSource, @keyword, Translation, LanguageId, SYSUTCDATETIME(), @user, SYSUTCDATETIME(), @user);
end;
go