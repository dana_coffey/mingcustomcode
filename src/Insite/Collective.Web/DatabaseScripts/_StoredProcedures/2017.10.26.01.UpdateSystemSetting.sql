﻿if object_id('UpdateSystemSetting') is null
    exec('create procedure UpdateSystemSetting as set nocount on;')
go

alter procedure UpdateSystemSetting @settingName nvarchar(max), @value nvarchar(max), @websiteId uniqueidentifier = NULL
as begin
    declare @defaultWebsiteId uniqueidentifier = 'C2C53320-98DC-4ECA-8022-9EFC00DEA0DC';
    declare @user nvarchar(max) = 'migration';
    declare @true bit = 1;
    declare @false bit = 0;

    IF @websiteId = NULL
        DELETE FROM dbo.SystemSetting where Name = @settingName and WebsiteId = @defaultWebsiteId;

    MERGE dbo.SystemSetting AS target
    USING (VALUES(newid(), @settingName, @websiteId, @value, @user, @user)) 
        AS source (Id, Name, WebsiteId, [Value], CreatedBy, ModifiedBy)
    ON
        target.Name = source.Name
        and (target.WebsiteId = source.WebsiteId or (target.WebsiteId is null and source.WebsiteId is null))
    WHEN MATCHED THEN 
        UPDATE SET 
            [Value] = source.[Value]
    WHEN NOT MATCHED THEN 
        INSERT(Id, Name, WebsiteId, [Value], CreatedBy, ModifiedBy)
        VALUES(Id, Name, WebsiteId, [Value], CreatedBy, ModifiedBy);
end;
go
