﻿if object_id('UpdateContentItemField') is null
    exec('create procedure UpdateContentItemField as set nocount on;')
go

alter procedure UpdateContentItemField
    @contentItemClass nvarchar(max), 
    @contentItemName nvarchar(max),
    @languageId uniqueidentifier,
    @fieldType nvarchar(max),
    @fieldName nvarchar(max),
    @dateTimeValue datetimeoffset = null, 
    @decimalValue decimal = null, 
    @intValue int = null, 
    @stringValue nvarchar(max) = null, 
    @booleanValue bit = null, 
    @objectValue varbinary(max) = null,
    @websiteId uniqueidentifier = 'C2C53320-98DC-4ECA-8022-9EFC00DEA0DC',
    @personaId uniqueidentifier = 'D06988C0-9358-4DBB-AA3D-B7BE5B6A7FD9'
as begin
    declare @user nvarchar(max) = 'migration';
    declare @contentKey int = (select top 1 ContentKey from [dbo].[ContentItem] where Class = @contentItemClass and Name = @contentItemName and WebSiteId = @websiteId order by PublishOn desc)

    insert into [dbo].[ContentItemField] (Id, ContentKey, LanguageId, PersonaId, FieldType, FieldName, DateTimeValue, DecimalValue, IntValue, StringValue, BooleanValue, ObjectValue, PublishOn, ApprovedOn, CreatedOn, IsRetracted, CreatedBy, ModifiedOn, ModifiedBy)
    values(newid(), @contentKey, @languageId, @personaId, @fieldType, @fieldName, @dateTimeValue, @decimalValue, @intValue, @stringValue, @booleanValue, @objectValue, sysutcdatetime(), sysutcdatetime(), sysutcdatetime(), 0, @user, sysutcdatetime(), @user)
end;
go