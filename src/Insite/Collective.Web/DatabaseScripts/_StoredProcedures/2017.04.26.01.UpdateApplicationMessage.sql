﻿if object_id('UpdateApplicationMessage') is null
    exec('create procedure UpdateApplicationMessage as set nocount on;')
go

alter procedure UpdateApplicationMessage
    @name nvarchar(max),
    @message nvarchar(max)
as begin
    declare @user nvarchar(max) = 'migration';

    MERGE
        [dbo].[ApplicationMessage] AS target
    USING (VALUES(@name, @message))
        AS sourceTable (Name, [Message])
    ON
        target.Name = sourceTable.Name
    WHEN MATCHED THEN 
        UPDATE SET 
            [Message] = sourceTable.[Message],
            ModifiedOn = SYSUTCDATETIME(),
            ModifiedBy = @user
    WHEN NOT MATCHED THEN 
        INSERT(Id, Name, [Message], CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        VALUES(NEWID(), Name, [Message], SYSUTCDATETIME(), @user, SYSUTCDATETIME(), @user);
end;
go