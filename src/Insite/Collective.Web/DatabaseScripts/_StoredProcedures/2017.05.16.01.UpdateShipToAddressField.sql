﻿if object_id('UpdateShipToAddressField') is null
    exec('create procedure UpdateShipToAddressField as set nocount on;')
go

alter procedure UpdateShipToAddressField @fieldName nvarchar(max), @isVisible bit, @isRequired bit, @maxFieldLength int = null, @websiteId uniqueidentifier = 'C2C53320-98DC-4ECA-8022-9EFC00DEA0DC'
as begin
    update dbo.ShipToAddressField
    set MaxFieldLength = IsNull(@maxFieldLength, MaxFieldLength),
        IsRequired = @isRequired,
        IsVisible = @isVisible
    where FieldName = @fieldName and WebsiteId = @websiteId
end;
go
