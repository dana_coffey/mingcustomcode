﻿update [dbo].[SystemSetting]
set value = '/UserFiles/Images/Products/Default_Large.png'
where name = 'CatalogGeneral_NotFoundLargeImagePath'

update [dbo].[SystemSetting]
set value = '/UserFiles/Images/Products/Default_Medium.png'
where name = 'CatalogGeneral_NotFoundMediumImagePath'

update [dbo].[SystemSetting]
set value = '/UserFiles/Images/Products/Default_Small.png'
where name = 'CatalogGeneral_NotFoundSmallImagePath'