﻿update dbo.SystemSetting 
set Value = 'false'
where Name = 'Autocomplete_ContentEnabled'

update dbo.SystemSetting 
set Value = 2
where Name = 'FuzzySearch_MaxEdits'

update dbo.SystemSetting 
set Value = 3
where Name = 'FuzzySearch_PrefixLength'

update dbo.SystemSetting 
set Value = 'false'
where Name = 'SearchSuggestions_Enabled'

update dbo.SystemSetting 
set Value = 3
where Name = 'SearchSuggestions_AutoCorrectThreshold'

update dbo.SystemSetting 
set Value = 1
where Name = 'SearchSuggestions_DidYouMeanThreshold'
