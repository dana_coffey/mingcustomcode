﻿declare @fieldType varchar(25) = 'General';
declare @fieldName varchar(25) = 'DisallowInRobotsTxt';
declare @true bit = 1;

exec UpdateContentItemField @contentItemClass = 'InvoicesPage',
    @contentItemName = 'Invoice History',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'InvoiceDetailPage',
    @contentItemName = 'Invoice Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'MyJobQuotesPage',
    @contentItemName = 'My Job Quotes',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'JobQuoteDetailsPage',
    @contentItemName = 'Job Quote Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'OrdersPage',
    @contentItemName = 'Order History',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'OrderDetailPage',
    @contentItemName = 'Order Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'OrderApprovalListPage',
    @contentItemName = 'Order Approval',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'OrderApprovalDetailPage',
    @contentItemName = 'Order Approval Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RequisitionPage',
    @contentItemName = 'Requisitions',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RfqMyQuotesPage',
    @contentItemName = 'My Quotes',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RfqQuoteDetailsPage',
    @contentItemName = 'Quote Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'WishListPage',
    @contentItemName = 'Wishlists',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'AccountSettingsPage',
    @contentItemName = 'Account Settings',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ChangeAccountPasswordPage',
    @contentItemName = 'Change Password',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ExpiredAccountActivationLinkPage',
    @contentItemName = 'Expired Account Activation Link',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ExpiredResetPasswordLinkPage',
    @contentItemName = 'Expired Reset Password Link',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ResetPasswordPage',
    @contentItemName = 'Reset Password',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'MyAccountAddressPage',
    @contentItemName = 'My Addresses',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ChangeCustomerPage',
    @contentItemName = 'Change Customer',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'CreateAccountPage',
    @contentItemName = 'Create Account',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'SignInPage',
    @contentItemName = 'Sign In',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'UserListPage',
    @contentItemName = 'User Administration',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'UserSetupPage',
    @contentItemName = 'User Setup',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'UserSetupShipToPage',
    @contentItemName = 'Assign / Edit Ship To',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'BudgetPage',
    @contentItemName = 'Budget Management',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'SavedOrderListPage',
    @contentItemName = 'Saved Orders',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'SavedOrderDetailPage',
    @contentItemName = 'Saved Order Details',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'CartPage',
    @contentItemName = 'Cart',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'CheckoutAddressPage',
    @contentItemName = 'Addresses',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'OrderConfirmationPage',
    @contentItemName = 'Order Confirmation',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ReviewAndPayPage',
    @contentItemName = 'Review and Pay',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RequisitionConfirmationPage',
    @contentItemName = 'Requisition Confirmation',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RfqConfirmationPage',
    @contentItemName = 'Quote Confirmation',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'RfqRequestQuotePage',
    @contentItemName = 'Request a Quote',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ErrorPage',
    @contentItemName = 'Unhandled Error',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'ForbiddenErrorPage',
    @contentItemName = 'Unauthorized Access',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true

exec UpdateContentItemField @contentItemClass = 'NotFoundErrorPage',
    @contentItemName = 'Page Not Found',
    @languageId = null,
    @fieldType = @fieldType,
    @fieldName = @fieldName,
    @booleanValue = @true