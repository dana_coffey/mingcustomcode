﻿exec UpdateSystemSetting 'ConsoleSecurity_RequireUniqueEmail', 'true'
exec UpdateSystemSetting 'ConsoleSecurity_PasswordRequiresUppercase', 'true'
exec UpdateSystemSetting 'ConsoleSecurity_PasswordRequiresLowercase', 'true'
exec UpdateSystemSetting 'ConsoleSecurity_PasswordRequiresDigit', 'true'

exec UpdateSystemSetting 'ConsoleSecurity_PasswordRequiresSpecialCharacter', 'false'

exec UpdateSystemSetting 'StorefrontSecurity_RequireUniqueEmail', 'true'
exec UpdateSystemSetting 'StorefrontSecurity_PasswordRequiresUppercase', 'true'
exec UpdateSystemSetting 'StorefrontSecurity_PasswordRequiresLowercase', 'true'
exec UpdateSystemSetting 'StorefrontSecurity_PasswordRequiresDigit', 'true'

exec UpdateSystemSetting 'StorefrontSecurity_PasswordRequiresSpecialCharacter', 'false'

exec UpdateSystemSetting 'StorefrontUserPermissions_AllowBillToAddressEdit', 'false'
exec UpdateSystemSetting 'StorefrontUserPermissions_AllowCreateAccount', 'false'
exec UpdateSystemSetting 'StorefrontUserPermissions_AllowCreateNewShipToAddress', 'false'

exec UpdateSystemSetting 'Inventory_ShowInventoryAvailability', 'Secure'

exec UpdateSystemSetting 'Checkout_ShowNewsletterSignup', 'false'
exec UpdateSystemSetting 'Checkout_AllowCreditCardPaymentMethod', 'false'
exec UpdateSystemSetting 'Checkout_AllowGuestCheckout', 'false'

exec UpdateSystemSetting 'SiteConfigurationsGeneral_AllowSubscribeToNewsletter', 'false'