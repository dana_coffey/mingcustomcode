﻿declare @attributeRefresh uniqueidentifier = '2cf45ed2-258a-4ef2-b2ce-5efa0e0156e3';
declare @categoryRefresh uniqueidentifier = 'baefbc39-ca30-4449-bf9a-4afad494d0ce';
declare @productRefresh uniqueidentifier = 'b6dd86f8-aea7-4c87-9776-6cfd7840aa6c';
declare @resourceRefresh uniqueidentifier = 'cff29d18-1d41-4e82-bb1d-221253f47dc3';
declare @rebuildSearchIndex uniqueidentifier = '54e0019f-e220-436e-85b3-a5e80169a3dd';
declare @rebuildSitemap uniqueidentifier = '2ce6a7bd-a119-4ad2-8aa4-a74300f772b8';
declare @archive uniqueidentifier = '58117ae2-19bc-43f0-a343-a94843cbe984';

declare @notifyEmail varchar(max) = 'nrobichaud@absolunet.com,bmoreau@absolunet.com,amarsolais@absolunet.com,mfilion@absolunet.com';


update [dbo].[JobDefinition]
    set NotifyEmail = @notifyEmail,
        NotifyCondition = 'Failure'
where Id in(@attributeRefresh, @categoryRefresh, @productRefresh, @resourceRefresh, @rebuildSearchIndex, @rebuildSitemap, @archive)