﻿declare @jobDefinitionId uniqueidentifier = 'CFF29D18-1D41-4E82-BB1D-221253F47DC3';
declare @jobDefinitionStepId uniqueidentifier = 'C502AA18-25EF-4BD8-A07F-6643CBC6A059';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
         @connectionId, --IntegrationConnectionId
         'Collective - Resource Refresh', --Name
         '', --Description
         'Refresh', --JobType
         '', --NotifyEmail
         'Failure', --NotifyCondition
         null, --LinkedJobId
         @false, --PassDataSetToLinkedJob
         @false, --UseDeltaDataSet
         'Collective - Resource Refresh', --PreProcessor
         'CollectiveResourceRefresh', --IntegrationProcessor
         'None', --PostProcessor
         @false, --RecurringJob
         null, --RecurringStartDateTime
         null, --RecurringEndDateTime
         @false, --RecurringInterval
         'Days', --RecurringType
         null, --RecurringStartTime
         @false, --RecurringStartDay
         null, --EmailTemplateId
         @false, --RunStepsInParallel
         'SuccessOnly', --LinkedJobCondition
         @user,
         @user);

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
          @jobDefinitionId, --JobDefinitionId
         1, --Sequence
         'Resource Refresh', --Name
         '', --ObjectName
         'Ignore', --DeleteAction
         '', --DeleteActionFieldToSet
         '', --DeleteActionValueToSet
         @true, --SkipHeaderRow
         @user, --CreatedBy
         @user); --ModifiedBy
