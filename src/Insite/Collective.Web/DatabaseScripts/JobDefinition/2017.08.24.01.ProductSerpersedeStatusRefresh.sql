﻿declare @jobDefinitionId uniqueidentifier = 'D3E824F5-CA59-4107-8EF9-C8F8619B54D4';
declare @jobDefinitionStepId uniqueidentifier = '743801C4-9E0D-4CE3-861F-D6BD9676480B';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
        @connectionId, --IntegrationConnectionId
        'Collective - Product Supersede Status Refresh', --Name
        '', --Description
        'Refresh', --JobType
        '', --NotifyEmail
        'Failure', --NotifyCondition
        null, --LinkedJobId
        @false, --PassDataSetToLinkedJob
        @false, --UseDeltaDataSet
        'None', --PreProcessor
        'None', --IntegrationProcessor
        'Collective - Product Supersede Status Refresh', --PostProcessor
        @false, --RecurringJob
        null, --RecurringStartDateTime
        null, --RecurringEndDateTime
        @false, --RecurringInterval
        'Days', --RecurringType
        null, --RecurringStartTime
        @false, --RecurringStartDay
        null, --EmailTemplateId
        @false, --RunStepsInParallel
        'SuccessOnly', --LinkedJobCondition
        @user,
        @user);

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
        @jobDefinitionId, --JobDefinitionId
        1, --Sequence
        'Product Supersede Status Refresh', --Name
        '', --ObjectName
        'Ignore', --DeleteAction
        '', --DeleteActionFieldToSet
        '', --DeleteActionValueToSet
        @true, --SkipHeaderRow
        @user, --CreatedBy
        @user); --ModifiedBy

