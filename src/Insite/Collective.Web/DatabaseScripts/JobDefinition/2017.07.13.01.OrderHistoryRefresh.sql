﻿declare @jobDefinitionId uniqueidentifier = '37CB86CF-B830-40EF-8193-D84F43335194';
declare @jobDefinitionStepId uniqueidentifier = '503BCC2B-DE98-41B9-9810-2FABEADBEE46';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
         @connectionId, --IntegrationConnectionId
         'Collective - Order History Refresh', --Name
         '', --Description
         'Refresh', --JobType
         '', --NotifyEmail
         'Failure', --NotifyCondition
         null, --LinkedJobId
         @false, --PassDataSetToLinkedJob
         @false, --UseDeltaDataSet
         'None', --PreProcessor
         'CollectiveOrderHistoryRefresh', --IntegrationProcessor
         'Collective - Order History Refresh', --PostProcessor
         @false, --RecurringJob
         null, --RecurringStartDateTime
         null, --RecurringEndDateTime
         @false, --RecurringInterval
         'Days', --RecurringType
         null, --RecurringStartTime
         @false, --RecurringStartDay
         null, --EmailTemplateId
         @false, --RunStepsInParallel
         'SuccessOnly', --LinkedJobCondition
         @user,
         @user);

--Job definition UpdatedSince parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        0, --Sequence
        'SpecificDateTime', --ValueType
        '', --DefaultValue
        'Updated Since', --Prompt
        'UpdatedSince', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition UpdatedTo parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        1, --Sequence
        'SpecificDateTime', --ValueType
        '', --DefaultValue
        'Updated To', --Prompt
        'UpdatedTo', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition ErpOrderNumber parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        2, --Sequence
        'String', --ValueType
        '', --DefaultValue
        'ERP Order Number', --Prompt
        'ErpOrderNumber', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition ErpOrderNumber parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        3, --Sequence
        'String', --ValueType
        '', --DefaultValue
        'ERP Order Suffix', --Prompt
        'ErpOrderSuffix', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
          @jobDefinitionId, --JobDefinitionId
         1, --Sequence
         'Order History Refresh', --Name
         '', --ObjectName
         'Ignore', --DeleteAction
         '', --DeleteActionFieldToSet
         '', --DeleteActionValueToSet
         @true, --SkipHeaderRow
         @user, --CreatedBy
         @user); --ModifiedBy
