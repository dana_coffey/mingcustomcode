﻿declare @jobDefinitionId uniqueidentifier = '749D5611-7E58-4221-946B-31EE9BA26CB0';
declare @jobDefinitionStepId uniqueidentifier = 'A8413F1E-3909-4CF7-8572-A051C3DF5558';
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;

--Job definition
insert into [dbo].[JobDefinition](
    Id,
    IntegrationConnectionId,
    Name,
    Description,
    JobType,
    NotifyEmail,
    NotifyCondition,
    LinkedJobId,
    PassDataSetToLinkedJob,
    UseDeltaDataSet,
    PreProcessor,
    IntegrationProcessor,
    PostProcessor,
    RecurringJob,
    RecurringStartDateTime,
    RecurringEndDateTime,
    RecurringInterval,
    RecurringType,
    RecurringStartTime,
    RecurringStartDay,
    EmailTemplateId,
    RunStepsInParallel,
    LinkedJobCondition,
    CreatedBy,
    ModifiedBy)
    values(@jobDefinitionId, --Id
         @connectionId, --IntegrationConnectionId
         'Order Submit', --Name
         'Submit the order to SX.e', --Description
         'Submit', --JobType
         '', --NotifyEmail
         'Failure', --NotifyCondition
         null, --LinkedJobId
         @false, --PassDataSetToLinkedJob
         @false, --UseDeltaDataSet
         'GenericSubmit', --PreProcessor
         'CollectiveOrderSubmit', --IntegrationProcessor
         'Collective - Order Submit', --PostProcessor
         @false, --RecurringJob
         null, --RecurringStartDateTime
         null, --RecurringEndDateTime
         @false, --RecurringInterval
         'Days', --RecurringType
         null, --RecurringStartTime
         @false, --RecurringStartDay
         null, --EmailTemplateId
         @false, --RunStepsInParallel
         'SuccessOnly', --LinkedJobCondition
         @user,
         @user);

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
          @jobDefinitionId, --JobDefinitionId
         1, --Sequence
         'Submit Order', --Name
         'CustomerOrder', --ObjectName
         'Ignore', --DeleteAction
         '', --DeleteActionFieldToSet
         '', --DeleteActionValueToSet
         @true, --SkipHeaderRow
         @user, --CreatedBy
         @user); --ModifiedBy

--Job definition step parameter
insert into [dbo].[JobDefinitionStepParameter](JobDefinitionStepId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --JobDefinitionStepId
         1, --Sequence
         'String', --ValueType
         '', --DefaultValue
         'Order Number', --Prompt
         'OrderNumber', --Name
         @user, --CreatedBy
         @user); --ModifiedBy
