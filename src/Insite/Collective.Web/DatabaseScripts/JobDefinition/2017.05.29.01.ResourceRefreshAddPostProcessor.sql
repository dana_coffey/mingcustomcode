﻿declare @jobDefinitionId uniqueidentifier = 'CFF29D18-1D41-4E82-BB1D-221253F47DC3';

--Job definition
update [dbo].[JobDefinition]
set PostProcessor = 'Collective - Resource Refresh'
where Id = @jobDefinitionId