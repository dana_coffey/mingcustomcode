﻿declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';


MERGE [dbo].[IntegrationConnection] AS target
USING (VALUES(@connectionId, 'Collective Service', 'Internal', @false, 30, @user, @user, @false)) 
    AS source (Id, Name, TypeName, DebuggingEnabled, ArchiveRetentionDays, CreatedBy, ModifiedBy, IntegratedSecurity)
ON
    target.Id = source.Id
WHEN MATCHED THEN 
    UPDATE SET
        Name = source.Name,
        TypeName = source.TypeName,
        DebuggingEnabled = source.DebuggingEnabled,
        ArchiveRetentionDays= source.ArchiveRetentionDays,
        CreatedBy = source.CreatedBy,
        ModifiedBy = source.ModifiedBy,
        IntegratedSecurity = source.IntegratedSecurity
WHEN NOT MATCHED THEN 
    INSERT(Id, Name, TypeName, DebuggingEnabled, ArchiveRetentionDays, CreatedBy, ModifiedBy, IntegratedSecurity)
    VALUES(Id, Name, TypeName, DebuggingEnabled, ArchiveRetentionDays, CreatedBy, ModifiedBy, IntegratedSecurity)
;