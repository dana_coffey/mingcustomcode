﻿declare @jobDefinitionId uniqueidentifier = '58117AE2-19BC-43F0-A343-A94843CBE984';
declare @jobDefinitionStepId uniqueidentifier = '5FDA6495-633D-4810-A3F6-B54FCC696C92';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
         @connectionId, --IntegrationConnectionId
         'Collective - Archive inRiver Queue', --Name
         '', --Description
         'Refresh', --JobType
         '', --NotifyEmail
         'Failure', --NotifyCondition
         null, --LinkedJobId
         @false, --PassDataSetToLinkedJob
         @false, --UseDeltaDataSet
         'None', --PreProcessor
         'CollectiveArchiveInRiverQueue', --IntegrationProcessor
         'None', --PostProcessor
         @false, --RecurringJob
         null, --RecurringStartDateTime
         null, --RecurringEndDateTime
         @false, --RecurringInterval
         'Days', --RecurringType
         null, --RecurringStartTime
         @false, --RecurringStartDay
         null, --EmailTemplateId
         @false, --RunStepsInParallel
         'SuccessOnly', --LinkedJobCondition
         @user,
         @user);

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
          @jobDefinitionId, --JobDefinitionId
         1, --Sequence
         'Archive inRiver Queue', --Name
         '', --ObjectName
         'Ignore', --DeleteAction
         '', --DeleteActionFieldToSet
         '', --DeleteActionValueToSet
         @true, --SkipHeaderRow
         @user, --CreatedBy
         @user); --ModifiedBy
