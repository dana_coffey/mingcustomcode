﻿declare @jobDefinitionStepId uniqueidentifier = 'A8413F1E-3909-4CF7-8572-A051C3DF5558';
declare @user nvarchar(max) = 'migration';

--Job definition step parameter
insert into [dbo].[JobDefinitionStepParameter](Id, JobDefinitionStepId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values('E4084CD5-FCC2-E711-A9DF-28F0762A7561',
         @jobDefinitionStepId, --JobDefinitionStepId
         1, --Sequence
         'String', --ValueType
         'ISC', --DefaultValue
         'ERP Custom Ship To Prefix', --Prompt
         'ErpShipToPrefix', --Name
         @user, --CreatedBy
         @user); --ModifiedBy
