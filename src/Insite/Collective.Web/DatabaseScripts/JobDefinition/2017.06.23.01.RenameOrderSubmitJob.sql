﻿declare @jobDefinitionId uniqueidentifier = '749D5611-7E58-4221-946B-31EE9BA26CB0';

update [dbo].[JobDefinition]
set [Name] = 'Collective - Order Submit',
[StandardJobName] = 'OrderSubmit'
where [Id] = @jobDefinitionId
