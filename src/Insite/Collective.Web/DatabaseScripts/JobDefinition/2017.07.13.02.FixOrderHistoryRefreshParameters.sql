﻿declare @jobDefinitionId uniqueidentifier = '37CB86CF-B830-40EF-8193-D84F43335194';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

delete from [dbo].[JobDefinitionParameter]
    where [JobDefinitionId] = @jobDefinitionId;

--Job definition UpdatedSince parameter
insert into [dbo].[JobDefinitionParameter](Id, JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values('2AA4BADE-433D-478B-8B06-C6B77E7DE4CE', --Id
        @jobDefinitionId, --JobDefinitionId
        0, --Sequence
        'SpecificDateTime', --ValueType
        '', --DefaultValue
        'Updated Since', --Prompt
        'UpdatedSince', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition UpdatedTo parameter
insert into [dbo].[JobDefinitionParameter](Id, JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values('AA51E99F-9811-476C-92F0-C6C0CA3F5B7C', --Id
        @jobDefinitionId, --JobDefinitionId
        1, --Sequence
        'SpecificDateTime', --ValueType
        '', --DefaultValue
        'Updated To', --Prompt
        'UpdatedTo', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition ErpOrderNumber parameter
insert into [dbo].[JobDefinitionParameter](Id, JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values('1153CD9C-BE53-47C6-9672-C8D8C12E8BC2', --Id
        @jobDefinitionId, --JobDefinitionId
        2, --Sequence
        'String', --ValueType
        '', --DefaultValue
        'ERP Order Number', --Prompt
        'ErpOrderNumber', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition ErpOrderNumber parameter
insert into [dbo].[JobDefinitionParameter](Id, JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values('93C426E9-7D24-45D7-9651-A63F074C658E', --Id
        @jobDefinitionId, --JobDefinitionId
        3, --Sequence
        'String', --ValueType
        '', --DefaultValue
        'ERP Order Suffix', --Prompt
        'ErpOrderSuffix', --Name
        @user, --CreatedBy
        @user); --ModifiedBy