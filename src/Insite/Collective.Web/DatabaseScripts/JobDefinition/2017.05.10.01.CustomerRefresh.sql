﻿declare @jobDefinitionId uniqueidentifier = '1A2FB3C0-99F8-47AA-9DA4-DD9F6296EB05';
declare @jobDefinitionBillToStepId uniqueidentifier = 'A3FCB841-071C-455C-B031-780DE2D688E2';
declare @jobDefinitionShipToStepId uniqueidentifier = '68A66DD4-0B49-4E87-BF9D-6C3F05C6125E';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
        @connectionId, --IntegrationConnectionId
        'Collective - Customer Refresh', --Name
        '', --Description
        'Refresh', --JobType
        '', --NotifyEmail
        'Failure', --NotifyCondition
        null, --LinkedJobId
        @false, --PassDataSetToLinkedJob
        @false, --UseDeltaDataSet
        'None', --PreProcessor
        'CollectiveCustomerRefresh', --IntegrationProcessor
        'Collective - Customer Refresh', --PostProcessor
        @false, --RecurringJob
        null, --RecurringStartDateTime
        null, --RecurringEndDateTime
        @false, --RecurringInterval
        'Days', --RecurringType
        null, --RecurringStartTime
        @false, --RecurringStartDay
        null, --EmailTemplateId
        @false, --RunStepsInParallel
        'SuccessOnly', --LinkedJobCondition
        @user,
        @user);

--Job definition UpdatedSince parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        0, --Sequence
        'SpecificDateTime', --ValueType
        '', --DefaultValue
        'Updated Since', --Prompt
        'UpdatedSince', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition CustomerNumber parameter
insert into [dbo].[JobDefinitionParameter](JobDefinitionId, Sequence, ValueType, DefaultValue, Prompt, Name, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --JobDefinitionId
        1, --Sequence
        'String', --ValueType
        '', --DefaultValue
        'Customer Number', --Prompt
        'CustomerNumber', --Name
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition bill to step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionBillToStepId, --Id
        @jobDefinitionId, --JobDefinitionId
        1, --Sequence
        'Customer Refresh - Bill To', --Name
        'customer', --ObjectName
        'Ignore', --DeleteAction
        '', --DeleteActionFieldToSet
        '', --DeleteActionValueToSet
        @true, --SkipHeaderRow
        @user, --CreatedBy
        @user); --ModifiedBy

--Job definition ship to step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionShipToStepId, --Id
        @jobDefinitionId, --JobDefinitionId
        2, --Sequence
        'Customer Refresh - Ship To', --Name
        'customer', --ObjectName
        'Ignore', --DeleteAction
        '', --DeleteActionFieldToSet
        '', --DeleteActionValueToSet
        @true, --SkipHeaderRow
        @user, --CreatedBy
        @user); --ModifiedBy
