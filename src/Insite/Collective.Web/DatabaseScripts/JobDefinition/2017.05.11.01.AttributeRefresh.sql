﻿declare @jobDefinitionId uniqueidentifier = '2cf45ed2-258a-4ef2-b2ce-5efa0e0156e3';
declare @jobDefinitionStepId uniqueidentifier = '8b3bd992-ef43-46fe-9e95-bac3ffb84ee0';
declare @user nvarchar(max) = 'migration';
declare @true bit = 1;
declare @false bit = 0;
declare @connectionId uniqueidentifier = '14744980-601a-404e-94c9-a7620154e0f0';

--Job definition
insert into [dbo].[JobDefinition](Id, IntegrationConnectionId, Name, Description, JobType, NotifyEmail, NotifyCondition, LinkedJobId, PassDataSetToLinkedJob, UseDeltaDataSet, PreProcessor, IntegrationProcessor, PostProcessor, RecurringJob, RecurringStartDateTime, RecurringEndDateTime, RecurringInterval, RecurringType, RecurringStartTime, RecurringStartDay, EmailTemplateId, RunStepsInParallel, LinkedJobCondition, CreatedBy, ModifiedBy)
    values(@jobDefinitionId, --Id
         @connectionId, --IntegrationConnectionId
         'Collective - Attribute Refresh', --Name
         '', --Description
         'Refresh', --JobType
         '', --NotifyEmail
         'Failure', --NotifyCondition
         null, --LinkedJobId
         @false, --PassDataSetToLinkedJob
         @false, --UseDeltaDataSet
         'None', --PreProcessor
         'CollectiveAttributeRefresh', --IntegrationProcessor
         'Collective - Attribute Refresh', --PostProcessor
         @false, --RecurringJob
         null, --RecurringStartDateTime
         null, --RecurringEndDateTime
         @false, --RecurringInterval
         'Days', --RecurringType
         null, --RecurringStartTime
         @false, --RecurringStartDay
         null, --EmailTemplateId
         @false, --RunStepsInParallel
         'SuccessOnly', --LinkedJobCondition
         @user,
         @user);

--Job definition step
insert into [dbo].[JobDefinitionStep](Id, JobDefinitionId, Sequence, Name, ObjectName, DeleteAction, DeleteActionFieldToSet, DeleteActionValueToSet, SkipHeaderRow, CreatedBy, ModifiedBy)
    values(@jobDefinitionStepId, --Id
          @jobDefinitionId, --JobDefinitionId
         1, --Sequence
         'Attribute Refresh', --Name
         'Attribute', --ObjectName
         'Ignore', --DeleteAction
         '', --DeleteActionFieldToSet
         '', --DeleteActionValueToSet
         @true, --SkipHeaderRow
         @user, --CreatedBy
         @user); --ModifiedBy
