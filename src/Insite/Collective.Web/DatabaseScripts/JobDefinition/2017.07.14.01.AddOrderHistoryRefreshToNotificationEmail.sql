﻿declare @orderHistory uniqueidentifier = '37CB86CF-B830-40EF-8193-D84F43335194';

declare @notifyEmail varchar(max) = 'nrobichaud@absolunet.com,bmoreau@absolunet.com,amarsolais@absolunet.com,mfilion@absolunet.com';

update [dbo].[JobDefinition]
    set NotifyEmail = @notifyEmail,
        NotifyCondition = 'Failure'
where Id = @orderHistory