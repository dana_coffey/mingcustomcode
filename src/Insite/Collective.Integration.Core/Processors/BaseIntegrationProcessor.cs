﻿using System.Data;
using System.Globalization;
using System.Threading;
using Collective.Integration.Core.Contexts;
using Insite.WIS.Broker;
using Insite.WIS.Broker.Interfaces;
using Insite.WIS.Broker.Plugins;
using Insite.WIS.Broker.WebIntegrationService;

namespace Collective.Integration.Core.Processors
{
    public abstract class BaseIntegrationProcessor : IIntegrationProcessor
    {
        public DataSet Execute(SiteConnection siteConnection, IntegrationJob integrationJob, JobDefinitionStep jobStep)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en");

            return ExecuteJob(new IntegrationProcessorContext
            {
                SiteConnection = siteConnection,
                IntegrationJob = integrationJob,
                JobStep = jobStep,
                Logger = new IntegrationJobLogger(siteConnection, integrationJob)
            });
        }

        protected abstract DataSet ExecuteJob(IntegrationProcessorContext context);
    }
}
