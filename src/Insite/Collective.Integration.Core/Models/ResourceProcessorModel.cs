﻿namespace Collective.Integration.Core.Models
{
    public class ResourceConnectorModel
    {
        public string Name { get; set; }
        public string Sha1Sum { get; set; }
        public string SourcePath { get; set; }
    }
}
