﻿namespace Collective.Integration.Core.Models
{
    public class ProductFieldConnectorModel
    {
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string DataType { get; set; }
        public bool ExcludedFromSpecifications { get; set; }
        public string FieldCvlId { get; set; }
        public string FieldId { get; set; }
        public string FieldName { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
