﻿namespace Collective.Integration.Core.Models
{
    public class CustomerConnectorModel
    {
        public string Addr { get; set; }
        public string Addr3 { get; set; }
        public string City { get; set; }
        public int Cono { get; set; }
        public string CountryCd { get; set; }
        public string CustNo { get; set; }
        public string CustType { get; set; }
        public string Email { get; set; }
        public string FaxPhoneNo { get; set; }
        public bool IsShipTo { get; set; }
        public bool IsTaxExempt { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string ShipTo { get; set; }
        public string ShipViaTy { get; set; }
        public string SlsRepOut { get; set; }
        public string SpecificWarehouse { get; set; }
        public string State { get; set; }
        public bool StatusType { get; set; }
        public string Whse { get; set; }
        public string ZipCd { get; set; }
    }
}
