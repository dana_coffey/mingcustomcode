﻿namespace Collective.Integration.Core.Models
{
    public class OrderHistoryAddOnConnectorModel
    {
        public decimal AddonNet { get; set; }
        public int AddonNo { get; set; }
        public int AddOnType { get; set; }
        public int SequenceNo { get; set; }
    }
}
