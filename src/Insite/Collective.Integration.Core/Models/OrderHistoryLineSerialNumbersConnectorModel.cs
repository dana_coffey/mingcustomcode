﻿namespace Collective.Integration.Core.Models
{
    public class OrderHistoryLineSerialNumbersConnectorModel
    {
        public string Key { get; set; }
        public int SeqNo { get; set; }
        public string SerialNo { get; set; }
    }
}
