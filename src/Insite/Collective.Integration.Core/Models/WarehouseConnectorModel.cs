﻿namespace Collective.Integration.Core.Models
{
    public class WarehouseConnectorModel
    {
        public string Addr { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string State { get; set; }
        public string WarehouseCustomer { get; set; }
        public string WarehouseStatus { get; set; }
        public string WarehouseType { get; set; }
        public string Whse { get; set; }
        public string ZipCd { get; set; }
    }
}
