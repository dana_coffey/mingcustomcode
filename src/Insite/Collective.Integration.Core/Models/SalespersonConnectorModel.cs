﻿namespace Collective.Integration.Core.Models
{
    public class SalespersonConnectorModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string SlsRep { get; set; }
    }
}
