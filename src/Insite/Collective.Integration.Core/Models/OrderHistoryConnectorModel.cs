﻿using System;
using System.Collections.Generic;

namespace Collective.Integration.Core.Models
{
    public class OrderHistoryConnectorModel
    {
        public List<OrderHistoryAddOnConnectorModel> AddOns { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public bool BoExistsFl { get; set; }
        public string City { get; set; }
        public int Cono { get; set; }
        public string CustNo { get; set; }
        public string CustomerEmail { get; set; }
        public string CustPo { get; set; }
        public DateTime? EnterDt { get; set; }
        public string EnterTm { get; set; }
        public string Key { get; set; }
        public List<OrderHistoryLineConnectorModel> Lines { get; set; }
        public string Name { get; set; }
        public string OrderDisp { get; set; }
        public int OrderNo { get; set; }
        public int OrderSuf { get; set; }
        public DateTime? PromiseDt { get; set; }
        public string Refer { get; set; }
        public DateTime? ReqShipDt { get; set; }
        public DateTime? ShipDt { get; set; }
        public string ShipTo { get; set; }
        public string ShipToAddr1 { get; set; }
        public string ShipToAddr2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToNm { get; set; }
        public string ShipToSt { get; set; }
        public string ShipToZip { get; set; }
        public string ShipViaTy { get; set; }
        public string SlsRepOut { get; set; }
        public decimal SpecDiscAmt { get; set; }
        public int StageCd { get; set; }
        public string State { get; set; }
        public decimal TaxAmt { get; set; }
        public string TermsType { get; set; }
        public decimal TotInvAmt { get; set; }
        public decimal TotInvOrd { get; set; }
        public decimal TotLineAmt { get; set; }
        public decimal TotLineOrd { get; set; }
        public string TransType { get; set; }
        public string Warehouse { get; set; }
        public string WebOrderNumber { get; set; }
        public string ZipCd { get; set; }
    }
}
