﻿using System.Collections.Generic;

namespace Collective.Integration.Core.Models
{
    public class OrderHistoryLineConnectorModel
    {
        public decimal CoreCharge { get; set; }
        public string CoreChgTy { get; set; }
        public string Descrip { get; set; }
        public string Key { get; set; }
        public int LineNo { get; set; }
        public decimal NetAmt { get; set; }
        public decimal NetOrd { get; set; }
        public string NoteLn { get; set; }
        public decimal Price { get; set; }
        public string ProdDesc { get; set; }
        public string ProdDesc2 { get; set; }
        public int QtyOrd { get; set; }
        public int QtyShip { get; set; }
        public List<OrderHistoryLineSerialNumbersConnectorModel> SerialNumbers { get; set; }
        public string ShipProd { get; set; }
        public string SpecNsType { get; set; }
        public string Unit { get; set; }
    }
}
