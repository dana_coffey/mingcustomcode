﻿using Insite.WIS.Broker;
using Insite.WIS.Broker.Plugins;
using Insite.WIS.Broker.WebIntegrationService;

namespace Collective.Integration.Core.Contexts
{
    public class IntegrationProcessorContext
    {
        public IntegrationJob IntegrationJob { get; set; }
        public JobDefinitionStep JobStep { get; set; }
        public IIntegrationJobLogger Logger { get; set; }
        public SiteConnection SiteConnection { get; set; }
    }
}
