﻿using System;
using System.Linq;
using Collective.Core.Helpers;
using Insite.WIS.Broker.WebIntegrationService;

namespace Collective.Integration.Core.Helpers
{
    public static class IntegrationJobHelper
    {
        public static DateTime GetParameterValue(IntegrationJob job, string parameterName, DateTime defaultTo)
        {
            var parameter = GetParameter(job, parameterName);
            return parameter != null ? ParseHelper.ParseDateTime(parameter.Value, defaultTo) : defaultTo;
        }

        public static int GetParameterValue(IntegrationJob job, string parameterName, int defaultTo)
        {
            var parameter = GetParameter(job, parameterName);
            return parameter != null ? ParseHelper.ParseInt(parameter.Value, defaultTo) : defaultTo;
        }

        public static bool GetParameterValue(IntegrationJob job, string parameterName, bool defaultTo)
        {
            var parameter = GetParameter(job, parameterName);
            return parameter != null ? ParseHelper.ParseBoolean(parameter.Value, defaultTo) : defaultTo;
        }

        public static string GetParameterValue(IntegrationJob job, string parameterName, string defaultTo = null)
        {
            var parameter = GetParameter(job, parameterName);
            return parameter?.Value ?? defaultTo;
        }

        private static IntegrationJobParameter GetParameter(IntegrationJob job, string parameterName)
        {
            return job.IntegrationJobParameters.FirstOrDefault(x => x.JobDefinitionParameter?.Name == parameterName || x.JobDefinitionStepParameter?.Name == parameterName);
        }
    }
}
