﻿using Collective.Core.Constant;

namespace Collective.Integration.Core.Helpers
{
    public static class ConnectionHelper
    {
        public static string GetInforSxeConnectionString()
        {
            return string.Concat($"DRIVER={Constants.AppSettings.Connector.InforSxe.Driver}; ",
                $"HOST={Constants.AppSettings.Connector.InforSxe.Hostname}; ",
                $"PORT={Constants.AppSettings.Connector.InforSxe.Port}; ",
                $"DB={Constants.AppSettings.Connector.InforSxe.Database}; ",
                $"UID={Constants.AppSettings.Connector.InforSxe.UserId}; ",
                $"PWD={Constants.AppSettings.Connector.InforSxe.Password}");
        }

        public static string GetInRiverConnectionString()
        {
            return Constants.AppSettings.Connector.Inriver.ConnectionString;
        }
    }
}
