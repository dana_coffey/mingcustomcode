﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;

namespace Collective.Integration.Core.Helpers
{
    public static class CommandHelper
    {
        public static List<TModel> ExecuteOdbc<TModel>(string sql, string connectionString)
            where TModel : new()
        {
            var connection = new OdbcConnection(connectionString);
            return Execute<TModel, OdbcConnection, OdbcCommand>(connection, new OdbcCommand(sql, connection));
        }

        public static List<TModel> ExecuteSql<TModel>(string sql, string connectionString)
            where TModel : new()
        {
            var connection = new SqlConnection(connectionString);
            return Execute<TModel, SqlConnection, SqlCommand>(connection, new SqlCommand(sql, connection));
        }

        private static List<TModel> Execute<TModel, TConnection, TCommand>(TConnection connection, TCommand command)
            where TModel : new()
            where TConnection : DbConnection
            where TCommand : DbCommand
        {
            try
            {
                var results = new List<TModel>();

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var fieldsList = Enumerable.Range(0, reader.FieldCount).Select(x => reader.GetName(x)).ToList();

                    while (reader.Read())
                    {
                        var item = ToModel<TModel>(reader, fieldsList);
                        if (item != null)
                        {
                            results.Add(item);
                        }
                    }
                }

                return results;
            }
            finally
            {
                command.Dispose();

                connection.Close();
                connection.Dispose();
            }
        }

        private static object GetDefault(Type type)
        {
            if (type == typeof(string))
            {
                return string.Empty;
            }

            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        private static Type GetNullableUnderlyingTypeIfNeeded(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return Nullable.GetUnderlyingType(type);
            }

            return type;
        }

        private static T ToModel<T>(DbDataReader reader, List<string> fieldsList) where T : new()
        {
            var model = new T();
            foreach (var property in typeof(T).GetProperties())
            {
                object value;

                if (fieldsList.Contains(property.Name, StringComparer.InvariantCultureIgnoreCase))
                {
                    value = reader.IsDBNull(reader.GetOrdinal(property.Name)) ? GetDefault(property.PropertyType) : Convert.ChangeType(reader[property.Name], GetNullableUnderlyingTypeIfNeeded(property.PropertyType));
                }
                else
                {
                    value = GetDefault(property.PropertyType);
                }

                property.SetMethod.Invoke(model, new[] { value });
            }

            return model;
        }
    }
}
