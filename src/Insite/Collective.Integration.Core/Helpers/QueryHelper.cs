﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Collective.Integration.Core.Helpers
{
    public static class QueryHelper
    {
        public static string GetQueryFromEmbeddedFile(string resourceName)
        {
            var assembly = Assembly.GetCallingAssembly();
            var fullResourceName = string.Concat(assembly.GetName().Name, ".Connectors.Sql.", resourceName);

            using (var stream = assembly.GetManifestResourceStream(fullResourceName))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static string ReplaceTags(string query, Dictionary<string, string> tags)
        {
            foreach (var tag in tags)
            {
                query = query.Replace(tag.Key, tag.Value);
            }

            return query;
        }
    }
}
