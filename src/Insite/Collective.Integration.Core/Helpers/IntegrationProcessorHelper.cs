﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using Collective.Core.Constant;
using Collective.Integration.Core.Contexts;
using Newtonsoft.Json;

namespace Collective.Integration.Core.Helpers
{
    public static class IntegrationProcessorHelper
    {
        public static string BuildJsonFilePath(string fileName)
        {
            return Path.Combine(Constants.AppSettings.Connector.Inriver.FtpPath, Constants.AppSettings.Connector.Folder.Queue, fileName);
        }

        public static DataSet ToJsonDataSet<TModel>(IEnumerable<TModel> items, IntegrationProcessorContext context)
        {
            return ToJsonDataSet(items, context.JobStep.Sequence, context.JobStep.ObjectName);
        }

        public static DataSet ToJsonDataSet<TModel>(IEnumerable<TModel> items, int sequence, string jobStepName)
        {
            var dataset = new DataSet();
            var table = new DataTable($"{sequence}{jobStepName}");
            table.Columns.Add(new DataColumn(Constants.Jobs.DatasetJsonPropertyName));

            foreach (var item in items)
            {
                var row = table.NewRow();
                row[Constants.Jobs.DatasetJsonPropertyName] = JsonConvert.SerializeObject(item);
                table.Rows.Add(row);
            }

            dataset.Tables.Add(table);
            return dataset;
        }
    }
}
