﻿using System;

namespace Collective.Integration.Core.Helpers
{
    public static class DateTimeHelper
    {
        public static string ToSqlFormat(DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }
    }
}
