﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Collective.Integration.Core.Helpers
{
    public static class JsonHelper
    {
        public static List<T> FromFile<T>(string file) where T : class
        {
            return FromValue<T>(File.ReadAllText(file));
        }

        public static List<T> FromValue<T>(string value) where T : class
        {
            return JsonConvert.DeserializeObject<List<T>>(value);
        }
    }
}
