﻿using System;

namespace Collective.Core.Helpers
{
    public static class StringHelper
    {
        public static string NewlineToBr(string html)
        {
            if (string.IsNullOrWhiteSpace(html))
            {
                return null;
            }

            return html.Replace(Environment.NewLine, "<br />").Replace("\n", "<br />");
        }

        public static string RemoveWhitespaces(string value)
        {
            return value.Replace(" ", string.Empty);
        }
    }
}
