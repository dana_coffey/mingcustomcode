﻿using System.Collections.Specialized;
using System.Configuration;
using Collective.Core.Constant;

namespace Collective.Core.Helpers
{
    public static class AppSettingHelper
    {
        public static string GetAppSetting(string name)
        {
            return GetAppSetting(name, string.Empty);
        }

        public static string GetAppSetting(string name, string defaultTo)
        {
            return ConfigurationManager.AppSettings.Get(name) ?? defaultTo;
        }

        public static int GetAppSetting(string name, int defaultTo)
        {
            int i;
            return int.TryParse(GetAppSetting(name), out i) ? i : defaultTo;
        }

        public static string GetCollectiveAppSetting(string name)
        {
            return GetCollectiveAppSetting(name, string.Empty);
        }

        public static string GetCollectiveAppSetting(string name, string defaultTo)
        {
            return ((NameValueCollection)ConfigurationManager.GetSection(Constants.AppSettings.ConfigurationSection.CollectiveAppSettings))[name] ?? defaultTo;
        }

        public static int GetCollectiveAppSetting(string name, int defaultTo)
        {
            int i;
            return int.TryParse(GetCollectiveAppSetting(name), out i) ? i : defaultTo;
        }
    }
}
