﻿using System;
using System.Globalization;

namespace Collective.Core.Helpers
{
    public static class ParseHelper
    {
        public static bool? ParseBoolean(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return bool.TryParse(value, out var result) ? result : (bool?)null;
            }

            return null;
        }

        public static bool ParseBoolean(string value, bool defaultTo)
        {
            return ParseBoolean(value).GetValueOrDefault(defaultTo);
        }

        public static DateTime ParseDateTime(string value, DateTime defaultTo)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return DateTime.TryParse(value, out var result) ? result : defaultTo;
            }

            return defaultTo;
        }

        public static DateTimeOffset? ParseDateTimeOffset(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return DateTimeOffset.TryParse(value, out var result) ? result : (DateTimeOffset?)null;
            }

            return null;
        }

        public static decimal? ParseDecimal(string value)
        {
            return ParseDecimal(value, new CultureInfo("en-CA"));
        }

        public static decimal? ParseDecimal(string value, CultureInfo culture)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return decimal.TryParse(value, NumberStyles.Number, culture, out var result) ? result : (decimal?)null;
            }

            return null;
        }

        public static double? ParseDouble(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return double.TryParse(value, NumberStyles.Number, new CultureInfo("en-CA"), out var result) ? result : (double?)null;
            }

            return null;
        }

        public static int ParseInt(string value, int defaultTo)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return int.TryParse(value, NumberStyles.Number, new CultureInfo("en-CA"), out var result) ? result : defaultTo;
            }

            return defaultTo;
        }
    }
}
