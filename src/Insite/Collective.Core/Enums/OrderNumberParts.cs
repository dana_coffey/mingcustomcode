﻿namespace Collective.Core.Enums
{
    public enum OrderNumberParts
    {
        OrderNumber,
        Suffix
    }
}
