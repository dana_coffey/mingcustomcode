﻿using System.Reflection;

namespace Collective.Core.Injector.Models
{
    public class CollectiveInjectorResourceModel : CollectiveInjectorItemModel
    {
        public Assembly Assembly { get; set; }
        public string FullName { get; set; }
    }
}
