﻿namespace Collective.Core.Injector.Models
{
    public abstract class CollectiveInjectorItemModel
    {
        public int Order { get; set; }
    }
}
