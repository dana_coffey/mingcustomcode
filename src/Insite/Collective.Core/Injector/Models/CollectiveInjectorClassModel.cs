﻿using System;

namespace Collective.Core.Injector.Models
{
    public class CollectiveInjectorClassModel : CollectiveInjectorItemModel
    {
        public Type Type { get; set; }
    }
}
