﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Collective.Core.Injector.Models;

namespace Collective.Core.Injector
{
    public static class CollectiveInjector
    {
        private static readonly List<CollectiveInjectorResourceModel> Resources = new List<CollectiveInjectorResourceModel>();
        private static readonly List<CollectiveInjectorClassModel> Types = new List<CollectiveInjectorClassModel>();
        private static bool isRegisteryDone;

        public static T GetInstance<T>(params object[] args) where T : class
        {
            var matchingType = GetMatchingTypes<T>().FirstOrDefault();
            return matchingType != null ? (T)Activator.CreateInstance(matchingType.Type, args) : null;
        }

        public static List<T> GetInstances<T>(params object[] args) where T : class
        {
            return GetMatchingTypes<T>().Select(p => (T)Activator.CreateInstance(p.Type, args)).ToList();
        }

        public static string GetResource(string resourceName)
        {
            if (!isRegisteryDone)
            {
                Register();
            }

            var resource = Resources.Where(x => x.FullName.EndsWith(resourceName, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.Order).FirstOrDefault();
            if (resource != null)
            {
                using (var stream = resource.Assembly.GetManifestResourceStream(resource.FullName))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            return string.Empty;
        }

        private static IOrderedEnumerable<CollectiveInjectorClassModel> GetMatchingTypes<T>() where T : class
        {
            if (!isRegisteryDone)
            {
                Register();
            }

            var requiredType = typeof(T);
            var matchingTypes = Types.Where(p => requiredType.IsAssignableFrom(p.Type)).OrderBy(p => p.Order);
            return matchingTypes;
        }

        private static int GetOrder(Assembly assembly)
        {
            var assemblyName = assembly.FullName.ToLower();
            if (assemblyName.Contains("ming"))
            {
                return 500;
            }

            return 1000;
        }

        private static void Register()
        {
            var injectableType = typeof(ICollectiveInjectableClass);

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Where(p => p.FullName.ToLower().StartsWith("collective.")))
            {
                var order = GetOrder(assembly);

                Resources.AddRange(assembly.GetManifestResourceNames().Select(p => new CollectiveInjectorResourceModel
                {
                    Assembly = assembly,
                    FullName = p,
                    Order = order
                }));

                var types = assembly.GetTypes().Where(p => injectableType.IsAssignableFrom(p));
                Types.AddRange(types.Select(p => new CollectiveInjectorClassModel
                {
                    Order = order,
                    Type = p
                }));
            }

            isRegisteryDone = true;
        }
    }
}
