﻿namespace Collective.Core.Models
{
    public class SalespersonProcessorModel
    {
        public string Email { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
