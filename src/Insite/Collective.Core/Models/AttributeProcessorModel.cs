﻿using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class AttributeProcessorModel
    {
        public string AttributeTypeName { get; set; }
        public Dictionary<string, string> OptionDescription { get; set; }
        public bool OptionIsEnabled { get; set; }
        public string OptionName { get; set; }
    }
}
