﻿using System;
using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class CategoryProcessorModel
    {
        public List<string> Images { get; set; }
        public bool IsEnabled { get; set; }
        public string Name { get; set; }
        public string ParentPath { get; set; }
        public string Path { get; set; }
        public string PreviousPath { get; set; }
        public Dictionary<string, string> ShortDescription { get; set; }
        public int SortOrder { get; set; }
        public Guid WebsiteId { get; set; }
    }
}
