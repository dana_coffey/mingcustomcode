﻿namespace Collective.Core.Models
{
    public class ResourceProcessorModel
    {
        public string Name { get; set; }
        public string Sha1Sum { get; set; }
    }
}
