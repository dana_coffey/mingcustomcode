﻿using System;
using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class OrderHistoryProcessorModel
    {
        public string BillToAddress1 { get; set; }
        public string BillToAddress2 { get; set; }
        public string BillToCity { get; set; }
        public string BillToCompanyName { get; set; }
        public string BillToPostalCode { get; set; }
        public string BillToState { get; set; }
        public int CompanyNumber { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerPo { get; set; }
        public string CustomerReference { get; set; }
        public string CustomerSequence { get; set; }
        public decimal DiscountAmount { get; set; }
        public string ErpOrderNumber { get; set; }
        public string ErpOrderSuffix { get; set; }
        public List<OrderHistoryLineProcessorModel> Lines { get; set; }
        public DateTime? OrderDate { get; set; }
        public string OrderDisposition { get; set; }
        public string OrderStatus { get; set; }
        public decimal OrderTotal { get; set; }
        public decimal OtherCharges { get; set; }
        public decimal ProductTotal { get; set; }
        public DateTime? PromiseDate { get; set; }
        public DateTime? RequestedShipDate { get; set; }
        public string SalesRepresentative { get; set; }
        public string ShipCode { get; set; }
        public DateTime? ShipDate { get; set; }
        public decimal ShippingCharges { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToState { get; set; }
        public int? StageCode { get; set; }
        public decimal TaxAmount { get; set; }
        public string Terms { get; set; }
        public string Warehouse { get; set; }
        public string WebOrderNumber { get; set; }
    }
}
