﻿namespace Collective.Core.Models
{
    public class WarehouseProcessorModel
    {
        public string Address { get; set; }
        public bool CanPickUp { get; set; }
        public bool CanShip { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public string WarehouseCustomer { get; set; }
        public string WarehouseId { get; set; }
        public string WarehouseType { get; set; }
        public string ZipCode { get; set; }
    }
}
