﻿using System.Net;
using Collective.Core.Constant;
using Collective.Core.Extensions;

namespace Collective.Core.Models
{
    public class InvoiceDownloadModel
    {
        public bool CanDownloadInvoice => !string.IsNullOrEmpty(InvoiceUrl) && OrderStatus.In(Constants.Insite.Order.Status.Complete, Constants.Insite.Order.Status.Invoiced, Constants.Insite.Order.Status.Cancelled);
        public bool CanDownloadProofOfDelivery => !string.IsNullOrEmpty(ProofOfDeliveryUrl) && OrderStatus.In(Constants.Insite.Order.Status.Complete, Constants.Insite.Order.Status.Invoiced, Constants.Insite.Order.Status.Cancelled);
        public string InvoiceUrl { get; set; }
        public string OrderStatus { get; set; }
        public string ProofOfDeliveryUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
