﻿namespace Collective.Core.Models
{
    public class SerialNumberProcessorModel
    {
        public int SeqNo { get; set; }
        public string SerialNo { get; set; }
    }
}
