﻿using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class OrderHistoryLineProcessorModel
    {
        public decimal LineNumber { get; set; }
        public decimal LineTotal { get; set; }
        public string Notes { get; set; }
        public string ProductDescription { get; set; }
        public string ProductNumber { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyShipped { get; set; }
        public string UnitOfMeasure { get; set; }
        public decimal UnitPrice { get; set; }
        public List<SerialNumberProcessorModel> SerialNumbers { get; set; }
    }
}
