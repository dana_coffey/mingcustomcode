﻿using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class ProductFieldProcessorModel
    {
        public string CategoryId { get; set; }
        public Dictionary<string, string> CategoryName { get; set; }
        public string DataType { get; set; }
        public bool ExcludedFromSpecifications { get; set; }
        public string FieldCvlId { get; set; }
        public string FieldId { get; set; }
        public Dictionary<string, string> FieldName { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
