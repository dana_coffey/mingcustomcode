﻿namespace Collective.Core.Models
{
    public class OrderLocationModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryId { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string StateId { get; set; }
    }
}
