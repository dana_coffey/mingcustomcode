﻿namespace Collective.Core.Models
{
    public class ProductSupersedeStatusProcessorModel
    {
        public string ErpNumber { get; set; }
        public bool IsCatalogProduct { get; set; }
    }
}
