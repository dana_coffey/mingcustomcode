﻿using System.Collections.Generic;

namespace Collective.Core.Models
{
    public class ProductProcessorModel
    {
        public List<string> Categories { get; set; }
        public Dictionary<string, Dictionary<string, string>> CustomFields { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public Dictionary<string, string> MetaDescription { get; set; }
        public List<string> Documents { get; set; }
        public string ErpNumber { get; set; }
        public List<string> Images { get; set; }
        public bool IsEnabled { get; set; }
        public List<ProductLinkProcessorModel> Links { get; set; }
        public string ManufacturerItem { get; set; }
        public Dictionary<string, string> Name { get; set; }
    }
}
