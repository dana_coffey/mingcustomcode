﻿namespace Collective.Core.Models
{
    public class CustomerProcessorModel
    {
        public bool Active { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string CustomerType { get; set; }
        public string Email { get; set; }
        public bool IsBillTo { get; set; }
        public bool IsShipTo { get; set; }
        public bool IsTaxExempt { get; set; }
        public string Number { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string SalespersonNumber { get; set; }
        public string ShipTo { get; set; }
        public string SpecificWarehouse { get; set; }
        public string State { get; set; }
        public string Warehouse { get; set; }
    }
}
