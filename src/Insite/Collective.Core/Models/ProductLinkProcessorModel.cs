﻿namespace Collective.Core.Models
{
    public class ProductLinkProcessorModel
    {
        public string ErpNumber { get; set; }
        public string Type { get; set; }
    }
}
