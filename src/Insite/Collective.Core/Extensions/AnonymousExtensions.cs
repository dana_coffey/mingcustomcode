﻿using System.Linq;

namespace Collective.Core.Extensions
{
    public static class AnonymousExtensions
    {
        public static bool In<T>(this T x, params T[] set)
        {
            return set.Contains(x);
        }
    }
}
