﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Collective.Core.Enums;

namespace Collective.Core.Extensions
{
    public static class StringExtensions
    {
        public static string AddParam(this string queryString, string paramName, object value)
        {
            queryString = queryString.RemoveParam(paramName);

            if (queryString.Contains('?'))
            {
                return string.Concat(queryString, "&", paramName, "=", value);
            }

            return string.Concat(queryString, "?", paramName, "=", value);
        }

        public static string Capitalize(this string str)
        {
            if (str.Length == 0)
            {
                return str;
            }

            return str.Substring(0, 1).ToUpper() + str.Substring(1);
        }

        public static string ClearParams(this string queryString)
        {
            var indexOfInterrogation = queryString.IndexOf('?');

            if (indexOfInterrogation > -1)
            {
                return queryString.Substring(0, indexOfInterrogation);
            }

            return queryString;
        }

        public static string DecodeBase64(this string text)
        {
            return new UTF8Encoding().GetString(Convert.FromBase64String(text));
        }

        public static string EncodeBase64(this string text)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
        }

        public static string GetOrderNumberPart(this string erpOrderNumber, OrderNumberParts part)
        {
            switch (part)
            {
                case OrderNumberParts.OrderNumber:
                    return Convert.ToInt32(erpOrderNumber.Split('-')[0]).ToString();
                case OrderNumberParts.Suffix:
                    return erpOrderNumber.Split('-')[1];
                default:
                    return erpOrderNumber;
            }
        }

        public static string HtmlDecode(this string text)
        {
            return HttpUtility.HtmlDecode(text);
        }

        public static string HtmlEncode(this string text)
        {
            return HttpUtility.HtmlEncode(text);
        }

        public static string NormalizeName(this string name, int length)
        {
            if (string.IsNullOrEmpty(name))
            {
                return NewNormalizedName();
            }

            var buffer = name.Normalize(NormalizationForm.FormD).ToCharArray();
            buffer = Array.FindAll(buffer, c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark && (char.IsLetterOrDigit(c) || c == ' '));
            var newName = new string(buffer).Substring(0, Math.Min(length, buffer.Length)).Trim();

            return string.IsNullOrEmpty(newName) ? NewNormalizedName() : newName;
        }

        public static string RemoveHtmlTags(this string source)
        {
            var array = new char[source.Length];
            var arrayIndex = 0;
            var inside = false;

            foreach (var c in source)
            {
                if (c == '<')
                {
                    inside = true;
                    continue;
                }

                if (c == '>')
                {
                    inside = false;
                    continue;
                }

                if (!inside)
                {
                    array[arrayIndex] = c;
                    arrayIndex++;
                }
            }

            return new string(array, 0, arrayIndex);
        }

        public static string RemoveParam(this string queryString, string paramName)
        {
            var indexOfInterrogation = queryString.IndexOf('?');

            if (indexOfInterrogation > -1)
            {
                var indexOfParam = queryString.IndexOf(paramName + "=", indexOfInterrogation, StringComparison.OrdinalIgnoreCase);

                if (indexOfParam > 0)
                {
                    var index = indexOfParam - 1;
                    var previousChar = queryString[index];
                    var nextParamIndex = queryString.IndexOf('&', indexOfParam);
                    var hasMoreParams = false;

                    if (nextParamIndex == -1)
                    {
                        nextParamIndex = queryString.Length + 1;
                    }
                    else
                    {
                        hasMoreParams = true;
                        nextParamIndex++;
                    }

                    if (previousChar == '?' && hasMoreParams)
                    {
                        queryString = queryString.Remove(indexOfParam, nextParamIndex - indexOfParam);
                    }
                    else
                    {
                        queryString = queryString.Remove(index, nextParamIndex - indexOfParam);
                    }
                }
            }

            return queryString;
        }

        public static string SafeSubstring(this string source, int length)
        {
            return source.SafeSubstring(0, length);
        }

        public static string SafeSubstring(this string source, int startIndex, int length)
        {
            return source.Length > length ? source.Substring(startIndex, length) : source;
        }

        public static string ToAccentInsensitive(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            str = str.Normalize(NormalizationForm.FormD);
            var sb = new StringBuilder();

            foreach (var c in str)
            {
                var uc = CharUnicodeInfo.GetUnicodeCategory(c);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(c);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC).Replace("&", string.Empty);
        }

        public static string ToTitleCase(this string str)
        {
            return str.Length == 0 ? str : CultureInfo.InvariantCulture.TextInfo.ToTitleCase(str);
        }

        public static string UrlDecode(this string source)
        {
            return HttpContext.Current.Server.UrlDecode(source);
        }

        public static string UrlDecodeBase64(this string text)
        {
            return HttpUtility.UrlDecode(text.DecodeBase64());
        }

        public static string UrlEncode(this string source)
        {
            return HttpContext.Current.Server.UrlEncode(source);
        }

        public static string UrlEncodeBase64(this string text)
        {
            return HttpUtility.UrlEncode(text.EncodeBase64());
        }

        private static string NewNormalizedName()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty);
        }
    }
}
