﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class CustomProperties
        {
            public static class Cart
            {
                public const string CustomerOrderPropertyTableName = "CustomerOrderProperty";

                public const string CustomerReference = "customerReference";
                public const string Location = "location";
                public const string OrderSubmitErrorReferenceNo = "orderSubmitErrorReferenceNo";
                public const string ShipComplete = "shipComplete";
                public const string Warehouse = "warehouse";
            }

            public static class Customer
            {
                public const string CustomShippingAddressCartId = "customShippingAddressCartId";
                public const string SpecificWarehouse = "CustomerSpecificWarehouse";
            }

            public static class Order
            {
                public const string CustomerEmail = "customerEmail";
                public const string CustomerReference = "customerReference";
                public const string Location = "location";
                public const string StageCode = "stageCode";
                public const string MyStore = "myStore";
                public const string ShipComplete = "shipComplete";
                public const string SubAccount = "subAccount";
                public const string Warehouse = "warehouse";
            }

            public static class OrderHistory
            {
                public const string CustomerReference = "customerReference";
                public const string ErpLineNumber = "erpLineNumber";
            }

            public static class Product
            {
                public const string CustomFields = "customFields";
                public const string IsCatalogProduct = "IsCatalogProduct";
                public const string IsSouthCarolinaSolidWasteTaxable = "IsSouthCarolinaSolidWasteTaxable";
                public const string StatusField = "Status";

                public static class Status
                {
                    public const string None = "None";
                    public const string Obsolete = "Obsolete";
                    public const string Superseded = "Superseded";
                    public const string SupersededWithInventory = "Superseded With Inventory";
                }
            }

            public static class ShipVia
            {
                public const string ErpShipViaCode = "ERPShipViaCode";
                public const string IsPickup = "IsPickup";
            }

            public static class Warehouse
            {
                public const string CanPickUp = "CanPickUp";
                public const string CanShip = "CanShip";
                public const string Email = "Email";
                public const string Latitude = "Latitude";
                public const string Longitude = "Longitude";
                public const string WarehouseCustomer = "WarehouseCustomer";
                public const string WarehouseType = "WarehouseType";
            }
        }
    }
}
