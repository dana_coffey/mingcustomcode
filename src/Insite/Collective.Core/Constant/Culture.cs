﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Culture
        {
            public const string DefaultCulture = "en";
            public const string InvariantCulture = "";
        }
    }
}
