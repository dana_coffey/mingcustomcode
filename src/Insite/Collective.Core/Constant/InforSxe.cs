﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class InforSxe
        {
            public const int CompanyNumber = 1;
            public const string DefaultCountryIsoCode = "US";
            public const string DefaultCurrencyCode = "US";

            public static class Order
            {
                public static class AddonSequenceNo
                {
                    public const int ShippingAndHandling = 2;
                }

                public static class StageCode
                {
                    public const int Cancelled = 9;
                    public const int Complete = 5;
                    public const int Processing2 = 2;
                    public const int Processing3 = 3;
                    public const int Processing4 = 4;
                    public const int Quoted = 0;
                    public const int Submitted = 1;
                }

                public static class TransactionType
                {
                    public const string Return = "RM";
                }
            }
        }
    }
}
