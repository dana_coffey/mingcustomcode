﻿using System;

namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Jobs
        {
            public const string DatasetJsonPropertyName = "json";
            public const int DefaultDaysBack = -5;

            public class CustomerRefresh
            {
                public class Steps
                {
                    public const int CustomerBillToStepSequence = 1;
                    public const int CustomerShipToStepSequence = 2;
                }
            }

            public class OrderHistoryRefresh
            {
                public static readonly Guid JobDefinitionId = new Guid("37cb86cf-b830-40ef-8193-d84f43335194");

                public static class JobDefinitionParameter
                {
                    public static readonly Guid ErpOrderNumber = new Guid("1153cd9c-be53-47c6-9672-c8d8c12e8bc2");
                    public static readonly Guid ErpOrderSuffix = new Guid("93c426e9-7d24-45d7-9651-a63f074c658e");
                    public static readonly Guid UpdatedSince = new Guid("2aa4bade-433d-478b-8b06-c6b77e7de4ce");
                    public static readonly Guid UpdatedTo = new Guid("aa51e99f-9811-476c-92f0-c6c0ca3f5b7c");
                }
            }

            public class OrderSubmit
            {
                public static readonly Guid StepErpShipToPrefixId = new Guid("E4084CD5-FCC2-E711-A9DF-28F0762A7561");

                public class DataSet
                {
                    public static class Columns
                    {
                        public const string ApiErrorCode = "ApiErrorCode";
                        public const string ApiErrorMessage = "ApiErrorMessage";
                        public const string ErpOrderNumber = "ErpOrderNumber";
                        public const string JobReferenceNumber = "JobReferenceNumber";
                        public const string QtyShipped = "QtyShipped";
                    }

                    public static class Tables
                    {
                        public const string OrderSubmit = "OrderSubmit";
                        public const string OrderSubmitItems = "OrderSubmitItems";
                        public const string SouthCarolinaSolidWasteTax = "SouthCarolinaSolidWasteTax";
                    }
                }
            }

            public class Parameters
            {
                public const string CustomerNumber = "CustomerNumber";
                public const string ErpOrderNumber = "ErpOrderNumber";
                public const string ErpOrderSuffix = "ErpOrderSuffix";
                public const string UpdatedSince = "UpdatedSince";
                public const string UpdatedTo = "UpdatedTo";
            }
        }
    }
}
