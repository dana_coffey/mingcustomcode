﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Roles
        {
            public const string PointOfSale = "MING_PointOfSale";
            public const string Technician = "MING_Technician";
        }
    }
}
