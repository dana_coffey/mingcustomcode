﻿using System;

namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Ids
        {
            public static class Country
            {
                public static readonly Guid UnitedStates = new Guid("8aac0470-e310-e311-ba31-d43d7e4e88b2");
            }

            public static class States
            {
                public static readonly Guid SouthCarolina = new Guid("03c36795-e510-e311-ba31-d43d7e4e88b2");
            }
        }
    }
}
