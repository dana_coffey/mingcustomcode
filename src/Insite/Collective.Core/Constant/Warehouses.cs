﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Warehouses
        {
            public static class StatusFlags
            {
                public const string CanDisplayOnWeb = "W";
                public const string CanPickUp = "P";
                public const string CanShip = "D";
            }

            public static class WarehouseType
            {
                public const string Branch = "Branch";
                public const string MMI = "MMI";
            }
        }
    }
}
