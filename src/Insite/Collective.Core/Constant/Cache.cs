﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Cache
        {
            public const string InventoryApi = "collective_inventoryapi_";
            public const string ItemCache = "collective_itemcache_";
            public const string MaxRecall = "collective_maxrecall_";
            public const string PartsListApi = "collective_partslistapi_";
            public const string PricingApi = "collective_pricingapi_";
            public const string ProductField = "collective_productfield";
        }
    }
}
