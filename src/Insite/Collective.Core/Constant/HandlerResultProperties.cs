﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class HandlerResultProperties
        {
            public static class Account
            {
                public const string AvailableRoles = "availableRoles";
                public const string IsDeactivated = "isDeactivated";
            }

            public static class Cart
            {
                public const string AvailabilityStatus = "availabilityStatus";
                public const string CanShowPrice = "canShowPrice";
                public const string CartHasRestrictedProducts = "cartHasRestrictedProducts";
                public const string CartId = "cartId";
                public const string ErpOrderNumber = "erpOrderNumber";
                public const string HasSouthCarolinaSolidWasteTax = "hasSouthCarolinaSolidWasteTax";
                public const string IsTaxExempt = "isTaxExempt";
                public const string NoTaxCalculation = "noTaxCalculation";
                public const string ShipComplete = "shipComplete";
                public const string ShipViaIsPickup = "shipViaIsPickup";
                public const string SouthCarolinaSolidWasteTaxAmount = "southCarolinaSolidWasteTaxAmount";
            }

            public static class Order
            {
                public const string CanShowPrice = "canShowPrice";
                public const string CustomerEmail = "customerEmail";
                public const string HasInvoice = "hasInvoice";
                public const string HasProofOfDelivery = "hasProofOfDelivery";
                public const string ShipViaIsPickup = "shipViaIsPickup";
                public const string Warehouse = "warehouse";
            }

            public static class OrderLine
            {
                public const string SerialNumbers = "serialNumbers";
            }

            public static class Product
            {
                public const string AvailabilityStatus = "availabilityStatus";
                public const string CustomFields = "customFields";
                public const string ShowPartsTab = "showPartsTab";
                public const string HideAvailability = "hideAvailability";
                public const string IsObsolete = "isObsolete";
                public const string IsReplacement = "isReplacement";
                public const string IsSuperseded = "isSuperseded";
                public const string ReplacementFor = "replacementFor";
                public const string SupersededBy = "supersededBy";
            }

            public static class Session
            {
                public const string DefaultCustomerIsBillTo = "DefaultCustomerIsBillTo";
                public const string DefaultCustomerIsShipTo = "DefaultCustomerIsShipTo";
                public const string UserRolesDisplay = "userRolesDisplay";
                public const string Warehouse = "warehouse";
            }

            public static class ShipToCollection
            {
                public const string HasShipTos = "hasShipTos";
            }
        }
    }
}
