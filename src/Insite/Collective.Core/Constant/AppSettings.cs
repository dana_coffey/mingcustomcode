﻿using Collective.Core.Helpers;

namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class AppSettings
        {
            public static class CarrierApi
            {
                public static readonly string ApiPassword = AppSettingHelper.GetCollectiveAppSetting("api:carrier:password");
                public static readonly string ApiUsername = AppSettingHelper.GetCollectiveAppSetting("api:carrier:user");
                public static readonly string BearerTokenPassword = AppSettingHelper.GetCollectiveAppSetting("api:carrier:bearer:password");
                public static readonly string BearerTokenUsername = AppSettingHelper.GetCollectiveAppSetting("api:carrier:bearer:user");
                public static readonly string Url = AppSettingHelper.GetCollectiveAppSetting("api:carrier:url");
            }

            public static class ConfigurationSection
            {
                public const string CollectiveAppSettings = "collectiveAppSettings";
            }

            public static class Connector
            {
                public static class Archive
                {
                    public static readonly int ArchiveLifetime = AppSettingHelper.GetAppSetting("archive:lifetime:days", 30);
                }

                public static class Folder
                {
                    public static readonly string Archives = AppSettingHelper.GetAppSetting("folder:name:archives");
                    public static readonly string ImageResources = AppSettingHelper.GetAppSetting("folder:name:resources:images");
                    public static readonly string OtherResources = AppSettingHelper.GetAppSetting("folder:name:resources:others");
                    public static readonly string Queue = AppSettingHelper.GetAppSetting("folder:name:queue");
                    public static readonly string Resources = AppSettingHelper.GetAppSetting("folder:name:resources");
                    public static readonly string ResourcesDestination = AppSettingHelper.GetCollectiveAppSetting("folder:name:resources:destination");
                    public static readonly string Success = AppSettingHelper.GetAppSetting("folder:name:success");
                }

                public static class InforSxe
                {
                    public static readonly string Database = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:database");
                    public static readonly string Driver = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:driver");
                    public static readonly string Hostname = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:hostname");
                    public static readonly string Password = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:password");
                    public static readonly string Port = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:port");
                    public static readonly string UserId = AppSettingHelper.GetCollectiveAppSetting("connector:infor.sx.e:user.id");
                }

                public static class Inriver
                {
                    public static readonly string ConnectionString = AppSettingHelper.GetCollectiveAppSetting("connector:inriver:connectionstring");
                    public static readonly string FtpPath = AppSettingHelper.GetCollectiveAppSetting("connector:inriver:ftp");
                }

                public static class Ressource
                {
                    public static readonly string Destination = AppSettingHelper.GetCollectiveAppSetting("ressource:ftp:destination");
                    public static readonly string Hostname = AppSettingHelper.GetCollectiveAppSetting("ressource:ftp:hostname");
                    public static readonly string Password = AppSettingHelper.GetCollectiveAppSetting("ressource:ftp:password");
                    public static readonly string Port = AppSettingHelper.GetCollectiveAppSetting("ressource:ftp:port");
                    public static readonly string User = AppSettingHelper.GetCollectiveAppSetting("ressource:ftp:user");
                }
            }

            public static class Image
            {
                public static readonly int Quality = AppSettingHelper.GetAppSetting("image:quality", 100);
                public static readonly string SuffixLarge = AppSettingHelper.GetAppSetting("image:suffix:large");
                public static readonly string SuffixMedium = AppSettingHelper.GetAppSetting("image:suffix:medium");
                public static readonly string SuffixSmall = AppSettingHelper.GetAppSetting("image:suffix:small");

                public static class Product
                {
                    public static readonly int SizeLarge = AppSettingHelper.GetAppSetting("image:product:size:large", 1000);
                    public static readonly int SizeMedium = AppSettingHelper.GetAppSetting("image:product:size:medium", 640);
                    public static readonly int SizeSmall = AppSettingHelper.GetAppSetting("image:product:size:small", 200);
                }
            }

            public static class InforSxeApi
            {
                public static readonly int CompanyNumber = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:company.number", 1);
                public static readonly string ConnectionString = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:connection.string");
                public static readonly string Password = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:password");
                public static readonly string Url = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:url");
                public static readonly string Username = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:user");
            }
        }
    }
}
