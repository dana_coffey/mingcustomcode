﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Warehouse
        {
            public static class WarehouseType
            {
                public const string Branch = "Branch";
                public const string MMI = "MMI";
            }
        }
    }
}
