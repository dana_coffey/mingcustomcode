﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class Insite
        {
            public static class Documents
            {
                public const int MaxNameLength = 100;
            }

            public static class Order
            {
                public static class LineType
                {
                    public const string Product = "Product";
                }

                public static class Status
                {
                    public const string Cancelled = "Cancelled";
                    public const string Complete = "Complete";
                    public const string Processing = "Processing";
                    public const string Invoiced = "Invoiced";
                    public const string Quoted = "Quoted";
                    public const string Submitting = "Submitting";
                    public const string Submitted = "Submitted";
                }
            }

            public static class RoundingRules
            {
                public const string MultipleOnly = "Multiple Only";
            }
        }
    }
}
