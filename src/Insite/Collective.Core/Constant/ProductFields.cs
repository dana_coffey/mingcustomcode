﻿namespace Collective.Core.Constant
{
    public static partial class Constants
    {
        public static class ProductFields
        {
            public static class DataTypes
            {
                public const string Boolean = "Boolean";
                public const string Cvl = "CVL";
            }

            public static class FieldNames
            {
                public const string ItemErpVendorNo = "ItemERPVendorNo";
                public const string ItemIsCatalogProduct = "ItemIsCatalogProduct";
                public const string ItemObsolete = "ItemObsolete";
                public const string ItemUpc = "ItemUpc";
                public const string ItemUrn = "ItemUrn";
                public const string ProductIsSouthCarolinaSolidWasteTaxable = "ProductIsSouthCarolinaSolidWasteTaxable";
                public const string ProductNameOverride = "ProductNameOverride";
                public const string ProductMetaDescription = "ProductMetaDescription";
                public const string RoundBy = "RoundBy";
                public const string UnitOfMeasure = "UnitOfMeasure";
                public const string UnitOfMeasureDescription = "UnitOfMeasureDescription";
            }

            public static class Settings
            {
                public const string ExcludedFromSpecifications = "ExcludedFromSpecifications";
                public const string UnitOfMeasure = "UOM";
            }
        }
    }
}
