﻿using System.Collections.Generic;
using Collective.InforSxe.Core.Models;

namespace Collective.InforSxe.Core.Interfaces
{
    public interface ISxeApiHelper
    {
        InventoryApiModel GetInventory(string productErpNumber, List<string> activeWarehouses);

        IEnumerable<InventoryApiModel> GetInventory(List<string> productErpNumbers, List<string> activeWarehouses);

        PricingApiModel GetPrice(int billtoId, string shiptoId, string productId, string warehouseId);

        IEnumerable<PricingApiModel> GetPrices(int billtoId, string shiptoId, List<string> productIds, string warehouseId);
    }
}
