﻿using System.Collections.Generic;
using Collective.InforSxe.Core.Models;
using Collective.InforSxe.Core.Models.OrderSubmit;

namespace Collective.InforSxe.Core.Interfaces
{
    public interface ICollectiveApiService
    {
        IEnumerable<InventoryApiModel> GetInventory(List<string> productIds, List<string> activeWarehouseIds);

        InventoryApiModel GetInventory(string productId, List<string> activeWarehouseIds);

        PricingApiModel GetPrice(int billtoId, string shiptoId, string productId, string warehouseId);

        IEnumerable<PricingApiModel> GetPrices(int billtoId, string shiptoId, List<string> productIds, string warehouseId);

        OrderSubmitResponseApiModel SubmitOrder(OrderSubmitRequestApiModel orderSubmitRequestModel);
    }
}
