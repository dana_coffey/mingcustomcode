﻿using System.Data;

namespace Collective.InforSxe.Core.Extensions
{
    public static class DataRowExtensions
    {
        public static string GetColumnStringValueOrEmpty(this DataRow row, string columnName)
        {
            return row.Table.Columns.Contains(columnName) ? row[columnName].ToString() : string.Empty;
        }
    }
}
