﻿namespace Collective.InforSxe.Core.Models
{
    public class WarehouseInventoryApiModel
    {
        public decimal Quantity { get; set; }
        public string WarehouseId { get; set; }
    }
}
