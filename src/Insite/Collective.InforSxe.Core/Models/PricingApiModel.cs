﻿using System.Collections.Generic;

namespace Collective.InforSxe.Core.Models
{
    public class PricingApiModel
    {
        public List<BreakPriceApiModel> BreakPrices { get; set; }
        public decimal CurrencyConversionRate { get; set; }
        public bool IsOnSale { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Price { get; set; }
        public string ProductId { get; set; }
    }
}
