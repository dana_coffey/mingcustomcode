﻿namespace Collective.InforSxe.Core.Models
{
    public class BreakPriceApiModel
    {
        public decimal BreakQty { get; set; }
        public decimal Price { get; set; }
    }
}
