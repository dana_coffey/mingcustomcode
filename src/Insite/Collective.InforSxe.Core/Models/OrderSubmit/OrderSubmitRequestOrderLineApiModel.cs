﻿namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestOrderLineApiModel
    {
        public string ErpNumber { get; set; }
        public int LineNumber { get; set; }
        public string Notes { get; set; }
        public decimal ProductDiscountPerEach { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal UnitNetPrice { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
