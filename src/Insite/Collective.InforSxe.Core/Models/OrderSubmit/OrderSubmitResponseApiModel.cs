﻿using System.Collections.Generic;

namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitResponseApiModel
    {
        public string ErpOrderNumber { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public IEnumerable<OrderSubmitResponseOrderLineApiModel> OrderLines { get; set; }
        public string WebOrderNumber { get; set; }
    }
}
