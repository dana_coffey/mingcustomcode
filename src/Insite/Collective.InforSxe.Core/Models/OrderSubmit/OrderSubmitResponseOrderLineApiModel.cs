﻿namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitResponseOrderLineApiModel
    {
        public string ErpNumber { get; set; }
        public string LineIdentifier { get; set; }
        public int LineNumber { get; set; }
        public int QtyOrdered { get; set; }
        public int QtyShipped { get; set; }
    }
}
