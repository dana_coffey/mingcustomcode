﻿namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestCustomerOrderApiModel
    {
        public string Buyer { get; set; }
        public string CurrencyCode { get; set; }
        public string CustomerPurchaseOrderNumber { get; set; }
        public string CustomerReference { get; set; }
        public string DefaultWarehouseName { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal Handling { get; set; }
        public string Notes { get; set; }
        public string OrderNumber { get; set; }
        public string ShipAddonCode { get; set; }
        public decimal ShippingAmount { get; set; }
        public string ShipViaCode { get; set; }
        public string TermsCode { get; set; }
        public string RequestedShipDate { get; set; }
        public string OrderDisposition { get; set; }
    }
}
