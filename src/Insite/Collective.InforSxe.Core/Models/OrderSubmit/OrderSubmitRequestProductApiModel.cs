﻿using System;

namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestProductApiModel
    {
        public string ErpNumber { get; set; }
        public Guid? Id { get; set; }
    }
}
