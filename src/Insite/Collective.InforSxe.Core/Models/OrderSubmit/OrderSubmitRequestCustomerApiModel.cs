﻿namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestCustomerApiModel
    {
        public string CustomerEmail { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerSequence { get; set; }
    }
}
