﻿namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestShipToApiModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string CustomerSequence { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
    }
}
