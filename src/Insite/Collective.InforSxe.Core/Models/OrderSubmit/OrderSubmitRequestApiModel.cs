﻿using System.Collections.Generic;

namespace Collective.InforSxe.Core.Models.OrderSubmit
{
    public class OrderSubmitRequestApiModel
    {
        public OrderSubmitRequestCustomerApiModel Customer { get; set; }
        public OrderSubmitRequestCustomerOrderApiModel CustomerOrder { get; set; }
        public List<OrderSubmitRequestOrderLineApiModel> OrderLine { get; set; } = new List<OrderSubmitRequestOrderLineApiModel>();
        public OrderSubmitRequestShipToApiModel ShipTo { get; set; }
    }
}
