﻿using System.Collections.Generic;
using System.Linq;

namespace Collective.InforSxe.Core.Models
{
    public class InventoryApiModel
    {
        public string ProductId { get; set; }
        public decimal TotalQuantity => Warehouses?.Sum(x => x.Quantity) ?? 0;
        public IEnumerable<WarehouseInventoryApiModel> Warehouses { get; set; }
    }
}
