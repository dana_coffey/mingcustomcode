param([string]$buildConfiguration, [string]$solutionDir)

function transform-xml([string]$xml, [string]$xdt, [string]$output) {
    if (!$xml -or !(Test-Path -path $xml -PathType Leaf)) {
        throw "File not found. $xml";
    }
    if (!$xdt -or !(Test-Path -path $xdt -PathType Leaf)) {
        throw "File not found. $xdt";
    }

    Add-Type -LiteralPath "..\..\packages\Microsoft.Web.Xdt.2.1.2\lib\net40\Microsoft.Web.XmlTransform.dll"

    $xmldoc = New-Object Microsoft.Web.XmlTransform.XmlTransformableDocument;
    $xmldoc.PreserveWhitespace = $true
    $xmldoc.Load($xml);

    $transf = New-Object Microsoft.Web.XmlTransform.XmlTransformation($xdt);
    if ($transf.Apply($xmldoc) -eq $false)
    {
        throw "Transformation failed."
    }
    $xmldoc.Save($output);
}

transform-xml -xml ($solutionDir + "Collective.Web\config\appSettings\appSettings.config.default") -xdt ($solutionDir + "Collective.Web\config\appSettings\appSettings.config.$buildConfiguration") -output ($solutionDir + "Collective.Web\config\appSettings.config")
transform-xml -xml ($solutionDir + "Wis\SiteConnections\SiteConnections.config.default") -xdt ($solutionDir + "Wis\SiteConnections\SiteConnections.config.$buildConfiguration") -output ($solutionDir + "Wis\SiteConnections.config")