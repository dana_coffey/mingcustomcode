﻿using Collective.Core.Helpers;

namespace Collective.InforSxe.Ming.Constant
{
    public static partial class Constants
    {
        public static class AppSettings
        {
            public static class InforSxeApi
            {
                public static readonly string UrlIc = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:url:ic");
                public static readonly string UrlOe = AppSettingHelper.GetCollectiveAppSetting("api:infor.sx.e:url:oe");
            }
        }
    }
}
