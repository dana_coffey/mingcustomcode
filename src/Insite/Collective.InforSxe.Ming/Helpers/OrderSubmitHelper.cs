﻿using System;
using System.Globalization;
using System.Linq;
using Collective.Core.Helpers;
using Collective.InforSxe.Core.Models.OrderSubmit;
using Collective.InforSxe.Ming.SxApiOE;

namespace Collective.InforSxe.Ming.Helpers
{
    public static class OrderSubmitHelper
    {
        public static OEFullOrderMntV6Request GetOrderSubmitRequestModel(OrderSubmitRequestApiModel orderSubmitRequestModel)
        {
            var sxRequestModel = new OEFullOrderMntV6Request();

            AddOrderHeaderToRequest(sxRequestModel, orderSubmitRequestModel);
            AddBillToRequest(sxRequestModel, orderSubmitRequestModel);
            AddCustomerToRequest(sxRequestModel, orderSubmitRequestModel);
            AddShipToToRequest(sxRequestModel, orderSubmitRequestModel);
            AddOrderLinesToRequest(sxRequestModel, orderSubmitRequestModel);

            return sxRequestModel;
        }

        public static OrderSubmitResponseApiModel ToOrderSubmitResponseModel(string orderNumber, OEFullOrderMntV6Response apiResult)
        {
            var result = new OrderSubmitResponseApiModel
            {
                WebOrderNumber = orderNumber
            };

            if (apiResult.Outacknowledgement?.Any() ?? false)
            {
                result.ErrorCode = apiResult.Outacknowledgement[0].ErrorNumber;
                result.ErrorMessage = apiResult.Outacknowledgement[0].Message;
            }

            if (apiResult.Outheader?.Any() ?? false)
            {
                result.ErpOrderNumber = apiResult.Outheader[0].InvoiceNumber + "-" + apiResult.Outheader[0].InvoiceSuffix;
            }

            if (apiResult.Outitem?.Any() ?? false)
            {
                result.OrderLines = apiResult.Outitem.Select(x => new OrderSubmitResponseOrderLineApiModel
                {
                    QtyShipped = int.TryParse(x.QuantityShip, out var qtyShip) ? qtyShip : 0,
                    QtyOrdered = int.TryParse(x.QuantityOrdered, out var qtyOrdered) ? qtyOrdered : 0,
                    LineIdentifier = x.LineIdentifier,
                    ErpNumber = x.BuyerProductCode,
                });
            }

            return result;
        }

        private static void AddBillToRequest(OEFullOrderMntV6Request sxRequestModel, OrderSubmitRequestApiModel model)
        {
            sxRequestModel.InbillTo = new[] { new InbillTo5 { BillToNumber = model.Customer.CustomerNumber } };
        }

        private static void AddCustomerToRequest(OEFullOrderMntV6Request sxRequestModel, OrderSubmitRequestApiModel model)
        {
            sxRequestModel.Incustomer = new[] { new Incustomer5 { CustomerNumber = model.Customer.CustomerNumber } };
        }

        private static void AddOrderHeaderToRequest(OEFullOrderMntV6Request sxRequestModel, OrderSubmitRequestApiModel orderSubmitRequestModel)
        {
            var shipInstructionNote = orderSubmitRequestModel.CustomerOrder.Notes.Substring(0, orderSubmitRequestModel.CustomerOrder.Notes.Length < 198 ? orderSubmitRequestModel.CustomerOrder.Notes.Length : 198);

            var sxOrderModel = new Inorder5
            {
                ActionType = "STOREFRONT",
                CompanyNumber = Collective.Core.Constant.Constants.AppSettings.InforSxeApi.CompanyNumber.ToString(),
                Warehouse = orderSubmitRequestModel.CustomerOrder.DefaultWarehouseName,
                CurrencyCode = orderSubmitRequestModel.CustomerOrder.CurrencyCode,
                Buyer = orderSubmitRequestModel.CustomerOrder.Buyer,
                PurchaseOrderNumber = orderSubmitRequestModel.CustomerOrder.CustomerPurchaseOrderNumber,
                ApprovalType = "h",
                TransactionType = "SO",
                ShipVia = orderSubmitRequestModel.CustomerOrder.ShipViaCode,
                OrderDisposition = orderSubmitRequestModel.CustomerOrder.OrderDisposition,
                WholeOrderDiscountAmount = orderSubmitRequestModel.CustomerOrder.DiscountAmount,
                WholeOrderDiscountType = "$",
                User1 = orderSubmitRequestModel.CustomerOrder.OrderNumber,
                AddonAmount2 = orderSubmitRequestModel.CustomerOrder.ShippingAmount,
                AddonNumber2 = ParseHelper.ParseInt(orderSubmitRequestModel.CustomerOrder.ShipAddonCode, 0),
                AddonType2 = "$",
                ShipInstructions = shipInstructionNote,
                User3 = orderSubmitRequestModel.Customer.CustomerEmail,
                Reference = orderSubmitRequestModel.CustomerOrder.CustomerReference,
                RequestedShipDate = orderSubmitRequestModel.CustomerOrder.RequestedShipDate,
                PromiseDate = orderSubmitRequestModel.CustomerOrder.RequestedShipDate
            };

            sxRequestModel.Inorder = new[] { sxOrderModel };
        }

        private static void AddOrderLinesToRequest(OEFullOrderMntV6Request sxRequestModel, OrderSubmitRequestApiModel model)
        {
            var sxOrderLineModels = new Initem5[model.OrderLine.Count];
            var itemCount = 0;

            foreach (var orderLine in model.OrderLine)
            {
                var fullUnitCost = orderLine.ProductDiscountPerEach + orderLine.UnitNetPrice;

                var sxOrderLineModel = new Initem5
                {
                    LineIdentifier = orderLine.LineNumber.ToString("D6"),
                    SellerProductCode = orderLine.ErpNumber,
                    QuantityOrdered = orderLine.QtyOrdered.ToString(CultureInfo.InvariantCulture),
                    UnitOfMeasure = orderLine.UnitOfMeasure,
                    LineComments = orderLine.Notes,
                    UnitCost = fullUnitCost.ToString(CultureInfo.InvariantCulture),
                    DiscountType = true // true means amount, false means percent
                };

                sxOrderLineModels[itemCount] = sxOrderLineModel;
                itemCount++;
            }

            sxRequestModel.Initem = sxOrderLineModels;
        }

        private static void AddShipToToRequest(OEFullOrderMntV6Request sxRequestModel, OrderSubmitRequestApiModel model)
        {
            sxRequestModel.InshipTo = new[]
            {
                new InshipTo5
                {
                    ShipToNumber = model.ShipTo.CustomerSequence,
                    Name = model.ShipTo.CompanyName,
                    Address1 = model.ShipTo.Address1,
                    Address2 = model.ShipTo.Address2,
                    City = model.ShipTo.City,
                    CountryCode = model.ShipTo.Country,
                    Phone = model.ShipTo.Phone,
                    PostalCode = model.ShipTo.PostalCode,
                    State = model.ShipTo.State
                }
            };
        }
    }
}
