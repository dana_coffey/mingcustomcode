﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.InforSxe.Core.Interfaces;
using Collective.InforSxe.Core.Models;
using Collective.InforSxe.Core.Models.OrderSubmit;
using Collective.InforSxe.Ming.Helpers;
using Collective.InforSxe.Ming.SxApiIC;
using Collective.InforSxe.Ming.SxApiOE;
using Insite.Common.Logging;

namespace Collective.InforSxe.Ming.Services
{
    public class MingApiService : ICollectiveApiService, ICollectiveInjectableClass
    {
        private static readonly BasicHttpBinding DefaultBasicHttpBinding = new BasicHttpBinding
        {
            MaxReceivedMessageSize = 104857600,
            MaxBufferSize = 104857600,
            MaxBufferPoolSize = 104857600,
            SendTimeout = new TimeSpan(hours: 0, minutes: 2, seconds: 30),
            ReceiveTimeout = new TimeSpan(hours: 0, minutes: 2, seconds: 30)
        };

        public InventoryApiModel GetInventory(string productId, List<string> activeWarehouseIds)
        {
            return GetInventory(new List<string> { productId }, activeWarehouseIds).FirstOrDefault();
        }

        public IEnumerable<InventoryApiModel> GetInventory(List<string> productIds, List<string> activeWarehouseIds)
        {
            using (var client = new ServiceICClient(DefaultBasicHttpBinding, new EndpointAddress(Constant.Constants.AppSettings.InforSxeApi.UrlIc)))
            {
                var request = new ICProductAvailByWhseRequest { IgnoreZeroAvailable = false };
                var inventoryModels = new List<InventoryApiModel>();

                foreach (var productId in productIds)
                {
                    request.ProductCode = productId;

                    var apiResult = client.ICProductAvailByWhse(new SxApiIC.CallConnection
                    {
                        CompanyNumber = Constants.AppSettings.InforSxeApi.CompanyNumber,
                        ConnectionString = Constants.AppSettings.InforSxeApi.ConnectionString,
                        OperatorInitials = Constants.AppSettings.InforSxeApi.Username,
                        OperatorPassword = Constants.AppSettings.InforSxeApi.Password
                    }, request);

                    if (string.IsNullOrEmpty(apiResult.ErrorMessage))
                    {
                        inventoryModels.Add(new InventoryApiModel
                        {
                            ProductId = productId,
                            Warehouses = apiResult.Outavailability.Where(x => activeWarehouseIds.Any(y => y.Equals(x.Warehouse, StringComparison.OrdinalIgnoreCase))).Select(x => new WarehouseInventoryApiModel
                            {
                                Quantity = x.NetAvailable,
                                WarehouseId = x.Warehouse
                            }).ToList()
                        });
                    }
                    else
                    {
                        LogHelper.For(apiResult).Error($"{productId} - {apiResult.ErrorMessage}");
                    }
                }

                return inventoryModels;
            }
        }

        public PricingApiModel GetPrice(int billtoId, string shiptoId, string productId, string warehouseId)
        {
            return GetPrices(billtoId, shiptoId, new List<string> { productId }, warehouseId).FirstOrDefault();
        }

        public IEnumerable<PricingApiModel> GetPrices(int billtoId, string shiptoId, List<string> productIds, string warehouseId)
        {
            warehouseId = warehouseId ?? Constant.Constants.Ids.Warehouse.DefaultWarehouseId;

            var apiResult = GetPricesFromApi(billtoId, shiptoId, warehouseId, productIds.Select(p => new KeyValuePair<string, decimal>(p, 1)).ToList());

            if (string.IsNullOrEmpty(apiResult.ErrorMessage))
            {
                var apiPricingModels = apiResult.Outprice.Select(
                    p => new PricingApiModel
                    {
                        ProductId = p.ProductCode,
                        Price = p.Price,
                        NetPrice = p.ExtendedAmount,
                        CurrencyConversionRate = p.UnitConversion,
                        IsOnSale = p.PromotionalFlag,
                        BreakPrices = GetBreakPrices(apiResult.Outpricebreak?.FirstOrDefault(x => x.ProductCode == p.ProductCode), billtoId, shiptoId, p.ProductCode, warehouseId)
                    }).ToList();

                return apiPricingModels;
            }

            LogHelper.For(apiResult).Error(apiResult.ErrorMessage);

            return null;
        }

        public OrderSubmitResponseApiModel SubmitOrder(OrderSubmitRequestApiModel orderSubmitRequestModel)
        {
            using (var client = new ServiceOEClient(DefaultBasicHttpBinding, new EndpointAddress(Constant.Constants.AppSettings.InforSxeApi.UrlOe)))
            {
                var submitOrderRequest = OrderSubmitHelper.GetOrderSubmitRequestModel(orderSubmitRequestModel);

                var apiResult = client.OEFullOrderMntV6(new SxApiOE.CallConnection
                {
                    CompanyNumber = Constants.AppSettings.InforSxeApi.CompanyNumber,
                    ConnectionString = Constants.AppSettings.InforSxeApi.ConnectionString,
                    OperatorInitials = Constants.AppSettings.InforSxeApi.Username,
                    OperatorPassword = Constants.AppSettings.InforSxeApi.Password
                }, submitOrderRequest);

                return OrderSubmitHelper.ToOrderSubmitResponseModel(orderSubmitRequestModel.CustomerOrder.OrderNumber, apiResult);
            }
        }

        private List<BreakPriceApiModel> GetBreakPrices(Outpricebreak2 breakingData, int billtoId, string shiptoId, string productCode, string warehouseId)
        {
            if (breakingData != null)
            {
                var quantityBreaks = new List<decimal>
                {
                    1,
                    breakingData.QuantityBreak1,
                    breakingData.QuantityBreak2,
                    breakingData.QuantityBreak3,
                    breakingData.QuantityBreak4,
                    breakingData.QuantityBreak5,
                    breakingData.QuantityBreak6,
                    breakingData.QuantityBreak7,
                    breakingData.QuantityBreak8
                }.Where(p => p > 0).ToList();

                if (quantityBreaks.Any())
                {
                    var apiResult = GetPricesFromApi(billtoId, shiptoId, warehouseId, quantityBreaks.Select(p => new KeyValuePair<string, decimal>(productCode, p)));

                    return apiResult.Outprice.Select(p => new BreakPriceApiModel
                    {
                        BreakQty = p.Quantity,
                        Price = p.ExtendedAmount / p.Quantity
                    }).ToList();
                }
            }

            return null;
        }

        private OEPricingMultipleV3Response GetPricesFromApi(int billtoId, string shiptoId, string warehouseId, IEnumerable<KeyValuePair<string, decimal>> products)
        {
            using (var client = new ServiceOEClient(DefaultBasicHttpBinding, new EndpointAddress(Constant.Constants.AppSettings.InforSxeApi.UrlOe)))
            {
                var request = new OEPricingMultipleV3Request
                {
                    CustomerNumber = billtoId,
                    UseDefaultWhse = string.IsNullOrEmpty(warehouseId),
                    GetPriceBreaks = true,
                    ShipTo = shiptoId,
                    Inproduct = products.Select(x => new Inproduct2 { ProductCode = x.Key, Quantity = x.Value, Warehouse = warehouseId ?? string.Empty }).ToArray()
                };

                return client.OEPricingMultipleV3(new SxApiOE.CallConnection
                {
                    CompanyNumber = Constants.AppSettings.InforSxeApi.CompanyNumber,
                    ConnectionString = Constants.AppSettings.InforSxeApi.ConnectionString,
                    OperatorInitials = Constants.AppSettings.InforSxeApi.Username,
                    OperatorPassword = Constants.AppSettings.InforSxeApi.Password
                }, request);
            }
        }
    }
}
