function CopyBaseFiles([string] $wisPath, [string] $wisPathDllFolder)
{
    if (!(Test-Path -path $wisPathDllFolder)) 
    {
        New-Item $wisPathDllFolder -Type Directory
    }

    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Wis\Insite.WindowsIntegrationService.exe.config") -Destination "$wisPath"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Wis\SiteConnections.config") -Destination "$wisPath"
	Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Wis\collectiveAppSettings.config") -Destination "$wisPath"

    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Core\bin\Collective.Core.dll") -Destination "$wisPath"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Core\bin\Collective.Core.pdb") -Destination "$wisPath"
	
	Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.InforSxe.Core\bin\Collective.InforSxe.Core.dll") -Destination "$wisPath"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.InforSxe.Core\bin\Collective.InforSxe.Core.pdb") -Destination "$wisPath"

    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Integration.Core\bin\Collective.Integration.Core.dll") -Destination "$wisPath"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Integration.Core\bin\Collective.Integration.Core.pdb") -Destination "$wisPath"

    
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Integration\bin\Absolunet.InRiver.Dequeue.Insite.Models.dll") -Destination "$wisPath"
    
    Remove-Item "$wisPathDllFolder\*"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Integration\bin\Collective.Integration.dll") -Destination "$wisPathDllFolder"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.Integration\bin\Collective.Integration.pdb") -Destination "$wisPathDllFolder"
}

function DeployWis([ScriptBlock]$copyAdditionalFiles)
{
    Write-Host "Collective Deploy Script [LOCAL] WIS"

    $sw = [Diagnostics.Stopwatch]::StartNew()
    $wisPath = "C:\Program Files\Insite Software\Commerce Integration Service V4.3 - MING"
    $wisPathDllFolder = "$wisPath\Collective"
    $wisServiceName = "Commerce Integration Service V4.3 (x64) - MING"

    StopService $wisServiceName

    Write-Host "Copying files..."

    CopyBaseFiles $wisPath $wisPathDllFolder

    if ($copyAdditionalFiles)
    {
        $copyAdditionalFiles.Invoke($wisPath, $wisPathDllFolder)
    }    

    StartService $wisServiceName

    $sw.Stop()
    Write-Host "Deploy done. Execution time:" $sw.Elapsed -foregroundcolor "Green"
    Write-Host "Press any key to exit."
    Read-Host
}