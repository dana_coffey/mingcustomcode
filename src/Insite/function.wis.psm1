function RunAsAdministrator($arguments, $path)
{
    $myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
    $myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
    $adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator

    # Check to see if we are currently running "as Administrator"
    if (!$myWindowsPrincipal.IsInRole($adminRole))
    {
        # Create a new process object that starts PowerShell as Administrator
        $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
        $newProcess.Arguments = $arguments;
        $newProcess.Verb = "runas";
        $newProcess.WorkingDirectory = $path;
        [System.Diagnostics.Process]::Start($newProcess);
        exit
    }
}

function StopService([string] $wisServiceName)
{
    $service = (get-service -Name "$wisServiceName")
    if($service.CanStop)
    {
        Write-Host "Stopping service..."
        $service.Stop()	
        do 
        {
            Start-Sleep -Milliseconds 200
        } until ((get-service -Name "$wisServiceName").Status -eq "Stopped")
    }
}

function StartService([string] $wisServiceName)
{
    $service = (get-service -Name "$wisServiceName")
    Write-Host "Starting service..."
    $service.Start()
    do 
    {
        Start-Sleep -Milliseconds 200
    } until ((get-service -Name "$wisServiceName").Status -eq "Running")
}