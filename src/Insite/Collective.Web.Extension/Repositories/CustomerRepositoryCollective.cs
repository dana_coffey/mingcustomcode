﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Collective.Core.Constant;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Data.Entities;
using Insite.Data.Repositories;

namespace Collective.Web.Extension.Repositories
{
    public class CustomerRepositoryCollective : CustomerRepository, IDependency
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;
        private readonly IRepository<UserProfile> _userProfileRepository;

        public CustomerRepositoryCollective(CustomerDefaultsSettings customerDefaultsSettings, IAuthenticationService authenticationService, IRepository<UserProfile> userProfileRepository)
        {
            _customerDefaultsSettings = customerDefaultsSettings;
            _authenticationService = authenticationService;
            _userProfileRepository = userProfileRepository;
        }

        public override IQueryable<Customer> GetAssignedBillTos(Guid userProfileId)
        {
            if (IsUserInRole(Constants.Roles.PointOfSale, userProfileId))
            {
                var customerNumbers = GetTable().Where(c => c.IsShipTo && c.IsActive).Select(c => c.CustomerNumber).Distinct().ToList();
                return GetTable().Where(c => c.IsBillTo && c.IsActive && customerNumbers.Contains(c.CustomerNumber));
            }

            return base.GetAssignedBillTos(userProfileId);
        }

        public override IQueryable<Customer> GetAvailableShipTos(Customer billTo, Guid userProfileId, Guid websiteId)
        {
            if (billTo == null)
            {
                return new List<Customer>().AsQueryable();
            }

            var source = GetTable()
                .Include(x => x.Country)
                .Include(x => x.State)
                .Include(x => x.DefaultWarehouse)
                .Where(o => (o.Id == billTo.Id || o.ParentId == (Guid?)billTo.Id) && o.IsShipTo && o.IsActive);

            if (!GetAssignedShipTos(billTo, userProfileId).Any())
            {
                return source;
            }

            return source.Where(c => c.UserProfiles.Any(u => u.Id == userProfileId) && !(string.IsNullOrEmpty(c.Address1) || string.IsNullOrEmpty(c.PostalCode) || string.IsNullOrEmpty(c.City) || c.StateId == null));
        }

        public override Customer GetDefaultShipTo(Customer billTo, Guid userProfileId, Guid websiteId)
        {
            Customer result = null;

            var source = GetAvailableShipTos(billTo, userProfileId, websiteId).Where(x => !x.CustomerSequence.StartsWith(_customerDefaultsSettings.ErpShipToPrefix)).OrderBy(x => x.CustomerSequence);

            var userProfile = DataProvider.GetById<UserProfile>(userProfileId);
            if (userProfile?.DefaultCustomerId.HasValue ?? false)
            {
                result = source.FirstOrDefault(x => x.Id == userProfile.DefaultCustomerId);
            }

            return result ?? source.FirstOrDefault();
        }

        private bool IsUserInRole(string roleName, Guid userProfileId)
        {
            var userProfile = _userProfileRepository.Get(userProfileId);

            if (_authenticationService.IsAuthenticated() && userProfile != null)
            {
                return _authenticationService.IsUserInRole(userProfile.UserName, roleName);
            }

            return false;
        }
    }
}
