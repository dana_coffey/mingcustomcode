﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.ContactUs
{
    public class AddContactUsContentPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private const string ContentZone = "Content";

        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 11, 20, 11, 55, 00));

        public AddContactUsContentPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var contactUsPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "ContentPage" && p.ContentKey == 14);
            if (contactUsPage != null)
            {
                var pageModel = _contentItemMapper.Map(contactUsPage);

                CleanZone(contactUsPage.ContentKey, ContentZone);
                UpdatePageInfo(pageModel);
                CreatePageTitleWidget(contactUsPage.ContentKey);
                CreateContent(contactUsPage.ContentKey);
            }
        }

        private void CleanZone(int parentContentKey, string zoneName)
        {
            var items = _contentItemRepository.GetTable().Where(p => p.ParentKey == parentContentKey && p.Zone == zoneName);

            foreach (var item in items)
            {
                _contentItemRepository.Delete(item);
            }
        }

        private void CreateContent(int parentContentKey)
        {
            var contactUs = _contentCreatorHelper.InitializeItem<ContactUsForm>(ContentZone, parentContentKey);

            contactUs.ParentKey = parentContentKey;
            contactUs.SuccessMessage = "<p>Your message has been sent.</p>";
            contactUs.Topics = new List<string>
            {
                "Account creation request",
                "Feedback",
                "Issue",
                "General",
                "Part information or special order"
            };
            contactUs.EmailTo = new List<string>
            {
                "contactus@thecollective.com"
            };

            SetTemplateView(contactUs);
            _contentCreatorHelper.SaveItem(contactUs, DateTimeOffset.Now);
        }

        private void CreatePageTitleWidget(int parentContentKey)
        {
            var pageTitle = _contentCreatorHelper.InitializeItem<PageTitle>(ContentZone, parentContentKey);
            pageTitle.SortOrder = 100;
            SetTemplateView(pageTitle);

            _contentCreatorHelper.SaveItem(pageTitle, DateTimeOffset.Now);
        }

        private void SetTemplateView(ContentItemModel contentItem)
        {
            contentItem.TemplateView = "Store/Standard";
        }

        private void UpdatePageInfo(ContentItemModel pageModel)
        {
            pageModel.SetValue(nameof(ContentPage.DisallowFromSiteSearch), "false", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.DisallowInRobotsTxt), "false", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.ExcludeFromNavigation), "true", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.ExcludeFromSignInRequired), "true", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.HideFooter), "false", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.HideHeader), "false", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.Title), "Contact Us", FieldType.Contextual);
            pageModel.SetValue(nameof(ContentPage.Url), "/contact-us", FieldType.Contextual);
            _contentCreatorHelper.SaveItem(pageModel, DateTimeOffset.Now);
        }
    }
}
