﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class SetBreadcrumbsPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 11, 08, 15, 00, 00));

        public SetBreadcrumbsPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var pagesName = new List<string> { "ChangeAccountPasswordPage", "MyAccountPage", "OrderUploadPage", "UserListPage", "UserSetupPage", "OrdersPage", "AccountSettingsPage" };

            foreach (var pageName in pagesName)
            {
                var page = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == pageName);

                if (page != null)
                {
                    var oldBreadcrumbs = _contentItemRepository.GetTable().Where(p => p.ParentKey == page.ContentKey && p.Class == "Breadcrumb");

                    foreach (var oldBreadcrumb in oldBreadcrumbs)
                    {
                        _contentItemRepository.Delete(oldBreadcrumb);
                    }

                    var breadcrumb = _contentCreatorHelper.InitializeItem<Breadcrumb>("TopContent", page.ContentKey);
                    breadcrumb.TemplateView = "Store/Standard";

                    _contentCreatorHelper.SaveItem(breadcrumb, DateTimeOffset.Now);
                }
            }
        }
    }
}
