﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts
{
    public class FooterLinksUpdatePostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IContentItemRepository _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 02, 17, 07, 16));

        public FooterLinksUpdatePostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetTypedRepository<IContentItemRepository>();
        }

        public override void Run()
        {
            var footerLinks = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(FooterLinks));
            if (footerLinks != null)
            {
                var model = _contentItemMapper.Map(footerLinks);

                model.SetValue(nameof(FooterLinks.TitleColumn4), "Need to contact us?", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.RightContent), "<p>Please contact our support team.</p><p>1-800-347-8804</p>", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.FacebookUrl), "https://www.facebook.com/Absolunet", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.TwitterUrl), "https://twitter.com/Absolunet", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(model, DateTimeOffset.Now);
            }
        }
    }
}
