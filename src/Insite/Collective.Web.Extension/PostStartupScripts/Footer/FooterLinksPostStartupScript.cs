﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts
{
    public class FooterLinksPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 09, 25, 15, 18, 59));

        public FooterLinksPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var footer = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "Footer");

            if (footer != null)
            {
                var oldFooterLinks = _contentItemRepository.GetTable().Where(p => p.ParentKey == footer.ContentKey && p.Class == "FooterLinks");

                foreach (var oldFooterLink in oldFooterLinks)
                {
                    _contentItemRepository.Delete(oldFooterLink);
                }

                var footerLinks = _contentCreatorHelper.InitializeItem<FooterLinks>("Content", footer.ContentKey);

                footerLinks.LinksColumn1 = "<ul><li><a href=\"/catalog/residential\">Residential</a></li><li><a href=\"/catalog/commercial\">Commercial</a></li><li><a href=\"/catalog/parts\">Parts</a></li><li><a href=\"/catalog/supplies\">Supplies</a></li></ul>";
                footerLinks.LinksColumn2 = "<ul><li><a href=\"/MyAccount\">Dashboard</a></li><li><a href=\"/MyAccount/Orders\">Order History</a></li></ul>";
                footerLinks.LinksColumn3 = "<ul><li><a href=\"#\">Our Company</a></li></ul>";
                footerLinks.RightContent = "<h5 class=\"title\">Need to contact us?</h5><p>Please contact our support team.</p><p>1-800-347-8804</p><p class=\"copyright\">Copyright &copy; Collective. All rights reserved.</p>";
                footerLinks.TitleColumn1 = "Catalog";
                footerLinks.TitleColumn2 = "My Account";
                footerLinks.TitleColumn3 = "Collective";

                footerLinks.TemplateView = "Store/Standard";

                _contentCreatorHelper.SaveItem(footerLinks, DateTimeOffset.Now);
            }
        }
    }
}
