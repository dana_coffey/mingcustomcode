﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts
{
    public class FooterLinksUpdate2PostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IContentItemRepository _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 03, 16, 41, 29));

        public FooterLinksUpdate2PostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetTypedRepository<IContentItemRepository>();
        }

        public override void Run()
        {
            var footerLinks = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(FooterLinks));
            if (footerLinks != null)
            {
                var model = _contentItemMapper.Map(footerLinks);

                model.SetValue(nameof(FooterLinks.TitleColumn4), "Need help with your order?", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.RightContent), "<p>We have the depth of experience and inventoried stock plus technical support to handle your job from the smallest to the largest!</p><p class=\"large-text\">1-800-347-8804</p>", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.FacebookUrl), "#", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.TwitterUrl), "#", FieldType.Contextual);
                model.SetValue(nameof(FooterLinks.GooglePlusUrl), "#", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(model, DateTimeOffset.Now);
            }
        }
    }
}
