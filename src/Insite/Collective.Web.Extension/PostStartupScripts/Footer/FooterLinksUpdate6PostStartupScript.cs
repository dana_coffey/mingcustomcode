﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts
{
    public class FooterLinksUpdate6PostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IContentItemRepository _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 12, 06, 11, 15, 00));

        public FooterLinksUpdate6PostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetTypedRepository<IContentItemRepository>();
        }

        public override void Run()
        {
            var footerLinks = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(FooterLinks));
            if (footerLinks != null)
            {
                var model = _contentItemMapper.Map(footerLinks);

                model.SetValue(nameof(FooterLinks.LinksColumn3), "<ul><li><a href=\"/AboutUs\">About Us</a></li><li><a href=\"[% translate 'Store_ContactUsUrl' %]\">[% translate 'Store_ContactUs' %]</a></li></ul>", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(model, DateTimeOffset.Now);
            }
        }
    }
}
