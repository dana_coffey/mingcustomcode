﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.PostStartupScripts.CartPage
{
    public class CartPageCleanUpPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 26, 12, 00, 00));

        public CartPageCleanUpPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var footer = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "CartPage");

            if (footer != null)
            {
                var webCrossSells = _contentItemRepository.GetTable().Where(p => p.ParentKey == footer.ContentKey && p.Class == "webcrosssells");

                foreach (var webCrossSell in webCrossSells)
                {
                    _contentItemRepository.Delete(webCrossSell);
                }
            }
        }
    }
}
