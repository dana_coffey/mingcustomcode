﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class AddBannerPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 14, 00, 00));

        public AddBannerPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");

            if (homePage != null)
            {
                var oldBanners = _contentItemRepository.GetTable().Where(p => p.ParentKey == homePage.ContentKey && p.Class == "Banner");

                foreach (var oldBanner in oldBanners)
                {
                    _contentItemRepository.Delete(oldBanner);
                }

                var banner = _contentCreatorHelper.InitializeItem<Banner>("Content", homePage.ContentKey);

                banner.Title = "Any job. Any size. Anywhere.";
                banner.Subtitle = "HVAC solutions for all your needs.";
                banner.BackgroundImageUrl = "/UserFiles/homepage/banner.jpg";
                banner.TemplateView = "Store/Standard";

                _contentCreatorHelper.SaveItem(banner, DateTimeOffset.Now);
            }
        }
    }
}
