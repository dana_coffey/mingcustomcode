﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class AddContentPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 15, 34, 00));

        public AddContentPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            CreateTwoColumnContent(CreateTwoColumnWidget());
        }

        private void CreateTwoColumnContent(ContentItem twoColumn)
        {
            if (twoColumn != null)
            {
                var leftRichContent = _contentCreatorHelper.InitializeItem<RichContent>("LeftColumn", twoColumn.ContentKey);
                var rightRichContent = _contentCreatorHelper.InitializeItem<RichContent>("RightColumn", twoColumn.ContentKey);

                leftRichContent.Body = "<blockquote><p>Collective is a pleasure to work with. They really care about us and are always making improvements to make our jobs easier.</p><p style=\"text-align: right;\"><strong>- Darren Hamilton</strong></p></blockquote>";
                rightRichContent.Body = "<blockquote><p>Collective has always made us feel like family. We value the close relationships that have developed over the last 50 years with the Absolunet family, and Collective staff</p><p style=\"text-align: right;\"><strong>- Patrick Ouelette</strong></p></blockquote>";

                leftRichContent.TemplateView = "Store/Standard";
                rightRichContent.TemplateView = "Store/Standard";

                _contentCreatorHelper.SaveItem(leftRichContent, DateTimeOffset.Now);
                _contentCreatorHelper.SaveItem(rightRichContent, DateTimeOffset.Now);
            }
        }

        private ContentItem CreateTwoColumnWidget()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");

            if (homePage != null)
            {
                var twoColumn = _contentCreatorHelper.InitializeItem<TwoColumn>("Content", homePage.ContentKey);

                twoColumn.LeftColumnSize = 6;
                twoColumn.RightColumnSize = 6;
                twoColumn.TemplateView = "Store/Standard";

                _contentCreatorHelper.SaveItem(twoColumn, DateTimeOffset.Now);

                return _contentItemRepository.GetTable().FirstOrDefault(p => p.ParentKey == homePage.ContentKey && p.Class == "TwoColumn");
            }

            return null;
        }
    }
}
