﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class AddMosaicPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 14, 15, 00));

        public AddMosaicPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");

            if (homePage != null)
            {
                var oldMosaics = _contentItemRepository.GetTable().Where(p => p.ParentKey == homePage.ContentKey && p.Class == "Mosaic");

                foreach (var oldMosaic in oldMosaics)
                {
                    _contentItemRepository.Delete(oldMosaic);
                }

                var mosaic = _contentCreatorHelper.InitializeItem<Mosaic>("Content", homePage.ContentKey);

                mosaic.GeneralTitleMosaic = "Browse Categories";
                mosaic.DescriptionMosaic1 = "Residential";
                mosaic.ImageMosaic1 = "/UserFiles/homepage/residential.jpg";
                mosaic.LinkMosaic1 = "/catalog/residential";

                mosaic.DescriptionMosaic2 = "Residential";
                mosaic.ImageMosaic2 = "/UserFiles/homepage/commercial.jpg";
                mosaic.LinkMosaic2 = "/catalog/commercial";

                mosaic.DescriptionMosaic3 = "Residential";
                mosaic.ImageMosaic3 = "/UserFiles/homepage/parts.jpg";
                mosaic.LinkMosaic3 = "/catalog/parts";

                mosaic.DescriptionMosaic4 = "Residential";
                mosaic.ImageMosaic4 = "/UserFiles/homepage/supplies.jpg";
                mosaic.LinkMosaic4 = "/catalog/supplies";

                mosaic.TemplateView = "Store/Standard";

                _contentCreatorHelper.SaveItem(mosaic, DateTimeOffset.Now);
            }
        }
    }
}
