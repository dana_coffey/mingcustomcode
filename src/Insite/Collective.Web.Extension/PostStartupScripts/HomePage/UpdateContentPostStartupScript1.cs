﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class UpdateContentPostStartupScript1 : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 15, 35, 00));

        public UpdateContentPostStartupScript1(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");
            if (homePage != null)
            {
                var twoColumn = _contentItemRepository.GetTable().FirstOrDefault(p => p.ParentKey == homePage.ContentKey && p.Class == "TwoColumn");
                if (twoColumn != null)
                {
                    var leftRichContent = _contentItemRepository.GetTable().FirstOrDefault(p => p.ParentKey == twoColumn.ContentKey && p.Class == "RichContent" && p.Zone == "LeftColumn");
                    var rightRichContent = _contentItemRepository.GetTable().FirstOrDefault(p => p.ParentKey == twoColumn.ContentKey && p.Class == "RichContent" && p.Zone == "RightColumn");
                    var leftRichContentModel = _contentItemMapper.Map(leftRichContent);
                    var rightRichContentModel = _contentItemMapper.Map(rightRichContent);

                    leftRichContentModel.SetValue(nameof(RichContent.Body), "<blockquote><p>Collective is a pleasure to work with. They really care about us and are always making improvements to make our jobs easier.</p></blockquote><p style=\"text-align: right;\"><strong>- Darren Hamilton</strong></p>", FieldType.Contextual);
                    rightRichContentModel.SetValue(nameof(RichContent.Body), "<blockquote><p>Collective has always made us feel like family. We value the close relationships that have developed over the last 50 years with the Absolunet family, and Collective staff</p></blockquote><p style=\"text-align: right;\"><strong>- Patrick Ouelette</strong></p>", FieldType.Contextual);

                    _contentCreatorHelper.SaveItem(leftRichContentModel, DateTimeOffset.Now);
                    _contentCreatorHelper.SaveItem(rightRichContentModel, DateTimeOffset.Now);
                }
            }
        }
    }
}
