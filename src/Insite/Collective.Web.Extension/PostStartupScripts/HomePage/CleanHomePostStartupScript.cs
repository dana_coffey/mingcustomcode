﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class CleanHomePostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 14, 30, 00));

        public CleanHomePostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");

            if (homePage != null)
            {
                var oldRichContent = _contentItemRepository.GetTable().Where(p => p.ParentKey == homePage.ContentKey && p.Class == "RichContent");

                foreach (var oldContent in oldRichContent)
                {
                    _contentItemRepository.Delete(oldContent);
                }
            }
        }
    }
}
