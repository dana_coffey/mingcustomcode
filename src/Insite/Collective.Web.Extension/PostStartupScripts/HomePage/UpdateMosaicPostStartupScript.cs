﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Collective.Web.Extension.Widgets;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.HomePage
{
    public class UpdateMosaicPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 14, 45, 00));

        public UpdateMosaicPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var homePage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "HomePage");

            if (homePage != null)
            {
                var mosaic = _contentItemRepository.GetTable().FirstOrDefault(p => p.ParentKey == homePage.ContentKey && p.Class == "Mosaic");
                var model = _contentItemMapper.Map(mosaic);

                model.SetValue(nameof(Mosaic.DescriptionMosaic1), string.Empty, FieldType.Contextual);
                model.SetValue(nameof(Mosaic.TitleMosaic1), "Residential", FieldType.Contextual);

                model.SetValue(nameof(Mosaic.DescriptionMosaic2), string.Empty, FieldType.Contextual);
                model.SetValue(nameof(Mosaic.TitleMosaic2), "Commercial", FieldType.Contextual);

                model.SetValue(nameof(Mosaic.DescriptionMosaic3), string.Empty, FieldType.Contextual);
                model.SetValue(nameof(Mosaic.TitleMosaic3), "Parts", FieldType.Contextual);

                model.SetValue(nameof(Mosaic.DescriptionMosaic4), string.Empty, FieldType.Contextual);
                model.SetValue(nameof(Mosaic.TitleMosaic4), "Supplies", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(model, DateTimeOffset.Now);
            }
        }
    }
}
