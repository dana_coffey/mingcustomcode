﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Pages;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.StoreLocator
{
    public class UpdateStoreLocatorPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 26, 14, 35, 00));

        public UpdateStoreLocatorPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var page = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "DealerLocatorPage");
            if (page != null)
            {
                var pageModel = _contentItemMapper.Map(page);

                pageModel.SetValue(nameof(ContentPage.Title), "Store Locator", FieldType.Contextual);
                pageModel.SetValue(nameof(ContentPage.Url), "/StoreLocator", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(pageModel, DateTimeOffset.Now);
            }
        }
    }
}
