﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.AboutUs
{
    public class AddAboutUsContentPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private const string ContentZone = "Content";
        private const string LeftColumnZone = "LeftColumn";
        private const string RightColumnZone = "RightColumn";

        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItemField> _contentItemFieldRepository;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 06, 15, 34, 00));

        public AddAboutUsContentPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemFieldRepository = unitOfWork.GetRepository<ContentItemField>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var aboutUsPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "ContentPage" && p.ContentKey == 10);
            if (aboutUsPage != null)
            {
                CleanZone(aboutUsPage.ContentKey, ContentZone);

                CreatePageTitleWidget(aboutUsPage.ContentKey);
                CreateDcneContent(CreateTwoColumnWidget(aboutUsPage.ContentKey, leftColumnSize: 4, cssClass: string.Empty, sortOrder: 200));
                CreateMingContent(CreateTwoColumnWidget(aboutUsPage.ContentKey, leftColumnSize: 8, cssClass: "alt", sortOrder: 300));
                CreateTecContent(CreateTwoColumnWidget(aboutUsPage.ContentKey, leftColumnSize: 4, cssClass: string.Empty, sortOrder: 400));

                UpdatePageTitle(aboutUsPage);
            }
        }

        private void CleanZone(int parentContentKey, string zoneName)
        {
            var items = _contentItemRepository.GetTable().Where(p => p.ParentKey == parentContentKey && p.Zone == zoneName);

            foreach (var item in items)
            {
                _contentItemRepository.Delete(item);
            }
        }

        private void CreateDcneContent(ContentItem twoColumn)
        {
            if (twoColumn != null)
            {
                var image = _contentCreatorHelper.InitializeItem<Image>(LeftColumnZone, twoColumn.ContentKey);
                image.AltText = "DCNE";
                image.ImageUrl = "/UserFiles/aboutus/dcne.png";
                SetTemplateView(image);
                _contentCreatorHelper.SaveItem(image, DateTimeOffset.Now);

                var richContent = _contentCreatorHelper.InitializeItem<RichContent>(RightColumnZone, twoColumn.ContentKey);
                richContent.Body = "<h2>DCNE</h2><p>In 1963, two brothers, Gregory Archie Kolligian and Jack H. Kolligian became the New England distributor for Carrier Air Conditioning. The brothers were already involved in the home heating oil business and saw great opportunities in the future of residential and commercial heating and air conditioning.</p><p>To this very day, Distributor Corporation of New England is one of the largest distributors of commercial and residential heating and air conditioning equipment, parts and supplies in New England. The company is still owned and operated by the Kolligian Family and prides itself on providing the highest in product quality and customer service.</p><p>Distributor Corporation of New England is headquartered in Malden, Massachusetts and covers Maine, New Hampshire, Rhode Island and Eastern Massachusetts. Satellite Branches are located in Westwood, MA - Plymouth, MA - Salem, NH - Portland, ME and Cranston, RI.</p>";
                SetTemplateView(richContent);
                _contentCreatorHelper.SaveItem(richContent, DateTimeOffset.Now);
            }
        }

        private void CreateMingContent(ContentItem twoColumn)
        {
            if (twoColumn != null)
            {
                var richContent = _contentCreatorHelper.InitializeItem<RichContent>(LeftColumnZone, twoColumn.ContentKey);
                richContent.Body = "<h2>Mingledorff&#39;s</h2><p>Since 1958, Mingledorff&#39;s has concentrated our core business as a wholesale distributor to air conditioning contractors and industrial accounts. We have expanded the business with the addition of numerous product lines and regional locations of offices, warehouses, and parts stores. Mingledorff&#39;s has been distributing the Carrier brand for 75 years and the Bryant brand for over 20 years, along with products from hundreds of other manufacturers of heating and air conditioning equipment.</p><p>Mingledorff&#39;s is not your typical distributor. In addition to stocking a wide variety of HVAC products, Mingledorff&#39;s employs technical staff to support the products they sell from cradle to grave. From working with end-users, architects and engineers to specify products, to helping engineers and contractors design HVAC systems, to quoting jobs, to commissioning systems, to troubleshooting and through to warranty support, Mingledorff&#39;s does it all. Recognizing this, a wise HVAC manufacturer once described Mingledorff&#39;s like this, &quot;Mingledorff&#39;s is what you would get if a great distributor and superb manufacturer&#39;s rep agency were to get married and have a child.&quot;</p><p>As a leading wholesale distributor of parts, equipment and supplies for heating, ventilation and air conditioning systems. Mingledorff&#39;s Distributors prides itself on its status as a total support organization for the HVAC industry.</p>";
                SetTemplateView(richContent);
                _contentCreatorHelper.SaveItem(richContent, DateTimeOffset.Now);

                var image = _contentCreatorHelper.InitializeItem<Image>(RightColumnZone, twoColumn.ContentKey);
                image.AltText = "Mingledorff's";
                image.ImageUrl = "/UserFiles/aboutus/mingledorffs.jpg";
                SetTemplateView(image);
                _contentCreatorHelper.SaveItem(image, DateTimeOffset.Now);
            }
        }

        private void CreatePageTitleWidget(int parentContentKey)
        {
            var pageTitle = _contentCreatorHelper.InitializeItem<PageTitle>(ContentZone, parentContentKey);
            pageTitle.SortOrder = 100;
            SetTemplateView(pageTitle);
            _contentCreatorHelper.SaveItem(pageTitle, DateTimeOffset.Now);
        }

        private void CreateTecContent(ContentItem twoColumn)
        {
            if (twoColumn != null)
            {
                var image = _contentCreatorHelper.InitializeItem<Image>(LeftColumnZone, twoColumn.ContentKey);
                image.AltText = "TEC";
                image.ImageUrl = "/UserFiles/aboutus/tec.png";
                SetTemplateView(image);
                _contentCreatorHelper.SaveItem(image, DateTimeOffset.Now);

                var richContent = _contentCreatorHelper.InitializeItem<RichContent>(RightColumnZone, twoColumn.ContentKey);
                richContent.Body = "<h2>TEC</h2><p>So Who Is TEC in a Nutshell?</p><ul><li>In business since 1935</li><li>Exclusive distributor of Carrier, Bryant and Payne in Chicagoland, NW Indiana, SW Michigan, and Milwaukee areas</li><li>Largest privately owned Carrier distributor in the U.S.</li><li>Dedicated professional Training Department</li><li>Largest, most experienced sales force in the Midwest</li><li>Inside Sales Engineers, LEED Accredited Pofessionals, Product Managers &amp; Specialists</li><li>Dedicated Customer Support Teams<ul><li>Sales</li><li>Marketing &amp; Graphic Design</li><li>Credit</li><li>Warranty</li><li>Customer Assurance</li><li>Technical Support</li></ul></li></ul>";
                SetTemplateView(richContent);
                _contentCreatorHelper.SaveItem(richContent, DateTimeOffset.Now);
            }
        }

        private ContentItem CreateTwoColumnWidget(int parentContentKey, int leftColumnSize, string cssClass, int sortOrder)
        {
            var twoColumn = _contentCreatorHelper.InitializeItem<TwoColumn>(ContentZone, parentContentKey);

            twoColumn.CssClass = cssClass;
            twoColumn.LeftColumnSize = leftColumnSize;
            twoColumn.RightColumnSize = 12 - leftColumnSize;
            twoColumn.SortOrder = sortOrder;
            SetTemplateView(twoColumn);

            _contentCreatorHelper.SaveItem(twoColumn, DateTimeOffset.Now);

            return _contentItemMapper.Map(twoColumn);
        }

        private void SetTemplateView(ContentItemModel contentItem)
        {
            contentItem.TemplateView = "Store/Standard";
        }

        private void UpdatePageTitle(ContentItem aboutUsPage)
        {
            var field = _contentItemFieldRepository.GetTable().FirstOrDefault(p => p.ContentKey == aboutUsPage.ContentKey && p.FieldName == "Title");
            if (field != null)
            {
                field.StringValue = "About the Collective";
                UnitOfWork.Save();
            }
        }
    }
}
