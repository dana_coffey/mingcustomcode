﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.WebFramework.Content;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.NotFoundError
{
    public class SetNotFoundErrorContentPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private const string ContentZone = "Content";

        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IRepository<ContentItemField> _contentItemFieldRepository;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IRepository<ContentItem> _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 11, 23, 16, 02, 00));

        public SetNotFoundErrorContentPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemFieldRepository = unitOfWork.GetRepository<ContentItemField>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
        }

        public override void Run()
        {
            var notFoundErrorPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == "NotFoundErrorPage");
            if (notFoundErrorPage != null)
            {
                CleanZone(notFoundErrorPage.ContentKey, ContentZone);

                CreatePageTitleWidget(notFoundErrorPage.ContentKey);
                CreateContent(notFoundErrorPage.ContentKey);
            }
        }

        private void CleanZone(int parentContentKey, string zoneName)
        {
            var items = _contentItemRepository.GetTable().Where(p => p.ParentKey == parentContentKey && p.Zone == zoneName);

            foreach (var item in items)
            {
                _contentItemRepository.Delete(item);
            }
        }

        private void CreateContent(int parentContentKey)
        {
            var richContent = _contentCreatorHelper.InitializeItem<RichContent>(ContentZone, parentContentKey);
            richContent.Body = "<p>The page your looking for was moved, removed, renamed or might have never existed.</p><a class=\"button\" href=\"/\">Back to homepage</a>";
            SetTemplateView(richContent);
            _contentCreatorHelper.SaveItem(richContent, DateTimeOffset.Now);
        }

        private void CreatePageTitleWidget(int parentContentKey)
        {
            var pageTitle = _contentCreatorHelper.InitializeItem<PageTitle>(ContentZone, parentContentKey);
            pageTitle.SortOrder = 100;
            SetTemplateView(pageTitle);
            _contentCreatorHelper.SaveItem(pageTitle, DateTimeOffset.Now);
        }

        private void SetTemplateView(ContentItemModel contentItem)
        {
            contentItem.TemplateView = "Store/Standard";
        }
    }
}
