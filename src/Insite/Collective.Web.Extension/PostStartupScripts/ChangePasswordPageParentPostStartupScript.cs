﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Account.Content;
using Insite.ContentLibrary.Pages;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.PostStartupScripts
{
    public class ChangePasswordPageParentPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IRepository<ContentItem> _contentItemRepository;
        private readonly IRepository<ContentPageState> _contentPageStateRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 09, 26, 11, 36, 52));

        public ChangePasswordPageParentPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentItemRepository = unitOfWork.GetRepository<ContentItem>();
            _contentPageStateRepository = unitOfWork.GetRepository<ContentPageState>();
        }

        public override void Run()
        {
            var changePasswordPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(ChangeAccountPasswordPage));
            var myAccountPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(MyAccountPage));

            if (changePasswordPage != null && myAccountPage != null)
            {
                changePasswordPage.ParentKey = myAccountPage.VariantKey;
                changePasswordPage.Path = $"{myAccountPage.Path},{changePasswordPage.VariantKey}";

                var changePasswordPageState = _contentPageStateRepository.GetTable().FirstOrDefault(p => p.ContentKey == changePasswordPage.ContentKey);
                var myAccountPageState = _contentPageStateRepository.GetTable().FirstOrDefault(p => p.ContentKey == myAccountPage.ContentKey);

                if (changePasswordPageState != null && myAccountPageState != null)
                {
                    changePasswordPageState.Path = $"{myAccountPageState.Path},{changePasswordPageState.VariantKey}-{changePasswordPageState.ContentKey}";
                }

                UnitOfWork.Save();
            }
        }
    }
}
