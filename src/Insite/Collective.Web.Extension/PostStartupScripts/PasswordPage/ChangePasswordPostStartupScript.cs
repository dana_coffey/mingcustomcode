﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Account.Content;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.PostStartupScripts.PasswordPage
{
    public class ChangePasswordPostStartupScript : BaseContentMigrationPostStartupScript, ICollectiveInjectableClass
    {
        private readonly IContentCreatorHelper _contentCreatorHelper;
        private readonly IContentItemMapper _contentItemMapper;
        private readonly IContentItemRepository _contentItemRepository;

        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 11, 13, 14, 01, 00));

        public ChangePasswordPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentCreatorHelper = DependencyLocator.Current.GetInstance<IContentCreatorHelper>();
            _contentItemMapper = DependencyLocator.Current.GetInstance<IContentItemMapper>();
            _contentItemRepository = unitOfWork.GetTypedRepository<IContentItemRepository>();
        }

        public override void Run()
        {
            var resetPasswordPage = _contentItemRepository.GetTable().FirstOrDefault(p => p.Class == nameof(ResetPasswordPage));
            if (resetPasswordPage != null)
            {
                var model = _contentItemMapper.Map(resetPasswordPage);

                model.SetValue(nameof(ResetPasswordPage.PageTitle), "Manage Password", FieldType.Contextual);

                _contentCreatorHelper.SaveItem(model, DateTimeOffset.Now);
            }
        }
    }
}
