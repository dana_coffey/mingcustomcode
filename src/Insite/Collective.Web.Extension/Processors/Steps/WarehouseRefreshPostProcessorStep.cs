﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Processors.Steps
{
    public class WarehouseRefreshPostProcessorStep : EntityPostProcessorStep<Warehouse, WarehouseProcessorModel>
    {
        private readonly IRepository<Warehouse> _warehouseRepository;

        public override DeleteAction DeleteAction => DeleteAction.Archive;

        public WarehouseRefreshPostProcessorStep(IUnitOfWork unitOfWork)
        {
            _warehouseRepository = unitOfWork.GetRepository<Warehouse>();
        }

        public override void ArchiveEntity(Warehouse entity)
        {
            if (!entity.DeactivateOn.HasValue)
            {
                entity.DeactivateOn = DateTimeOffset.Now;
            }
        }

        public override List<Warehouse> GetEntitiesToDeleteOrArchive(List<WarehouseProcessorModel> models)
        {
            var warehouseIds = models.Select(p => p.WarehouseId).ToList();
            return _warehouseRepository.GetTable().Where(x => !warehouseIds.Contains(x.Name)).ToList();
        }

        public override Warehouse GetEntity(WarehouseProcessorModel model)
        {
            return _warehouseRepository.GetTable().FirstOrDefault(p => p.Name == model.WarehouseId);
        }

        public override bool InsertEntity(WarehouseProcessorModel model, Warehouse entity)
        {
            entity.Name = model.WarehouseId;

            return true;
        }

        public override void UpdateEntity(WarehouseProcessorModel model, Warehouse entity)
        {
            var addresses = model.Address.Split(';');
            entity.Address1 = !string.IsNullOrEmpty(addresses.ElementAtOrDefault(0)) ? addresses.ElementAtOrDefault(0) : string.Empty;
            entity.Address2 = !string.IsNullOrEmpty(addresses.ElementAtOrDefault(1)) ? addresses.ElementAtOrDefault(1) : string.Empty;
            entity.City = model.City;
            entity.CountryId = Constants.Ids.Country.UnitedStates;
            entity.DeactivateOn = null;
            entity.Description = model.Name;
            entity.Phone = model.PhoneNumber;
            entity.PostalCode = model.ZipCode;
            entity.State = model.State;

            entity.SetProperty(Constants.CustomProperties.Warehouse.CanShip, model.CanShip.ToString());
            entity.SetProperty(Constants.CustomProperties.Warehouse.CanPickUp, model.CanPickUp.ToString());
            entity.SetProperty(Constants.CustomProperties.Warehouse.Email, model.Email);
            entity.SetProperty(Constants.CustomProperties.Warehouse.WarehouseCustomer, model.WarehouseCustomer);
            entity.SetProperty(Constants.CustomProperties.Warehouse.WarehouseType, model.WarehouseType);

            if (decimal.TryParse(model.Latitude, out _))
            {
                entity.SetProperty(Constants.CustomProperties.Warehouse.Latitude, model.Latitude);
            }

            if (decimal.TryParse(model.Longitude, out _))
            {
                entity.SetProperty(Constants.CustomProperties.Warehouse.Longitude, model.Longitude);
            }
        }
    }
}
