﻿using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Processors.Steps
{
    public class CustomerRefreshBillToPostProcessorStep : BaseCustomerRefreshPostProcessorStep
    {
        public override bool IsBatchSaving => true;

        public override Customer GetEntity(CustomerProcessorModel model)
        {
            return CustomerRepository.GetTable().FirstOrDefault(x => x.CustomerNumber == model.Number && string.IsNullOrEmpty(x.CustomerSequence));
        }

        public override void UpdateEntity(CustomerProcessorModel model, Customer entity)
        {
            base.UpdateEntity(model, entity);
            entity.IsBillTo = true;
            entity.SetProperty(Constants.CustomProperties.Customer.SpecificWarehouse, model.SpecificWarehouse);
        }
    }
}
