﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Processors.Steps
{
    public abstract class BaseCustomerRefreshPostProcessorStep : EntityPostProcessorStep<Customer, CustomerProcessorModel>
    {
        private Dictionary<string, Guid> _salespersonIds;
        public override bool IsBatchSaving => true;

        protected IRepository<Country> CountryRepository { get; }
        protected IRepository<Customer> CustomerRepository { get; }
        protected IRepository<Salesperson> SalespersonRepository { get; }
        protected IRepository<Warehouse> WarehouseRepository { get; }
        protected IRepository<Website> WebsiteRepository { get; }
        protected string WebsiteTaxExemptionCode { get; set; }

        protected BaseCustomerRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();
            var unitOfWork = unitOfWorkFactory.GetUnitOfWork();

            CountryRepository = unitOfWork.GetRepository<Country>();
            CustomerRepository = unitOfWork.GetRepository<Customer>();
            SalespersonRepository = unitOfWork.GetRepository<Salesperson>();
            WarehouseRepository = unitOfWork.GetRepository<Warehouse>();
            WebsiteRepository = unitOfWork.GetRepository<Website>();
        }

        public override void Initialize(List<CustomerProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            WebsiteTaxExemptionCode = WebsiteRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Id == SiteContext.Current.WebsiteDto.Id)?.TaxFreeTaxCode;
            _salespersonIds = SalespersonRepository.GetTableAsNoTracking().ToDictionary(x => x.SalespersonNumber.Trim().ToLower(), x => x.Id);
        }

        public override bool InsertEntity(CustomerProcessorModel model, Customer entity)
        {
            entity.CustomerNumber = model.Number;
            entity.ErpNumber = model.Number;

            return true;
        }

        public override void UpdateEntity(CustomerProcessorModel model, Customer entity)
        {
            entity.IsActive = model.Active;
            entity.City = model.City;
            entity.Address1 = model.Address1;
            entity.Address2 = model.Address2;
            entity.Address3 = model.Address3;
            entity.Country = CountryRepository.GetTable().FirstOrDefault(x => x.IsoCode2 == model.Country);
            entity.CompanyName = model.CompanyName;
            entity.IsShipTo = model.IsShipTo;
            entity.Email = DataValidatorHelper.ValidateEmail(model.Email) ? model.Email : string.Empty;
            entity.Phone = model.Phone;
            entity.PostalCode = model.PostalCode;
            entity.State = entity.Country?.States?.FirstOrDefault(x => x.Abbreviation == model.State);
            entity.DefaultWarehouse = WarehouseRepository.GetTable().FirstOrDefault(x => x.Name == model.Warehouse);
            entity.CustomerType = model.CustomerType;
            entity.TaxCode1 = model.IsTaxExempt ? WebsiteTaxExemptionCode ?? string.Empty : string.Empty;
            entity.PrimarySalespersonId = GetSalespersonId(model);
        }

        private Guid? GetSalespersonId(CustomerProcessorModel model)
        {
            if (!string.IsNullOrEmpty(model.SalespersonNumber))
            {
                if (_salespersonIds.TryGetValue(model.SalespersonNumber.Trim().ToLower(), out var salespersonId))
                {
                    return salespersonId;
                }

                JobLogger.Info($"Unable to find salesperson #{model.SalespersonNumber} for customer {model.Number}");
            }

            return null;
        }
    }
}
