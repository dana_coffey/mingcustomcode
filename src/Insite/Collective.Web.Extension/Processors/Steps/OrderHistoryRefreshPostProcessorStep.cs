﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.ApplicationDictionary;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors.Steps
{
    public class OrderHistoryRefreshPostProcessorStep : EntityPostProcessorStep<OrderHistory, OrderHistoryProcessorModel>, IDependency
    {
        private readonly ICurrencyRepository _currencyRepository;
        private readonly ICollectiveOrderConfirmationService _orderConfirmationService;
        private readonly IRepository<OrderHistoryLine> _orderHistoryLineRepository;
        private readonly IRepository<OrderHistoryLineSerialNumber> _orderHistoryLineSerialNumberRepository;
        private readonly IRepository<OrderHistory> _orderHistoryRepository;
        private List<PropertyDefinitionDto> _orderHistoryLineProperties;

        public override bool IsBatchSaving => true;

        public OrderHistoryRefreshPostProcessorStep(IUnitOfWorkFactory unitOfWorkFactory, ICurrencyRepository currencyRepository, ICollectiveOrderConfirmationService orderConfirmationService)
        {
            var unitOfWork = unitOfWorkFactory.GetUnitOfWork();

            _currencyRepository = currencyRepository;
            _orderConfirmationService = orderConfirmationService;

            _orderHistoryRepository = unitOfWork.GetRepository<OrderHistory>();
            _orderHistoryLineRepository = unitOfWork.GetRepository<OrderHistoryLine>();
            _orderHistoryLineSerialNumberRepository = unitOfWork.GetRepository<OrderHistoryLineSerialNumber>();
        }

        public override OrderHistory GetEntity(OrderHistoryProcessorModel model)
        {
            var webOrderNumber = GetWebOrderNumber(model);

            return _orderHistoryRepository.GetTable().FirstOrDefault(x => x.ErpOrderNumber == model.ErpOrderNumber)
                ?? _orderHistoryRepository.GetTable().FirstOrDefault(x => x.WebOrderNumber == webOrderNumber);
        }

        public override void Initialize(List<OrderHistoryProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            _orderHistoryLineProperties = PostProcessorHelper.GetEntityPropertyDefinitions(typeof(OrderHistoryLine));
        }

        public override bool InsertEntity(OrderHistoryProcessorModel model, OrderHistory entity)
        {
            JobLogger.Debug($"Adding A new entity : ERP #{model.ErpOrderNumber}");
            return true;
        }

        public override void UpdateEntity(OrderHistoryProcessorModel model, OrderHistory entity)
        {
            JobLogger.Debug("Entering Update Entity Process...");
            if (string.IsNullOrEmpty(entity.ErpOrderNumber))
            {
                entity.ErpOrderNumber = model.ErpOrderNumber;
            }

            if (string.IsNullOrEmpty(entity.CurrencyCode))
            {
                entity.CurrencyCode = _currencyRepository.GetDefault().CurrencyCode;
            }

            entity.SetProperty(Constants.CustomProperties.Order.Warehouse, model.Warehouse ?? string.Empty);
            entity.SetProperty(Constants.CustomProperties.Order.CustomerEmail, model.CustomerEmail ?? string.Empty);
            entity.SetProperty(Constants.CustomProperties.Order.CustomerReference, model.CustomerReference ?? string.Empty);

            entity.WebOrderNumber = GetWebOrderNumber(model);
            entity.CustomerPO = model.CustomerPo;
            entity.OrderDate = model.OrderDate ?? DateTime.MinValue;

            // Order was "submitting" and refresh now sets status as "submitted"
            var isOrderSubmitted = entity.Status.Equals(Constants.Insite.Order.Status.Submitting, StringComparison.InvariantCultureIgnoreCase);

            entity.Status = model.OrderStatus;
            entity.OrderTotal = model.OrderTotal;
            entity.ProductTotal = model.ProductTotal;
            entity.HandlingCharges = decimal.Zero;
            entity.ShippingCharges = model.ShippingCharges;
            entity.OtherCharges = model.OtherCharges;
            entity.ShipCode = model.ShipCode;
            entity.Salesperson = model.SalesRepresentative;
            entity.TaxAmount = model.TaxAmount;
            entity.Terms = model.Terms;

            entity.CustomerNumber = model.CustomerNumber;
            entity.BTCompanyName = model.BillToCompanyName;
            entity.BTAddress1 = model.BillToAddress1;
            entity.BTAddress2 = model.BillToAddress2;
            entity.BTCity = model.BillToCity;
            entity.BTState = model.BillToState;
            entity.BTPostalCode = model.BillToPostalCode;

            entity.CustomerSequence = model.CustomerSequence;
            entity.STCompanyName = model.ShipToCompanyName;
            entity.STAddress1 = model.ShipToAddress1;
            entity.STAddress2 = model.ShipToAddress2;
            entity.STCity = model.ShipToCity;
            entity.STState = model.ShipToState;
            entity.STPostalCode = model.ShipToPostalCode;

            // Don't ask
            entity.RequestedDeliveryDate = model.RequestedShipDate;
            entity.SetProperty(Constants.CustomProperties.Cart.ShipComplete, model.OrderDisposition == "s" ? bool.TrueString.ToLower() : bool.FalseString.ToLower());
            JobLogger.Debug("Entity properties updated...");
            UpdateOrderLines(model, entity);

            if (isOrderSubmitted)
            {
                JobLogger.Debug("Entering email confirmation process...");
                _orderConfirmationService.SendConfirmationEmailByErpOrderNumber(entity.ErpOrderNumber);
            }
        }

        private static string GetWebOrderNumber(OrderHistoryProcessorModel model)
        {
            if (!string.IsNullOrEmpty(model.WebOrderNumber))
            {
                return model.ErpOrderSuffix != "00" ? $"{model.WebOrderNumber}-{model.ErpOrderSuffix}" : model.WebOrderNumber;
            }

            return model.ErpOrderNumber;
        }

        private OrderHistoryLine GetOrCreateOrderHistoryLine(OrderHistory entity, OrderHistoryLineProcessorModel model)
        {
            JobLogger.Debug("Entering Get Or Create Order History Line...");
            var lineEntity = entity.OrderHistoryLines.FirstOrDefault(x => x.ProductErpNumber == model.ProductNumber && x.CustomProperties.FirstOrDefault(y => y.Name == Constants.CustomProperties.OrderHistory.ErpLineNumber)?.Value == model.LineNumber.ToString("0"));

            if (lineEntity == null)
            {
                JobLogger.Debug("Retrive existing Line Entity...");
                lineEntity = entity.OrderHistoryLines.FirstOrDefault(x => x.ProductErpNumber == model.ProductNumber && x.CustomProperties.FirstOrDefault(y => y.Name == Constants.CustomProperties.OrderHistory.ErpLineNumber)?.Value == null);
            }

            if (lineEntity == null)
            {
                JobLogger.Debug("No Line Entity find Creating a new line...");
                lineEntity = _orderHistoryLineRepository.Create();
                PostProcessorHelper.SetEntityDefaultValues(lineEntity, _orderHistoryLineProperties);
                lineEntity.LineNumber = (entity.OrderHistoryLines.OrderByDescending(x => x.LineNumber).FirstOrDefault()?.LineNumber ?? 0) + 1;
                lineEntity.SetProperty(Constants.CustomProperties.OrderHistory.ErpLineNumber, model.LineNumber.ToString("0"));
                lineEntity.ReleaseNumber = decimal.Zero;

                entity.OrderHistoryLines.Add(lineEntity);
            }

            return lineEntity;
        }

        private void UpdateOrderLines(OrderHistoryProcessorModel model, OrderHistory entity)
        {
            JobLogger.Debug("Entering Order Line update process...");

            var lineEntityIds = _orderHistoryLineRepository.GetTableAsNoTracking().Where(x => x.OrderHistoryId == entity.Id).Select(x => x.Id).ToList();
            if (lineEntityIds.Count > 0)
            {
                // Remove order history lines serial numbers entries
                JobLogger.Debug("Remove Current Serial Number on database...");
                _orderHistoryLineSerialNumberRepository.BatchDelete(x => lineEntityIds.Contains(x.OrderHistoryLineId), 1000);
                JobLogger.Debug("Serial Number removed.");
            }

            JobLogger.Debug("Updating Lines...");
            foreach (var line in model.Lines)
            {
                var lineEntity = GetOrCreateOrderHistoryLine(entity, line);
                if (lineEntity != null)
                {
                    JobLogger.Debug("Adding/updating properties of Line...");
                    lineEntity.CustomerNumber = model.CustomerNumber;
                    lineEntity.CustomerSequence = model.CustomerSequence;
                    lineEntity.LineType = Constants.Insite.Order.LineType.Product;
                    lineEntity.ProductErpNumber = line.ProductNumber;
                    lineEntity.Description = string.IsNullOrEmpty(lineEntity.Description) ? line.ProductDescription : lineEntity.Description;
                    lineEntity.QtyOrdered = line.QtyOrdered;
                    lineEntity.QtyShipped = line.QtyShipped;
                    lineEntity.UnitOfMeasure = line.UnitOfMeasure;
                    lineEntity.Notes = line.Notes;
                    lineEntity.LastShipDate = model.ShipDate;
                    lineEntity.UnitRegularPrice = line.UnitPrice;
                    lineEntity.UnitNetPrice = line.UnitPrice;
                    lineEntity.TotalRegularPrice = line.LineTotal;
                    lineEntity.TotalNetPrice = line.LineTotal;

                    JobLogger.Debug("Manage line serial numbers...");
                    if (line.SerialNumbers != null && line.SerialNumbers.Count > 0)
                    {
                        foreach (var serialNumber in line.SerialNumbers.Where(x => !string.IsNullOrEmpty(x.SerialNo)))
                        {
                            JobLogger.Debug($"Insert Serial Number : {serialNumber}...");
                            _orderHistoryLineSerialNumberRepository.Insert(new OrderHistoryLineSerialNumber
                            {
                                OrderHistoryLineId = lineEntity.Id,
                                SerialNo = serialNumber.SerialNo.ToUpperInvariant(),
                                SeqNo = serialNumber.SeqNo
                            });
                        }
                    }
                }
            }

            JobLogger.Debug("Line Update done.");
        }
    }
}
