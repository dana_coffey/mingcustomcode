﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Collective.Web.Extension.Helpers;
using Insite.Common.Dependencies;
using Insite.Core.ApplicationDictionary;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.EnumTypes;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors.Steps
{
    public class AttributeRefreshPostProcessorStep : SimplePostProcessorStep<AttributeProcessorModel>
    {
        private readonly IRepository<AttributeType> _attributeTypeRepository;
        private readonly IRepository<AttributeValue> _attributeValueRepository;
        private readonly IRepository<Language> _languageRepository;
        private readonly IRepository<TranslationDictionary> _translationRepository;
        private List<PropertyDefinitionDto> _attributeTypeProperties;
        private List<PropertyDefinitionDto> _attributeValueProperties;

        public override bool IsBatchSaving => false;

        public AttributeRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();

            _attributeTypeRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<AttributeType>();
            _attributeValueRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<AttributeValue>();
            _translationRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<TranslationDictionary>();
            _languageRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<Language>();
        }

        public override void Initialize(List<AttributeProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            _attributeTypeProperties = PostProcessorHelper.GetEntityPropertyDefinitions(typeof(AttributeType));
            _attributeValueProperties = PostProcessorHelper.GetEntityPropertyDefinitions(typeof(AttributeValue));
        }

        public override SimplePostProcessorStepProcessStats Process(AttributeProcessorModel model)
        {
            var stats = base.Process(model);
            var attributeTypeEntity = _attributeTypeRepository.GetTable().FirstOrDefault(x => x.Name == model.AttributeTypeName);

            if (attributeTypeEntity == null)
            {
                attributeTypeEntity = CreateAttributeType(model);
                stats.RecordsAdded++;
            }

            UpsertAttributeValue(model, attributeTypeEntity, attributeTypeEntity.AttributeValues?.FirstOrDefault(x => x.Value == model.OptionName), stats);

            return stats;
        }

        private AttributeType CreateAttributeType(AttributeProcessorModel model)
        {
            var entity = _attributeTypeRepository.Create();
            PostProcessorHelper.SetEntityDefaultValues(entity, _attributeTypeProperties);

            entity.IsActive = true;
            entity.IsFilter = false;
            entity.Label = ProductFieldHelper.GetLocalizedFieldName(ProductFieldHelper.GetProductFields().FirstOrDefault(x => x.FieldCvlId == model.AttributeTypeName));
            entity.Name = model.AttributeTypeName;

            _attributeTypeRepository.Insert(entity);

            return entity;
        }

        private void UpsertAttributeValue(AttributeProcessorModel model, AttributeType attributeTypeEntity, AttributeValue attributeValueEntity, SimplePostProcessorStepProcessStats stats)
        {
            var adding = attributeValueEntity == null;

            if (adding && !model.OptionIsEnabled)
            {
                return;
            }

            if (adding)
            {
                attributeValueEntity = _attributeValueRepository.Create();
                PostProcessorHelper.SetEntityDefaultValues(attributeValueEntity, _attributeValueProperties);

                attributeValueEntity.Value = model.OptionName;
                attributeValueEntity.AttributeType = attributeTypeEntity;

                _attributeValueRepository.Insert(attributeValueEntity);
            }

            attributeValueEntity.IsActive = model.OptionIsEnabled;
            TranslationHelper.Translate(_translationRepository, _languageRepository, model.OptionName, TranslationDictionarySource.AttributeValue, model.OptionDescription ?? new Dictionary<string, string>());

            if (adding)
            {
                stats.RecordsAdded++;
            }
            else
            {
                stats.RecordsModified++;
            }
        }
    }
}
