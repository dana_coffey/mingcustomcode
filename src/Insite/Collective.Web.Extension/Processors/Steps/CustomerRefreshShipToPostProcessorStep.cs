﻿using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Processors.Steps
{
    public class CustomerRefreshShipToPostProcessorStep : BaseCustomerRefreshPostProcessorStep
    {
        public override bool IsBatchSaving => true;

        public override Customer GetEntity(CustomerProcessorModel model)
        {
            return CustomerRepository.GetTable().FirstOrDefault(x => x.CustomerNumber == model.Number && x.CustomerSequence == model.ShipTo);
        }

        public override bool InsertEntity(CustomerProcessorModel model, Customer entity)
        {
            if (!base.InsertEntity(model, entity))
            {
                return false;
            }

            entity.CustomerSequence = model.ShipTo;
            entity.ErpSequence = model.ShipTo;

            return true;
        }

        public override void UpdateEntity(CustomerProcessorModel model, Customer entity)
        {
            base.UpdateEntity(model, entity);
            entity.IsBillTo = false;

            var billTo = CustomerRepository.GetTable().FirstOrDefault(x => x.CustomerNumber == model.Number && string.IsNullOrEmpty(x.CustomerSequence));
            entity.SetProperty(Constants.CustomProperties.Customer.SpecificWarehouse, model.SpecificWarehouse);
            entity.CustomerType = billTo?.CustomerType ?? string.Empty;
        }
    }
}
