﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Extensions;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Integration.Helpers;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Collective.Web.Extension.Helpers;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Processors.Steps
{
    public class ProductRefreshPostProcessorStep : EntityPostProcessorStep<Product, ProductProcessorModel>
    {
        private List<Category> _categories;
        private List<ProductBarcode> _productBarcodes;
        private List<string> _urlSegments;

        protected IRepository<AttributeType> AttributeTypeRepository { get; }
        protected IRepository<AttributeValue> AttributeValueRepository { get; }
        protected IRepository<Category> CategoryRepository { get; }
        protected IRepository<Document> DocumentRepository { get; }
        protected IRepository<Language> LanguageRepository { get; }
        protected IRepository<Persona> PersonaRepository { get; }
        protected IRepository<ProductBarcode> ProductBarcodeRepository { get; }
        protected IRepository<ProductImage> ProductImageRepository { get; }
        protected IRepository<ProductLinkEntity> ProductLinkRepository { get; }
        protected IRepository<Product> ProductRepository { get; }
        protected IRepository<TranslationProperty> TranslationRepository { get; }
        protected IUnitOfWork UnitOfWork { get; }

        public ProductRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();
            UnitOfWork = unitOfWorkFactory.GetUnitOfWork();

            AttributeTypeRepository = UnitOfWork.GetRepository<AttributeType>();
            AttributeValueRepository = UnitOfWork.GetRepository<AttributeValue>();
            CategoryRepository = UnitOfWork.GetRepository<Category>();
            DocumentRepository = UnitOfWork.GetRepository<Document>();
            LanguageRepository = UnitOfWork.GetRepository<Language>();
            PersonaRepository = UnitOfWork.GetRepository<Persona>();
            ProductRepository = UnitOfWork.GetRepository<Product>();
            ProductBarcodeRepository = UnitOfWork.GetRepository<ProductBarcode>();
            ProductLinkRepository = UnitOfWork.GetRepository<ProductLinkEntity>();
            ProductImageRepository = UnitOfWork.GetRepository<ProductImage>();
            TranslationRepository = UnitOfWork.GetRepository<TranslationProperty>();
        }

        public override Product GetEntity(ProductProcessorModel model)
        {
            return ProductRepository.GetTable().FirstOrDefault(p => p.ErpNumber == model.ErpNumber);
        }

        public override void Initialize(List<ProductProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            _urlSegments = ProductRepository.GetTableAsNoTracking().Select(x => x.UrlSegment).ToList();
            _productBarcodes = ProductBarcodeRepository.GetTableAsNoTracking().ToList();
        }

        public override void InitializeBatch(List<ProductProcessorModel> models)
        {
            var modelCategories = models.Where(x => x.Categories != null).SelectMany(x => x.Categories).Distinct();
            _categories = CategoryRepository.GetTable().Where(x => modelCategories.Contains(x.Name)).ToList();
        }

        public override bool InsertEntity(ProductProcessorModel model, Product entity)
        {
            if (model.IsEnabled)
            {
                entity.ErpNumber = model.ErpNumber;

                var productName = GetProductName(model);
                var urlSegment = UrlHelper.CleanUrl(string.Join("-", TranslationHelper.GetDefaultDictionaryTranslation(productName), model.ErpNumber));

                var urlSegmentAddon = string.Empty;
                if (_urlSegments.Contains(urlSegment))
                {
                    urlSegmentAddon = $"-{Guid.NewGuid().ToString().Substring(0, 8)}";
                }

                urlSegment += urlSegmentAddon;
                _urlSegments.Add(urlSegment);

                entity.UrlSegment = urlSegment;
                TranslationHelper.Translate(TranslationRepository, LanguageRepository, entity.Id, nameof(Product.UrlSegment), nameof(Product), productName, x => UrlHelper.CleanUrl(string.Join("-", x, model.ErpNumber, urlSegmentAddon)));

                return true;
            }

            return false;
        }

        public override void UpdateEntity(ProductProcessorModel model, Product entity)
        {
            if (!model.IsEnabled)
            {
                entity.DeactivateOn = DateTimeOffset.Now;
            }
            else
            {
                entity.ContentManager = ContentManagerHelper.Create(nameof(Product));
                entity.DeactivateOn = null;
                entity.ManufacturerItem = GetProductManufacturerItem(model);
                entity.Name = TranslationHelper.GetDefaultDictionaryTranslation(model.Name);
                entity.MetaDescription = TranslationHelper.GetDefaultDictionaryTranslation(model.MetaDescription);
                entity.TrackInventory = true;

                TranslationHelper.Translate(TranslationRepository, LanguageRepository, entity.Id, nameof(Product.MetaDescription), nameof(Product), model.MetaDescription, x => x.Replace("\n", " "));
                TranslationHelper.Translate(TranslationRepository, LanguageRepository, entity.Id, nameof(Product.Name), nameof(Product), model.Name);
                TranslationHelper.TranslateContent(entity.ContentManager, LanguageRepository, PersonaRepository, model.Description);

                entity.SetProperty(Constants.CustomProperties.Product.IsCatalogProduct, GetIsCatalogProduct(model).ToString().ToLower());
                entity.SetProperty(Constants.CustomProperties.Product.IsSouthCarolinaSolidWasteTaxable, GetIsSouthCarolinaSolidWasteTaxable(model).ToString().ToLower());

                SetProductShortDescription(model, entity);
                SetProductBarcodes(model, entity);
                SetProductCategories(model, entity);
                SetProductImages(model, entity);
                SetProductDocuments(model, entity);
                ProcessCustomFields(model, entity);
                SetProductLinks(model);
                SetProductStatus(model, entity);
                SetProductUnitOfMeasure(model, entity);
            }
        }

        private void CleanupCategoriesAttributes()
        {
            UnitOfWork.DataProvider.SqlExecuteNonQuery(@"
                delete from
                    CategoryAttributeType
                where
                    Id in (
                        select Id
                        from CategoryAttributeType cat
                        where
                            not exists(
                                select 0
                                from CategoryProduct cp
                                    inner join ProductAttributeValue pav on pav.ProductId = cp.ProductId
                                    inner join AttributeValue av on av.Id = pav.AttributeValueId and av.AttributeTypeId = cat.AttributeTypeId
                                where
                                    cp.CategoryId = cat.CategoryId
                            )
                    )
            ");
        }

        private List<string> GetBarcodes(ProductProcessorModel model)
        {
            var barcodes = string.Empty;
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ItemUpc) ?? false)
            {
                var itemUpc = model.CustomFields[Constants.ProductFields.FieldNames.ItemUpc];
                if (itemUpc != null)
                {
                    barcodes = TranslationHelper.GetDefaultDictionaryTranslation(itemUpc);
                }
            }

            return string.IsNullOrEmpty(barcodes) ? new List<string>() : barcodes.Split(';').Select(x => x.Trim().ToUpper()).Distinct().ToList();
        }

        private bool GetIsCatalogProduct(ProductProcessorModel model)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ItemIsCatalogProduct) ?? false)
            {
                var isCatalogProduct = model.CustomFields[Constants.ProductFields.FieldNames.ItemIsCatalogProduct];
                if (isCatalogProduct != null)
                {
                    return TranslationHelper.GetDefaultDictionaryTranslation(isCatalogProduct).Equals(true.ToString(), StringComparison.InvariantCultureIgnoreCase);
                }
            }

            return false;
        }

        private bool GetIsSouthCarolinaSolidWasteTaxable(ProductProcessorModel model)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ProductIsSouthCarolinaSolidWasteTaxable) ?? false)
            {
                var isSouthCarolinaSolidWasteTaxable = model.CustomFields[Constants.ProductFields.FieldNames.ProductIsSouthCarolinaSolidWasteTaxable];
                if (isSouthCarolinaSolidWasteTaxable != null)
                {
                    return TranslationHelper.GetDefaultDictionaryTranslation(isSouthCarolinaSolidWasteTaxable).Equals(true.ToString(), StringComparison.InvariantCultureIgnoreCase);
                }
            }

            return false;
        }

        private string GetProductManufacturerItem(ProductProcessorModel model)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ItemUrn) ?? false)
            {
                var itemUrn = model.CustomFields[Constants.ProductFields.FieldNames.ItemUrn];
                if (itemUrn != null)
                {
                    return TranslationHelper.GetDefaultDictionaryTranslation(itemUrn);
                }
            }

            return string.Empty;
        }

        private Dictionary<string, string> GetProductName(ProductProcessorModel model)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ProductNameOverride) ?? false)
            {
                if (model.CustomFields[Constants.ProductFields.FieldNames.ProductNameOverride] != null)
                {
                    return model.CustomFields[Constants.ProductFields.FieldNames.ProductNameOverride];
                }
            }

            return model.Name;
        }

        private bool IsObsolete(ProductProcessorModel model)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ItemObsolete) ?? false)
            {
                var obsolete = model.CustomFields[Constants.ProductFields.FieldNames.ItemObsolete];
                if (obsolete != null)
                {
                    return ParseHelper.ParseBoolean(TranslationHelper.GetDefaultDictionaryTranslation(obsolete), false);
                }
            }

            return false;
        }

        private void ProcessCustomFields(ProductProcessorModel model, Product entity)
        {
            entity.SetProperty(Constants.CustomProperties.Product.CustomFields, JsonConvert.SerializeObject(model.CustomFields));

            var attributeValuesToRemove = entity.AttributeValues.ToList();

            var productFields = ProductFieldHelper.GetProductFields();
            foreach (var field in model.CustomFields)
            {
                var productField = productFields.FirstOrDefault(x => x.FieldId.Equals(field.Key, StringComparison.OrdinalIgnoreCase));
                if (productField?.DataType == Constants.ProductFields.DataTypes.Cvl)
                {
                    if (field.Value == null || !field.Value.TryGetValue(string.Empty, out var fieldValue))
                    {
                        continue;
                    }

                    var attributeTypeId = AttributeTypeRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Name == productField.FieldCvlId)?.Id;
                    if (attributeTypeId != null)
                    {
                        foreach (var value in fieldValue.Split(';'))
                        {
                            if (attributeValuesToRemove.RemoveAll(x => x.AttributeTypeId == attributeTypeId && x.Value == value) == 0)
                            {
                                var attributeValue = AttributeValueRepository.GetTable().FirstOrDefault(x => x.AttributeTypeId == attributeTypeId && x.Value == value);
                                if (attributeValue != null)
                                {
                                    entity.AttributeValues.Add(attributeValue);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var attributeValue in attributeValuesToRemove)
            {
                entity.AttributeValues.Remove(attributeValue);
            }

            foreach (var attributeType in entity.AttributeValues.Select(x => x.AttributeType).Distinct())
            {
                foreach (var category in entity.Categories.Where(x => x.CategoryAttributeTypes.All(p => p.AttributeType != attributeType)))
                {
                    category.CategoryAttributeTypes.Add(new CategoryAttributeType
                    {
                        AttributeType = attributeType,
                        Category = category,
                        Id = Guid.NewGuid(),
                        IsActive = true
                    });
                }
            }

            CleanupCategoriesAttributes();
        }

        private void SetProductBarcodes(ProductProcessorModel model, Product entity)
        {
            var barcodes = GetBarcodes(model);

            foreach (var barcode in barcodes)
            {
                if (_productBarcodes.FirstOrDefault(x => x.Barcode.Equals(barcode, StringComparison.InvariantCultureIgnoreCase)) == null)
                {
                    ProductBarcodeRepository.Insert(new ProductBarcode
                    {
                        Id = Guid.NewGuid(),
                        ProductId = entity.Id,
                        Barcode = barcode
                    });
                }
            }

            ProductBarcodeRepository.BatchDelete(x => x.ProductId == entity.Id && !barcodes.Contains(x.Barcode), 100);
        }

        private void SetProductCategories(ProductProcessorModel model, Product entity)
        {
            entity.Categories.Clear();

            foreach (var categoryFullPath in model.Categories)
            {
                var category = _categories.FirstOrDefault(p => p.Name == categoryFullPath);
                if (category != null)
                {
                    entity.Categories.Add(category);
                }
                else
                {
                    JobLogger.Info($"Category '{categoryFullPath}' in product '{model.ErpNumber}' not found.");
                }
            }
        }

        private void SetProductDocuments(ProductProcessorModel model, Product entity)
        {
            DocumentRepository.BatchDelete(x => x.ParentId == entity.Id, 100);
            entity.Documents.Clear();

            foreach (var document in model.Documents)
            {
                var documentName = Path.GetFileNameWithoutExtension(document);
                if (!string.IsNullOrEmpty(documentName))
                {
                    entity.Documents.Add(new Document
                    {
                        Name = documentName.Substring(0, Math.Min(documentName.Length, Constants.Insite.Documents.MaxNameLength)),
                        Id = Guid.NewGuid(),
                        ParentId = entity.Id,
                        FilePath = $"{Constants.Urls.Resources}{document}",
                        ParentTable = nameof(Product)
                    });
                }
            }
        }

        private void SetProductImages(ProductProcessorModel model, Product entity)
        {
            ProductImageRepository.BatchDelete(x => x.ProductId == entity.Id, 100);
            entity.ProductImages.Clear();

            var order = 1;
            foreach (var image in model.Images)
            {
                entity.ProductImages.Add(new ProductImage
                {
                    Id = Guid.NewGuid(),
                    Name = image,
                    AltText = image,
                    SortOrder = order++,
                    LargeImagePath = $"{Constants.Urls.Resources}{ImageHelper.GetImageFileName(image, Constants.AppSettings.Image.SuffixLarge)}",
                    MediumImagePath = $"{Constants.Urls.Resources}{ImageHelper.GetImageFileName(image, Constants.AppSettings.Image.SuffixMedium)}",
                    SmallImagePath = $"{Constants.Urls.Resources}{ImageHelper.GetImageFileName(image, Constants.AppSettings.Image.SuffixSmall)}"
                });
            }
        }

        private void SetProductLinks(ProductProcessorModel model)
        {
            var productLinkEntities = ProductLinkRepository.GetTable().Where(x => x.ErpNumber == model.ErpNumber).ToList();

            var toRemoveEntities = productLinkEntities.Where(linkEntity => !model.Links.Any(x => x.ErpNumber == linkEntity.LinkTo && x.Type == linkEntity.Type)).ToList();
            foreach (var toRemoveEntity in toRemoveEntities)
            {
                ProductLinkRepository.Delete(toRemoveEntity);
            }

            foreach (var linkGroup in model.Links.GroupBy(p => p.Type))
            {
                var order = 0;

                foreach (var linkModel in linkGroup)
                {
                    var dbLink = productLinkEntities.FirstOrDefault(x => x.LinkTo == linkModel.ErpNumber && x.Type == linkModel.Type);
                    if (dbLink != null)
                    {
                        dbLink.Order = order++;
                    }
                    else
                    {
                        ProductLinkRepository.Insert(new ProductLinkEntity
                        {
                            ErpNumber = model.ErpNumber,
                            LinkTo = linkModel.ErpNumber,
                            Order = order++,
                            Type = linkModel.Type,
                        });
                    }
                }
            }
        }

        private void SetProductShortDescription(ProductProcessorModel model, Product entity)
        {
            var productName = GetProductName(model);

            var defaultTranslation = TranslationHelper.GetDefaultDictionaryTranslation(productName);
            if (string.IsNullOrEmpty(defaultTranslation))
            {
                defaultTranslation = TranslationHelper.GetDefaultDictionaryTranslation(model.Name);
            }

            entity.ShortDescription = defaultTranslation;
            TranslationHelper.Translate(TranslationRepository, LanguageRepository, entity.Id, nameof(Product.ShortDescription), nameof(Product), productName, x => string.IsNullOrEmpty(x) ? defaultTranslation : x);
        }

        private void SetProductStatus(ProductProcessorModel model, Product entity)
        {
            // if entity is obsolete, set status obsolete and skip all superceded stuff
            var status = entity.GetProperty(Constants.CustomProperties.Product.StatusField, string.Empty);

            if (IsObsolete(model))
            {
                entity.SetProperty(Constants.CustomProperties.Product.StatusField, Constants.CustomProperties.Product.Status.Obsolete);
            }
            else
            {
                if (model.Links.Any(x => x.Type.Equals(Constants.CustomProperties.Product.Status.Superseded, StringComparison.InvariantCultureIgnoreCase)))
                {
                    if (!status.Equals(Constants.CustomProperties.Product.Status.Superseded, StringComparison.InvariantCultureIgnoreCase) && !status.Equals(Constants.CustomProperties.Product.Status.Obsolete, StringComparison.InvariantCultureIgnoreCase))
                    {
                        entity.SetProperty(Constants.CustomProperties.Product.StatusField, Constants.CustomProperties.Product.Status.SupersededWithInventory);
                    }
                }
                else
                {
                    if (!status.Equals(Constants.CustomProperties.Product.Status.Obsolete, StringComparison.InvariantCultureIgnoreCase))
                    {
                        entity.SetProperty(Constants.CustomProperties.Product.StatusField, string.Empty);
                    }
                }
            }
        }

        private void SetProductUnitOfMeasure(ProductProcessorModel model, Product entity)
        {
            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.UnitOfMeasure) ?? false)
            {
                entity.UnitOfMeasure = TranslationHelper.GetDefaultDictionaryTranslation(model.CustomFields[Constants.ProductFields.FieldNames.UnitOfMeasure]);
            }

            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.UnitOfMeasureDescription) ?? false)
            {
                var descriptions = model.CustomFields[Constants.ProductFields.FieldNames.UnitOfMeasureDescription];
                if (descriptions != null)
                {
                    entity.UnitOfMeasureDescription = TranslationHelper.GetDefaultDictionaryTranslation(model.CustomFields[Constants.ProductFields.FieldNames.UnitOfMeasureDescription]);
                    TranslationHelper.Translate(TranslationRepository, LanguageRepository, entity.Id, nameof(Product.UnitOfMeasureDescription), nameof(Product), descriptions);
                }
            }

            if (model.CustomFields?.ContainsKey(Constants.ProductFields.FieldNames.RoundBy) ?? false)
            {
                entity.MultipleSaleQty = ParseHelper.ParseInt(TranslationHelper.GetDefaultDictionaryTranslation(model.CustomFields[Constants.ProductFields.FieldNames.RoundBy]), 1);
                entity.RoundingRule = entity.MultipleSaleQty > 1 ? Constants.Insite.RoundingRules.MultipleOnly : string.Empty;
            }
        }
    }
}
