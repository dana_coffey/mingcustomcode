﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Integration.Helpers;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Common.Dependencies;
using Insite.Common.DynamicLinq;
using Insite.Core.ApplicationDictionary;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors.Steps
{
    public class CategoryRefreshPostProcessorStep : SimplePostProcessorStep<CategoryProcessorModel>
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Language> _languageRepository;
        private readonly IRepository<TranslationProperty> _translationRepository;

        private List<PropertyDefinitionDto> _categoryProperties;

        public override bool IsBatchSaving => false;
        public override bool UseTransaction => true;

        public CategoryRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();

            _categoryRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<Category>();
            _translationRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<TranslationProperty>();
            _languageRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<Language>();
        }

        public override void Initialize(List<CategoryProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            _categoryProperties = PostProcessorHelper.GetEntityPropertyDefinitions(typeof(Category));
        }

        public override SimplePostProcessorStepProcessStats Process(CategoryProcessorModel model)
        {
            var stats = base.Process(model);
            var category = GetCategory(model);

            if (category == null && model.IsEnabled)
            {
                CreateCategory(model);
                stats.RecordsAdded++;
            }
            else if (category != null && model.IsEnabled)
            {
                UpdateCategory(model, category);
                stats.RecordsModified++;
            }
            else if (category != null && !model.IsEnabled)
            {
                DeleteCategory(category);
                stats.RecordsDeleted++;
            }

            return stats;
        }

        private void CreateCategory(CategoryProcessorModel model)
        {
            var entity = _categoryRepository.Create();
            PostProcessorHelper.SetEntityDefaultValues(entity, _categoryProperties);

            entity.ActivateOn = DateTimeOffset.Now;
            entity.ContentManager = ContentManagerHelper.Create(nameof(Category));
            UpdateCategory(model, entity);

            _categoryRepository.Insert(entity);
        }

        private void DeleteCategory(Category entity)
        {
            var categoriesToDelete = new List<Category>();
            FillSubCategoriesToDelete(categoriesToDelete, entity);
            categoriesToDelete.ForEach(_categoryRepository.Delete);
        }

        private void FillSubCategoriesToDelete(List<Category> categoriesToDelete, Category category)
        {
            if (category != null)
            {
                foreach (var subCategory in category.SubCategories)
                {
                    FillSubCategoriesToDelete(categoriesToDelete, subCategory);
                }

                categoriesToDelete.Add(category);
            }
        }

        private Category GetCategory(CategoryProcessorModel model)
        {
            if (!string.IsNullOrEmpty(model.PreviousPath))
            {
                // A previous category path is almost always sent even if the path did not change, so we fetch the "new" position before the previous path
                return GetCategoryFromPath(model.Path) ?? GetCategoryFromPath(model.PreviousPath);
            }

            return GetCategoryFromPath(model.Path);
        }

        private Category GetCategoryFromPath(string path)
        {
            return _categoryRepository.GetTable().FirstOrDefault(x => x.Name == path);
        }

        private Guid? GetParentCategoryId(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                var parentCategory = GetCategoryFromPath(path);
                if (parentCategory == null)
                {
                    throw new Exception($"Could not find the parent category. ({path})");
                }

                return parentCategory.Id;
            }

            return null;
        }

        private void SetCategoryImage(CategoryProcessorModel model, Category entity)
        {
            var image = model.Images.FirstOrDefault();
            if (image != null)
            {
                entity.LargeImagePath = $"{Constants.Urls.Resources}{ImageHelper.GetImageFileName(image, Constants.AppSettings.Image.SuffixLarge)}";
                entity.SmallImagePath = $"{Constants.Urls.Resources}{ImageHelper.GetImageFileName(image, Constants.AppSettings.Image.SuffixMedium)}";
            }
            else
            {
                entity.LargeImagePath = string.Empty;
                entity.SmallImagePath = string.Empty;
            }
        }

        private void UpdateCategory(CategoryProcessorModel model, Category entity)
        {
            // Rename the category and childs
            if (entity.Name != model.Path)
            {
                UpdateChildrenCategories(entity, model.Path);
            }

            entity.DeactivateOn = model.IsEnabled ? (DateTimeOffset?)null : DateTimeOffset.Now;
            entity.Name = model.Path;
            entity.ParentId = GetParentCategoryId(model.ParentPath);
            entity.UrlSegment = UrlHelper.CleanUrl(model.Name);
            entity.WebsiteId = model.WebsiteId;
            entity.ShortDescription = TranslationHelper.GetDefaultDictionaryTranslation(model.ShortDescription);
            TranslationHelper.Translate(_translationRepository, _languageRepository, entity.Id, nameof(Category.ShortDescription), nameof(Category), model.ShortDescription);
            SetCategoryImage(model, entity);
            UpdateCategorySortOrder(entity, model.SortOrder);
        }

        private void UpdateCategorySortOrder(Category category, int newSortOrder)
        {
            if (category.SortOrder != newSortOrder)
            {
                category.SortOrder = newSortOrder;
                var siblingCategories = _categoryRepository.GetTable().Where(x => x.ParentId == category.ParentId && x.Id != category.Id).OrderBy(x => x.SortOrder).ToList();
                var sortOrder = 1;
                foreach (var siblingCategory in siblingCategories)
                {
                    siblingCategory.SortOrder = sortOrder == newSortOrder ? ++sortOrder : sortOrder;
                    sortOrder++;
                }
            }
        }

        private void UpdateChildrenCategories(Category rootCategory, string newName)
        {
            var categories = _categoryRepository.GetTable().Where(x => x.Name.StartsWith(rootCategory.Name + "/"));
            foreach (var category in categories)
            {
                category.Name = $"{newName}/{category.Name.Substring(rootCategory.Name.Length + 1)}";
            }
        }
    }
}
