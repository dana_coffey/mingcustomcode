﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Integration.WebService.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Processors.Steps
{
    public class ProductFieldRefreshPostProcessorStep : SimplePostProcessorStep<ProductFieldProcessorModel>
    {
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<ProductFieldEntity> _productFieldRepository;

        public ProductFieldRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();
            _productFieldRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<ProductFieldEntity>();

            _cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
        }

        public override void Initialize(List<ProductFieldProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            var fieldIds = models.Select(y => y.FieldId).ToList();
            _productFieldRepository.BatchDelete(x => !fieldIds.Contains(x.FieldId), 100);
        }

        public override SimplePostProcessorStepProcessStats Process(ProductFieldProcessorModel model)
        {
            var stats = new SimplePostProcessorStepProcessStats();
            var entity = _productFieldRepository.GetTable().FirstOrDefault(x => x.FieldId == model.FieldId);
            if (entity == null)
            {
                entity = new ProductFieldEntity
                {
                    Id = Guid.NewGuid(),
                    CreatedOn = DateTimeOffset.Now
                };
                _productFieldRepository.Insert(entity);
                stats.RecordsAdded++;
            }
            else
            {
                stats.RecordsModified++;
            }

            entity.ModifiedOn = DateTimeOffset.Now;
            entity.CategoryId = model.CategoryId;
            entity.CategoryName = SerializeLocalizedString(model.CategoryName);
            entity.DataType = model.DataType;
            entity.ExcludedFromSpecifications = model.ExcludedFromSpecifications;
            entity.FieldId = model.FieldId;
            entity.FieldCvlId = model.FieldCvlId;
            entity.FieldName = SerializeLocalizedString(model.FieldName);
            entity.UnitOfMeasure = model.UnitOfMeasure;

            return stats;
        }

        public override void Terminate(List<ProductFieldProcessorModel> models)
        {
            if (_cacheManager.Contains(Constants.Cache.ProductField))
            {
                _cacheManager.Remove(Constants.Cache.ProductField);
            }

            base.Terminate(models);
        }

        private static string SerializeLocalizedString(Dictionary<string, string> localizedString)
        {
            return localizedString == null ? null : JsonConvert.SerializeObject(localizedString);
        }
    }
}
