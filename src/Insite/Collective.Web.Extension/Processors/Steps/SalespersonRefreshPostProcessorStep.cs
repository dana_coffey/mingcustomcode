﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Processors.Steps
{
    public class SalespersonRefreshPostProcessorStep : EntityPostProcessorStep<Salesperson, SalespersonProcessorModel>
    {
        private readonly IRepository<Salesperson> _salespersonRepository;

        public override DeleteAction DeleteAction => DeleteAction.None;

        public SalespersonRefreshPostProcessorStep(IUnitOfWork unitOfWork)
        {
            _salespersonRepository = unitOfWork.GetRepository<Salesperson>();
        }

        public override List<Salesperson> GetEntitiesToDeleteOrArchive(List<SalespersonProcessorModel> models)
        {
            var modelsSalespersonNumbers = models.Select(x => x.Number).ToList();
            var salespersons = _salespersonRepository.GetTable().ToList();

            return salespersons.Where(x => !modelsSalespersonNumbers.Contains(x.SalespersonNumber)).ToList();
        }

        public override Salesperson GetEntity(SalespersonProcessorModel model)
        {
            return _salespersonRepository.GetTable().FirstOrDefault(x => x.SalespersonNumber == model.Number);
        }

        public override bool InsertEntity(SalespersonProcessorModel model, Salesperson entity)
        {
            if (string.IsNullOrEmpty(model.Number) || string.IsNullOrEmpty(model.Name))
            {
                JobLogger.Info("Can't insert new salesperson with empty Number or Name");
                return false;
            }

            entity.Name = model.Name;
            entity.SalespersonNumber = model.Number;
            return true;
        }

        public override void UpdateEntity(SalespersonProcessorModel model, Salesperson entity)
        {
            if (!string.IsNullOrEmpty(model.Name))
            {
                entity.Name = model.Name;
                entity.Phone1 = model.PhoneNumber;

                if (string.IsNullOrEmpty(model.Email) || DataValidatorHelper.ValidateEmailsList(model.Email))
                {
                    entity.Email = model.Email;
                }
                else
                {
                    JobLogger.Info($"Invalid emails list '{model.Email}' for salesperson number {model.Number} - {model.Name}");
                }
            }
            else
            {
                JobLogger.Info($"Empty name detected for salesperson number {model.Number}");
            }
        }
    }
}
