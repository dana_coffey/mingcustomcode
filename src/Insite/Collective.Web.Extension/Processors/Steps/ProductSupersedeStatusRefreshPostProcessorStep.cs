﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.InforSxe.Core.Interfaces;
using Collective.InforSxe.Core.Models;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors.Steps
{
    public class ProductSupersedeStatusRefreshPostProcessorStep : SimplePostProcessorStep<ProductSupersedeStatusProcessorModel>
    {
        private readonly ICollectiveApiService _collectiveApiService;
        private readonly IRepository<Product> _productRepository;
        private List<InventoryApiModel> _inventoryApiModels;

        public ProductSupersedeStatusRefreshPostProcessorStep()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();
            _productRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<Product>();

            _collectiveApiService = CollectiveInjector.GetInstance<ICollectiveApiService>();
        }

        public override void Initialize(List<ProductSupersedeStatusProcessorModel> models, IJobLogger jobLogger)
        {
            base.Initialize(models, jobLogger);

            _inventoryApiModels = _collectiveApiService.GetInventory(models.Where(x => !x.IsCatalogProduct).Select(x => x.ErpNumber).ToList(), WarehouseHelper.GetActiveWarehouseList()).ToList();
        }

        public override SimplePostProcessorStepProcessStats Process(ProductSupersedeStatusProcessorModel model)
        {
            var stats = new SimplePostProcessorStepProcessStats();
            var product = _productRepository.GetTable().FirstOrDefault(x => x.ErpNumber == model.ErpNumber);

            if (product != null)
            {
                if (model.IsCatalogProduct)
                {
                    product.SetProperty(Constants.CustomProperties.Product.StatusField, Constants.CustomProperties.Product.Status.Superseded);
                    stats.RecordsModified++;
                }
                else
                {
                    var productInventoryModel = _inventoryApiModels.FirstOrDefault(x => x.ProductId == model.ErpNumber);
                    if (productInventoryModel != null)
                    {
                        if (productInventoryModel.TotalQuantity == 0)
                        {
                            product.SetProperty(Constants.CustomProperties.Product.StatusField, Constants.CustomProperties.Product.Status.Superseded);
                            stats.RecordsModified++;
                        }
                        else if (!product.GetProperty(Constants.CustomProperties.Product.StatusField, string.Empty).Equals(Constants.CustomProperties.Product.Status.SupersededWithInventory))
                        {
                            product.SetProperty(Constants.CustomProperties.Product.StatusField, Constants.CustomProperties.Product.Status.SupersededWithInventory);
                            stats.RecordsModified++;
                        }
                    }
                }
            }

            return stats;
        }
    }
}
