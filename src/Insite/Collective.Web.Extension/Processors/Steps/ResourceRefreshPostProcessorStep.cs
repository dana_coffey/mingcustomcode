﻿using System;
using System.Linq;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Processors.Steps
{
    public class ResourceRefreshPostProcessorStep : SimplePostProcessorStep<ResourceProcessorModel>
    {
        private readonly IRepository<ResourceChecksum> _resourceChecksumRepository;

        public ResourceRefreshPostProcessorStep(IUnitOfWork unitOfWork)
        {
            _resourceChecksumRepository = unitOfWork.GetRepository<ResourceChecksum>();
        }

        public override SimplePostProcessorStepProcessStats Process(ResourceProcessorModel model)
        {
            var stats = new SimplePostProcessorStepProcessStats();

            var entity = _resourceChecksumRepository.GetTable().FirstOrDefault(p => p.Name == model.Name);
            if (entity != null)
            {
                entity.Sha1Sum = model.Sha1Sum;
                entity.ModifiedOn = DateTimeOffset.Now;
                stats.RecordsModified++;
            }
            else
            {
                entity = new ResourceChecksum
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                    Sha1Sum = model.Sha1Sum
                };

                _resourceChecksumRepository.Insert(entity);
                stats.RecordsAdded++;
            }

            return stats;
        }
    }
}
