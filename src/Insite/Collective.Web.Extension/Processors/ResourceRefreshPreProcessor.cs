﻿using System.Linq;
using Collective.Core.Models;
using Collective.Web.Extension.Core.Entities;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Resource Refresh")]
    public class ResourceRefreshPreProcessor : IJobPreprocessor
    {
        private readonly IRepository<ResourceChecksum> _resourceChecksumRepository;
        public IntegrationJob IntegrationJob { get; set; }
        public IJobLogger JobLogger { get; set; }

        public ResourceRefreshPreProcessor()
        {
            var unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _resourceChecksumRepository = unitOfWork.GetRepository<ResourceChecksum>();
        }

        public IntegrationJob Execute()
        {
            IntegrationJob.InitialData = JsonConvert.SerializeObject(_resourceChecksumRepository.GetTableAsNoTracking().Select(p => new ResourceProcessorModel
            {
                Name = p.Name,
                Sha1Sum = p.Sha1Sum
            }).ToList());
            return IntegrationJob;
        }
    }
}
