﻿using System.Data;
using System.Linq;
using System.Threading;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Integration.Core.Helpers;
using Collective.Web.Extension.Core.Services;
using Collective.Web.Extension.Core.Services.Contracts;
using Collective.Web.Extension.Processors.Steps;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Product Supersede Status Refresh")]
    public class ProductSupersedeStatusRefreshPostProcessor : IJobPostprocessor
    {
        private readonly IRepository<CustomProperty> _customPropertyRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public IntegrationJob IntegrationJob { get; set; }

        public IJobLogger JobLogger { get; set; }

        public ProductSupersedeStatusRefreshPostProcessor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
            _customPropertyRepository = _unitOfWork.GetRepository<CustomProperty>();
            _productRepository = _unitOfWork.GetRepository<Product>();
        }

        public void Cancel()
        {
        }

        public void Execute(DataSet dataSet, CancellationToken cancellationToken)
        {
            var simplePostProcessorService = new SimplePostProcessorService(_unitOfWork, JobLogger, IntegrationJob);
            var productStep = new ProductSupersedeStatusRefreshPostProcessorStep();

            var supersededWithInventoryProductIds = _customPropertyRepository.GetTableAsNoTracking().Where(x => x.ParentTable == nameof(Product) && x.Name == Constants.CustomProperties.Product.StatusField && x.Value == Constants.CustomProperties.Product.Status.SupersededWithInventory).Select(x => x.ParentId).ToList();

            var productSuperseded = _productRepository.GetTableAsNoTracking().Where(x => supersededWithInventoryProductIds.Contains(x.Id)).ToList();
            var productSupersedeStatusProcessorModels = productSuperseded.Select(x => new ProductSupersedeStatusProcessorModel
            {
                ErpNumber = x.ErpNumber,
                IsCatalogProduct = GetIsCatalogProduct(x)
            });

            if (IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault() != null)
            {
                simplePostProcessorService.ProcessStep(
                    productStep,
                    IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(),
                    IntegrationProcessorHelper.ToJsonDataSet(productSupersedeStatusProcessorModels, IntegrationJob.JobDefinition.JobDefinitionSteps.First().Sequence, IntegrationJob.JobDefinition.JobDefinitionSteps.First().ObjectName),
                    cancellationToken);
            }
        }

        private bool GetIsCatalogProduct(Product product)
        {
            var isCatalogProduct = product.CustomProperties.FirstOrDefault(x => x.Name.Equals(Constants.CustomProperties.Product.IsCatalogProduct))?.Value;
            return ParseHelper.ParseBoolean(isCatalogProduct, false);
        }
    }
}
