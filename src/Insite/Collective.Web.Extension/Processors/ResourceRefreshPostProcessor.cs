﻿using System.Data;
using System.Linq;
using System.Threading;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Services;
using Collective.Web.Extension.Processors.Steps;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Resource Refresh")]
    public class ResourceRefreshPostProcessor : IJobPostprocessor
    {
        private readonly IUnitOfWork _unitOfWork;
        public IntegrationJob IntegrationJob { get; set; }

        public IJobLogger JobLogger { get; set; }

        public ResourceRefreshPostProcessor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
        }

        public void Cancel()
        {
        }

        public void Execute(DataSet dataSet, CancellationToken cancellationToken)
        {
            var entityPostProcessorService = new SimplePostProcessorService(_unitOfWork, JobLogger, IntegrationJob);
            var step = new ResourceRefreshPostProcessorStep(_unitOfWork);

            entityPostProcessorService.ProcessStep(step, IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(), dataSet, cancellationToken);
        }
    }
}
