﻿using System.Data;
using System.Linq;
using System.Threading;
using Collective.Web.Extension.Core.Services;
using Collective.Web.Extension.Processors.Steps;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Product Refresh")]
    public class ProductRefreshPostProcessor : IJobPostprocessor
    {
        private readonly IUnitOfWork _unitOfWork;
        public IntegrationJob IntegrationJob { get; set; }
        public IJobLogger JobLogger { get; set; }

        public ProductRefreshPostProcessor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
        }

        public void Cancel()
        {
        }

        public void Execute(DataSet dataSet, CancellationToken cancellationToken)
        {
            var entityPostProcessorService = new EntityPostProcessorService(_unitOfWork, JobLogger, IntegrationJob);

            var productStep = new ProductRefreshPostProcessorStep();
            entityPostProcessorService.ProcessStep(productStep, IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(), dataSet, cancellationToken);
        }
    }
}
