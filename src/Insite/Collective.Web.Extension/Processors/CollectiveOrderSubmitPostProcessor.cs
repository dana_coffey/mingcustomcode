﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    public class CollectiveOrderSubmitPostProcessor
    {
        protected IJobLogger JobLogger { get; }
        protected IRepository<OrderHistory> OrderHistoryRepository { get; }
        protected IUnitOfWork UnitOfWork { get; }

        public CollectiveOrderSubmitPostProcessor(IUnitOfWork unitOfWork, IJobLogger jobLogger)
        {
            UnitOfWork = unitOfWork;
            JobLogger = jobLogger;
            OrderHistoryRepository = UnitOfWork.GetRepository<OrderHistory>();
        }

        public virtual void ProcessOrderSubmit(DataSet dataSet)
        {
            var orderSubmitRow = dataSet?.Tables[Constants.Jobs.OrderSubmit.DataSet.Tables.OrderSubmit]?.Rows[0];
            var erpOrderNumber = orderSubmitRow?[Constants.Jobs.OrderSubmit.DataSet.Columns.ErpOrderNumber]?.ToString();
            var apiErrorCode = (int)(orderSubmitRow?[Constants.Jobs.OrderSubmit.DataSet.Columns.ApiErrorCode] ?? 0);

            if (apiErrorCode != 0)
            {
                var apiErrorMessage = orderSubmitRow?[Constants.Jobs.OrderSubmit.DataSet.Columns.ApiErrorMessage]?.ToString();
                JobLogger.Error($"Order Submit API call failed: {apiErrorCode} - {apiErrorMessage}");
            }
            else
            {
                ScheduleOrderHistoryRefresh(erpOrderNumber);
            }
        }

        private void ScheduleOrderHistoryRefresh(string erpOrderNumber)
        {
            var jobDefinition = UnitOfWork.GetRepository<JobDefinition>().GetTable().FirstOrDefault(p => p.Id == Constants.Jobs.OrderHistoryRefresh.JobDefinitionId);

            if (jobDefinition != null && !string.IsNullOrEmpty(erpOrderNumber))
            {
                var integrationJob = new IntegrationJob
                {
                    JobDefinitionId = jobDefinition.Id,
                    ScheduleDateTime = DateTimeOffset.Now.AddMinutes(2),
                    RunStandalone = true,
                    IsRealTime = false,
                    IntegrationJobParameters = new List<IntegrationJobParameter>
                    {
                        new IntegrationJobParameter
                        {
                            JobDefinitionParameterId = Constants.Jobs.OrderHistoryRefresh.JobDefinitionParameter.ErpOrderNumber,
                            Value = erpOrderNumber.Replace("-00", string.Empty)
                        }
                    }
                };

                UnitOfWork.GetRepository<IntegrationJob>().Insert(integrationJob);
                UnitOfWork.Save();
            }
        }
    }
}
