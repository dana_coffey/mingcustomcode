﻿using System.Data;
using System.Linq;
using System.Threading;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Services;
using Collective.Web.Extension.Processors.Steps;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Customer Refresh")]
    public class CustomerRefreshPostProcessor : IJobPostprocessor
    {
        private readonly IUnitOfWork _unitOfWork;
        public IntegrationJob IntegrationJob { get; set; }

        public IJobLogger JobLogger { get; set; }

        public CustomerRefreshPostProcessor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
        }

        public void Cancel()
        {
        }

        public void Execute(DataSet dataSet, CancellationToken cancellationToken)
        {
            var postProcessorService = new EntityPostProcessorService(_unitOfWork, JobLogger, IntegrationJob);

            var billToStep = new CustomerRefreshBillToPostProcessorStep();
            postProcessorService.ProcessStep(billToStep, IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(x => x.Sequence == Constants.Jobs.CustomerRefresh.Steps.CustomerBillToStepSequence), dataSet, cancellationToken);

            var shipToStep = new CustomerRefreshShipToPostProcessorStep();
            postProcessorService.ProcessStep(shipToStep, IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(x => x.Sequence == Constants.Jobs.CustomerRefresh.Steps.CustomerShipToStepSequence), dataSet, cancellationToken);
        }
    }
}
