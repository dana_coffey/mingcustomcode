﻿using System;
using System.Data;
using System.Linq;
using Collective.Core.Constant;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.PlugIns.Postprocessor;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Order Submit")]
    public class OrderSubmitPostProcessor : JobPostprocessorProcessSubmitResponse
    {
        public OrderSubmitPostProcessor(IUnitOfWorkFactory unitOfWorkFactory) : base(unitOfWorkFactory)
        {
        }

        protected override void ProcessOrderSubmit(DataSet dataSet)
        {
            base.ProcessOrderSubmit(dataSet);

            var orderSubmitRow = dataSet?.Tables[Constants.Jobs.OrderSubmit.DataSet.Tables.OrderSubmit]?.Rows[0];
            if (orderSubmitRow != null)
            {
                if ((int)(orderSubmitRow?[Constants.Jobs.OrderSubmit.DataSet.Columns.ApiErrorCode] ?? 0) != 0)
                {
                    var orderNumber = orderSubmitRow["OrderNumber"]?.ToString();
                    var jobReferenceNumber = orderSubmitRow[Constants.Jobs.OrderSubmit.DataSet.Columns.JobReferenceNumber]?.ToString();
                    var customerOrder = UnitOfWork.GetRepository<CustomerOrder>().GetTable().FirstOrDefault(x => x.OrderNumber.Equals(orderNumber, StringComparison.OrdinalIgnoreCase));

                    customerOrder?.SetProperty(Constants.CustomProperties.Cart.OrderSubmitErrorReferenceNo, jobReferenceNumber);
                }
            }

            UnitOfWork.DataProvider.DetectChanges();
            UnitOfWork.Save();

            new CollectiveOrderSubmitPostProcessor(UnitOfWork, JobLogger).ProcessOrderSubmit(dataSet);
        }
    }
}
