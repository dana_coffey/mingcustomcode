﻿using System.Data;
using System.Linq;
using System.Threading;
using Collective.Web.Extension.Core.Services;
using Collective.Web.Extension.Processors.Steps;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Processors
{
    [DependencyName("Collective - Category Refresh")]
    public class CategoryRefreshPostProcessor : IJobPostprocessor
    {
        private readonly IUnitOfWork _unitOfWork;
        public IntegrationJob IntegrationJob { get; set; }

        public IJobLogger JobLogger { get; set; }

        public CategoryRefreshPostProcessor(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
        }

        public void Cancel()
        {
        }

        public void Execute(DataSet dataSet, CancellationToken cancellationToken)
        {
            var postProcessorService = new SimplePostProcessorService(_unitOfWork, JobLogger, IntegrationJob);
            var step = new CategoryRefreshPostProcessorStep();

            postProcessorService.ProcessStep(step, IntegrationJob.JobDefinition.JobDefinitionSteps.FirstOrDefault(), dataSet, cancellationToken);
        }
    }
}
