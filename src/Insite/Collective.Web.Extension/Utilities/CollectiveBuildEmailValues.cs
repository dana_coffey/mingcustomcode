﻿using System;
using System.Collections.Generic;
using System.Linq;
using Insite.Common;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Localization;
using Insite.Core.SystemSetting.Groups.OrderManagement;
using Insite.Core.SystemSetting.Groups.SiteConfigurations;
using Insite.Data.Entities;
using Insite.Plugins.Emails;

namespace Collective.Web.Extension.Utilities
{
    public class CollectiveBuildEmailValues : BuildEmailValues
    {
        private readonly char[] _emailAddressesSepartors = { ',', ';' };

        public CollectiveBuildEmailValues(IUnitOfWorkFactory unitOfWorkFactory, ITranslationLocalizer translationLocalizer, CheckoutSettings checkoutSettings, EmailsSettings emailsSettings) : base(unitOfWorkFactory, translationLocalizer, checkoutSettings, emailsSettings)
        {
        }

        public override List<string> BuildOrderConfirmationEmailToList(Guid customerOrderId)
        {
            var emailsList = new List<string>();
            var customerOrder = UnitOfWork.GetRepository<CustomerOrder>().Get(customerOrderId);

            CheckoutSettings.OverrideCurrentWebsite(customerOrder.WebsiteId);
            var orderConfirmationTo = CheckoutSettings.SendOrderConfirmationTo;

            if (customerOrder.BTEmail.Length > 0 && (orderConfirmationTo == OrderConfirmationEmails.BillToEmail || orderConfirmationTo == OrderConfirmationEmails.UserAndBillToEmails || (orderConfirmationTo == OrderConfirmationEmails.UserBillToAndShipToEmails || orderConfirmationTo == OrderConfirmationEmails.BillToAndShipToEmails)))
            {
                foreach (var email in customerOrder.BTEmail.Split(_emailAddressesSepartors))
                {
                    AddEmail(emailsList, email.Trim());
                }
            }

            if (customerOrder.STEmail.Length > 0 && (orderConfirmationTo == OrderConfirmationEmails.ShipToEmail || orderConfirmationTo == OrderConfirmationEmails.UserAndShipToEmails || (orderConfirmationTo == OrderConfirmationEmails.UserBillToAndShipToEmails || orderConfirmationTo == OrderConfirmationEmails.BillToAndShipToEmails)))
            {
                foreach (var email in customerOrder.STEmail.Split(_emailAddressesSepartors))
                {
                    AddEmail(emailsList, email.Trim());
                }
            }

            if ((customerOrder.PlacedByUserProfile?.Email.Length ?? 0) > 0 && (orderConfirmationTo == OrderConfirmationEmails.UserEmail || orderConfirmationTo == OrderConfirmationEmails.UserAndBillToEmails || orderConfirmationTo == OrderConfirmationEmails.UserAndShipToEmails || orderConfirmationTo == OrderConfirmationEmails.UserBillToAndShipToEmails))
            {
                AddEmail(emailsList, customerOrder.PlacedByUserProfile?.Email.Trim());
            }

            return emailsList;
        }

        private void AddEmail(IList<string> emailsList, string email)
        {
            if (!email.IsBlank() && RegularExpressionLibrary.IsValidEmail(email) && !emailsList.Contains(email, StringComparer.OrdinalIgnoreCase))
            {
                emailsList.Add(email);
            }
        }
    }
}
