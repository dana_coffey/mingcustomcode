﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Settings;
using Insite.Common.Helpers;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.Plugins.Pipelines.Pricing;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.Integration;
using Insite.Core.SystemSetting.Groups.OrderManagement;
using Insite.Core.SystemSetting.Groups.Shipping;
using Insite.Core.SystemSetting.Groups.SiteConfigurations;
using Insite.Data.Entities;
using Insite.Plugins.EntityUtilities;

namespace Collective.Web.Extension.Utilities
{
    public class CollectiveCustomerOrderUtilities : CustomerOrderUtilities
    {
        private const string DefaultWarehouseNameColumn = "DefaultWarehouseName";
        private const string DiscountAmountColumn = "DiscountAmount";

        private readonly CollectiveSouthCarolinaSolidWasteTaxSettings _collectiveSouthCarolinaSolidWasteTaxSettings;
        private readonly ICollectiveInventoryService _collectiveInventoryService;

        public CollectiveCustomerOrderUtilities(IUnitOfWorkFactory unitOfWorkFactory, Lazy<IPromotionAmountProvider> promotionAmountProvider, Lazy<IOrderLineUtilities> orderLineUtilities, Lazy<IRoundingRulesProvider> roundingRulesProvider, Lazy<IProductUtilities> productUtilities, BudgetsAndOrderApprovalSettings budgetsAndOrderApprovalSettings, CartSettings cartSettings, CheckoutSettings checkoutSettings, ShippingGeneralSettings shippingGeneralSettings, TaxesSettings taxesSettings, GiftCardsSettings giftCardsSettings, OrderSubmitSettings orderSubmitSettings, IPricingPipeline pricingPipeline, CollectiveSouthCarolinaSolidWasteTaxSettings collectiveSouthCarolinaSolidWasteTaxSettings, ICollectiveInventoryService collectiveInventoryService)
            : base(unitOfWorkFactory, promotionAmountProvider, orderLineUtilities, roundingRulesProvider, productUtilities, budgetsAndOrderApprovalSettings, cartSettings, checkoutSettings, shippingGeneralSettings, taxesSettings, giftCardsSettings, orderSubmitSettings, pricingPipeline)
        {
            _collectiveSouthCarolinaSolidWasteTaxSettings = collectiveSouthCarolinaSolidWasteTaxSettings;
            _collectiveInventoryService = collectiveInventoryService;
        }

        public override bool CustomerOrderIsTaxable(CustomerOrder customerOrder)
        {
            var collectiveTaxingHelper = CollectiveInjector.GetInstance<ICollectiveTaxingHelper>();
            return collectiveTaxingHelper.IsCustomerOrderTaxable(customerOrder, this.TaxesSettings.StorePickupShipCode, base.CustomerOrderIsTaxable(customerOrder));
        }

        public override State GetStateTaxRate(CustomerOrder customerOrder)
        {
            if (customerOrder.ShipVia != null && customerOrder.ShipVia.ShipCode.EqualsIgnoreCase(this.TaxesSettings.StorePickupShipCode))
            {
                var cartWarehouseName = customerOrder.GetProperty(Constants.CustomProperties.Cart.Warehouse, string.Empty);
                if (!string.IsNullOrEmpty(cartWarehouseName))
                {
                    var warehouse = this.UnitOfWork.GetRepository<Warehouse>().GetTableAsNoTracking().FirstOrDefault(x => x.Name == cartWarehouseName);
                    if (warehouse != null)
                    {
                        var byNaturalKey = this.UnitOfWork.GetRepository<State>().GetTableAsNoTracking().FirstOrDefault(x => x.Abbreviation == warehouse.State && x.CountryId == warehouse.CountryId);
                        if (byNaturalKey != null)
                        {
                            return byNaturalKey;
                        }
                    }
                }
            }

            return customerOrder.ShipTo.State;
        }

        protected override void AddCustomerOrderToDataset(CustomerOrder customerOrder, DataSet ds)
        {
            var arrayList = new ArrayList { customerOrder };
            var type = customerOrder.GetType();
            var dataTableFromList1 = ObjectHelper.GetDataTableFromList(arrayList, type);

            dataTableFromList1.Columns.Add(DefaultWarehouseNameColumn, typeof(string));
            dataTableFromList1.Rows[0][DefaultWarehouseNameColumn] = GetWarehouse(customerOrder);
            dataTableFromList1.Columns.Add(DiscountAmountColumn, typeof(decimal));
            dataTableFromList1.Rows[0][DiscountAmountColumn] = GetPromotionOrderDiscountTotal(customerOrder);
            ds.Tables.Add(dataTableFromList1);

            var dataTableFromList2 = ObjectHelper.GetDataTableFromList(customerOrder.CustomProperties.ToList(), typeof(CustomProperty), "ParentId", customerOrder.Id);
            dataTableFromList2.TableName = Constants.CustomProperties.Cart.CustomerOrderPropertyTableName;
            ds.Tables.Add(dataTableFromList2);

            AddSouthCarolinaSolidWasteTaxes(customerOrder, ds);
        }

        protected override void AddShipViaToDataset(CustomerOrder customerOrder, DataSet ds)
        {
            if (customerOrder.ShipVia != null)
            {
                var tables = ds.Tables;
                var arrayList = new ArrayList { customerOrder.ShipVia };
                var type = customerOrder.ShipVia.GetType();
                var dataTableFromList = ObjectHelper.GetDataTableFromList(arrayList, type);

                dataTableFromList.Columns.Add(Constants.CustomProperties.ShipVia.ErpShipViaCode, typeof(string));
                dataTableFromList.Rows[0][Constants.CustomProperties.ShipVia.ErpShipViaCode] = customerOrder.ShipVia.GetProperty(Constants.CustomProperties.ShipVia.ErpShipViaCode, string.Empty);

                tables.Add(dataTableFromList);
            }
        }

        private void AddSouthCarolinaSolidWasteTaxes(CustomerOrder customerOrder, DataSet ds)
        {
            if (GetStateTaxRate(customerOrder)?.Id == Constants.Ids.States.SouthCarolina)
            {
                var solidWasteTaxQty = 0m;
                foreach (var orderLine in customerOrder.OrderLines)
                {
                    var isSolidWasteTaxable = ParseHelper.ParseBoolean(orderLine.Product?.GetProperty(Constants.CustomProperties.Product.IsSouthCarolinaSolidWasteTaxable, false.ToString()), false);
                    if (isSolidWasteTaxable)
                    {
                        solidWasteTaxQty += orderLine.QtyOrdered;
                    }
                }

                if (solidWasteTaxQty > 0)
                {
                    var southCarolinaSolidWasteTaxTable = new DataTable { TableName = Constants.Jobs.OrderSubmit.DataSet.Tables.SouthCarolinaSolidWasteTax };
                    southCarolinaSolidWasteTaxTable.Columns.Add(nameof(Product.ErpNumber), typeof(string));
                    southCarolinaSolidWasteTaxTable.Columns.Add(nameof(OrderLine.QtyOrdered), typeof(decimal));
                    southCarolinaSolidWasteTaxTable.Columns.Add(nameof(OrderLine.UnitNetPrice), typeof(decimal));

                    var row = southCarolinaSolidWasteTaxTable.NewRow();
                    row[nameof(Product.ErpNumber)] = _collectiveSouthCarolinaSolidWasteTaxSettings.ProductNumber;
                    row[nameof(OrderLine.QtyOrdered)] = solidWasteTaxQty;
                    row[nameof(OrderLine.UnitNetPrice)] = _collectiveSouthCarolinaSolidWasteTaxSettings.Amount;

                    southCarolinaSolidWasteTaxTable.Rows.Add(row);
                    ds.Tables.Add(southCarolinaSolidWasteTaxTable);
                }
            }
        }

        private string GetWarehouse(CustomerOrder customerOrder)
        {
            if (!string.IsNullOrEmpty(customerOrder.GetProperty(Constants.CustomProperties.Cart.Warehouse, string.Empty)))
            {
                return customerOrder.GetProperty(Constants.CustomProperties.Cart.Warehouse, string.Empty);
            }

            return (customerOrder.DefaultWarehouse ?? _collectiveInventoryService.GetDefaultWarehouse(customerOrder.Customer))?.Name ?? string.Empty;
        }
    }
}
