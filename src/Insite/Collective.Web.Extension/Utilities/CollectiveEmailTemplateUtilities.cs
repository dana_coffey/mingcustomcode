﻿using System;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Collective.Core.Constant;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Data.Entities;
using Insite.Data.Extensions;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Mvc;

namespace Collective.Web.Extension.Utilities
{
    public class CollectiveEmailTemplateUtilities : EntityUtilities<EmailTemplate>, IEmailTemplateUtilities
    {
        private readonly IContentManagerUtilities _contentManagerUtilities;
        private readonly ISystemResourceFileProvider _systemResourceFileProvider;

        public CollectiveEmailTemplateUtilities(IUnitOfWorkFactory unitOfWorkFactory, IContentManagerUtilities contentManagerUtilities, ISystemResourceFileProvider systemResourceFileProvider) : base(unitOfWorkFactory)
        {
            _contentManagerUtilities = contentManagerUtilities;
            _systemResourceFileProvider = systemResourceFileProvider;
        }

        public EmailTemplate GetOrCreateByName(string name, Guid? templateWebsiteId = null, bool websiteSpecific = false)
        {
            var languageRepository = UnitOfWork.GetTypedRepository<ILanguageRepository>();
            var language = languageRepository.GetDefault();

            var emailTemplate = GetEmailTemplateByWebsiteId(name, templateWebsiteId, websiteSpecific);
            if (emailTemplate == null)
            {
                emailTemplate = new EmailTemplate
                {
                    Name = name,
                    ContentManager = new ContentManager
                    {
                        Name = "EmailTemplate"
                    },
                    WebsiteId = websiteSpecific ? templateWebsiteId : null
                };

                UnitOfWork.GetRepository<ContentManager>().Insert(emailTemplate.ContentManager);
                UnitOfWork.GetRepository<EmailTemplate>().Insert(emailTemplate);
            }

            if (emailTemplate.ContentManager.Contents.All(x => x.Language != language))
            {
                var content = new Content
                {
                    ApprovedOn = DateTimeOffset.Now,
                    DeviceType = Constants.Content.DeviceType.Desktop,
                    Html = GetDefaultEmailTemplateContent(name, language),
                    Language = language,
                    Persona = UnitOfWork.GetTypedRepository<IPersonaRepository>().GetDefault(),
                    PublishToProductionOn = DateTimeOffset.Now,
                    Revision = 1,
                    SubmittedForApprovalOn = DateTimeOffset.Now,
                };

                _contentManagerUtilities.AddContent(emailTemplate.ContentManager, content);
                UnitOfWork.Save();
            }

            return emailTemplate;
        }

        private string GetDefaultEmailTemplateContent(string templateName, Language language)
        {
            var path = HostingEnvironment.MapPath($"~/Views/DefaultEmails/{templateName}.{language.CultureCode}.cshtml");
            if (string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                path = HostingEnvironment.MapPath($"~/Views/DefaultEmails/{templateName}.cshtml");
            }

            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                return File.ReadAllText(path);
            }

            var systemResourcesVirtualPath = $"~/SystemResources/Views/DefaultEmails/{templateName}.cshtml";
            if (!_systemResourceFileProvider.FileExists(systemResourcesVirtualPath))
            {
                throw new ArgumentException($"The Email Template '{templateName}' had no approved revisions and there was no default template found at {systemResourcesVirtualPath}");
            }

            return _systemResourceFileProvider.GetContent(systemResourcesVirtualPath);
        }

        private EmailTemplate GetEmailTemplateByWebsiteId(string templateName, Guid? templateWebsiteId = null, bool websiteSpecific = false)
        {
            EmailTemplate result = null;

            var repository = UnitOfWork.GetRepository<EmailTemplate>();

            if (templateWebsiteId.HasValue)
            {
                result = repository.GetTable().Expand(x => x.ContentManager).FirstOrDefault(x => x.Name.Equals(templateName) && x.WebsiteId.HasValue && x.WebsiteId.Value == templateWebsiteId.Value);
            }

            if (websiteSpecific || result != null)
            {
                return result;
            }

            return repository.GetTable().Expand(x => x.ContentManager).FirstOrDefault(x => x.Name.Equals(templateName) && !x.WebsiteId.HasValue);
        }
    }
}
