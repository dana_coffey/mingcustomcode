﻿using System;
using System.Linq;
using Collective.Core.Enums;
using Collective.Core.Extensions;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Settings;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Utilities
{
    public class MaxRecallUrlBuilder
    {
        private readonly CollectiveInvoiceSettings _invoiceSettings;
        public string ErpOrderNumber { get; }

        public string FileName => $"{ErpOrderNumber}{FileSuffix}.pdf";

        public string FileSuffix
        {
            get
            {
                switch (FileType)
                {
                    case MaxRecallFileType.Invoice:
                        return "_inv";
                    case MaxRecallFileType.ProofOfDelivery:
                        return "_pod";
                    default:
                        return string.Empty;
                }
            }
        }

        public MaxRecallFileType FileType { get; }
        public bool OrderExists { get; }
        public Guid OrderId { get; }
        public string OrderNumber { get; set; }
        public string OrderNumberSuffix { get; set; }

        public string Url
        {
            get
            {
                var fileUrl = _invoiceSettings.MaxRecallUrl
                    .Replace("{orderNumber}", OrderNumber)
                    .Replace("{orderSuffix}", OrderNumberSuffix);

                switch (FileType)
                {
                    case MaxRecallFileType.Invoice:
                        return fileUrl.Replace("{action}", _invoiceSettings.InvoiceRequestActionName);

                    case MaxRecallFileType.ProofOfDelivery:
                        return fileUrl.Replace("{action}", _invoiceSettings.ProofOfDeliveryActionName);
                }

                throw new ArgumentOutOfRangeException(nameof(FileType));
            }
        }

        public MaxRecallUrlBuilder(Guid orderId, MaxRecallFileType fileType, CollectiveInvoiceSettings invoiceSettings, IUnitOfWork unitOfWork)
        {
            _invoiceSettings = invoiceSettings;
            FileType = fileType;

            var orderHistory = unitOfWork.GetRepository<OrderHistory>().GetTable().FirstOrDefault(x => x.Id == orderId);
            OrderId = orderId;

            if (orderHistory != null)
            {
                ErpOrderNumber = orderHistory.ErpOrderNumber;
                OrderExists = true;
            }
            else
            {
                OrderExists = false;
            }

            OrderNumber = ErpOrderNumber.GetOrderNumberPart(OrderNumberParts.OrderNumber);
            OrderNumberSuffix = ErpOrderNumber.GetOrderNumberPart(OrderNumberParts.Suffix);
        }
    }
}
