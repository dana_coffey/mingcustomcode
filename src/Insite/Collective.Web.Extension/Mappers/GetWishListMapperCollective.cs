﻿using System.Linq;
using System.Net.Http;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Inventory;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi.Interfaces;
using Insite.WishLists.Services.Results;
using Insite.WishLists.WebApi.V1.ApiModels;
using Insite.WishLists.WebApi.V1.Mappers;
using Insite.WishLists.WebApi.V1.Mappers.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Mappers
{
    public class GetWishListMapperCollective : GetWishListMapper
    {
        private readonly ICollectiveInventoryService _collectiveInventoryService;

        public GetWishListMapperCollective(IGetWishListLineCollectionMapper getWishListLineCollectionMapper, IUrlHelper urlHelper, IObjectToObjectMapper objectToObjectMapper, ICollectiveInventoryService collectiveInventoryService)
            : base(getWishListLineCollectionMapper, urlHelper, objectToObjectMapper)
        {
            _collectiveInventoryService = collectiveInventoryService;
        }

        public override WishListModel MapResult(WishListResult serviceResult, HttpRequestMessage request)
        {
            var wishListModel = base.MapResult(serviceResult, request);

            foreach (var wishListLine in wishListModel.WishListLineCollection)
            {
                var productDto = serviceResult.WishListLineResults.FirstOrDefault(x => x.ProductDto.ERPNumber == wishListLine.ErpNumber)?.ProductDto;
                var isCatalogProduct = (productDto?.Properties.ContainsKey(Constants.CustomProperties.Product.IsCatalogProduct) ?? false) && ParseHelper.ParseBoolean(productDto.Properties[Constants.CustomProperties.Product.IsCatalogProduct], false);

                var availabilityStatus = _collectiveInventoryService.GetProductInventoryStatusModel(wishListLine.ErpNumber, isCatalogProduct, string.Empty);

                if (availabilityStatus.Status == AvailabilityStatusType.Restricted)
                {
                    wishListLine.CanAddToCart = false;
                    wishListLine.CanShowPrice = false;
                    wishListLine.CanEnterQuantity = false;
                    wishListLine.CanShowUnitOfMeasure = false;
                }

                wishListLine.Properties.Add(Constants.HandlerResultProperties.Cart.AvailabilityStatus, JsonConvert.SerializeObject(availabilityStatus));
            }

            return wishListModel;
        }
    }
}
