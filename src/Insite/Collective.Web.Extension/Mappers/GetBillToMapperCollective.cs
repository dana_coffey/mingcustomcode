﻿using System.Net.Http;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi.Interfaces;
using Insite.Customers.Services.Results;
using Insite.Customers.WebApi.V1.ApiModels;
using Insite.Customers.WebApi.V1.Mappers;
using Insite.Customers.WebApi.V1.Mappers.Interfaces;
using Insite.Websites.WebApi.V1.Mappers.Interfaces;

namespace Collective.Web.Extension.Mappers
{
    public class GetBillToMapperCollective : GetBillToMapper
    {
        public GetBillToMapperCollective(IGetShipToMapper getShipToMapper, IGetCountryMapper getCountryMapper, IGetStateMapper getStateMapper, IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper) : base(getShipToMapper, getCountryMapper, getStateMapper, objectToObjectMapper, urlHelper)
        {
        }

        public override BillToModel MapResult(GetBillToResult serviceResult, HttpRequestMessage request)
        {
            var model = base.MapResult(serviceResult, request);

            if (model != null)
            {
                model.CustomerName = serviceResult.BillTo.CompanyName;
            }

            return model;
        }
    }
}
