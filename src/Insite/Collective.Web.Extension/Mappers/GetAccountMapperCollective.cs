﻿using System.Linq;
using System.Net.Http;
using Collective.Core.Constant;
using Collective.Web.Extension.Helpers;
using Insite.Account.Services.Results;
using Insite.Account.WebApi.V1.ApiModels;
using Insite.Account.WebApi.V1.Mappers;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Mappers
{
    public class GetAccountMapperCollective : GetAccountMapper
    {
        private readonly ITranslationLocalizer _translationLocalizer;

        public GetAccountMapperCollective(IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper, ITranslationLocalizer translationLocalizer) : base(objectToObjectMapper, urlHelper)
        {
            _translationLocalizer = translationLocalizer;
        }

        public override AccountModel MapResult(GetAccountResult serviceResult, HttpRequestMessage request)
        {
            var accountModel = base.MapResult(serviceResult, request);

            accountModel.Properties.Add(Constants.HandlerResultProperties.Account.IsDeactivated, serviceResult.UserProfile?.IsDeactivated.ToString().ToLower() ?? false.ToString());
            accountModel.Properties.Add(Constants.HandlerResultProperties.Account.AvailableRoles, JsonConvert.SerializeObject(RoleHelper.GetAvailableRoles().Select(p => new
            {
                id = p,
                name = _translationLocalizer.TranslateLabel(p)
            })));

            return accountModel;
        }
    }
}
