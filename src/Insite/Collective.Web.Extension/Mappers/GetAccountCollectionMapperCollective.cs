﻿using System.Linq;
using System.Net.Http;
using Collective.Core.Constant;
using Collective.Web.Extension.Helpers;
using Insite.Account.Services.Results;
using Insite.Account.WebApi.V1.ApiModels;
using Insite.Account.WebApi.V1.Mappers;
using Insite.Account.WebApi.V1.Mappers.Interfaces;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Mappers
{
    public class GetAccountCollectionMapperCollective : GetAccountCollectionMapper
    {
        private readonly ITranslationLocalizer _translationLocalizer;

        public GetAccountCollectionMapperCollective(IObjectToObjectMapper objectToObjectMapper, IGetAccountMapper getAccountMapper, IUrlHelper urlHelper, ITranslationLocalizer translationLocalizer) : base(objectToObjectMapper, getAccountMapper, urlHelper)
        {
            _translationLocalizer = translationLocalizer;
        }

        public override AccountCollectionModel MapResult(GetAccountCollectionResult serviceResult, HttpRequestMessage request)
        {
            var model = base.MapResult(serviceResult, request);

            model.Properties.Add(Constants.HandlerResultProperties.Account.AvailableRoles, JsonConvert.SerializeObject(RoleHelper.GetAvailableRoles().Select(p => new
            {
                id = p,
                name = _translationLocalizer.TranslateLabel(p)
            })));

            return model;
        }
    }
}
