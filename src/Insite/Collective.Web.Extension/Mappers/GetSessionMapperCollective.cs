﻿using System.Linq;
using System.Net.Http;
using Insite.Account.Services.Results;
using Insite.Account.WebApi.V1.ApiModels;
using Insite.Account.WebApi.V1.Mappers;
using Insite.Common.DynamicLinq;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Core.WebApi.Interfaces;
using Insite.Customers.WebApi.V1.Mappers.Interfaces;
using Insite.Data.Repositories.Interfaces;
using Insite.Websites.WebApi.V1.Mappers.Interfaces;

namespace Collective.Web.Extension.Mappers
{
    public class GetSessionMapperCollective : GetSessionMapper
    {
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;
        private readonly ICustomerRepository _customerRepository;

        public GetSessionMapperCollective(IGetBillToMapper getBillToMapper, IGetShipToMapper getShipToMapper, IGetLanguageMapper getLanguageMapper, IGetCurrencyMapper getCurrencyMapper, IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper, IUnitOfWorkFactory unitOfWorkFactory, CustomerDefaultsSettings customerDefaultsSettings) : base(getBillToMapper, getShipToMapper, getLanguageMapper, getCurrencyMapper, objectToObjectMapper, urlHelper)
        {
            _customerDefaultsSettings = customerDefaultsSettings;
            _customerRepository = unitOfWorkFactory.GetUnitOfWork().GetTypedRepository<ICustomerRepository>();
        }

        public override SessionModel MapResult(GetSessionResult serviceResult, HttpRequestMessage request)
        {
            var sessionModel = base.MapResult(serviceResult, request);

            if (sessionModel.RedirectToChangeCustomerPageOnSignIn)
            {
                var userProfileId = SiteContext.Current.RememberedUserProfile?.Id ?? SiteContext.Current.UserProfile.Id;

                var billTosCount = DynamicQueryable.Count(_customerRepository.GetAvailableBillTos(SiteContext.Current.WebsiteDto, userProfileId));
                if (billTosCount == 1)
                {
                    var availableShipTos = _customerRepository.GetAvailableShipTos(SiteContext.Current.BillTo, userProfileId, SiteContext.Current.WebsiteDto.Id);
                    if (availableShipTos.Count(x => !x.CustomerSequence.StartsWith(_customerDefaultsSettings.ErpShipToPrefix)) <= 1)
                    {
                        sessionModel.RedirectToChangeCustomerPageOnSignIn = false;
                    }
                }
            }

            return sessionModel;
        }
    }
}
