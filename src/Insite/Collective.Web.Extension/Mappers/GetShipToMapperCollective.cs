﻿using System.Net.Http;
using System.Text;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Core.WebApi.Interfaces;
using Insite.Customers.Services.Results;
using Insite.Customers.WebApi.V1.ApiModels;
using Insite.Customers.WebApi.V1.Mappers;
using Insite.Websites.WebApi.V1.Mappers.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Collective.Web.Extension.Mappers
{
    public class GetShipToMapperCollective : GetShipToMapper
    {
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;
        private readonly ICollectiveWarehouseService _warehouseService;

        public GetShipToMapperCollective(IGetCountryMapper getCountryMapper, IGetStateMapper getStateMapper, IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper, CustomerDefaultsSettings customerDefaultsSettings, ICollectiveWarehouseService warehouseService) : base(getCountryMapper, getStateMapper, objectToObjectMapper, urlHelper)
        {
            _customerDefaultsSettings = customerDefaultsSettings;
            _warehouseService = warehouseService;
        }

        public override ShipToModel MapResult(GetShipToResult serviceResult, HttpRequestMessage request)
        {
            var model = base.MapResult(serviceResult, request);

            if (model != null)
            {
                model.CustomerName = serviceResult.BillTo.CompanyName;

                // CustomerSequence "" is "Use billing address"
                // CustomerSequence -1 is "Show all"
                if (!model.IsNew && !string.IsNullOrEmpty(model.CustomerSequence) && model.CustomerSequence != "-1")
                {
                    var builder = new StringBuilder();

                    builder.Append(!model.CustomerSequence.StartsWith(_customerDefaultsSettings.ErpShipToPrefix) ? $"{model.CustomerSequence} - " : string.Empty);
                    builder.Append(model.CompanyName);
                    builder.Append(model.Address1.Length > 0 ? $" - {model.Address1}" : string.Empty);
                    builder.Append(model.Address2.Length > 0 ? $", {model.Address2}" : string.Empty);
                    builder.Append(model.City.Length > 0 ? $", {model.City}" : string.Empty);
                    builder.Append(model.State != null ? $", {model.State.Abbreviation}" : string.Empty);
                    builder.Append(model.PostalCode.Length > 0 ? $", {model.PostalCode}" : string.Empty);

                    model.Label = builder.ToString().Trim();
                }

                var warehouse = _warehouseService.GetCustomerWarehouse(serviceResult.ShipTo);
                model.Properties.Add("DefaultWarehouse", JsonConvert.SerializeObject(warehouse, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
            }

            return model;
        }
    }
}
