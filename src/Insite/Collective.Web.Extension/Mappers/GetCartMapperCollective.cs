﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Inventory;
using Collective.Web.Extension.Core.Settings;
using Insite.Cart.Services.Results;
using Insite.Cart.WebApi.V1.ApiModels;
using Insite.Cart.WebApi.V1.Mappers;
using Insite.Cart.WebApi.V1.Mappers.Interfaces;
using Insite.Core.Context;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi.Extensions;
using Insite.Core.WebApi.Interfaces;
using Insite.Customers.WebApi.V1.Mappers.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Mappers
{
    public class GetCartMapperCollective : GetCartMapper
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ICollectiveInventoryService _collectiveInventoryService;
        private readonly ICustomerOrderUtilities _customerOrderUtilities;
        private readonly CollectiveSouthCarolinaSolidWasteTaxSettings _southCarolinaSolidWasteTaxSettings;

        public GetCartMapperCollective(ICurrencyFormatProvider currencyFormatProvider, IGetBillToMapper getBillToMapper, IGetShipToMapper getShipToMapper, IGetCartLineCollectionMapper getCartLineCollectionMapper, IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper, IRouteDataProvider routeDataProvider, ITranslationLocalizer translationLocalizer, ICustomerOrderUtilities customerOrderUtilities, CollectiveSouthCarolinaSolidWasteTaxSettings southCarolinaSolidWasteTaxSettings, ICollectiveInventoryService collectiveInventoryService, IAuthenticationService authenticationService)
            : base(currencyFormatProvider, getBillToMapper, getShipToMapper, getCartLineCollectionMapper, objectToObjectMapper, urlHelper, routeDataProvider, translationLocalizer)
        {
            _customerOrderUtilities = customerOrderUtilities;
            _southCarolinaSolidWasteTaxSettings = southCarolinaSolidWasteTaxSettings;
            _collectiveInventoryService = collectiveInventoryService;
            _authenticationService = authenticationService;
        }

        public override CartModel MapResult(GetCartResult serviceResult, HttpRequestMessage request)
        {
            var cartModel = base.MapResult(serviceResult, request);

            var isUserLogged = SiteContext.Current.UserProfile != null;
            var canShowPrice = false;
            var cartHasRestrictedProducts = false;

            cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.CartId, serviceResult.Cart.Id.ToString());

            if (!string.IsNullOrEmpty(serviceResult.Cart.ErpOrderNumber))
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.ErpOrderNumber, serviceResult.Cart.ErpOrderNumber);
            }

            var isSouthCarolinaSolidWasteTaxable = isUserLogged && _customerOrderUtilities.GetStateTaxRate(serviceResult.Cart)?.Id == Constants.Ids.States.SouthCarolina;
            foreach (var cartLine in cartModel.CartLines)
            {
                var productDto = serviceResult.CartLineResults.FirstOrDefault(x => x.ProductDto.ERPNumber == cartLine.ErpNumber)?.ProductDto;
                var isCatalogProduct = (productDto?.Properties.ContainsKey(Constants.CustomProperties.Product.IsCatalogProduct) ?? false) && ParseHelper.ParseBoolean(productDto.Properties[Constants.CustomProperties.Product.IsCatalogProduct], false);

                var useCartWarehouse = ParseHelper.ParseBoolean(request.GetQueryString("useCartWarehouse")).GetValueOrDefault(false);
                var warehouseName = useCartWarehouse ? serviceResult.Cart.GetProperty(Constants.CustomProperties.Cart.Warehouse, string.Empty) : string.Empty;

                var availabilityStatus = _collectiveInventoryService.GetProductInventoryStatusModel(cartLine.ErpNumber, isCatalogProduct, warehouseName);

                if (availabilityStatus.Status == AvailabilityStatusType.Restricted)
                {
                    cartLine.CanAddToCart = false;
                    cartLine.CanBackOrder = false;
                    cartHasRestrictedProducts = true;
                }

                cartLine.Properties.Add(Constants.HandlerResultProperties.Cart.AvailabilityStatus, JsonConvert.SerializeObject(availabilityStatus));

                var lineCanShowPrice = isUserLogged && availabilityStatus.Status != AvailabilityStatusType.Restricted;
                if (lineCanShowPrice)
                {
                    cartLine.Properties.Add(Constants.HandlerResultProperties.Cart.CanShowPrice, bool.TrueString.ToLower());
                }

                if (isSouthCarolinaSolidWasteTaxable)
                {
                    if (productDto?.Properties.ContainsKey(Constants.CustomProperties.Product.IsSouthCarolinaSolidWasteTaxable) ?? false)
                    {
                        if (ParseHelper.ParseBoolean(productDto.Properties[Constants.CustomProperties.Product.IsSouthCarolinaSolidWasteTaxable], false))
                        {
                            cartLine.Properties.Add(Constants.HandlerResultProperties.Cart.HasSouthCarolinaSolidWasteTax, bool.TrueString.ToLower());
                            cartLine.Properties.Add(Constants.HandlerResultProperties.Cart.SouthCarolinaSolidWasteTaxAmount, _southCarolinaSolidWasteTaxSettings.Amount.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }

                canShowPrice |= lineCanShowPrice;
            }

            if (canShowPrice)
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.CanShowPrice, bool.TrueString.ToLower());
            }

            var isPickUp = ParseHelper.ParseBoolean(serviceResult.Cart.ShipVia?.GetProperty(Constants.CustomProperties.ShipVia.IsPickup, string.Empty)).GetValueOrDefault(false);

            if (isPickUp)
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.ShipViaIsPickup, bool.TrueString.ToLower());
                cartModel.Properties.Remove(Constants.HandlerResultProperties.Cart.ShipComplete);
            }
            else
            {
                if (!cartModel.Properties.ContainsKey(Constants.HandlerResultProperties.Cart.ShipComplete))
                {
                    cartModel.Properties.Add(
                        Constants.HandlerResultProperties.Cart.ShipComplete,
                        ParseHelper.ParseBoolean(serviceResult.Cart.GetProperty(Constants.HandlerResultProperties.Cart.ShipComplete, bool.TrueString.ToLower()))
                            .GetValueOrDefault(true).ToString().ToLower());
                }
            }

            if (cartHasRestrictedProducts)
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.CartHasRestrictedProducts, bool.TrueString.ToLower());
            }

            if (!WebsiteHelper.WebsiteHasTaxCalculation())
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.NoTaxCalculation, bool.TrueString.ToLower());
            }

            if (!_customerOrderUtilities.CustomerOrderIsTaxable(serviceResult.Cart))
            {
                cartModel.Properties.Add(Constants.HandlerResultProperties.Cart.IsTaxExempt, bool.TrueString.ToLower());
            }

            if (cartModel.RequestedDeliveryDateDisplay.HasValue)
            {
                if (DateTime.TryParse(cartModel.RequestedDeliveryDate, out var requestedDeliveryDate))
                {
                    cartModel.RequestedDeliveryDate = requestedDeliveryDate.ToString("yyyy-MM-dd");
                }
                else
                {
                    cartModel.RequestedDeliveryDateDisplay = DateTime.Today;
                    cartModel.RequestedDeliveryDate = DateTime.Today.ToString("yyyy-MM-dd");
                }
            }
            else
            {
                cartModel.RequestedDeliveryDateDisplay = DateTime.Today;
                cartModel.RequestedDeliveryDate = DateTime.Today.ToString("yyyy-MM-dd");
            }

            if (isUserLogged && _authenticationService.IsUserInRole(SiteContext.Current.UserProfile.UserName, Constants.Roles.Technician))
            {
                cartModel.CanCheckOut = false;
            }

            return cartModel;
        }
    }
}
