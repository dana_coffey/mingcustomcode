﻿using System;
using System.Linq;
using System.Net.Http;
using Collective.Core.Constant;
using Collective.Core.Extensions;
using Collective.Core.Helpers;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Models.Warehouse;
using Collective.Web.Extension.Services;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Core.WebApi.Extensions;
using Insite.Core.WebApi.Interfaces;
using Insite.Data.Entities;
using Insite.Order.Services.Results;
using Insite.Order.WebApi.V1.ApiModels;
using Insite.Order.WebApi.V1.Mappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Collective.Web.Extension.Mappers
{
    public class GetOrderMapperCollective : GetOrderMapper
    {
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;
        private readonly ICollectiveMaxRecallService _maxRecallService;
        private readonly IRepository<OrderHistoryLineSerialNumber> _orderHistoryLineSerialNumberRepository;
        private readonly IRepository<ShipVia> _shipViaRepository;
        private readonly IRepository<Warehouse> _warehouseRepository;

        public GetOrderMapperCollective(ICurrencyFormatProvider currencyFormatProvider, IUrlHelper urlHelper, IObjectToObjectMapper objectToObjectMapper, ITranslationLocalizer translationLocalizer, IUnitOfWorkFactory unitOfWorkFactory, CustomerDefaultsSettings customerDefaultsSettings, CollectiveMaxRecallService maxRecallService) : base(currencyFormatProvider, urlHelper, objectToObjectMapper, translationLocalizer)
        {
            _customerDefaultsSettings = customerDefaultsSettings;

            _shipViaRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<ShipVia>();
            _warehouseRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<Warehouse>();
            _orderHistoryLineSerialNumberRepository = unitOfWorkFactory.GetUnitOfWork().GetRepository<OrderHistoryLineSerialNumber>();
            _maxRecallService = maxRecallService;
        }

        public override OrderModel MapResult(GetOrderResult serviceResult, HttpRequestMessage request)
        {
            var model = base.MapResult(serviceResult, request);

            // remove customer sequence of custom shipping addresses
            if (model.CustomerSequence.StartsWithIgnoreCase(_customerDefaultsSettings.ErpShipToPrefix))
            {
                model.CustomerSequence = string.Empty;
            }

            var shipVia = _shipViaRepository.GetTableAsNoTracking().FirstOrDefault(p => p.ShipCode == model.ShipCode);
            var shipViaIsPickup = ParseHelper.ParseBoolean(shipVia?.GetProperty(Constants.CustomProperties.ShipVia.IsPickup, string.Empty)).GetValueOrDefault(false);
            if (shipViaIsPickup)
            {
                model.Properties.Add(Constants.HandlerResultProperties.Order.ShipViaIsPickup, true.ToString().ToLower());
            }

            var warehouseName = serviceResult.OrderHistory.GetProperty(Constants.CustomProperties.Order.Warehouse, string.Empty);
            if (!string.IsNullOrEmpty(warehouseName))
            {
                var warehouse = _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(p => p.Name == warehouseName);
                if (warehouse != null)
                {
                    model.Properties.Remove(Constants.HandlerResultProperties.Order.Warehouse);
                    model.Properties.Add(Constants.HandlerResultProperties.Order.Warehouse, JsonConvert.SerializeObject(ToWarehouseModel(warehouse), new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }));
                }
            }

            var canShowPrice = GetCanShowPrice();

            model.Properties.Add(Constants.HandlerResultProperties.Order.CanShowPrice, canShowPrice.ToString().ToLower());

            var orderLinesIds = model.OrderLines.Select(y => y.Id).ToList();
            var serialNumbers = _orderHistoryLineSerialNumberRepository.GetTableAsNoTracking().Where(x => orderLinesIds.Contains(x.OrderHistoryLineId.ToString()));

            foreach (var orderLine in model.OrderLines)
            {
                var orderLineSerialNumbers = serialNumbers.Where(x => x.OrderHistoryLineId.ToString() == orderLine.Id).OrderBy(x => x.SeqNo).Select(x => x.SerialNo).ToArray();
                orderLine.Properties.Add(Constants.HandlerResultProperties.OrderLine.SerialNumbers, string.Join(", ", orderLineSerialNumbers));

                if (!canShowPrice)
                {
                    orderLine.ExtendedUnitNetPrice = 0;
                    orderLine.ExtendedUnitNetPriceDisplay = string.Empty;
                    orderLine.UnitNetPrice = 0;
                    orderLine.UnitNetPriceDisplay = string.Empty;
                    orderLine.UnitListPrice = 0;
                    orderLine.UnitListPriceDisplay = string.Empty;
                    orderLine.UnitRegularPrice = 0;
                    orderLine.UnitRegularPriceDisplay = string.Empty;
                    orderLine.UnitCost = 0;
                    orderLine.UnitCostDisplay = string.Empty;
                    orderLine.TotalRegularPrice = 0;
                    orderLine.TotalRegularPriceDisplay = string.Empty;
                }
            }

            if (model.Status.In(Constants.Insite.Order.Status.Complete, Constants.Insite.Order.Status.Invoiced, Constants.Insite.Order.Status.Cancelled)
                && !string.IsNullOrEmpty(request.GetQueryString("expand"))
                && request.GetQueryString("expand").Contains("documents"))
            {
                if (model.Properties.All(x => x.Key != Constants.HandlerResultProperties.Order.HasInvoice))
                {
                    if (_maxRecallService.DoesRemoteFileExists(serviceResult.OrderHistory.Id, MaxRecallFileType.Invoice))
                    {
                        model.Properties.Add(Constants.HandlerResultProperties.Order.HasInvoice, bool.TrueString);
                    }
                    else
                    {
                        model.Properties.Remove(Constants.HandlerResultProperties.Order.HasInvoice);
                    }
                }

                if (model.Properties.All(x => x.Key != Constants.HandlerResultProperties.Order.HasProofOfDelivery))
                {
                    if (_maxRecallService.DoesRemoteFileExists(serviceResult.OrderHistory.Id, MaxRecallFileType.ProofOfDelivery))
                    {
                        model.Properties.Add(Constants.HandlerResultProperties.Order.HasProofOfDelivery, bool.TrueString);
                    }
                    else
                    {
                        model.Properties.Remove(Constants.HandlerResultProperties.Order.HasProofOfDelivery);
                    }
                }
            }

            return model;
        }

        protected virtual bool GetCanShowPrice()
        {
            return true;
        }

        protected virtual WarehouseModel ToWarehouseModel(Warehouse warehouse)
        {
            return new WarehouseModel
            {
                Address1 = warehouse.Address1,
                Address2 = warehouse.Address2,
                City = warehouse.City,
                Country = warehouse.Country.Name,
                Description = warehouse.Description,
                DisplayName = warehouse.Description,
                Id = warehouse.Id.ToString(),
                Name = warehouse.Name,
                PostalCode = warehouse.PostalCode,
                Phone = warehouse.Phone,
                State = warehouse.State,
            };
        }
    }
}
