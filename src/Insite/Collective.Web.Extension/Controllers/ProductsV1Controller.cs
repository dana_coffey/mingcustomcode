﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Product;
using Collective.Web.Extension.Core.Models.Product.PartsList;
using Collective.Web.Extension.Helpers;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi;
using TheCollective.Common.Carrier.Models.Warranty;
using TheCollective.Common.Carrier.Services;

namespace Collective.Web.Extension.Controllers
{
    [RoutePrefix("api/v1/products")]
    public class StoreProductsV1Controller : BaseApiController
    {
        private readonly ICollectiveProductService _productService;
        private readonly ICarrierWarrantyLookupApiService _warrantyService;

        public StoreProductsV1Controller(ICookieManager cookieManager) : base(cookieManager)
        {
            _warrantyService = CarrierApiHelper.GetWarrantyLookupApiService();
            _productService = CollectiveInjector.GetInstance<ICollectiveProductService>();
        }

        [Route("{productId}/parts")]
        [ResponseType(typeof(PartsListModel))]
        public async Task<IHttpActionResult> GetPartsList(Guid productId)
        {
            return await Task.FromResult(Ok(CarrierApiHelper.GetPartsList(productId)));
        }

        [Route("{productId}/related-product/{type}")]
        [ResponseType(typeof(RelatedProductCollectionModel))]
        public async Task<IHttpActionResult> GetRelatedProduct(Guid productId, string type)
        {
            return await Task.FromResult(Ok(_productService.GetRelatedProducts(productId, type)));
        }

        [HttpGet]
        [Route("warranty/{serialNumber}/{modelNumber?}")]
        [ResponseType(typeof(WarrantyResult))]
        public IHttpActionResult GetWarrantyDataBySerialNumber(string serialNumber, string modelNumber = null)
        {
            IHttpActionResult httpActionResult = BadRequest();

            if (!string.IsNullOrEmpty(serialNumber))
            {
                var warrantyResult = _warrantyService.GetProductWarrantyInformation(serialNumber, modelNumber);
                if (warrantyResult == null)
                {
                    httpActionResult = NotFound();
                }
                else if (warrantyResult.Type == WarrantyResultType.Error)
                {
                    httpActionResult = InternalServerError();
                }
                else
                {
                    _warrantyService.TranslateWarrantyLabels(warrantyResult);
                    httpActionResult = Ok(warrantyResult);
                }
            }

            return httpActionResult;
        }
    }
}
