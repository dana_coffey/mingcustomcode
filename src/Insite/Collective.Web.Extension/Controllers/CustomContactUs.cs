﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Insite.ContentLibrary.Controllers;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Emails;
using Insite.Data.Repositories.Interfaces;
using Insite.WebFramework.Mvc;

namespace Collective.Web.Extension.Controllers
{
    public class CustomContactUsController : BaseController
    {
        private readonly IEmailService _emailService;

        public CustomContactUsController(IUnitOfWorkFactory unitOfWorkFactory, IEmailService emailService) : base(unitOfWorkFactory)
        {
            this._emailService = emailService;
        }

        [HttpPost]
        public ActionResult Submit(string message, string topic, string emailTo, string subTopic, string email, string fname, string lname)
        {
            if (email != string.Empty)
            {
                emailTo += "," + email;
            }

            IList<Attachment> attachmentList = new List<Attachment>();

            if (Request.Files.Count > 0)
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    if (file.FileName != string.Empty)
                    {
                        attachmentList.Add(new Attachment(file.InputStream, file.FileName));
                    }
                }

                SendEmail(message, topic, subTopic, email, fname, lname, emailTo, attachmentList);
            }
            else
            {
                SendEmail(message, topic, subTopic, email, fname, lname, emailTo);
            }

            return this.Json(new { Success = true });
        }

        private void SendEmail(string message, string topic, string subTopic, string email, string fname, string lname, string emailTo, IList<Attachment> attachmentList = null)
        {
            dynamic emailModel = new ExpandoObject();
            emailModel.Email = email;
            emailModel.FirstName = fname;
            emailModel.LastName = lname;
            emailModel.Topic = topic;
            emailModel.Message = message;
            emailModel.SubTopic = subTopic;

            var emailList = this.UnitOfWork.GetTypedRepository<IEmailListRepository>().GetOrCreateByName("FeedbackTemplate", "Feedback");
            this._emailService.SendEmailList(emailList.Id, emailTo.Split(','), emailModel, "Feedback Submission: " + subTopic, this.UnitOfWork, null, attachmentList);
        }
    }
}
