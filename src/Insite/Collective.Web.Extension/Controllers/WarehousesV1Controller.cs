﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Warehouse;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi;

namespace Collective.Web.Extension.Controllers
{
    [RoutePrefix("api/v1/warehouses")]
    public class WarehousesV1Controller : BaseApiController
    {
        private readonly ICollectiveWarehouseService _warehouseService;

        public WarehousesV1Controller(ICookieManager cookieManager) : base(cookieManager)
        {
            _warehouseService = CollectiveInjector.GetInstance<ICollectiveWarehouseService>();
        }

        [Route("available/{customerSpecificWarehouse}")]
        [ResponseType(typeof(List<WarehouseModel>))]
        public async Task<IHttpActionResult> GetAvailableWarehouses(string customerSpecificWarehouse)
        {
            return await Task.FromResult(Ok(new { warehouses = _warehouseService.GetAvailableWarehouses(customerSpecificWarehouse) }));
        }

        [Route("pickup")]
        [ResponseType(typeof(List<WarehouseModel>))]
        public async Task<IHttpActionResult> GetPickUpWarehouses()
        {
            return await Task.FromResult(Ok(new { warehouses = _warehouseService.GetPickUpWarehouses() }));
        }

        [Route("shipfrom")]
        [ResponseType(typeof(List<WarehouseModel>))]
        public async Task<IHttpActionResult> GetShipFromWarehouses()
        {
            return await Task.FromResult(Ok(new { warehouses = _warehouseService.GetShipFromWarehouses() }));
        }

        [Route("{warehouseName}")]
        [ResponseType(typeof(WarehouseModel))]
        public async Task<IHttpActionResult> GetWarehouse(string warehouseName)
        {
            var warehouseModel = _warehouseService.GetWarehouse(warehouseName);
            if (warehouseModel != null)
            {
                return await Task.FromResult(Ok(warehouseModel));
            }

            return await Task.FromResult(this.NotFound());
        }
    }
}
