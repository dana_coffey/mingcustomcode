﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Location;
using Insite.Core.Context;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi;

namespace Collective.Web.Extension.Controllers
{
    [RoutePrefix("api/v1/locations")]
    public class LocationsV1Controller : BaseApiController
    {
        private readonly ICollectiveLocationService _locationService;

        public LocationsV1Controller(ICookieManager cookieManager, ICollectiveLocationService locationService) : base(cookieManager)
        {
            _locationService = locationService;
        }

        [Route("current")]
        [ResponseType(typeof(LocationModel))]
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            return await Task.FromResult(Ok(_locationService.Get(SiteContext.Current?.UserProfile?.Id, SiteContext.Current?.UserProfile?.DefaultCustomer?.IsShipTo ?? false ? SiteContext.Current.UserProfile.DefaultCustomer : SiteContext.Current?.ShipTo)));
        }

        [Route("reset")]
        [ResponseType(typeof(void))]
        [HttpPost]
        public async Task<IHttpActionResult> Reset()
        {
            _locationService.Reset(SiteContext.Current?.UserProfile?.Id);
            return await Task.FromResult(Ok());
        }

        [Route("update")]
        [ResponseType(typeof(LocationModel))]
        [HttpPost]
        public async Task<IHttpActionResult> Update(LocationModel location)
        {
            _locationService.Update(SiteContext.Current?.UserProfile?.Id, location);
            return await Task.FromResult(Ok());
        }
    }
}
