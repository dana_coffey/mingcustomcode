﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Services;
using Insite.Core.Context;
using Insite.Core.Plugins.Utilities;
using Insite.Core.WebApi;

namespace Collective.Web.Extension.Controllers
{
    [RoutePrefix("api/v1/orders")]
    public class CustomOrderV1Controller : BaseApiController
    {
        private readonly ICollectiveMaxRecallService _maxRecallService;

        public CustomOrderV1Controller(ICookieManager cookieManager, ICollectiveMaxRecallService maxRecallService) : base(cookieManager)
        {
            _maxRecallService = maxRecallService;
        }

        [HttpGet]
        [Route("document/download/inv/{orderId}", Name = "CustomOrderV1DownloadInvoice")]
        public HttpResponseMessage DownloadInvoice(Guid orderId)
        {
            return DownloadFile(orderId, MaxRecallFileType.Invoice);
        }

        [HttpGet]
        [Route("document/download/pod/{orderId}", Name = "CustomOrderV1DownloadProofOfDelivery")]
        public HttpResponseMessage DownloadProofOfDelivery(Guid orderId)
        {
            return DownloadFile(orderId, MaxRecallFileType.ProofOfDelivery);
        }

        private HttpResponseMessage DownloadFile(Guid orderId, MaxRecallFileType fileType)
        {
            return _maxRecallService.IsAuthorized(orderId, SiteContext.Current?.BillTo?.CustomerNumber) ? _maxRecallService.DownloadFile(orderId, fileType) : Request.CreateResponse(HttpStatusCode.Forbidden);
        }
    }
}
