﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Helpers
{
    public class CollectiveTaxingHelper : ICollectiveTaxingHelper, ICollectiveInjectableClass
    {
        private readonly string _erpShipToPrefix;
        private readonly IUnitOfWork _unitOfWork;

        public CollectiveTaxingHelper()
        {
            _unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _erpShipToPrefix = DependencyLocator.Current.GetInstance<CustomerDefaultsSettings>().ErpShipToPrefix;
        }

        public virtual bool IsCustomerTaxable(string customerNumber, string stateCode, Guid? countryId)
        {
            var websiteTaxExemptionCode = _unitOfWork.GetRepository<Website>().GetTableAsNoTracking().FirstOrDefault(x => x.Id == SiteContext.Current.WebsiteDto.Id)?.TaxFreeTaxCode;
            var stateMatchingShipTos = _unitOfWork.GetRepository<Customer>().GetTableAsNoTracking().Where(x => x.CustomerNumber == customerNumber && !x.CustomerSequence.StartsWith(_erpShipToPrefix) && x.State.Abbreviation == stateCode && x.CountryId == countryId).ToList();

            if (stateMatchingShipTos.Any())
            {
                return stateMatchingShipTos.All(x => !string.Equals(x.TaxCode1, websiteTaxExemptionCode, StringComparison.InvariantCultureIgnoreCase));
            }

            // Validate if the bill to is tax exempted
            return _unitOfWork.GetRepository<Customer>().GetTableAsNoTracking().All(x => x.CustomerNumber == customerNumber && string.IsNullOrEmpty(x.CustomerSequence) && x.TaxCode1 != websiteTaxExemptionCode);
        }

        public virtual bool IsCustomerOrderTaxable(CustomerOrder customerOrder, string pickupShipCode, bool defaultValue)
        {
            return defaultValue;
        }
    }
}
