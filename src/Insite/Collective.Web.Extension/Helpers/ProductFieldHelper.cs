﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Models.Product;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Helpers
{
    public static class ProductFieldHelper
    {
        public static string GetLocalizedFieldName(ProductFieldEntity productField)
        {
            if (productField != null)
            {
                var productFieldNames = JsonConvert.DeserializeObject<LocalizedStringJsonModel>(productField.FieldName);

                return productFieldNames.GetByCulture(SiteContext.Current.LanguageDto.CultureCode);
            }

            return string.Empty;
        }

        public static List<ProductFieldEntity> GetProductFields()
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();

            var cacheEntry = cacheManager.Get<List<ProductFieldEntity>>(Constants.Cache.ProductField);
            if (cacheEntry != null)
            {
                return cacheEntry;
            }

            var unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            var productFieldsRepository = unitOfWork.GetRepository<ProductFieldEntity>();

            var productFields = productFieldsRepository.GetTableAsNoTracking().ToList();
            cacheManager.Add(Constants.Cache.ProductField, productFields, TimeSpan.FromHours(12));

            return productFields;
        }
    }
}
