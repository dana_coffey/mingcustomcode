﻿using System;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Product.PartsList;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Plugins.Caching;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Services;

namespace Collective.Web.Extension.Helpers
{
    public static class CarrierApiHelper
    {
        private static readonly object Lock = new object();

        private static ICarrierClient _carrierClient;
        private static ICarrierEpicApiService _epicApiService;
        private static ICarrierEpicClient _epicClient;
        private static ICarrierWarrantyLookupApiService _warrantyLookupApiService;
        private static ICarrierWarrantyLookupClient _warrantyLookupClient;

        private static bool _isEpicInitialized;
        private static bool _isWarrantyInitialized;

        public static ICarrierEpicApiService GetEpicApiService()
        {
            lock (Lock)
            {
                if (!_isEpicInitialized)
                {
                    _isEpicInitialized = true;

                    _carrierClient = new CarrierClient();
                    _epicClient = new CarrierEpicClient(_carrierClient);
                    _epicApiService = new CarrierEpicApiService(_epicClient)
                    {
                        ApiCredentials = new CarrierApiCredentials
                        {
                            ApiPassword = Constants.AppSettings.CarrierApi.ApiPassword,
                            BaseApiUrl = Constants.AppSettings.CarrierApi.Url,
                            BearerTokenPassword = Constants.AppSettings.CarrierApi.BearerTokenPassword,
                            BearerTokenUsername = Constants.AppSettings.CarrierApi.BearerTokenUsername,
                            Username = Constants.AppSettings.CarrierApi.ApiUsername
                        }
                    };
                }
            }

            return _epicApiService;
        }

        public static ICarrierWarrantyLookupApiService GetWarrantyLookupApiService()
        {
            lock (Lock)
            {
                if (!_isWarrantyInitialized)
                {
                    _isWarrantyInitialized = true;

                    _carrierClient = new CarrierClient();
                    _warrantyLookupClient = new CarrierWarrantyLookupClient(_carrierClient);
                    _warrantyLookupApiService = new CarrierWarrantyLookupApiService(_warrantyLookupClient)
                    {
                        ApiCredentials = new CarrierApiCredentials
                        {
                            ApiPassword = Constants.AppSettings.CarrierApi.ApiPassword,
                            BaseApiUrl = Constants.AppSettings.CarrierApi.Url,
                            BearerTokenPassword = Constants.AppSettings.CarrierApi.BearerTokenPassword,
                            BearerTokenUsername = Constants.AppSettings.CarrierApi.BearerTokenUsername,
                            Username = Constants.AppSettings.CarrierApi.ApiUsername
                        }
                    };
                }
            }

            return _warrantyLookupApiService;
        }

        public static PartsListModel GetPartsList(Guid productId)
        {
            var partService = CollectiveInjector.GetInstance<ICollectivePartService>();
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cacheKey = CacheKeyHelper.GeneratePartsListCacheKey(productId.ToString());

            if (cacheManager?.Contains(cacheKey) ?? false)
            {
                return cacheManager.Get<PartsListModel>(cacheKey);
            }

            var partsList = partService.GetPartsList(productId);
            cacheManager?.Add(cacheKey, partsList, TimeSpan.FromDays(1));

            return partsList;
        }
    }
}
