﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Helpers
{
    public static class WarehouseHelper
    {
        private const string CustomerSpecificWarehouseAllAccess = "1";
        private static readonly object LockObject = new object();

        public static List<string> GetAvailableWarehousesNamesWithCustomerSpecificWarehouse(string customerSpecificWarehouse)
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cacheKey = $"SpecificWarehouse_AvailableWarehouseNames_{customerSpecificWarehouse}";

            if (cacheManager.Contains(cacheKey))
            {
                return cacheManager.Get<List<string>>(cacheKey);
            }

            var names = new List<string>();

            lock (LockObject)
            {
                var activeWarehouses = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<Warehouse>().GetTableAsNoTracking().Where(x => !x.DeactivateOn.HasValue || x.DeactivateOn >= DateTimeOffset.Now).ToList();

                foreach (var warehouse in activeWarehouses)
                {
                    var warehouseType = warehouse.CustomProperties.FirstOrDefault(p => p.Name == Constants.CustomProperties.Warehouse.WarehouseType)?.Value ?? string.Empty;
                    var warehouseCustomer = warehouse.CustomProperties.FirstOrDefault(p => p.Name == Constants.CustomProperties.Warehouse.WarehouseCustomer)?.Value ?? string.Empty;

                    if (warehouseType.Equals(Constants.Warehouses.WarehouseType.Branch, StringComparison.InvariantCultureIgnoreCase) ||
                        (warehouseType.Equals(Constants.Warehouses.WarehouseType.MMI, StringComparison.InvariantCultureIgnoreCase) &&
                            ((!string.IsNullOrEmpty(warehouseCustomer.Trim()) && warehouseCustomer == customerSpecificWarehouse) || customerSpecificWarehouse == CustomerSpecificWarehouseAllAccess)))
                    {
                        names.Add(warehouse.Name);
                    }
                }

                cacheManager.Add(cacheKey, names, TimeSpan.FromHours(1));
            }

            return names;
        }

        public static List<string> GetAvailableWarehousesNamesForCurrentBillTo()
        {
            var customerSpecificWarehouse = GetBillToSpecificWarehouse(SiteContext.Current?.BillTo);
            return GetAvailableWarehousesNamesWithCustomerSpecificWarehouse(customerSpecificWarehouse);
        }

        public static string GetBillToSpecificWarehouse(Customer billTo)
        {
            if (billTo != null)
            {
                return billTo.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Customer.SpecificWarehouse)?.Value ?? string.Empty;
            }

            return string.Empty;
        }
    }
}
