﻿using System;
using Collective.Core.Constant;
using Insite.Account.Services.Handlers.Helpers;
using Insite.Core.Interfaces.Plugins.Emails;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.SystemSetting.Groups.OrderManagement;

namespace Collective.Web.Extension.Helpers
{
    public class AccountHelperCollective : AccountHelper
    {
        public AccountHelperCollective(Lazy<IAuthenticationService> authenticationService, Lazy<IEmailService> emailService, BudgetsAndOrderApprovalSettings budgetsAndOrderApprovalSettings, RequisitionsSettings requisitionsSettings) : base(authenticationService, emailService, budgetsAndOrderApprovalSettings, requisitionsSettings)
        {
            if (!StandardRoles.Contains("Buyer3"))
            {
                StandardRoles.Add("Buyer3");
            }

            if (!StandardRoles.Contains(Constants.Roles.Technician))
            {
                StandardRoles.Add(Constants.Roles.Technician);
            }
        }
    }
}
