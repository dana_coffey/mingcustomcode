﻿using System.Collections.Generic;

namespace Collective.Web.Extension.Helpers
{
    public static class RoleHelper
    {
        public static List<string> GetAvailableRoles()
        {
            return new List<string>
            {
                "Administrator",
                "Buyer3",
                "MING_Technician"
            };
        }
    }
}
