﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.InforSxe.Core.Interfaces;
using Collective.InforSxe.Core.Models;
using Collective.InforSxe.Core.Models.OrderSubmit;
using Collective.Web.Extension.Core.Helpers;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Helpers
{
    public class MockSxeApiHelper : ISxeApiHelper
    {
        public InventoryApiModel GetInventory(string productErpNumber, List<string> activeWarehouses)
        {
            return GetInventory(new List<string> { productErpNumber }, activeWarehouses)?.FirstOrDefault();
        }

        public IEnumerable<InventoryApiModel> GetInventory(List<string> productErpNumbers, List<string> activeWarehouses)
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cachedItems = GetFromCache<InventoryApiModel>(productErpNumbers, cacheManager, CacheKeyHelper.GenerateInventoryCacheKey);
            var productIdsNotInCache = productErpNumbers.Where(p => cachedItems.All(c => c.ProductId != p)).ToList();

            if (productIdsNotInCache.Any())
            {
                try
                {
                    var allActiveWarehouses = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<Warehouse>().GetTableAsNoTracking().Where(x => x.DeactivateOn == null || x.DeactivateOn > DateTimeOffset.Now).ToList();

                    // Remove Random warehouses to get Restricted products
                    allActiveWarehouses.RemoveRange(new Random().Next(1, allActiveWarehouses.Count - 2), 2);

                    var inventoryModels = productErpNumbers.Select(x => new InventoryApiModel
                    {
                        ProductId = x,
                        Warehouses = allActiveWarehouses.Select(w => new WarehouseInventoryApiModel
                        {
                            Quantity = GetQuantity(x, w.Name),
                            WarehouseId = w.Name
                        }),
                    }).ToList();

                    SetInCache(inventoryModels, cacheManager, inventoryModel => CacheKeyHelper.GenerateInventoryCacheKey(inventoryModel.ProductId));

                    return cachedItems.Concat(inventoryModels);
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return cachedItems;
        }

        public PricingApiModel GetPrice(int billtoId, string shiptoId, string productId, string warehouseId)
        {
            return GetPrices(billtoId, shiptoId, new List<string> { productId }, warehouseId).FirstOrDefault();
        }

        public IEnumerable<PricingApiModel> GetPrices(int billtoId, string shiptoId, List<string> productIds, string warehouseId)
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cachedItems = GetFromCache<PricingApiModel>(productIds, cacheManager, productId => CacheKeyHelper.GeneratePricingCacheKey(billtoId, shiptoId, productId, warehouseId));
            var productIdsNotInCache = productIds.Where(p => cachedItems.All(c => !string.Equals(c.ProductId, p, StringComparison.OrdinalIgnoreCase))).ToList();

            if (productIdsNotInCache.Any())
            {
                try
                {
                    var random = new Random();
                    var pricingModels = productIdsNotInCache.Select(x => new PricingApiModel
                    {
                        BreakPrices = null,
                        CurrencyConversionRate = 1,
                        IsOnSale = random.Next(0, 1) != 0,
                        ProductId = x,
                        Price = (decimal)(random.Next(2, 2500) + random.NextDouble()),
                    }).ToList();

                    pricingModels.ForEach(x => x.NetPrice = x.IsOnSale ? x.Price - new Random().Next(1, 15) : x.Price);
                    SetInCache(pricingModels, cacheManager, pricingModel => CacheKeyHelper.GeneratePricingCacheKey(billtoId, shiptoId, pricingModel.ProductId, warehouseId));

                    return cachedItems.Concat(pricingModels);
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return cachedItems;
        }

        private IEnumerable<T> GetFromCache<T>(List<string> productIds, ICacheManager cacheManager, Func<string, string> generateCacheKeyFunc)
        {
            foreach (var productId in productIds)
            {
                var cacheKey = generateCacheKeyFunc(productId);
                if (cacheManager?.Contains(cacheKey) ?? false)
                {
                    yield return cacheManager.Get<T>(cacheKey);
                }
            }
        }

        private decimal GetQuantity(string productErpNumber, string warehouse)
        {
            decimal quantity = new Random().Next(0, 20);
            var unavailableProducts = new Dictionary<string, string> // sku, warehouse
            {
                { "FE4ANF002L00", string.Empty },
                { "FX4DNF019T00", "21" }
            };

            if (unavailableProducts.ContainsKey(productErpNumber))
            {
                if (string.IsNullOrEmpty(unavailableProducts[productErpNumber]))
                {
                    return 0;
                }

                if (unavailableProducts[productErpNumber] == warehouse)
                {
                    return 0;
                }
            }

            return quantity;
        }

        private void SetInCache<T>(List<T> models, ICacheManager cacheManager, Func<T, string> generateCacheKeyFunc)
        {
            models.ForEach(model => cacheManager?.Add(generateCacheKeyFunc(model), model, TimeSpan.FromMinutes(5)));
        }
    }
}
