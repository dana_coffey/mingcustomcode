﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Collective.Core.Injector;
using Collective.InforSxe.Core.Interfaces;
using Collective.InforSxe.Core.Models;
using Collective.Web.Extension.Core.Helpers;
using Insite.Common.Dependencies;
using Insite.Common.Logging;
using Insite.Core.Interfaces.Plugins.Caching;

namespace Collective.Web.Extension.Helpers
{
    public class SxeApiHelper : ISxeApiHelper
    {
        protected const string InventoryApiErrorKey = "ApiInventoryError";
        protected const string PricingApiErrorKey = "PricingApiErrorKey";

        public virtual InventoryApiModel GetInventory(string productErpNumber, List<string> activeWarehouses)
        {
            return GetInventory(new List<string> { productErpNumber }, activeWarehouses)?.FirstOrDefault();
        }

        public virtual IEnumerable<InventoryApiModel> GetInventory(List<string> productErpNumbers, List<string> activeWarehouses)
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cachedItems = GetFromCache<InventoryApiModel>(productErpNumbers, cacheManager, CacheKeyHelper.GenerateInventoryCacheKey).ToList();
            var productIdsNotInCache = productErpNumbers.Where(p => cachedItems.All(c => c.ProductId != p)).ToList();
            var availableWarehouseNames = WarehouseHelper.GetAvailableWarehousesNamesForCurrentBillTo();
            var inventoryModels = new List<InventoryApiModel>();

            if (productIdsNotInCache.Any())
            {
                try
                {
                    if (!HttpContext.Current.Items.Contains(InventoryApiErrorKey))
                    {
                        var apiService = CollectiveInjector.GetInstance<ICollectiveApiService>();
                        var inventoryApiModels = apiService.GetInventory(productErpNumbers, activeWarehouses).ToList();
                        SetInCache(inventoryApiModels, cacheManager, inventoryModel => CacheKeyHelper.GenerateInventoryCacheKey(inventoryModel.ProductId));
                        cachedItems.AddRange(inventoryApiModels);
                    }
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Items[InventoryApiErrorKey] = true;
                    LogHelper.For(typeof(InventoryApiModel)).Error("Inventory API call failed", ex);

                    return null;
                }
            }

            foreach (var apiInventory in cachedItems)
            {
                inventoryModels.Add(new InventoryApiModel
                {
                    ProductId = apiInventory.ProductId,
                    Warehouses = apiInventory.Warehouses.Where(p => availableWarehouseNames.Contains(p.WarehouseId)).ToList(),
                });
            }

            return inventoryModels;
        }

        public virtual PricingApiModel GetPrice(int billtoId, string shiptoId, string productId, string warehouseId)
        {
            return GetPrices(billtoId, shiptoId, new List<string> { productId }, warehouseId).FirstOrDefault();
        }

        public virtual IEnumerable<PricingApiModel> GetPrices(int billtoId, string shiptoId, List<string> productIds, string warehouseId)
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();
            var cachedItems = GetFromCache<PricingApiModel>(productIds, cacheManager, productId => CacheKeyHelper.GeneratePricingCacheKey(billtoId, shiptoId, productId, warehouseId));
            var productIdsNotInCache = productIds.Where(p => cachedItems.All(c => !string.Equals(c.ProductId, p, StringComparison.OrdinalIgnoreCase))).ToList();

            if (productIdsNotInCache.Any())
            {
                try
                {
                    if (!HttpContext.Current.Items.Contains(PricingApiErrorKey))
                    {
                        var apiService = CollectiveInjector.GetInstance<ICollectiveApiService>();
                        var pricingModels = apiService.GetPrices(billtoId, shiptoId, productIdsNotInCache, warehouseId)?.ToList();
                        if (pricingModels != null)
                        {
                            SetInCache(pricingModels, cacheManager, pricingModel => CacheKeyHelper.GeneratePricingCacheKey(billtoId, shiptoId, pricingModel.ProductId, warehouseId));
                            return cachedItems.Concat(pricingModels);
                        }
                    }
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Items[PricingApiErrorKey] = true;
                    LogHelper.For(typeof(InventoryApiModel)).Error("Pricing API call failed", ex);

                    return null;
                }
            }

            return cachedItems;
        }

        protected IEnumerable<T> GetFromCache<T>(List<string> productIds, ICacheManager cacheManager, Func<string, string> generateCacheKeyFunc)
        {
            foreach (var productId in productIds)
            {
                var cacheKey = generateCacheKeyFunc(productId);
                if (cacheManager?.Contains(cacheKey) ?? false)
                {
                    yield return cacheManager.Get<T>(cacheKey);
                }
            }
        }

        protected void SetInCache<T>(List<T> models, ICacheManager cacheManager, Func<T, string> generateCacheKeyFunc)
        {
            models.ForEach(model => cacheManager?.Add(generateCacheKeyFunc(model), model, TimeSpan.FromMinutes(5)));
        }
    }
}
