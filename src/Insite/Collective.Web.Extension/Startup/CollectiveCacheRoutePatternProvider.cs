﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using CacheCow.Server.RoutePatternPolicy;
using Insite.Common.Dependencies;
using Insite.Core.SystemSetting.Groups.Catalog;
using Insite.Core.WebApi.Extensions;

namespace Collective.Web.Extension.Startup
{
    public class CollectiveCacheRoutePatternProvider : IRoutePatternProvider
    {
        public IEnumerable<string> GetLinkedRoutePatterns(HttpRequestMessage request)
        {
            if (request.Method == HttpMethod.Get)
            {
                return new List<string> { string.Empty };
            }

            var pattern = GetPattern(request);
            if (request.Method == HttpMethod.Post && pattern.StartsWith("quotes", StringComparison.OrdinalIgnoreCase))
            {
                return new List<string>
                {
                    "carts" + request.GetCookie("InsiteCacheId")
                };
            }

            var instance = DependencyLocator.Current.GetInstance<InventorySettings>();
            if (request.Method == new HttpMethod("PATCH") && pattern.StartsWith("carts", StringComparison.OrdinalIgnoreCase) && instance.ShowInventoryAvailability != InventoryAvailabilityDisplay.Never)
            {
                return new List<string>()
                {
                    "products" + request.GetCookie("InsiteCacheId")
                };
            }

            if (pattern.StartsWith("sessions", StringComparison.OrdinalIgnoreCase))
            {
                return GetInvalidateRoutes(request);
            }

            if ((pattern.StartsWith("accounts", StringComparison.OrdinalIgnoreCase) || pattern.StartsWith("locations", StringComparison.OrdinalIgnoreCase)) && request.Method == HttpMethod.Post)
            {
                return GetInvalidateRoutes(request);
            }

            return new List<string> { pattern };
        }

        public string GetRoutePattern(HttpRequestMessage request)
        {
            return GetPattern(request);
        }

        private static List<string> GetInvalidateRoutes(HttpRequestMessage request)
        {
            var cookie = request.GetCookie("InsiteCacheId");

            return new List<string>()
            {
                "accounts" + cookie,
                "autocomplete" + cookie,
                "billtos" + cookie,
                "carts" + cookie,
                "catalogpages" + cookie,
                "categories" + cookie,
                "products" + cookie,
                "sessions" + cookie,
                "websites/current/crosssells" + cookie
            };
        }

        private static string GetPattern(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new HttpException(404, "Invalid Route");
            }

            var requestUri = request.RequestUri;
            if (requestUri.Segments.Count() < 4)
            {
                return string.Empty;
            }

            var text = requestUri.Segments[3].TrimEnd('/');
            if (text.EqualsIgnoreCase("admin") && requestUri.Segments.Length >= 5)
            {
                text = requestUri.Segments[4].TrimEnd('/');
                if (text.Contains("("))
                {
                    text = text.Substring(0, text.IndexOf("("));
                }
            }
            else if (text.EqualsIgnoreCase("websites") && requestUri.Segments.Length >= 6 && requestUri.Segments[5].TrimEnd('/').EqualsIgnoreCase("crosssells"))
            {
                text += "/current/crosssells";
            }

            return text + request.GetCookie("InsiteCacheId");
        }
    }
}
