﻿using System.Linq;
using System.Web.Http;
using CacheCow.Server;
using Insite.Core.BootStrapper;
using Insite.Core.Interfaces.BootStrapper;
using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups.SystemSettings;
using Insite.Core.WebApi;
using Owin;

namespace Collective.Web.Extension.Startup
{
    [BootStrapperOrder(21)]
    public class EtagCacheStartupTask : IStartupTask
    {
        public void Run(IAppBuilder app, HttpConfiguration config)
        {
            if (SettingsGroupProvider.Current.Get<PerformanceSettings>().EtagCachingEnabled)
            {
                if (config.MessageHandlers.FirstOrDefault(x => x is CachingHandler handler && handler.RoutePatternProvider.GetType() == typeof(CacheRoutePatternProvider)) is CachingHandler cachingHandler)
                {
                    cachingHandler.RoutePatternProvider = new CollectiveCacheRoutePatternProvider();
                }
            }
        }
    }
}
