﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Insite.ContentLibrary.Widgets;
using Insite.Core.BootStrapper;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.WebFramework.Content;
using Insite.WebFramework.Content.Providers;

namespace Collective.Web.Extension.Admin.Providers
{
    public class ContentItemTypeProviderCollective : IContentItemTypeProvider
    {
        private readonly List<Type> _allowedWidgets = new List<Type>
        {
            typeof(Image),
            typeof(OneColumn),
            typeof(TwoColumn),
            typeof(RichContent),
            typeof(PageTitle),
            typeof(Breadcrumb),
            typeof(Image),
            typeof(ContactUsForm)
        };

        protected ICacheManager CacheManager { get; }

        public ContentItemTypeProviderCollective(ICacheManager cacheManager)
        {
            CacheManager = cacheManager;
        }

        public ContentItemModel CreateForClassName(string className)
        {
            return CreateForClassName<ContentItemModel>(className);
        }

        public T CreateForClassName<T>(string className) where T : ContentItemModel
        {
            var type = GetPotentialTypes().FirstOrDefault(x => x.Name.EqualsIgnoreCase(className));
            if (type == null)
            {
                throw new ArgumentException("There was no Type found for the className: " + className, nameof(className));
            }

            if (Activator.CreateInstance(type) is T instance)
            {
                return instance;
            }

            throw new ArgumentException($"The type {type.Name} identified by the className {className} was not assignable to the type {typeof(T).Name}");
        }

        public Type GetTypeForClassName(string className)
        {
            var type = GetPotentialTypes().FirstOrDefault(o => o.Name.EqualsIgnoreCase(className));
            if (type != null)
            {
                return type;
            }

            throw new ArgumentException("There was no Type found for the className: " + className, nameof(className));
        }

        public IList<Type> GetTypes(Type basedOnType = null)
        {
            if (basedOnType == null)
            {
                basedOnType = typeof(ContentItemModel);
            }

            var list = GetPotentialTypes().Where(x => !x.IsAbstract && basedOnType.IsAssignableFrom(x) && x.Name != "NullAbstractPage").ToList();

            if (basedOnType == typeof(AbstractWidget))
            {
                return list.Where(x => _allowedWidgets.Contains(x) || x.Assembly.FullName.ToLower().Contains("collective")).ToList();
            }

            return list;
        }

        protected virtual IReadOnlyCollection<Assembly> GetPotentialDependencyAssemblies()
        {
            return AssemblyLocator.GetPotentialDependencyAssemblies();
        }

        protected IList<Type> GetPotentialTypes()
        {
            var contentItemModelType = typeof(ContentItemModel);
            return CacheManager.Get("ContentItemTypeProvider_PotentialTypes", () => GetPotentialDependencyAssemblies().SelectMany(o => o.GetTypes()).Where(x => contentItemModelType.IsAssignableFrom(x)).ToList());
        }
    }
}
