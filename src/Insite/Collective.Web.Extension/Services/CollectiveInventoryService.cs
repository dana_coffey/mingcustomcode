﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.InforSxe.Core.Interfaces;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Inventory;
using Collective.Web.Extension.Core.Models.Warehouse;
using Collective.Web.Extension.Core.Settings;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Localization;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Services
{
    public class CollectiveInventoryService : ICollectiveInventoryService
    {
        private readonly CollectiveInventorySettings _collectiveInventorySettings;
        private readonly ISiteContextServiceFactory _siteContextServiceFactory;
        private readonly ISxeApiHelper _sxeApiHelper;
        private readonly ITranslationLocalizer _translationLocalizer;
        private readonly ICollectiveLocationService _collectiveLocationService;
        private readonly IRepository<Warehouse> _warehouseRepository;

        private ISiteContext SiteContext => _siteContextServiceFactory?.GetSiteContextService()?.GetSiteContext();

        public CollectiveInventoryService(ISiteContextServiceFactory siteContextServiceFactory, ISxeApiHelper sxeApiHelper, IRepository<Warehouse> warehouseRepository, CollectiveInventorySettings inventorySettings, ITranslationLocalizer translationLocalizer, ICollectiveLocationService collectiveLocationService)
        {
            _siteContextServiceFactory = siteContextServiceFactory;
            _collectiveInventorySettings = inventorySettings;
            _warehouseRepository = warehouseRepository;
            _sxeApiHelper = sxeApiHelper;
            _translationLocalizer = translationLocalizer;
            _collectiveLocationService = collectiveLocationService;
        }

        public Warehouse GetDefaultWarehouse(Customer currentCustomer)
        {
            return currentCustomer.DefaultWarehouse ?? GetDefaultWarehouse();
        }

        public AvailabilityStatusModel GetProductInventoryStatusModel(string productErpNumber, bool isCatalogProduct)
        {
            return GetProductInventoryStatusModel(productErpNumber, isCatalogProduct, string.Empty);
        }

        public AvailabilityStatusModel GetProductInventoryStatusModel(string productErpNumber, bool isCatalogProduct, string warehouse)
        {
            AvailabilityStatusModel result;

            if (isCatalogProduct)
            {
                result = new AvailabilityStatusModel
                {
                    Status = AvailabilityStatusType.NotAvailable,
                    LocalQuantity = 0,
                    TotalQuantity = 0
                };
            }
            else
            {
                try
                {
                    var inventoryModel = _sxeApiHelper.GetInventory(productErpNumber, WarehouseHelper.GetActiveWarehouseList());
                    var qtyAllWarehouses = inventoryModel?.TotalQuantity ?? decimal.Zero;

                    if (!string.IsNullOrEmpty(warehouse))
                    {
                        var localWarehouse = inventoryModel?.Warehouses?.FirstOrDefault(x => x.WarehouseId.Equals(warehouse, StringComparison.OrdinalIgnoreCase));
                        var isRestricted = localWarehouse == null;
                        var qtyLocalWarehouse = isRestricted ? decimal.Zero : localWarehouse.Quantity;

                        result = new AvailabilityStatusModel
                        {
                            Status = isRestricted ? AvailabilityStatusType.Restricted : qtyLocalWarehouse > 0 ? AvailabilityStatusType.AvailableLocal : AvailabilityStatusType.NotAvailable,
                            LocalQuantity = qtyLocalWarehouse,
                            TotalQuantity = qtyAllWarehouses
                        };
                    }
                    else
                    {
                        if (SiteContext?.UserProfile != null)
                        {
                            var localWarehouse = _collectiveLocationService.Get(SiteContext.UserProfile.Id, SiteContext.ShipTo, createLocation: false).Warehouse;
                            var qtyLocalWarehouse = inventoryModel?.Warehouses.FirstOrDefault(x => x.WarehouseId.Equals(localWarehouse.Name, StringComparison.InvariantCultureIgnoreCase))?.Quantity ?? decimal.Zero;

                            result = new AvailabilityStatusModel
                            {
                                Status = qtyLocalWarehouse > 0 ? AvailabilityStatusType.AvailableLocal : AvailabilityStatusType.NotAvailable,
                                LocalQuantity = qtyLocalWarehouse,
                                TotalQuantity = qtyAllWarehouses,
                                LocalWarehouse = localWarehouse
                            };
                        }
                        else
                        {
                            result = new AvailabilityStatusModel
                            {
                                Status = qtyAllWarehouses > 0 ? AvailabilityStatusType.AvailableLocal : AvailabilityStatusType.NotAvailable,
                                LocalQuantity = qtyAllWarehouses,
                                TotalQuantity = qtyAllWarehouses
                            };
                        }
                    }

                    if (_collectiveInventorySettings.ShowAvailabilityByWarehouse)
                    {
                        var activeWarehouses = _warehouseRepository.GetTableAsNoTracking().Where(x => !x.DeactivateOn.HasValue || x.DeactivateOn >= DateTimeOffset.Now).ToList();

                        result.Warehouses = inventoryModel?.Warehouses?.Select(p => ToAvailabilityStatusWarehouseModel(activeWarehouses.FirstOrDefault(x => x.Name.Equals(p.WarehouseId, StringComparison.InvariantCultureIgnoreCase)), p.Quantity))
                            .Where(p => p.Quantity > 0 || p.CanPickup || p.CanShipFrom).OrderBy(x => x.DisplayName).ToList();

                        result.ShowWarehouses = qtyAllWarehouses > 0;
                    }
                }
                catch (Exception)
                {
                    result = new AvailabilityStatusModel
                    {
                        Status = AvailabilityStatusType.Unknown,
                        LocalQuantity = decimal.Zero,
                        TotalQuantity = decimal.Zero
                    };
                }
            }

            result.Message = _translationLocalizer.TranslateLabel($"Store_ProductAvailability_{result.Status}");

            return result;
        }

        protected virtual Warehouse GetDefaultWarehouse()
        {
            var warehouse = _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(w => w.IsDefault);
            if (warehouse == null && SiteContext.WarehouseDto != null)
            {
                return _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(p => p.Id == SiteContext.WarehouseDto.Id);
            }

            return warehouse;
        }

        protected virtual AvailabilityStatusWarehouseModel ToAvailabilityStatusWarehouseModel(Warehouse warehouse, decimal quantity)
        {
            if (warehouse != null)
            {
                return new AvailabilityStatusWarehouseModel
                {
                    Address1 = warehouse.Address1,
                    Address2 = warehouse.Address2,
                    CanPickup = ParseHelper.ParseBoolean(warehouse.GetProperty(Constants.CustomProperties.Warehouse.CanPickUp, string.Empty), false),
                    CanShipFrom = ParseHelper.ParseBoolean(warehouse.GetProperty(Constants.CustomProperties.Warehouse.CanShip, string.Empty), false),
                    City = warehouse.City,
                    DisplayName = warehouse.Description,
                    Latitude = ParseHelper.ParseDecimal(warehouse.GetProperty(Constants.CustomProperties.Warehouse.Latitude, string.Empty)).GetValueOrDefault(0),
                    Longitude = ParseHelper.ParseDecimal(warehouse.GetProperty(Constants.CustomProperties.Warehouse.Longitude, string.Empty)).GetValueOrDefault(0),
                    Name = warehouse.Name,
                    PostalCode = warehouse.PostalCode,
                    Quantity = quantity,
                    State = warehouse.State,
                };
            }

            return null;
        }

        protected virtual WarehouseModel ToWarehouseModel(Warehouse warehouse)
        {
            return new WarehouseModel
            {
                Address1 = warehouse.Address1,
                Address2 = warehouse.Address2,
                City = warehouse.City,
                Country = warehouse.Country.Name,
                Description = warehouse.Description,
                DisplayName = warehouse.Description,
                Id = warehouse.Id.ToString(),
                Name = warehouse.Name,
                PostalCode = warehouse.PostalCode,
                Phone = warehouse.Phone,
                State = warehouse.State,
            };
        }
    }
}
