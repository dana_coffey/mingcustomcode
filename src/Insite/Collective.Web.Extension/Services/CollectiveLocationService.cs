﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Location;
using Collective.Web.Extension.Core.Models.Warehouse;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Services.Interfaces;
using Insite.Websites.WebApi.V1.ApiModels;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Services
{
    public class CollectiveLocationService : ICollectiveLocationService
    {
        private static readonly object _lock = new object();
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<ItemCache> _itemCacheRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICollectiveWarehouseService _warehouseService;

        public CollectiveLocationService(IUnitOfWorkFactory unitOfWorkFactory, ICollectiveWarehouseService warehouseService, ICacheManager cacheManager, IEtagCacheService etagCacheService)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
            _warehouseService = warehouseService;
            _cacheManager = cacheManager;
            _itemCacheRepository = _unitOfWork.GetRepository<ItemCache>();
        }

        public LocationModel Get(Guid? userId, Customer shipTo, bool createLocation = true)
        {
            lock (_lock)
            {
                LocationModel location = null;
                if (userId != null)
                {
                    location = JsonConvert.DeserializeObject<LocationModel>(GetLocationFromCache(userId)?.Value ?? string.Empty);

                    if (createLocation)
                    {
                        location = Create(userId, shipTo);
                    }
                }

                return location;
            }
        }

        public void Reset(Guid? userId)
        {
            lock (_lock)
            {
                RemoveLocationFromCache(userId);
            }
        }

        public void Update(Guid? userId, LocationModel location)
        {
            lock (_lock)
            {
                location.InitialLocation = false;
                SetLocationInCache(userId, location);
            }
        }

        private static string GetKey(Guid? userId)
        {
            return $"{Constants.Cache.ItemCache}{userId?.ToString()}";
        }

        private LocationModel Create(Guid? userId, Customer shipTo)
        {
            LocationModel location = null;
            if (userId != null)
            {
                location = JsonConvert.DeserializeObject<LocationModel>(GetLocationFromCache(userId)?.Value ?? string.Empty);

                if (location == null)
                {
                    var warehouse = _warehouseService.GetCustomerWarehouse(shipTo);

                    location = new LocationModel
                    {
                        Address1 = shipTo.Address1,
                        Address2 = shipTo.Address2,
                        Address3 = shipTo.Address3,
                        Address4 = shipTo.Address4,
                        City = shipTo.City,
                        PostalCode = shipTo.PostalCode,
                        Country = new CountryModel
                        {
                            Id = shipTo.Country.Id.ToString(),
                            Abbreviation = shipTo.Country.Abbreviation,
                            Name = shipTo.Country.Name,
                            States = shipTo.Country.States.ToList().Select(x => new StateModel
                            {
                                Id = x.Id.ToString(),
                                Abbreviation = x.Abbreviation,
                                Name = x.Name,
                            }).ToList()
                        },
                        State = new StateModel
                        {
                            Id = shipTo.State.Id.ToString(),
                            Abbreviation = shipTo.State.Abbreviation,
                            Name = shipTo.State.Name,
                        },
                        Warehouse = new WarehouseModel
                        {
                            Id = warehouse.Id,
                            Country = warehouse.Country,
                            Name = warehouse.Name,
                            State = warehouse.State,
                            Address1 = warehouse.Address1,
                            Address2 = warehouse.Address2,
                            City = warehouse.City,
                            Description = warehouse.Description,
                            DisplayName = warehouse.Description,
                            IsDefault = warehouse.IsDefault,
                            Phone = warehouse.Phone,
                            PostalCode = warehouse.PostalCode
                        },
                        Latitude = null,
                        Longitude = null,
                        InitialLocation = true
                    };

                    SetLocationInCache(userId, location);
                }
            }

            return location;
        }

        private ItemCache GetLocationFromCache(Guid? userId)
        {
            var key = GetKey(userId);

            return _cacheManager.Get<ItemCache>(key, () =>
            {
                return _itemCacheRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Key == key);
            }, TimeSpan.FromMinutes(15));
        }

        private void RemoveLocationFromCache(Guid? userId)
        {
            var key = GetKey(userId);

            var item = _itemCacheRepository.GetTable().FirstOrDefault(x => x.Key == key);
            if (item != null)
            {
                _itemCacheRepository.Delete(item);
                _unitOfWork.Save();
            }

            if (_cacheManager.Contains(key))
            {
                _cacheManager.Remove(key);
            }
        }

        private void SetLocationInCache(Guid? userId, LocationModel location)
        {
            var key = GetKey(userId);
            var item = _itemCacheRepository.GetTable().FirstOrDefault(x => x.Key == key);

            if (item == null)
            {
                _itemCacheRepository.Insert(new ItemCache { Key = key, Value = JsonConvert.SerializeObject(location) });
            }
            else
            {
                item.Value = JsonConvert.SerializeObject(location);

                if (_cacheManager.Contains(key))
                {
                    _cacheManager.Remove(key);
                }
            }

            _cacheManager.Add(key, item, TimeSpan.FromMinutes(15));
            _unitOfWork.Save();
        }
    }
}
