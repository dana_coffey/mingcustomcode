﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Product;
using Collective.Web.Extension.Helpers;
using Insite.Catalog.Services.Handlers.Helpers;
using Insite.Catalog.Services.Parameters;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.EnumTypes;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Catalog;
using Insite.Data.Entities;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Services
{
    public class CollectiveProductService : ICollectiveProductService, ICollectiveInjectableClass
    {
        private readonly ICatalogPathBuilder _catalogPathBuilder;
        private readonly IGetProductHandlerHelper _getProductHandlerHelper;
        private readonly IRepository<ProductBarcode> _productBarcodeRepository;
        private readonly IRepository<ProductLinkEntity> _productLinkRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly ITranslationLocalizer _translationLocalizer;
        private readonly IUnitOfWork _unitOfWork;

        public CollectiveProductService()
        {
            _catalogPathBuilder = DependencyLocator.Current.GetInstance<ICatalogPathBuilder>();

            _unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _productRepository = _unitOfWork.GetRepository<Product>();
            _productBarcodeRepository = _unitOfWork.GetRepository<ProductBarcode>();
            _productLinkRepository = _unitOfWork.GetRepository<ProductLinkEntity>();

            _getProductHandlerHelper = DependencyLocator.Current.GetInstance<IGetProductHandlerHelper>();

            _translationLocalizer = DependencyLocator.Current.GetInstance<ITranslationLocalizer>();
        }

        public List<ProductCustomFieldModel> GetCustomFields(Guid productId)
        {
            var results = new List<ProductCustomFieldModel>();
            var productFields = ProductFieldHelper.GetProductFields();
            var product = _productRepository.Get(productId);

            if (product != null)
            {
                var customFields = GetCustomFields(product);
                if (customFields != null)
                {
                    foreach (var customField in customFields)
                    {
                        var productField = productFields.FirstOrDefault(x => x.FieldId.Equals(customField.Key, StringComparison.OrdinalIgnoreCase) && x.ExcludedFromSpecifications == false);
                        if (customField.Value != null && productField != null)
                        {
                            results.Add(new ProductCustomFieldModel
                            {
                                Name = ProductFieldHelper.GetLocalizedFieldName(productField),
                                Value = GetCustomFieldValue(customField, productField)
                            });
                        }
                    }
                }
            }

            return results;
        }

        public List<ProductScanBarcodeModel> GetProductsByBarcode(string barcode)
        {
            var products = _productBarcodeRepository.GetTableAsNoTracking().Where(x => x.Barcode == barcode).ToList();
            return products.Select(x => new ProductScanBarcodeModel { ProductNumber = x.Product.ErpNumber, Url = _catalogPathBuilder.MakeCanonicalProductUrlPath(x.Product) }).ToList();
        }

        public RelatedProductCollectionModel GetRelatedProducts(Guid productId, string type)
        {
            var product = _productRepository.Get(productId);
            if (product != null)
            {
                var productLinks = _productLinkRepository.GetTableAsNoTracking().Where(x => x.ErpNumber == product.ErpNumber && x.Type == type).OrderBy(p => p.Order).ToList();
                if (productLinks.Any())
                {
                    var relatedProducts = productLinks
                        .Select(link => _productRepository.GetTableAsNoTracking().FirstOrDefault(x => x.ErpNumber == link.LinkTo && (!x.DeactivateOn.HasValue || x.DeactivateOn.Value > DateTime.Today)))
                        .Where(relatedProduct => relatedProduct != null)
                        .ToList();

                    if (relatedProducts.Any())
                    {
                        var productDtos = _getProductHandlerHelper.LoadProductDtoList(_unitOfWork, new GetProductCollectionParameter(), relatedProducts, null, SiteContext.Current.CurrencyDto);

                        return new RelatedProductCollectionModel
                        {
                            Products = relatedProducts.Select(p => productDtos.FirstOrDefault(dto => dto.Id == p.Id)).Where(p => p != null).ToList()
                        };
                    }
                }
            }

            return null;
        }

        public List<ProductReplacedModel> GetReplacementProducts(string erpNumber)
        {
            var productLinks = _productLinkRepository.GetTableAsNoTracking().Where(x => x.LinkTo.Equals(erpNumber, StringComparison.InvariantCultureIgnoreCase) && x.Type == Constants.ProductRelations.Supersede).OrderBy(p => p.Order).ToList();
            if (productLinks.Any())
            {
                var replacedProducts = productLinks
                    .Select(link => _productRepository.GetTableAsNoTracking().FirstOrDefault(x => x.ErpNumber.Equals(link.ErpNumber, StringComparison.InvariantCultureIgnoreCase)))
                    .ToList();

                if (replacedProducts.Any())
                {
                    return replacedProducts.Select(x => new ProductReplacedModel { ErpNumber = x.ErpNumber, ProductName = x.Name, Url = _catalogPathBuilder.MakeCanonicalProductUrlPath(x.UrlSegment) }).ToList();
                }
            }

            return null;
        }

        public List<ProductLinkEntity> GetSupersedeProducts(List<string> erpNumbers)
        {
            return _productLinkRepository.GetTableAsNoTracking().Where(x => erpNumbers.Contains(x.ErpNumber) && x.Type == Constants.ProductRelations.Supersede).OrderBy(p => p.Order).ToList();
        }

        private static ProductCustomFieldJsonModel GetCustomFields(Product product)
        {
            var json = product.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Product.CustomFields)?.Value;
            return !string.IsNullOrEmpty(json) ? JsonConvert.DeserializeObject<ProductCustomFieldJsonModel>(json) : null;
        }

        private string GetCustomFieldValue(KeyValuePair<string, LocalizedStringJsonModel> customField, ProductFieldEntity productField)
        {
            if (productField.DataType == Constants.ProductFields.DataTypes.Cvl)
            {
                var values = customField.Value.GetByCulture(SiteContext.Current.LanguageDto.CultureCode)?.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries) ?? new List<string>().ToArray();
                var results = values.Select(value => _translationLocalizer.Translate(TranslationDictionarySource.AttributeValue, value)).ToList();
                return $"{string.Join(", ", results)} {productField.UnitOfMeasure}";
            }

            if (productField.DataType == Constants.ProductFields.DataTypes.Boolean)
            {
                return _translationLocalizer.Translate(TranslationDictionarySource.Label, customField.Value.GetByCulture(SiteContext.Current.LanguageDto.CultureCode));
            }

            return $"{customField.Value.GetByCulture(SiteContext.Current.LanguageDto.CultureCode)} {productField.UnitOfMeasure}";
        }
    }
}
