﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Settings;
using Collective.Web.Extension.Utilities;
using Insite.Common.Logging;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Data.Entities;
using RestSharp;

namespace Collective.Web.Extension.Services
{
    public class CollectiveMaxRecallService : ICollectiveMaxRecallService
    {
        private readonly ICacheManager _cacheManager;
        private readonly CollectiveInvoiceSettings _invoiceSettings;
        private readonly IUnitOfWork _unitOfWork;

        public CollectiveMaxRecallService(IUnitOfWorkFactory unitOfWorkFactory, CollectiveInvoiceSettings invoiceSettings, ICacheManager cacheManager)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
            _invoiceSettings = invoiceSettings;
            _cacheManager = cacheManager;
        }

        public bool DoesRemoteFileExists(Guid orderId, MaxRecallFileType fileType)
        {
            var cacheKey = CacheKeyHelper.GenerateMaxRecallCacheKey(orderId, fileType);
            if (!_cacheManager.Contains(cacheKey))
            {
                _cacheManager.Add(cacheKey, FetchRemoteFileExists(orderId, fileType));
            }

            return _cacheManager.Get<bool>(cacheKey);
        }

        public HttpResponseMessage DownloadFile(Guid orderId, MaxRecallFileType fileType)
        {
            var fileResponse = new HttpResponseMessage(HttpStatusCode.NotFound);
            var urlBuilder = new MaxRecallUrlBuilder(orderId, fileType, _invoiceSettings, _unitOfWork);

            if (urlBuilder.OrderExists)
            {
                var apiResponse = GetFileData(urlBuilder);

                if (apiResponse.IsSuccessful)
                {
                    fileResponse = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(apiResponse.RawBytes)
                    };

                    fileResponse.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Pdf);
                    fileResponse.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = urlBuilder.FileName
                    };
                }
            }

            return fileResponse;
        }

        public bool IsAuthorized(Guid orderId, string customerNumber)
        {
            var orderHistory = _unitOfWork.GetRepository<OrderHistory>().GetTableAsNoTracking().FirstOrDefault(x => x.Id == orderId);

            if (orderHistory == null)
            {
                return false;
            }

            return customerNumber == orderHistory.CustomerNumber;
        }

        private bool FetchRemoteFileExists(Guid orderId, MaxRecallFileType fileType)
        {
            var urlBuilder = new MaxRecallUrlBuilder(orderId, fileType, _invoiceSettings, _unitOfWork);

            var request = WebRequest.CreateHttp(urlBuilder.Url);
            request.Method = WebRequestMethods.Http.Head;

            try
            {
                if (request.GetResponse() is HttpWebResponse response)
                {
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (WebException ex) when ((ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.NotFound)
            {
                LogHelper.For(request).Debug(ex);
            }
            catch (Exception ex)
            {
                LogHelper.For(request).Error(ex);
            }

            return false;
        }

        private IRestResponse GetFileData(MaxRecallUrlBuilder urlBuilder)
        {
            LogHelper.For(this).Debug($"Getting file {urlBuilder.FileType} from MaxRecall at url {urlBuilder.Url}");

            var client = new RestClient(urlBuilder.Url);
            client.AddDefaultHeader("Accept", MediaTypeNames.Application.Pdf);

            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", MediaTypeNames.Application.Pdf);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-disposition", $"attachment; filename={urlBuilder.FileName}");
            request.AddHeader("Content-Type", MediaTypeNames.Application.Pdf);

            return client.Execute(request);
        }
    }
}
