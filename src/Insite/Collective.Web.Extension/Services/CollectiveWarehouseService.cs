﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Warehouse;
using Collective.Web.Extension.Helpers;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Data.Entities.Dtos;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Services
{
    public class CollectiveWarehouseService : ICollectiveWarehouseService, ICollectiveInjectableClass
    {
        private readonly IWarehouseRepository _warehouseRepository;

        public CollectiveWarehouseService()
        {
            var unitOfWorkFactory = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>();
            _warehouseRepository = unitOfWorkFactory.GetUnitOfWork().GetTypedRepository<IWarehouseRepository>();
        }

        public virtual List<WarehouseModel> GetAvailableWarehouses(string customerSpecificWarehouse)
        {
            var warehouses = _warehouseRepository.GetTableAsNoTracking()
                .Where(x => x.DeactivateOn == null || x.DeactivateOn > DateTimeOffset.Now)
                .ToList()
                .Select(ToModel)
                .OrderBy(x => x.DisplayName)
                .ToList();
            return FilterWarehouses(warehouses, customerSpecificWarehouse);
        }

        public virtual WarehouseModel GetCustomerWarehouse(Customer customer)
        {
            var warehouse = customer.DefaultWarehouse;
            if (warehouse == null)
            {
                var warehouses = _warehouseRepository.GetTableAsNoTracking();
                warehouse = warehouses.FirstOrDefault(x => x.IsDefault) ?? warehouses.First();
            }

            return ToModel(warehouse);
        }

        public virtual List<WarehouseModel> GetPickUpWarehouses()
        {
            var warehouses = _warehouseRepository.GetTableAsNoTracking().Where(x => x.DeactivateOn == null || x.DeactivateOn > DateTimeOffset.Now).ToList();
            var pickupWarehouses = warehouses.Where(x => ParseHelper.ParseBoolean(x.GetProperty(Constants.CustomProperties.Warehouse.CanPickUp, string.Empty), false)).Select(ToModel).OrderBy(x => x.DisplayName).ToList();
            return FilterWarehouses(pickupWarehouses, SiteContext.Current?.BillTo);
        }

        public virtual List<WarehouseModel> GetShipFromWarehouses()
        {
            var warehouses = _warehouseRepository.GetTableAsNoTracking().Where(x => x.DeactivateOn == null || x.DeactivateOn > DateTimeOffset.Now).ToList();
            var shipFromWarehouses = warehouses.Where(x => ParseHelper.ParseBoolean(x.GetProperty(Constants.CustomProperties.Warehouse.CanShip, string.Empty), false)).Select(ToModel).OrderBy(x => x.DisplayName).ToList();
            return FilterWarehouses(shipFromWarehouses, SiteContext.Current?.BillTo);
        }

        public virtual WarehouseModel GetWarehouse(Guid warehouseId)
        {
            var warehouse = _warehouseRepository.Get(warehouseId);
            return warehouse != null ? ToModel(warehouse) : null;
        }

        public virtual WarehouseModel GetWarehouse(string warehouseName)
        {
            var warehouse = _warehouseRepository.GetByNaturalKey(warehouseName);
            return warehouse != null ? ToModel(warehouse) : null;
        }

        public virtual WarehouseDto GetWarehouseDto(Guid warehouseId)
        {
            var warehouse = _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Id == warehouseId);
            return warehouse != null ? new WarehouseDto(warehouse, loadAlternateWarehouses: false) : null;
        }

        protected virtual WarehouseModel ToModel(Warehouse warehouse)
        {
            return new WarehouseModel
            {
                Address1 = warehouse.Address1,
                Address2 = warehouse.Address2,
                City = warehouse.City,
                Country = warehouse.Country.Name,
                Name = warehouse.Name,
                Description = warehouse.Description,
                DisplayName = warehouse.Description,
                Id = warehouse.Id.ToString(),
                IsDefault = warehouse.IsDefault,
                Phone = warehouse.Phone,
                PostalCode = warehouse.PostalCode,
                State = warehouse.State,
                Properties = warehouse.CustomProperties.ToDictionary(x => x.Name, x => x.Value)
            };
        }

        private static List<WarehouseModel> FilterWarehouses(List<WarehouseModel> warehouses, Customer billTo)
        {
            var customerSpecificWarehouse = WarehouseHelper.GetBillToSpecificWarehouse(billTo);
            return FilterWarehouses(warehouses, customerSpecificWarehouse);
        }

        private static List<WarehouseModel> FilterWarehouses(List<WarehouseModel> warehouses, string customerSpecificWarehouse)
        {
            var availableWarehouseNames = WarehouseHelper.GetAvailableWarehousesNamesWithCustomerSpecificWarehouse(customerSpecificWarehouse);
            return warehouses.Where(p => availableWarehouseNames.Contains(p.Name)).ToList();
        }
    }
}
