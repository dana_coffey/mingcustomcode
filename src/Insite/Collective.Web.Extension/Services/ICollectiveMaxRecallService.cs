﻿using System;
using System.Net.Http;
using Collective.Web.Extension.Core.Enums;
using Insite.Core.Interfaces.Dependency;

namespace Collective.Web.Extension.Services
{
    public interface ICollectiveMaxRecallService : IDependency
    {
        bool DoesRemoteFileExists(Guid orderId, MaxRecallFileType fileType);

        HttpResponseMessage DownloadFile(Guid orderId, MaxRecallFileType fileType);

        bool IsAuthorized(Guid orderId, string customerNumber);
    }
}
