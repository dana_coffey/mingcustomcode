﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.InforSxe.Core.Interfaces;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Services
{
    public class CollectiveInventoryProvider : ICollectiveInventoryProvider, IDependency
    {
        private readonly ISxeApiHelper _sxeApiHelper;

        public CollectiveInventoryProvider(ISxeApiHelper sxeApiHelper)
        {
            _sxeApiHelper = sxeApiHelper;
        }

        public decimal GetQtyOnHand(Product product, string warehouseName)
        {
            if (ParseHelper.ParseBoolean(product.GetProperty(Constants.CustomProperties.Product.IsCatalogProduct, false.ToString()), false))
            {
                return 0;
            }

            var inventoryModel = _sxeApiHelper.GetInventory(product.ErpNumber, WarehouseHelper.GetActiveWarehouseList());

            return string.IsNullOrEmpty(warehouseName) ? inventoryModel?.TotalQuantity ?? decimal.Zero : inventoryModel?.Warehouses?.FirstOrDefault(x => x.WarehouseId.Equals(warehouseName, StringComparison.OrdinalIgnoreCase))?.Quantity ?? 0;
        }
    }
}
