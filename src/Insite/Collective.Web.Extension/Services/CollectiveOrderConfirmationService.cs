﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Models;
using Collective.Integration.Core.Models;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Order;
using Collective.Web.Extension.Core.Settings;
using Insite.Common;
using Insite.Common.Dependencies;
using Insite.Common.Logging;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.EnumTypes;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Interfaces.Plugins.Emails;
using Insite.Core.Plugins.Emails;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.SystemSetting.Groups.SiteConfigurations;
using Insite.Core.Translation;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Services
{
    public class CollectiveOrderConfirmationService : ICollectiveOrderConfirmationService
    {
        private readonly IBuildEmailValues _buildEmailValues;
        private readonly IContentManagerUtilities _contentManagerUtilities;
        private readonly IRepository<CustomerOrder> _customerOrderRepository;
        private readonly ICustomerOrderUtilities _customerOrderUtilities;

        private readonly IEmailListRepository _emailListRepository;
        private readonly IEmailService _emailService;
        private readonly EmailsSettings _emailsSettings;
        private readonly IEmailTemplateUtilities _emailTemplateUtilities;
        private readonly IEntityTranslationService _entityTranslationService;
        private readonly ILanguageRepository _languageRepository;
        private readonly IRepository<OrderHistory> _orderHistoryRepository;
        private readonly IRepository<ShipVia> _shipViaRepository;
        private readonly IRepository<Salesperson> _salesPersonRepository;
        private readonly ITranslationLocalizer _translationLocalizer;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Warehouse> _warehouseRepository;

        public CollectiveOrderConfirmationService(IUnitOfWorkFactory unitOfWorkFactory, IEmailService emailService, EmailsSettings emailsSettings, ICustomerOrderUtilities customerOrderUtilities, IEmailTemplateUtilities emailTemplateUtilities, IBuildEmailValues buildEmailValues, IContentManagerUtilities contentManagerUtilities, IEntityTranslationService entityTranslationService, ITranslationLocalizer translationLocalizer)
        {
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
            _emailService = emailService;
            _emailsSettings = emailsSettings;
            _customerOrderUtilities = customerOrderUtilities;
            _emailTemplateUtilities = emailTemplateUtilities;
            _buildEmailValues = buildEmailValues;
            _contentManagerUtilities = contentManagerUtilities;
            _entityTranslationService = entityTranslationService;
            _translationLocalizer = translationLocalizer;

            _emailListRepository = _unitOfWork.GetTypedRepository<IEmailListRepository>();
            _languageRepository = _unitOfWork.GetTypedRepository<ILanguageRepository>();
            _orderHistoryRepository = _unitOfWork.GetRepository<OrderHistory>();
            _warehouseRepository = _unitOfWork.GetRepository<Warehouse>();
            _customerOrderRepository = _unitOfWork.GetRepository<CustomerOrder>();
            _shipViaRepository = _unitOfWork.GetRepository<ShipVia>();
            _salesPersonRepository = _unitOfWork.GetRepository<Salesperson>();
        }

        public void SendConfirmationEmailByErpOrderNumber(string erpOrderNumber)
        {
            var order = _orderHistoryRepository.GetTable().FirstOrDefault(p => p.ErpOrderNumber == erpOrderNumber);
            var customerOrder = _customerOrderRepository.GetTable().FirstOrDefault(p => p.ErpOrderNumber == erpOrderNumber);
            LogHelper.For(this).Debug($"OrderConfirmationService: Order ID (from ERP): {order?.Id.ToString()}");
            SendConfirmationEmail(order, customerOrder, erpOrderNumber);
        }

        public void SendConfirmationEmailByWebOrderNumber(string webOrderNumber)
        {
            var order = _orderHistoryRepository.GetTable().FirstOrDefault(p => p.WebOrderNumber == webOrderNumber);
            var customerOrder = _customerOrderRepository.GetTable().FirstOrDefault(p => p.OrderNumber == webOrderNumber);
            LogHelper.For(this).Debug($"OrderConfirmationService: Order ID (from Web): {order?.Id.ToString()}");
            SendConfirmationEmail(order, customerOrder, webOrderNumber);
        }

        private void BuildConfirmationEmailViewModelLocationData(OrderHistory order, dynamic viewModel)
        {
            var locationJson = order.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Order.Location)?.Value;
            LogHelper.For(this).Debug($"OrderConfirmationService: Location JSON : {locationJson}");
            if (locationJson == null)
            {
                return;
            }

            var location = JsonConvert.DeserializeObject<OrderLocationModel>(locationJson);

            viewModel.HasLocation = location != null;
            if (location != null)
            {
                viewModel.LAddress1 = location.Address1;
                viewModel.LAddress2 = location.Address2;
                viewModel.LAddress3 = location.Address3;
                viewModel.LAddress4 = location.Address4;
                viewModel.LCity = location.City;
                viewModel.LState = location.State;
                viewModel.LPostalCode = location.PostalCode;
            }
        }

        private void BuildConfirmationEmailViewModelSubAccountData(OrderHistory order, dynamic viewModel)
        {
            var subAccountJson = order.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Order.SubAccount)?.Value;
            LogHelper.For(this).Debug($"OrderConfirmationService: Sub Account JSON : {subAccountJson}");
            if (subAccountJson == null)
            {
                return;
            }

            var subAccount = JsonConvert.DeserializeObject<OrderSubAccountModel>(subAccountJson);

            viewModel.HasSubAccount = subAccount != null;
            if (subAccount != null)
            {
                viewModel.SADisplayName = subAccount.CompanyName;
                viewModel.SAAddress1 = subAccount.Address1;
                viewModel.SAAddress2 = subAccount.Address2;
                viewModel.SAAddress3 = subAccount.Address3;
                viewModel.SAAddress4 = subAccount.Address4;
                viewModel.SACity = subAccount.City;
                viewModel.SAState = subAccount.State;
                viewModel.SAPostalCode = subAccount.PostalCode;
            }
        }

        private ExpandoObject BuildConfirmationEmailViewModel(OrderHistory order, Language language, List<string> southCarolinaSolidWasteTaxableProducts)
        {
            var culture = new CultureInfo(language.LanguageCode);
            dynamic viewModel = new ExpandoObject();

            viewModel.OrderNumber = order.ErpOrderNumber;
            viewModel.CustomerPO = order.CustomerPO;
            viewModel.OrderDate = order.OrderDate.ToString("D", culture);
            viewModel.OrderLines = order.OrderHistoryLines.Select(p => GetOrderLine(p, culture, southCarolinaSolidWasteTaxableProducts.Contains(p.ProductErpNumber, StringComparer.InvariantCultureIgnoreCase))).ToList();
            viewModel.HasBackOrder = order.OrderHistoryLines.Any(x => x.QtyOrdered > x.QtyShipped);

            var southCarolinaSolidWasteTaxSettings = DependencyLocator.Current.GetInstance<CollectiveSouthCarolinaSolidWasteTaxSettings>();
            viewModel.SouthCarolinaSolidWasteTaxAmountDisplay = southCarolinaSolidWasteTaxSettings.Amount.ToString("C", culture);

            viewModel.BTDisplayName = order.BTCompanyName;
            viewModel.BTAddress1 = order.BTAddress1;
            viewModel.BTAddress2 = order.BTAddress2;
            viewModel.BTAddress3 = string.Empty;
            viewModel.BTCity = order.BTCity;
            viewModel.BTState = order.BTState;
            viewModel.BTPostalCode = order.BTPostalCode;

            viewModel.STDisplayName = order.STCompanyName;
            viewModel.STAddress1 = order.STAddress1;
            viewModel.STAddress2 = order.STAddress2;
            viewModel.STAddress3 = string.Empty;
            viewModel.STCity = order.STCity;
            viewModel.STState = order.STState;
            viewModel.STPostalCode = order.STPostalCode;

            BuildConfirmationEmailViewModelSubAccountData(order, viewModel);
            BuildConfirmationEmailViewModelLocationData(order, viewModel);
            BuildConfirmationEmailViewModelWarehouseData(order, viewModel);

            var shipVia = _shipViaRepository.GetTableAsNoTracking().FirstOrDefault(p => p.ShipCode == order.ShipCode);
            viewModel.ShipMethod = shipVia != null ? _entityTranslationService.TranslateProperty(shipVia, p => p.Description, language) : string.Empty;
            viewModel.ShipMethodIsPickup = ParseHelper.ParseBoolean(shipVia?.GetProperty(Constants.CustomProperties.ShipVia.IsPickup, string.Empty)).GetValueOrDefault(false);

            viewModel.OrderSubTotal = order.OrderHistoryLines.Sum(l => l.QtyOrdered * l.UnitNetPrice);
            viewModel.OrderSubTotalDisplay = viewModel.OrderSubTotal.ToString("C", culture);
            viewModel.PromotionProductDiscountTotal = 0;
            viewModel.PromotionProductDiscountTotalDisplay = string.Empty;
            viewModel.PromotionOrderDiscountTotal = order.OrderDiscountAmount;
            viewModel.PromotionOrderDiscountTotalDisplay = order.OrderDiscountAmount.ToString("C", culture);
            viewModel.FullShippingChargeDisplay = order.ShippingCharges.ToString("C", culture);
            viewModel.Handling = 0;
            viewModel.HandlingDisplay = string.Empty;
            viewModel.PromotionShippingDiscountTotal = 0;
            viewModel.PromotionShippingDiscountTotalDisplay = string.Empty;
            viewModel.CustomerOrderTaxes = order.OrderHistoryTaxes.Select(p => GetOrderTaxes(p, culture, language)).ToList();
            viewModel.TotalTaxDisplay = WebsiteHelper.WebsiteHasTaxCalculation() ? order.TaxAmount.ToString("C", culture) : _translationLocalizer.TranslateLabel("TBD");
            viewModel.CreditCardWillBeCharged = false;
            viewModel.OrderGrandTotalDisplay = viewModel.OrderSubTotalDisplay;
            viewModel.GiftCardTotal = 0;
            viewModel.GiftCardTotalDisplay = string.Empty;
            viewModel.OrderTotalDueDisplay = string.Empty;
            viewModel.Notes = order.Notes;
            viewModel.RequestedDeliveryDate = order.RequestedDeliveryDate.HasValue ? order.RequestedDeliveryDate.Value.ToString("D", culture) : string.Empty;
            viewModel.ShipComplete = ParseHelper.ParseBoolean(order.GetProperty(Constants.CustomProperties.Cart.ShipComplete, bool.TrueString)).GetValueOrDefault(true);

            return viewModel;
        }

        private void BuildConfirmationEmailViewModelWarehouseData(OrderHistory order, dynamic viewModel)
        {
            var warehouseName = order.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Order.Warehouse)?.Value ?? string.Empty;
            var warehouse = _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Name == warehouseName);
            viewModel.HasWarehouse = warehouse != null;
            if (warehouse != null)
            {
                viewModel.WAddress1 = warehouse.Address1;
                viewModel.WAddress2 = warehouse.Address2;
                viewModel.WCity = warehouse.City;
                viewModel.WState = warehouse.State;
                viewModel.WPostalCode = warehouse.PostalCode;
                viewModel.WDisplayName = warehouse.Description;
            }
        }

        private List<string> GetConfirmationEmailCcAddresses(OrderHistory order)
        {
            if (!string.IsNullOrEmpty(_emailsSettings.TestEmail) || string.IsNullOrEmpty(order.Salesperson))
            {
                return null;
            }

            var emailList = new List<string>();
            var salesPersonEmail = _salesPersonRepository.GetTableAsNoTracking().FirstOrDefault(x => x.SalespersonNumber.Equals(order.Salesperson, StringComparison.InvariantCultureIgnoreCase))?.Email ?? string.Empty;

            if (string.IsNullOrEmpty(salesPersonEmail) || !RegularExpressionLibrary.IsValidEmail(salesPersonEmail))
            {
                return null;
            }

            emailList.Add(salesPersonEmail);
            return emailList;
        }

        private List<string> GetConfirmationEmailBccAddresses(OrderHistory order)
        {
            if (!string.IsNullOrEmpty(_emailsSettings.TestEmail))
            {
                return null;
            }

            var bccAddresses = GetOrderNotificationEmail() ?? new List<string>();

            var warehouseName = order.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Order.Warehouse)?.Value ?? string.Empty;
            if (!string.IsNullOrEmpty(warehouseName))
            {
                LogHelper.For(this).Debug($"Warehouse name : {warehouseName}");

                var warehouse = _warehouseRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Name == warehouseName);
                if (warehouse != null)
                {
                    var warehouseEmails = warehouse.CustomProperties.FirstOrDefault(x => x.Name == Constants.CustomProperties.Warehouse.Email)?.Value ?? string.Empty;
                    LogHelper.For(this).Debug($"Warehouse emails : {warehouseEmails}");

                    foreach (var email in warehouseEmails.Split(',', ';').Select(x => x.Trim()))
                    {
                        if (!string.IsNullOrEmpty(email) && RegularExpressionLibrary.IsValidEmail(email))
                        {
                            LogHelper.For(this).Debug("Warehouse email is valid");
                            bccAddresses.Add(email);
                        }
                        else
                        {
                            LogHelper.For(this).Debug($"Warehouse email is invalid or empty : {email}");
                        }
                    }
                }
            }

            LogHelper.For(this).Debug($"Bcc addresses : {string.Join(",", bccAddresses)}");
            return bccAddresses;
        }

        private ExpandoObject GetOrderLine(OrderHistoryLine orderHistoryLine, CultureInfo culture, bool isSouthCarolinaSolidWasteTaxable)
        {
            dynamic orderLine = new ExpandoObject();

            orderLine.ProductName = orderHistoryLine.ProductErpNumber;
            orderLine.Description = orderHistoryLine.Description;
            orderLine.QtyOrdered = orderHistoryLine.QtyOrdered;
            orderLine.QtyShipped = orderHistoryLine.QtyShipped;
            orderLine.QtyOrderedDisplay = orderLine.QtyOrdered.ToString("0.##", culture);
            orderLine.QtyShippedDisplay = orderLine.QtyShipped.ToString("0.##", culture);
            orderLine.UnitNetPriceDisplay = orderHistoryLine.UnitNetPrice.ToString("C", culture);
            orderLine.ExtendedUnitNetPrice = orderHistoryLine.QtyOrdered * orderHistoryLine.UnitNetPrice;
            orderLine.ExtendedUnitNetPriceDisplay = orderLine.ExtendedUnitNetPrice.ToString("C", culture);
            orderLine.IsSouthCarolinaSolidWasteTaxable = isSouthCarolinaSolidWasteTaxable;
            orderLine.HasBackOrder = orderLine.QtyOrdered > orderLine.QtyShipped;

            return orderLine;
        }

        private List<string> GetOrderNotificationEmail()
        {
            return _emailsSettings.OrderNotificationEmail.Split(',', ';').Select(x => x.Trim()).ToList();
        }

        private ExpandoObject GetOrderTaxes(OrderHistoryTax orderHistoryTax, CultureInfo culture, Language language)
        {
            dynamic tax = new ExpandoObject();

            tax.TaxDescription = _translationLocalizer.Translate(language.Id, TranslationDictionarySource.Label, orderHistoryTax.TaxDescription);
            tax.TaxAmountDisplay = orderHistoryTax.TaxAmount.ToString("C", culture);

            return tax;
        }

        private void SendConfirmationEmail(OrderHistory order, CustomerOrder customerOrder, string reference)
        {
            try
            {
                if (order != null && customerOrder != null)
                {
                    var southCarolinaSolidWasteTaxableProducts = _customerOrderUtilities.GetStateTaxRate(customerOrder)?.Id == Constants.Ids.States.SouthCarolina ? customerOrder.OrderLines.Where(x => ParseHelper.ParseBoolean(x.Product?.GetProperty(Constants.CustomProperties.Product.IsSouthCarolinaSolidWasteTaxable, false.ToString()), false)).Select(x => x.Product.ErpNumber).ToList() : new List<string>();

                    var language = customerOrder.PlacedByUserProfile?.Language ?? _languageRepository.GetDefault();
                    var viewModel = BuildConfirmationEmailViewModel(order, language, southCarolinaSolidWasteTaxableProducts);
                    var emailList = _emailListRepository.GetOrCreateByName("OrderConfirmation", "Order Confirmation");
                    var content = _contentManagerUtilities.CurrentContent(_emailTemplateUtilities.GetOrCreateByName(emailList.EmailTemplate.Name, customerOrder.WebsiteId).ContentManager).Html;

                    if (content != null)
                    {
                        _emailsSettings.OverrideCurrentWebsite(customerOrder.WebsiteId);

                        _emailService.SendEmail(
                            new SendEmailParameter
                            {
                                Body = _emailService.ParseTemplate(content, viewModel),
                                FromAddress = !string.IsNullOrEmpty(emailList.FromAddress) ? emailList.FromAddress : _emailsSettings.DefaultEmail,
                                Subject = $"{_entityTranslationService.TranslateProperty(emailList, p => p.Subject, language)} - {reference}",
                                ToAddresses = _buildEmailValues.BuildOrderConfirmationEmailToList(customerOrder.Id),
                                BccAddresses = GetConfirmationEmailBccAddresses(order) ?? new List<string>(),
                                CCAddresses = GetConfirmationEmailCcAddresses(order) ?? new List<string>()
                            },
                            _unitOfWork);
                    }
                }
                else
                {
                    throw new Exception($"Order and Customer order not found for erp Order Number {reference}");
                }
            }
            catch (Exception exception)
            {
                LogHelper.For(this).Error($"Could not send order confirmation email : {exception.Message}", exception);
            }
        }
    }
}
