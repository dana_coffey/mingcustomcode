﻿using System;
using System.Linq;
using System.Web;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common.Extensions;
using Insite.Core.Context.Services;
using Insite.Core.Context.Services.Parameters;
using Insite.Core.Interfaces.Data;
using Insite.Core.Security;
using Insite.Data.Entities.Dtos;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Services
{
    public class CollectiveContextWarehouseService : IWarehouseService
    {
        private readonly ICollectiveLocationService _collectiveLocationService;
        private readonly ICollectiveWarehouseService _collectiveWarehouseService;
        private readonly HttpContextBase _httpContext;
        private readonly IUserProfileService _userProfileService;
        private readonly IWebsiteService _websiteService;

        public CollectiveContextWarehouseService(ICollectiveWarehouseService collectiveWarehouseService, ICollectiveLocationService collectiveLocationService, HttpContextBase httpContext, IWebsiteService websiteService, IUserProfileService userProfileService)
        {
            _collectiveWarehouseService = collectiveWarehouseService;
            _collectiveLocationService = collectiveLocationService;
            _httpContext = httpContext;
            _websiteService = websiteService;
            _userProfileService = userProfileService;
        }

        public WarehouseDto GetWarehouse(GetWarehouseParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            if (parameter.UnitOfWork == null)
            {
                throw new ArgumentNullException(nameof(parameter.UnitOfWork));
            }

            WarehouseDto warehouse = null;
            var warehouseRepository = parameter.UnitOfWork.GetTypedRepository<IWarehouseRepository>();
            var cachedWarehouses = warehouseRepository.GetCachedWarehouses();

            try
            {
                var location = _collectiveLocationService.Get(GetCurrentUserProfileId(parameter.UnitOfWork), parameter.ShipTo, createLocation: false);
                if (location?.Warehouse != null)
                {
                    warehouse = cachedWarehouses.First(x => x.Id == Guid.Parse(location.Warehouse.Id));
                }
                else
                {
                    if (parameter.ShipTo != null && parameter.ShipTo.CustomerNumber != "ABANDONED CART" && parameter.ShipTo.DefaultWarehouse != null)
                    {
                        warehouse = cachedWarehouses.FirstOrDefault(x => x.Id == Guid.Parse(_collectiveWarehouseService.GetCustomerWarehouse(parameter.ShipTo).Id));
                    }
                }
            }
            catch (Exception)
            {
                warehouse = warehouseRepository.GetCachedDefault() ?? warehouseRepository.GetCachedWarehouses().FirstOrDefault();
            }

            return warehouse;
        }

        private Guid? GetCurrentUserProfileId(IUnitOfWork unitOfWork)
        {
            var website = _websiteService.GetWebsite(new GetWebsiteParameter(unitOfWork, _httpContext.GetDomainPageUrl()));
            var userName = _httpContext.User?.Identity?.Name;

            return string.IsNullOrEmpty(userName) || AdminUserNameHelper.IsAdminUser(userName) ? null : _userProfileService.GetUserProfile(new GetUserProfileParameter(unitOfWork, website.WebsiteDto, userName))?.Id;
        }
    }
}
