﻿using System;
using System.Data.Entity;
using System.Linq;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Product.PartsList;
using Collective.Web.Extension.Helpers;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Core.Plugins.Catalog;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Services
{
    public class CollectivePartService : ICollectivePartService, ICollectiveInjectableClass
    {
        private readonly ICatalogPathBuilder _catalogPathBuilder;
        private readonly IRepository<Product> _productRepository;

        public CollectivePartService()
        {
            _catalogPathBuilder = DependencyLocator.Current.GetInstance<ICatalogPathBuilder>();

            var unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _productRepository = unitOfWork.GetRepository<Product>();
        }

        public PartsListModel GetPartsList(Guid productId)
        {
            var product = _productRepository.GetTableAsNoTracking().Include(x => x.CustomProperties).FirstOrDefault(x => x.Id == productId);

            if (product != null)
            {
                if (!ProductHelper.IsProductPartsListCompatible(product))
                {
                    return null;
                }

                var epicApiService = CarrierApiHelper.GetEpicApiService();

                try
                {
                    var productNumber = !string.IsNullOrEmpty(product.ManufacturerItem) ? product.ManufacturerItem.Replace('-', '.') : product.ErpNumber.Replace('-', '.');
                    var response = epicApiService.GetParts(productNumber);
                    var partsListNumber = response.Select(x =>
                    {
                        var partNumber = x.PartNumber.Replace("·", string.Empty);
                        return partNumber.IndexOf(':') == -1 ? partNumber : partNumber.Replace(partNumber.Substring(0, partNumber.IndexOf(':') + 1), string.Empty);
                    }).Distinct().ToList();
                    var productsUrls = _productRepository.GetTableAsNoTracking().Where(x => partsListNumber.Contains(x.ManufacturerItem) || partsListNumber.Contains(x.ErpNumber)).ToList();

                    var partsListData = response.SelectMany(x => x.PackageNumber.Split(' ')).Distinct().Select(
                        packageNumber => new PackageModel
                        {
                            Number = packageNumber.IndexOf(':') == -1 ? packageNumber : packageNumber.Replace(packageNumber.Substring(0, packageNumber.IndexOf(':') + 1), string.Empty),
                            Groups = response.Where(x => x.PackageNumber.Contains(packageNumber)).GroupBy(x => x.Group).Select(
                                x => new GroupModel
                                {
                                    Name = x.Key,
                                    Parts = x.Select(
                                        part =>
                                        {
                                            var partNumber = part.PartNumber.Replace("·", string.Empty);
                                            partNumber = partNumber.IndexOf(':') == -1 ? partNumber : partNumber.Replace(partNumber.Substring(0, partNumber.IndexOf(':') + 1), string.Empty);

                                            var productUrl = productsUrls.FirstOrDefault(z => z.ManufacturerItem.Equals(partNumber, StringComparison.InvariantCultureIgnoreCase) || z.ErpNumber.Equals(partNumber, StringComparison.InvariantCultureIgnoreCase));
                                            return new PartModel
                                            {
                                                Number = partNumber,
                                                Name = StringHelper.NewlineToBr(part.Description.Replace("►", "<span></span>").Trim()),
                                                Quantity = part.Quantity,
                                                Url = productUrl?.UrlSegment != null ? _catalogPathBuilder.MakeCanonicalProductUrlPath(productUrl.UrlSegment) : string.Empty,
                                                HasHistory = part.HasHistory
                                            };
                                        }).ToList()
                                }).ToList()
                        }).ToList();
                    return partsListData.Any() ? new PartsListModel { Packages = partsListData } : null;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return null;
        }
    }
}
