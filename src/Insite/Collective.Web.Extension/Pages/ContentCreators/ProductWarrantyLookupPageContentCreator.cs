﻿using System;
using Collective.Web.Extension.Widgets;
using Insite.ContentLibrary.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.WebFramework.Content;

namespace Collective.Web.Extension.Pages.ContentCreators
{
    public class ProductWarrantyLookupContentCreator : AbstractContentCreator<ProductWarrantyLookupPage>
    {
        protected override ProductWarrantyLookupPage Create()
        {
            var now = DateTimeOffset.Now;
            var page = InitializePageWithParentType<ProductWarrantyLookupPage>(typeof(HomePage));
            page.Name = "Product Warranty Lookup";
            page.Title = "Product Warranty";
            page.Url = "/product-warranty-lookup";
            page.ExcludeFromNavigation = false;

            SaveItem(page, now);
            SaveItem(InitializeWidget<PageTitle>("Content", page), now);
            SaveItem(InitializeWidget<ProductWarrantyLookup>("Content", page), now);

            var breadcrumb = InitializeWidget<Breadcrumb>("Content", page);
            breadcrumb.SortOrder = -100;
            breadcrumb.TemplateView = "Store/Standard";
            SaveItem(breadcrumb, DateTimeOffset.Now);

            return page;
        }
    }
}
