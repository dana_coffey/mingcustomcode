﻿using System;
using Collective.Web.Extension.Widgets;
using Insite.Cart.Content;
using Insite.WebFramework.Content;

namespace Collective.Web.Extension.Pages.ContentCreators
{
    public class CheckoutOrderConfirmationPageContentCreator : AbstractContentCreator<CheckoutOrderConfirmationPage>
    {
        protected override CheckoutOrderConfirmationPage Create()
        {
            var now = DateTimeOffset.Now;
            var page = this.InitializePageWithParentType<CheckoutOrderConfirmationPage>(typeof(CartPage));
            page.Name = "Checkout - Order Confirmation";
            page.Title = "Checkout - Order Confirmation";
            page.Url = "/checkout-order-confirmation";
            page.ExcludeFromNavigation = true;

            this.SaveItem(page, now);
            this.SaveItem(this.InitializeWidget<CheckoutOrderConfirmation>("Content", page), now);

            return page;
        }
    }
}
