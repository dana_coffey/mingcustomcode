﻿using System;
using Collective.Web.Extension.Widgets;
using Insite.Cart.Content;
using Insite.WebFramework.Content;

namespace Collective.Web.Extension.Pages.ContentCreators
{
    public class CheckoutOrderReviewPageContentCreator : AbstractContentCreator<CheckoutOrderReviewPage>
    {
        protected override CheckoutOrderReviewPage Create()
        {
            var now = DateTimeOffset.Now;
            var page = this.InitializePageWithParentType<CheckoutOrderReviewPage>(typeof(CartPage));
            page.Name = "Checkout - Order Review";
            page.Title = "Checkout - Order Review";
            page.Url = "/checkout-order-review";
            page.ExcludeFromNavigation = true;

            this.SaveItem(page, now);
            this.SaveItem(this.InitializeWidget<CheckoutOrderReview>("Content", page), now);

            return page;
        }
    }
}
