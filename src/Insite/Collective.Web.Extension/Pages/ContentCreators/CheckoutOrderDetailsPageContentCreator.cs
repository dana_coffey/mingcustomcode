﻿using System;
using Collective.Web.Extension.Widgets;
using Insite.Cart.Content;
using Insite.WebFramework.Content;

namespace Collective.Web.Extension.Pages.ContentCreators
{
    public class CheckoutOrderDetailsPageContentCreator : AbstractContentCreator<CheckoutOrderDetailsPage>
    {
        protected override CheckoutOrderDetailsPage Create()
        {
            var now = DateTimeOffset.Now;
            var page = this.InitializePageWithParentType<CheckoutOrderDetailsPage>(typeof(CartPage));
            page.Name = "Checkout - Order Details";
            page.Title = "Checkout - Order Details";
            page.Url = "/checkout-order-details";
            page.ExcludeFromNavigation = true;

            this.SaveItem(page, now);
            this.SaveItem(this.InitializeWidget<CheckoutOrderDetails>("Content", page), now);

            return page;
        }
    }
}
