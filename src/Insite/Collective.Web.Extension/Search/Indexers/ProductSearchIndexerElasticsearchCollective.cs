﻿using System;
using Collective.Web.Extension.Search.Models;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Search.Elasticsearch;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index;
using Insite.Search.Elasticsearch.SystemSettings;

namespace Collective.Web.Extension.Search.Indexers
{
    [DependencySystemSetting("SearchGeneral", "SearchIndexerName", "Collective")]
    public class ProductSearchIndexerElasticsearchCollective : ProductSearchIndexerElasticsearch
    {
        public ProductSearchIndexerElasticsearchCollective(IElasticsearchIndex index, IUnitOfWorkFactory unitOfWorkFactory, IIndexableProductRepository indexableProductRepository, SearchIndexSettings searchIndexSettings) : base(index, unitOfWorkFactory, indexableProductRepository, searchIndexSettings)
        {
        }

        public override void BuildProductSearchIndex(string indexName = null, bool incremental = false, Action<string> logMessage = null)
        {
            indexName = indexName ?? Index.Alias;
            try
            {
                IndexProducts<IndexableProductCollective, ElasticsearchProductCollective>(indexName, logMessage, incremental);
            }
            catch (Exception ex)
            {
                logMessage?.Invoke($"Product search indexer exception: {ex.Message} {ex.StackTrace}");
                throw;
            }
        }
    }
}
