﻿using Collective.Web.Extension.Search.Models;
using Insite.Core.SystemSetting.Groups.Search;
using Insite.Search.Elasticsearch;
using Insite.Search.Elasticsearch.DocumentTypes.Product;

namespace Collective.Web.Extension.Search.Factories
{
    public class ElasticsearchProductFactoryCollective : ElasticsearchProductFactoryBase<IndexableProductCollective, ElasticsearchProductCollective>
    {
        public ElasticsearchProductFactoryCollective(IBoostHelper boostHelper, SearchGeneralSettings searchGeneralSettings) : base(boostHelper, searchGeneralSettings)
        {
        }

        protected override ElasticsearchProductCollective MapAdditionalProperties(IndexableProductCollective indexableProduct, ElasticsearchProductCollective elasticsearchProduct)
        {
            elasticsearchProduct.Status = indexableProduct.Status;
            return elasticsearchProduct;
        }
    }
}
