﻿using System.Collections.Generic;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Core.Interfaces.Plugins.Catalog;
using Insite.Core.Plugins.Search.Enums;
using Insite.Core.SystemSetting.Groups.Catalog;
using Insite.Core.SystemSetting.Groups.Search;
using Insite.Core.SystemSetting.Groups.SystemSettings;
using Insite.Search.Elasticsearch;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Query;
using Insite.Search.Elasticsearch.SystemSettings;
using Nest;

namespace Collective.Web.Extension.Search.Providers
{
    [DependencySystemSetting("SearchGeneral", "SearchProviderName", "Collective")]
    public class ProductSearchProviderElasticsearchCollective : ProductSearchProviderElasticsearch
    {
        public ProductSearchProviderElasticsearchCollective(IElasticsearchIndex index, ICacheManager cacheManager, ICatalogCacheKeyProvider catalogCacheKeyProvider, IPerRequestCacheManager perRequestCacheManager, IUnitOfWorkFactory unitOfWorkFactory, IProductSearchFacetProcessor facetProcessor, IElasticsearchQueryBuilder queryBuilder, IPhraseSuggestConfiguration phraseSuggestConfiguration, IBoostHelper boostHelper, SearchGeneralSettings searchGeneralSettings, PricingSettings pricingSettings, ProductListSettings productListSettings, ProductRestrictionsSettings productRestrictionsSettings, SearchSuggestionsSettings searchSuggestionsSettings, FuzzySearchSettings fuzzySearchSettings, SponsoredSearchSettings sponsoredSearchSettings, CatalogGeneralSettings catalogGeneralSettings, SecuritySettings securitySettings) : base(index, cacheManager, catalogCacheKeyProvider, perRequestCacheManager, unitOfWorkFactory, facetProcessor, queryBuilder, phraseSuggestConfiguration, boostHelper, searchGeneralSettings, pricingSettings, productListSettings, productRestrictionsSettings, searchSuggestionsSettings, fuzzySearchSettings, sponsoredSearchSettings, catalogGeneralSettings, securitySettings)
        {
        }

        protected override QueryContainer GenerateQueries(string searchCriteria, string searchWithinCriteria, bool autoComplete)
        {
            var baseQuery = base.GenerateQueries(searchCriteria, searchWithinCriteria, autoComplete);

            var statusFieldName = "status";
            var supersedeClauses = new List<QueryContainer>
            {
                QueryBuilder.MakeFieldQuery(statusFieldName, Constants.CustomProperties.Product.Status.None),
                QueryBuilder.MakeFieldQuery(statusFieldName, StringHelper.RemoveWhitespaces(Constants.CustomProperties.Product.Status.SupersededWithInventory)),
            };

            if (!string.IsNullOrEmpty(searchCriteria))
            {
                supersedeClauses.Add(QueryBuilder.MakeFieldQuery("erpNumber", searchCriteria));
            }

            return QueryBuilder.MakeBooleanQuery(new List<QueryContainer>
            {
                baseQuery,
                QueryBuilder.MakeBooleanQuery(supersedeClauses, Operation.Or)
            }, Operation.And);
        }
    }
}
