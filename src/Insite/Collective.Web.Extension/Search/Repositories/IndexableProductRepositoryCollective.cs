﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Web.Extension.Search.Models;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Search.Elasticsearch.DocumentTypes.Product.Index;

namespace Collective.Web.Extension.Search.Repositories
{
    public class IndexableProductRepositoryCollective : IndexableProductRepository
    {
        public IndexableProductRepositoryCollective(IUnitOfWorkFactory unitOfWorkFactory) : base(unitOfWorkFactory)
        {
        }

        public override IEnumerable<T> GetProducts<T>(bool incremental)
        {
            var products = base.GetProducts<IndexableProductCollective>(incremental).ToList();
            var customProperties = UnitOfWork.GetRepository<CustomProperty>().GetTableAsNoTracking().Where(p => p.Name == Constants.CustomProperties.Product.StatusField && p.Value != Constants.CustomProperties.Product.Status.None);

            foreach (var product in products)
            {
                var customProperty = customProperties.FirstOrDefault(p => p.ParentId == product.ProductId)?.Value;
                product.Status = !string.IsNullOrEmpty(customProperty) ? StringHelper.RemoveWhitespaces(customProperty) : Constants.CustomProperties.Product.Status.None;
            }

            return products as IEnumerable<T>;
        }
    }
}
