﻿using Insite.Search.Elasticsearch.DocumentTypes.Product;

namespace Collective.Web.Extension.Search.Models
{
    public class ElasticsearchProductCollective : ElasticsearchProduct
    {
        public string Status { get; set; }
    }
}
