﻿using Insite.Search.Elasticsearch.DocumentTypes.Product.Index;

namespace Collective.Web.Extension.Search.Models
{
    public class IndexableProductCollective : IndexableProduct
    {
        public string Status { get; set; }
    }
}
