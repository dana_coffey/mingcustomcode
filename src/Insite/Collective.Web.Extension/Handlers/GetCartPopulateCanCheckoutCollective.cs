﻿using Collective.Core.Constant;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Common.Providers;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;
using Insite.Core.SystemSetting.Groups.OrderManagement;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(GetCartPopulateCanCheckOutCollective))]
    public class GetCartPopulateCanCheckOutCollective : HandlerBase<GetCartParameter, GetCartResult>
    {
        private readonly CartSettings _cartSettings;

        public GetCartPopulateCanCheckOutCollective(CartSettings cartSettings)
        {
            _cartSettings = cartSettings;
        }

        public override int Order => 1300;

        public override GetCartResult Execute(IUnitOfWork unitOfWork, GetCartParameter parameter, GetCartResult result)
        {
            /* Taken from v4.4 Insite.Cart.Services.Handlers.GetCartHandler.PopulateCanCheckOut */

            result.CanCheckOut = true;

            if (!parameter.GetCartLineResults && result.Cart.OrderLines.Count > result.LineCount)
            {
                result.CanCheckOut = false;
            }

            if (result.CanRequisition)
            {
                result.CanCheckOut = false;
            }

            if (!result.Cart.LastPricingOn.HasValue || result.Cart.LastPricingOn.Value < DateTimeProvider.Current.Now.AddMinutes(-_cartSettings.MinutesBeforeRecalculation))
            {
                result.CanCheckOut = false;
            }

            if (result.UserRoles != null && result.CanCheckOut)
            {
                if (result.UserRoles.Contains(Constants.Roles.Technician))
                {
                    result.CanCheckOut = false;
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
