﻿using System.Collections.Generic;
using System.Web;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Inventory;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.Services.Handlers;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartRemoveRestrictedItemsHandlerCollective")]
    public class UpdateCartRemoveRestrictedItemsHandlerCollective : HandlerBase<UpdateCartParameter, UpdateCartResult>
    {
        private readonly ICollectiveInventoryService _collectiveInventoryService;
        private readonly ICustomerOrderUtilities _customerOrderUtilities;

        public override int Order => 550;

        public UpdateCartRemoveRestrictedItemsHandlerCollective(ICustomerOrderUtilities customerOrderUtilities, ICollectiveInventoryService collectiveInventoryService)
        {
            _customerOrderUtilities = customerOrderUtilities;
            _collectiveInventoryService = collectiveInventoryService;
        }

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (ParseHelper.ParseBoolean(HttpContext.Current.Request.QueryString.Get("removeRestrictedItems")).GetValueOrDefault(false))
            {
                var cart = result?.GetCartResult?.Cart;
                if (cart != null)
                {
                    var warehouseName = cart.GetProperty(Constants.CustomProperties.Cart.Warehouse, string.Empty);

                    var cartLinesToRemove = new List<OrderLine>();
                    foreach (var cartLine in cart.OrderLines)
                    {
                        var isCatalogProduct = ParseHelper.ParseBoolean(cartLine.Product.GetProperty(Constants.CustomProperties.Product.IsCatalogProduct, false.ToString()), false);
                        var availabilityStatus = _collectiveInventoryService.GetProductInventoryStatusModel(cartLine.Product.ErpNumber, isCatalogProduct, warehouseName);

                        if (availabilityStatus.Status == AvailabilityStatusType.Restricted)
                        {
                            cartLinesToRemove.Add(cartLine);
                        }
                    }

                    foreach (var cartLineToRemove in cartLinesToRemove)
                    {
                        _customerOrderUtilities.RemoveOrderLine(cart, cartLineToRemove);
                    }
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
