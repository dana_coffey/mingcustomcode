﻿using System.Linq;
using Collective.Core.Constant;
using Insite.Account.Services.Parameters;
using Insite.Account.Services.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(GetSessionHandlerCollective))]
    public class GetSessionHandlerCollective : HandlerBase<GetSessionParameter, GetSessionResult>
    {
        private readonly ITranslationLocalizer _translationLocalizer;
        public override int Order => 600;

        public GetSessionHandlerCollective(ITranslationLocalizer translationLocalizer)
        {
            _translationLocalizer = translationLocalizer;
        }

        public override GetSessionResult Execute(IUnitOfWork unitOfWork, GetSessionParameter parameter, GetSessionResult result)
        {
            if (!string.IsNullOrEmpty(result.UserRoles))
            {
                result.Properties.Add(Constants.HandlerResultProperties.Session.UserRolesDisplay, string.Join(", ", result.UserRoles.Split(',').Select(x => _translationLocalizer.TranslateLabel(x.Trim()))));
            }

            var defaultCustomer = SiteContext.Current.UserProfile?.DefaultCustomer;
            if (defaultCustomer != null)
            {
                result.Properties.Add(Constants.HandlerResultProperties.Session.DefaultCustomerIsShipTo, defaultCustomer.IsShipTo.ToString());
                result.Properties.Add(Constants.HandlerResultProperties.Session.DefaultCustomerIsBillTo, defaultCustomer.IsBillTo.ToString());
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
