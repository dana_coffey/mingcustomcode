﻿using Collective.Core.Constant;
using Insite.Account.Services.Parameters;
using Insite.Account.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(AddAccountHandlerCollective))]
    public class AddAccountHandlerCollective : HandlerBase<AddAccountParameter, AddAccountResult>
    {
        public override int Order => 600;

        public override AddAccountResult Execute(IUnitOfWork unitOfWork, AddAccountParameter parameter, AddAccountResult result)
        {
            parameter.Properties.Remove(Constants.HandlerResultProperties.Account.IsDeactivated);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Account.AvailableRoles);
            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
