﻿using System;
using Collective.Core.Constant;
using Insite.Cart.Services.Handlers;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Plugins.EntityUtilities;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartSubmitOrderHistoryHandler")]
    public class UpdateCartSubmitOrderHistoryHandlerCollective : UpdateCartSubmitOrderHistoryHandler
    {
        public override int Order => 1350;

        public UpdateCartSubmitOrderHistoryHandlerCollective(Lazy<IProductUtilities> productUtilities, IOrderLineUtilities orderLineUtilities, ICustomerOrderUtilities customerOrderUtilities) : base(productUtilities, orderLineUtilities, customerOrderUtilities)
        {
            CustomerOrderCustomPropertiesToCopy.Add(Constants.CustomProperties.Cart.Warehouse);
            CustomerOrderCustomPropertiesToCopy.Add(Constants.CustomProperties.Cart.CustomerReference);
            CustomerOrderCustomPropertiesToCopy.Add(Constants.CustomProperties.Cart.Location);
        }
    }
}
