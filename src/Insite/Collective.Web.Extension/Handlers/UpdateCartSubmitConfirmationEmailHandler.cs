﻿using System;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Cart.Services.Handlers;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Interfaces.Plugins.Emails;
using Insite.Core.Plugins.Emails;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.Integration;
using Insite.Core.Translation;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartSubmitConfirmationEmailHandler")]
    public class UpdateCartSubmitConfirmationEmailHandlerCollective : UpdateCartSubmitConfirmationEmailHandler
    {
        public override int Order => 1520;

        private readonly OrderSubmitSettings _orderSubmitSettings;
        private readonly ICollectiveOrderConfirmationService _orderConfirmationService;
        public UpdateCartSubmitConfirmationEmailHandlerCollective(Lazy<IBuildEmailValues> buildEmailValues, Lazy<IEmailService> emailService, Lazy<ICurrencyFormatProvider> currencyFormatProvider, Lazy<IEntityTranslationService> entityTranslationService, Lazy<ITranslationLocalizer> translationLocalizer, IOrderLineUtilities orderLineUtilities, ICustomerOrderUtilities customerOrderUtilities, OrderSubmitSettings orderSubmitSettings, ICollectiveOrderConfirmationService orderConfirmationService)
            : base(buildEmailValues, emailService, currencyFormatProvider, entityTranslationService, translationLocalizer, orderLineUtilities, customerOrderUtilities)
        {
            _orderSubmitSettings = orderSubmitSettings;
            _orderConfirmationService = orderConfirmationService;
        }

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (!_orderSubmitSettings.ErpSubmitOrders)
            {
                var cart = result.GetCartResult.Cart;
                _orderConfirmationService.SendConfirmationEmailByWebOrderNumber(cart.OrderNumber);
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
