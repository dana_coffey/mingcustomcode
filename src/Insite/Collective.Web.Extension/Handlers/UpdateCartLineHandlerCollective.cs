﻿using Collective.Core.Constant;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartLineHandlerCollective")]
    public class UpdateCartLineHandlerCollective : HandlerBase<UpdateCartLineParameter, UpdateCartLineResult>
    {
        public override int Order => 450;

        public override UpdateCartLineResult Execute(IUnitOfWork unitOfWork, UpdateCartLineParameter parameter, UpdateCartLineResult result)
        {
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.AvailabilityStatus);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.CanShowPrice);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.HasSouthCarolinaSolidWasteTax);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.SouthCarolinaSolidWasteTaxAmount);

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
