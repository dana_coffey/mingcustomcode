﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Services;
using Insite.Cart.Services.Handlers;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Common.Logging;
using Insite.Core.Context;
using Insite.Core.Enums;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Plugins.Integration;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Core.SystemSetting.Groups.Integration;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartSubmitOrderToErpHandler")]
    public class UpdateCartSubmitOrderToErpHandlerCollective : UpdateCartSubmitOrderToErpHandler
    {
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;

        public UpdateCartSubmitOrderToErpHandlerCollective(Lazy<IIntegrationJobSchedulingService> integrationJobSchedulingService, OrderSubmitSettings orderSubmitSettings, CustomerDefaultsSettings customerDefaultsSettings) : base(integrationJobSchedulingService, orderSubmitSettings)
        {
            _customerDefaultsSettings = customerDefaultsSettings;
        }

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (!parameter.Status.EqualsIgnoreCase("Submitted"))
            {
                return this.NextHandler.Execute(unitOfWork, parameter, result);
            }

            var cart = result.GetCartResult.Cart;
            try
            {
                if (OrderSubmitSettings.ErpSubmitOrders)
                {
                    var byStandardName = unitOfWork.GetTypedRepository<IJobDefinitionRepository>().GetByStandardName(JobDefinitionStandardJobName.OrderSubmit.ToString());
                    var customShipToPrefixParam = unitOfWork.GetRepository<JobDefinitionStepParameter>().GetTableAsNoTracking().FirstOrDefault(x => x.Id == Constants.Jobs.OrderSubmit.StepErpShipToPrefixId);

                    if (customShipToPrefixParam != null)
                    {
                        customShipToPrefixParam.Value = _customerDefaultsSettings.ErpShipToPrefix;
                    }

                    if (byStandardName == null)
                    {
                        throw new Exception($"Unable to find a JobDefinition for {JobDefinitionStandardJobName.OrderSubmit}.");
                    }

                    if (OrderSubmitSettings.UseRealTimeOrderSubmit)
                    {
                        unitOfWork.CommitTransaction();
                        IntegrationJobSchedulingService.Value.RunRealTimeIntegrationJob(byStandardName.Name, null, new Collection<JobDefinitionStepParameter> { customShipToPrefixParam }, cart.OrderNumber);
                        unitOfWork.BeginTransaction();
                        unitOfWork.Refresh(result.GetCartResult.Cart);
                    }
                    else
                    {
                        IntegrationJobSchedulingService.Value.ScheduleBatchIntegrationJob(byStandardName.Name, null, new Collection<JobDefinitionStepParameter> { customShipToPrefixParam }, cart.OrderNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.For(this).Error(ex.Message, ex, "UpdateCartSubmitOrderToErpHandler");
            }
            finally
            {
                if (!unitOfWork.DataProvider.TransactionActive)
                {
                    unitOfWork.BeginTransaction();
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
