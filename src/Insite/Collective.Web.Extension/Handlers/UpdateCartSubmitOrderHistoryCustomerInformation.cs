﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Models.Order;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Common.DynamicLinq;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;
using Insite.Data.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartSubmitOrderHistoryCustomerInformation")]
    public class UpdateCartSubmitOrderHistoryCustomerInformation : HandlerBase<UpdateCartParameter, UpdateCartResult>
    {
        public override int Order => 1510;

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            var customerOrder = result.GetCartResult.Cart;
            var orderHistory = unitOfWork.GetRepository<OrderHistory>().GetTable().FirstOrDefault(x => x.WebOrderNumber == customerOrder.OrderNumber);
            if (orderHistory != null)
            {
                orderHistory.SetProperty(Constants.CustomProperties.Order.SubAccount, JsonConvert.SerializeObject(MapCustomerToOrderSubAccountModel(customerOrder.ShipTo), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
                unitOfWork.Save();
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }

        private OrderSubAccountModel MapCustomerToOrderSubAccountModel(Customer customer)
        {
            return new OrderSubAccountModel
            {
                CompanyName = customer.CompanyName,
                Address1 = customer.Address1,
                Address2 = customer.Address2,
                Address3 = customer.Address3,
                Address4 = customer.Address4,
                City = customer.City,
                Country = customer.Country.Abbreviation,
                State = customer.State.Abbreviation,
                PostalCode = customer.PostalCode
            };
        }
    }
}
