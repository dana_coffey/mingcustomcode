﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Injector;
using Collective.InforSxe.Core.Interfaces;
using Insite.Catalog.Services.Dtos;
using Insite.Catalog.Services.Handlers;
using Insite.Catalog.Services.Handlers.Helpers;
using Insite.Catalog.Services.Parameters;
using Insite.Catalog.Services.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Plugins.Catalog;
using Insite.Core.Plugins.Search;
using Insite.Core.SystemSetting.Groups.Catalog;
using Insite.Core.SystemSetting.Groups.SiteConfigurations;
using Insite.Data.Entities;
using Insite.Data.Entities.Dtos.Interfaces;
using Insite.Data.Extensions;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(GetProductCollectionHandler))]
    public class GetProductCollectionHandlerCollective : GetProductCollectionHandler
    {
        private readonly ISxeApiHelper _sxeApiHelper;

        public GetProductCollectionHandlerCollective(Lazy<IProductSearchProvider> productSearchProvider, Lazy<ITranslationLocalizer> translationLocalizer, Lazy<IProductBatchLoader> productBatchLoader, Lazy<IGetProductHandlerHelper> getProductHandlerHelper, PricingSettings pricingSettings, StorefrontApiSettings storefrontApiSettings, ISxeApiHelper sxeApiHelper)
            : base(productSearchProvider, translationLocalizer, productBatchLoader, getProductHandlerHelper, pricingSettings, storefrontApiSettings)
        {
            _sxeApiHelper = sxeApiHelper;
        }

        protected override List<ProductDto> ConvertSearchResultsToProductDtos(IUnitOfWork unitOfWork, GetProductCollectionParameter parameter, IProductSearchResult searchResults, ICurrency currency)
        {
            if (SiteContext.Current.BillTo != null && SiteContext.Current.ShipTo != null && parameter.GetPrices)
            {
                _sxeApiHelper.GetPrices(Convert.ToInt32(SiteContext.Current.BillTo.ErpNumber), SiteContext.Current.ShipTo.ErpSequence, searchResults.Products.Select(p => p.ERPNumber).ToList(), SiteContext.Current.WarehouseDto?.Name);
            }

            return base.ConvertSearchResultsToProductDtos(unitOfWork, parameter, searchResults, currency);
        }

        protected override bool FindProductsWithLookup(IUnitOfWork unitOfWork, GetProductCollectionParameter parameter, GetProductCollectionResult result)
        {
            if (SiteContext.Current.BillTo != null && SiteContext.Current.ShipTo != null && parameter.GetPrices)
            {
                List<string> skuList = null;

                if (parameter.ProductIds?.Any() ?? false)
                {
                    var productRepository = unitOfWork.GetRepository<Product>();
                    skuList = productRepository.GetTableAsNoTracking().WhereContains(x => x.Id, parameter.ProductIds).Select(x => x.ErpNumber).ToList();
                }
                else if (parameter.ErpNumbers?.Any() ?? false)
                {
                    skuList = parameter.ErpNumbers.ToList();
                }

                if (skuList?.Any() ?? false)
                {
                    _sxeApiHelper.GetPrices(Convert.ToInt32(SiteContext.Current.BillTo.ErpNumber), SiteContext.Current.ShipTo.ErpSequence, skuList, SiteContext.Current.WarehouseDto?.Name);
                }
            }

            return base.FindProductsWithLookup(unitOfWork, parameter, result);
        }
    }
}
