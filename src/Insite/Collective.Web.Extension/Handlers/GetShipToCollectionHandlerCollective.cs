﻿using System.Linq;
using Collective.Core.Constant;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;
using Insite.Customers.Services.Parameters;
using Insite.Customers.Services.Results;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("GetShipToCollectionHandlerCollective")]
    public class GetShipToCollectionHandlerCollective : HandlerBase<GetShipToCollectionParameter, GetShipToCollectionResult>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IHandlerFactory _handlerFactory;

        public override int Order => 600;

        public GetShipToCollectionHandlerCollective(IUnitOfWorkFactory unitOfWorkFactory, IHandlerFactory handlerFactory)
        {
            _handlerFactory = handlerFactory;
            var unitOfWork = unitOfWorkFactory.GetUnitOfWork();
            _customerRepository = unitOfWork.GetRepository<Customer>();
        }

        public override GetShipToCollectionResult Execute(IUnitOfWork unitOfWork, GetShipToCollectionParameter parameter, GetShipToCollectionResult result)
        {
            var settings = _handlerFactory.GetHandler<IHandler<GetSettingsParameter, GetCustomerSettingsResult>>().Execute(unitOfWork, new GetSettingsParameter(), new GetCustomerSettingsResult());
            if (settings.AllowCreateNewShipToAddress)
            {
                result.Properties.Add(Constants.HandlerResultProperties.ShipToCollection.HasShipTos, "1");
                return NextHandler.Execute(unitOfWork, parameter, result);
            }

            if (parameter.BillToId.HasValue)
            {
                var billTo = _customerRepository.GetTableAsNoTracking().FirstOrDefault(p => p.Id == parameter.BillToId.Value);
                if (billTo != null && billTo.IsBillTo && billTo.IsShipTo)
                {
                    result.Properties.Add(Constants.HandlerResultProperties.ShipToCollection.HasShipTos, "1");
                    return NextHandler.Execute(unitOfWork, parameter, result);
                }

                var shipTos = _customerRepository.GetTableAsNoTracking().Count(p => p.ParentId == parameter.BillToId);
                result.Properties.Add(Constants.HandlerResultProperties.ShipToCollection.HasShipTos, shipTos > 0 ? "1" : "0");
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
