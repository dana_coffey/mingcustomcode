﻿using Insite.Account.Services.Parameters;
using Insite.Account.Services.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.Providers;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(AddSessionHandlerCollective))]
    public class AddSessionHandlerCollective : HandlerBase<AddSessionParameter, AddSessionResult>
    {
        private readonly IAuthenticationService _authenticationService;

        public override int Order => 550;

        public AddSessionHandlerCollective(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public override AddSessionResult Execute(IUnitOfWork unitOfWork, AddSessionParameter parameter, AddSessionResult result)
        {
            if (SiteContext.Current.ShipTo == null)
            {
                _authenticationService.SignOut();
                return CreateErrorServiceResult(result, SubCode.AccountServiceContactCustomerSupport, MessageProvider.Current.Contact_Customer_Support);
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
