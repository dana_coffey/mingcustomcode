﻿using System;
using Collective.Core.Constant;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(UpdateCartHandlerCollective))]
    public class UpdateCartHandlerCollective : HandlerBase<UpdateCartParameter, UpdateCartResult>
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ITranslationLocalizer _translationLocalizer;

        public override int Order => 450;

        public UpdateCartHandlerCollective(IAuthenticationService authenticationService, ITranslationLocalizer translationLocalizer)
        {
            _authenticationService = authenticationService;
            _translationLocalizer = translationLocalizer;
        }

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (SiteContext.Current.UserProfile != null && parameter.Status.EqualsIgnoreCase("Submitted"))
            {
                if (_authenticationService.IsUserInRole(SiteContext.Current.UserProfile.UserName, Constants.Roles.Technician))
                {
                    return CreateErrorServiceResult(result, SubCode.CartServiceCantBeSubmitted, _translationLocalizer.TranslateLabel(Constants.Roles.Technician + "_Error"));
                }
            }

            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.CanShowPrice);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.CartHasRestrictedProducts);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.CartId);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.ErpOrderNumber);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.IsTaxExempt);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.NoTaxCalculation);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.ShipViaIsPickup);

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
