﻿using Collective.Core.Constant;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("AddCartLineHandlerCollective")]
    public class AddCartLineHandlerCollective : HandlerBase<AddCartLineParameter, AddCartLineResult>
    {
        public override int Order => 450;

        public override AddCartLineResult Execute(IUnitOfWork unitOfWork, AddCartLineParameter parameter, AddCartLineResult result)
        {
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.AvailabilityStatus);
            parameter.Properties.Remove(Constants.HandlerResultProperties.Cart.CanShowPrice);

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
