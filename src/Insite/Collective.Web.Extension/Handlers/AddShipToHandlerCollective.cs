﻿using System;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Providers;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;
using Insite.Customers.Services.Handlers;
using Insite.Customers.Services.Handlers.Helpers;
using Insite.Customers.Services.Parameters;
using Insite.Customers.Services.Results;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("AddShipToHandler")]
    public class AddShipToHandlerCollective : AddShipToHandler
    {
        public AddShipToHandlerCollective(IHandlerFactory handlerFactory, ICustomerHelper customerHelper) : base(handlerFactory, customerHelper)
        {
        }

        public override AddShipToResult Execute(IUnitOfWork unitOfWork, AddShipToParameter parameter, AddShipToResult result)
        {
            var userAndBillTo = CustomerHelper.GetUserAndBillTo(unitOfWork, result, parameter.BillToId);
            if (userAndBillTo.ErrorResult != null)
            {
                return CreateErrorServiceResult(result, userAndBillTo.ErrorResult.SubCode, userAndBillTo.ErrorResult.Message);
            }

            if (!parameter.Phone.IsBlank() && !RegularExpressionLibrary.IsValidPhone(parameter.Phone))
            {
                return CreateErrorServiceResult(result, SubCode.BadRequest, MessageProvider.Current.AddressInfo_Phone_Validation);
            }

            if (!parameter.Email.IsBlank() && !RegularExpressionLibrary.IsValidEmail(parameter.Email))
            {
                return CreateErrorServiceResult(result, SubCode.BadRequest, MessageProvider.Current.AddressInfo_EmailAddress_Validation);
            }

            var customerRepository = unitOfWork.GetRepository<Customer>();
            var billTo = userAndBillTo.BillTo;
            var customer = customerRepository.Create();

            customer.Email = billTo.Email;
            customer.IsGuest = billTo.IsGuest;
            CustomerHelper.SetShipToInitialValues(billTo, customer);
            customer.FirstName = parameter.FirstName ?? string.Empty;
            customer.LastName = parameter.LastName ?? string.Empty;
            customer.CompanyName = parameter.CompanyName ?? string.Empty;
            customer.Attention = parameter.Attention ?? string.Empty;
            customer.Address1 = parameter.Address1 ?? string.Empty;
            customer.Address2 = parameter.Address2 ?? string.Empty;
            customer.Address3 = parameter.Address3 ?? string.Empty;
            customer.Address4 = parameter.Address4 ?? string.Empty;
            customer.City = parameter.City ?? string.Empty;
            customer.PostalCode = parameter.PostalCode ?? string.Empty;
            customer.Phone = parameter.Phone ?? string.Empty;
            customer.Email = parameter.Email ?? string.Empty;
            customer.Fax = parameter.Fax ?? string.Empty;
            customer.Country = !Guid.TryParse(parameter.CountryId, out var countryId) || !(countryId != Guid.Empty) ? unitOfWork.GetTypedRepository<ICountryRepository>().GetDefaultCountry(SiteContext.Current.WebsiteDto.Id) : unitOfWork.GetTypedRepository<ICountryRepository>().Get(countryId);
            customer.State = !Guid.TryParse(parameter.StateId, out var stateId) || !(stateId != Guid.Empty) ? unitOfWork.GetTypedRepository<IStateRepository>().GetDefaultState(SiteContext.Current.WebsiteDto.Id) : unitOfWork.GetTypedRepository<IStateRepository>().Get(stateId);

            if (!CollectiveInjector.GetInstance<ICollectiveTaxingHelper>().IsCustomerTaxable(billTo.CustomerNumber, customer.State?.Abbreviation, countryId))
            {
                customer.TaxCode1 = unitOfWork.GetRepository<Website>().GetTableAsNoTracking().FirstOrDefault(x => x.Id == SiteContext.Current.WebsiteDto.Id)?.TaxFreeTaxCode;
            }

            unitOfWork.GetTypedRepository<ICustomerRepository>().AddShipTo(billTo, customer);

            if (unitOfWork.GetTypedRepository<ICustomerRepository>().GetAssignedShipTos(billTo, userAndBillTo.UserProfile.Id).Any())
            {
                customer.UserProfiles.Add(userAndBillTo.UserProfile);
            }

            unitOfWork.Save();

            result.Label = CustomerHelper.FormatAddress(customer);
            result.BillTo = userAndBillTo.BillTo;
            result.ShipTo = customer;

            var settings = HandlerFactory.GetHandler<IHandler<GetSettingsParameter, GetCustomerSettingsResult>>().Execute(unitOfWork, new GetSettingsParameter(), new GetCustomerSettingsResult());
            result.Validation = CustomerHelper.PopulateShipToValidation(unitOfWork, settings, userAndBillTo.BillTo, customer);

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
