﻿using Collective.Core.Constant;
using Insite.Account.Services.Parameters;
using Insite.Account.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    public class UpdateSessionHandlerCollective : HandlerBase<UpdateSessionParameter, UpdateSessionResult>
    {
        public override int Order => 600;

        public override UpdateSessionResult Execute(IUnitOfWork unitOfWork, UpdateSessionParameter parameter, UpdateSessionResult result)
        {
            result.Properties.Remove(Constants.HandlerResultProperties.Session.UserRolesDisplay);
            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
