﻿using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Helpers;
using Insite.Account.Services.Handlers.Helpers;
using Insite.Account.Services.Parameters;
using Insite.Account.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateAccountHandlerCollective")]
    public class UpdateAccountHandlerCollective : HandlerBase<UpdateAccountParameter, UpdateAccountResult>
    {
        private readonly IAccountHelper _accountHelper;
        private readonly IUnitOfWork _unitOfWork;
        public override int Order => 600;

        public UpdateAccountHandlerCollective(IUnitOfWorkFactory unitOfWorkFactory, IAccountHelper accountHelper)
        {
            _accountHelper = accountHelper;
            _unitOfWork = unitOfWorkFactory.GetUnitOfWork();
        }

        public override UpdateAccountResult Execute(IUnitOfWork unitOfWork, UpdateAccountParameter parameter, UpdateAccountResult result)
        {
            var userProfile = result.GetAccountResult.UserProfile;
            var roles = _accountHelper.GetRoles(userProfile).ToList();
            var availableRoles = RoleHelper.GetAvailableRoles();

            availableRoles.ForEach(p =>
            {
                if (roles.Contains(p))
                {
                    _accountHelper.RemoveRole(userProfile, p);
                }
            });
            _accountHelper.AddRole(userProfile, parameter.Role);

            if (parameter.Id.HasValue && parameter.Properties.ContainsKey(Constants.HandlerResultProperties.Account.IsDeactivated))
            {
                userProfile.IsDeactivated = parameter.Properties[Constants.HandlerResultProperties.Account.IsDeactivated] == "true";

                parameter.Properties.Remove(Constants.HandlerResultProperties.Account.IsDeactivated);
            }

            parameter.Properties.Remove(Constants.HandlerResultProperties.Account.AvailableRoles);
            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
