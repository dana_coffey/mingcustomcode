﻿using System;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;
using Insite.Core.SystemSetting.Groups.AccountManagement;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(UpdateCartSubmitResetSessionCustomerIfCustomAddressHandler))]
    public class UpdateCartSubmitResetSessionCustomerIfCustomAddressHandler : HandlerBase<UpdateCartParameter, UpdateCartResult>
    {
        private readonly CustomerDefaultsSettings _customerDefaultsSettings;
        private readonly ISiteContextService _siteContextService;

        public override int Order => 2000;

        public UpdateCartSubmitResetSessionCustomerIfCustomAddressHandler(CustomerDefaultsSettings customerDefaultsSettings, ISiteContextServiceFactory siteContextServiceFactory)
        {
            _customerDefaultsSettings = customerDefaultsSettings;
            _siteContextService = siteContextServiceFactory.GetSiteContextService();
        }

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (parameter.Status.EqualsIgnoreCase("Submitted"))
            {
                if (SiteContext.Current.ShipTo.CustomerSequence.StartsWithIgnoreCase(_customerDefaultsSettings.ErpShipToPrefix))
                {
                    _siteContextService.SetShipTo(null);
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
