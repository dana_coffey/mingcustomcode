﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common.DynamicLinq;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Providers;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;
using Insite.Customers.Services.Handlers;
using Insite.Customers.Services.Parameters;
using Insite.Customers.Services.Results;
using Insite.Data.Entities;
using Insite.Data.Extensions;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("GetBillToCollectionHandler")]
    public class GetBillToCollectionHandlerOverride : GetBillToCollectionHandler
    {
        private readonly ICollectiveLocationService _collectiveLocationService;

        public GetBillToCollectionHandlerOverride(IHandlerFactory handlerFactory, ICollectiveLocationService collectiveLocationService) : base(handlerFactory)
        {
            _collectiveLocationService = collectiveLocationService;
        }

        public override GetBillToCollectionResult Execute(IUnitOfWork unitOfWork, GetBillToCollectionParameter parameter, GetBillToCollectionResult result)
        {
            var userProfile = SiteContext.Current.UserProfile;
            if (userProfile == null)
            {
                return CreateErrorServiceResult(result, SubCode.Forbidden, MessageProvider.Current.Forbidden);
            }

            IQueryable<Customer> availableBillTos = unitOfWork.GetTypedRepository<ICustomerRepository>().GetAvailableBillTos(SiteContext.Current.WebsiteDto, userProfile.Id);
            if (!availableBillTos.Any())
            {
                return CreateErrorServiceResult(result, SubCode.NotFound, MessageProvider.Current.Customer_BillToNotFound);
            }

            if (parameter.GetState)
            {
                availableBillTos = availableBillTos.Expand((x => x.State));
            }

            if (parameter.GetCountry)
            {
                availableBillTos = availableBillTos.Expand((x => x.Country));
            }

            if (parameter.GetUserProfiles)
            {
                availableBillTos = availableBillTos.Expand((x => x.UserProfiles));
            }

            if (!parameter.Filter.IsBlank())
            {
                availableBillTos = availableBillTos.Where(x => x.CustomerNumber.Contains(parameter.Filter) ||
                    x.ErpNumber.Contains(parameter.Filter) ||
                    x.CompanyName.Contains(parameter.Filter) ||
                    x.FirstName.Contains(parameter.Filter) ||
                    x.LastName.Contains(parameter.Filter) ||
                    x.Address1.Contains(parameter.Filter) ||
                    x.Address2.Contains(parameter.Filter) ||
                    x.City.Contains(parameter.Filter) ||
                    x.PostalCode.Contains(parameter.Filter));
            }

            var sortedAvailableBillTos = GetSortedBillTos(userProfile, availableBillTos, parameter);

            if (parameter.PageSize.HasValue)
            {
                sortedAvailableBillTos = ApplyPaging(unitOfWork, parameter, result, sortedAvailableBillTos);
            }

            var customerSettingsResult = HandlerFactory.GetHandler<IHandler<GetSettingsParameter, GetCustomerSettingsResult>>().Execute(unitOfWork, new GetSettingsParameter(), new GetCustomerSettingsResult());
            var getBillToResultList = new List<GetBillToResult>();

            if (sortedAvailableBillTos != null)
            {
                foreach (var customer in sortedAvailableBillTos.ToList())
                {
                    var getBillToResult = HandlerFactory.GetHandler<IHandler<GetBillToParameter, GetBillToResult>>().Execute(unitOfWork, new GetBillToParameter
                    {
                        BillToId = customer.Id,
                        GetShipTos = parameter.GetShipTos,
                        GetValidation = parameter.GetValidation,
                        GetUserProfiles = false,
                        GetState = false,
                        GetCountry = false,
                        CustomerSettings = customerSettingsResult,
                        SkipUserBillToValidation = true
                    }, new GetBillToResult());

                    if (getBillToResult.ResultCode == ResultCode.Success)
                    {
                        if (userProfile.DefaultCustomer != null && userProfile.DefaultCustomer.IsActive)
                        {
                            var defaultCustomerId = userProfile.DefaultCustomer.ParentId ?? (userProfile.DefaultCustomer.IsShipTo ? userProfile.DefaultCustomer.Id : Guid.Empty);
                            getBillToResult.IsDefault = getBillToResult.BillTo.Id.Equals(defaultCustomerId);
                        }

                        getBillToResultList.Add(getBillToResult);
                    }
                    else
                    {
                        return CreateErrorServiceResult(result, getBillToResult.SubCode, getBillToResult.Message);
                    }
                }
            }

            result.GetBillToResults = getBillToResultList;
            return NextHandler.Execute(unitOfWork, parameter, result);
        }

        private IQueryable<Customer> GetSortedBillTos(UserProfile userProfile, IQueryable<Customer> availableBillTos, PagingParameterBase parameter)
        {
            Customer defaultBillTo = null;

            if (userProfile.DefaultCustomer != null)
            {
                defaultBillTo = userProfile.DefaultCustomer?.IsBillTo ?? false ? userProfile.DefaultCustomer : userProfile.DefaultCustomer.Parent;
            }

            var currentLocation = _collectiveLocationService.Get(userProfile.Id, SiteContext.Current?.ShipTo, false);
            if (defaultBillTo != null && (currentLocation?.InitialLocation ?? false))
            {
                return !parameter.Sort.IsBlank() ? availableBillTos.OrderByDescending(x => defaultBillTo.Id.Equals(x.Id)).ThenBy(_ => parameter.Sort) : availableBillTos.OrderByDescending(x => defaultBillTo.Id.Equals(x.Id)).ThenBy(x => x.CustomerNumber);
            }

            if (SiteContext.Current?.BillTo != null)
            {
                return !parameter.Sort.IsBlank() ? availableBillTos.OrderByDescending(x => SiteContext.Current.BillTo.Id.Equals(x.Id)).ThenBy(_ => parameter.Sort) : availableBillTos.OrderByDescending(x => SiteContext.Current.BillTo.Id.Equals(x.Id)).ThenBy(x => x.CustomerNumber);
            }

            return !parameter.Sort.IsBlank() ? availableBillTos.OrderBy(parameter.Sort) : availableBillTos.OrderBy(x => x.CustomerNumber);
        }
    }
}
