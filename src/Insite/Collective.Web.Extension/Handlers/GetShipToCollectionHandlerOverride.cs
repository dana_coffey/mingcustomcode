﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common.DynamicLinq;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Plugins.Security;
using Insite.Core.Plugins.Cart;
using Insite.Core.Services;
using Insite.Core.Services.Handlers;
using Insite.Customers.Services.Handlers;
using Insite.Customers.Services.Handlers.Helpers;
using Insite.Customers.Services.Parameters;
using Insite.Customers.Services.Results;
using Insite.Data.Entities;
using Insite.Data.Extensions;
using Insite.Data.Repositories.Interfaces;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("GetShipToCollectionHandler")]
    public class GetShipToCollectionHandlerOverride : GetShipToCollectionHandler
    {
        private readonly ICartOrderProvider _cartOrderProvider;
        private readonly ICollectiveLocationService _collectiveLocationService;

        public GetShipToCollectionHandlerOverride(ICartOrderProviderFactory cartOrderProviderFactory, IHandlerFactory handlerFactory, ICustomerHelper customerHelper, IAuthenticationService authenticationService, ICollectiveLocationService collectiveLocationService) : base(handlerFactory, customerHelper, authenticationService)
        {
            _collectiveLocationService = collectiveLocationService;
            _cartOrderProvider = cartOrderProviderFactory.GetCartOrderProvider();
        }

        public override GetShipToCollectionResult Execute(IUnitOfWork unitOfWork, GetShipToCollectionParameter parameter, GetShipToCollectionResult result)
        {
            var userProfile = SiteContext.Current.UserProfile;
            var getUserAndBillToResult = CustomerHelper.GetUserAndBillTo(unitOfWork, result, parameter.BillToId);
            if (getUserAndBillToResult.ErrorResult != null)
            {
                return CreateErrorServiceResult(result, getUserAndBillToResult.ErrorResult.SubCode, getUserAndBillToResult.ErrorResult.Message);
            }

            result.BillTo = getUserAndBillToResult.BillTo;
            if (!parameter.ExcludeShowAll)
            {
                result.GetShipToResults.Add(new GetShipToResult
                {
                    Label = CustomerHelper.T("Show All"),
                    BillTo = result.BillTo,
                    ShipTo = new Customer
                    {
                        CustomerNumber = result.BillTo.CustomerNumber,
                        CustomerSequence = "-1"
                    }
                });
            }

            var settings = HandlerFactory.GetHandler<IHandler<GetSettingsParameter, GetCustomerSettingsResult>>().Execute(unitOfWork, new GetSettingsParameter(), new GetCustomerSettingsResult());
            var assignedShipTos = GetAssignedShipTos(unitOfWork, parameter, getUserAndBillToResult);

            if (parameter.ExcludeBillTo)
            {
                assignedShipTos = assignedShipTos.Where(o => o.Id != getUserAndBillToResult.BillTo.Id);
            }

            var cartOrder = _cartOrderProvider.GetCartOrder();
            if (cartOrder != null)
            {
                assignedShipTos = assignedShipTos.Expand(x => x.CustomProperties).Where(x => x.CustomProperties.FirstOrDefault(y => y.Name == Constants.CustomProperties.Customer.CustomShippingAddressCartId) == null ||
                    x.CustomProperties.FirstOrDefault(y => y.Name == Constants.CustomProperties.Customer.CustomShippingAddressCartId).Value == cartOrder.Id.ToString());
            }
            else
            {
                assignedShipTos = assignedShipTos.Expand(x => x.CustomProperties).Where(x => x.CustomProperties.FirstOrDefault(y => y.Name == Constants.CustomProperties.Customer.CustomShippingAddressCartId) == null);
            }

            if (!parameter.Filter.IsBlank())
            {
                assignedShipTos = assignedShipTos.Where(
                    x => x.CustomerSequence.Contains(parameter.Filter) ||
                        x.ErpSequence.Contains(parameter.Filter) ||
                        x.CompanyName.Contains(parameter.Filter) ||
                        x.FirstName.Contains(parameter.Filter) ||
                        x.LastName.Contains(parameter.Filter) ||
                        x.Address1.Contains(parameter.Filter) ||
                        x.Address2.Contains(parameter.Filter) ||
                        x.City.Contains(parameter.Filter) ||
                        x.PostalCode.Contains(parameter.Filter));
            }

            if (parameter.GetCountry)
            {
                assignedShipTos = assignedShipTos.Expand(x => x.Country);
            }

            if (parameter.GetState)
            {
                assignedShipTos = assignedShipTos.Expand(x => x.State);
            }

            var sortedAvailableShipTos = GetSortedShipTos(userProfile, assignedShipTos, parameter);

            if (parameter.PageSize.HasValue)
            {
                sortedAvailableShipTos = ApplyPaging(unitOfWork, parameter, result, sortedAvailableShipTos) as IOrderedQueryable<Customer>;
            }

            if (sortedAvailableShipTos != null)
            {
                foreach (var shipTo in sortedAvailableShipTos.Where(x => x.Id != getUserAndBillToResult.BillTo.Id).ToList())
                {
                    var formatAddress = CustomerHelper.FormatAddress(shipTo);
                    if (parameter.GetApprovalCounts)
                    {
                        formatAddress = GetApprovalCountLabel(unitOfWork, result.BillTo, shipTo) + formatAddress;
                    }

                    result.GetShipToResults.Add(new GetShipToResult
                    {
                        Label = formatAddress,
                        BillTo = result.BillTo,
                        ShipTo = shipTo,
                        Validation = parameter.GetValidation ? CustomerHelper.PopulateShipToValidation(unitOfWork, settings, result.BillTo, shipTo) : null,
                        IsDefault = SiteContext.Current.UserProfile.DefaultCustomerId.HasValue && SiteContext.Current.UserProfile.DefaultCustomerId.GetValueOrDefault() == shipTo.Id,
                    });
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }

        private static IQueryable<Customer> GetAssignedShipTos(IUnitOfWork unitOfWork, GetShipToCollectionParameter parameter, GetUserAndBillToResult getUserAndBillToResult)
        {
            if (parameter.AssignedOnly)
            {
                return unitOfWork.GetTypedRepository<ICustomerRepository>().GetAssignedShipTos(getUserAndBillToResult.BillTo, getUserAndBillToResult.UserProfile.Id);
            }

            return unitOfWork.GetTypedRepository<ICustomerRepository>().GetAvailableShipTos(getUserAndBillToResult.BillTo, getUserAndBillToResult.UserProfile.Id, SiteContext.Current.WebsiteDto.Id);
        }

        private string GetApprovalCountLabel(IUnitOfWork unitOfWork, Customer billTo, Customer shipTo)
        {
            var approvalCount = GetApprovalCount(unitOfWork, billTo, shipTo);
            return approvalCount > 0 ? $"({approvalCount})" : string.Empty;
        }

        private IQueryable<Customer> GetSortedShipTos(UserProfile userProfile, IQueryable<Customer> assignedShipTos, GetShipToCollectionParameter parameter)
        {
            var defaultShipTo = userProfile.DefaultCustomer?.IsShipTo ?? false ? userProfile.DefaultCustomer : null;
            var currentLocation = _collectiveLocationService.Get(userProfile.Id, defaultShipTo, false);

            if (defaultShipTo != null && (currentLocation?.InitialLocation ?? false))
            {
                return !parameter.Sort.IsBlank() ? assignedShipTos.OrderByDescending(x => defaultShipTo.Id.Equals(x.Id)).ThenBy(_ => parameter.Sort) : assignedShipTos.OrderByDescending(x => defaultShipTo.Id.Equals(x.Id)).ThenBy(x => x.CustomerSequence);
            }

            if (SiteContext.Current?.ShipTo != null)
            {
                return !parameter.Sort.IsBlank() ? assignedShipTos.OrderByDescending(x => SiteContext.Current.ShipTo.Id.Equals(x.Id)).ThenBy(_ => parameter.Sort) : assignedShipTos.OrderByDescending(x => SiteContext.Current.ShipTo.Id.Equals(x.Id)).ThenBy(x => x.CustomerSequence);
            }

            return !parameter.Sort.IsBlank() ? assignedShipTos.OrderBy(parameter.Sort) : assignedShipTos.OrderBy(x => x.CustomerSequence);
        }
    }
}
