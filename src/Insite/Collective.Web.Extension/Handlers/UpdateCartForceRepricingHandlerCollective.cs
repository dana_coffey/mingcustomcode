﻿using System.Web;
using Collective.Core.Helpers;
using Insite.Cart.Services.Parameters;
using Insite.Cart.Services.Results;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName("UpdateCartForceRepricingHandlerCollective")]
    public class UpdateCartForceRepricingHandlerCollective : HandlerBase<UpdateCartParameter, UpdateCartResult>
    {
        public override int Order => 560;

        public override UpdateCartResult Execute(IUnitOfWork unitOfWork, UpdateCartParameter parameter, UpdateCartResult result)
        {
            if (ParseHelper.ParseBoolean(HttpContext.Current.Request.QueryString.Get("forceCartRepricing")).GetValueOrDefault(false))
            {
                var cart = result?.GetCartResult?.Cart;
                if (cart != null)
                {
                    cart.LastPricingOn = null;
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
