﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Entities;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Order.Services.Handlers;
using Insite.Order.Services.Parameters;
using Insite.Order.Services.Results;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(GetStoredOrderCollectionFilterHandler))]
    public class GetStoredOrderCollectionFilterHandlerCollective : GetStoredOrderCollectionFilterHandler
    {
        public override GetOrderCollectionResult Execute(IUnitOfWork unitOfWork, GetOrderCollectionParameter parameter, GetOrderCollectionResult result)
        {
            if (!result.IsRealTime)
            {
                if (parameter.StatusCollection != null && parameter.StatusCollection.Any())
                {
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(o => parameter.StatusCollection.Contains(o.Status));
                }

                if (!string.IsNullOrWhiteSpace(parameter.OrderNumber))
                {
                    var orderNumber = parameter.OrderNumber.ToLower().PadLeft(parameter.OrderNumber.Length > 8 ? 11 : 8, '0');
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(p => p.ErpOrderNumber.ToLower().StartsWith(orderNumber));
                }

                if (!string.IsNullOrWhiteSpace(parameter.CustomerPO))
                {
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(p => p.CustomerPO.ToLower().Contains(parameter.CustomerPO.ToLower()));
                }

                var reference = HttpContext.Current.Request.QueryString.Get("reference");
                if (!string.IsNullOrWhiteSpace(reference))
                {
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(p => p.CustomProperties.Any(x => x.Name == Constants.CustomProperties.OrderHistory.CustomerReference && x.Value.Contains(reference)));
                }

                if (parameter.CustomerSequence != "-1")
                {
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(o => o.CustomerSequence == (parameter.CustomerSequence ?? string.Empty));
                }

                if (parameter.FromDate.HasValue)
                {
                    var fromDate = parameter.FromDate.Value.Date;
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(o => o.OrderDate >= fromDate);
                }

                if (parameter.ToDate.HasValue)
                {
                    var toDate = parameter.ToDate.Value.Date.AddDays(1.0).AddMinutes(-1.0);
                    parameter.OrdersQuery = parameter.OrdersQuery.Where(o => o.OrderDate <= toDate);
                }

                if (!string.IsNullOrWhiteSpace(parameter.OrderTotalOperator))
                {
                    if (parameter.OrderTotalOperator.Equals("Less Than", StringComparison.OrdinalIgnoreCase))
                    {
                        parameter.OrdersQuery = parameter.OrdersQuery.Where(o => o.OrderTotal < parameter.OrderTotal);
                    }
                    else if (parameter.OrderTotalOperator.Equals("Greater Than", StringComparison.OrdinalIgnoreCase))
                    {
                        parameter.OrdersQuery = parameter.OrdersQuery.Where(o => o.OrderTotal > parameter.OrderTotal);
                    }
                }

                var productSerialNumberFilter = HttpContext.Current.Request.QueryString.Get("serialNumber");
                if (!string.IsNullOrEmpty(productSerialNumberFilter))
                {
                    List<Guid> lineIds;
                    if (productSerialNumberFilter.Length > 3)
                    {
                        // Over 3 characters, we will try to find a serial number containing specified string.
                        lineIds = unitOfWork.GetRepository<OrderHistoryLineSerialNumber>().GetTableAsNoTracking().Where(x => x.SerialNo.ToUpper().Contains(productSerialNumberFilter.ToUpper())).Select(x => x.OrderHistoryLineId).ToList();
                    }
                    else
                    {
                        // Under 4 characters, the serial number must be equal to the specified one.
                        lineIds = unitOfWork.GetRepository<OrderHistoryLineSerialNumber>().GetTableAsNoTracking().Where(x => x.SerialNo.Equals(productSerialNumberFilter, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.OrderHistoryLineId).ToList();
                    }

                    parameter.OrdersQuery = parameter.OrdersQuery.Where(p => p.OrderHistoryLines.Any(line => lineIds.Contains(line.Id)));
                }
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
