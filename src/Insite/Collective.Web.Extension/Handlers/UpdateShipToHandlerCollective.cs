﻿using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services.Handlers;
using Insite.Core.SystemSetting.Groups.AccountManagement;
using Insite.Customers.Services.Parameters;
using Insite.Customers.Services.Results;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Handlers
{
    [DependencyName(nameof(UpdateShipToHandlerCollective))]
    public class UpdateShipToHandlerCollective : HandlerBase<UpdateShipToParameter, UpdateShipToResult>
    {
        public override int Order => 600;

        public override UpdateShipToResult Execute(IUnitOfWork unitOfWork, UpdateShipToParameter parameter, UpdateShipToResult result)
        {
            if (result.ShipTo != null && result.ShipTo.CustomerSequence.StartsWith(DependencyLocator.Current.GetInstance<CustomerDefaultsSettings>()?.ErpShipToPrefix ?? string.Empty))
            {
                var isTaxable = CollectiveInjector.GetInstance<ICollectiveTaxingHelper>()?.IsCustomerTaxable(result.ShipTo.CustomerNumber, result.ShipTo.State?.Abbreviation, result.ShipTo.CountryId) ?? true;

                result.ShipTo.TaxCode1 = isTaxable ? string.Empty : unitOfWork.GetRepository<Website>().GetTableAsNoTracking().FirstOrDefault(x => x.Id == SiteContext.Current.WebsiteDto.Id)?.TaxFreeTaxCode ?? string.Empty;
            }

            return NextHandler.Execute(unitOfWork, parameter, result);
        }
    }
}
