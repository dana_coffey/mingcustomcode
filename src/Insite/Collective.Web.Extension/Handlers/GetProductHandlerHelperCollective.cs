﻿using System;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Interfaces;
using Collective.Web.Extension.Core.Models.Inventory;
using Insite.Catalog.Services.Dtos;
using Insite.Catalog.Services.Handlers.Helpers;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Localization;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Core.Plugins.Catalog;
using Insite.Core.Plugins.EntityUtilities;
using Insite.Core.Plugins.Inventory;
using Insite.Core.Plugins.Pipelines.Pricing;
using Insite.Core.Plugins.Search;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting.Groups.Catalog;
using Insite.Core.SystemSetting.Groups.SystemSettings;
using Insite.Core.Translation;
using Insite.Data.Entities;
using Insite.Data.Repositories.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Handlers
{
    public class GetProductHandlerHelperCollective : GetProductHandlerHelper
    {
        private readonly ICollectiveInventoryService _collectiveInventoryService;

        public GetProductHandlerHelperCollective(Lazy<IProductSearchProvider> productSearchProvider, Lazy<IProductBatchLoader> productBatchLoader, Lazy<ICacheManager> cacheManager, Lazy<ITranslationLocalizer> translationLocalizer, Lazy<IInventoryProvider> inventoryProvider, Lazy<IProductUtilities> productUtilities, Lazy<ICategoryUtilities> categoryUtilities, Lazy<IHtmlContentProvider> htmlContentProvider, Lazy<IObjectToObjectMapper> objectToObjectMapper, Lazy<IEntityTranslationService> entityTranslationService, Lazy<ICurrencyFormatProvider> currencyFormatProvider, Lazy<ICatalogPathBuilder> catalogPathBuilder, SecuritySettings securitySettings, InventorySettings inventorySettings, PricingSettings pricingSettings, ProductListSettings productListSettings, CatalogGeneralSettings catalogGeneralSettings, Lazy<IPricingPipeline> pricingPipeline, Lazy<IOrderLineUtilities> orderLineUtilities, ICollectiveInventoryService collectiveInventoryService)
            : base(productSearchProvider, productBatchLoader, cacheManager, translationLocalizer, inventoryProvider, productUtilities, categoryUtilities, htmlContentProvider, objectToObjectMapper, entityTranslationService, currencyFormatProvider, catalogPathBuilder, securitySettings, inventorySettings, pricingSettings, productListSettings, catalogGeneralSettings, pricingPipeline, orderLineUtilities)
        {
            _collectiveInventoryService = collectiveInventoryService;
        }

        public override void PopulateProductDto(IUnitOfWork unitOfWork, ICategoryRepository categoryRepository, IProductRepository productRepository, Product product, ProductDto productDto, Category category, Customer currentCustomer, bool ignoreIsConfigured = false, bool getHtmlContent = false)
        {
            base.PopulateProductDto(unitOfWork, categoryRepository, productRepository, product, productDto, category, currentCustomer, ignoreIsConfigured, getHtmlContent);

            var isCatalogProduct = productDto.Properties.ContainsKey(Constants.CustomProperties.Product.IsCatalogProduct) && ParseHelper.ParseBoolean(productDto.Properties[Constants.CustomProperties.Product.IsCatalogProduct], false);

            var availabilityStatus = _collectiveInventoryService.GetProductInventoryStatusModel(productDto.ERPNumber, isCatalogProduct);

            if (availabilityStatus.Status == AvailabilityStatusType.Restricted)
            {
                DisableAddToCart(productDto);
            }

            productDto.Properties.Add(Constants.HandlerResultProperties.Product.AvailabilityStatus, JsonConvert.SerializeObject(availabilityStatus));

            if (productDto.Properties.ContainsKey(Constants.HandlerResultProperties.Product.CustomFields))
            {
                productDto.Properties.Remove(Constants.HandlerResultProperties.Product.CustomFields);
            }

            productDto.Properties.Add(Constants.HandlerResultProperties.Product.CustomFields, JsonConvert.SerializeObject(CollectiveInjector.GetInstance<ICollectiveProductService>().GetCustomFields(productDto.Id)));
            productDto.Properties.Add(Constants.HandlerResultProperties.Product.ShowPartsTab, ProductHelper.IsProductPartsListCompatible(product).ToString());

            SetReplacementForProperties(productDto);
            SetSupersedeProperties(productDto, availabilityStatus);
            SetObsoleteProperties(productDto, availabilityStatus);
        }

        private static void DisableAddToCart(ProductDto productDto)
        {
            productDto.CanAddToCart = false;
            productDto.CanShowPrice = false;
            productDto.CanEnterQuantity = false;
            productDto.CanAddToWishlist = false;
            productDto.CanBackOrder = false;
            productDto.CanConfigure = false;
            productDto.CanShowUnitOfMeasure = false;
        }

        private void SetObsoleteProperties(ProductDto productDto, AvailabilityStatusModel availabilityStatus)
        {
            if (productDto.Properties.TryGetValue(Constants.CustomProperties.Product.StatusField, out var statusField) && statusField == Constants.CustomProperties.Product.Status.Obsolete)
            {
                productDto.Properties.Add(Constants.HandlerResultProperties.Product.IsObsolete, true.ToString().ToLower());

                if (availabilityStatus.Status != AvailabilityStatusType.AvailableLocal)
                {
                    DisableAddToCart(productDto);
                    productDto.Properties.Add(Constants.HandlerResultProperties.Product.HideAvailability, true.ToString());
                }
            }
        }

        private void SetReplacementForProperties(ProductDto productDto)
        {
            if (productDto.Properties.TryGetValue(Constants.CustomProperties.Product.StatusField, out var statusField) && (statusField == Constants.CustomProperties.Product.Status.None || statusField == string.Empty))
            {
                var replacementFor = CollectiveInjector.GetInstance<ICollectiveProductService>().GetReplacementProducts(productDto.ERPNumber);
                if (replacementFor?.Any() ?? false)
                {
                    productDto.Properties.Add(Constants.HandlerResultProperties.Product.IsReplacement, true.ToString());
                    productDto.Properties.Add(Constants.HandlerResultProperties.Product.ReplacementFor, JsonConvert.SerializeObject(replacementFor));
                }
            }
        }

        private void SetSupersedeProperties(ProductDto productDto, AvailabilityStatusModel availabilityStatus)
        {
            if (productDto.Properties.TryGetValue(Constants.CustomProperties.Product.StatusField, out var statusField) && statusField == Constants.CustomProperties.Product.Status.Superseded)
            {
                var relatedProducts = CollectiveInjector.GetInstance<ICollectiveProductService>().GetRelatedProducts(productDto.Id, Constants.ProductRelations.Supersede);
                if (relatedProducts?.Products.Any() ?? false)
                {
                    productDto.Properties.Add(Constants.HandlerResultProperties.Product.IsSuperseded, true.ToString().ToLower());
                    productDto.Properties.Add(Constants.HandlerResultProperties.Product.SupersededBy, JsonConvert.SerializeObject(relatedProducts.Products.Select(p => new
                    {
                        ErpNumber = p.ERPNumber,
                        ProductName = p.Name,
                        Url = p.ProductDetailUrl
                    })));

                    if (availabilityStatus.Status != AvailabilityStatusType.AvailableLocal)
                    {
                        DisableAddToCart(productDto);
                        productDto.Properties.Add(Constants.HandlerResultProperties.Product.HideAvailability, true.ToString());
                    }
                }
            }
        }
    }
}
