﻿using System.ComponentModel;
using Insite.ContentLibrary.ContentFields;
using Insite.ContentLibrary.Widgets;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Mosaic")]
    public class Mosaic : ContentWidget
    {
        [TextContentField(SortOrder = 102, IsRequired = false, DisplayName = "Description - Mosaic 1")]
        public virtual string DescriptionMosaic1
        {
            get => GetValue("DescriptionMosaic1", string.Empty, FieldType.Contextual);
            set => SetValue("DescriptionMosaic1", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 106, IsRequired = false, DisplayName = "Description - Mosaic 2")]
        public virtual string DescriptionMosaic2
        {
            get => GetValue("DescriptionMosaic2", string.Empty, FieldType.Contextual);
            set => SetValue("DescriptionMosaic2", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 110, IsRequired = false, DisplayName = "Description - Mosaic 3")]
        public virtual string DescriptionMosaic3
        {
            get => GetValue("DescriptionMosaic3", string.Empty, FieldType.Contextual);
            set => SetValue("DescriptionMosaic3", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 114, IsRequired = false, DisplayName = "Description - Mosaic 4")]
        public virtual string DescriptionMosaic4
        {
            get => GetValue("DescriptionMosaic4", string.Empty, FieldType.Contextual);
            set => SetValue("DescriptionMosaic4", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 100, IsRequired = false, DisplayName = "General Title - Mosaic")]
        public virtual string GeneralTitleMosaic
        {
            get => GetValue("GeneralTitleMosaic", string.Empty, FieldType.Contextual);
            set => SetValue("GeneralTitleMosaic", value, FieldType.Contextual);
        }

        [FilePickerField(SortOrder = 103, IsRequired = true, DisplayName = "Image", ResourceType = "ImageFiles - Mosaic 1")]
        public virtual string ImageMosaic1
        {
            get => GetValue("ImageMosaic1", string.Empty, FieldType.Contextual);
            set => SetValue("ImageMosaic1", value, FieldType.Contextual);
        }

        [FilePickerField(SortOrder = 107, IsRequired = false, DisplayName = "Image", ResourceType = "ImageFiles - Mosaic 2")]
        public virtual string ImageMosaic2
        {
            get => GetValue("ImageMosaic2", string.Empty, FieldType.Contextual);
            set => SetValue("ImageMosaic2", value, FieldType.Contextual);
        }

        [FilePickerField(SortOrder = 111, IsRequired = false, DisplayName = "Image", ResourceType = "ImageFiles - Mosaic 3")]
        public virtual string ImageMosaic3
        {
            get => GetValue("ImageMosaic3", string.Empty, FieldType.Contextual);
            set => SetValue("ImageMosaic3", value, FieldType.Contextual);
        }

        [FilePickerField(SortOrder = 115, IsRequired = false, DisplayName = "Image", ResourceType = "ImageFiles - Mosaic 4")]
        public virtual string ImageMosaic4
        {
            get => GetValue("ImageMosaic4", string.Empty, FieldType.Contextual);
            set => SetValue("ImageMosaic4", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 104, IsRequired = true, DisplayName = "Link - Mosaic 1")]
        public virtual string LinkMosaic1
        {
            get => GetValue("LinkMosaic1", string.Empty, FieldType.Contextual);
            set => SetValue("LinkMosaic1", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 108, IsRequired = false, DisplayName = "Link - Mosaic 2")]
        public virtual string LinkMosaic2
        {
            get => GetValue("LinkMosaic2", string.Empty, FieldType.Contextual);
            set => SetValue("LinkMosaic2", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 112, IsRequired = false, DisplayName = "Link - Mosaic 3")]
        public virtual string LinkMosaic3
        {
            get => GetValue("LinkMosaic3", string.Empty, FieldType.Contextual);
            set => SetValue("LinkMosaic3", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 116, IsRequired = false, DisplayName = "Link - Mosaic 4")]
        public virtual string LinkMosaic4
        {
            get => GetValue("LinkMosaic4", string.Empty, FieldType.Contextual);
            set => SetValue("LinkMosaic4", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 101, IsRequired = true, DisplayName = "Title - Mosaic 1")]
        public virtual string TitleMosaic1
        {
            get => GetValue("TitleMosaic1", string.Empty, FieldType.Contextual);
            set => SetValue("TitleMosaic1", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 105, IsRequired = false, DisplayName = "Title - Mosaic 2")]
        public virtual string TitleMosaic2
        {
            get => GetValue("TitleMosaic2", string.Empty, FieldType.Contextual);
            set => SetValue("TitleMosaic2", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 109, IsRequired = false, DisplayName = "Title - Mosaic 3")]
        public virtual string TitleMosaic3
        {
            get => GetValue("TitleMosaic3", string.Empty, FieldType.Contextual);
            set => SetValue("TitleMosaic3", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 113, IsRequired = false, DisplayName = "Title - Mosaic 4")]
        public virtual string TitleMosaic4
        {
            get => GetValue("TitleMosaic4", string.Empty, FieldType.Contextual);
            set => SetValue("TitleMosaic4", value, FieldType.Contextual);
        }
    }
}
