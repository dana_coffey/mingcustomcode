﻿using System.ComponentModel;
using Insite.ContentLibrary.ContentFields;
using Insite.ContentLibrary.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.Data.Entities;
using Insite.WebFramework.Content.Attributes;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Footer Links")]
    [AllowedParents(typeof(Footer))]
    public class FooterLinks : ContentWidget
    {
        [TextContentField(SortOrder = 8, IsRequired = false, DisplayName = "Facebook url")]
        public virtual string FacebookUrl
        {
            get => GetValue("FacebookUrl", string.Empty, FieldType.Contextual);
            set => SetValue("FacebookUrl", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 10, IsRequired = false, DisplayName = "Google+ url")]
        public virtual string GooglePlusUrl
        {
            get => GetValue("GooglePlusUrl", string.Empty, FieldType.Contextual);
            set => SetValue("GooglePlusUrl", value, FieldType.Contextual);
        }

        [RichTextContentField(SortOrder = 1, IsRequired = false, DisplayName = "Links - Column 1")]
        public virtual string LinksColumn1
        {
            get => GetValue("LinksColumn1", string.Empty, FieldType.Contextual);
            set => SetValue("LinksColumn1", value, FieldType.Contextual);
        }

        [RichTextContentField(SortOrder = 3, IsRequired = false, DisplayName = "Links - Column 2")]
        public virtual string LinksColumn2
        {
            get => GetValue("LinksColumn2", string.Empty, FieldType.Contextual);
            set => SetValue("LinksColumn2", value, FieldType.Contextual);
        }

        [RichTextContentField(SortOrder = 5, IsRequired = false, DisplayName = "Links - Column 3")]
        public virtual string LinksColumn3
        {
            get => GetValue("LinksColumn3", string.Empty, FieldType.Contextual);
            set => SetValue("LinksColumn3", value, FieldType.Contextual);
        }

        [RichTextContentField(SortOrder = 7, IsRequired = true, DisplayName = "Right Content")]
        public virtual string RightContent
        {
            get => GetValue("RightContent", string.Empty, FieldType.Contextual);
            set => SetValue("RightContent", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 0, IsRequired = false, DisplayName = "Title - Column 1")]
        public virtual string TitleColumn1
        {
            get => GetValue("TitleColumn1", string.Empty, FieldType.Contextual);
            set => SetValue("TitleColumn1", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 2, IsRequired = false, DisplayName = "Title - Column 2")]
        public virtual string TitleColumn2
        {
            get => GetValue("TitleColumn2", string.Empty, FieldType.Contextual);
            set => SetValue("TitleColumn2", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 4, IsRequired = false, DisplayName = "Title - Column 3")]
        public virtual string TitleColumn3
        {
            get => GetValue("TitleColumn3", string.Empty, FieldType.Contextual);
            set => SetValue("TitleColumn3", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 6, IsRequired = false, DisplayName = "Title - Column 4")]
        public virtual string TitleColumn4
        {
            get => GetValue("TitleColumn4", string.Empty, FieldType.Contextual);
            set => SetValue("TitleColumn4", value, FieldType.Contextual);
        }

        [TextContentField(SortOrder = 9, IsRequired = false, DisplayName = "Twitter url")]
        public virtual string TwitterUrl
        {
            get => GetValue("TwitterUrl", string.Empty, FieldType.Contextual);
            set => SetValue("TwitterUrl", value, FieldType.Contextual);
        }
    }
}
