﻿using System.ComponentModel;
using Insite.ContentLibrary.ContentFields;
using Insite.ContentLibrary.Widgets;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Banner")]
    public class Banner : ContentWidget
    {
        [FilePickerField(DisplayName = "Background Image", ResourceType = "ImageFiles")]
        public virtual string BackgroundImageUrl
        {
            get => GetValue("BackgroundImageUrl", string.Empty, FieldType.Contextual);
            set => SetValue("BackgroundImageUrl", value, FieldType.Contextual);
        }

        [TextContentField(DisplayName = "Subtitle")]
        public virtual string Subtitle
        {
            get => GetValue("Subtitle", string.Empty, FieldType.Contextual);
            set => SetValue("Subtitle", value, FieldType.Contextual);
        }

        [TextContentField(DisplayName = "Title")]
        public virtual string Title
        {
            get => GetValue("Title", string.Empty, FieldType.Contextual);
            set => SetValue("Title", value, FieldType.Contextual);
        }
    }
}
