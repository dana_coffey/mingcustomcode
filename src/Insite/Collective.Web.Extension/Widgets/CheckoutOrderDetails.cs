﻿using System.ComponentModel;
using Collective.Web.Extension.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.WebFramework.Content.Attributes;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Checkout Order Details")]
    [AllowedParents(typeof(CheckoutOrderDetailsPage))]
    public class CheckoutOrderDetails : ContentWidget
    {
    }
}
