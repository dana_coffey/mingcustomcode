﻿using System.ComponentModel;
using Collective.Web.Extension.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.WebFramework.Content.Attributes;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Product Warranty Lookup")]
    [AllowedParents(typeof(ProductWarrantyLookupPage))]
    public class ProductWarrantyLookup : ContentWidget
    {
    }
}