﻿using System.ComponentModel;
using Collective.Web.Extension.Pages;
using Insite.ContentLibrary.Widgets;
using Insite.WebFramework.Content.Attributes;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Collective - Checkout Order Review")]
    [AllowedParents(typeof(CheckoutOrderReviewPage))]
    public class CheckoutOrderReview : ContentWidget
    {
    }
}
