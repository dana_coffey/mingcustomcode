﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insite.ContentLibrary.ContentFields;
using Insite.ContentLibrary.Widgets;
using Insite.Core.Providers;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Widgets
{
    [DisplayName("Form - Feedback")]
    public class Feedback : ContentWidget
    {
        private string _emailIsInvalidErrorMessage;
        private string _emailIsRequiredErrorMessage;
        private string _messageIsRequiredErrorMessage;
        private string _topicIsRequiredErrorMessage;
        private string _subTopicIsRequiredErrorMessage;

        [RichTextContentField(IsRequired = true)]
        public virtual string SuccessMessage
        {
            get
            {
                return this.GetValue<string>(nameof(SuccessMessage), "<p>Your message has been sent.</p>", FieldType.Contextual);
            }
            set
            {
                this.SetValue<string>(nameof(SuccessMessage), value, FieldType.Contextual);
            }
        }

        [ListContentField(DisplayName = "Send Email To", InvalidRegExMessage = "Invalid Email Address", RegExValidation = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*")]
        public virtual List<string> EmailTo
        {
            get
            {
                return this.GetValue<List<string>>(nameof(EmailTo), new List<string>(), FieldType.Contextual);
            }
            set
            {
                this.SetValue<List<string>>(nameof(EmailTo), value, FieldType.Contextual);
            }
        }

        public virtual string EmailToValue
        {
            get
            {
                return string.Join(",", this.EmailTo.ToArray());
            }
        }

        [ListContentField(DisplayName = "Available Topics", IsRequired = true)]
        public virtual List<string> Topics
        {
            get
            {
                return this.GetValue<List<string>>(nameof(Topics), new List<string>(), FieldType.Contextual);
            }
            set
            {
                this.SetValue<List<string>>(nameof(Topics), value, FieldType.Contextual);
            }
        }

        [ListContentField(DisplayName = "Available Sub Topics", IsRequired = true)]
        public virtual List<string> SubTopics
        {
            get
            {
                return this.GetValue<List<string>>(nameof(SubTopics), new List<string>(), FieldType.Contextual);
            }
            set
            {
                this.SetValue<List<string>>(nameof(SubTopics), value, FieldType.Contextual);
            }
        }

        public virtual string EmailIsInvalidErrorMessage
        {
            get
            {
                return this._emailIsInvalidErrorMessage ?? (this._emailIsInvalidErrorMessage = MessageProvider.Current.ContactUsForm_EmailIsInvalidErrorMessage);
            }
        }

        public virtual string EmailIsRequiredErrorMessage
        {
            get
            {
                return this._emailIsRequiredErrorMessage ?? (this._emailIsRequiredErrorMessage = MessageProvider.Current.ContactUsForm_EmailIsRequiredErrorMessage);
            }
        }

        public virtual string MessageIsRequiredErrorMessage
        {
            get
            {
                return this._messageIsRequiredErrorMessage ?? (this._messageIsRequiredErrorMessage = MessageProvider.Current.ContactUsForm_MessageIsRequiredErrorMessage);
            }
        }

        public virtual string TopicIsRequiredErrorMessage
        {
            get
            {
                return this._topicIsRequiredErrorMessage ?? (this._topicIsRequiredErrorMessage = MessageProvider.Current.ContactUsForm_TopicIsRequiredErrorMessage);
            }
        }

        public virtual string SubTopicIsRequiredErrorMessage
        {
            get
            {
                return this._subTopicIsRequiredErrorMessage ?? (this._subTopicIsRequiredErrorMessage = MessageProvider.Current.GetMessage("SubTopicIsRequiredErrorMessage", "Sub Topic is Required"));
            }
        }

        public virtual string EmailAddressRegexPattern
        {
            get
            {
                return "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            }
        }
    }
}
