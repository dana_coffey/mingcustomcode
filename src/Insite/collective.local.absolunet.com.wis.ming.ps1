$ScriptPath = Split-Path $MyInvocation.MyCommand.Path
Import-Module $ScriptPath\function.wis.psm1 -Force
Import-Module $ScriptPath\function.wis.collective.psm1 -Force

RunAsAdministrator $myInvocation.MyCommand.Definition $MyInvocation.MyCommand.Path

function DeployMing([string] $wisPath, [string] $wisPathDllFolder)
{
	Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.InforSxe.Ming\bin\Collective.InforSxe.Ming.dll") -Destination "$wisPathDllFolder"
    Copy-Item -Force -Path (Join-Path (Split-Path -parent $PSCommandPath) "\Collective.InforSxe.Ming\bin\Collective.InforSxe.Ming.pdb") -Destination "$wisPathDllFolder"
}

DeployWis $function:DeployMing