﻿using System;
using System.Collections.Generic;
using System.IO;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;

namespace Collective.Integration.Connectors
{
    public class ProductRefreshConnector
    {
        private readonly IntegrationProcessorContext _context;

        public ProductRefreshConnector(IntegrationProcessorContext context)
        {
            _context = context;
        }

        public virtual List<InsiteProduct> GetProducts()
        {
            try
            {
                var filePath = IntegrationProcessorHelper.BuildJsonFilePath(Constants.ExportProductsFileName);

                if (File.Exists(filePath))
                {
                    return JsonHelper.FromFile<InsiteProduct>(filePath);
                }

                _context.Logger.Info("There is no file to import");
            }
            catch (Exception ex)
            {
                _context.Logger.Error(ex.Message + " " + ex.StackTrace);
            }

            return new List<InsiteProduct>();
        }
    }
}
