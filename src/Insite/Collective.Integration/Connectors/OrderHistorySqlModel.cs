﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Collective.Core.Helpers;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Connectors
{
    internal class OrderHistorySqlModel
    {
        public decimal AddonNet { get; set; }
        public int AddonNo { get; set; }
        public int AddOnType { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public bool BoExistsFl { get; set; }
        public string City { get; set; }
        public int Cono { get; set; }
        public decimal CoreCharge { get; set; }
        public string CoreChgTy { get; set; }
        public string CustNo { get; set; }
        public string CustomerEmail { get; set; }
        public string CustPo { get; set; }
        public string Descrip { get; set; }
        public DateTime? EnterDt { get; set; }
        public string EnterTm { get; set; }
        public int LineNo { get; set; }
        public string Name { get; set; }
        public decimal NetAmt { get; set; }
        public decimal NetOrd { get; set; }
        public string NoteLn { get; set; }
        public string OrderDisp { get; set; }
        public int OrderNo { get; set; }
        public int OrderSuf { get; set; }
        public decimal Price { get; set; }
        public string ProdDesc { get; set; }
        public string ProdDesc2 { get; set; }
        public DateTime? PromiseDt { get; set; }
        public int QtyOrd { get; set; }
        public int QtyShip { get; set; }
        public string Refer { get; set; }
        public DateTime? ReqShipDt { get; set; }
        public int SeqNo { get; set; }
        public string SerialNo { get; set; }
        public int SerialSeqNo { get; set; }
        public DateTime? ShipDt { get; set; }
        public string ShipProd { get; set; }
        public string ShipTo { get; set; }
        public string ShipToAddr1 { get; set; }
        public string ShipToAddr2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToNm { get; set; }
        public string ShipToSt { get; set; }
        public string ShipToZip { get; set; }
        public string ShipViaTy { get; set; }
        public string SlsRepOut { get; set; }
        public decimal SpecDiscAmt { get; set; }
        public string SpecNsType { get; set; }
        public int StageCd { get; set; }
        public string State { get; set; }
        public string TaxAmt { get; set; }
        public string TermsType { get; set; }
        public decimal TotInvAmt { get; set; }
        public decimal TotInvOrd { get; set; }
        public decimal TotLineAmt { get; set; }
        public decimal TotLineOrd { get; set; }
        public string TransType { get; set; }
        public string Unit { get; set; }
        public string WebOrderNumber { get; set; }
        public string Whse { get; set; }
        public string ZipCd { get; set; }

        public static List<OrderHistoryConnectorModel> ToConnectorModelList(IEnumerable<OrderHistorySqlModel> models)
        {
            var orders = new List<OrderHistoryConnectorModel>();

            foreach (var model in models)
            {
                var orderKey = $"{model.OrderNo}-{model.OrderSuf}";
                var lineKey = $"{model.ShipProd}-{model.LineNo}";
                var serialKey = $"{lineKey}-{model.SerialNo}";

                var order = orders.FirstOrDefault(x => x.Key == orderKey);
                if (order == null)
                {
                    order = new OrderHistoryConnectorModel
                    {
                        Name = model.Name,
                        Key = orderKey,
                        OrderSuf = model.OrderSuf,
                        Cono = model.Cono,
                        TotInvAmt = model.TotInvAmt,
                        State = model.State,
                        SpecDiscAmt = model.SpecDiscAmt,
                        ShipTo = model.ShipTo,
                        Lines = new List<OrderHistoryLineConnectorModel>(),
                        ZipCd = model.ZipCd,
                        City = model.City,
                        CustNo = model.CustNo,
                        CustPo = model.CustPo,
                        ShipToAddr1 = model.ShipToAddr1,
                        ShipToAddr2 = model.ShipToAddr2,
                        OrderNo = model.OrderNo,
                        OrderDisp = model.OrderDisp,
                        EnterTm = model.EnterTm,
                        Addr1 = model.Addr1,
                        Addr2 = model.Addr2,
                        AddOns = new List<OrderHistoryAddOnConnectorModel>(),
                        BoExistsFl = model.BoExistsFl,
                        EnterDt = model.EnterDt,
                        PromiseDt = model.ReqShipDt,
                        ReqShipDt = model.ReqShipDt,
                        ShipDt = model.ShipDt,
                        ShipToCity = model.ShipToCity,
                        ShipToNm = model.ShipToNm,
                        ShipToSt = model.ShipToSt,
                        ShipToZip = model.ShipToZip,
                        ShipViaTy = model.ShipViaTy,
                        SlsRepOut = model.SlsRepOut,
                        StageCd = model.StageCd,
                        TaxAmt = model.TaxAmt.Split(';').Select(x => ParseHelper.ParseDecimal(x, CultureInfo.InvariantCulture) ?? 0).Sum(),
                        TermsType = model.TermsType,
                        TotInvOrd = model.TotInvOrd,
                        TotLineAmt = model.TotLineAmt,
                        TotLineOrd = model.TotLineOrd,
                        TransType = model.TransType,
                        WebOrderNumber = model.WebOrderNumber,
                        Warehouse = model.Whse,
                        CustomerEmail = model.CustomerEmail,
                        Refer = model.Refer
                    };

                    orders.Add(order);
                }

                var line = order.Lines.FirstOrDefault(x => x.Key == lineKey);
                if (line == null)
                {
                    line = new OrderHistoryLineConnectorModel
                    {
                        Key = lineKey,
                        ShipProd = model.ShipProd,
                        CoreCharge = model.CoreCharge,
                        CoreChgTy = model.CoreChgTy,
                        Descrip = model.Descrip,
                        LineNo = model.LineNo,
                        NetAmt = model.NetAmt,
                        NetOrd = model.NetOrd,
                        NoteLn = model.NoteLn,
                        Price = model.Price,
                        ProdDesc = model.ProdDesc,
                        ProdDesc2 = model.ProdDesc2,
                        QtyOrd = model.QtyOrd,
                        QtyShip = model.QtyShip,
                        SpecNsType = model.SpecNsType,
                        Unit = model.Unit,
                        SerialNumbers = new List<OrderHistoryLineSerialNumbersConnectorModel>()
                    };
                    order.Lines.Add(line);
                }

                if (line.SerialNumbers.FirstOrDefault(x => x.Key == serialKey) == null)
                {
                    line.SerialNumbers.Add(new OrderHistoryLineSerialNumbersConnectorModel
                    {
                        Key = serialKey,
                        SeqNo = model.SerialSeqNo,
                        SerialNo = model.SerialNo
                    });
                }

                if (order.AddOns.FirstOrDefault(x => x.SequenceNo == model.SeqNo) == null)
                {
                    order.AddOns.Add(new OrderHistoryAddOnConnectorModel
                    {
                        SequenceNo = model.SeqNo,
                        AddonNo = model.AddonNo,
                        AddOnType = model.AddOnType,
                        AddonNet = model.AddonNet
                    });
                }
            }

            return orders;
        }
    }
}
