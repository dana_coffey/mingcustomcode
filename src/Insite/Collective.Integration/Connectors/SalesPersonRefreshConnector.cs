﻿using System.Collections.Generic;
using Collective.Core.Constant;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Connectors
{
    public class SalespersonRefreshConnector
    {
        public List<SalespersonConnectorModel> GetSalespersons()
        {
            var tags = new Dictionary<string, string>
            {
                { "{CompanyNumber}", Constants.InforSxe.CompanyNumber.ToString() }
            };

            var query = QueryHelper.ReplaceTags(QueryHelper.GetQueryFromEmbeddedFile("SalespersonRefreshQuery.sql"), tags);

            return CommandHelper.ExecuteOdbc<SalespersonConnectorModel>(query, ConnectionHelper.GetInforSxeConnectionString());
        }
    }
}
