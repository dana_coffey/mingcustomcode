﻿using System;
using System.Collections.Generic;
using Collective.Core.Constant;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Connectors
{
    public class CustomerRefreshConnector
    {
        public List<CustomerConnectorModel> GetBillToCustomers(DateTime? updatedSince, string customerNumber)
        {
            return ExecuteQuery(QueryHelper.GetQueryFromEmbeddedFile("CustomerRefreshBillToQuery.sql"), updatedSince, customerNumber);
        }

        public List<CustomerConnectorModel> GetShipToCustomers(DateTime? updatedSince, string customerNumber)
        {
            return ExecuteQuery(QueryHelper.GetQueryFromEmbeddedFile("CustomerRefreshShipToQuery.sql"), updatedSince, customerNumber);
        }

        private List<CustomerConnectorModel> ExecuteQuery(string query, DateTime? updatedSince, string customerNumber)
        {
            var tags = new Dictionary<string, string>
            {
                { "{CompanyNumber}", Constants.InforSxe.CompanyNumber.ToString() },
                { "{CustomerNumberCondition}", !string.IsNullOrEmpty(customerNumber) ? $" and custno = '{customerNumber}'" : string.Empty },
                { "{UpdatedSinceCondition}", updatedSince.HasValue && string.IsNullOrEmpty(customerNumber) ? $" and transdt >= '{DateTimeHelper.ToSqlFormat(updatedSince.Value)}'" : string.Empty },
            };

            query = QueryHelper.ReplaceTags(query, tags);

            return CommandHelper.ExecuteOdbc<CustomerConnectorModel>(query, ConnectionHelper.GetInforSxeConnectionString());
        }
    }
}
