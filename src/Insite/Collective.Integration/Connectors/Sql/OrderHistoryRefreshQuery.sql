﻿select

    ------------------
    ----- Header -----
    ------------------
    oeeh.cono,
    oeeh.orderno,
    oeeh.ordersuf,
    oeeh.stagecd,
    oeeh.custno,
    arsc.name,
    arsc.addr[1] as addr1,
    arsc.addr[2] as addr2,
    arsc.city,
    arsc.state,
    arsc.zipcd,
    oeeh.shipto,
    oeeh.shiptonm,
    oeeh.shiptoaddr[1] as shiptoaddr1,
    oeeh.shiptoaddr[2] as shiptoaddr2,
    oeeh.shiptocity,
    oeeh.shiptost,
    oeeh.shiptozip,
    oeeh.user1 as webordernumber,
    oeeh.user3 as customerEmail,
    oeeh.custpo,
    oeeh.enterdt,
    oeeh.entertm,
    oeeh.shipdt,
    oeeh.transtype,
    oeeh.totinvord,
    oeeh.totinvamt,
    oeeh.totlineord,
    oeeh.totlineamt,
    oeeh.specdiscamt,
    oeeh.shipviaty,
    oeeh.slsrepout,
    oeeh.termstype,
    oeeh.taxamt,
    oeeh.boexistsfl,
    oeeh.taxamt,
    oeeh.whse,
    oeeh.refer,
	oeeh.promisedt,
	oeeh.reqshipdt,
	oeeh.orderdisp,

    -----------------
    ----- Lines -----
    -----------------
    oeel.lineno,
    oeel.shipprod,
    oeel.specnstype,
    oeel.proddesc,
    oeel.proddesc2,
    icsp.descrip,
    oeel.qtyord,
    oeel.qtyship,
    oeel.price,
    oeel.netord,
    oeel.netamt,
    oeel.unit,
    oeel.corechgty,
    oeel.corecharge,
    com.noteln,

	--------------------------
    ----- Serial Numbers -----
    --------------------------
    icses.serialno,
    icses.seqno as serialseqno,

    ------------------
    ----- AddOns -----
    ------------------
    addon.seqno,
    addon.addontype,
    addon.addonnet,
    addon.addonno

from
    pub.oeeh oeeh
    inner join pub.arsc
        on oeeh.cono = arsc.cono
        and oeeh.custno = arsc.custno
    inner join pub.oeel
        on oeeh.cono = oeel.cono
        and oeeh.orderno = oeel.orderno
        and oeeh.ordersuf = oeel.ordersuf
        left join pub.icsp
            on  icsp.cono = oeel.cono
            and icsp.prod = oeel.shipprod
        left join pub.com
            on  com.cono = oeel.cono
            and com.comtype = 'oe'
            and com.orderno = oeel.orderno
            and com.ordersuf = oeel.ordersuf
            and com.lineno = oeel.lineno
            and (com.printfl = 1 or com.printfl2 = 1)
	left join pub.icses
        on icses.cono = oeel.cono
        and icses.prod = oeel.shipprod
        and icses.whse = oeel.whse
        and icses.invno = oeel.orderno
        and icses.invsuf = oeel.ordersuf
        and icses.invlineno = oeel.lineno
    left join pub.addon
        on  oeeh.cono = addon.cono
        and addon.ordertype = 'oe'
        and oeeh.orderno = addon.orderno
        and oeeh.ordersuf = addon.ordersuf
where oeeh.cono = {CompanyNumber}
    and oeeh.enterdt is not null
    {TransactionDateWhereClause}
    {ErpOrderNumberWhereClause}
    {ErpOrderSuffixWhereClause}
    and ((oeeh.stagecd >= 0 and oeeh.stagecd <= 5) or oeeh.stagecd = 9)
    and oeeh.transtype not in ('BL', 'ST', 'FO', 'BR')
with(nolock)