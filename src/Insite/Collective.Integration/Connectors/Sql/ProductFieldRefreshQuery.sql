﻿select		FT.CategoryId					as CategoryId
		,	null							as CategoryName
		,	FT.Id							as FieldId
		,	FT.Name							as FieldName
		,	FT.CVLId						as FieldCvlId
		,	FT.DataType						as DataType
		,	FTS_UnitOfMeasure.Value			as UnitOfMeasure
from dbo.FieldType FT
	left join dbo.FieldType_Settings FTS_Excluded
		on FT.Id = FTS_Excluded.FieldTypeId
		and FTS_Excluded.[Key] = 'ExcludedFromInsiteCustomFields'
		and FTS_Excluded.Value = 'true'
	left join dbo.FieldType_Settings FTS_Excluded_Specs
		on FT.Id = FTS_Excluded_Specs.FieldTypeId
		and FTS_Excluded_Specs.[Key] = 'ExcludedFromSpecifications'
		and FTS_Excluded_Specs.Value = 'true'
	left join dbo.FieldType_Settings FTS_UnitOfMeasure
		on FT.Id = FTS_UnitOfMeasure.FieldTypeId
		and FTS_UnitOfMeasure.[Key] = 'UOM'
where EntityTypeId in ('Item', 'Product')
  and FTS_Excluded.FieldTypeId IS NULL and FTS_Excluded_Specs.FieldTypeId IS NULL
order by FT.[Index]
with(nolock)