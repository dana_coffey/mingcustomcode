﻿SELECT
  arss.cono,
  arss.custno,
  arss.shipto,
  arss.name,
  arss.addr,
  arss.addr3,
  arss.city,
  arss.countrycd,
  arss.email,
  arss.phoneno,
  arss.state,
  arss.faxphoneno,
  arss.zipcd,
  arss.statustype,
  1 as isshipto,
  '' as custtype,
  arss.shipviaty,
  arss.salesterr as whse,
  case when arss.taxablety = 'n' then 1 else 0 end as istaxexempt,
  arss.user10 as specificwarehouse,
  arss.slsrepout
FROM
  pub.arss
WHERE
  arss.Cono = {CompanyNumber}
  {UpdatedSinceCondition}
  {CustomerNumberCondition}
WITH(NOLOCK)
