﻿select
  arsc.cono,
  arsc.custno,
  '' as shipto,
  arsc.name,
  arsc.addr,
  arsc.addr3,
  arsc.city,
  arsc.countrycd,
  arsc.email,
  arsc.phoneno,
  arsc.state,
  arsc.faxphoneno,
  arsc.zipcd,
  arsc.statustype,
  0 as isshipto,
  arsc.custtype,
  arsc.shipviaty,
  arsc.salesterr as whse,
  case when arsc.taxablety = 'n' then 1 else 0 end as istaxexempt,
  arsc.user10 as specificwarehouse,
  arsc.slsrepout
from
  pub.arsc
where
  arsc.Cono = {CompanyNumber}
  {UpdatedSinceCondition}
  {CustomerNumberCondition}
WITH(NOLOCK)