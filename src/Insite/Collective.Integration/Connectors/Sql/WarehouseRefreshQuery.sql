﻿select
    icsd.whse,
    icsd.addr,
    icsd.city,
    icsd.state,
    icsd.zipcd,
    icsd.phoneno,
    icsd.faxphoneno,
    'WDP' as 'WarehouseStatus',
    icsd.user3 as 'WarehouseType',
    icsd.user4 as 'Name',
    icsd.user5 as 'Email',
    icsd.user10 as 'WarehouseCustomer',
    oedc.longitude,
    oedc.latitude
from pub.icsd icsd
    left join pub.oedc oedc ON oedc.key1 = whse
where icsd.cono = {CompanyNumber}
    and upper(icsd.user1) = 'WEB'
    and icsd.salesfl = 1
ORDER BY icsd.whse
WITH(NOLOCK)