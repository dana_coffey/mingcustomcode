﻿using System.Collections.Generic;
using System.IO;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;

namespace Collective.Integration.Connectors
{
    public class ProductFieldRefreshConnector
    {
        private readonly IntegrationProcessorContext _context;

        public ProductFieldRefreshConnector(IntegrationProcessorContext context)
        {
            _context = context;
        }

        public virtual List<FieldsModel> GetProductFields()
        {
            var filePath = IntegrationProcessorHelper.BuildJsonFilePath(Constants.ExportFieldsModelFileName);

            if (File.Exists(filePath))
            {
                return JsonHelper.FromFile<FieldsModel>(filePath);
            }

            _context.Logger.Info("There is no file to import");
            return new List<FieldsModel>();
        }
    }
}
