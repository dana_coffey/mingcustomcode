﻿using System.Collections.Generic;
using Collective.Core.Constant;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Connectors
{
    public class WarehouseRefreshConnector
    {
        public virtual List<WarehouseConnectorModel> GetWarehouses()
        {
            var tags = new Dictionary<string, string>
            {
                { "{CompanyNumber}", Constants.InforSxe.CompanyNumber.ToString() }
            };

            var query = QueryHelper.ReplaceTags(QueryHelper.GetQueryFromEmbeddedFile("WarehouseRefreshQuery.sql"), tags);
            return CommandHelper.ExecuteOdbc<WarehouseConnectorModel>(query, ConnectionHelper.GetInforSxeConnectionString());
        }
    }
}
