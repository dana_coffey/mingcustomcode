﻿using System.Collections.Generic;
using System.IO;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;

namespace Collective.Integration.Connectors
{
    public class CategoryRefreshConnector
    {
        private readonly IntegrationProcessorContext _context;

        public CategoryRefreshConnector(IntegrationProcessorContext context)
        {
            _context = context;
        }

        public virtual List<InsiteCategory> GetCategories()
        {
            var filePath = IntegrationProcessorHelper.BuildJsonFilePath(Constants.ExportCategoriesFileName);

            if (File.Exists(filePath))
            {
                return JsonHelper.FromFile<InsiteCategory>(filePath);
            }

            _context.Logger.Info("There is no file to import");
            return new List<InsiteCategory>();
        }
    }
}
