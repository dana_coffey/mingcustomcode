﻿using System;
using System.Collections.Generic;
using Collective.Core.Constant;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;
using Newtonsoft.Json;

namespace Collective.Integration.Connectors
{
    public class OrderHistoryRefreshConnector
    {
        public List<OrderHistoryConnectorModel> GetOrders(DateTime? updatedSince, DateTime? updatedTo, string erpOrderNumber, string erpOrderSuffix)
        {
            var erpOrderNumberSql = string.Empty;
            var erpOrderSuffixSql = string.Empty;
            var transactionDateSql = string.Empty;

            if (!string.IsNullOrEmpty(erpOrderNumber))
            {
                erpOrderNumberSql = $" and oeeh.orderno = '{erpOrderNumber}'";
                if (!string.IsNullOrEmpty(erpOrderSuffix))
                {
                    erpOrderSuffixSql = $" and oeeh.ordersuf = '{erpOrderSuffix}'";
                }
            }
            else if (updatedSince.HasValue)
            {
                transactionDateSql = $" and (oeeh.transdt >= '{DateTimeHelper.ToSqlFormat(updatedSince.Value)}' and oeeh.transdt <= '{DateTimeHelper.ToSqlFormat(updatedTo ?? DateTime.Today)}')";
            }

            var tags = new Dictionary<string, string>
            {
                { "{CompanyNumber}", Constants.InforSxe.CompanyNumber.ToString() },
                { "{TransactionDateWhereClause}", transactionDateSql },
                { "{ErpOrderNumberWhereClause}", erpOrderNumberSql },
                { "{ErpOrderSuffixWhereClause}", erpOrderSuffixSql }
            };

            var query = QueryHelper.ReplaceTags(QueryHelper.GetQueryFromEmbeddedFile("OrderHistoryRefreshQuery.sql"), tags);

            return OrderHistorySqlModel.ToConnectorModelList(CommandHelper.ExecuteOdbc<OrderHistorySqlModel>(query, ConnectionHelper.GetInforSxeConnectionString()));
        }
    }
}
