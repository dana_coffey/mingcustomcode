﻿using System.Collections.Generic;
using System.IO;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;

namespace Collective.Integration.Connectors
{
    public class AttributeRefreshConnector
    {
        private readonly IntegrationProcessorContext _context;

        public AttributeRefreshConnector(IntegrationProcessorContext context)
        {
            _context = context;
        }

        public virtual List<InsiteCvl> GetAttributes()
        {
            var filePath = IntegrationProcessorHelper.BuildJsonFilePath(Constants.ExportCvlFileName);

            if (File.Exists(filePath))
            {
                return JsonHelper.FromFile<InsiteCvl>(filePath);
            }

            _context.Logger.Info("There is no file to import");
            return new List<InsiteCvl>();
        }
    }
}
