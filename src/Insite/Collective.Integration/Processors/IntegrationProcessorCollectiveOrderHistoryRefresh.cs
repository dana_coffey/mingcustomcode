﻿using System;
using System.Data;
using Collective.Core.Constant;
using Collective.Integration.Connectors;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveOrderHistoryRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            try
            {
                var erpOrderNumber = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.ErpOrderNumber, string.Empty);
                var erpOrderSuffix = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.ErpOrderSuffix, string.Empty);
                var updatedSince = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.UpdatedSince, DateTime.Today.AddDays(Constants.Jobs.DefaultDaysBack));
                var updatedTo = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.UpdatedTo, DateTime.Today);

                var orderHistoryConnector = new OrderHistoryRefreshConnector();
                var orders = orderHistoryConnector.GetOrders(updatedSince, updatedTo, erpOrderNumber, erpOrderSuffix);

                var orderHistoryModelMapping = new OrderHistoryProcessorModelMapping();
                return IntegrationProcessorHelper.ToJsonDataSet(orderHistoryModelMapping.Map(orders), context);
            }
            catch (Exception ex)
            {
                context.Logger.Error($"An error occured while importing order history. ({ex.Message})");
            }

            return null;
        }
    }
}
