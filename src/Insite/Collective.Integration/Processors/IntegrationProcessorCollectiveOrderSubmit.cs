﻿using System.Data;
using System.Linq;
using Collective.Core.Injector;
using Collective.InforSxe.Core.Interfaces;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;
using Insite.Common.Helpers;
using Newtonsoft.Json;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveOrderSubmit : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            var initialDataset = XmlDatasetManager.ConvertXmlToDataset(context.IntegrationJob.InitialData);

            var orderSubmitRequestModel = new OrderSubmitRequestProcessorModelMapping().Map(initialDataset, context.JobStep.JobDefinitionStepParameters.ToList());
            context.Logger.Info("IntegrationProcessorCollectiveOrderSubmit Request: " + JsonConvert.SerializeObject(orderSubmitRequestModel));

            var orderSubmitResponseModel = CollectiveInjector.GetInstance<ICollectiveApiService>().SubmitOrder(orderSubmitRequestModel);
            context.Logger.Info("IntegrationProcessorCollectiveOrderSubmit Response: " + JsonConvert.SerializeObject(orderSubmitResponseModel));

            var dataset = new OrderSubmitResponseProcessorModelMapping().Map(orderSubmitResponseModel, context.IntegrationJob.JobNumber);

            return dataset;
        }
    }
}
