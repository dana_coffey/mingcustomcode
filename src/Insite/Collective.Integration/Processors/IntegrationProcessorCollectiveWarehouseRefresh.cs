﻿using System.Data;
using Collective.Core.Injector;
using Collective.Integration.Connectors;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveWarehouseRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            return IntegrationProcessorHelper.ToJsonDataSet(new WarehouseProcessorModelMapping().Map(new WarehouseRefreshConnector().GetWarehouses()), context);
        }
    }
}
