﻿using System.Data;
using Collective.Integration.Connectors;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveSalespersonRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            return IntegrationProcessorHelper.ToJsonDataSet(new SalespersonProcessorModelMapping().Map(new SalespersonRefreshConnector().GetSalespersons()), context);
        }
    }
}
