﻿using System.Data;
using Collective.Integration.Connectors;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveProductFieldRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            return IntegrationProcessorHelper.ToJsonDataSet(new ProductFieldProcessorModelMapping().Map(new ProductFieldRefreshConnector(context).GetProductFields()), context);
        }
    }
}
