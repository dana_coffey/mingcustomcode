﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Collective.Core.Constant;
using Collective.Core.Models;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;
using Collective.Integration.Core.Processors;
using Collective.Integration.Helpers;
using Newtonsoft.Json;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveResourceRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            var checksums = JsonConvert.DeserializeObject<List<ResourceProcessorModel>>(context.IntegrationJob.InitialData);
            var imageResources = GetModifiedImageResources(checksums);
            var otherResources = GetModifiedOtherResources(checksums);
            var uriBuilder = new UriBuilder(Uri.UriSchemeFtp, Constants.AppSettings.Connector.Ressource.Hostname, int.TryParse(Constants.AppSettings.Connector.Ressource.Port, out var port) ? port : 21);
            var destination = new Uri(uriBuilder.Uri, Constants.AppSettings.Connector.Ressource.Destination).ToString();
            var credentials = new NetworkCredential(Constants.AppSettings.Connector.Ressource.User, Constants.AppSettings.Connector.Ressource.Password);

            ResizeAndSendImagesToFtp(imageResources, destination, credentials);
            CopyResourcesToFtp(otherResources, destination, credentials);

            return IntegrationProcessorHelper.ToJsonDataSet(imageResources.Concat(otherResources).Select(p => new ResourceProcessorModel
            {
                Name = p.Name,
                Sha1Sum = p.Sha1Sum
            }).ToList(), context);
        }

        private static string ComputeResourceChecksum(HashAlgorithm algorithm, string filePath)
        {
            using (var fs = new FileStream(filePath, FileMode.Open))
            {
                var hash = algorithm.ComputeHash(fs);
                var formatted = new StringBuilder(2 * hash.Length);
                foreach (var b in hash)
                {
                    formatted.AppendFormat("{0:X2}", b);
                }

                return formatted.ToString();
            }
        }

        private static void CopyResourcesToFtp(List<ResourceConnectorModel> resources, string destinationHostFolder, NetworkCredential credential)
        {
            foreach (var resource in resources)
            {
                var request = (FtpWebRequest)WebRequest.Create(Path.Combine(destinationHostFolder, resource.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = credential;
                request.UseBinary = true;

                var fileContents = File.ReadAllBytes(resource.SourcePath);
                request.ContentLength = fileContents.Length;

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContents, 0, fileContents.Length);
                    requestStream.Close();
                }
            }
        }

        private static List<ResourceConnectorModel> GetModifiedImageResources(List<ResourceProcessorModel> checksums)
        {
            var path = Path.Combine(Constants.AppSettings.Connector.Inriver.FtpPath, Constants.AppSettings.Connector.Folder.Queue, Constants.AppSettings.Connector.Folder.Resources, Constants.AppSettings.Connector.Folder.ImageResources);
            return GetModifiedResources(path, checksums);
        }

        private static List<ResourceConnectorModel> GetModifiedOtherResources(List<ResourceProcessorModel> checksums)
        {
            var path = Path.Combine(Constants.AppSettings.Connector.Inriver.FtpPath, Constants.AppSettings.Connector.Folder.Queue, Constants.AppSettings.Connector.Folder.Resources, Constants.AppSettings.Connector.Folder.OtherResources);
            return GetModifiedResources(path, checksums);
        }

        private static List<ResourceConnectorModel> GetModifiedResources(string path, List<ResourceProcessorModel> checksums)
        {
            using (var sha1Provider = new SHA1CryptoServiceProvider())
            {
                if (Directory.Exists(path))
                {
                    return Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories).Select(p => new ResourceConnectorModel
                    {
                        Name = Path.GetFileName(p)?.ToLower(),
                        Sha1Sum = ComputeResourceChecksum(sha1Provider, p),
                        SourcePath = p
                    }).Where(p => IsModified(p, checksums)).ToList();
                }
            }

            return new List<ResourceConnectorModel>();
        }

        private static bool IsModified(ResourceConnectorModel file, List<ResourceProcessorModel> checksums)
        {
            var checksum = checksums.FirstOrDefault(p => p.Name == file.Name);
            if (checksum != null)
            {
                return checksum.Sha1Sum != file.Sha1Sum;
            }

            return true;
        }

        private static void ResizeAndSendImagesToFtp(List<ResourceConnectorModel> imageFiles, string destinationFolder, NetworkCredential credential)
        {
            foreach (var imageFile in imageFiles)
            {
                using (var image = ImageHelper.Load(imageFile.SourcePath))
                {
                    if (image != null)
                    {
                        ResizeAndSendImageToFtp(image, Constants.AppSettings.Image.Product.SizeLarge, Constants.AppSettings.Image.Quality, destinationFolder, ImageHelper.GetImageFileName(imageFile.Name, Constants.AppSettings.Image.SuffixLarge), credential);
                        ResizeAndSendImageToFtp(image, Constants.AppSettings.Image.Product.SizeMedium, Constants.AppSettings.Image.Quality, destinationFolder, ImageHelper.GetImageFileName(imageFile.Name, Constants.AppSettings.Image.SuffixMedium), credential);
                        ResizeAndSendImageToFtp(image, Constants.AppSettings.Image.Product.SizeSmall, Constants.AppSettings.Image.Quality, destinationFolder, ImageHelper.GetImageFileName(imageFile.Name, Constants.AppSettings.Image.SuffixSmall), credential);
                    }
                    else
                    {
                        CopyResourcesToFtp(new List<ResourceConnectorModel> { imageFile }, destinationFolder, credential);
                    }
                }
            }
        }

        private static void ResizeAndSendImageToFtp(Image image, int size, int quality, string folder, string fileName, NetworkCredential credential)
        {
            using (var resizedImage = ImageHelper.Resize(image, size, Color.White))
            {
                ImageHelper.SaveToFtp(resizedImage, folder, fileName, ImageFormat.Jpeg, quality, credential);
            }
        }
    }
}
