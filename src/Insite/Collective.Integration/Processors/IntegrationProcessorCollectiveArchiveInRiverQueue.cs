﻿using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Collective.Core.Constant;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Processors;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveArchiveInRiverQueue : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            var queueDirectoryPath = Path.Combine(Constants.AppSettings.Connector.Inriver.FtpPath, Constants.AppSettings.Connector.Folder.Queue);
            var archiveDirectoryPath = Path.Combine(Constants.AppSettings.Connector.Inriver.FtpPath, Constants.AppSettings.Connector.Folder.Archives);
            var zipPath = Path.Combine(archiveDirectoryPath, $"{DateTimeOffset.Now:yyyy-MM-ddTHH.mm.ss}.zip");

            Directory.CreateDirectory(archiveDirectoryPath);

            if (Directory.EnumerateFileSystemEntries(queueDirectoryPath).Any())
            {
                ZipFile.CreateFromDirectory(queueDirectoryPath, zipPath, CompressionLevel.Optimal, includeBaseDirectory: false);
                CleanDirectory(queueDirectoryPath);
                CleanArchives(archiveDirectoryPath);
            }

            return new DataSet();
        }

        private static void CleanArchives(string archiveDirectoryPath)
        {
            foreach (var filePath in Directory.EnumerateFiles(archiveDirectoryPath))
            {
                if (File.GetCreationTime(filePath) < DateTimeOffset.Now.Date.AddDays(Constants.AppSettings.Connector.Archive.ArchiveLifetime * -1))
                {
                    File.Delete(filePath);
                }
            }
        }

        private static void CleanDirectory(string directoryPath)
        {
            foreach (var directory in Directory.EnumerateDirectories(directoryPath))
            {
                Directory.Delete(directory, recursive: true);
            }

            foreach (var file in Directory.EnumerateFiles(directoryPath, "*", SearchOption.AllDirectories))
            {
                File.Delete(file);
            }
        }
    }
}
