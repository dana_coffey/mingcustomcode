﻿using System;
using System.Collections.Generic;
using System.Data;
using Collective.Core.Constant;
using Collective.Integration.Connectors;
using Collective.Integration.Core.Contexts;
using Collective.Integration.Core.Helpers;
using Collective.Integration.Core.Models;
using Collective.Integration.Core.Processors;
using Collective.Integration.Mappings;

namespace Collective.Integration.Processors
{
    public class IntegrationProcessorCollectiveCustomerRefresh : BaseIntegrationProcessor
    {
        protected override DataSet ExecuteJob(IntegrationProcessorContext context)
        {
            try
            {
                var updatedSince = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.UpdatedSince, DateTime.Today.AddDays(-1));
                var customerNumber = IntegrationJobHelper.GetParameterValue(context.IntegrationJob, Constants.Jobs.Parameters.CustomerNumber, string.Empty);

                var customerConnector = new CustomerRefreshConnector();
                List<CustomerConnectorModel> customers;
                switch (context.JobStep.Sequence)
                {
                    case Constants.Jobs.CustomerRefresh.Steps.CustomerBillToStepSequence:
                        context.Logger.Info("Getting bill to customers from Infor SX.e...");
                        customers = customerConnector.GetBillToCustomers(updatedSince, customerNumber);
                        break;

                    case Constants.Jobs.CustomerRefresh.Steps.CustomerShipToStepSequence:
                        context.Logger.Info("Getting ship to customers from Infor SX.e...");
                        customers = customerConnector.GetShipToCustomers(updatedSince, customerNumber);
                        break;

                    default:
                        throw new Exception($"Unknown Job Step ({context.JobStep.Sequence}-{context.JobStep.Name}).");
                }

                var customerModelMapping = new CustomerProcessorModelMapping();
                return IntegrationProcessorHelper.ToJsonDataSet(customerModelMapping.Map(customers), context);
            }
            catch (Exception ex)
            {
                context.Logger.Error($"An error occured while importing customers. ({ex.Message})");
            }

            return null;
        }
    }
}
