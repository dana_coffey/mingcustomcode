﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;

namespace Collective.Integration.Helpers
{
    public static class ImageHelper
    {
        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().FirstOrDefault(codec => codec.FormatID == format.Guid);
        }

        public static string GetImageFileName(string imagePath, string suffix)
        {
            return $"{Path.GetFileNameWithoutExtension(imagePath)}{suffix}{Path.GetExtension(imagePath)}";
        }

        public static Image Load(string file)
        {
            try
            {
                return Image.FromFile(file);
            }
            catch
            {
                return null;
            }
        }

        public static Image Resize(Image image, int size, Color fill)
        {
            var width = image.Width;
            var height = image.Height;
            if (Math.Max(image.Width, image.Height) > size)
            {
                if (image.Width > image.Height)
                {
                    width = size;
                    height = size * image.Height / image.Width;
                }
                else
                {
                    height = size;
                    width = size * image.Width / image.Height;
                }
            }

            var result = new Bitmap(width, height);
            using (var g = Graphics.FromImage(result))
            {
                g.Clear(fill);
                g.DrawImage(image, 0, 0, width, height);
            }

            return result;
        }

        public static void Save(Image image, string folder, string file, ImageFormat format, int quality)
        {
            FileSystemHelper.DeleteFile(Path.Combine(folder, file));
            FileSystemHelper.CreateDirectory(folder);

            image.Save(Path.Combine(folder, file), GetEncoder(format), new EncoderParameters(1)
            {
                Param =
                {
                    [0] = new EncoderParameter(Encoder.Quality, quality)
                }
            });
        }

        public static void SaveToFtp(Image image, string folder, string file, ImageFormat format, int quality, NetworkCredential credential)
        {
            var request = (FtpWebRequest)WebRequest.Create(Path.Combine(folder, file));
            request.Credentials = credential;
            request.Method = WebRequestMethods.Ftp.UploadFile;

            var requestStream = request.GetRequestStream();
            image.Save(requestStream, GetEncoder(format), new EncoderParameters(1) { Param = { [0] = new EncoderParameter(Encoder.Quality, quality) } });
            requestStream.Close();
        }
    }
}
