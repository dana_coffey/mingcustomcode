﻿using System.IO;

namespace Collective.Integration.Helpers
{
    public static class FileSystemHelper
    {
        public static void CreateDirectory(string folder)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        public static void DeleteFile(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public static string GetFolderStructure(string value, int folderCount, int folderNameLength, string illegalCharacterReplaceWith)
        {
            var folder = SafeFolderName(value, illegalCharacterReplaceWith);
            if (folder.Length < folderCount * folderNameLength)
            {
                folder = folder.PadRight(folderCount * folderNameLength, '_');
            }

            var structure = string.Empty;
            for (var i = 0; i < folderCount; i++)
            {
                structure = Path.Combine(structure, folder.Substring(i * folderNameLength, folderNameLength));
            }

            return structure;
        }

        public static string SafeFileName(string value, string replaceWith)
        {
            foreach (var illegal in new[] { "/", "?", "<", ">", "\\", ":", "*", "|", "\"" })
            {
                value = value.Replace(illegal, replaceWith);
            }

            return value;
        }

        public static string SafeFolderName(string value, string replaceWith)
        {
            foreach (var illegal in new[] { " ", "/", "?", "<", ">", "\\", ":", "*", "|", "\"" })
            {
                value = value.Replace(illegal, replaceWith);
            }

            return value;
        }
    }
}
