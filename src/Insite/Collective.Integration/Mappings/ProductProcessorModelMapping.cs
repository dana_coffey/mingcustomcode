﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Core.Injector;
using Collective.Core.Models;

namespace Collective.Integration.Mappings
{
    public class ProductProcessorModelMapping : ICollectiveInjectableClass
    {
        private const string TrimSpacesRegEx = "[ ]*/[ ]*";

        public virtual ProductProcessorModel Map(InsiteProduct source)
        {
            var name = source.Name ?? new Dictionary<string, string> { { Collective.Core.Constant.Constants.Culture.InvariantCulture, string.Empty } };
            var categories = source.Categories?.Select(x => Regex.Replace(x.Trim(), TrimSpacesRegEx, "/")).ToList();

            var customFields = new Dictionary<string, Dictionary<string, string>>();
            foreach (var field in source.CustomFields)
            {
                customFields.Add(field.Key, field.Value);
            }

            var links = source.Links.Select(link => new ProductLinkProcessorModel
            {
                ErpNumber = link.Sku,
                Type = link.Type
            }).GroupBy(x => new { x.Type, x.ErpNumber }).Select(x => x.First()).ToList();

            return new ProductProcessorModel
            {
                Categories = categories,
                CustomFields = customFields,
                Description = source.Description ?? name,
                MetaDescription = source.MetaDescription ?? name,
                ErpNumber = source.Sku,
                Images = source.Images,
                IsEnabled = source.IsOnline,
                Links = links,
                Name = name,
                Documents = source.Resources
            };
        }

        public virtual List<ProductProcessorModel> Map(List<InsiteProduct> sources)
        {
            return sources.Select(Map).ToList();
        }
    }
}
