﻿using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Core.Injector;
using Collective.Core.Models;

namespace Collective.Integration.Mappings
{
    public class AttributeProcessorModelMapping : ICollectiveInjectableClass
    {
        public virtual AttributeProcessorModel Map(InsiteCvl source)
        {
            return new AttributeProcessorModel
            {
                OptionIsEnabled = source.IsOnline,
                AttributeTypeName = source.CvlType,
                OptionDescription = source.Value,
                OptionName = source.Key
            };
        }

        public virtual List<AttributeProcessorModel> Map(List<InsiteCvl> sources)
        {
            return sources.Select(Map).ToList();
        }
    }
}
