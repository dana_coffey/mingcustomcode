﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Core.Injector;
using Collective.Core.Models;

namespace Collective.Integration.Mappings
{
    public class CategoryProcessorModelMapping : ICollectiveInjectableClass
    {
        private const char CategorySeparator = '/';
        private const string TrimSpacesRegEx = "[ ]*/[ ]*";

        public virtual CategoryProcessorModel Map(InsiteCategory source)
        {
            var name = (source.Name.Values.FirstOrDefault() ?? string.Empty).Trim();
            var parentPath = Regex.Replace(source.ParentPath, TrimSpacesRegEx, "/").Trim();

            return new CategoryProcessorModel
            {
                Images = source.Images,
                IsEnabled = source.IsOnline,
                Name = name,
                ShortDescription = source.Name ?? new Dictionary<string, string> { { Collective.Core.Constant.Constants.Culture.InvariantCulture, string.Empty } },
                ParentPath = parentPath,
                Path = string.IsNullOrEmpty(parentPath) ? name : string.Join(CategorySeparator.ToString(), parentPath, name),
                PreviousPath = Regex.Replace(source.PreviousPath, TrimSpacesRegEx, "/").Trim(),
                SortOrder = source.Position,
                WebsiteId = Guid.TryParse(source.WebsiteId, out var websiteId) ? websiteId : Guid.Empty,
            };
        }

        public virtual List<CategoryProcessorModel> Map(List<InsiteCategory> sources)
        {
            return sources.Select(Map).ToList();
        }
    }
}
