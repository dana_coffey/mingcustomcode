﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Models;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Mappings
{
    public class SalespersonProcessorModelMapping
    {
        public SalespersonProcessorModel Map(SalespersonConnectorModel source)
        {
            return new SalespersonProcessorModel
            {
                Email = source.Email,
                Name = source.Name,
                Number = source.SlsRep,
                PhoneNumber = source.PhoneNo
            };
        }

        public List<SalespersonProcessorModel> Map(List<SalespersonConnectorModel> sources)
        {
            return sources.Select(Map).ToList();
        }
    }
}
