﻿using System.Data;
using System.Linq;
using Collective.Core.Constant;
using Collective.InforSxe.Core.Models.OrderSubmit;
using Insite.WIS.Broker.Plugins.Constants;

namespace Collective.Integration.Mappings
{
    public class OrderSubmitResponseProcessorModelMapping
    {
        public DataSet Map(OrderSubmitResponseApiModel source, int jobReferenceNumber)
        {
            var orderSubmitDataTable = new DataTable(Data.OrderSubmitTable);
            orderSubmitDataTable.Columns.Add(Data.OrderNumberColumn);
            orderSubmitDataTable.Columns.Add(Data.ErpOrderNumberColumn);
            orderSubmitDataTable.Columns.Add(Constants.Jobs.OrderSubmit.DataSet.Columns.ApiErrorCode, typeof(int));
            orderSubmitDataTable.Columns.Add(Constants.Jobs.OrderSubmit.DataSet.Columns.ApiErrorMessage);
            orderSubmitDataTable.Columns.Add(Constants.Jobs.OrderSubmit.DataSet.Columns.JobReferenceNumber, typeof(int));
            orderSubmitDataTable.Rows.Add(source.WebOrderNumber, source.ErpOrderNumber, source.ErrorCode, source.ErrorMessage, jobReferenceNumber);

            var orderSubmitItemsDataTable = new DataTable(Constants.Jobs.OrderSubmit.DataSet.Tables.OrderSubmitItems);
            orderSubmitItemsDataTable.Columns.Add(Data.LineNumberColumn);
            orderSubmitItemsDataTable.Columns.Add(Data.ProductNumberColumn);
            orderSubmitItemsDataTable.Columns.Add(Data.QtyOrderedColumn);
            orderSubmitItemsDataTable.Columns.Add(Constants.Jobs.OrderSubmit.DataSet.Columns.QtyShipped);

            if (source.OrderLines?.Any() ?? false)
            {
                foreach (var item in source.OrderLines)
                {
                    orderSubmitItemsDataTable.Rows.Add(item.LineIdentifier, item.ErpNumber, item.QtyOrdered, item.QtyShipped);
                }
            }

            var dataSet = new DataSet();
            dataSet.Tables.Add(orderSubmitDataTable);
            dataSet.Tables.Add(orderSubmitItemsDataTable);

            return dataSet;
        }
    }
}
