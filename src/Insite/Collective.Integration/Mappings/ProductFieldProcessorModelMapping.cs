﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolunet.InRiver.Dequeue.Insite.Models;
using Collective.Core.Injector;
using Collective.Core.Models;

namespace Collective.Integration.Mappings
{
    public class ProductFieldProcessorModelMapping : ICollectiveInjectableClass
    {
        public ProductFieldProcessorModel Map(FieldsModel source)
        {
            var excludeFromSpecificationsSetting = source.Settings?.FirstOrDefault(x => x.Key == Collective.Core.Constant.Constants.ProductFields.Settings.ExcludedFromSpecifications);
            var unitOfMeasureSetting = source.Settings?.FirstOrDefault(x => x.Key == Collective.Core.Constant.Constants.ProductFields.Settings.UnitOfMeasure);

            return new ProductFieldProcessorModel
            {
                CategoryId = source.CategoryId,
                CategoryName = null,
                DataType = source.DataType,
                ExcludedFromSpecifications = Convert.ToBoolean(excludeFromSpecificationsSetting?.Value),
                FieldId = source.FieldTypeId,
                FieldCvlId = source.FieldCvlId,
                FieldName = source.FieldTypeName,
                UnitOfMeasure = unitOfMeasureSetting?.Value
            };
        }

        public List<ProductFieldProcessorModel> Map(List<FieldsModel> sources)
        {
            return sources.Select(Map).ToList();
        }
    }
}
