﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Collective.InforSxe.Core.Extensions;
using Collective.InforSxe.Core.Models.OrderSubmit;
using Insite.WIS.Broker.Plugins.Constants;
using Insite.WIS.Broker.WebIntegrationService;
using Newtonsoft.Json;

namespace Collective.Integration.Mappings
{
    public class OrderSubmitRequestProcessorModelMapping
    {
        public OrderSubmitRequestApiModel Map(DataSet source, List<JobDefinitionStepParameter> stepParameters)
        {
            return new OrderSubmitRequestApiModel { Customer = GetCustomer(source, stepParameters), CustomerOrder = GetCustomerOrder(source, stepParameters), OrderLine = GetOrderLines(source, stepParameters), ShipTo = GetShipTo(source, stepParameters) };
        }

        protected virtual OrderSubmitRequestCustomerApiModel GetCustomer(DataSet initialDataset, List<JobDefinitionStepParameter> stepParameters)
        {
            var customer = initialDataset.Tables[Data.CustomerTable].Rows[0];
            return new OrderSubmitRequestCustomerApiModel
            {
                CustomerSequence = customer.Table.Columns.Contains(Data.CustomerSequenceColumn) ? customer[Data.CustomerSequenceColumn].ToString() : string.Empty,
                CustomerNumber = customer.Table.Columns.Contains(Data.CustomerNumberColumn) ? customer[Data.CustomerNumberColumn].ToString() : string.Empty,
                CustomerEmail = customer.Table.Columns.Contains(Data.EmailColumn) ? customer[Data.EmailColumn].ToString() : string.Empty,
            };
        }

        protected virtual OrderSubmitRequestCustomerOrderApiModel GetCustomerOrder(DataSet initialDataset, List<JobDefinitionStepParameter> stepParameters)
        {
            var customerOrder = initialDataset.Tables[Data.CustomerOrderTable].Rows[0];
            var customerReference = initialDataset.Tables[Constants.CustomProperties.Cart.CustomerOrderPropertyTableName].AsEnumerable().FirstOrDefault(x => x[Data.NameColumn].ToString() == Constants.CustomProperties.Cart.CustomerReference);
            var shipVia = initialDataset.Tables["ShipVia"]?.Rows[0];

            return new OrderSubmitRequestCustomerOrderApiModel
            {
                DefaultWarehouseName = customerOrder[Data.DefaultWarehouseName].ToString(),
                ShippingAmount = customerOrder.Table.Columns.Contains(Data.ShippingColumn) ? (decimal)customerOrder[Data.ShippingColumn] : 0,
                Handling = customerOrder.Table.Columns.Contains(Data.HandlingColumn) ? (decimal)customerOrder[Data.HandlingColumn] : 0,
                DiscountAmount = customerOrder.Table.Columns.Contains(Data.DiscountAmountColumn) ? (decimal)customerOrder[Data.DiscountAmountColumn] : 0,
                CustomerPurchaseOrderNumber = customerOrder.Table.Columns.Contains(Data.CustomerPoColumn) ? customerOrder[Data.CustomerPoColumn].ToString() : string.Empty,
                Notes = customerOrder.Table.Columns.Contains(Data.NotesColumn) ? customerOrder[Data.NotesColumn].ToString() : string.Empty,
                Buyer = customerOrder.Table.Columns.Contains(Data.BuyerColumn) ? customerOrder[Data.BuyerColumn].ToString() : string.Empty,
                OrderNumber = customerOrder.Table.Columns.Contains(Data.OrderNumberColumn) ? customerOrder[Data.OrderNumberColumn].ToString() : string.Empty,
                CurrencyCode = customerOrder.Table.Columns.Contains(Data.CurrencyCodeColumn) ? customerOrder[Data.CurrencyCodeColumn].ToString() : string.Empty,
                TermsCode = customerOrder.Table.Columns.Contains(Data.TermsCodeColumn) ? customerOrder[Data.TermsCodeColumn].ToString() : string.Empty,
                ShipAddonCode = shipVia?.Table.Columns.Contains(Data.ErpShipCodeColumn) ?? false ? shipVia[Data.ErpShipCodeColumn].ToString() : string.Empty,
                ShipViaCode = shipVia?.Table.Columns.Contains(Constants.CustomProperties.ShipVia.ErpShipViaCode) ?? false ? shipVia[Constants.CustomProperties.ShipVia.ErpShipViaCode].ToString() : string.Empty,
                CustomerReference = customerReference?.Table.Columns.Contains(Data.ValueColumn) ?? false ? customerReference[Data.ValueColumn].ToString() : string.Empty,
                RequestedShipDate = GetFormattedRequestedShipDate(customerOrder.GetColumnStringValueOrEmpty("RequestedDeliveryDate")),
                OrderDisposition = GetOrderDisposition(initialDataset, shipVia)
            };
        }

        protected virtual List<OrderSubmitRequestOrderLineApiModel> GetOrderLines(DataSet initialDataset, List<JobDefinitionStepParameter> stepParameters)
        {
            var orderLines = initialDataset.Tables[Data.OrderLineTable].Rows;
            var products = GetProductsList(initialDataset, stepParameters);
            var orderLinesList = new List<OrderSubmitRequestOrderLineApiModel>();

            foreach (DataRow orderLine in orderLines)
            {
                orderLinesList.Add(
                    new OrderSubmitRequestOrderLineApiModel
                    {
                        LineNumber = orderLine.Table.Columns.Contains(Data.LineColumn) ? (int)orderLine[Data.LineColumn] : 0,
                        ProductDiscountPerEach = orderLine.Table.Columns.Contains(Data.ProductDiscountPerEachColumn) ? (decimal)orderLine[Data.ProductDiscountPerEachColumn] : 0,
                        ErpNumber = GetProductErpNumber(products, orderLine.Table.Columns.Contains(Data.ProductIdColumn) ? (Guid?)orderLine[Data.ProductIdColumn] : null),
                        Notes = orderLine.Table.Columns.Contains(Data.NotesColumn) ? orderLine[Data.NotesColumn].ToString() : string.Empty,
                        UnitOfMeasure = orderLine.Table.Columns.Contains(Data.UnitOfMeasureColumn) ? orderLine[Data.UnitOfMeasureColumn].ToString() : string.Empty,
                        UnitNetPrice = orderLine.Table.Columns.Contains(Data.UnitNetPriceColumn) ? (decimal)orderLine[Data.UnitNetPriceColumn] : 0,
                        QtyOrdered = orderLine.Table.Columns.Contains(Data.QtyOrderedColumn) ? (decimal)orderLine[Data.QtyOrderedColumn] : 0
                    });
            }

            if (initialDataset.Tables.Contains(Constants.Jobs.OrderSubmit.DataSet.Tables.SouthCarolinaSolidWasteTax))
            {
                var solidWasteTaxes = initialDataset.Tables[Constants.Jobs.OrderSubmit.DataSet.Tables.SouthCarolinaSolidWasteTax].Rows;
                foreach (DataRow solidWasteTax in solidWasteTaxes)
                {
                    orderLinesList.Add(
                        new OrderSubmitRequestOrderLineApiModel
                        {
                            LineNumber = orderLinesList.Count + 1,
                            ErpNumber = solidWasteTax.Table.Columns.Contains(Data.ErpNumberColumn) ? solidWasteTax[Data.ErpNumberColumn].ToString() : string.Empty,
                            UnitNetPrice = solidWasteTax.Table.Columns.Contains(Data.UnitNetPriceColumn) ? (decimal)solidWasteTax[Data.UnitNetPriceColumn] : 0,
                            QtyOrdered = solidWasteTax.Table.Columns.Contains(Data.QtyOrderedColumn) ? (decimal)solidWasteTax[Data.QtyOrderedColumn] : 0
                        });
                }
            }

            return orderLinesList;
        }

        protected virtual string GetProductErpNumber(List<OrderSubmitRequestProductApiModel> products, Guid? productId)
        {
            return productId == null ? string.Empty : products.FirstOrDefault(x => x.Id == productId)?.ErpNumber;
        }

        protected virtual List<OrderSubmitRequestProductApiModel> GetProductsList(DataSet initialDataset, List<JobDefinitionStepParameter> stepParameters)
        {
            var products = initialDataset.Tables[Data.ProductTable].Rows;
            var productsList = new List<OrderSubmitRequestProductApiModel>();

            foreach (DataRow product in products)
            {
                productsList.Add(new OrderSubmitRequestProductApiModel { Id = product.Table.Columns.Contains("Id") ? (Guid?)product["Id"] : null, ErpNumber = product.Table.Columns.Contains(Data.ErpNumberColumn) ? product[Data.ErpNumberColumn].ToString() : string.Empty });
            }

            return productsList;
        }

        protected virtual OrderSubmitRequestShipToApiModel GetShipTo(DataSet initialDataset, List<JobDefinitionStepParameter> stepParameters)
        {
            var shipTo = initialDataset.Tables[Data.ShipToTable].Rows[0];
            var location = GetLocation(initialDataset);

            var countryId = (Guid?)shipTo["CountryId"];
            var country = initialDataset.Tables["ShipToCountry"].AsEnumerable().FirstOrDefault(x => (Guid?)x["Id"] == countryId);
            var countryAbbr = location != null ? location.Country : country?["IsoCode2"].ToString();

            var stateId = (Guid?)shipTo["StateId"];
            var state = initialDataset.Tables[Data.ShipToStateTable].AsEnumerable().FirstOrDefault(x => (Guid?)x["Id"] == stateId);
            var stateAbbr = location != null ? location.State : state?[Data.StateAbbreviationColumn].ToString();

            var orderSubmitRequestShipToApiModel = new OrderSubmitRequestShipToApiModel
            {
                Address1 = location != null ? location.Address1 : (shipTo.Table.Columns.Contains(Data.Address1Column) ? shipTo[Data.Address1Column].ToString() : string.Empty),
                Address2 = location != null ? location.Address2 : (shipTo.Table.Columns.Contains(Data.Address2Column) ? shipTo[Data.Address2Column].ToString() : string.Empty),
                City = location != null ? location.City : (shipTo.Table.Columns.Contains(Data.CityColumn) ? shipTo[Data.CityColumn].ToString() : string.Empty),
                CompanyName = shipTo.Table.Columns.Contains(Data.CompanyNameColumn) ? shipTo[Data.CompanyNameColumn].ToString() : string.Empty,
                Country = countryAbbr ?? string.Empty,
                CustomerSequence = shipTo.Table.Columns.Contains(Data.CustomerSequenceColumn) ? shipTo[Data.CustomerSequenceColumn].ToString() : string.Empty,
                Phone = shipTo.Table.Columns.Contains(Data.PhoneColumn) ? shipTo[Data.PhoneColumn].ToString() : string.Empty,
                PostalCode = location != null ? location.PostalCode : (shipTo.Table.Columns.Contains(Data.PostalCodeColumn) ? shipTo[Data.PostalCodeColumn].ToString() : string.Empty),
                State = stateAbbr ?? string.Empty,
            };

            // Remove the custom customer sequence of the order, else the erp call will fail
            var customAddressPrefix = stepParameters.FirstOrDefault(x => x.Name == Core.Constant.Constants.JobParameters.OrderSubmit.ErpShipToPrefix)?.Value;
            if (customAddressPrefix != null && orderSubmitRequestShipToApiModel.CustomerSequence.StartsWith(customAddressPrefix))
            {
                orderSubmitRequestShipToApiModel.CustomerSequence = string.Empty;
            }

            return orderSubmitRequestShipToApiModel;
        }

        private string GetCustomProperty(DataSet initialDataset, string propertyName)
        {
            var propertyRow = initialDataset.Tables[Constants.CustomProperties.Cart.CustomerOrderPropertyTableName]
                .AsEnumerable()
                .FirstOrDefault(x => x[Data.NameColumn].ToString() == propertyName);

            if (propertyRow != null)
            {
                return propertyRow.Table.Columns.Contains(Data.ValueColumn) ? propertyRow[Data.ValueColumn].ToString() : string.Empty;
            }

            return string.Empty;
        }

        private OrderLocationModel GetLocation(DataSet initialDataset)
        {
            OrderLocationModel orderLocation = null;

            var locationRow = initialDataset.Tables[Constants.CustomProperties.Cart.CustomerOrderPropertyTableName].AsEnumerable().FirstOrDefault(x => x[Data.NameColumn].ToString() == Constants.CustomProperties.Cart.Location);

            if (locationRow != null)
            {
                var locationJson = locationRow.Table.Columns.Contains(Data.ValueColumn) ? locationRow[Data.ValueColumn].ToString() : string.Empty;
                orderLocation = JsonConvert.DeserializeObject<OrderLocationModel>(locationJson);
            }

            return orderLocation;
        }

        private string GetOrderDisposition(DataSet initialDataset, DataRow shipViaRow)
        {
            var shipViaCode = shipViaRow?.Table.Columns.Contains(Constants.CustomProperties.ShipVia.ErpShipViaCode) ?? false ? shipViaRow[Constants.CustomProperties.ShipVia.ErpShipViaCode].ToString() : string.Empty;

            // s = ship complete, w = will call
            if (!string.IsNullOrEmpty(shipViaCode))
            {
                if (shipViaCode.Equals("PKP", StringComparison.InvariantCultureIgnoreCase))
                {
                    return "w";
                }

                var propertyValue = GetCustomProperty(initialDataset, Constants.CustomProperties.Cart.ShipComplete);

                return propertyValue.Equals(bool.TrueString, StringComparison.InvariantCultureIgnoreCase) ? "s" : string.Empty;
            }

            return string.Empty;
        }

        private string GetFormattedRequestedShipDate(string requestedShipDate)
        {
            return DateTime.TryParse(requestedShipDate, out var requestedShipDateTime) ? requestedShipDateTime.ToString("MMddyy") : string.Empty;
        }
    }
}
