﻿using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Injector;
using Collective.Core.Models;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Mappings
{
    public class WarehouseProcessorModelMapping : ICollectiveInjectableClass
    {
        public WarehouseProcessorModel Map(WarehouseConnectorModel source)
        {
            return new WarehouseProcessorModel
            {
                Address = source.Addr,
                CanPickUp = GetCanPickUp(source.WarehouseStatus),
                CanShip = GetCanShip(source.WarehouseStatus),
                City = source.City,
                Email = source.Email,
                Latitude = source.Latitude,
                Longitude = source.Longitude,
                Name = source.Name,
                PhoneNumber = source.PhoneNo,
                State = source.State,
                WarehouseCustomer = source.WarehouseCustomer,
                WarehouseId = source.Whse,
                WarehouseType = source.WarehouseType,
                ZipCode = source.ZipCd,
            };
        }

        public List<WarehouseProcessorModel> Map(List<WarehouseConnectorModel> sources)
        {
            return sources.Select(Map).ToList();
        }

        private static bool GetCanPickUp(string status)
        {
            return !string.IsNullOrEmpty(status) && status.ToUpper().Contains(Constants.Warehouses.StatusFlags.CanPickUp);
        }

        private static bool GetCanShip(string status)
        {
            return !string.IsNullOrEmpty(status) && status.ToUpper().Contains(Constants.Warehouses.StatusFlags.CanShip);
        }
    }
}
