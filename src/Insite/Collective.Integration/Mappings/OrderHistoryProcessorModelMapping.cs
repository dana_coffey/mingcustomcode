﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Collective.Core.Models;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Mappings
{
    public class OrderHistoryProcessorModelMapping
    {
        public OrderHistoryProcessorModel Map(OrderHistoryConnectorModel source)
        {
            var orderHasBackOrder = OrderHasBackOrderItems(source);

            return new OrderHistoryProcessorModel
            {
                BillToAddress1 = source.Addr1,
                BillToAddress2 = source.Addr2,
                BillToCity = source.City,
                BillToCompanyName = source.Name,
                BillToPostalCode = source.ZipCd,
                BillToState = source.State,
                CompanyNumber = source.Cono,
                CustomerEmail = source.CustomerEmail,
                CustomerNumber = source.CustNo,
                CustomerPo = source.CustPo,
                CustomerReference = source.Refer,
                CustomerSequence = source.ShipTo,
                DiscountAmount = source.SpecDiscAmt,
                ErpOrderNumber = GetErpOrderNumber(source.OrderNo, source.OrderSuf),
                ErpOrderSuffix = GetErpOrderSuffix(source.OrderSuf),
                Lines = GetOrderLines(source.Lines, source.StageCd, orderHasBackOrder),
                OrderDate = GetOrderDate(source.EnterDt, source.EnterTm),
                OrderDisposition = source.OrderDisp,
                OrderStatus = GetOrderStatus(source.StageCd),
                OrderTotal = source.TotInvAmt,
                OtherCharges = GetOrderOtherCharges(source.AddOns, source.TransType),
                ProductTotal = GetProductTotal(source.StageCd, orderHasBackOrder, source.TransType, source.TotLineOrd, source.TotLineAmt),
                PromiseDate = source.PromiseDt,
                RequestedShipDate = source.ReqShipDt,
                SalesRepresentative = source.SlsRepOut,
                ShipCode = source.ShipViaTy,
                ShipDate = source.ShipDt,
                ShippingCharges = GetOrderShippingCharges(source.AddOns, source.TransType),
                ShipToAddress1 = source.ShipToAddr1,
                ShipToAddress2 = source.ShipToAddr2,
                ShipToCity = source.ShipToCity,
                ShipToCompanyName = source.ShipToNm,
                ShipToPostalCode = source.ShipToZip,
                ShipToState = source.ShipToSt,
                StageCode = source.StageCd,
                TaxAmount = source.TaxAmt,
                Terms = source.TermsType,
                Warehouse = source.Warehouse,
                WebOrderNumber = source.WebOrderNumber
            };
        }

        public List<OrderHistoryProcessorModel> Map(List<OrderHistoryConnectorModel> sources)
        {
            return sources.Select(Map).ToList();
        }

        private static string GetErpOrderNumber(int number, int suffix)
        {
            return $"{number:D8}-{GetErpOrderSuffix(suffix)}";
        }

        private static string GetErpOrderSuffix(int suffix)
        {
            return $"{suffix:D2}";
        }

        private static DateTime? GetOrderDate(DateTime? date, string time)
        {
            return date?.AddHours(ParseHelper.ParseInt(time?.Substring(0, 2), 0))
                .AddMinutes(ParseHelper.ParseInt(time?.Substring(2, 2), 0));
        }

        private static string GetOrderLineNote(string note)
        {
            return note.Replace(';', ' ').Trim();
        }

        private static string GetOrderLineProductDescription(string specNsType, string prodDesc, string prodDesc2, string descrip)
        {
            // "n" = the product description came from oeel table ( order line ), elsewhere it came from the icsp table ( product )
            return (specNsType == "n" ? $"{prodDesc} {prodDesc2}" : descrip).Replace(';', ' ').Trim();
        }

        private static List<OrderHistoryLineProcessorModel> GetOrderLines(List<OrderHistoryLineConnectorModel> models, int stageCd, bool boExistFl)
        {
            return models.Select(model => new OrderHistoryLineProcessorModel
            {
                LineNumber = model.LineNo,
                LineTotal = GetOrderLineTotal(model.NetAmt, model.NetOrd, stageCd, boExistFl),
                Notes = GetOrderLineNote(model.NoteLn),
                ProductDescription = GetOrderLineProductDescription(model.SpecNsType, model.ProdDesc, model.ProdDesc2, model.Descrip),
                ProductNumber = model.ShipProd,
                QtyOrdered = model.QtyOrd,
                QtyShipped = model.QtyShip,
                UnitOfMeasure = model.Unit,
                UnitPrice = model.Price,
                SerialNumbers = model.SerialNumbers.Select(MapSerialNumber).ToList(),
            }).ToList();
        }

        private static decimal GetOrderLineTotal(decimal netAmt, decimal netOrd, int stageCd, bool orderHasBackOrder)
        {
            return stageCd != Constants.InforSxe.Order.StageCode.Submitted || orderHasBackOrder ? netAmt : netOrd;
        }

        private static decimal GetOrderOtherCharges(List<OrderHistoryAddOnConnectorModel> models, string transationType)
        {
            return models.Sum(x => (x.SequenceNo != Constants.InforSxe.Order.AddonSequenceNo.ShippingAndHandling ? x.AddonNet : 0) * (transationType.Equals(Constants.InforSxe.Order.TransactionType.Return, StringComparison.OrdinalIgnoreCase) ? -1 : 1));
        }

        private static decimal GetOrderShippingCharges(List<OrderHistoryAddOnConnectorModel> models, string transationType)
        {
            return models.Sum(x => (x.SequenceNo == Constants.InforSxe.Order.AddonSequenceNo.ShippingAndHandling ? x.AddonNet : 0) * (transationType.Equals(Constants.InforSxe.Order.TransactionType.Return, StringComparison.OrdinalIgnoreCase) ? -1 : 1));
        }

        private static string GetOrderStatus(int stageCd)
        {
            switch (stageCd)
            {
                case Constants.InforSxe.Order.StageCode.Quoted:
                    return Constants.Insite.Order.Status.Quoted;

                case Constants.InforSxe.Order.StageCode.Submitted:
                    return Constants.Insite.Order.Status.Submitted;

                case Constants.InforSxe.Order.StageCode.Processing2:
                case Constants.InforSxe.Order.StageCode.Processing3:
                    return Constants.Insite.Order.Status.Processing;

                case Constants.InforSxe.Order.StageCode.Processing4:
                    return Constants.Insite.Order.Status.Invoiced;

                case Constants.InforSxe.Order.StageCode.Complete:
                    return Constants.Insite.Order.Status.Complete;

                case Constants.InforSxe.Order.StageCode.Cancelled:
                    return Constants.Insite.Order.Status.Cancelled;
            }

            return string.Empty;
        }

        private static decimal GetProductTotal(int stageCd, bool orderHasBackOrder, string transType, decimal totLineOrd, decimal totLineAmt)
        {
            var productTotal = stageCd != Constants.InforSxe.Order.StageCode.Submitted || orderHasBackOrder ? totLineAmt : totLineOrd;

            if (transType.Equals(Constants.InforSxe.Order.TransactionType.Return, StringComparison.OrdinalIgnoreCase))
            {
                productTotal *= -1;
            }

            return productTotal;
        }

        private static SerialNumberProcessorModel MapSerialNumber(OrderHistoryLineSerialNumbersConnectorModel serialNumber)
        {
            return new SerialNumberProcessorModel
            {
                SeqNo = serialNumber.SeqNo,
                SerialNo = serialNumber.SerialNo,
            };
        }

        private static bool OrderHasBackOrderItems(OrderHistoryConnectorModel order)
        {
            return (order.Lines.FirstOrDefault(x => x.QtyShip < x.QtyOrd) != null) || order.BoExistsFl;
        }
    }
}
