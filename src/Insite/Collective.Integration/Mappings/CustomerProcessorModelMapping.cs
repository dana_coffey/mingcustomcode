﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Models;
using Collective.Integration.Core.Models;

namespace Collective.Integration.Mappings
{
    public class CustomerProcessorModelMapping
    {
        public CustomerProcessorModel Map(CustomerConnectorModel source)
        {
            return new CustomerProcessorModel
            {
                Active = source.StatusType,
                Address1 = GetAddressSplit(0, source.Addr),
                Address2 = GetAddressSplit(1, source.Addr),
                Address3 = source.Addr3,
                City = source.City,
                CompanyName = source.Name,
                Country = !string.IsNullOrEmpty(source.CountryCd) ? source.CountryCd.ToUpper() : Constants.InforSxe.DefaultCountryIsoCode,
                CustomerType = source.CustType,
                Email = source.Email,
                IsShipTo = source.IsShipTo,
                IsTaxExempt = source.IsTaxExempt,
                Number = source.CustNo,
                Phone = source.PhoneNo,
                PostalCode = source.ZipCd,
                SalespersonNumber = source.SlsRepOut.SafeTrim(),
                ShipTo = source.ShipTo.ToUpper(),
                SpecificWarehouse = source.SpecificWarehouse,
                State = source.State.ToUpper(),
                Warehouse = source.Whse
            };
        }

        public List<CustomerProcessorModel> Map(List<CustomerConnectorModel> sources)
        {
            return sources.Select(Map).ToList();
        }

        private string GetAddressSplit(int index, string address)
        {
            if (!string.IsNullOrEmpty(address))
            {
                var split = address.Split(';');

                if (split.Length >= index)
                {
                    return split[index];
                }
            }

            return string.Empty;
        }
    }
}
