﻿using System.Linq;
using System.Net.Http;
using Insite.Catalog.Services.Results;
using Insite.Catalog.WebApi.V1.ApiModels;
using Insite.Catalog.WebApi.V1.Mappers;
using Insite.Core.Plugins.Catalog;
using Insite.Core.Plugins.Utilities;
using Insite.Core.SystemSetting;
using Insite.Core.Translation;
using Insite.Core.WebApi.Interfaces;
using Insite.WebFramework.SystemSettings;

namespace Collective.Web.Extension.Core.Mappers
{
    public class CollectiveGetCatalogPageMapper : GetCatalogPageMapper
    {
        public CollectiveGetCatalogPageMapper(IObjectToObjectMapper objectToObjectMapper, IUrlHelper urlHelper, IEntityTranslationService entityTranslationService, ICatalogPathBuilder catalogPathBuilder) : base(objectToObjectMapper, urlHelper, entityTranslationService, catalogPathBuilder)
        {
        }

        public override CatalogPageModel MapResult(GetCategoryByPathResult getCategoryResult, HttpRequestMessage request)
        {
            var result = base.MapResult(getCategoryResult, request);

            var rootCategory = result?.BreadCrumbs?.FirstOrDefault(x => !string.IsNullOrEmpty(x.CategoryId));
            if (rootCategory != null && rootCategory != result.BreadCrumbs?.LastOrDefault())
            {
                var pageTitleSettings = SettingsGroupProvider.Current.Get<PageTitleSettings>();
                result.Title += $" {pageTitleSettings.Delimiter} {rootCategory.Text}";
            }

            return result;
        }
    }
}
