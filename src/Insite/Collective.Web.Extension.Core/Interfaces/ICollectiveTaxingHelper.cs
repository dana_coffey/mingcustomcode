﻿using System;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveTaxingHelper
    {
        bool IsCustomerOrderTaxable(CustomerOrder customerOrder, string pickupShipCode, bool defaultValue);
        bool IsCustomerTaxable(string customerNumber, string stateCode, Guid? countryId);
    }
}
