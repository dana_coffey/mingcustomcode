﻿using System;
using System.Collections.Generic;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.Models.Product;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveProductService
    {
        List<ProductCustomFieldModel> GetCustomFields(Guid productId);

        List<ProductScanBarcodeModel> GetProductsByBarcode(string barcode);

        RelatedProductCollectionModel GetRelatedProducts(Guid productId, string type);

        List<ProductReplacedModel> GetReplacementProducts(string erpNumber);

        List<ProductLinkEntity> GetSupersedeProducts(List<string> erpNumbers);
    }
}
