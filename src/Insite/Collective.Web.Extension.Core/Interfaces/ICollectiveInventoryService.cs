﻿using Collective.Web.Extension.Core.Models.Inventory;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveInventoryService : IDependency
    {
        Warehouse GetDefaultWarehouse(Customer currentCustomer);

        AvailabilityStatusModel GetProductInventoryStatusModel(string productErpNumber, bool isCatalogProduct);

        AvailabilityStatusModel GetProductInventoryStatusModel(string productErpNumber, bool isCatalogProduct, string warehouse);
    }
}
