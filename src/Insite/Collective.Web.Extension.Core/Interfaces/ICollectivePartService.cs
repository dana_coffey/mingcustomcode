﻿using System;
using Collective.Web.Extension.Core.Models.Product.PartsList;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectivePartService
    {
        PartsListModel GetPartsList(Guid productId);
    }
}
