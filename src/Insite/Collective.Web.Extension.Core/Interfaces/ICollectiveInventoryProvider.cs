﻿using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveInventoryProvider : IDependency
    {
        decimal GetQtyOnHand(Product product, string warehouseName);
    }
}
