﻿using Insite.Core.Interfaces.Dependency;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveOrderConfirmationService : IDependency
    {
        void SendConfirmationEmailByErpOrderNumber(string erpOrderNumber);

        void SendConfirmationEmailByWebOrderNumber(string webOrderNumber);
    }
}
