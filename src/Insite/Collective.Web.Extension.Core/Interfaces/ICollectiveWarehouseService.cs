﻿using System;
using System.Collections.Generic;
using Collective.Web.Extension.Core.Models.Warehouse;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;
using Insite.Data.Entities.Dtos;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveWarehouseService : IDependency
    {
        List<WarehouseModel> GetAvailableWarehouses(string customerSpecificWarehouse);

        List<WarehouseModel> GetPickUpWarehouses();

        List<WarehouseModel> GetShipFromWarehouses();

        WarehouseModel GetWarehouse(Guid warehouseId);

        WarehouseModel GetWarehouse(string warehouseName);

        WarehouseDto GetWarehouseDto(Guid warehouseId);

        WarehouseModel GetCustomerWarehouse(Customer customer);
    }
}
