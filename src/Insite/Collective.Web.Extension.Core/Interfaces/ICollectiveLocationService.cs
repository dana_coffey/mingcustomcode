﻿using System;
using Collective.Web.Extension.Core.Models.Location;
using Insite.Core.Interfaces.Dependency;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Interfaces
{
    public interface ICollectiveLocationService : IDependency
    {
        LocationModel Get(Guid? userId, Customer shipTo, bool createLocation = true);

        void Reset(Guid? userId);

        void Update(Guid? userId, LocationModel location);
    }
}
