﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.Entities;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts
{
    public static class PostStartupScriptsRunner
    {
        private static readonly List<string> PostStartupScripts = new List<string>();
        private static bool isRegisteryDone;

        public static void RunAllScripts<T>() where T : BasePostStartupScript
        {
            try
            {
                var executeSave = false;
                var unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
                var postStartupScriptRepository = unitOfWork.GetRepository<PostStartupScript>();
                var scripts = CollectiveInjector.GetInstances<T>(unitOfWork).OrderBy(p => p.ScriptOrderDate);

                foreach (var script in scripts.Where(s => !GetPostStartupScripts(postStartupScriptRepository).Contains(s.GetType().FullName)))
                {
                    script.Run();

                    var postStartupScript = new PostStartupScript
                    {
                        ScriptName = script.GetType().FullName,
                        Id = Guid.NewGuid(),
                        AppliedOn = DateTimeOffset.Now,
                        CreatedOn = DateTimeOffset.Now,
                        ModifiedOn = DateTimeOffset.Now,
                        ScriptOrderDate = script.ScriptOrderDate
                    };

                    PostStartupScripts.Add(postStartupScript.ScriptName);
                    postStartupScriptRepository.Insert(postStartupScript);

                    executeSave = true;
                }

                if (executeSave)
                {
                    unitOfWork.Save();
                }
            }
            catch (Exception)
            {
                // Kill false db context exception
            }
        }

        private static List<string> GetPostStartupScripts(IRepository<PostStartupScript> postStartupScriptRepository)
        {
            if (!isRegisteryDone)
            {
                PostStartupScripts.AddRange(postStartupScriptRepository.GetTableAsNoTracking().Select(p => p.ScriptName).ToList());
                isRegisteryDone = true;
            }

            return PostStartupScripts;
        }
    }
}
