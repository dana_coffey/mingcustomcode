﻿using System;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.WidgetDefault
{
    public class ProductWarrantyLookupPostStartupScript : BaseContentItemTemplateScript
    {
        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2019, 03, 15, 10, 30, 00));

        public ProductWarrantyLookupPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Run()
        {
            SetIsDefault("ProductWarrantyLookup");
        }
    }
}
