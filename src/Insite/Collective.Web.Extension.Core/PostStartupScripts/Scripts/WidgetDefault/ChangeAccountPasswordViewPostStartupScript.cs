﻿using System;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.WidgetDefault
{
    public class ChangeAccountPasswordViewPostStartupScript : BaseContentItemTemplateScript, ICollectiveInjectableClass
    {
        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 18, 11, 00, 00));

        public ChangeAccountPasswordViewPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Run()
        {
            SetIsDefault("ChangeAccountPasswordView");
        }
    }
}
