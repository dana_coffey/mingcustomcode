﻿using System;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.WidgetDefault
{
    public class SignInViewPostStartupScript : BaseContentItemTemplateScript, ICollectiveInjectableClass
    {
        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 05, 31, 15, 06, 42));

        public SignInViewPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Run()
        {
            SetIsDefault("SignInView");
        }
    }
}
