﻿using System;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.WidgetDefault
{
    public class DealerViewPostStartupScript : BaseContentItemTemplateScript, ICollectiveInjectableClass
    {
        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 10, 23, 10, 05, 05));

        public DealerViewPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Run()
        {
            SetIsDefault("DealerView");
        }
    }
}
