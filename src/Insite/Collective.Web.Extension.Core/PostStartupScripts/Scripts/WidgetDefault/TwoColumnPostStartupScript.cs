﻿using System;
using Collective.Core.Injector;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.WidgetDefault
{
    public class TwoColumnPostStartupScript : BaseContentItemTemplateScript, ICollectiveInjectableClass
    {
        public override DateTimeOffset ScriptOrderDate => new DateTimeOffset(new DateTime(2017, 09, 21, 16, 46, 42));

        public TwoColumnPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void Run()
        {
            SetIsDefault("TwoColumn");
        }
    }
}
