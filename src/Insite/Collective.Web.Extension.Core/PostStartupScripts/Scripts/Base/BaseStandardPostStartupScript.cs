﻿using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base
{
    public abstract class BaseStandardPostStartupScript : BasePostStartupScript
    {
        protected BaseStandardPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
