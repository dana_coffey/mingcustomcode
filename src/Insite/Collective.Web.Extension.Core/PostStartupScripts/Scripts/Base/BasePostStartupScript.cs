﻿using System;
using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base
{
    public abstract class BasePostStartupScript
    {
        public abstract DateTimeOffset ScriptOrderDate { get; }
        protected IUnitOfWork UnitOfWork { get; }

        protected BasePostStartupScript(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public abstract void Run();
    }
}
