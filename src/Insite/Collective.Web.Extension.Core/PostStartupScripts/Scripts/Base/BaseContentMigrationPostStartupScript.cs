﻿using Insite.Core.Interfaces.Data;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base
{
    public abstract class BaseContentMigrationPostStartupScript : BasePostStartupScript
    {
        protected BaseContentMigrationPostStartupScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
