﻿using System;
using System.Linq;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base
{
    public abstract class BaseContentItemTemplateScript : BaseStandardPostStartupScript
    {
        private readonly IRepository<ContentItemTemplate> _contentItemTemplateRepository;

        protected BaseContentItemTemplateScript(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _contentItemTemplateRepository = UnitOfWork.GetRepository<ContentItemTemplate>();
        }

        protected void SetIsDefault(string contentItemClass)
        {
            var contentItems = _contentItemTemplateRepository.GetTable().Where(p => p.ContentItemClass == contentItemClass).ToList();

            foreach (var contentItem in contentItems)
            {
                contentItem.IsDefault = contentItem.Group.Equals("Store", StringComparison.InvariantCultureIgnoreCase);
            }

            UnitOfWork.Save();
        }
    }
}
