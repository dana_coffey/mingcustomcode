﻿using System.ComponentModel.DataAnnotations.Schema;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    [Table("ItemCache")]
    public class ItemCache : EntityBase
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
