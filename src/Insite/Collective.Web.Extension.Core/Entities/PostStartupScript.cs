﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    [Table("PostStartupScript")]
    public class PostStartupScript : EntityBase
    {
        public DateTimeOffset AppliedOn { get; set; }
        public string ScriptName { get; set; }
        public DateTimeOffset ScriptOrderDate { get; set; }
    }
}
