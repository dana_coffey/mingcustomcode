﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    [Table("OrderHistoryLineSerialNumber")]
    public class OrderHistoryLineSerialNumber : EntityBase
    {
        public Guid OrderHistoryLineId { get; set; }
        public string SerialNo { get; set; }
        public int SeqNo { get; set; }
    }
}
