﻿using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    public class ProductLinkEntity : EntityBase
    {
        public string ErpNumber { get; set; }
        public string LinkTo { get; set; }
        public int? Order { get; set; }
        public string Type { get; set; }
    }
}
