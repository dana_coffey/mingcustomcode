﻿using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    public class ResourceChecksum : EntityBase
    {
        public string Name { get; set; }
        public string Sha1Sum { get; set; }
    }
}
