﻿using System;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    public class ProductBarcode : EntityBase
    {
        public string Barcode { get; set; }
        public virtual Product Product { get; set; }
        public Guid ProductId { get; set; }
    }
}
