﻿using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Entities
{
    public class ProductFieldEntity : EntityBase
    {
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string DataType { get; set; }
        public bool ExcludedFromSpecifications { get; set; }
        public string FieldCvlId { get; set; }
        public string FieldId { get; set; }
        public string FieldName { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}
