﻿using Insite.Data.Providers.EntityFramework.EntityMappings;

namespace Collective.Web.Extension.Core.Entities.Configurations
{
    public class ResourceChecksumMapping : EntityBaseTypeConfiguration<ResourceChecksum>
    {
        public ResourceChecksumMapping()
        {
            ToTable(nameof(ResourceChecksum));

            Property(x => x.Name).IsRequired().HasMaxLength(512);
            Property(x => x.Sha1Sum).IsRequired().HasMaxLength(40);
        }
    }
}
