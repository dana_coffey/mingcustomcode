﻿using Insite.Data.Providers.EntityFramework.EntityMappings;

namespace Collective.Web.Extension.Core.Entities.Configurations
{
    public class ProductBarcodeMapping : EntityBaseTypeConfiguration<ProductBarcode>
    {
        public ProductBarcodeMapping()
        {
            ToTable(nameof(ProductBarcode));

            Property(x => x.ProductId).IsRequired();
            Property(x => x.Barcode).IsRequired().HasMaxLength(100);
            HasRequired(x => x.Product);
        }
    }
}
