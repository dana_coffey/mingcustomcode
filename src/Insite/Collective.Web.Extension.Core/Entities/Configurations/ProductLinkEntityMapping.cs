﻿using Insite.Data.Providers.EntityFramework.EntityMappings;

namespace Collective.Web.Extension.Core.Entities.Configurations
{
    public class ProductLinkEntityMapping : EntityBaseTypeConfiguration<ProductLinkEntity>
    {
        public ProductLinkEntityMapping()
        {
            ToTable("ProductLink");

            Property(x => x.ErpNumber).IsRequired().HasMaxLength(50);
            Property(x => x.LinkTo).IsRequired().HasMaxLength(50);
            Property(x => x.Type).IsRequired().HasMaxLength(50);
        }
    }
}
