﻿using Insite.Data.Providers.EntityFramework.EntityMappings;

namespace Collective.Web.Extension.Core.Entities.Configurations
{
    public class PostStartupScriptMapping : EntityBaseTypeConfiguration<PostStartupScript>
    {
    }
}
