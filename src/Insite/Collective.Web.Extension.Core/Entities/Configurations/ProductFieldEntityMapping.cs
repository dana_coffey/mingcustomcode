﻿using Insite.Data.Providers.EntityFramework.EntityMappings;

namespace Collective.Web.Extension.Core.Entities.Configurations
{
    public class ProductFieldEntityMapping : EntityBaseTypeConfiguration<ProductFieldEntity>
    {
        public ProductFieldEntityMapping()
        {
            ToTable("ProductField");

            Property(x => x.CategoryId).IsRequired().HasMaxLength(128);
            Property(x => x.CategoryName).HasMaxLength(null);
            Property(x => x.DataType).IsRequired().HasMaxLength(128);
            Property(x => x.FieldId).IsRequired().HasMaxLength(128);
            Property(x => x.FieldName).HasMaxLength(null);
            Property(x => x.FieldCvlId).HasMaxLength(128);
            Property(x => x.UnitOfMeasure).HasMaxLength(128);
        }
    }
}
