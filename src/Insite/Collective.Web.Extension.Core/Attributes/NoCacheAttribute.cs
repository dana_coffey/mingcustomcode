﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace Collective.Web.Extension.Core.Attributes
{
    public class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            context.Response.Headers.CacheControl = context.Response.Headers.CacheControl ?? new CacheControlHeaderValue();
            context.Response.Headers.CacheControl.NoCache = true;
            context.Response.Headers.CacheControl.NoStore = true;
            context.Response.Headers.CacheControl.MustRevalidate = true;

            if (context.Response.Content != null)
            {
                context.Response.Content.Headers.Expires = DateTimeOffset.Now.AddHours(-12);
            }

            context.Response.Headers.Add("pragma", "no-cache");
        }
    }
}
