﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Web.Hosting;
using Collective.Web.Extension.Core.Models.Scheduler;
using FluentScheduler;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.Enums;

namespace Collective.Web.Extension.Core.Jobs
{
    public class SchedulerJob : IJob, IRegisteredObject
    {
        private static readonly ConcurrentQueue<JobModel> _jobs = new ConcurrentQueue<JobModel>();

        private static readonly List<string> ErrorIntegrationJobStatuses = new List<string>
        {
            IntegrationJobStatus.CancelRequested.ToString(),
            IntegrationJobStatus.Canceled.ToString(),
            IntegrationJobStatus.CompletedWithErrors.ToString(),
            IntegrationJobStatus.Failure.ToString()
        };

        private static readonly List<string> SuccessIntegrationJobStatuses = new List<string>
        {
            IntegrationJobStatus.Success.ToString(),
            IntegrationJobStatus.CompletedWithWarnings.ToString()
        };

        private static JobModel currentJob;
        private readonly IRepository<IntegrationJob> _integrationJobRepository;
        private readonly object _lock = new object();
        private readonly IUnitOfWork _unitOfWork;
        private bool _shuttingDown;

        public SchedulerJob()
        {
            // Register this job with the hosting environment.
            // Allows for a more graceful stop of the job, in the case of IIS shutting down.
            HostingEnvironment.RegisterObject(this);

            _unitOfWork = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _integrationJobRepository = _unitOfWork.GetRepository<IntegrationJob>();
        }

        public static bool CanScheduleJobs()
        {
            return currentJob == null || currentJob.CanScheduleNewJobDuringExecution;
        }

        public static bool ScheduleJobs(List<JobModel> jobs)
        {
            if (CanScheduleJobs())
            {
                jobs.ForEach(_jobs.Enqueue);
                return true;
            }

            return false;
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }

                ProcessCurrentState();
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }

            HostingEnvironment.UnregisterObject(this);
        }

        private void CancelQueue(Guid queueId)
        {
            JobModel job;
            while (_jobs.TryPeek(out job) && job.QueueId == queueId)
            {
                _jobs.TryDequeue(out job);
            }
        }

        private void ProcessCurrentState()
        {
            if (currentJob != null)
            {
                var currentIntegrationJob = _integrationJobRepository.Get(currentJob.Job.Id);

                if (currentIntegrationJob == null || ErrorIntegrationJobStatuses.Contains(currentIntegrationJob.Status))
                {
                    if (currentJob.CancelQueueOnError)
                    {
                        CancelQueue(currentJob.QueueId);
                        currentJob = null;
                    }
                    else
                    {
                        currentJob = null;
                        StartNextJob();
                    }
                }
                else if (SuccessIntegrationJobStatuses.Contains(currentIntegrationJob.Status))
                {
                    StartNextJob();
                }
            }
            else
            {
                StartNextJob();
            }
        }

        private void StartNextJob()
        {
            JobModel nextJob;
            if (_jobs.TryDequeue(out nextJob))
            {
                _integrationJobRepository.Insert(nextJob.Job);
                _unitOfWork.Save();
                currentJob = nextJob;
            }
        }
    }
}
