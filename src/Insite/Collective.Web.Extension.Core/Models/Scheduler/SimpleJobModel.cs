﻿namespace Collective.Web.Extension.Core.Models.Scheduler
{
    public class SimpleJobModel
    {
        public bool CancelQueueOnError { get; set; }
        public bool CanScheduleNewJobDuringExecution { get; set; }
        public string IntegrationJobName { get; set; }
    }
}
