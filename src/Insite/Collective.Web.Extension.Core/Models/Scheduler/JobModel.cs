﻿using System;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Models.Scheduler
{
    public class JobModel
    {
        public bool CancelQueueOnError { get; set; }
        public bool CanScheduleNewJobDuringExecution { get; set; }
        public IntegrationJob Job { get; set; }
        public Guid QueueId { get; set; }
    }
}
