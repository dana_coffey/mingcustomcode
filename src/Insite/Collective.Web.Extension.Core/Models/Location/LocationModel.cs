﻿using System;
using Collective.Web.Extension.Core.Models.Warehouse;
using Insite.Websites.WebApi.V1.ApiModels;

namespace Collective.Web.Extension.Core.Models.Location
{
    public class LocationModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public CountryModel Country { get; set; }
        public bool InitialLocation { get; set; }
        public Guid Id { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }
        public StateModel State { get; set; }
        public WarehouseModel Warehouse { get; set; }
    }
}
