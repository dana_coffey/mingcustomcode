﻿using Insite.Core.Services;

namespace Collective.Web.Extension.Core.Models.Settings
{
    public class CollectiveSettingsModel : ResultBase
    {
        public string PaymentGatewayUrl { get; set; }
        public bool ShowAvailabilityByWarehouse { get; set; }
        public bool ShowTermsAndConditions { get; set; }
        public bool UsePostProcessorBatchSaving { get; set; }
    }
}
