﻿namespace Collective.Web.Extension.Core.Models.Order
{
    public class OrderWarehouseModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string DisplayName { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
    }
}
