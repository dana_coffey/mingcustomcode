﻿using Insite.Core.WebApi;

namespace Collective.Web.Extension.Core.Models.Warehouse
{
    public class WarehouseModel : BaseModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public string Id { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
    }
}
