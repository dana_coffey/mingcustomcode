﻿namespace Collective.Web.Extension.Core.Models.Product
{
    public class ProductScanBarcodeModel
    {
        public string ProductNumber { get; set; }
        public string Url { get; set; }
    }
}
