﻿using System.Collections.Generic;
using Insite.Catalog.Services.Dtos;
using Insite.Core.WebApi;

namespace Collective.Web.Extension.Core.Models.Product
{
    public class RelatedProductCollectionModel : BaseModel
    {
        public ICollection<ProductDto> Products { get; set; }
    }
}
