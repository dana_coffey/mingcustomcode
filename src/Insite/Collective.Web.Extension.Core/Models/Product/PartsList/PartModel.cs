﻿namespace Collective.Web.Extension.Core.Models.Product.PartsList
{
    public class PartModel
    {
        public bool HasHistory { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public int Quantity { get; set; }
        public string Url { get; set; }
    }
}
