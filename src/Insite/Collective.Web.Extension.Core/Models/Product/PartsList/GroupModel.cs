﻿using System.Collections.Generic;

namespace Collective.Web.Extension.Core.Models.Product.PartsList
{
    public class GroupModel
    {
        public string Name { get; set; }
        public List<PartModel> Parts { get; set; }
    }
}
