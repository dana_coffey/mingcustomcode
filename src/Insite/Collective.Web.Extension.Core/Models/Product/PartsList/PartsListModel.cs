﻿using System.Collections.Generic;

namespace Collective.Web.Extension.Core.Models.Product.PartsList
{
    public class PartsListModel
    {
        public List<PackageModel> Packages { get; set; }
    }
}
