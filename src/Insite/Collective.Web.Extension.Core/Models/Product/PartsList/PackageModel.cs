﻿using System.Collections.Generic;

namespace Collective.Web.Extension.Core.Models.Product.PartsList
{
    public class PackageModel
    {
        public List<GroupModel> Groups { get; set; }
        public string Number { get; set; }
    }
}
