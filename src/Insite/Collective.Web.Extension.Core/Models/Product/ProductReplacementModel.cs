﻿namespace Collective.Web.Extension.Core.Models.Product
{
    public class ProductReplacedModel
    {
        public string ErpNumber { get; set; }
        public string ProductName { get; set; }
        public string Url { get; set; }
    }
}
