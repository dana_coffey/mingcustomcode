﻿namespace Collective.Web.Extension.Core.Models.Product
{
    public class ProductCustomFieldModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
