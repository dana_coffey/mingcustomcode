﻿using System.Collections.Generic;
using System.Linq;

namespace Collective.Web.Extension.Core.Models.Product
{
    public class LocalizedStringJsonModel : Dictionary<string, string>
    {
        public string GetByCulture(string culture)
        {
            return this.ContainsKey(culture) ? this[culture] : this.FirstOrDefault().Value;
        }
    }
}
