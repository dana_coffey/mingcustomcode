﻿using System.Collections.Generic;
using Collective.Web.Extension.Core.Models.Warehouse;

namespace Collective.Web.Extension.Core.Models.Inventory
{
    public class AvailabilityStatusModel
    {
        public bool HasLocalWarehouse => LocalWarehouse != null;
        public decimal LocalQuantity { get; set; }
        public WarehouseModel LocalWarehouse { get; set; }
        public string Message { get; set; }
        public bool ShowWarehouses { get; set; }
        public AvailabilityStatusType Status { get; set; }
        public decimal TotalQuantity { get; set; }
        public List<AvailabilityStatusWarehouseModel> Warehouses { get; set; }
    }
}
