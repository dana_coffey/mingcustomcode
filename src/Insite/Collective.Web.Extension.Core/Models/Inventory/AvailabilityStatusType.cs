﻿namespace Collective.Web.Extension.Core.Models.Inventory
{
    public enum AvailabilityStatusType
    {
        AvailableLocal = 0,
        AvailableSoon = 2,
        NotAvailable = 3,
        Unknown = 4,
        Restricted = 5
    }
}
