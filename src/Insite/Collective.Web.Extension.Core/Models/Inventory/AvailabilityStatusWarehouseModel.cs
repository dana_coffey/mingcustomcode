﻿namespace Collective.Web.Extension.Core.Models.Inventory
{
    public class AvailabilityStatusWarehouseModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool CanPickup { get; set; }
        public bool CanShipFrom { get; set; }
        public string City { get; set; }
        public string DisplayName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }
        public decimal Quantity { get; set; }
        public string State { get; set; }
    }
}
