﻿using System.IO;
using DotLiquid;
using Insite.Core.Context;

namespace Collective.Web.Extension.Core.DotLiquid.Tags
{
    public class WebsiteNameTag : Tag
    {
        public override void Render(Context context, TextWriter result)
        {
            result.Write(SiteContext.Current.WebsiteDto.Name);
        }
    }
}
