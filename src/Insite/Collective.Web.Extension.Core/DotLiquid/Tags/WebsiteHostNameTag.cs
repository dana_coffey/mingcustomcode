﻿using System.IO;
using Collective.Web.Extension.Core.Helpers;
using DotLiquid;

namespace Collective.Web.Extension.Core.DotLiquid.Tags
{
    public class WebsiteHostNameTag : Tag
    {
        public override void Render(Context context, TextWriter result)
        {
            result.Write(WebsiteHelper.GetWebsiteHostName());
        }
    }
}
