﻿using Collective.Web.Extension.Core.PostStartupScripts;
using Collective.Web.Extension.Core.PostStartupScripts.Scripts.Base;
using Insite.WebFramework.Content.Interfaces;

namespace Collective.Web.Extension.Core.ContentMigrator
{
    public class ContentMigratorCollective : IContentMigrator
    {
        public void MigrateContent()
        {
            PostStartupScriptsRunner.RunAllScripts<BaseContentMigrationPostStartupScript>();
        }
    }
}
