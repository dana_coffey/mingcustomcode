﻿namespace Collective.Web.Extension.Core.Enums
{
    public enum MaxRecallFileType
    {
        Invoice,
        ProofOfDelivery
    }
}
