﻿namespace Collective.Web.Extension.Core.Enums
{
    public enum DeleteAction
    {
        None,
        Delete,
        Archive
    }
}
