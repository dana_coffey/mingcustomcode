﻿using System;
using Collective.Web.Extension.Core.Interfaces;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Plugins.Inventory;

namespace Collective.Web.Extension.Core.Services
{
    public class RealTimeInventoryProvider : InventoryProvider
    {
        private readonly ICollectiveInventoryProvider _collectiveInventoryProvider;

        public RealTimeInventoryProvider(IUnitOfWorkFactory unitOfWorkFactory, ICollectiveInventoryProvider collectiveInventoryProvider) : base(unitOfWorkFactory)
        {
            _collectiveInventoryProvider = collectiveInventoryProvider;
        }

        public override void DecrementQtyOnHand(decimal qtyToDecrement, Guid productId, Guid? warehouseId = null)
        {
        }

        public override decimal GetQtyOnHand(Product product, Guid? warehouseId = null)
        {
            var validWarehouse = GetValidWarehouse(warehouseId);
            if (!ValidateInventoryContext(product, validWarehouse) && SiteContext.Current.ShipTo?.DefaultWarehouse != null)
            {
                return decimal.Zero;
            }

            return _collectiveInventoryProvider.GetQtyOnHand(product, validWarehouse?.Name);
        }
    }
}
