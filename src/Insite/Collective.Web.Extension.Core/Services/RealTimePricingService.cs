﻿using System.Collections.Generic;
using System.Linq;
using Collective.InforSxe.Core.Interfaces;
using Collective.InforSxe.Core.Models;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Interfaces.Plugins.Pricing;
using Insite.Core.Plugins.Pricing;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Services
{
    [DependencyName("Collective - Real-time")]
    public class CollectiveRealTimePricingService : IPricingService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly ISxeApiHelper _sxeApiHelper;

        public CollectiveRealTimePricingService(ISxeApiHelper sxeApiHelper)
        {
            var unitOfWOrk = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork();
            _customerRepository = unitOfWOrk.GetRepository<Customer>();
            _productRepository = unitOfWOrk.GetRepository<Product>();
            _sxeApiHelper = sxeApiHelper;
        }

        public PricingServiceResult CalculatePrice(PricingServiceParameter pricingServiceParameter)
        {
            var productCode = _productRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Id == pricingServiceParameter.ProductId)?.ErpNumber;
            var billTo = GetBillTo(pricingServiceParameter);
            var shipTo = GetShipTo(pricingServiceParameter);

            if (billTo.HasValue && !string.IsNullOrEmpty(productCode))
            {
                var warehouseName = SiteContext.Current.WarehouseDto?.Name ?? pricingServiceParameter.Warehouse;
                var apiModel = _sxeApiHelper.GetPrice(billTo.Value, shipTo, productCode, warehouseName);

                if (apiModel != null)
                {
                    var unitNetPrice = GetNetPrice(apiModel, (pricingServiceParameter as PricingServiceParameterWithOrderLine)?.OrderLine.QtyOrdered ?? pricingServiceParameter.QtyOrdered);

                    return new PricingServiceResult
                    {
                        CurrencyRate = apiModel.CurrencyConversionRate,
                        IsOnSale = false, // apiModel.IsOnSale,
                        UnitListPrice = apiModel.Price,
                        UnitListPriceDisplay = apiModel.Price.ToString("C"),
                        UnitRegularBreakPrices =
                            apiModel.BreakPrices?.Select(x => new ProductPrice { BreakQty = x.BreakQty, PriceDisplay = x.Price.ToString("C"), Price = x.Price }).ToList()
                            ?? new List<ProductPrice> { new ProductPrice { BreakQty = 1, PriceDisplay = apiModel.NetPrice.ToString("C"), Price = apiModel.NetPrice } },
                        UnitRegularPrice = unitNetPrice,
                        UnitRegularPriceDisplay = unitNetPrice.ToString("C")
                    };
                }
            }

            return new PricingServiceResult();
        }

        private static decimal GetNetPrice(PricingApiModel pricingModel, decimal quantity)
        {
            return pricingModel.BreakPrices?.OrderByDescending(x => x.BreakQty).FirstOrDefault(x => x.BreakQty <= quantity)?.Price ?? pricingModel.NetPrice;
        }

        private int? GetBillTo(PricingServiceParameter pricingServiceParameter)
        {
            var erpNumber = SiteContext.Current.BillTo?.ErpNumber;

            if (pricingServiceParameter.BillToId.HasValue)
            {
                erpNumber = _customerRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Id == pricingServiceParameter.BillToId.Value)?.ErpNumber;
            }

            return int.TryParse(erpNumber, out var i) ? i : (int?)null;
        }

        private string GetShipTo(PricingServiceParameter pricingServiceParameter)
        {
            if (pricingServiceParameter.ShipToId.HasValue)
            {
                return _customerRepository.GetTableAsNoTracking().FirstOrDefault(x => x.Id == pricingServiceParameter.ShipToId.Value)?.ErpSequence;
            }

            return SiteContext.Current.ShipTo?.ErpSequence;
        }
    }
}