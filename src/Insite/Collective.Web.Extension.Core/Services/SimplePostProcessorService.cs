﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services
{
    public class SimplePostProcessorService : BasePostProcessorService
    {
        private readonly IntegrationJob _integrationJob;

        public SimplePostProcessorService(IUnitOfWork unitOfWork, IJobLogger jobLogger, IntegrationJob job) : base(unitOfWork, jobLogger)
        {
            _integrationJob = job;
        }

        public void ProcessStep<TModel>(ISimplePostProcessorStep<TModel> step, JobDefinitionStep jobDefinitionStep, DataSet dataSet, CancellationToken cancellationToken) where TModel : class
        {
            if (jobDefinitionStep == null)
            {
                throw new ArgumentNullException(nameof(jobDefinitionStep));
            }

            if (dataSet == null)
            {
                throw new ArgumentNullException(nameof(dataSet));
            }

            var models = GetStepModels<TModel>(dataSet, jobDefinitionStep);
            if (models?.Count > 0)
            {
                InitializeStep(step, models);
                ProcessStep(step, models, cancellationToken);
                TerminateStep(step, models);
            }
        }

        private void InitializeStep<TModel>(ISimplePostProcessorStep<TModel> step, List<TModel> models) where TModel : class
        {
            DoInitializeStep(step.UseTransaction, () => { step.Initialize(models, JobLogger); });
        }

        private void ProcessStep<TModel>(ISimplePostProcessorStep<TModel> step, List<TModel> models, CancellationToken cancellationToken) where TModel : class
        {
            DoProcessStep(models, cancellationToken, step.IsBatchSaving, step.UseTransaction, model =>
            {
                var result = step.Process(model);

                _integrationJob.RecordsAdded = _integrationJob.RecordsAdded + result.RecordsAdded;
                _integrationJob.RecordsModified = _integrationJob.RecordsModified + result.RecordsModified;
                _integrationJob.RecordsDeleted = _integrationJob.RecordsDeleted + result.RecordsDeleted;
            }, step.InitializeBatch);
        }

        private void TerminateStep<TModel>(ISimplePostProcessorStep<TModel> step, List<TModel> models) where TModel : class
        {
            DoTerminateStep(step.UseTransaction, () => { step.Terminate(models); });
        }
    }
}
