﻿using Collective.Web.Extension.Core.Models.Settings;
using Collective.Web.Extension.Core.Settings;
using Insite.Core.Interfaces.Dependency;
using Insite.Core.Services;

namespace Collective.Web.Extension.Core.Services
{
    public class CollectiveSettingsService : ISettingsService<CollectiveSettingsModel>, IDependency
    {
        private readonly CollectiveGeneralSettings _collectiveGeneralSettings;
        private readonly CollectiveInventorySettings _collectiveInventorySettings;
        private readonly CollectiveJobsSettings _collectiveJobsSettings;

        public CollectiveSettingsService(CollectiveGeneralSettings collectiveGeneralSettings, CollectiveInventorySettings collectiveInventorySettings, CollectiveJobsSettings collectiveJobsSettings)
        {
            _collectiveGeneralSettings = collectiveGeneralSettings;
            _collectiveInventorySettings = collectiveInventorySettings;
            _collectiveJobsSettings = collectiveJobsSettings;
        }

        public CollectiveSettingsModel GetSettings(GetSettingsParameter parameter)
        {
            return new CollectiveSettingsModel
            {
                PaymentGatewayUrl = _collectiveGeneralSettings.PaymentGatewayUrl,
                ShowAvailabilityByWarehouse = _collectiveInventorySettings.ShowAvailabilityByWarehouse,
                UsePostProcessorBatchSaving = _collectiveJobsSettings.UsePostProcessorBatchSaving,
                ShowTermsAndConditions = _collectiveGeneralSettings.ShowTermsAndConditions
            };
        }
    }
}
