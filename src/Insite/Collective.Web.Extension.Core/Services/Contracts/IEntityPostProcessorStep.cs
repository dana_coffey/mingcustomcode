﻿using System.Collections.Generic;
using Collective.Web.Extension.Core.Enums;
using Insite.Data.Entities;
using Insite.Integration.WebService;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services.Contracts
{
    public interface IEntityPostProcessorStep<TEntity, TModel> where TEntity : EntityBase where TModel : class
    {
        DeleteAction DeleteAction { get; }

        bool IsBatchSaving { get; }

        bool UseTransaction { get; }

        void ArchiveEntity(TEntity entity);

        List<TEntity> GetEntitiesToDeleteOrArchive(List<TModel> models);

        TEntity GetEntity(TModel model);

        void Initialize(List<TModel> models, IJobLogger jobLogger);

        void InitializeBatch(List<TModel> models);

        bool InsertEntity(TModel model, TEntity entity);

        void Terminate(List<TModel> models);

        void UpdateEntity(TModel model, TEntity entity);
    }
}
