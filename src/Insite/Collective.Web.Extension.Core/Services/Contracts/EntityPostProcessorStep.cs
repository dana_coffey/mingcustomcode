﻿using System.Collections.Generic;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Helpers;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services.Contracts
{
    public abstract class EntityPostProcessorStep<TEntity, TModel> : IEntityPostProcessorStep<TEntity, TModel> where TEntity : EntityBase where TModel : class
    {
        public virtual DeleteAction DeleteAction => DeleteAction.None;
        public virtual bool IsBatchSaving => PostProcessorHelper.IsBatchSavingEnabled();
        public virtual bool UseTransaction => false;
        protected IJobLogger JobLogger { get; set; }

        public virtual void ArchiveEntity(TEntity entity)
        {
        }

        public virtual List<TEntity> GetEntitiesToDeleteOrArchive(List<TModel> models)
        {
            return null;
        }

        public virtual TEntity GetEntity(TModel model)
        {
            return null;
        }

        public virtual void Initialize(List<TModel> models, IJobLogger jobLogger)
        {
            JobLogger = jobLogger;
        }

        public virtual void InitializeBatch(List<TModel> models)
        {
        }

        public virtual bool InsertEntity(TModel model, TEntity entity)
        {
            return false;
        }

        public virtual void Terminate(List<TModel> models)
        {
        }

        public virtual void UpdateEntity(TModel model, TEntity entity)
        {
        }
    }
}
