﻿using System.Collections.Generic;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services.Contracts
{
    public interface ISimplePostProcessorStep<TModel> where TModel : class
    {
        bool IsBatchSaving { get; }

        bool UseTransaction { get; }

        void Initialize(List<TModel> models, IJobLogger jobLogger);

        void InitializeBatch(List<TModel> models);

        SimplePostProcessorStepProcessStats Process(TModel model);

        void Terminate(List<TModel> models);
    }
}
