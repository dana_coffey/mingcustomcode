﻿namespace Collective.Web.Extension.Core.Services.Contracts
{
    public class SimplePostProcessorStepProcessStats
    {
        public int RecordsAdded { get; set; }
        public int RecordsDeleted { get; set; }
        public int RecordsModified { get; set; }
    }
}
