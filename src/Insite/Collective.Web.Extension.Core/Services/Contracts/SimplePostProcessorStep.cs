﻿using System.Collections.Generic;
using Collective.Web.Extension.Core.Helpers;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services.Contracts
{
    public class SimplePostProcessorStep<TModel> : ISimplePostProcessorStep<TModel> where TModel : class
    {
        public virtual bool IsBatchSaving => PostProcessorHelper.IsBatchSavingEnabled();
        public virtual bool UseTransaction => false;
        protected IJobLogger JobLogger { get; set; }

        public virtual void Initialize(List<TModel> models, IJobLogger jobLogger)
        {
            JobLogger = jobLogger;
        }

        public virtual void InitializeBatch(List<TModel> models)
        {
        }

        public virtual SimplePostProcessorStepProcessStats Process(TModel model)
        {
            return new SimplePostProcessorStepProcessStats
            {
                RecordsAdded = 0,
                RecordsModified = 0
            };
        }

        public virtual void Terminate(List<TModel> models)
        {
        }
    }
}
