﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Collective.Web.Extension.Core.Enums;
using Collective.Web.Extension.Core.Helpers;
using Collective.Web.Extension.Core.Services.Contracts;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services
{
    public class EntityPostProcessorService : BasePostProcessorService
    {
        private readonly IntegrationJob _integrationJob;

        public EntityPostProcessorService(IUnitOfWork unitOfWork, IJobLogger jobLogger, IntegrationJob job) : base(unitOfWork, jobLogger)
        {
            _integrationJob = job;
        }

        public void ProcessStep<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, JobDefinitionStep jobDefinitionStep, DataSet dataSet, CancellationToken cancellationToken) where TEntity : EntityBase where TModel : class
        {
            if (jobDefinitionStep == null)
            {
                throw new ArgumentNullException(nameof(jobDefinitionStep));
            }

            if (dataSet == null)
            {
                throw new ArgumentNullException(nameof(dataSet));
            }

            var models = GetStepModels<TModel>(dataSet, jobDefinitionStep);
            if (models?.Count > 0)
            {
                InitializeStep(step, models);
                ProcessStep(step, models, cancellationToken);
                TerminateStep(step, models);
            }
        }

        private List<TEntity> GetEntitiesToDeleteOrArchive<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models) where TEntity : EntityBase where TModel : class
        {
            JobLogger.Debug("Getting entities to delete or archive...");
            return step.GetEntitiesToDeleteOrArchive(models);
        }

        private void InitializeStep<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models) where TEntity : EntityBase where TModel : class
        {
            DoInitializeStep(step.UseTransaction, () => { step.Initialize(models, JobLogger); });
        }

        private void ProcessStep<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models, CancellationToken cancellationToken) where TEntity : EntityBase where TModel : class
        {
            var repository = UnitOfWork.GetRepository<TEntity>();

            ProcessStepUpsert(step, models, repository, cancellationToken);
            ProcessStepDelete(step, models, repository, cancellationToken);
        }

        private void ProcessStepDelete<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models, IRepository<TEntity> repository, CancellationToken cancellationToken) where TEntity : EntityBase where TModel : class
        {
            if (step.DeleteAction != DeleteAction.None)
            {
                var toDeleteEntities = GetEntitiesToDeleteOrArchive(step, models);
                if (toDeleteEntities?.Count > 0)
                {
                    DoProcessStep(toDeleteEntities, cancellationToken, step.IsBatchSaving, step.UseTransaction, item =>
                    {
                        var entityToDelete = repository.GetTable().FirstOrDefault(x => x.Id == item.Id);
                        if (entityToDelete != null)
                        {
                            switch (step.DeleteAction)
                            {
                                case DeleteAction.Delete:
                                    repository.Delete(entityToDelete);
                                    _integrationJob.RecordsDeleted++;
                                    break;

                                case DeleteAction.Archive:
                                    step.ArchiveEntity(entityToDelete);
                                    _integrationJob.RecordsDeleted++;
                                    break;
                            }
                        }
                    }, doInitializeBatch: null);
                }
            }
        }

        private void ProcessStepUpsert<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models, IRepository<TEntity> repository, CancellationToken cancellationToken) where TEntity : EntityBase where TModel : class
        {
            var properties = PostProcessorHelper.GetEntityPropertyDefinitions(typeof(TEntity));

            DoProcessStep(models, cancellationToken, step.IsBatchSaving, step.UseTransaction, model =>
            {
                var entity = step.GetEntity(model);
                if (entity != null)
                {
                    step.UpdateEntity(model, entity);
                    _integrationJob.RecordsModified++;
                }
                else
                {
                    entity = repository.Create();

                    PostProcessorHelper.SetEntityDefaultValues(entity, properties);

                    if (step.InsertEntity(model, entity))
                    {
                        step.UpdateEntity(model, entity);
                        repository.Insert(entity);
                        _integrationJob.RecordsAdded++;
                    }
                }
            }, step.InitializeBatch);
        }

        private void TerminateStep<TEntity, TModel>(IEntityPostProcessorStep<TEntity, TModel> step, List<TModel> models) where TEntity : EntityBase where TModel : class
        {
            DoTerminateStep(step.UseTransaction, () => { step.Terminate(models); });
        }
    }
}
