﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Collective.Web.Extension.Core.Helpers;
using Insite.Core.Interfaces.Data;
using Insite.Data;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;

namespace Collective.Web.Extension.Core.Services
{
    public abstract class BasePostProcessorService
    {
        protected virtual int BatchSize => 100;
        protected IJobLogger JobLogger { get; }
        protected IUnitOfWork UnitOfWork { get; }

        protected BasePostProcessorService(IUnitOfWork unitOfWork, IJobLogger jobLogger)
        {
            UnitOfWork = unitOfWork;
            unitOfWork.DataProvider.SetConfiguration(new DataProviderConfiguration(unitOfWork.DataProvider.GetConfiguration())
            {
                ChangeTrackingEnabled = false
            });

            JobLogger = jobLogger;
        }

        protected void DoInitializeStep(bool useTransaction, Action doAction)
        {
            JobLogger.Debug("Initializing step...");

            if (useTransaction)
            {
                JobLogger.Debug("Beginning transaction");
                UnitOfWork.BeginTransaction();
            }

            doAction();
        }

        protected void DoProcessStep<T>(List<T> items, CancellationToken cancellationToken, bool batchSaving, bool useTransaction, Action<T> doAction, Action<List<T>> doInitializeBatch) where T : class
        {
            JobLogger.Debug("Processing step...");

            var errors = 0;
            var mustRollbackTransaction = false;
            var batchCount = Math.Ceiling((double)items.Count / BatchSize);

            for (var i = 0; i < batchCount; i++)
            {
                JobLogger.Debug($"Processing batch {i + 1} of {batchCount}... (Errors: {errors})");

                try
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        JobLogger.Debug("Cancellation Requested");

                        if (useTransaction)
                        {
                            UnitOfWork.RollbackTransaction();
                        }

                        UnitOfWork.Close();
                        break;
                    }

                    var batchItems = items.Skip(i * BatchSize).Take(BatchSize).ToList();

                    if (batchSaving)
                    {
                        doInitializeBatch?.Invoke(batchItems);
                    }

                    foreach (var model in batchItems)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            break;
                        }

                        try
                        {
                            doAction(model);

                            if (!batchSaving)
                            {
                                UnitOfWork.DataProvider.DetectChanges();
                                UnitOfWork.Save();
                            }
                        }
                        catch (Exception ex)
                        {
                            errors++;
                            LogError(ex);
                            mustRollbackTransaction = useTransaction;
                        }
                    }

                    if (batchSaving)
                    {
                        UnitOfWork.DataProvider.DetectChanges();
                        UnitOfWork.Save();
                    }

                    UnitOfWork.Clear(dispose: !useTransaction);
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    mustRollbackTransaction = useTransaction;
                }
            }

            if (mustRollbackTransaction)
            {
                JobLogger.Debug("Rollbacking transaction");
                UnitOfWork.RollbackTransaction();
            }
        }

        protected void DoTerminateStep(bool useTransaction,  Action doAction)
        {
            JobLogger.Debug("Terminating step...");

            doAction();

            if (useTransaction)
            {
                JobLogger.Debug("Committing transaction");
                UnitOfWork.CommitTransaction();
            }

            UnitOfWork.Close();
        }

        protected List<TModel> GetStepModels<TModel>(DataSet dataSet, JobDefinitionStep jobDefinitionStep) where TModel : class
        {
            JobLogger.Debug($"Getting models ({jobDefinitionStep.Name})...");

            return PostProcessorHelper.ReadDataSet<TModel>(dataSet, jobDefinitionStep);
        }

        private void LogError(Exception exception)
        {
            PostProcessorHelper.LogError(JobLogger, exception);
        }
    }
}
