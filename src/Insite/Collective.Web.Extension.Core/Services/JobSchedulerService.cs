﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Web.Extension.Core.Jobs;
using Collective.Web.Extension.Core.Models.Scheduler;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Services
{
    public static class JobSchedulerService
    {
        public static bool ScheduleJobs(List<SimpleJobModel> jobs)
        {
            if (SchedulerJob.CanScheduleJobs())
            {
                var jobDefinitions = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<JobDefinition>().GetTableAsNoTracking().ToList();
                var queueId = Guid.NewGuid();
                var jobToSchedule = new List<JobModel>();

                foreach (var job in jobs)
                {
                    var integrationJob = jobDefinitions.FirstOrDefault(p => p.Name == job.IntegrationJobName);
                    if (integrationJob != null)
                    {
                        jobToSchedule.Add(new JobModel
                        {
                            CancelQueueOnError = job.CancelQueueOnError,
                            CanScheduleNewJobDuringExecution = job.CanScheduleNewJobDuringExecution,
                            QueueId = queueId,
                            Job = new IntegrationJob
                            {
                                JobDefinitionId = integrationJob.Id,
                                ScheduleDateTime = DateTimeOffset.Now,
                                RunStandalone = true,
                                IsRealTime = false,
                                IntegrationJobParameters = new List<IntegrationJobParameter>()
                            }
                        });
                    }
                }

                return SchedulerJob.ScheduleJobs(jobToSchedule);
            }

            return false;
        }
    }
}
