﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "Jobs", PrimaryGroupName = "CollectiveSettings", SortOrder = 3)]
    public class CollectiveJobsSettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Use Post Processor Batch Saving", SortOrder = 0)]
        public virtual bool UsePostProcessorBatchSaving => GetValue(true);
    }
}
