﻿using Insite.Core.Interfaces.Dependency;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [DependencyName("CollectiveSettings")]
    public class CollectiveSettingsPrimaryGroup : BasePrimaryGroup
    {
        public override string Description => string.Empty;
        public override string Label => "Collective";
        public override string Name => "CollectiveSettings";
        public override string ShortDescription => "Custom settings for the Collective.";
    }
}
