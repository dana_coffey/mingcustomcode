﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "Inventory", PrimaryGroupName = "CollectiveSettings", SortOrder = 1)]
    public class CollectiveInventorySettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Show Availability by Warehouse", SortOrder = 0, ControlType = SystemSettingControlType.Toggle)]
        public virtual bool ShowAvailabilityByWarehouse => GetValue(false);
    }
}
