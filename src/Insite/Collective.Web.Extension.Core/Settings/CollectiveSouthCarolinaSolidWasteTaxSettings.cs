﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "South Carolina Solid Waste Tax", PrimaryGroupName = "CollectiveSettings", SortOrder = 99)]
    public class CollectiveSouthCarolinaSolidWasteTaxSettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Amount per Unit ($)", SortOrder = 1, ControlType = SystemSettingControlType.Decimal)]
        public virtual decimal Amount => GetValue(0m);
        [SettingsField(DisplayName = "Product Number", SortOrder = 0, ControlType = SystemSettingControlType.Text, Description = "Product number to use when sending the tax amount to the ERP")]
        public virtual string ProductNumber => GetValue(string.Empty);
    }
}
