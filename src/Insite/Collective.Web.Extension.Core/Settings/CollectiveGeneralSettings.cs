﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "General", PrimaryGroupName = "CollectiveSettings", SortOrder = 0)]
    public class CollectiveGeneralSettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Payment Gateway Url", SortOrder = 0, ControlType = SystemSettingControlType.Url)]
        public virtual string PaymentGatewayUrl => GetValue(string.Empty);

        [SettingsField(DisplayName = "Show Terms and Conditions", SortOrder = 1, ControlType = SystemSettingControlType.Toggle)]
        public virtual bool ShowTermsAndConditions => GetValue(false);
    }
}
