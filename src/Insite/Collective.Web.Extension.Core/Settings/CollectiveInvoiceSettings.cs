﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "Invoices", PrimaryGroupName = "CollectiveSettings", SortOrder = 2)]
    public class CollectiveInvoiceSettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Invoice Request Action Name", SortOrder = 10, ControlType = SystemSettingControlType.Text)]
        public virtual string InvoiceRequestActionName => GetValue(string.Empty);
        [SettingsField(DisplayName = "MaxRecall Url", SortOrder = 0, ControlType = SystemSettingControlType.Url)]
        public virtual string MaxRecallUrl => GetValue(string.Empty);

        [SettingsField(DisplayName = "Proof of Delivery Action Name", SortOrder = 20, ControlType = SystemSettingControlType.Text)]
        public virtual string ProofOfDeliveryActionName => GetValue(string.Empty);
    }
}
