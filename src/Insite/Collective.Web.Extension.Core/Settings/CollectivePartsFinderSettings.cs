﻿using Insite.Core.SystemSetting;
using Insite.Core.SystemSetting.Groups;

namespace Collective.Web.Extension.Core.Settings
{
    [SettingsGroup(Description = "", Label = "Equipment Part List", PrimaryGroupName = "CollectiveSettings", SortOrder = 4)]
    public class CollectivePartsFinderSettings : BaseSettingsGroup
    {
        [SettingsField(DisplayName = "Use Carrier equipment part list service for product with following ERP Vendor Number", SortOrder = 0, ControlType = SystemSettingControlType.MultipleValue, Description = "To configure this filter refer to InRiver Model Tool under CVL List ERPVendorNo and use the key corresponding to your vendor")]
        public virtual string ErpVendorNumbers => GetValue("20080,20081,20467,30093");
    }
}