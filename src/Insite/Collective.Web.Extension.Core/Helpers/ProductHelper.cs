﻿using System.Linq;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Models.Product;
using Collective.Web.Extension.Core.Settings;
using Insite.Common.Dependencies;
using Insite.Data.Entities;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class ProductHelper
    {
        public static string GetProductVendor(Product product)
        {
            var productCustomFieldsJson = product?.GetProperty(Constants.CustomProperties.Product.CustomFields, string.Empty);

            if (!string.IsNullOrEmpty(productCustomFieldsJson))
            {
                var productCustomFields = JsonConvert.DeserializeObject<ProductCustomFieldJsonModel>(productCustomFieldsJson);

                if (productCustomFields?.ContainsKey(Constants.ProductFields.FieldNames.ItemErpVendorNo) ?? false)
                {
                    return productCustomFields[Constants.ProductFields.FieldNames.ItemErpVendorNo]?.GetByCulture(string.Empty);
                }
            }

            return null;
        }

        public static bool IsProductPartsListCompatible(Product product)
        {
            return DependencyLocator.Current.GetInstance<CollectivePartsFinderSettings>()?.ErpVendorNumbers.Split(',').ToList().Contains(GetProductVendor(product)) ?? false;
        }
    }
}