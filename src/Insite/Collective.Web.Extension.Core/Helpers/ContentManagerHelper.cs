﻿using System.Collections.Generic;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class ContentManagerHelper
    {
        public static ContentManager Create(string name)
        {
            return new ContentManager
            {
                Name = name,
                Contents = new List<Content>()
            };
        }
    }
}
