﻿using System.Collections.Generic;
using System.Globalization;
using System.Web;
using Insite.Catalog.Services.Dtos;
using Insite.Catalog.WebApi.V1.ApiModels;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class MicrositeDataHelper
    {
        public static HtmlString GetBreadcrumbScript(ICollection<BreadCrumbModel> breadcrumbs, HttpRequestBase request)
        {
            var breadcrumbScriptItems = new List<string>();
            var index = 0;
            foreach (var breadcrumb in breadcrumbs)
            {
                index++;
                breadcrumbScriptItems.Add(
                    "{" +
                    "\"@type\": \"ListItem\"," +
                    $"\"position\": \"{index}\"," +
                    "\"item\": " +
                    "{" +
                    $"\"@id\": \"{(!string.IsNullOrEmpty(breadcrumb.Url) ? UrlHelper.GetAbsoluteUrl(request, breadcrumb.Url) : UrlHelper.GetAbsoluteUrl(request))}\"," +
                    $"\"name\": \"{breadcrumb.Text}\"" +
                    "}" +
                    "}");
            }

            return new HtmlString(
                "<script type=\"application/ld+json\">" +
                "{" +
                "\"@context\": \"http://schema.org\"," +
                "\"@type\": \"Breadcrumb\"," +
                "\"itemListElement\": " +
                "[" +
                string.Join(", ", breadcrumbScriptItems) +
                "]" +
                "}" +
                "</script>");
        }

        public static HtmlString GetProductScript(ProductDto product, ICollection<BreadCrumbModel> breadcrumbs, HttpRequestBase request)
        {
            double d;

            return new HtmlString(
                "<script type=\"application/ld+json\">" +
                "{" +
                "\"@context\": \"http://schema.org\"," +
                "\"@type\": \"Product\"," +
                $"\"category\": \"{CatalogPageHelper.GetCategoryPathFromBreadCrumb(breadcrumbs)}\"," +
                $"\"description\": \"{product.ShortDescription}\"," +
                (double.TryParse(product.ShippingHeight, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && d > 0 ? $"\"height\": \"{product.ShippingHeight}\"," : string.Empty) +
                (double.TryParse(product.ShippingWeight, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && d > 0 ? $"\"weight\": \"{product.ShippingWeight}\"," : string.Empty) +
                (double.TryParse(product.ShippingWidth, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && d > 0 ? $"\"width\": \"{product.ShippingWidth}\"," : string.Empty) +
                $"\"image\": \"{product.LargeImagePath}\"," +
                $"\"name\": \"{product.Name}\"," +
                $"\"sku\": \"{product.ERPNumber}\"," +
                $"\"url\": \"{UrlHelper.GetAbsoluteUrl(request)}\"" +
                "}" +
                "</script>");
        }
    }
}
