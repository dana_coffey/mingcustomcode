﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Settings;
using Insite.Common.Dependencies;
using Insite.Core.ApplicationDictionary;
using Insite.Data.Entities;
using Insite.Integration.WebService.Interfaces;
using Newtonsoft.Json;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class PostProcessorHelper
    {
        public static List<PropertyDefinitionDto> GetEntityPropertyDefinitions(Type type)
        {
            var properties = DependencyLocator.Current.GetInstance<IEntityDefinitionProvider>().GetByName(type.Name, HttpContext.Current?.User?.Identity?.Name ?? "admin").Properties;
            return properties.Where(p => p.IsHidden || (p.CanView && !p.CanEdit)).ToList();
        }

        public static bool IsBatchSavingEnabled()
        {
            return DependencyLocator.Current.GetInstance<CollectiveJobsSettings>().UsePostProcessorBatchSaving;
        }

        public static void LogError(IJobLogger jobLogger, Exception exception)
        {
            if (jobLogger != null)
            {
                while (!string.IsNullOrEmpty(exception?.Message))
                {
                    jobLogger.Error(exception.Message);
                    exception = exception.InnerException;
                }
            }
        }

        public static List<TModel> ReadDataSet<TModel>(DataSet dataSet, JobDefinitionStep jobDefinitionStep) where TModel : class
        {
            var table = dataSet.Tables[$"{jobDefinitionStep.Sequence}{jobDefinitionStep.ObjectName}"];
            if (table != null)
            {
                var models = new List<TModel>();
                foreach (DataRow row in table.Rows)
                {
                    models.Add(JsonConvert.DeserializeObject<TModel>(row[Constants.Jobs.DatasetJsonPropertyName]?.ToString()));
                }

                return models;
            }

            return null;
        }

        public static void SetEntityDefaultValues<TEntity>(TEntity entity, IList<PropertyDefinitionDto> properties = null) where TEntity : EntityBase
        {
            if (properties == null)
            {
                properties = GetEntityPropertyDefinitions(typeof(TEntity));
            }

            foreach (var property in properties)
            {
                var propertyInfo = property.DataTypeValidator?.GetType().GetProperty("DefaultValue");
                var value = propertyInfo?.GetValue(property.DataTypeValidator);
                if (value != null)
                {
                    var entityPropertyInfo = entity.GetType().GetProperties().FirstOrDefault(p => p.Name.EqualsIgnoreCase(property.Name));
                    entityPropertyInfo?.SetValue(entity, value);
                }
            }
        }
    }
}
