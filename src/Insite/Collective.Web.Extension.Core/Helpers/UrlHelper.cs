﻿using System.Text.RegularExpressions;
using System.Web;
using Collective.Core.Extensions;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class UrlHelper
    {
        public static string CleanUrl(string url)
        {
            url = url.ToAccentInsensitive().ToLower();
            url = Regex.Replace(url, "[^a-zA-Z0-9]", "-").Trim('-');
            return Regex.Replace(url, "[-]+", "-");
        }

        public static string GetAbsoluteUrl(HttpRequestBase request)
        {
            return request?.Url?.AbsoluteUri ?? string.Empty;
        }

        public static string GetAbsoluteUrl(HttpRequestBase request, string url)
        {
            if (request == null || request.Url == null)
            {
                return null;
            }

            return $"{request.Url.Scheme}://{request.Url.Host}{url}";
        }

        public static string GetSearchUrl(string criteria)
        {
            return $"/search?criteria={HttpUtility.UrlEncode(criteria)}";
        }
    }
}
