﻿using System;
using System.Linq;
using Insite.Common.Dependencies;
using Insite.Core.Context;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.SystemSetting;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class WebsiteHelper
    {
        public static string GetCurrentThemeName()
        {
            var repository = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<Theme>();
            return repository.Get(SiteContext.Current.WebsiteDto.ThemeId)?.Name ?? "store";
        }

        public static string GetWebsiteHostName()
        {
            var sslMode = DependencyLocator.Current.GetInstance<ISystemSettingProvider>()?.GetValue("SslMode", string.Empty, false, null);

            if (!string.IsNullOrEmpty(SiteContext.Current?.WebsiteDto?.DomainName))
            {
                var protocol = sslMode == "Always" ? "https" : "http";
                return $"{protocol}://{SiteContext.Current.WebsiteDto.DomainName.Split(',').FirstOrDefault()}/";
            }

            return "http://ming.local.absolunet.com/";
        }

        public static bool WebsiteHasTaxCalculation()
        {
            var website = DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<Website>().Get(SiteContext.Current.WebsiteDto.Id);
            if (website != null)
            {
                return !website.TaxCalculation.Equals("none", StringComparison.InvariantCultureIgnoreCase);
            }

            return false;
        }
    }
}
