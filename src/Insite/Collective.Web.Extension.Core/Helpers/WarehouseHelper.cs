﻿using System;
using System.Collections.Generic;
using System.Linq;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class WarehouseHelper
    {
        private const string ActiveWarehouseEntityListCacheKey = "ActiveWarehouseEntityList";
        private const string ActiveWarehouseListCacheKey = "ActiveWarehouseIdList";

        public static List<string> GetActiveWarehouseList()
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();

            if (!cacheManager.Contains(ActiveWarehouseListCacheKey))
            {
                cacheManager.Add(ActiveWarehouseListCacheKey, DependencyLocator.Current.GetInstance<IUnitOfWorkFactory>().GetUnitOfWork().GetRepository<Warehouse>().GetTableAsNoTracking().Where(x => !x.DeactivateOn.HasValue || x.DeactivateOn >= DateTimeOffset.Now).Select(x => x.Name).ToList(), TimeSpan.FromMinutes(60));
            }

            return cacheManager.Get<List<string>>(ActiveWarehouseListCacheKey);
        }
    }
}
