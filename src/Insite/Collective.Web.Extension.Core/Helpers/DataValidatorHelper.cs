﻿using System.Text.RegularExpressions;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class DataValidatorHelper
    {
        private static readonly Regex EmailRegularExpression = new Regex("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*.", RegexOptions.Compiled);
        private static readonly Regex EmailsListRegularExpression = new Regex(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*$", RegexOptions.Compiled);

        public static bool ValidateEmail(string value)
        {
            return EmailRegularExpression.IsMatch(value);
        }

        public static bool ValidateEmailsList(string value)
        {
            return EmailsListRegularExpression.IsMatch(value);
        }
    }
}
