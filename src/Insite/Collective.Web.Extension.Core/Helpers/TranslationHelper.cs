﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collective.Core.Constant;
using Collective.Core.Helpers;
using Insite.Core.Interfaces.Data;
using Insite.Core.Interfaces.EnumTypes;
using Insite.Data.Entities;

namespace Collective.Web.Extension.Core.Helpers
{
    public class TranslationHelper
    {
        public static string GetDefaultDictionaryTranslation(Dictionary<string, string> translations)
        {
            if (translations?.Count > 0)
            {
                if (translations.ContainsKey(Constants.Culture.DefaultCulture))
                {
                    return translations[Constants.Culture.DefaultCulture];
                }
                else if (translations.ContainsKey(Constants.Culture.InvariantCulture))
                {
                    return translations[Constants.Culture.InvariantCulture];
                }
                else
                {
                    return translations.FirstOrDefault().Value;
                }
            }

            return string.Empty;
        }

        public static void Translate(IRepository<TranslationProperty> repository, IRepository<Language> languageRepository, Guid entityId, string propertyName, string table, Dictionary<string, string> translations, Func<string, string> transformFunction = null)
        {
            foreach (var translation in translations.OrderBy(x => x.Key))
            {
                var value = transformFunction != null ? transformFunction(translation.Value) : translation.Value;

                // Is invariant language
                if (string.IsNullOrEmpty(translation.Key))
                {
                    // Default all translations of all languages to this value
                    foreach (var language in languageRepository.GetTable().ToList())
                    {
                        Translate(repository, entityId, language.Id, propertyName, table, value);
                    }
                }
                else
                {
                    var language = languageRepository.GetTable().FirstOrDefault(x => x.CultureCode == translation.Key);
                    if (language != null)
                    {
                        Translate(repository, entityId, language.Id, propertyName, table, value);
                    }
                }
            }
        }

        public static void Translate(IRepository<TranslationProperty> repository, Guid entityId, Guid languageId, string propertyName, string table, string value)
        {
            var translation = repository.GetTable().FirstOrDefault(p => p.ParentId == entityId && p.LanguageId == languageId && p.ParentTable == table && p.Name == propertyName);
            if (translation != null)
            {
                translation.TranslatedValue = value;
            }
            else
            {
                if (!string.IsNullOrEmpty(value))
                {
                    translation = repository.Create();

                    translation.Name = propertyName;
                    translation.LanguageId = languageId;
                    translation.ParentId = entityId;
                    translation.ParentTable = table;
                    translation.TranslatedValue = value;

                    repository.Insert(translation);
                }
            }
        }

        public static void Translate(IRepository<TranslationDictionary> repository, IRepository<Language> languageRepository, string keyword, TranslationDictionarySource source, Dictionary<string, string> translations)
        {
            foreach (var translation in translations.OrderBy(x => x.Key))
            {
                // Is invariant language
                if (string.IsNullOrEmpty(translation.Key))
                {
                    // Default all translations of all languages to this value
                    foreach (var language in languageRepository.GetTable().ToList())
                    {
                        Translate(repository, language.Id, keyword, source, translation.Value);
                    }
                }
                else
                {
                    var language = languageRepository.GetTable().FirstOrDefault(x => x.CultureCode == translation.Key);
                    if (language != null)
                    {
                        Translate(repository, language.Id, keyword, source, translation.Value);
                    }
                }
            }
        }

        public static void Translate(IRepository<TranslationDictionary> repository, Guid languageId, string keyword, TranslationDictionarySource source, string translation)
        {
            if (!string.IsNullOrEmpty(translation))
            {
                var entity = repository.GetTable().FirstOrDefault(p => p.Keyword == keyword && p.LanguageId == languageId && p.Source == source.ToString());
                if (entity != null)
                {
                    entity.Translation = translation;
                }
                else
                {
                    entity = new TranslationDictionary { Id = Guid.NewGuid(), LanguageId = languageId, Keyword = keyword, Source = source.ToString(), Translation = translation };

                    repository.Insert(entity);
                }
            }
        }

        public static void TranslateContent(ContentManager contentManager, IRepository<Language> languageRepository, IRepository<Persona> personaRepository, Dictionary<string, string> values)
        {
            foreach (var value in values)
            {
                var language = GetLanguage(languageRepository, value.Key);
                if (language != null)
                {
                    TranslateContent(contentManager, personaRepository, language.Id, value.Value);
                }
            }
        }

        public static void TranslateContent(ContentManager contentManager, IRepository<Persona> personaRepository, Guid languageId, string value)
        {
            foreach (var persona in personaRepository.GetTable())
            {
                var filteredContents = contentManager.Contents.Where(x => x.LanguageId == languageId && x.PersonaId == persona.Id).ToList();
                var lastContent = filteredContents.Any() ? filteredContents.OrderBy(x => x.Revision).Last() : null;

                if ((lastContent == null && !string.IsNullOrEmpty(value)) || (lastContent != null && lastContent.Html != (value ?? string.Empty)))
                {
                    contentManager.Contents.Add(
                        new Content
                        {
                            DeviceType = Constants.Content.DeviceType.Desktop,
                            LanguageId = languageId,
                            PersonaId = persona.Id,
                            Html = StringHelper.NewlineToBr(value)?.Trim() ?? string.Empty,
                            ApprovedOn = DateTimeOffset.Now,
                            PublishToProductionOn = DateTimeOffset.Now,
                            Revision = lastContent?.Revision + 1 ?? 1
                        });
                }
            }
        }

        private static Language GetLanguage(IRepository<Language> languageRepository, string cultureCode)
        {
            return languageRepository.GetTable().FirstOrDefault(x => x.CultureCode == cultureCode);
        }
    }
}
