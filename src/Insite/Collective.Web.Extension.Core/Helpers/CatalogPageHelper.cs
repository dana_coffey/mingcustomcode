﻿using System.Collections.Generic;
using System.Linq;
using Insite.Catalog.WebApi.V1.ApiModels;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class CatalogPageHelper
    {
        public static string GetCategoryPathFromBreadCrumb(ICollection<BreadCrumbModel> breadcrumbItems)
        {
            return string.Join("/", breadcrumbItems.Where(x => !string.IsNullOrEmpty(x.CategoryId)).Select(x => x.Text));
        }
    }
}
