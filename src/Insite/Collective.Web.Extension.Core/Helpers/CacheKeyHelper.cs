﻿using System;
using Collective.Core.Constant;
using Collective.Web.Extension.Core.Enums;

namespace Collective.Web.Extension.Core.Helpers
{
    public static class CacheKeyHelper
    {
        public static string GenerateInventoryCacheKey(string productId)
        {
            return $"{Constants.Cache.InventoryApi}{productId}";
        }

        public static string GeneratePartsListCacheKey(string productId)
        {
            return $"{Constants.Cache.PartsListApi}{productId}";
        }

        public static string GeneratePricingCacheKey(double billtoId, string shiptoId, string productId, string warehouseId)
        {
            return $"{Constants.Cache.PricingApi}{string.Join(";", productId, billtoId, shiptoId, warehouseId)}";
        }

        public static string GenerateMaxRecallCacheKey(Guid orderId, MaxRecallFileType fileType)
        {
            return $"{Constants.Cache.MaxRecall}{orderId:N}{fileType}";
        }
    }
}
