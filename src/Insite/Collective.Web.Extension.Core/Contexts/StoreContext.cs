﻿using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Plugins.Caching;
using Insite.Core.Plugins.Utilities;

namespace Collective.Web.Extension.Core.Contexts
{
    public class StoreContext
    {
        public static StoreContext Current
        {
            get
            {
                var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();

                var cacheKey = GetCacheKey();
                if (!string.IsNullOrEmpty(cacheKey))
                {
                    if (!cacheManager.Contains(cacheKey))
                    {
                        cacheManager.Add(cacheKey, new StoreContext());
                    }

                    return cacheManager.Get<StoreContext>(cacheKey);
                }

                return null;
            }
        }

        public void Save()
        {
            var cacheManager = DependencyLocator.Current.GetInstance<ICacheManager>();

            var cacheKey = GetCacheKey();
            if (!string.IsNullOrEmpty(cacheKey))
            {
                if (cacheManager.Contains(cacheKey))
                {
                    cacheManager.Remove(cacheKey);
                }

                cacheManager.Add(cacheKey, this);
            }
        }

        private static string GetCacheKey()
        {
            var cookieManager = DependencyLocator.Current.GetInstance<ICookieManager>();
            var insiteCacheId = cookieManager.Get("InsiteCacheId");

            return !string.IsNullOrEmpty(insiteCacheId) ? $"{insiteCacheId}-storecontext" : null;
        }
    }
}
