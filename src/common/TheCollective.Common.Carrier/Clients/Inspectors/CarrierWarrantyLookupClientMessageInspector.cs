﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace TheCollective.Common.Carrier.Clients.Inspectors
{
    public class CarrierWarrantyLookupClientMessageInspector : IClientMessageInspector
    {
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            // Backup properties and headers for when we will create new Message
            var properties = new MessageProperties(request.Properties);
            var headers = new MessageHeaders(request.Headers);
            request.Properties.Clear();
            request.Headers.Clear();

            // Modify the request message string, and put it into an XmlReader
            var modifiedMessage = ModifyRequestMessage(request.ToString());

            var ms = new MemoryStream();

            var writer = new StreamWriter(ms);
            writer.Write(modifiedMessage);
            writer.Flush();

            ms.Position = 0;
            var reader = XmlReader.Create(ms);

            // Create new Message with the modified string
            request = Message.CreateMessage(reader, int.MaxValue, request.Version);
            request.Headers.CopyHeadersFrom(headers);
            request.Properties.CopyProperties(properties);

            return request;
        }

        private static string ModifyRequestMessage(string request)
        {
            // Change 's' for 'soap', remote does not accept 's'...
            request = request.Replace("xmlns:s", "xmlns:soap");
            request = request.Replace("s:Envelope", "soap:Envelope");
            request = request.Replace("s:Header", "soap:Header");
            request = request.Replace("s:Body", "soap:Body");

            var document = new XmlDocument();
            document.LoadXml(request);

            return document.InnerXml;
        }
    }
}
