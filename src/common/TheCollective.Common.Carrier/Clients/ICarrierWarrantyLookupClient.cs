﻿using TheCollective.Common.Carrier.CarrierWarrantyLookupApi;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    public interface ICarrierWarrantyLookupClient
    {
        CarrierApiCredentials ApiCredentials { set; }

        EntitlementResponse GetEntitlement(string serialNumber, string modelNumber);

        ServiceHistoryResponse GetServiceHistory(string serialNumber, string modelNumber);
    }
}
