﻿using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    public interface ICarrierClient
    {
        CarrierApiCredentials ApiCredentials { get; set; }

        T Delete<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class;

        /// <summary>
        /// Does a get request on Carrier API at requested url
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="actionUrl">the URL to perform the get operation</param>
        /// <returns></returns>
        T Get<T>(string actionUrl) where T : class, new();

        string GetBearerToken();

        T Post<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class;

        T Put<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class;
    }
}
