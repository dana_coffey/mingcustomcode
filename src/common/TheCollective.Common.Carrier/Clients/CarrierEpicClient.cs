﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using TheCollective.Common.Carrier.CarrierEpicApi;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    /// <summary>
    /// In order to use this class, you must copy paste the configuration lines (from this project app.config) in your final app/web.config
    /// </summary>
    public class CarrierEpicClient : ICarrierEpicClient
    {
        private const string DefaultUserBrands = "CA,BR,PY";
        private const string DefaultUserCompanyType = "?";
        private readonly ICarrierClient _carrierClient;

        public CarrierApiCredentials ApiCredentials
        {
            set => _carrierClient.ApiCredentials = value;
        }

        public CarrierEpicClient(ICarrierClient carrierClient)
        {
            _carrierClient = carrierClient;
        }

        public GetModelDataReply GetModelData(string modelNumber)
        {
            GetModelDataReply response;

            using (var client = GetEpicClient())
            {
                var modelDataRequest = new GetModelData
                {
                    ExpandMultiples = "Y",
                    ModelNumber = modelNumber,
                    UserBrands = DefaultUserBrands,
                    UserCompanyType = DefaultUserCompanyType
                };

                response = client.GetModelData(modelDataRequest);
            }

            return response;
        }

        public GetPartDataReply GetPartData(string partNumber)
        {
            try
            {
                GetPartDataReply response;
                using (var client = GetEpicClient())
                {
                    var partDataRequest = new GetPartData
                    {
                        PartNumber = partNumber,
                        UserBrands = DefaultUserBrands,
                        UserCompanyType = DefaultUserCompanyType
                    };

                    response = client.GetPartData(partDataRequest);
                }

                return response;
            }
            catch (Exception)
            {
            }

            return null;
        }

        private EPICServiceClient GetEpicClient()
        {
            var client = new EPICServiceClient("CarrierEpicApi_Endpoint");

            var bearerToken = _carrierClient.GetBearerToken();

            var hrmp = new HttpRequestMessageProperty();
            hrmp.Headers.Add("Authorization", $"Bearer {bearerToken}");

            // this line is mandatory otherwise, the line after will throw an exception
            var contextScope = new OperationContextScope(client.InnerChannel);
            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = hrmp;

            return client;
        }
    }
}
