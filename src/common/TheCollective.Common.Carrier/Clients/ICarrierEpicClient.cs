﻿using TheCollective.Common.Carrier.CarrierEpicApi;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    public interface ICarrierEpicClient
    {
        CarrierApiCredentials ApiCredentials { set; }

        GetModelDataReply GetModelData(string modelNumber);

        GetPartDataReply GetPartData(string partNumber);
    }
}
