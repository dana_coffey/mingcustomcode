﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    /// <summary>
    ///     Client interface to interact with Carrier API
    /// </summary>
    public class CarrierClient : ICarrierClient
    {
        private string _bearerToken;
        private DateTime _bearerTokenExpiration;

        public CarrierApiCredentials ApiCredentials { get; set; }

        public CarrierClient()
        {
            _bearerTokenExpiration = DateTime.MinValue;
        }

        public T Delete<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class
        {
            if (string.IsNullOrEmpty(actionUrl))
            {
                throw new ArgumentNullException(nameof(actionUrl));
            }

            using (var httpClient = GetHttpClientWithBearerToken())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Delete, actionUrl)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json")
                };

                return DeserializeResponse<T>(httpClient.SendAsync(requestMessage).Result);
            }
        }

        /// <summary>
        ///     Does a get request on Carrier API at requested url
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="actionUrl">the URL to perform the get operation</param>
        /// <returns></returns>
        public T Get<T>(string actionUrl) where T : class, new()
        {
            if (string.IsNullOrEmpty(actionUrl))
            {
                throw new ArgumentNullException(nameof(actionUrl));
            }

            using (var httpClient = GetHttpClientWithBearerToken())
            {
                return DeserializeResponse<T>(httpClient.GetAsync(actionUrl).Result);
            }
        }

        public string GetBearerToken()
        {
            if (DateTime.Now <= _bearerTokenExpiration)
            {
                return _bearerToken;
            }

            using (var httpClient = new HttpClient { BaseAddress = new Uri(ApiCredentials.BaseApiUrl) })
            {
                var credentials = string.Concat(ApiCredentials.BearerTokenUsername, ":", ApiCredentials.BearerTokenPassword);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Base64Encode(credentials));

                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", ApiCredentials.Username),
                    new KeyValuePair<string, string>("password", ApiCredentials.ApiPassword)
                });

                var bearerTokenResponse = DeserializeResponse<BearerTokenResponse>(httpClient.PostAsync("/auth/realms/hvac/tokens/grants/access", content).Result);
                if (!string.IsNullOrEmpty(bearerTokenResponse?.AccessToken))
                {
                    _bearerToken = bearerTokenResponse.AccessToken;
                    _bearerTokenExpiration = DateTime.Now.AddSeconds(bearerTokenResponse.ExpiresIn - 120); // Buffer of 2 minutes
                }
            }

            return _bearerToken;
        }

        public T Post<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class
        {
            if (string.IsNullOrEmpty(actionUrl))
            {
                throw new ArgumentNullException(nameof(actionUrl));
            }

            using (var httpClient = GetHttpClientWithBearerToken())
            {
                return DeserializeResponse<T>(httpClient.PostAsJsonAsync(actionUrl, requestModel).Result);
            }
        }

        public T Put<T, TR>(string actionUrl, TR requestModel)
            where T : class, new()
            where TR : class
        {
            if (string.IsNullOrEmpty(actionUrl))
            {
                throw new ArgumentNullException(nameof(actionUrl));
            }

            using (var httpClient = GetHttpClientWithBearerToken())
            {
                return DeserializeResponse<T>(httpClient.PutAsJsonAsync(actionUrl, requestModel).Result);
            }
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        private static T DeserializeResponse<T>(HttpResponseMessage response) where T : class, new()
        {
            var jsonString = response?.Content?.ReadAsStringAsync().Result ?? string.Empty;

            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        private HttpClient GetHttpClientWithBearerToken()
        {
            var httpClient = new HttpClient { BaseAddress = new Uri(ApiCredentials.BaseApiUrl) };

            var bearerToken = GetBearerToken();

            if (string.IsNullOrEmpty(bearerToken))
            {
                throw new ArgumentException("Unable to get bearer token");
            }

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);

            return httpClient;
        }
    }
}
