﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using TheCollective.Common.Carrier.CarrierWarrantyLookupApi;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Clients
{
    public class CarrierWarrantyLookupClient : ICarrierWarrantyLookupClient
    {
        private readonly ICarrierClient _carrierClient;

        public CarrierApiCredentials ApiCredentials
        {
            set => _carrierClient.ApiCredentials = value;
        }

        public CarrierWarrantyLookupClient(ICarrierClient carrierClient)
        {
            _carrierClient = carrierClient;
        }

        public EntitlementResponse GetEntitlement(string serialNumber, string modelNumber)
        {
            EntitlementResponse response;

            using (var client = GetWarrantyLookupApiClient())
            {
                var modelDataRequest = new EntitlementRequest
                {
                    serialNumber = serialNumber,
                    modelNumber = modelNumber,
                    serviceAdministrator = Constants.Warranty.WarrantyCarrierCompanyCode,
                    version = Constants.Warranty.ApiVersion
                };
                response = client.getEntitlementDetail(modelDataRequest);
            }

            return response;
        }

        public ServiceHistoryResponse GetServiceHistory(string serialNumber, string modelNumber)
        {
            ServiceHistoryResponse response;

            using (var client = GetWarrantyLookupApiClient())
            {
                var modelDataRequest = new ServiceHistoryRequest
                {
                    serialNumber = serialNumber,
                    modelNumber = modelNumber,
                    serviceAdministrator = Constants.Warranty.WarrantyCarrierCompanyCode
                };
                response = client.getServiceHistory(modelDataRequest);
            }

            return response;
        }

        private ProductEntitlementServicePortTypeClient GetWarrantyLookupApiClient()
        {
            var client = new ProductEntitlementServicePortTypeClient("CarrierWarrantyLookupApi_Endpoint");

            var bearerToken = _carrierClient.GetBearerToken();

            var httpRequestMessageProperty = new HttpRequestMessageProperty();
            httpRequestMessageProperty.Headers.Add("Authorization", $"Bearer {bearerToken}");

            // this line is mandatory otherwise, the line after will throw an exception
            var contextScope = new OperationContextScope(client.InnerChannel);
            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestMessageProperty;

            return client;
        }
    }
}
