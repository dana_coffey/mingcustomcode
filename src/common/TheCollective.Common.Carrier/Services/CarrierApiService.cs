﻿using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Services
{
    public class CarrierApiService : ICarrierApiService
    {
        private readonly ICarrierClient _carrierClient;

        public CarrierApiCredentials ApiCredentials
        {
            get => _carrierClient.ApiCredentials;
            set => _carrierClient.ApiCredentials = value;
        }

        public CarrierApiService(ICarrierClient carrierClient)
        {
            _carrierClient = carrierClient;
        }

        public DocumentList GetDocuments(string sku)
        {
            var actionUrl = $"/apps/catalog/product/{sku}/documents?types=technical-literature";

            return _carrierClient.Get<DocumentList>(actionUrl);
        }

        public ProductModel GetProduct(string sku)
        {
            var actionUrl = $"/apps/catalog/product/{sku}/attributes";

            return _carrierClient.Get<ProductModel>(actionUrl);
        }
    }
}
