﻿using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Models.Warranty;

namespace TheCollective.Common.Carrier.Services
{
    public interface ICarrierWarrantyLookupApiService
    {
        CarrierApiCredentials ApiCredentials { set; }

        WarrantyResult GetProductWarrantyInformation(string serialNumber, string modelNumber);

        void TranslateWarrantyLabels(WarrantyResult warrantyResult);
    }
}
