﻿using System.Collections.Generic;
using TheCollective.Common.Carrier.Models;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Services
{
    public interface ICarrierEpicApiService
    {
        CarrierApiCredentials ApiCredentials { set; }

        List<PartData> GetParts(string partNumber);

        List<PartHistory> GetSupersededParts(string partNumber);
    }
}
