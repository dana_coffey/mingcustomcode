﻿using System.Collections.Generic;
using System.Linq;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Services
{
    public class CarrierEpicApiService : ICarrierEpicApiService
    {
        private readonly ICarrierEpicClient _carrierEpicClient;

        public CarrierApiCredentials ApiCredentials
        {
            set => _carrierEpicClient.ApiCredentials = value;
        }

        public CarrierEpicApiService(ICarrierEpicClient carrierEpicClient)
        {
            _carrierEpicClient = carrierEpicClient;
        }

        public List<PartData> GetParts(string partNumber)
        {
            var response = _carrierEpicClient.GetModelData(partNumber);
            if (response?.BomLine?.Any() ?? false)
            {
                return response.BomLine.Select(x => new PartData
                {
                    Description = x.ItemDescription,
                    Group = response.ModelGroup?.FirstOrDefault(group => group.GroupCode == x.GroupCode)?.GroupDescription ?? string.Empty,
                    HasHistory = x.HistoryFlag.ToUpper() == "Y",
                    ItemNumber = int.TryParse(x.ItemNumber, out var itemNumberAsInt) ? itemNumberAsInt : 0,
                    PackageNumber = x.PartPackages,
                    PartNumber = x.PartNumber,
                    Quantity = int.TryParse(x.Quantity, out var quantityAsInt) ? quantityAsInt : 0
                }).ToList();
            }

            return new List<PartData>();
        }

        /// <summary>
        /// Returns list of superseded (my "partNumber" is a replacement for this list of parts)
        /// </summary>
        /// <param name="partNumber">The part for which we want the list of superseded part</param>
        /// <returns></returns>
        public List<PartHistory> GetSupersededParts(string partNumber)
        {
            var result = new List<PartHistory>();

            var response = _carrierEpicClient.GetPartData(partNumber);

            if (response?.PartHistory != null)
            {
                result.AddRange(response.PartHistory.Select(dataReplyPartHistory => new PartHistory
                {
                    HistoryNote = dataReplyPartHistory.HistoryNote,
                    Sequence = dataReplyPartHistory.Sequence,
                    PartNumber = dataReplyPartHistory.ChainPartNumber
                }));
            }

            if (result.Any())
            {
                // if I'm looking a supersede, it will be on top of the list
                if (result[0].PartNumber == partNumber)
                {
                    // I just want to returns the list of superseded part
                    result.RemoveAt(0);
                }
                else
                {
                    // if i'm looking a superseded, i don't want to returns something
                    result.Clear();
                }
            }

            return result;
        }
    }
}
