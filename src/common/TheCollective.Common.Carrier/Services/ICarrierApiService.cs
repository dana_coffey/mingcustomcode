﻿using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Services
{
    public interface ICarrierApiService
    {
        CarrierApiCredentials ApiCredentials { get; set; }

        DocumentList GetDocuments(string sku);

        ProductModel GetProduct(string sku);
    }
}
