﻿using System;
using System.Collections.Generic;
using System.Linq;
using Insite.Common.Dependencies;
using Insite.Core.Interfaces.Localization;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;
using TheCollective.Common.Carrier.Models.Warranty;

namespace TheCollective.Common.Carrier.Services
{
    public class CarrierWarrantyLookupApiService : ICarrierWarrantyLookupApiService
    {
        private readonly ICarrierWarrantyLookupClient _carrierWarrantyLookupClient;
        private readonly ITranslationLocalizer _translationLocalizer;

        public CarrierApiCredentials ApiCredentials
        {
            set => _carrierWarrantyLookupClient.ApiCredentials = value;
        }

        public CarrierWarrantyLookupApiService(ICarrierWarrantyLookupClient carrierWarrantyLookupClient)
        {
            _carrierWarrantyLookupClient = carrierWarrantyLookupClient;
            _translationLocalizer = DependencyLocator.Current.GetInstance<ITranslationLocalizer>();
        }

        public WarrantyResult GetProductWarrantyInformation(string serialNumber, string modelNumber)
        {
            WarrantyResult warrantyResult;

            try
            {
                warrantyResult = GetWarrantyData(serialNumber, modelNumber);
                if (warrantyResult?.Type == WarrantyResultType.WarrantyData)
                {
                    modelNumber = warrantyResult.Data?.Sections?.SelectMany(x => x.Details)?.FirstOrDefault(y => y.Keys?.FirstOrDefault()?.Equals(Constants.Warranty.ModelNumberDataKey, StringComparison.InvariantCultureIgnoreCase) ?? false)?.Values?.FirstOrDefault() ?? string.Empty;

                    if (!string.IsNullOrEmpty(modelNumber))
                    {
                        IncludeServiceOnWarranty(serialNumber, modelNumber, warrantyResult);
                    }
                }
            }
            catch (Exception)
            {
                warrantyResult = new WarrantyResult
                {
                    Type = WarrantyResultType.Error
                };
            }

            return warrantyResult;
        }

        public void TranslateWarrantyLabels(WarrantyResult warrantyResult)
        {
            foreach (var section in warrantyResult?.Data?.Sections ?? new List<WarrantySection>())
            {
                section.Value = _translationLocalizer.TranslateLabel($"Warranty_{section.Key}");
                foreach (var sectionDetail in section.Details)
                {
                    sectionDetail.Names = sectionDetail.Keys?.Select(x => _translationLocalizer.TranslateLabel($"Warranty_{x}")).ToList();
                }
            }
        }

        private WarrantyResult GetWarrantyData(string serialNumber, string modelNumber)
        {
            var apiEntitlementResult = _carrierWarrantyLookupClient.GetEntitlement(serialNumber, modelNumber);

            if (apiEntitlementResult != null && string.Equals(apiEntitlementResult.msgStatus, Constants.Warranty.ApiErrorStatus, StringComparison.InvariantCultureIgnoreCase))
            {
                return new WarrantyResult { Type = string.Equals(apiEntitlementResult.messageDetails.FirstOrDefault()?.errorText, Constants.Warranty.NotFoundErrorMessage) ? WarrantyResultType.NotFound : WarrantyResultType.Error };
            }

            if (apiEntitlementResult?.entitlementData != null)
            {
                // Validate if the result is a list of model numbers for more precise lookup
                if (string.Equals(apiEntitlementResult.entitlementData.FirstOrDefault()?.header, Constants.Warranty.LookupModelsHeaderKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    return new WarrantyResult
                    {
                        Type = WarrantyResultType.LookupModels,
                        ModelNumbers = apiEntitlementResult.entitlementData.SelectMany(x => x.detail.Where(detail => string.Equals(detail.dataKey, Constants.Warranty.ModelNumberDataKey, StringComparison.InvariantCultureIgnoreCase)).Select(detail => detail.dataDescription)).ToList()
                    };
                }

                var warrantyResult = new WarrantyResult
                {
                    Type = WarrantyResultType.WarrantyData,
                    Data = new WarrantyData()
                };

                foreach (var entitlement in apiEntitlementResult.entitlementData.Where(x => !Constants.Warranty.EntitlementDataHeaderIgnoreList.Contains(x.header) && (x.detail?.Any() ?? false) && !x.detail.All(detail => string.IsNullOrEmpty(detail.dataDescription))))
                {
                    var entitlementDetails = entitlement.detail.Where(x => !Constants.Warranty.DataKeysIgnoreList.Contains(x.dataKey));

                    // Clean up weird data not for the table
                    if (Constants.Warranty.WarrantyHeaderKeys.Contains(entitlement.header))
                    {
                        entitlementDetails = entitlementDetails.Where(x => x.dataKey == "lineNo");
                        foreach (var warrantyGroup in entitlementDetails.GroupBy(x => x.detailExt.FirstOrDefault(detailExt => detailExt.extDataKey == Constants.Warranty.WarrantyTypeKey)?.extDataDescription ?? string.Empty).OrderBy(x => x.Key))
                        {
                            var section = new WarrantySection { Key = $"{entitlement.header} ({warrantyGroup.Key})" };

                            section.Details.Add(new WarrantySectionRow { Keys = warrantyGroup.First().detailExt.Where(x => !Constants.Warranty.DataKeysIgnoreList.Contains(x.extDataKey)).Select(x => x.extDataKey).ToList() });

                            foreach (var warrantyGroupDetail in warrantyGroup)
                            {
                                section.Details.Add(new WarrantySectionRow { Values = warrantyGroupDetail.detailExt.Where(x => !Constants.Warranty.DataKeysIgnoreList.Contains(x.extDataKey)).Select(x => x.extDataDescription).ToList() });
                            }

                            warrantyResult.Data.Sections.Add(section);
                        }
                    }
                    else
                    {
                        var section = new WarrantySection { Key = entitlement.header };

                        foreach (var entitlementDetail in entitlementDetails)
                        {
                            if (entitlementDetail.detailExt?.Any() ?? false)
                            {
                                section.Details.Add(new WarrantySectionRow { Keys = entitlementDetail.detailExt.Where(x => !Constants.Warranty.DataKeysIgnoreList.Contains(x.extDataKey)).Select(x => x.extDataKey).ToList() });
                                section.Details.Add(new WarrantySectionRow { Values = entitlementDetail.detailExt.Where(x => !Constants.Warranty.DataKeysIgnoreList.Contains(x.extDataKey)).Select(x => x.extDataDescription).ToList() });
                            }
                            else
                            {
                                section.Details.Add(new WarrantySectionRow { Keys = new List<string> { entitlementDetail.dataKey }, Values = new List<string> { entitlementDetail.dataDescription } });
                            }
                        }

                        warrantyResult.Data.Sections.Add(section);
                    }
                }

                return warrantyResult;
            }

            return new WarrantyResult { Type = WarrantyResultType.NotFound };
        }

        private void IncludeServiceOnWarranty(string serialNumber, string modelNumber, WarrantyResult warrantyResult)
        {
            var service = _carrierWarrantyLookupClient.GetServiceHistory(serialNumber, modelNumber);
            if (service != null)
            {
                if (warrantyResult == null)
                {
                    warrantyResult = new WarrantyResult();
                }

                if (warrantyResult.Data == null)
                {
                    warrantyResult.Data = new WarrantyData();
                }

                if (service.serviceData != null)
                {
                    foreach (var claim in service.serviceData)
                    {
                        var claimDetails = new List<WarrantySectionRow>();
                        var headerIndex = true;

                        // Insert Claim Info
                        claimDetails.Add(new WarrantySectionRow { Keys = new List<string> { nameof(claim.claimNumber) }, Values = new List<string> { claim.claimNumber, Constants.Warranty.ForceEmptyValue } });
                        claimDetails.Add(new WarrantySectionRow { Keys = new List<string> { nameof(claim.serviceDate) }, Values = new List<string> { claim.serviceDate.ToShortDateString(), Constants.Warranty.ForceEmptyValue } });
                        claimDetails.Add(new WarrantySectionRow { Keys = new List<string> { nameof(claim.serviceExplanation) }, Values = new List<string> { claim.serviceExplanation, Constants.Warranty.ForceEmptyValue } });

                        // Insert Services Info
                        if (claim.service != null)
                        {
                            foreach (var serviceJob in claim.service)
                            {
                                claimDetails.Add(new WarrantySectionRow { Keys = new List<string>() { Constants.Warranty.ServiceInfo }, Values = new List<string> { serviceJob.dataKey, serviceJob.dataDescription } });
                            }
                        }

                        // Insert Parts Info
                        if (claim.parts != null)
                        {
                            foreach (var part in claim.parts)
                            {
                                var partRow = new WarrantySectionRow { Keys = new List<string>(), Values = new List<string>() };
                                if (headerIndex)
                                {
                                    partRow.Keys.Add(nameof(part.partNumber));
                                    partRow.Keys.Add(nameof(part.partName));
                                    partRow.Keys.Add(nameof(part.partQuantity));

                                    claimDetails.Add(partRow);
                                    headerIndex = false;

                                    partRow = new WarrantySectionRow { Keys = new List<string>(), Values = new List<string>() };
                                }

                                partRow.Values.Add(part.partNumber);
                                partRow.Values.Add(part.partName);
                                partRow.Values.Add(part.partQuantity.ToString());

                                claimDetails.Add(partRow);
                            }
                        }

                        warrantyResult.Data.Sections.Add(new WarrantySection { Key = Constants.Warranty.ServiceHistory, Value = Constants.Warranty.ServiceHistory, Details = claimDetails });
                    }
                }
            }
        }
    }
}
