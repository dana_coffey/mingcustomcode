﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier
{
    public partial class Constants
    {
        public class Warranty
        {
            public const string ApiErrorStatus = "ERROR";
            public const string ApiVersion = "1.0";
            public const string ForceEmptyValue = " ";
            public const string LookupModelsHeaderKey = "Multiple Model Returned For Serial, Please Select Specific One";
            public const string ModelNumberDataKey = "modelNumber";
            public const string NotFoundErrorMessage = "Can't fetch entitlement detail.Please make sure you are sending correct request data.";
            public const string ServiceHistory = "claim";
            public const string ServiceInfo = "serviceInfo";
            public const string WarrantyCarrierCompanyCode = "92";
            public const string WarrantyTypeKey = "originalEquipmentOwner";
            public static List<string> DataKeysIgnoreList => new List<string> { "version", "labelProperties", "soldToDistributorName", "soldToDistributorNumber", "soldToDistributorCity", "soldToDistributorState" };
            public static List<string> EntitlementDataHeaderIgnoreList => new List<string> { "Service Bulletins Info", "Preauthorization" };
            public static List<string> WarrantyHeaderKeys => new List<string> { "Entitlement Warranty Info", "Entitlement Standard Warranty Info", "Entitlement Extended Warranty Info" };
        }
    }
}
