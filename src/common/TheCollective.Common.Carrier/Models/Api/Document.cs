﻿using System;
using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Api
{
    public class Document
    {
        public List<string> Brands { get; set; }
        public string ContentId { get; set; }
        public bool Current { get; set; }
        public string FileExtension { get; set; }
        public string Id { get; set; }
        public List<string> Locales { get; set; }
        public string MimeType { get; set; }
        public string PartNumber { get; set; }
        public DateTime PrintDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime ReleasedDate { get; set; }
        public string SubType { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}
