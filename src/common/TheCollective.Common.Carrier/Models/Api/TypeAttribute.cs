﻿namespace TheCollective.Common.Carrier.Models.Api
{
    public class TypeAttribute
    {
        public bool FilterBy { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string UnitOfMeasure { get; set; }
        public string Urn { get; set; }
        public string ValueType { get; set; }
    }
}
