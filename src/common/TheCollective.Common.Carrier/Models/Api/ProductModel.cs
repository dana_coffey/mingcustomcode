﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Api
{
    public class ProductModel
    {
        public bool Active { get; set; }
        public Dictionary<string, List<ProductAttributes>> Attributes { get; set; }
        public List<string> BrandCodes { get; set; }
        public List<string> BrandNames { get; set; }
        public string Division { get; set; }
        public ProductFamily FamilyProductOf { get; set; }
        public string Urn { get; set; }
    }
}
