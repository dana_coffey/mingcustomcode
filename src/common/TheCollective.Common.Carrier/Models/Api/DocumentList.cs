﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TheCollective.Common.Carrier.Models.Api
{
    public class DocumentList
    {
        [JsonProperty("items")]
        public List<Document> Documents { get; set; }
    }
}
