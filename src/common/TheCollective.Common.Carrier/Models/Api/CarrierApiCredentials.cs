﻿namespace TheCollective.Common.Carrier.Models.Api
{
    public class CarrierApiCredentials
    {
        public string ApiPassword { get; set; }
        public string BaseApiUrl { get; set; }
        public string BearerTokenPassword { get; set; }
        public string BearerTokenUsername { get; set; }
        public string Username { get; set; }
    }
}
