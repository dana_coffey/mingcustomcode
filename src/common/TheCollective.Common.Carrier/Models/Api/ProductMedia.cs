﻿namespace TheCollective.Common.Carrier.Models.Api
{
    public class ProductMedia
    {
        public string Brand { get; set; }
        public int Height { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string Urn { get; set; }
        public int Width { get; set; }
    }
}
