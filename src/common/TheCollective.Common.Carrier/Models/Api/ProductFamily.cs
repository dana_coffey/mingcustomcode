﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Api
{
    public class ProductFamily
    {
        public bool Active { get; set; }
        public List<string> BrandCodes { get; set; }
        public List<string> Categories { get; set; }
        public string Division { get; set; }
        public List<ProductModel> FamilyProducts { get; set; }
        public Dictionary<string, string> FeaturesTexts { get; set; }
        public Dictionary<string, List<ProductMedia>> Media { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Names { get; set; }
        public Dictionary<string, string> OverviewTexts { get; set; }
        public Dictionary<string, List<string>> Relationships { get; set; }
        public string Urn { get; set; }
    }
}
