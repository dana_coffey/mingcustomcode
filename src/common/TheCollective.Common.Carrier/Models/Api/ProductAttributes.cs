﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Api
{
    public class ProductAttributes
    {
        public TypeAttribute Type { get; set; }
        public string Urn { get; set; }
        public string Value { get; set; }
        public List<string> Values { get; set; }
    }
}
