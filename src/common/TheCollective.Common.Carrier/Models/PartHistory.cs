﻿namespace TheCollective.Common.Carrier.Models
{
    public class PartHistory
    {
        public string HistoryNote { get; set; }
        public string PartNumber { get; set; }
        public string Sequence { get; set; }
    }
}
