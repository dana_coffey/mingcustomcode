﻿namespace TheCollective.Common.Carrier.Models
{
    public class PartData
    {
        public string Description { get; set; }
        public string Group { get; set; }
        public bool HasHistory { get; set; }
        public int ItemNumber { get; set; }
        public string PackageNumber { get; set; }
        public string PartNumber { get; set; }
        public int Quantity { get; set; }
    }
}
