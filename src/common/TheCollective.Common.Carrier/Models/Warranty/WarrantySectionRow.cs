﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Warranty
{
    public class WarrantySectionRow
    {
        public List<string> Keys { get; set; }
        public List<string> Names { get; set; }
        public List<string> Values { get; set; }
    }
}
