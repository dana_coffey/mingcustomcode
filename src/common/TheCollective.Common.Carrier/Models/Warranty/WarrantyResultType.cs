﻿namespace TheCollective.Common.Carrier.Models.Warranty
{
    public enum WarrantyResultType
    {
        LookupModels,
        WarrantyData,
        NotFound,
        Error
    }
}
