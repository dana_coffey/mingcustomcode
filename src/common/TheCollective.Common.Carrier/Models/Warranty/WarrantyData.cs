﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Warranty
{
    public class WarrantyData
    {
        public List<WarrantySection> Sections { get; set; } = new List<WarrantySection>();
    }
}
