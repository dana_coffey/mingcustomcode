﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Warranty
{
    public class WarrantyResult
    {
        public WarrantyData Data { get; set; }
        public List<string> ModelNumbers { get; set; }
        public WarrantyResultType Type { get; set; }
    }
}
