﻿using System.Collections.Generic;

namespace TheCollective.Common.Carrier.Models.Warranty
{
    public class WarrantySection
    {
        public List<WarrantySectionRow> Details { get; set; } = new List<WarrantySectionRow>();
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
