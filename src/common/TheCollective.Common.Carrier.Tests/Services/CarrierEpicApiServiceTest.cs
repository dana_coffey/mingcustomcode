﻿using System.Linq;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using TheCollective.Common.Carrier.CarrierEpicApi;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Services;

namespace TheCollective.Common.Carrier.Tests.Services
{
    [TestFixture]
    public class CarrierEpicApiServiceTest
    {
        private CarrierEpicApiService _carrierEpicApiService;
        private Mock<ICarrierEpicClient> _mockCarrierEpicClient;

        [SetUp]
        public void SetUp()
        {
            _mockCarrierEpicClient = new Mock<ICarrierEpicClient>();
            _carrierEpicApiService = new CarrierEpicApiService(_mockCarrierEpicClient.Object);
        }

        [Test]
        public void GetSupersededParts_PartIsSupersede_ReturnsListOfSuperseded()
        {
            // Arrange
            var partNumber = "320981-752";

            var apiDataReply = new GetPartDataReply
            {
                PartHistory = new GetPartDataReplyPartHistory[4]
            };
            apiDataReply.PartHistory[0] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = partNumber,
                OldPartNumber = "⤵ Old below",
                Sequence = "1"
            };
            apiDataReply.PartHistory[1] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = "320981-751",
                HistoryNote = "RSM PER C.KAISER MAIL DTD.8/13/98 (USED ON FK4A)",
                NewPartNumber = "320981-752",
                OldPartDescription = "CONVERSION KIT",
                OldPartNumber = "320981-751",
                Sequence = "2"
            };
            apiDataReply.PartHistory[2] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = "CESO130007-00",
                HistoryNote = "PER INDY 10/16/03",
                NewPartNumber = "320981-752",
                OldPartDescription = "CIRCUIT BOARD",
                OldPartNumber = "CESO130007-00",
                Sequence = "3"
            };
            apiDataReply.PartHistory[3] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = "HK61GA002",
                HistoryNote = "RSM PER C.KAISER CC MAIL 9/24/98",
                NewPartNumber = "320981-752",
                OldPartDescription = "CIRCUIT BOARD",
                OldPartNumber = "HK61GA002",
                Sequence = "3"
            };

            _mockCarrierEpicClient.Setup(m => m.GetPartData(partNumber)).Returns(apiDataReply);

            // Act
            var result = _carrierEpicApiService.GetSupersededParts(partNumber);

            // Assert
            result.Should().NotBeEmpty();
            result.Count.Should().Be(3);
            result.Any(p => p.PartNumber == partNumber).Should().BeFalse();
        }

        [Test]
        public void GetSupersededParts_PartIsSuperseded_ReturnsEmptyList()
        {
            // Arrange
            var partNumber = "320981-751";

            var apiDataReply = new GetPartDataReply
            {
                PartHistory = new GetPartDataReplyPartHistory[2]
            };
            apiDataReply.PartHistory[0] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = "320981-752",
                HistoryNote = "RSM PER C.KAISER MAIL DTD.8/13/98 (USED ON FK4A)",
                NewPartNumber = "320981-752",
                OldPartDescription = "CONVERSION KIT",
                OldPartNumber = "320981-751",
                Sequence = "1"
            };
            apiDataReply.PartHistory[1] = new GetPartDataReplyPartHistory
            {
                ChainPartNumber = "320981-751",
                NewPartNumber = "⤴ New above",
                Sequence = "2"
            };

            _mockCarrierEpicClient.Setup(m => m.GetPartData(partNumber)).Returns(apiDataReply);

            // Act
            var result = _carrierEpicApiService.GetSupersededParts(partNumber);

            // Assert
            result.Should().BeEmpty();
        }

        [Test]
        public void GetSupersededParts_PartWithoutSupersede_ReturnsEmptyList()
        {
            // Arrange
            var partNumber = "320981-000";

            var apiDataReply = new GetPartDataReply
            {
                PartHistory = new GetPartDataReplyPartHistory[0]
            };
            _mockCarrierEpicClient.Setup(m => m.GetPartData(partNumber)).Returns(apiDataReply);

            // Act
            var result = _carrierEpicApiService.GetSupersededParts(partNumber);

            // Assert
            result.Should().BeEmpty();
        }
    }
}
