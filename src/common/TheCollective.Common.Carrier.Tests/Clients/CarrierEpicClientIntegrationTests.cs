﻿using FluentAssertions;
using NUnit.Framework;
using TheCollective.Common.Carrier.Clients;
using TheCollective.Common.Carrier.Models.Api;

namespace TheCollective.Common.Carrier.Tests.Clients
{
    [TestFixture]
    public class CarrierEpicClientIntegrationTests
    {
        [SetUp]
        public void SetUp()
        {
            var apiCredentials = new CarrierApiCredentials
            {
                ApiPassword = "zH7hmnQ#ZaqCTe3",
                BaseApiUrl = "https://services.ccs.utc.com",
                Username = "ming-10010",
                BearerTokenPassword = "f057c1ad-6e2e-4c66-849a-86347beed228"
            };

            _carrierEpicClient = new CarrierEpicClient(new CarrierClient()) { ApiCredentials = apiCredentials };
        }

        private CarrierEpicClient _carrierEpicClient;

        [Test]
        public void GetPartData()
        {
            // Arrange
            var partNumber = "P298-001";

            // act
            var response = _carrierEpicClient.GetPartData(partNumber);

            // assert
            response.Should().NotBeNull();
            response.PartNumber.Should().Be(partNumber);
        }
    }
}
