//-------------------------------------
//-- Table - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};

	// Public -----------------------------------------------------------------//

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		$.extend(true, $.fn.dataTable.defaults, {
			language: app.table.lang[app.env.lang],
			dom: '<"dt-top"<"dt-filter"f><"dt-pager"p>><"dt-table"<"table-overflow"t>><"dt-bottom"<"dt-pager"p>>'
		});
	};

	// Private ----------------------------------------------------------------//

	// Initialize -------------------------------------------------------------//

	local.cache();
	// DOM Ready
	// $.when(DOM_PARSE).done(() => {});

})();
