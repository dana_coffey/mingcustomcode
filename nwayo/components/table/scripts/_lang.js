//-------------------------------------
//-- Table - Lang
//-------------------------------------

(() => {
	'use strict';

	app.table = app.table || {};
	app.table.lang = {
		en: {
			searchPlaceholder: 'Search within results',
			zeroRecords: 'No results available',
			emptyTable: 'No results available',
			paginate: {
				first: 'First',
				previous: 'Prev',
				next: 'Next',
				last: 'Last'
			}
		}
	};

	/*
	fr: {
		processing: 'Traitement en cours...',
		decimal: ',',
		search: 'Rechercher&nbsp;:',
		lengthMenu: 'Afficher _MENU_ &eacute;l&eacute;ments',
		info: 'Affichage de l\'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments',
		infoEmpty: 'Affichage de l\'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments',
		infoFiltered: '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
		infoPostFix: '',
		loadingRecords: 'Chargement en cours...',
		zeroRecords: 'Aucun &eacute;l&eacute;ment &agrave; afficher',
		emptyTable: 'Aucune donnée disponible',
		paginate: {
			first: 'Premier',
			previous: 'Pr&eacute;c&eacute;dent',
			next: 'Suivant',
			last: 'Dernier'
		},
		aria: {
			sortAscending: ': activer pour trier la colonne par ordre croissant',
			sortDescending: ': activer pour trier la colonne par ordre décroissant'
		}
	}
	*/

})();
