//-------------------------------------
//-- Pick a date - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};

	// Cache ------------------------------------------------------------------//

	local.cache = () => {

		// Languages
		const _lang = {
			fr: {
				monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
				monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
				weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
				weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
				today: 'Aujourd\'hui',
				clear: 'Effacer',
				firstDay: 1
			}
		};

		// Set Language
		if (_lang[app.env.lang]) {
			$.extend($.fn.pickadate.defaults, _lang[app.env.lang]);
		}

		// Defaults
		$.extend(true, $.fn.pickadate.defaults, {
			container: __.$body,
			close: ' ',
			selectYears: true,
			selectMonths: true,
			format: 'yyyy-mm-dd',
			klass: {
				active: 'on',
				picker: 'picker',
				opened: 'open',
				focused: 'focus',
				holder: 'picker-holder',
				frame: 'picker-frame',
				wrap: 'picker-wrap',
				box: 'picker-box',
				header: 'picker-header',
				table: 'picker-table',
				footer: 'picker-footer',
				buttonClear: 'picker-clear',
				buttonToday: 'picker-today',
				buttonClose: 'picker-close',
				disabled: 'disabled',
				selected: 'selected',
				highlighted: 'highlight',
				now: 'today',
				infocus: 'infocus',
				outfocus: 'outfocus',
				navPrev: 'picker-nav prev',
				navNext: 'picker-nav next',
				navDisabled: 'disabled',
				month: 'picker-month',
				year: 'picker-year',
				selectMonth: 'picker-select-month',
				selectYear: 'picker-select-year',
				weekdays: 'picker-weekday',
				day: 'picker-day'
			},
			onStart: function() {
				this.$root
					.find(`.${$.fn.pickadate.defaults.klass.holder}`).addClass('reveal-overlay').end()
					.find(`.${$.fn.pickadate.defaults.klass.frame}`).addClass('reveal modal').end()
					.find(`.${$.fn.pickadate.defaults.klass.wrap}`).addClass('modal-wrapper');
			},
			onRender: function() {
				this.$root
					.find(`.${$.fn.pickadate.defaults.klass.buttonClear}`).addClass('button other small').end()
					.find(`.${$.fn.pickadate.defaults.klass.buttonToday}`).addClass('button small').end()
					.find(`.${$.fn.pickadate.defaults.klass.buttonClose}`).addClass('modal-close').end()
					.find(`select`).addClass('small');
			}
		});

	};

	// Subscribe --------------------------------------------------------------//

	local.subscribe = () => {
		PubSub.subscribe('nwayo.pickadate.open', () => {
			app.util.lockScroll('datepicker', true);
		});

		PubSub.subscribe('nwayo.pickadate.close', () => {
			global.setTimeout(() => { app.util.lockScroll('datepicker', false); }, konstan.transition.reveal);
		});
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	local.subscribe();

})();
