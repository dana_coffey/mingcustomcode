//-------------------------------------
//-- CKEditor
//-------------------------------------

//= require /components/ckeditor/scripts/util

/* eslint-disable camelcase, prefer-template, dot-notation, no-param-reassign, no-unused-vars */

(() => {
	'use strict';

	const plugins = {
		ckeditor: {
			base: ['widget', 'lineutils'],
			custom: ['WidgetYoutube']
		}
	};

	if (global.CKEDITOR && app.CKEditor) {

		global.CKEDITOR.dtd.$removeEmpty['p'] = true;

		app.CKEditor.options = {
			autoParagraph: false,
			allowedContent: true,
			fillEmptyBlocks: false,
			extraPlugins: 'imagemaps,' + app.CKEditor.addPlugins(plugins),
			fullPage: false,
			contentsCss: '/nwayo-build/admin/styles/editor.css',
			toolbar: [
				['SwitchBar', '-', 'Source'],
				['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
				['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll'],
				plugins.ckeditor.custom,
				'/',
				['Bold', 'Italic', '-', 'Subscript', 'Superscript'],
				['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
				['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
				['Link', 'Unlink', 'Anchor'],
				['Image', 'Table', 'HorizontalRule'],
				'/',
				['RemoveFormat', 'Styles', 'Format', 'ShowBlocks']
			],
			stylesSet: [
				{ name: 'Small', element: 'span', attributes: { 'class': 'small-text' } },
				{ name: 'Large', element: 'span', attributes: { 'class': 'large-text' } },
				{ name: 'Larger', element: 'span', attributes: { 'class': 'larger-text' } },
				{ name: 'Button', element: 'a', attributes: { 'class': 'btn' } },
				{ name: 'Title', element: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'], attributes: { 'class': 'title' } }
			],
			format_tags: 'p;h2;h3;h4;h5;h6'
		};
	}

})();
