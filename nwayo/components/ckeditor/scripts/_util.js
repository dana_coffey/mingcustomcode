//-------------------------------------
//-- Admin CKEditor
//-------------------------------------

//= require /components/nwayo/scripts/wrapper-jsrender

/* eslint-disable no-undef, prefer-template, dot-notation */

(() => {
	'use strict';

	// Public class
	app.CKEditor = class {

		// Add custom plugins from array
		static addCustomExternals(plugins, component) {
			plugins.forEach((name) => {
				CKEDITOR.plugins.addExternal(name, app.path.images + '/' + component + '/', '../../scripts/' + name + '.js');
			});
		}

		// Add built-in plugins from array
		static addExternals(plugins, component) {
			plugins.forEach((name) => {
				CKEDITOR.plugins.addExternal(name, app.path.raw + '/' + component + '/plugins/' + name + '/');
			});
		}

		// Add plugins from object, return an "extraPlugins" string
		static addPlugins(pluginsObj) {
			let _extraPlugins = [];

			Object.keys(pluginsObj).forEach((component) => {
				const _componentName = component;
				const _component = pluginsObj[_componentName];

				if (_component.base) {
					app.CKEditor.addExternals(_component.base, _componentName);
					_extraPlugins = [..._extraPlugins, ..._component.base];
				}

				if (_component.custom) {
					app.CKEditor.addCustomExternals(_component.custom, _componentName);
					_extraPlugins = [..._extraPlugins, ..._component.custom];
				}
			});

			return _extraPlugins.join();
		}

		// Add widget
		static addWidget(options) {

			const fields = [];
			options.fields.forEach((field) => {
				fields[field] = { selector:`[data-ckwidget-field="${field}"]` };
			});

			CKEDITOR.plugins.add(options.name, {
				requires: 'widget',
				icons:    options.name.toLowerCase(),
				init:     function(editor) {
					const widgetOptions = {
						button:	   options.desc,
						template:  options.tmpl.replace(/\t|\n/g, ''),
						editables: fields,
						upcast:    (element) => {
							return $(element.getOuterHtml()).data('ckwidget') === options.name;
						}
					};

					if (options.data) {
						widgetOptions.data = options.data;
					}

					if (options.init) {
						widgetOptions.init = options.init;
					}

					if (options.dialog) {
						widgetOptions.dialog = options.name;

						CKEDITOR.dialog.add(options.name, (editor2) => {
							return options.dialog(editor2);
						});
					}

					editor.widgets.add(options.name, widgetOptions);
				}
			});

		}

		// Dialog element
		static dialogElement(options) {
			const elementOptions = options;

			elementOptions.setup = function(widget) {
				this.setValue(widget.data[options.id]);
			};

			elementOptions.commit = function(widget) {
				widget.setData(options.id, this.getValue());
			};

			return elementOptions;
		}

	};

})();
