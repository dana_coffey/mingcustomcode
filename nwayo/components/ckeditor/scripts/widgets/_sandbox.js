//-------------------------------------
//-- Widget - Sandbox
//-------------------------------------

//= jshtml /components/ckeditor/templates/sandbox

(() => {
	'use strict';

	app.CKEditor.addWidget({
		name:   'WidgetSandbox',
		desc:   'Sandbox',
		fields: ['title', 'content'],
		tmpl:   app.tmpl.ckeditorSandbox.render(),



		init: function() {
			const $element = $(this.element.$);
			const width = this.element.getStyle('width');  // Exception for width

			this.setData('width', width || '');

			if ($element.hasClass('align-left')) {
				this.setData('align', 'left');
			}

			if ($element.hasClass('align-right')) {
				this.setData('align', 'right');
			}

			if ($element.hasClass('align-center')) {
				this.setData('align', 'center');
			}
		},



		data: function() {
			const $element = $(this.element.$);

			// Width
			$element.css('width', this.data.width);

			// Align
			$element.removeClass('align-left align-right align-center');
			if (this.data.align) {
				$element.addClass(`align-${this.data.align}`);
			}
		},



		dialog: (editor) => {
			return {
				title:     'Sandbox',
				minWidth:  200,
				minHeight: 100,
				contents:  [
					{
						id:       'info',
						elements: [
							app.CKEditor.dialogElement({
								id:    'align',
								type:  'select',
								label: 'Align',
								items: [
									[editor.lang.common.notSet, ''],
									[editor.lang.common.alignLeft, 'left'],
									[editor.lang.common.alignRight, 'right'],
									[editor.lang.common.alignCenter, 'center']
								]
							}),

							app.CKEditor.dialogElement({
								id:    'width',
								type:  'text',
								label: 'Width',
								width: '50px'
							}),

							{
								type:  'file',
								id:    'upload',
								label: 'Select file from your computer',
								size:  38
							},

							{
								'type':        'fileButton',
								'id':          'fileId',
								'label':       'Upload file',
								'for':         ['info', 'upload']

								/*
								 'filebrowser': {
								 	onSelect: (fileUrl) => { // , data
								 		alert(`Successfully uploaded: ${fileUrl}`);
							 		}
								}
								*/
							}
						]
					}
				]
			};
		}
	});





})();
