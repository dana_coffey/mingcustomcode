//-------------------------------------
//-- Widget - Youtube
//-------------------------------------

//= jshtml /components/ckeditor/templates/youtube

(() => {
	'use strict';

	app.CKEditor.addWidget({
		name: 'WidgetYoutube',
		desc: 'Youtube Video',
		fields: [],
		tmpl: app.tmpl.ckeditorYoutube.render(),
		// init: function() {},
		data: function() {
			const $element = $(this.element.$);
			if (this.data.videoId) {
				$element.empty().append(`<iframe type="text/html" width="640" height="360" src="https://www.youtube.com/embed/${this.data.videoId}" frameborder="0" allowFullScreen></iframe>`);
			}
		},
		dialog: () => {
			return {
				title: 'Youtube',
				minWidth: 200,
				minHeight: 100,
				contents: [
					{
						id: 'info',
						elements: [
							app.CKEditor.dialogElement({
								id: 'videoId',
								type: 'text',
								label: 'Video ID',
								width: '200px'
							})
						]
					}
				]
			};
		}
	});

})();
