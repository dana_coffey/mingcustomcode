//-------------------------------------
//-- Widget - Content
//-------------------------------------

//= jshtml /components/ckeditor/templates/content

(() => {
	'use strict';

	app.CKEditor.addWidget({
		name:   'WidgetContent',
		desc:   'Bloc de contenu texte',
		fields: ['title', 'content'],
		tmpl:   app.tmpl.ckeditorContent.render()
	});

})();
