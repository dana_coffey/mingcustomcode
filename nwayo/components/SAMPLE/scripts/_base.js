//-------------------------------------
//-- SAMPLE - Example
//-------------------------------------

//= **require     bower_components/foobar
//= **require     components/nwayo/scripts/wrapper-foobar
//= **jshtml      components/SAMPLE/templates/foobar
//= **jshtml_tree components/SAMPLE/templates

(() => {
	'use strict';

	const local = {};

	// Public -----------------------------------------------------------------//

	// Private ----------------------------------------------------------------//

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		//
	};

	// Cache [DOM] ------------------------------------------------------------//

	local.cacheDOM = () => {
		//
	};

	// Subscribe --------------------------------------------------------------//

	local.subscribe = () => {
		//
	};

	// Bind -------------------------------------------------------------------//

	local.bind = () => {
		//
	};

	// Start ------------------------------------------------------------------//

	local.start = () => {
		//
	};

	// Delayed Start ----------------------------------------------------------//

	local.delayedStart = () => {
		//
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	local.subscribe();
	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});
	// Document loaded
	$.when(DOCUMENT_LOAD).done(() => {
		local.delayedStart();
	});

})();
