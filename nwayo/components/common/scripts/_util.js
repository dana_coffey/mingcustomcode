//-------------------------------------
//-- Util
//-------------------------------------

//= require bower_components/kafe/dist/kafe
//= require bower_components/kafe/dist/style
//= require bower_components/jquery.scrollTo/jquery.scrollTo

(() => {
	'use strict';

	const _consoleLog = global.console.log;
	const _consoleWarn = global.console.warn;
	const _scrollLockStates = {};

	app.util = class {

		static disableLogger() {
			global.console.warn = () => { /**/ };
			global.console.log = () => { /**/ };
		}

		static enableLogger() {
			global.console.log = _consoleLog;
			global.console.warn = _consoleWarn;
		}

		static publishDataEvents(events, el) {
			if (events) {
				events.split('|')
					.forEach((event) => {
						const [_eventName, _eventData] = event.split('=');
						const _data = { element: el };
						if (_eventData) { _data.parameters = _eventData; }
						PubSub.publish(`nwayo.${_eventName}`, _data);
					});
			}
		}

		static getItemsPerRow(selector) {
			let _nbPerRow = 1;
			let _cachedOffset;

			$(selector).each((i, el) => {
				const _offset = $(el).offset().top;
				if (!_cachedOffset) {
					_cachedOffset = _offset;
				} else if (_cachedOffset === _offset) {
					_nbPerRow++;
				} else {
					return false;
				}

				return true;
			});

			return _nbPerRow;
		}

		static equalizeGrid(element, selectors, resize) {
			const $element = $(element);
			const _selectors = selectors.constructor === Array ? selectors : [selectors];

			_selectors.forEach((selector) => {
				const $items = $element.find(selector);
				if ($items.length > 0) {
					const _nbPerRow = app.util.getItemsPerRow($items);
					if (_nbPerRow > 1) {
						kafe.style.equalHeight($items, { nbPerRow: _nbPerRow, resetHeight: true });
					} else {
						$items.css('height', '');
					}
				}
			});

			if (resize && !$element.data('nwayo-equalize-grid')) {
				$element.data('nwayo-equalize-grid', true);
				const _e = PubSub.subscribe('nwayo.resize', () => {
					if ($.contains(global.document, $element.get(0))) {
						app.util.equalizeGrid(element, selectors);
					} else {
						PubSub.unsubscribe(_e);
						$element.removeData('nwayo-equalize-grid');
					}
				});
			}
		}

		static scrollTo(e, speed, options) {
			const $e = $(e);
			const _speed = speed || speed === 0 ? speed : 250;
			const _options = options || { margin: true };
			$.scrollTo($e, _speed, _options);
		}

		static lockScroll(key, value) {
			_scrollLockStates[key] = value;
			__.$html.toggleClass('scroll-lock', value || Object.values(_scrollLockStates).includes(true));
		}

	};

})();
