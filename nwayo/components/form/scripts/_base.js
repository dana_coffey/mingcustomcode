//-------------------------------------
//-- Form - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};

	// Public -----------------------------------------------------------------//

	// Bind -------------------------------------------------------------------//

	local.bind = () => {

		__.$document

			// Checkbox & radio behavior
			.on('click', ':checkbox, :radio', function(e) {
				const $this = $(this);
				const isChecked = $this.is(':checked');
				const id = $this.attr('id');

				if (id) {
					__.$body.find(`label[for="${id}"]`).toggleClass('on', isChecked);
				}

				if (isChecked) {
					const name = $this.attr('name');
					if (name) {
						__.$body.find(`[name="${name}"]`).not(this).prop('checked', false)
							.each((i, el) => {
								const inactiveId = $(el).attr('id');

								if (inactiveId) {
									__.$body.find(`label[for="${inactiveId}"]`).removeClass('on');
								}
							});
					}
				}

				if (!e.originalEvent) {
					e.preventDefault();
					e.stopPropagation();
				}
			});

	};

	// Private ----------------------------------------------------------------//

	// Initialize -------------------------------------------------------------//

	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.bind();
	});

})();

