//-------------------------------------
//-- SAMPLE - Example
//-------------------------------------

//= **require     bower_components/foobar
//= **require     components/nwayo/scripts/wrapper-foobar
//= **jshtml      components/SAMPLE/templates/foobar
//= **jshtml_tree components/SAMPLE/templates

(() => {
	'use strict';

	const local = {};
	const module = {};

	// Public -----------------------------------------------------------------//

	// Private ----------------------------------------------------------------//

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		module.selectors = {
			modal: '.reveal',
			overlay: '.reveal-overlay',
			openClass: 'open'
		};
		module.openClass = 'open';
	};

	// Bind -------------------------------------------------------------------//

	local.bind = () => {
		__.$document
			.on('open.zf.reveal', (e) => {
				$(e.target).closest(module.selectors.overlay).addClass(module.openClass);
				app.util.lockScroll('modal', true);
			})
			.on('closed.zf.reveal', (e) => {
				$(e.target).closest(module.selectors.overlay).removeClass(module.openClass);
				global.setTimeout(() => { app.util.lockScroll('modal', false); }, konstan.transition.reveal);
			});
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.bind();
	});

})();
