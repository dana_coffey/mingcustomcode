//-------------------------------------
//-- Site - Barcode
//-------------------------------------

//= require bower_components/webrtc-adapter/release/adapter_no_edge_no_global
//= require bower_components/quagga/dist/quagga.min

(() => {
	'use strict';

	const local = {};
	const module = {};

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		Modernizr.addTest('barcodestream', () => {
			return Modernizr.getusermedia && !__.$html.hasClass('k-ff');
		});

		module.streamIsAvailable = Modernizr.barcodestream;
		module.streamIsActive = false;
		module.errorVisibleTime = 3000;
		module.detectionCount = 0;
		module.detectedBarcode = '';

		module.debug = false;

		module.config = {
			locate: true,
			locator: {
				patchSize: 'medium',
				halfSample: true
			},
			decoder : {
				readers : [
					'code_128_reader',
					'code_39_reader',
					'code_39_vin_reader',
					'i2of5_reader',

					/*
					{
						format: 'ean_reader',
						config: {
							supplements: [
								'ean_5_reader', 'ean_2_reader'
							]
						}
					},
					'ean_reader',
					*/

					'upc_reader'
				],
				multiple: false
			},
			numOfWorkers: navigator && navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 1,
			lastResult : null,
			debug: module.debug
		};
	};

	// Cache [DOM] ------------------------------------------------------------//

	local.cacheDOM = () => {
		module.$modal = $('[data-barcode]');
		module.$stream = module.$modal.find('[data-barcode-stream]');
		module.$error = module.$modal.find('[data-barcode-error]');
		module.$errorMessages = module.$error.find('[data-barcode-error-msg]');
		module.$fileInput = module.$modal.find('[data-barcode-file]');
		module.$codeField = module.$modal.find('[data-barcode-field]');
	};

	// Bind -------------------------------------------------------------------//

	local.bind = () => {
		global.Quagga.onDetected(_onDetected);

		__.$action('barcode-open').on('click', () => {
			if (module.streamIsAvailable) {
				global.clearTimeout(module.streamTimeout);
				module.streamTimeout = global.setTimeout(_startStream, konstan.transition.animation);
			}
			app.util.lockScroll('barcode', true);
			module.$modal.addClass('show');
		});

		__.$action('barcode-close').on('click', () => {
			app.util.lockScroll('barcode', false);
			module.$modal.removeClass('show');
			global.clearTimeout(module.streamTimeout);
			module.streamTimeout = global.setTimeout(_stopStream, konstan.transition.animation);
		});

		__.$action('barcode-init-stream').on('click', () => {
			_showStream();
			global.setTimeout(_startStream, konstan.transition.animation);
		});

		__.$action('barcode-field-submit').on('click', _fieldSubmit);

		module.$codeField.on('keypress', (e) => {
			if ((e.which ? e.which : e.keyCode) === 13) {
				e.preventDefault();
				_fieldSubmit();
			}
		});

		module.$fileInput.on('change', (e) => {
			if (e.target.files && e.target.files.length) {
				_decode(URL.createObjectURL(e.target.files[0]));
			}
		});
	};

	// Start ------------------------------------------------------------------//

	local.start = () => {
		if (module.streamIsAvailable) {
			_showStream();
		}
	};

	// Private ----------------------------------------------------------------//

	const _getStreamConfig = () => {
		return $.extend(true, {}, module.config, {
			inputStream : {
				type : 'LiveStream',
				target: module.$stream.get(0),
				constraints: { facingMode: 'environment' }
			}
		});
	};

	const _startStream = () => {
		if (!module.streamIsActive) {
			global.Quagga.init(_getStreamConfig(), (error) => {
				if (error) {
					_hideStream();

					return;
				}

				global.Quagga.start();
				module.$stream.addClass('loaded');
				module.streamIsActive = true;
			});
		}
	};

	const _stopStream = () => {
		if (module.streamIsActive) {
			global.Quagga.stop();
			module.$stream.removeClass('loaded');
			module.streamIsActive = false;
		}
	};

	const _showStream = () => {
		module.$stream.addClass('show');
	};

	const _hideStream = () => {
		module.$stream.removeClass('show');
	};

	const _getStaticConfig = (src) => {
		return $.extend(true, {}, module.config, { src : src });
	};

	const _decode = (src) => {
		if (module.streamIsActive) {
			_stopStream();
		}

		global.Quagga.decodeSingle(_getStaticConfig(src), (data) => {
			if (!data || !data.codeResult) {
				_showError('file-decode');
				if (module.streamIsAvailable) {
					_startStream();
				}
			} else if (data.codeResult.code) {
				_submit(data.codeResult.code);
			}
		});
	};

	const _showError = (msgName = 'default') => {
		module.$errorMessages.hide().filter(`[data-barcode-error-msg="${msgName}"]`).show();
		module.$error.addClass('show');
		global.clearTimeout(module.errorTimeout);
		module.errorTimeout = global.setTimeout(() => {
			module.$error.removeClass('show');
		}, module.errorVisibleTime);
	};

	const _onDetected = (data) => {
		if (data.codeResult.code) {
			if (module.streamIsActive) {
				if (module.detectedBarcode === data.codeResult.code) {
					module.detectionCount++;
				} else {
					module.detectionCount = 1;
					module.detectedBarcode = data.codeResult.code;
				}

				if (module.detectionCount >= 10) {
					if (!module.debug) { _stopStream(); }
					_submit(data.codeResult.code);
				}
			}
		}
	};

	const _submit = (code) => {
		if (module.debug) {
			module.$codeField.val(code);
		} else {
			$.get(`/ScanBarcode/${code}`)
				.done((data) => {
					global.location.href = data.Url;
				})
				.fail(() => {
					_showError();
				});
		}
	};

	const _fieldSubmit = () => {
		const code = $.trim(module.$codeField.val());
		if (code) { _submit(code); }
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

})();

