//-------------------------------------
//-- Site
//-------------------------------------

(() => {
	'use strict';

	const local = {};

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		// Foundation
		__.$document.foundation();

		// Custom angular directives
		global.angular
			.module('insite')
			.directive('nwayoEvent', () => {
				return {
					link: function(scope, element, attrs) {
						if (attrs.ngRepeat && scope.$last) {
							app.util.publishDataEvents(attrs.nwayoEvent, element.parent().get(0));
						} else if (attrs.ngShow) {
							const _watcher = scope.$watch(attrs.ngShow, (val) => {
								if (val) {
									_watcher(); // clear watcher
									app.util.publishDataEvents(attrs.nwayoEvent, element.get(0));
								}
							});
						} else if (attrs.ngIf) {
							const _watcher = scope.$watch(attrs.ngIf, (val) => {
								if (val) {
									_watcher(); // clear watcher
									app.util.publishDataEvents(attrs.nwayoEvent, element.get(0));
								}
							});
						}
					}
				};
			})
			.directive('ngSrc', () => {
				return {
					link: function(scope, element, attrs) {
						if (!attrs.ngSrc && !attrs.src) {
							element.parent().addClass('error');
						}
					}
				};
			});
	};

	// Subscribe --------------------------------------------------------------//

	local.subscribe = () => {
		// Foundation Reflow
		PubSub.subscribe('nwayo.foundation-reflow', (e, data) => {
			const $target = data && data.element ? data.parameters ? $(data.element).find(data.parameters) : $(data.element) : __.$document;
			app.util.disableLogger();
			$target.foundation();
			app.util.enableLogger();
		});
		// Equalize
		PubSub.subscribe('nwayo.equalize', (e, data) => {
			if (data.element) {
				const _resize = data.resize !== undefined ? Boolean(data.resize) : true;
				const _selectors = data.parameters ? data.parameters.split(',').map((str) => { return str.trim(); }) : [];
				_equalize(data.element, _selectors, _resize);
			}
		});
		// Equalize Grid
		PubSub.subscribe('nwayo.equalize-grid', (e, data) => {
			if (data.element && data.parameters) {
				const _resize = data.resize !== undefined ? Boolean(data.resize) : true;
				const _selectors = data.parameters.split(',').map((str) => { return str.trim(); });
				if (_selectors.length > 0) {
					app.util.equalizeGrid(data.element, _selectors, _resize);
				}
			}
		});
		// Scroll to
		PubSub.subscribe('nwayo.scroll-to', (e, data) => {
			if (data && data.element) {
				const $el = $(data.element);
				if (__.$html.scrollTop() > $el.offset().top) {
					app.util.scrollTo(data.element);
				}
			}
		});
	};

	// Private ----------------------------------------------------------------//

	const _equalize = (element, selectors, resize) => {
		const $element = $(element);

		if (selectors && selectors.length > 0) {
			selectors.forEach((selector) => {
				kafe.style.equalHeight($element.find(selector), { resetHeight: true });
			});
		} else {
			kafe.style.equalHeight($element, { resetHeight: true });
		}

		if (resize && !$element.data('nwayo-equalize')) {
			$element.data('nwayo-equalize', true);
			const _e = PubSub.subscribe('nwayo.resize', () => {
				if ($.contains(global.document, $element.get(0))) {
					_equalize(element, selectors);
				} else {
					PubSub.unsubscribe(_e);
					$element.removeData('nwayo-equalize');
				}
			});
		}
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	local.subscribe();

})();

