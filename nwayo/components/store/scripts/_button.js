//-------------------------------------
//-- Site - Button
//-------------------------------------

(() => {
	'use strict';

	const local = {};
	const module = {};

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		module.loadingTimeout = 2000;
	};

	// Subscribe --------------------------------------------------------------//

	local.subscribe = () => {
		// Loading Start
		PubSub.subscribe('nwayo.button.load.start', (e, btn) => {
			$(btn).addClass('load');
		});
		// Loading Done
		PubSub.subscribe('nwayo.button.load.done', (e, btn) => {
			$(btn).addClass('load-done');
			global.setTimeout(() => {
				$(btn).addClass('load-out').removeClass('load');
				global.setTimeout(() => {
					$(btn).removeClass('load-done load-out');
				}, konstan.transition.animation);
			}, module.loadingTimeout);
		});
		// Loading Fail
		PubSub.subscribe('nwayo.button.load.fail', (e, btn) => {
			$(btn).removeClass('load');
		});
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	local.subscribe();

})();

