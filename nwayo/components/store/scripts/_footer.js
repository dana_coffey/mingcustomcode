//-------------------------------------
//-- Store - Footer Links
//-------------------------------------

(() => {
	'use strict';

	const local = {};
	const module = {};

	// Public -----------------------------------------------------------------//

	// Private ----------------------------------------------------------------//

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		module.selectors = {
			block: '.footer',
			equalize: '[data-equalize]'
		};
		module.activeClass = 'on';
	};

	// Cache DOM --------------------------------------------------------------//

	local.cacheDOM = () => {
		module.$block = $(module.selectors.block);
	};

	// Bind ------------------------------------------------------------------//

	local.bind = () => {
		app.util.equalizeGrid(module.$block, module.selectors.equalize, true);
	};

	// Start ------------------------------------------------------------------//

	local.start = () => {
		$(module.$block).find('.list').each(function() {
			const $list = $(this);
			const $handle = $list.find('.list-title');
			const $listWrapper = $list.find('.list-wrapper');
			const $listContent = $list.find('.list-content');
			let _timeout;

			$handle.on('click', () => {
				global.clearTimeout(_timeout);
				if ($list.hasClass(module.activeClass)) {
					$listWrapper.height($listContent.outerHeight(true));
					_timeout = global.setTimeout(() => {
						$listWrapper.css('height', 0);
						$list.removeClass(module.activeClass);
					}, 25);
				} else {
					$list.addClass(module.activeClass);
					$listWrapper.height(0).height($listContent.outerHeight(true));
					_timeout = global.setTimeout(() => {
						$listWrapper.height('auto');
					}, konstan.transition.animation);
				}
			});
		});
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

})();
