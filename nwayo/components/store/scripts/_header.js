//-------------------------------------
//-- Site - Header
//-------------------------------------

(() => {
	'use strict';

	const local = {};
	const module = {};

	// Cache ------------------------------------------------------------------//

	local.cache = () => {
		module.selector = {
			site: '#site-menu',
			account: '#account-menu',
			search: '#site-search'
		};

		module.allMenuSelector = `${module.selector.site}, ${module.selector.account}, ${module.selector.search}`;
		module.jsMenuSelector = `${module.selector.site}, ${module.selector.account}`;
	};

	// Cache [DOM] ------------------------------------------------------------//

	local.cacheDOM = () => {
		module.$header = $('.site-header');

		// cannot cache these because of angular workflow
		module.$allMenus = () => { return module.$header.find(module.allMenuSelector); };
		module.$jsMenus = () => { return module.$allMenus().filter(module.jsMenuSelector); };
	};

	// Subscribe --------------------------------------------------------------//

	local.subscribe = () => {
		PubSub.subscribe('nwayo.resize', () => { _updateJsMenus(); });
	};

	// Bind -------------------------------------------------------------------//

	local.bind = () => {
		module.$header
			.on('click', module.selector.search, function() {
				if (this.checked) {
					$(this).next().find('input').val('').trigger('focus');
				}
			});

		__.$document
			// close menus when a link is clicked
			.on('click', 'a', _closeMenus)
			// prevent close inside menu
			.on('click', '[data-site-menu]', function(e) {
				const _data = $(this).data('site-menu');
				if (_data === 'always' || _isMobile()) {
					e.stopPropagation();
				}
			})
			// toggle menu
			.on('click', module.allMenuSelector, (e) => {
				e.stopPropagation();
				_updateJsMenus();
			})
			// prevent close for labels that toggle a menu
			.on('click', 'label[for]', function(e) {
				if (module.$allMenus().filter(`#${this.getAttribute('for')}`).length) {
					e.stopPropagation();
				}
			})
			// close menus
			.on('click', _closeMenus);
	};

	// Start ------------------------------------------------------------------//

	local.start = () => {
		module.$allMenus().prop('disabled', false);
	};

	// Private ----------------------------------------------------------------//

	const _updateJsMenus = () => {
		const $menus = module.$jsMenus().filter(':checked');
		const _isActive = _isMobile() && $menus.length !== 0;

		if (_isActive) {
			$menus.each((i, menu) => {
				const $menu = $(menu);
				$menu.next().css('top', $menu.offset().top - __.$window.scrollTop());
			});
		}

		app.util.lockScroll('header', _isActive);
	};

	const _isMobile = (bp = 'xlarge') => {
		return !global.Foundation.MediaQuery.atLeast(bp);
	};

	const _closeMenus = () => {
		module.$allMenus().filter(':checked').prop('checked', false).trigger('click');
	};

	// Initialize -------------------------------------------------------------//

	local.cache();
	local.subscribe();
	// DOM Ready
	$.when(DOM_PARSE).done(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

})();
